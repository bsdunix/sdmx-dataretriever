﻿// -----------------------------------------------------------------------
// <copyright file="DataWriterBehavior.cs" company="EUROSTAT">
//   Date Created : 2015-10-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Constants
{
    using Org.Sdmxsource.Sdmx.Api.Engine;

    /// <summary>
    /// An enumeration of <see cref="IDataWriterEngine"/> delay behavior
    /// </summary>
    public enum DataWriterDelayBehavior
    {
        /// <summary>
        /// Delay writing until first call to <see cref="IDataWriterEngine.StartDataset"/>
        /// </summary>
        UntilFirstStartDataSet,

        /// <summary>
        /// Delay writing until first call to either <see cref="IDataWriterEngine.StartSeries"/> <see cref="IDataWriterEngine.StartGroup"/> or <see cref="IDataWriterEngine.WriteObservation(string,string,Org.Sdmxsource.Sdmx.Api.Model.Objects.Base.IAnnotation[])"/> and other overloads
        /// </summary>
        UntilFirstKey
    }
}
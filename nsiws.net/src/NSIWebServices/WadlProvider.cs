﻿// -----------------------------------------------------------------------
// <copyright file="WadlProvider.cs" company="EUROSTAT">
//   Date Created : 2015-10-15
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Rest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Web;
    using System.Text;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class WadlProvider : IWadlProvider
    {
        /// <summary>
        /// The predefined application wadl
        /// </summary>
        private const string ApplicationWadl = "application.wadl";

        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(WadlProvider));

        /// <summary>
        /// Gets the WADL.
        /// </summary>
        /// <returns>The <see cref="Stream"/> containing the WADL.</returns>
        public Stream GetWadl()
        {
            try
            {
                var context = WebOperationContext.Current;
                if (context != null)
                {
                    var originalString = context.IncomingRequest.UriTemplateMatch.RequestUri.OriginalString;
                    var index = originalString.IndexOf(ApplicationWadl, StringComparison.OrdinalIgnoreCase);
                    var restUri = originalString.Substring(0, index);

                    ApplicationWadlTemplate applicationWadl = new ApplicationWadlTemplate();
                    applicationWadl.Session = new Dictionary<string, object>(StringComparer.Ordinal) { { "applicationUrl", restUri } };
                    applicationWadl.Initialize();

                    var buffer = Encoding.UTF8.GetBytes(applicationWadl.TransformText());
                    context.OutgoingResponse.ContentType = "application/vnd.sun.wadl+xml";
                    return new MemoryStream(buffer);
                }
            }
            catch (Exception e)
            {
                _log.Error(e.Message, e);
                throw;
            }

            _log.Error("WebOperationContextCurrent is null");
            throw new SdmxInternalServerException();
        }
    }
}
$(document).ready(function() {
    var chart1 = new Highcharts.Chart({
        chart: {
            renderTo: 'chartContainer',
            type: 'bar'
        },
        title: {
            text: 'Fruit Consumption'
        },
        xAxis: {
            categories: ['Apples', 'Bananas', 'Oranges']
        },
        yAxis: {
            title: {
                text: 'Fruit eaten'
            }
        },
        series: [{
		name: 'Sonia',
		data: [1, 2, null]
        }]
    });
});

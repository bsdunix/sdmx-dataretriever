"use strict";
function selectSDMXDataXPath(product, flow, xmlDoc) {
    let finalResult = null;
    let resolveNS = {
        lookupNamespaceURI: function (prefix) {
            let result;
        prefix === "message" ? result = xmlDoc.documentElement.namespaceURI : result = null;
        return result;
        }
    }
    if (typeof flow === 'undefined') {
        finalResult = parseFloat(xmlDoc.evaluate("/message:StructureSpecificData/message:DataSet/Series[@SIEC='" + product + "']//@OBS_VALUE", xmlDoc, resolveNS, 2, null).stringValue.replace(/,/g, ""));
    } else {
        finalResult = parseFloat(xmlDoc.evaluate("/message:StructureSpecificData/message:DataSet/Series[@SIEC='" + product + "' and @NRG_FLOWS='" + flow + "']//@OBS_VALUE", xmlDoc, resolveNS, 2, null).stringValue.replace(/,/g, ""));
    }
    //NB put the unit code inside the conversionFactor and remove the switch statement?
    switch (xmlDoc.evaluate("/message:StructureSpecificData/message:DataSet/Series[@SIEC='" + product + "' and @NRG_FLOWS='" + flow + "']/@NRG_UNITS", xmlDoc, resolveNS, 2, null).stringValue) {
        case "HSO":
            finalResult *= 1;
            break;
        case "HWU":
            // What about Falling Water? Conversion factor = 0 in the spreadsheet.
            finalResult *= 3.6;
            break;
        case "CSR":
            finalResult *= 9.135;
            break;
        case "WSR":
            finalResult *= conversionFactor(product, "Terajoule");
            break;
    }
    return finalResult;
}

// Unit conversion function.
function conversionFactor(inputProduct, desiredUnit) {
    desiredUnit = desiredUnit.toLowerCase();
    // Have to refactor to include non-WSR measured products!!!
    let Terajoule = {
        "01": 29.3076,
        "0390": 20,
        "4100": 42.3,
        "0340": 28,
        "4671": 43,
        "4620": 46.4,
        "4300": 43,
        "4653": 44.3,
        "0312": 28.2,
        "4200": 44.2,
        "4661": 44.1,
        "4669": 43.8,
        "02": 11.2834,
        "0220": 11.9,
        "4630": 47.3,
        "4692": 40.2,
        "4652": 44.3,
        "4640": 44.5,
        "0129": 25.8,
        "4500": 36,
        "0311": 28.2,
        "5290": 27.4,
        "2000": 8.9,
        "4694": 32.5,
        "4699": 40.2,
        "11": 9.76,
        "4693": 40.2,
        "4680": 40.4,
        "4610": 49.5,
        "0210": 18.9,
        "4691": 40.2,
        "5210": 26.8,
        "4400": 30,
        "0110": 26.7,
        "4651": 44.3,
        "0330": 20.7,
        "0320": 20.7,
        "5220": 36.8,
        "5230": 40,
        "1": 9.76,
        "5120": 7.7221,
        "4695": 40.2,
        "0121": 28.2,
        "5160": 29.5
    }
    let returnValue = 1;
    if (desiredUnit === "terajoule" || desiredUnit === "tj") {
        if (Terajoule[inputProduct]) {
            returnValue = Terajoule[inputProduct];
        }
    }
    return returnValue;
}
// Try and make it recursive.
function regexFilter(string, regex, captureGroupNumber) {
    if (typeof captureGroupNumber === "string") {
        return console.log("Error: Capture Group should be a number");
    }
    let resultString = [];
    if (string.push === undefined) {
         let regexMatch = regex.exec(string);
         while (regexMatch !== null) {
             // Have to make the method more robust. Irrespective of a capture group being present it should do it's job. Have to look into this!!!!!! Maybe add a parameter tunable!
             resultString.push(regexMatch[captureGroupNumber]);
             regexMatch = regex.exec(string);
         }
    } else {
        resultString = {};
        for (let substring in string) {
            //Recursiveness should go here probably?!
            let regexMatch = regex.exec(string[substring]);
            resultString[string[substring]] = [];
            while (regexMatch !== null) {
                // Capture group not present.
                resultString[string[substring]].push(regexMatch[captureGroupNumber]);
                regexMatch = regex.exec(string[substring]);
            }
        }
    }
    return resultString;
}

function extractStringsForQuery(string) {
    let returnObject = {};
    if (string.indexOf("Sum of") !== -1 && string.indexOf("col") !== -1) {
        returnObject["Columns"] = [];
        returnObject["Columns"] = string.match(/\d+?/g);
    } else if (string.indexOf("row") !== -1 || string.indexOf("Row") !== -1) {
        returnObject["Rows"] = [];
        returnObject["Rows"] = string.match(/\d+?/g);
    } else {
        returnObject["PROD"] = [];
        returnObject["FLOW"] = [];
        let variablesArray = regexFilter(string, /\(([^)]*)\)/g, 1);
        variablesArray = regexFilter(variablesArray, /[\d\w]{2,}/g, 0);
        for (let variable in variablesArray) {
            returnObject["PROD"].push(variablesArray[variable][0]);
            if (variablesArray[variable][1] !== undefined) returnObject["FLOW"].push(variablesArray[variable][1]);
        }
    }
    return returnObject;
}

function removeDuplicates(array) {
    if (array.push === undefined) {
        return Console.log("Error: Not an Array!");
    }
    let uniqueList = [];
    let duplicateFilter = {};
        for (let item in array) {
            duplicateFilter[array[item]] = 0;
        }
        for (let uniqueItem in duplicateFilter) {
            uniqueList.push(uniqueItem);
        }
    return uniqueList;
}
// Changes XML to JSON
function xmlToJson(xml) {
    // Create the return object
    var obj = {};
    if (xml.nodeType == 1) { // element
        // do attributes
        if (xml.attributes.length > 0) {
            obj["@attributes"] = {};
            for (var j = 0; j < xml.attributes.length; j++) {
                var attribute = xml.attributes.item(j);
                obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
            }
        }
    } else if (xml.nodeType == 3) { // text
        obj = xml.nodeValue;
    }
    // do children
    if (xml.hasChildNodes()) {
        for(var i = 0; i < xml.childNodes.length; i++) {
            var item = xml.childNodes.item(i);
            var nodeName = item.nodeName;
            if (typeof (obj[nodeName]) === "undefined") {
                obj[nodeName] = xmlToJson(item);
            } else {
                if (typeof(obj[nodeName].push) == "undefined") {
                    var old = obj[nodeName];
                    obj[nodeName] = [];
                    obj[nodeName].push(old);
                }
                obj[nodeName].push(xmlToJson(item));
            }
        }
    }
    return obj;
}

function selectStep(event, step) {
    let tabcontent = document.getElementsByClassName("contents");
    for (let element = 0; element < tabcontent.length; element++) {
        tabcontent[element].style.display = "none";
    }
    let buttonLink = document.getElementsByClassName("link");
    for (let element = 0; element < buttonLink.length; element++) {
        buttonLink[element].className = buttonLink[element].className.replace(" active", "");
    }
    document.getElementById(step).style.display = "block";
    event.currentTarget.className += " active";
}

//* The function will parse the Excel XML file and output an array of columns within rows, that can be loaded into the wijmo flexgrid framework.
function parseExcelSpreadsheet(data) {
    let EB = [];
    let result = xmlToJson(data);
    result = result["Workbook"]["Worksheet"]["Table"];
    for (let row in result["Row"]) {
        let cells = result["Row"][row]["Cell"];
        let datacells = {};
        let lastIndex = 0;
        for (let cell in cells) {
            if (cells[cell]["Data"] === undefined) {
                datacells[lastIndex++] = cells[cell]["#text"];
            } else if (cells[cell]["Data"]["#text"] === undefined) {
                datacells[lastIndex++] = "";
            } else if (cells[cell]["@attributes"] !== undefined && cells[cell]["@attributes"]["ss:Index"]) {
                // Excel indexing starts with 1 vs. 0!!! OMG how much pain!!!!
                lastIndex = (parseInt(cells[cell]["@attributes"]["ss:Index"]) - 1);
                datacells[lastIndex++] = cells[cell]["Data"]["#text"];
            } else {
                datacells[lastIndex++] = cells[cell]["Data"]["#text"];
            }
        }
        EB.push(datacells);
    }
    return EB;
}

// The function will replace via regex variables with their respective data values from the database. opts takes XMLdata and the xml namespace as arguments, they are required.
function swapVariablesForData(cellContent, dataFile) {
    if (cellContent !== undefined) {
        if (cellContent.indexOf("+") === 0 || cellContent.indexOf("-") === 0) {
            let sum = 0;
            let regexPattern = /([+-])\(([^)]+)\)/g;
            let regexPattern2 = /[\d\w]{2,}/g;
            let returnedValues = regexPattern.exec(cellContent);
            while (returnedValues !== null) {
                let anotherSplit = {};
                anotherSplit[returnedValues[2]] = regexFilter(returnedValues[2], regexPattern2, 0);
                for (let item in anotherSplit) {
                    anotherSplit[item] = selectSDMXDataXPath(anotherSplit[item][0], anotherSplit[item][1], dataFile);
                    if (returnedValues[2] === item && anotherSplit[item]) {
                        if (returnedValues[1] === "+") {
                            sum += anotherSplit[item];
                        } else if (returnedValues[1] === "-") {
                            sum -= anotherSplit[item];
                        }
                    }
                }
                returnedValues = regexPattern.exec(cellContent);
            }
            cellContent = sum;
        }
        // Paste here the contents of the if statement from sumOfCells();
    } else cellContent = "";
    return cellContent;
}

// Does the arithmetic operation on cells that sum up columns and/or rows.
function sumOfCells(row, column, cellContent, gridObj) {
    let returnValue = null;
    if (cellContent !== undefined) {
        let sum = 0;
        let cellData = null;
        if (cellContent.indexOf("Sum of") !== -1 && (cellContent.indexOf("col.") !== -1 || cellContent.indexOf("Col.") !== -1)) {
            let sumOfRowsOrColumns = cellContent.match(/\d+?/g);
            for (let rowOrColumn in sumOfRowsOrColumns) {
                let offsetColumn = parseInt(sumOfRowsOrColumns[rowOrColumn]) + 2;
                // How to deal with object context and object methods?!
                cellData = parseFloat(gridObj.getCellData(parseInt(row), offsetColumn).replace(/,/g, ""));
                if (!(cellData)) {
                    sum += 0;
                } else sum += cellData;
            }
            returnValue = sum;
        } else if (cellContent.indexOf("Sum of") !== -1 && (cellContent.indexOf("rows") !== -1 || cellContent.indexOf("Rows") !== -1)) {
            let sumOfRowsOrColumns = cellContent.match(/\d+?/g);
            for (let rowOrColumn in sumOfRowsOrColumns) {
                // Have to put an offset as the data in the spreadsheet is not found exactly in the spefic row.
                cellData = parseFloat(gridObj.getCellData(parseInt(sumOfRowsOrColumns[rowOrColumn]), parseInt(column)).replace(/,/g, ""));
                if (!(cellData)) {
                    sum += 0;
                } else sum += cellData;
            }
            returnValue = sum;
        }
    } else returnValue = "";
    return returnValue;
}
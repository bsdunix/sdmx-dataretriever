// Creates drop-down menus based on the dimensions available in the DSD. By default requests the DSD for IRES but can be supplanted with any other DSD.
var spinnerContainer;
function createControls(jsonDataset, containerName) {
	jsonDataset = JSON.parse(jsonDataset);
	var controlPanel = document.getElementById(containerName);
	var forms = document.createElement("form");
	forms.setAttribute("name", "energy_balance_query");
	for (var dimensions in jsonDataset[0].dimensions.series) {
		if (jsonDataset[0].dimensions.series[dimensions].id === "GEO") { // Filter the dimensions in the DSD to output only the Country list so the end user can select what she wants to visualize.
			var formNameVar = document.createElement("select");
			formNameVar.setAttribute("name", jsonDataset[0].dimensions.series[dimensions].name);
			for (var country in jsonDataset[0].dimensions.series[dimensions].values) {
				var optionElement = document.createElement("option");
				optionElement.setAttribute("value", jsonDataset[0].dimensions.series[dimensions].values[country].name);
				optionElement.setAttribute("id", jsonDataset[0].dimensions.series[dimensions].values[country].id);
				optionElement.appendChild(document.createTextNode(jsonDataset[0].dimensions.series[dimensions].values[country].name));
				formNameVar.appendChild(optionElement);
			}
			var balanceSheetType = document.createElement("select");
			balanceSheetType.name = "spreadsheetType";
			var balanceSheetTypeOption = [];
			balanceSheetTypeOption[0] = document.createElement("option");
			balanceSheetTypeOption[0].id = "simple format";
			balanceSheetTypeOption[1] = document.createElement("option");
			balanceSheetTypeOption[1].setAttribute("id", "detailed format");
			balanceSheetTypeOption[0].appendChild(document.createTextNode("Simple"));
			balanceSheetTypeOption[1].appendChild(document.createTextNode("Detailed"));
			balanceSheetType.appendChild(balanceSheetTypeOption[0]);
			balanceSheetType.appendChild(balanceSheetTypeOption[1]);
			var formSubmitButton = document.createElement("input");
			formSubmitButton.setAttribute("type", "submit");
			formSubmitButton.setAttribute("value", "submit");
			forms.appendChild(document.createTextNode("Please select the " + jsonDataset[0].dimensions.series[dimensions].name + " for data visualization and the amount of detail you want in the balance sheet:"));
			forms.appendChild(formNameVar);
			forms.appendChild(balanceSheetType);
			forms.appendChild(formSubmitButton);
			controlPanel.appendChild(forms);
		}
	}
	forms.addEventListener("submit", function(event) {
		// Stop from submitting the form to the server and instead redirect the user input to restQuery() which will do an Ajax call to the sdmx server.
		event.preventDefault();
		spinnerContainer = document.createElement("div");
		spinnerContainer.setAttribute("id", "spinner");
		spinnerContainer.setAttribute("class", "loader");
		document.getElementById("balances").appendChild(spinnerContainer);
		//Pull the Energy Balances spreadsheet based on the type of table the end-user requests and extract all the information into an object that contains also the dimensions that need to be queried to the server.
		Ajax("UN EB Matrix (" + forms.elements.spreadsheetType[forms.spreadsheetType.selectedIndex].id + ").xml", function(xmlBalanceSheetData) {
			Ajax("commodity_codes.xml", function(transcoded_data) {
				var transcoded_object = createBalanceObject(transcoded_data);
				var balanceTable = createBalanceObject(xmlBalanceSheetData);
				balanceTable = JSON.parse(balanceTable);
				transcoded_data = JSON.parse(transcoded_object);
				for (var row in balanceTable) {
					for (var column in balanceTable[row].columns) {
						for (var flows in balanceTable[row].columns[column]) {
							for (var commodity in transcoded_data) {
								if (balanceTable[row].columns[column][flows] === commodity) {
									balanceTable[row].columns[column][flows] = transcoded_data[commodity].id;
								}
							}
						}
					}
				}
				balanceTable.restVariablesForQuery.countries = forms.elements.Country[forms.Country.selectedIndex].id;
				return restQuery(balanceTable);
			});
		});
	});
}

// Takes as input the data from the end user and GETs the information from the server.
function restQuery(user_data) {
	//An ajax request to SDMX for energy products takes the form of the first field containing the frequency dimension, second after dot is the country, third is energy products, fourth is flows, and fifth (which should be actually an attribute rather than dimension) unit of measurement.
	var ajax_request = "http://sdmx.northcentralus.cloudapp.azure.com:5000/rest/data/IRES/A." + user_data.restVariablesForQuery.countries + ".../?startPeriod=1999&endPeriod=2000";
	Ajax(ajax_request, function(xmlData) {
		if (xmlData === undefined || xmlData === null || xmlData.childNodes[0].nodeName === "string") {
			document.getElementById("balances").removeChild(spinnerContainer);
			var xmlResponseString = xmlData.getElementsByTagName("string")[0].childNodes[0];
			document.getElementById("balances").appendChild(document.createTextNode(xmlResponseString.nodeValue));
			console.log(xmlResponseString.nodeValue);
		}
		var jsonData = parseGenericDataset(xmlData);
		jsonData = JSON.parse(jsonData);
		console.log(jsonData.dataSets[0]);
		for (var series in jsonData.dataSets[0]) {
			console.log(series);
			var splitDimensions = series.split(":");
			console.log(splitDimensions);
		}
		jsonData = JSON.stringify(jsonData, null, 4);
		document.getElementById("balances").removeChild(spinnerContainer);
		return document.getElementById('balances').appendChild(document.createTextNode(jsonData));
	});
	document.getElementById("balances").appendChild(createBalanceSheet(user_data));
}

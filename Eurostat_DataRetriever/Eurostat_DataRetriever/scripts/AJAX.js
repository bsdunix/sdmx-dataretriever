function ajaxGET(fileObj, callback) {
	if (!fileObj.file) {
		console.log('null stream');
		return "could not fetch remote file or search criteria do not exist";
	}
	var xhr = new XMLHttpRequest();
	//xhr.timeout = 6000;
	xhr.onload = function() {
		if (callback) {
			callback(xhr.responseXML);
		} else {
			return xhr.responseXML;
		}
	};
	/*
	xhr.ontimeout = function() {
		xhr.customMessage = "Connection timeout. Could not reach host.";
		if (callback) {
			callback(xhr);
		} else {
			return xhr;
		}
	};
	*/
	xhr.onerror = function() {
		xhr.customMessage = "Connection timeout. Could not reach host. Check the console for details of the error.";
		if (callback) {
			callback(xhr);
		} else {
			return xhr;
		}
	};
	try {
		xhr.open('GET', fileObj.file, true);
		if (fileObj.customHeader) {
            for (var header in fileObj.customHeader) {
                xhr.setRequestHeader(header, fileObj.customHeader[header]);
            }
		}
		xhr.send();
	}
	catch (error) {
		xhr.customMessage = error.message;
		console.log(error);
	}
}

function ajaxPOST(URL, parameters, callback) {
	var xhr = new XMLHttpRequest();
	//xhr.timeout = 6000;
	/*
	xhr.ontimeout = function() {
		xhr.customMessage = "Connection timeout. Could not reach host.";
		if (callback) {
			callback(xhr);
		} else {
			return xhr;
		}
	};
	*/
	xhr.onload = function () {
	    if (callback) {
	        callback(xhr.responseXML);
	    } else {
	        return xhr.responseXML;
	    }
	}
	xhr.onerror = function() {
		xhr.customMessage = "Connection timeout. Could not reach host. Check the console for details of the error.";
		if (callback) {
			callback(xhr);
		} else {
			return xhr;
		}
	};
	try {
		xhr.open('POST', URL, true);
        for (var header in parameters.customHeader) {
            xhr.setRequestHeader(header, parameters.customHeader[header]);
        }
		xhr.send(parameters.RESTDataQuery);
	}
	catch (error) {
		xhr.customMessage = error.message;
		console.log(error);
	}
}

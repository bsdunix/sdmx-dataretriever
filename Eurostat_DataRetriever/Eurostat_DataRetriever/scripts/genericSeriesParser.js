var sdmxNS = {};
sdmxNS.mes = sdmxNS.message = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message";
sdmxNS.com = sdmxNS.common = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common";
sdmxNS.generic = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic";
sdmxNS.str = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure";

// This will likely be removed or changed.
function parseGenericMessage(jsonDataset, jsonDSD) {
	jsonDataset = JSON.parse(jsonDataset);
	jsonDSD = JSON.parse(jsonDSD);
	var genericMessage = {};
	for (var series in jsonDataset.dataSets) {
		for (var seriesContent in jsonDataset.dataSets[series].series) {
			for (var seriesObject in jsonDataset.dataSets[series].series[seriesContent]) {
				//console.log(jsonDataset.dataSets[series].series[seriesContent][seriesObject]);
				for (var dsd in jsonDSD) {
					for (var seriesItems in jsonDSD[dsd].dimensions.series) {
						if (jsonDataset.dataSets[series].series[seriesContent][seriesObject].id === jsonDSD[dsd].dimensions.series[seriesItems].id) {
							jsonDataset.dataSets[series].series[seriesContent][seriesObject] = jsonDSD[dsd].dimensions.series[seriesItems];
						}
					}
				}
			}
		}
	}
	genericMessage.header = jsonDataset.header;
	genericMessage.structure = jsonDSD;
	genericMessage.dataSets = jsonDataset.dataSets;
	genericMessage = JSON.stringify(genericMessage);
	return genericMessage;
}

// Takes the sdmx message in xml format and returns as a JSON string.
function parseGenericDataset(genericSeriesFile, callback) {
	if (!genericSeriesFile) {
		console.log("null stream");
		return false;
	}
	var genericSeries = {};
	genericSeries.header = {};
	genericSeries.header.id = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'ID')[0].childNodes[0].nodeValue;
	genericSeries.header.test = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'Test')[0].childNodes[0].nodeValue;
	genericSeries.header.prepared = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'Prepared')[0].childNodes[0].nodeValue;
	genericSeries.header.sender = [];
	var sendersNode = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'Sender');
	genericSeries.header.sender = populateSendersReceivers(sendersNode);
	genericSeries.header.receiver = [];
	var receiverNode = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'Receiver');
	genericSeries.header.receiver = populateSendersReceivers(receiverNode);
	//Don't like this, change it!
	genericSeries.header.name = genericSeriesFile.getElementsByTagNameNS(sdmxNS.common, 'Name')[2].childNodes[0].nodeValue;
	genericSeries.header.dataSetID = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'DataSetID')[0].childNodes[0].nodeValue;
	genericSeries.header.structure = {};
	/*
	genericSeries.header.reportingBegin = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'ReportingBegin')[0].childNodes[0].nodeValue;
	genericSeries.header.reportingEnd = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'ReportingEnd')[0].childNodes[0].nodeValue;
	genericSeries.header.validFrom = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'ValidFrom')[0].childNodes[0].nodeValue;
	genericSeries.header.validTo = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'ValidTo')[0].childNodes[0].nodeValue;
	genericSeries.header.publicationYear = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'PublicationYear')[0].childNodes[0].nodeValue;
	genericSeries.header.publicationPeriod = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'PublicationPeriod')[0].childNodes[0].nodeValue;
	*/
	genericSeries.header.links = [];
	var structure = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'Structure')[0];
	genericSeries.header.structure.structureId = structure.getAttribute('structureID');
	genericSeries.header.structure.dimensionAtObservation = structure.getAttribute('dimensionAtObservation');
	var structureNodes = removeTextNodes(structure.childNodes);
	for (var structureNodesCounter = 0; structureNodesCounter < structureNodes.length; structureNodesCounter++) {
		var structureRef = removeTextNodes(structureNodes[structureNodesCounter].childNodes);
		for (var structureRefCounter = 0; structureRefCounter < structureRef.length; structureRefCounter++) {
			var structureDetails = createLinks(structureRef[structureRefCounter], "datastructure");
			genericSeries.header.structure.agencyID = structureDetails.codelistObject.agencyID;
			genericSeries.header.structure.id = structureDetails.codelistObject.id;
			genericSeries.header.structure.version = structureDetails.codelistObject.version;
			genericSeries.header.links.push(structureDetails.linkObject);
		}
	}
	genericSeries.structure = {};
	genericSeries.structure.links = [];
	genericSeries.structure.dimensions = {};
	genericSeries.structure.dimensions.series = [];
	genericSeries.structure.dimensions.observation = [];
	genericSeries.structure.attributes = {};
	genericSeries.structure.attributes.series = [];
	// To populate the attributes at the series level, the SeriesKey nodes with the flag attributes set should be passed to parseCrossSectionalDimensions(). Currently a limitiation of the backend implementations.
	genericSeries.structure.attributes.observation = [];
	var seriesNodes = genericSeriesFile.getElementsByTagNameNS(sdmxNS.generic, "SeriesKey");
	var obsNodes = genericSeriesFile.getElementsByTagNameNS(sdmxNS.generic, "Obs");
	genericSeries.structure.dimensions.series = parseCrossSectionalDimensions(seriesNodes, "dimensions"); // Parse the dimensions by filtering the output. In this case only the dimensions at the series level would be printed out.
	genericSeries.structure.dimensions.observation = parseCrossSectionalDimensions(obsNodes, "dimensions"); // Parse the dimensions by filtering the output. In this case only the dimensions at the observation level would be printed out.
	genericSeries.structure.attributes.observation = parseCrossSectionalDimensions(obsNodes, "attributes"); // Parse the attributes by filtering the output. In this case only the attributes at the observation level would be printed out.
	genericSeries.dataSets = []; // DataSet related information
	var dataSets = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, "DataSet");
	var dataSetsLength = dataSets.length;
	for (var dataSetsIt = 0; dataSetsIt < dataSetsLength; dataSetsIt++) {
		var dataSet = {};
		dataSet.action = genericSeriesFile.getElementsByTagNameNS(sdmxNS.message, 'DataSetAction')[0].childNodes[0].nodeValue;
		dataSet.series = {};
		var series = removeTextNodes(dataSets[dataSetsIt].childNodes);
		var seriesLength = series.length;
		for (var seriesIt = 0; seriesIt < seriesLength; seriesIt++) {
			// parseCrossSectionalDataSet() returns an object with the attributes at the series and observation level plus the observation values for each dimension at observation. It's named after the SeriesKey elements comprising the series separated by a colon merged into one string.
			var seriesElement = parseCrossSectionalDataSet(series[seriesIt]);
			//have to reiterate the item so that I can extract the property name and assign it to the series object.
			for (var seriesItem in seriesElement) {
				dataSet.series[seriesItem] = seriesElement[seriesItem];
			}
		}
		genericSeries.dataSets.push(dataSet);
	}
	for (var datasetSeries in genericSeries.dataSets[0].series) {
		var seriesNameArray = [];
		var DatasetSeries;
		seriesNameArray = datasetSeries.split(":");
		seriesNameArrayLength = seriesNameArray.length;
		// Swap attributes at the observation level similarly as with the time_dimension.
		for (var datasetObservations in genericSeries.dataSets[0].series[datasetSeries].observations) {
			for (var datasetObservationAttributes in genericSeries.dataSets[0].series[datasetSeries].observations[datasetObservations]) {
				for (var structureObservationAttributes in genericSeries.structure.attributes.observation) {
					if (datasetObservationAttributes === (parseInt(structureObservationAttributes) + 1).toString()) {
						for (var datasetObservationAttributesValue in genericSeries.structure.attributes.observation[structureObservationAttributes].values) {
							if (genericSeries.structure.attributes.observation[structureObservationAttributes].values[datasetObservationAttributesValue].id === genericSeries.dataSets[0].series[datasetSeries].observations[datasetObservations][datasetObservationAttributes]) {
								genericSeries.dataSets[0].series[datasetSeries].observations[datasetObservations][datasetObservationAttributes] = structureObservationAttributes;
							}
						}
					}
				}
			}
			// Swap observation string (year value) with the structure based iterator position. eg. "1999" -> 0, where "1999" is the first value in the codelist for the requested message.
			for (var observationDimensions in genericSeries.structure.dimensions.observation[0].values) {
				if (genericSeries.structure.dimensions.observation[0].values[observationDimensions].id === datasetObservations) {
					genericSeries.dataSets[0].series[datasetSeries].observations[observationDimensions] = genericSeries.dataSets[0].series[datasetSeries].observations[datasetObservations];
					delete genericSeries.dataSets[0].series[datasetSeries].observations[datasetObservations];
				}
			}
		}
		// Swap the SeriesKey string values with the relative values built from the information in the message. From ID based to structure iteration based, eg "124" -> 0, where 124 is the code value for Canada and 0 is the position of 124 within the codelist for the GEO dimension.
		for (var datasetDimensions = 0; datasetDimensions < seriesNameArrayLength; datasetDimensions++) {
			for (var seriesDimensions in genericSeries.structure.dimensions.series) {
				for (var dimensionsCodelistCt in genericSeries.structure.dimensions.series[seriesDimensions].values) {
					if (genericSeries.structure.dimensions.series[seriesDimensions].values[dimensionsCodelistCt].id === seriesNameArray[datasetDimensions]) {
						seriesNameArray[datasetDimensions] = dimensionsCodelistCt;
					}
				}
			}
		}
		DatasetSeries = seriesNameArray.join(":");
		genericSeries.dataSets[0].series[DatasetSeries] = genericSeries.dataSets[0].series[datasetSeries];
		delete genericSeries.dataSets[0].series[datasetSeries];
	}
	var JSON_genericSeries = JSON.stringify(genericSeries);
	if (!callback) {
		return JSON_genericSeries;
	} else {
		callback(JSON_genericSeries);
	}
}

function parseDSD(dsdData, callback) {
	if (!dsdData) {
		console.log('null stream');
		return false;
	}
	var datastructures = dsdData.getElementsByTagNameNS(sdmxNS.str, 'DataStructure');
	var DSD = [];
	for (var datastructureCounter = 0; datastructureCounter < datastructures.length; datastructureCounter++) {
		var dimensionList = datastructures[datastructureCounter].getElementsByTagNameNS(sdmxNS.str, 'DimensionList');
		var attributeList = datastructures[datastructureCounter].getElementsByTagNameNS(sdmxNS.str, 'AttributeList');
		var structureInfo = {};
		// The NSI implementation uses a "newline" text node between each other nodes.
		var datastructureDetails = datastructures[datastructureCounter].childNodes;
		for (var datastructureDetailsCounter = 0; datastructureDetailsCounter < datastructureDetails.length; datastructureDetailsCounter++) {
			if (datastructureDetails[datastructureDetailsCounter].nodeName === "Name") {
				structureInfo.name = datastructureDetails[datastructureDetailsCounter].childNodes[0].nodeValue;
			} else {
				if (datastructureDetails[datastructureDetailsCounter].nodeName === "Description") {
					structureInfo.description = datastructureDetails[datastructureDetailsCounter].childNodes[0].nodeValue;
				}
			}
		}
		structureInfo.rel = datastructures[datastructureCounter].nodeName.toLowerCase();
		structureInfo.id = datastructures[datastructureCounter].getAttribute("id");
		structureInfo.agencyID = datastructures[datastructureCounter].getAttribute("agencyID");
		structureInfo.version = datastructures[datastructureCounter].getAttribute("version");
		structure = {};
		structure.name = structureInfo.name;
		structure.description = structureInfo.description;
		structure.links = [];
		var datastructureLink = {};
		datastructureLink.rel = structureInfo.rel;
		datastructureLink.title = structureInfo.description;
		datastructureLink.type = "text/xml";
		datastructureLink.href = document.location.protocol + "//" + document.location.hostname + document.location.port + "/rest/" + structureInfo.rel + "/" + structureInfo.agencyID + "/" + structureInfo.id + "/" + structureInfo.version;
		// Loop through the dimensions nodes and populate the series array of the dimensions object with information. NB: have to decide how dimensions are categorized according to level (dataset vs series vs observation) because sdmx-ml does not define levels of association for dimensions, only attributes.
		structure.dimensions = {};
		structure.dimensions.series = [];
		// Use createDSDComponents() to return a json object with the dimensions and the respective codelists. Returns the links for the codelists and concept schemes as well.
		var dimensionStructure = createDSDComponents(dsdData, dimensionList);
		structure.dimensions.series = dimensionStructure.dimensionSeriesLevel;
		structure.attributes = {};
		// Use createDSDComponents() to return a json object with the attributes and the respective codelists. Returns the links for the codelists and concept schemes as well.
		var attributeStructure = createDSDComponents(dsdData, attributeList);
		// Assign the attributes to the observation array of the structure object. Due to the lack of a attachment level tunnable, it's difficult to automate the attachment level.
		structure.attributes.observation = attributeStructure.dimensionSeriesLevel;
		// Combine both link arrays from dimensions and attributes and push into the links array of the structure object.
		var combinedArray = dimensionStructure.links;
		combinedArray.concat(attributeStructure.links);
		// Attach the datastructure link to the combined array.
		combinedArray.unshift(datastructureLink);
		structure.links = combinedArray;
		structure.annotations = [];
		DSD.push(structure);
	}
	var stringified_DSD = JSON.stringify(DSD);
	if (!callback) {
		return stringified_DSD;
	} else {
		callback(stringified_DSD);
	}
}
// Takes the DimensionList or AttributeList xml node and returns a json object with two arrays. One with the links for the codelists and conceptschemes of the dimensions/attributes. The other with the dimensions/attributes and the codelist values for each dimension/attribute.
function createDSDComponents(xmlNodesBuffer, DSD_component_list) {
	var DSD = {};
	DSD.dimensionSeriesLevel = [];
	DSD.links = [];
	for (var dimensionListCounter = 0; dimensionListCounter < DSD_component_list.length; dimensionListCounter++) {
		dimensionSet = removeTextNodes(DSD_component_list[dimensionListCounter].childNodes);
		for (var dimensionCounter = 0; dimensionCounter < dimensionSet.length; dimensionCounter++) {
			var dimensionObject = {};
			dimensionObject.id = dimensionSet[dimensionCounter].getAttribute('id');
			dimensionObject.name = dimensionSet[dimensionCounter].getAttribute('name');
			dimensionObject.description = dimensionSet[dimensionCounter].getAttribute('description');
			dimensionObject.role = dimensionObject.id;
			dimensionObject._default = null;
			if (DSD_component_list[dimensionListCounter].nodeName === "DimensionList") {
				dimensionObject.keyPosition = dimensionSet[dimensionCounter].getAttribute('position');
			} else {
				dimensionObject.assignmentStatus = dimensionSet[dimensionCounter].getAttribute('assignmentStatus');
			}
			var localRepresentation = removeTextNodes(dimensionSet[dimensionCounter].childNodes);
			for (var localRepresentationCounter = 0; localRepresentationCounter < localRepresentation.length; localRepresentationCounter++) {
				var ref, refCounter;
				if (localRepresentation[localRepresentationCounter].nodeName === 'LocalRepresentation') {
					var enumeration = removeTextNodes(localRepresentation[localRepresentationCounter].childNodes);
					for (var enumerationCounter = 0; enumerationCounter < enumeration.length; enumerationCounter++) {
						ref = removeTextNodes(enumeration[enumerationCounter].childNodes);
						for (refCounter = 0; refCounter < ref.length; refCounter++) {

							// Populate the dimensionSeriesLevel array with information regarding codelists and concept schemes.
							// Use the createLinks() to return the JSON object with the information regarding the codelist of each dimension/attribute and the link object.
							var codelists = createLinks(ref[refCounter], ref[refCounter].getAttribute('class'));
							dimensionObject.values = [];
							dimensionObject.values = populateCodelists(xmlNodesBuffer, codelists.codelistObject, "Codelist");
							DSD.dimensionSeriesLevel.push(dimensionObject);
							// Push the link object returned by createLinks() to the link array of the DSD object.
							DSD.links.push(codelists.linkObject);
						}
					}
				} else if (localRepresentation[localRepresentationCounter].nodeName === 'ConceptIdentity') {
					ref = removeTextNodes(localRepresentation[localRepresentationCounter].childNodes);
					for (refCounter = 0; refCounter < ref.length; refCounter++) {
						// Populate the dimensionSeriesLevel array with information regarding codelists and concept schemes.
						// Use the createLinks() to return the JSON object with the information regarding the conceptscheme of each dimension/attribute and the link object.
						var conceptSchemes = createLinks(ref[refCounter], ref[refCounter].getAttribute('class'));
						var conceptSchemeInfo = populateCodelists(xmlNodesBuffer, conceptSchemes.codelistObject, "ConceptScheme");
						for (var element = 0; element < conceptSchemeInfo.length; element++) {
							if (conceptSchemeInfo[element].id === dimensionObject.id) {
								dimensionObject.name = conceptSchemeInfo[element].name;
								dimensionObject.description = conceptSchemeInfo[element].description;
							}
						}
						DSD.links.push(conceptSchemes.linkObject);
					}
				}
			}
		}
	}
	return DSD;
}

// Populates the codelists of the Dimensions and/or Attributes.
function populateCodelists(xmlBuffer, codelistName, nodeParameter) {
	// Select all the nodes that are of parameter .rel (ie codelist, conceptscheme, dimension, attribute etc.)
	var codelistsNodes = xmlBuffer.getElementsByTagNameNS(sdmxNS.str, nodeParameter);
	var codelistArray = [];
	for (var nodeCounter = 0; nodeCounter < codelistsNodes.length; nodeCounter++) {
		if (codelistsNodes[nodeCounter].getAttribute('id') === codelistName.id && codelistsNodes[nodeCounter].getAttribute('version') === codelistName.version && codelistsNodes[nodeCounter].getAttribute('agencyID') === codelistName.agencyID) {
			var codelistNodes = codelistsNodes[nodeCounter].childNodes;
			for (var codeCounter = 0; codeCounter < codelistNodes.length; codeCounter++) {
				if (codelistNodes[codeCounter].nodeName === "Code" || codelistNodes[codeCounter].nodeName === "Concept") {
					var codeObject = {};
					var codeNodes = codelistNodes[codeCounter].childNodes;
					for (var codeNodesCounter = 0; codeNodesCounter < codeNodes.length; codeNodesCounter++) {
						codeObject.id = codelistNodes[codeCounter].getAttribute('id');
						if (codeNodes[codeNodesCounter].nodeName === 'Name') {
							codeObject.name = codeNodes[codeNodesCounter].childNodes[0].nodeValue;
						} else {
							if (codeNodes[codeNodesCounter].nodeName === 'Description') {
								codeObject.description = codeNodes[codeNodesCounter].childNodes[0].nodeValue;
							}
						}
					}
					codelistArray.push(codeObject);
				}
			}
		}
	}
	return codelistArray;
}

// Populates the information regarding Senders and Receivers in the Header section of the sdmx message.
function populateSendersReceivers(sendersNode) {
	var senderArray = [];
	sendersNode = removeTextNodes(sendersNode);
	for (var sendersNodeCounter = 0; sendersNodeCounter < sendersNode.length; sendersNodeCounter++) {
		var senderObject = {};
		var senderNode = removeTextNodes(sendersNode[sendersNodeCounter].childNodes);
		senderObject.id = sendersNode[sendersNodeCounter].getAttribute('id');
		for (var senderNodeCounter = 0; senderNodeCounter < senderNode.length; senderNodeCounter++) {
			if (senderNode[senderNodeCounter].nodeName === 'common:Name') {
				senderObject.name = senderNode[senderNodeCounter].childNodes[0].nodeValue;
			} else {
				if (senderNode[senderNodeCounter].nodeName === 'message:Contact') {
					senderObject.contact = [];
					var contactObject = {};
					var contactNode = removeTextNodes(senderNode[senderNodeCounter].childNodes);
					for (var contactNodeCounter = 0; contactNodeCounter < contactNode.length; contactNodeCounter++) {
						switch (contactNode[contactNodeCounter].nodeName) {
							case "common:Name":
								contactObject.name = contactNode[contactNodeCounter].childNodes[0].nodeValue;
								break;
							case "message:Department":
								contactObject.department = contactNode[contactNodeCounter].childNodes[0].nodeValue;
								break;
							case "message:Role":
								contactObject.role = contactNode[contactNodeCounter].childNodes[0].nodeValue;
								break;
							case "message:Email":
								contactObject.email = [];
								var emailsNode = contactNode[contactNodeCounter].childNodes;
								for (var emailsNodeCounter = 0; emailsNodeCounter < emailsNode.length; emailsNodeCounter++) {
									contactObject.email.push(emailsNode[emailsNodeCounter].nodeValue);
								}
								break;
							case "message:Telephone":
								contactObject.telephone = [];
								var telephonesNode = contactNode[contactNodeCounter].childNodes;
								for (var telephonesNodeCounter = 0; telephonesNodeCounter < telephonesNode.length; telephonesNodeCounter++) {
									contactObject.telephone.push(telephonesNode[telephonesNodeCounter].nodeValue);
								}
								break;
							case "message:Fax":
								contactObject.fax = [];
								var faxesNode = contactNode[contactNodeCounter].childNodes;
								for (var faxesNodeCounter = 0; faxesNodeCounter < faxesNode.length; faxesNodeCounter++) {
									contactObject.fax.push(faxesNode[faxesNodeCounter].childNodes[0].nodeValue);
								}
								break;
							case "message:Uri":
								contactObject.uri = [];
								var uriNode = contactNode[contactNodeCounter].childNodes;
								for (var uriNodeCounter = 0; uriNodeCounter < faxesNode.length; uriNodeCounter++) {
									contactObject.uri.push(uriNode[uriNodeCounter].childNodes[0].nodeValue);
								}
								break;
						}
					}
					senderObject.contact.push(contactObject);
				}
			}
		}
		senderArray.push(senderObject);
	}
	return senderArray;
}

//Takes as input the node that contains the information regarding the id, name, agencyID, version and what type of node it is in question (concept scheme, codelist, datastructure) and returns a json object.
function createLinks(refNode, type) {
	var codeLinkObject = {};
	codeLinkObject.codelistObject = {};
	codeLinkObject.codelistObject.id = refNode.getAttribute('id');
	codeLinkObject.codelistObject._package = refNode.getAttribute('package');
	codeLinkObject.codelistObject.agencyID = refNode.getAttribute('agencyID');
	codeLinkObject.codelistObject.version = refNode.getAttribute('version');
	codeLinkObject.codelistObject._class = refNode.getAttribute('class');
	if (type === "Concept") {
		codeLinkObject.codelistObject.id = refNode.getAttribute('maintainableParentID');
		codeLinkObject.codelistObject.version = refNode.getAttribute('maintainableParentVersion');
	}
	codeLinkObject.linkObject = {};
	codeLinkObject.linkObject.title = codeLinkObject.codelistObject.id;
	codeLinkObject.linkObject.type = "text/xml";
	if (type === "datastructure") {
		codeLinkObject.linkObject.rel = type;
	} else {
		codeLinkObject.linkObject.rel = codeLinkObject.codelistObject._class;
	}
	// The document.domain contains the address of the web service. Due to localhosting "localhost" is printed instead of the address.
	codeLinkObject.linkObject.href = document.location.protocol + "//" + "10.0.0.58" + document.location.port + "/rest/" + codeLinkObject.linkObject.rel + "/" + codeLinkObject.codelistObject.agencyID + "/" + codeLinkObject.codelistObject.id + "/" + codeLinkObject.codelistObject.version + "/";
	return codeLinkObject;
}

// Removes the CR text nodes that are present when accessing the childNodes property. Input is of the form: xmlNode.childNodes
function removeTextNodes(xmlNode) {
	var filteredArray = [];
	for (var xmlNodeCnt = 0; xmlNodeCnt < xmlNode.length; xmlNodeCnt++) {
		if (xmlNode[xmlNodeCnt].nodeName !== "#text") {
			filteredArray.push(xmlNode[xmlNodeCnt]);
		}
	}
	return filteredArray;
}

function removeDuplicates(elementArray) {
	var elementArrayLength = elementArray.length;
	filteredArray = [];
	for (var item = 0; item < elementArrayLength; item++) {
		var element = elementArray[item];
		if (filteredArray.indexOf(element) < 0) {
			filteredArray.push(element);
		}
	}
	return filteredArray;
}

function parseCrossSectionalDataSet(seriesData) { // Takes as argument one series node array that contains both the series key nodes and the observation nodes.
	var seriesNode = removeTextNodes(seriesData.childNodes);
	var seriesNodeLength = seriesNode.length;
	var seriesNameArray = []; // The array will contain the information from the SeriesKey which will then be joined into one string and used as the name of the object holding all the series information.
	var seriesName;
	var yearVar;
	var observations = {};
	for (var seriesNodeIt = 0; seriesNodeIt < seriesNodeLength; seriesNodeIt++) {
		if (seriesNode[seriesNodeIt].nodeName === "generic:SeriesKey") {
			var seriesKey = removeTextNodes(seriesNode[seriesNodeIt].childNodes);
			var seriesKeyLength = seriesKey.length;
			for (var seriesKeyIt = 0; seriesKeyIt < seriesKeyLength; seriesKeyIt++) { // If one day attributes will be defined at the series level, this is where a loop would be plugged in to parse them.
				seriesNameArray.push(seriesKey[seriesKeyIt].getAttribute("value"));
			}
			seriesName = seriesNameArray.join(":");
		} else if (seriesNode[seriesNodeIt].nodeName === "generic:Obs") {
			var obsValues = removeTextNodes(seriesNode[seriesNodeIt].childNodes);
			var obsValuesLength = obsValues.length;
			for (var obsValuesIt = 0; obsValuesIt < obsValuesLength; obsValuesIt++) {
				if (obsValues[obsValuesIt].nodeName === "generic:ObsDimension") {
					// After testing, should experiment with replacing with the iterator value to align with the sdmx standard. Put a conditional that checks for the ordinator number in the value array of the dimension structure.
					yearVar = obsValues[obsValuesIt].getAttribute("value");
					observations[yearVar] = [];
				} else if (obsValues[obsValuesIt].nodeName === "generic:ObsValue") {
					observations[yearVar] = [];
					observations[yearVar].push(obsValues[obsValuesIt].getAttribute("value"));
				} else if (obsValues[obsValuesIt].nodeName === "generic:Attributes") {
					var obsAttributes = removeTextNodes(obsValues[obsValuesIt].childNodes);
					var obsAttributesLength = obsAttributes.length;
					for (var obsAttributesIt = 0; obsAttributesIt < obsAttributesLength; obsAttributesIt++) {
						// Put in a conditional to compare with the values in the attributes
						// section of the structure object and replace with the ordinator
						// (iteration) value of the value array of the respective attribute?
						observations[yearVar].push(obsAttributes[obsAttributesIt].getAttribute("value")); 
					}
				}
			}
		}
		series = {};
		series[seriesName] = {};
		series[seriesName].attributes = [];
		series[seriesName].observations = observations;
	}
	return series;
}

// Make a filter so that the user can select what nodes to output, be it attributes, dimensions or time dimension.
function parseCrossSectionalDimensions(seriesData, filter) {
	var seriesArray = [];
	var seriesDataLength = seriesData.length;
	var dimensions = {};
	for (var seriesDataCt = 0; seriesDataCt < seriesDataLength; seriesDataCt++) {
		var seriesNode = removeTextNodes(seriesData[seriesDataCt].childNodes);
		var seriesNodeLength = seriesNode.length;
		for (var seriesNodeCt = 0; seriesNodeCt < seriesNodeLength; seriesNodeCt++) {
			if (filter === "attributes" && seriesNode[seriesNodeCt].nodeName !== "generic:ObsValue") {
				var attributeNodes = removeTextNodes(seriesNode[seriesNodeCt].childNodes);
				var attributeNodesLength = attributeNodes.length;
				for (var attributeNodesCt = 0; attributeNodesCt < attributeNodesLength; attributeNodesCt++) {
					if (seriesDataCt === 0) {
						dimensions[attributeNodes[attributeNodesCt].getAttribute("id")] = {};
						dimensions[attributeNodes[attributeNodesCt].getAttribute("id")].id = attributeNodes[attributeNodesCt].getAttribute("id");
						dimensions[attributeNodes[attributeNodesCt].getAttribute("id")].values = [];
						dimensions[attributeNodes[attributeNodesCt].getAttribute("id")].values.push(attributeNodes[attributeNodesCt].getAttribute("value"));
					} else {
						dimensions[attributeNodes[attributeNodesCt].getAttribute("id")].values.push(attributeNodes[attributeNodesCt].getAttribute("value"));
					}
				}
			} else if (filter === "dimensions" && seriesNode[seriesNodeCt].nodeName !== "generic:ObsValue") {
				if (seriesDataCt === 0) {
					dimensions[seriesNode[seriesNodeCt].getAttribute("id")] = {};
					dimensions[seriesNode[seriesNodeCt].getAttribute("id")].id = seriesNode[seriesNodeCt].getAttribute("id");
					dimensions[seriesNode[seriesNodeCt].getAttribute("id")].values = [];
					dimensions[seriesNode[seriesNodeCt].getAttribute("id")].values.push(seriesNode[seriesNodeCt].getAttribute("value"));
				} else {
					dimensions[seriesNode[seriesNodeCt].getAttribute("id")].values.push(seriesNode[seriesNodeCt].getAttribute("value"));
				}
			}
		}
	}
	// Remove the duplicate entries in the values array and then push the contents into an object that contains all the information regarding the codelist (in a dataset the only information supplied is the id of the codelist, by pulling the DSD separately the rest of the information can be supplied.) Then the array is pushed into each "seriesNode[seriesNodeCt].getAttribute("id")" object within the dimensions or attributes object into the seriesArray.
	for (var elements in dimensions) {
		dimensions[elements].values = removeDuplicates(dimensions[elements].values);
		//console.log(dimensions[elements]);
		for (var value in dimensions[elements].values) {
			var valueObject = {};
			valueObject.id = dimensions[elements].values[value];
			dimensions[elements].values[value] = valueObject;
		}
		seriesArray.push(dimensions[elements]);
	}
	return seriesArray;
}

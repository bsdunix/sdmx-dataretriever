// Create an xml implementation before moving to json.
function createBalanceObject(xmlEnergyBalancesSpreadsheet, callback) {
	var rows = xmlEnergyBalancesSpreadsheet.getElementsByTagName("Row");
	var rowsLength = rows.length;
	var balancesArray = [];
	for (var rowsCt = 0; rowsCt < rowsLength; rowsCt++) {
		var cell = rows[rowsCt].children;
		var cellLength = cell.length;
		balancesArray[rowsCt] = [];
		for (var cellCt = 0; cellCt < cellLength; cellCt++) {
			var data = cell[cellCt].children;
			var dataLength = data.length;
			balancesArray[rowsCt][cellCt] = [];
			for (var dataCt = 0; dataCt < dataLength; dataCt++) {
				var stringNode = data[dataCt].childNodes;
				var stringNodeLength = stringNode.length;
				for (var stringNodeCt = 0; stringNodeCt < stringNodeLength; stringNodeCt++) {
					balancesArray[rowsCt][cellCt] = stringNode[stringNodeCt].nodeValue;
				}
			}
		}
	}
	//Create an object _restVariablesForQuery_ that will take all the commodity values into one array and flow values in another. Then concatenate all the strings in their respective arrays into one megastring that represents a rest query to SDMX. Most likely for technical reasons SDMX does not allow to query long strings with the OR operator. Just query all available values for flows and commodities. Will remove this and instead create a meta value that includes all values necessary for the energy balances sheet.
	var restVariablesForQuery = {};
	restVariablesForQuery.commodities = [];
	restVariablesForQuery.flows = [];
	var filteredObject = {};
	for (var row = 3; row < balancesArray.length; row++) {
		filteredObject[balancesArray[row][0]] = {};
		filteredObject[balancesArray[row][0]].id = balancesArray[row][1];
		filteredObject[balancesArray[row][0]].name = balancesArray[row][2];
		filteredObject[balancesArray[row][0]].columns = {};
		for (var column = 3; column < balancesArray[row].length; column++) {
			filteredObject[balancesArray[row][0]].columns[balancesArray[1][column]] = {};
			filteredObject[balancesArray[row][0]].columns[balancesArray[1][column]] = balancesArray[row][column];
			for (var string in filteredObject[balancesArray[row][0]].columns) {
				if (typeof filteredObject[balancesArray[row][0]].columns[string] !== "object" && filteredObject[balancesArray[row][0]].columns[string] !== "undefined") {
					//console.log(filteredObject[balancesArray[row][0]].columns[string]);
						var splitStringsArray = filteredObject[balancesArray[row][0]].columns[string].split(" ");
						//console.log(splitStringsArray);
						filteredObject[balancesArray[row][0]].columns[string] = {};	
						for (var commodity in splitStringsArray) {
							//Filter the string to get the commodity code and the flow codes separately.
							//console.log(splitStringsArray[commodity]);
							filteredObject[balancesArray[row][0]].columns[string].operation = splitStringsArray[commodity].substring(0, 1);
							filteredObject[balancesArray[row][0]].columns[string].commodity = splitStringsArray[commodity].substring(1, 3);
							filteredObject[balancesArray[row][0]].columns[string].flow = splitStringsArray[commodity].substring(3);
							//console.log(filteredObject[balancesArray[row][0]].columns[string].flow);
						if (string !== "Total Energy" && filteredObject[balancesArray[row][0]].name !== "Total energy supply") {
								restVariablesForQuery.commodities.push(splitStringsArray[commodity].substring(1, 3));
								//console.log(commodity);
								//console.log(splitStringsArray[commodity]);
								if (splitStringsArray[commodity].substring(3) !== "." && splitStringsArray[commodity].substring(3) !== "" && splitStringsArray[commodity].substring(3) !== null)
								restVariablesForQuery.flows.push(splitStringsArray[commodity].substring(3));
						}
					}
				}
			}
		}
	}
	restVariablesForQuery.commodities = removeDuplicates(restVariablesForQuery.commodities);
	restVariablesForQuery.flows = removeDuplicates(restVariablesForQuery.flows);
	restVariablesForQuery.commodities = restVariablesForQuery.commodities.join("+");
	restVariablesForQuery.flows = restVariablesForQuery.flows.join("+");
	filteredObject.restVariablesForQuery = restVariablesForQuery;
	balancesArray = JSON.stringify(filteredObject, null, 4);
	if (!callback) {
		return balancesArray;
	} else {
		callback(balancesArray);
	}
}

//Create an html table using the object returned by the createBalanceObject() method.
function createBalanceSheet(balanceSheetObject, callback) {
	var balanceSheetTable = document.createElement("table");
	//Adding a visible border to the table. For some reason it won't change the layout. Not through javascript, not through CSS.
	balanceSheetTable.style.border = "1px";
	//Name of the table is based on the header information in the xml file. (The first cell that contains the name of the table.)
	balanceSheetTable.setAttribute("id", Object.keys(balanceSheetObject)[29]);
	//Create the header rows and columns of the table.
	for (var rows in balanceSheetObject) {
		//Create row headers which are then pushed to the row
		var row = document.createElement("tr");
		var rowHeader = document.createElement("th");
		rowHeader.appendChild(document.createTextNode(balanceSheetObject[rows].name));
		rowHeader.setAttribute("class", balanceSheetObject[rows].id);
		row.appendChild(rowHeader);
		for (var columns in balanceSheetObject[rows].columns) {
			var column = document.createElement('td');
			var columnHeader = document.createElement('th');
			column.appendChild(document.createTextNode(balanceSheetObject[rows].columns[columns]));
			columnHeader.appendChild(document.createTextNode(columns));
			columnHeader.setAttribute("class", columns);
			row.appendChild(columnHeader);
			row.appendChild(column);
		}
		balanceSheetTable.appendChild(row);
	}
	if (!callback) {	
		return balanceSheetTable;
	} else {
		callback(balanceSheetTable);
	}
}

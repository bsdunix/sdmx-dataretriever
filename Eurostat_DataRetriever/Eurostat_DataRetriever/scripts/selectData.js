function selectData(element, attribute) {
	if (element.getAttribute(attribute) === 'selected') {
		element.setAttribute(attribute, 'deselected');
	} else {
		element.setAttribute(attribute, 'selected');
	}
}

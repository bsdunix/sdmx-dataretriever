﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Eurostat_DataRetriever
{
    public partial class WebIndex : System.Web.UI.Page, IHttpHandler
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        private void SetRequestParameters(HttpContext URL)
        {
            HttpRequest URLRequest = URL.Request;
            HttpResponse URLResponse = URL.Response;
            //if (URLRequest.Url.AbsolutePath.StartsWith("/"))
            XmlDocument EBObject = new XmlDocument();
            EBObject.Load("c:\\Users\\sdmx\\Documents\\Visual Studio 2015\\Projects\\Eurostat DataRetriever\\Eurostat_DataRetriever\\Eurostat_DataRetriever\\scripts\\UN_EB_Matrix(simple_format)_no_space.xml");
            XmlElement root = EBObject.DocumentElement;
            Console.WriteLine(EBObject.NameTable);
            XmlNamespaceManager Namespaces = new XmlNamespaceManager(EBObject.NameTable);
            XmlNodeList rows = EBObject.SelectNodes("Data");
            foreach (XmlNode cell in rows)
            {
                Console.Write(cell);
                URLResponse.Write(cell.ChildNodes[0].InnerText);
                URLResponse.Output.Write(cell.FirstChild);
                Console.WriteLine(cell.Name);
            }
            string DocURI = EBObject.BaseURI;
            URLResponse.Output.Write(DocURI);
            Console.WriteLine(DocURI);
            Console.WriteLine(URLRequest);
        }
    }
}
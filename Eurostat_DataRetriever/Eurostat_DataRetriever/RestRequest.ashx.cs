﻿using Estat.Nsi.DataRetriever;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Rest;
using Org.Sdmxsource.Sdmx.Api.Model.Format;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Query;
using Org.Sdmxsource.Sdmx.DataParser.Factory;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.DataParser.Rest;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Builder;
using Org.Sdmxsource.Sdmx.Structureparser.Manager;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Rest;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using System;
using System.Configuration;
using System.Web;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Collections.Generic;

namespace Eurostat_DataRetriever
{
    /// <summary>
    /// Summary description for RestRequest
    /// </summary>
    public class RestRequest : IHttpHandler 
    {
        public class SDMXPOSTData
        {
            public ISet<string> FREQ { get; set; }
            public ISet<string> GEO { get; set; }
            public ISet<string> PROD { get; set; }
            public ISet<string> FLOW { get; set; }
            public ISet<string> UNIT { get; set; }
            public IDictionary<string, string> queryParams { get; set; }
        }

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            ConnectionStringSettings connectionStrings = new ConnectionStringSettings("MADB", "server=localhost;user id=iaea;password=pess25;database=madb", "MySql.Data.MySqlClient");
            // Workaround: replace all "-" by ".". Otherwise URL will not work
            string absUrl = request.Url.PathAndQuery.Replace('-', '.');
            response.ContentType = "text/xml";
            IStructureQueryBuilder<string> structureQueryBuilder = new StructureQueryBuilderRest();
            IStructureParsingManager spm = new StructureParsingManager();
            RESTSdmxObjectRetrievalManager retrievalManager = new RESTSdmxObjectRetrievalManager("http://localhost:5000/rest", structureQueryBuilder, spm, null);
            IHeader dataRequestHeader = new HeaderImpl("IRES", "Cristian Pogolsha");

            if (absUrl.StartsWith("/RestRequest.ashx/data/"))
            {
                string restUrl = absUrl.Substring(absUrl.IndexOf("/data"));
                IRestDataQuery query = new RESTDataQueryCore(restUrl);
                DataType dataType = GetDataFormat(request);
                ISdmxDataRetrievalWithWriter sqlQuery = new DataRetrieverCore(dataRequestHeader, connectionStrings);
                // Direct use of the DataRetrieverCore object.
                //IDataQuery dataQuery = new DataQueryImpl(query, retrievalManager);
                //sqlQuery.GetData(dataQuery, new CompactDataWriterEngine(response.OutputStream, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne)));
                // DataWriterManager instantiation
                SdmxDataFormatCore dataFormat = new SdmxDataFormatCore(dataType);
                DataWriterFactory dwf = new DataWriterFactory();
                dwf.GetDataWriterEngine(dataFormat, response.OutputStream);
                IDataWriterManager writerManager = new DataWriterManager(dwf);
                // ---
                // Indirect use of the DataRetrieverCore via RestDataQueryManager
                IRestDataQueryManager dataQueryManager = new RestDataQueryManager(sqlQuery, writerManager, retrievalManager);
                // Call for Data Query
                dataQueryManager.ExecuteQuery(query, dataFormat, response.OutputStream);
            }
            else if (absUrl.StartsWith("/RestRequest.ashx/sdmxPOST"))
            {
                foreach (string type in request.AcceptTypes)
                {
                    Debug.WriteLine(type);
                }
                string PostRequest;
                using (StreamReader POSTreader = new StreamReader(request.InputStream))
                {
                    PostRequest = POSTreader.ReadToEnd();
                }
                try
                {
                    KeyValuePair<IList<ISet<string>>, IDictionary<string, string>> DataQueryKV = PopulateDataQueryObject(PostRequest);
                    IRestDataQuery query = new RESTDataQueryCore(DataQueryKV.Key, DataQueryKV.Value);
                    DataType dataType = GetDataFormat(request);
                    ISdmxDataRetrievalWithWriter sqlQuery = new DataRetrieverCore(dataRequestHeader, connectionStrings);
                    // Direct use of the DataRetrieverCore object.
                    //IDataQuery dataQuery = new DataQueryImpl(query, retrievalManager);
                    //sqlQuery.GetData(dataQuery, new CompactDataWriterEngine(response.OutputStream, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne)));
                    // DataWriterManager instantiation
                    SdmxDataFormatCore dataFormat = new SdmxDataFormatCore(dataType);
                    DataWriterFactory dwf = new DataWriterFactory();
                    dwf.GetDataWriterEngine(dataFormat, response.OutputStream);
                    IDataWriterManager writerManager = new DataWriterManager(dwf);
                    // ---
                    // Indirect use of the DataRetrieverCore via RestDataQueryManager
                    IRestDataQueryManager dataQueryManager = new RestDataQueryManager(sqlQuery, writerManager, retrievalManager);
                    // Call for Data Query
                    dataQueryManager.ExecuteQuery(query, dataFormat, response.OutputStream);
                    StreamReader OutputStream = new StreamReader(response.OutputStream);
                    string OutputString = OutputStream.ReadToEnd();
                    //File.AppendAllText(OutputFile, OutputString);
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private KeyValuePair<IList<ISet<string>>, IDictionary<string, string>> PopulateDataQueryObject(string SDMXPOSTData)
        {
            SDMXPOSTData ParsedJSON = JsonConvert.DeserializeObject<SDMXPOSTData>(SDMXPOSTData);
            IList<ISet<string>> sdmxDBQuery = new List<ISet<string>>();
            sdmxDBQuery.Add(ParsedJSON.FREQ);
            sdmxDBQuery.Add(ParsedJSON.GEO);
            sdmxDBQuery.Add(ParsedJSON.PROD);
            sdmxDBQuery.Add(ParsedJSON.FLOW);
            sdmxDBQuery.Add(ParsedJSON.UNIT);
            IDictionary<string, string> queryParameters = ParsedJSON.queryParams;
            // Test for key/values in the queryParameters.
            try
            {
                foreach (KeyValuePair<string, string> param in ParsedJSON.queryParams)
                {
                    Debug.WriteLine("Key: " + param.Key + "\nValue: " + param.Value);
                }
            } catch (Newtonsoft.Json.JsonException JSONError)
            {
                Debug.WriteLine(JSONError.Message);
            }
            return new KeyValuePair<IList<ISet<string>>, IDictionary<string, string>>(sdmxDBQuery, queryParameters);
        }

        private DataType GetDataFormat(HttpRequest request)
        {
            string[] accept = request.AcceptTypes;

            for (int i = 0; i < accept.Length; i++)
            {
                if (accept[i].Contains("application/vnd.sdmx.genericdata+xml"))
                {
                    return DataType.GetFromEnum(DataEnumType.Generic21);
                }
                else if (accept[i].Contains("application/vnd.sdmx.structurespecificdata+xml"))
                {
                    return DataType.GetFromEnum(DataEnumType.Compact21);
                }
                else if (accept[i].Contains("application/xml"))
                {
                    return DataType.GetFromEnum(DataEnumType.Generic21);
                }
            }
            return DataType.GetFromEnum(DataEnumType.Generic21);
        }
    }
}
﻿using Estat.Nsi.DataRetriever;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Rest;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.Api.Model.Format;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Query;
using Org.Sdmxsource.Sdmx.DataParser.Engine;
using Org.Sdmxsource.Sdmx.DataParser.Factory;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.DataParser.Rest;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Builder;
using Org.Sdmxsource.Sdmx.Structureparser.Manager;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Rest;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using Org.Sdmxsource.Util;
using Newtonsoft.Json;

namespace Eurostat_DataRetriever
{
    // Create the SDMX data object and deserialize the json to this object. Then send the object to the RESTDataQueryCore.
    /// <summary>
    /// Summary description for RestService
    /// </summary>
    public class RestService : IHttpHandler
    {
        private class SDMXPOSTData
        {
            string[] dataQuery;
            string[] queryParamaters;
            string[] DataQuery
            {
                get
                {
                    return dataQuery;
                }
                set
                {
                    dataQuery = value;
                }
            }
            string[] QueryParameters
            {
                get
                {
                    return queryParamaters;
                }
                set
                {
                    queryParamaters = value;
                }
            }
            public SDMXPOSTData(string[] Dimensions, string[] Parameters)
                {
                    DataQuery = Dimensions;
                    QueryParameters = Parameters;
                }

           public void ProcessPOSTRequest(string JSONObject)
            {
                object parsedJSON = JsonConvert.DeserializeObject(JSONObject);
                foreach (object dimension in JSONObject)
                {
                    Debug.WriteLine(dimension);
                }
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            Debug.WriteLine(request.ContentLength);
            Debug.WriteLine(request.InputStream);
            Debug.WriteLine(request.ContentType);
            Debug.WriteLine(request.FilePath);
            ConnectionStringSettings connectionStrings = new ConnectionStringSettings("MADB", "server=localhost;user id=iaea;password=pess25;database=madb", "MySql.Data.MySqlClient");
            // Workaround: replace all "-" by ".". Otherwise URL will not work
            string absUrl = request.Url.PathAndQuery.Replace('-', '.');
            response.ContentType = "text/xml";
            IStructureQueryBuilder<string> structureQueryBuilder = new StructureQueryBuilderRest();
            IStructureParsingManager spm = new StructureParsingManager();
            RESTSdmxObjectRetrievalManager retrievalManager = new RESTSdmxObjectRetrievalManager("http://localhost:5000/rest", structureQueryBuilder, spm, null);

            if (absUrl.StartsWith("/RestService.ashx/data/"))
            {
                string restUrl = absUrl.Substring(absUrl.IndexOf("/data"));
                IHeader dataRequestHeader = new HeaderImpl("IRES", "Cristian Pogolsha");
                IRestDataQuery query = new RESTDataQueryCore(restUrl);
                DataType dataType = GetDataFormat(request);
                ISdmxDataRetrievalWithWriter sqlQuery = new DataRetrieverCore(dataRequestHeader, connectionStrings);
                // Direct use of the DataRetrieverCore object.
                //IDataQuery dataQuery = new DataQueryImpl(query, retrievalManager);
                //sqlQuery.GetData(dataQuery, new CompactDataWriterEngine(response.OutputStream, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne)));
                // DataWriterManager instantiation
                SdmxDataFormatCore dataFormat = new SdmxDataFormatCore(dataType);
                DataWriterFactory dwf = new DataWriterFactory();
                dwf.GetDataWriterEngine(dataFormat, response.OutputStream);
                IDataWriterManager writerManager = new DataWriterManager(dwf);
                // ---
                // Indirect use of the DataRetrieverCore via RestDataQueryManager
                IRestDataQueryManager dataQueryManager = new RestDataQueryManager(sqlQuery, writerManager, retrievalManager);
                // Call for Data Query
                dataQueryManager.ExecuteQuery(query, dataFormat, response.OutputStream);
            }
            else if (absUrl.StartsWith("/RestService.ashx/datastructure") || absUrl.StartsWith("/RestService.ashx/dataflow") || absUrl.StartsWith("/RestService.ashx/codelist"))
            {
                string restUrl = absUrl.Substring("/RestService.ashx".Length);
                IRestStructureQuery query = new RESTStructureQueryCore(restUrl);
                IStructureFormat structureFormat = new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument));
                StructureWriterManager structureWritingManager = new StructureWriterManager();
                IRestStructureQueryManager structureQueryManager = new RestStructureQueryManager(structureWritingManager, retrievalManager);
                structureQueryManager.GetStructures(query, response.OutputStream, structureFormat);
            }
            else if (absUrl.StartsWith("/RestService.ashx/energybalances"))
            {
                XmlDocument EBObject = new XmlDocument();
                // Have to refactor to more environment independent path
                string filename = @"C:\inetpub\wwwroot\sdmx_EnerBal\scripts\UNSD_concept_dos.xml";
                using (FileStream xmlFile = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    /* // Have to properly set-up a BOM filtration subroutine
                    byte[] bom = new byte[3];
                    xmlFile.Read(bom, 0, bom.Length);
                    string ByteOMark = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                    string ByteOMarkMark = Encoding.UTF8.GetString(bom);
                    Debug.WriteLine(ByteOMarkMark);
                    Debug.WriteLine(ByteOMark);
                    Debug.WriteLine("Byte order mark: {0} {1} {2}", bom[0], bom[1], bom[2]);
                    if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76)
                    {
                        ByteOMark = (string)Encoding.UTF7;
                    }
                    else if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf)
                    {
                        ByteOMark = Encoding.UTF8;
                    }
                    else if (bom[0] == 0xff && bom[1] == 0xfe)
                    {
                        ByteOMark = Encoding.Unicode;
                    }
                    // Have to properly format the xml file. Need to add the xml tags, enconding etc. etc.
                    if (xmlDoc.StartsWith(ByteOMark))
                    {
                        xmlDoc = xmlDoc.Remove(0, ByteOMarkMark.Length - 1);
                    }
                    */
                    string xmlDoc = File.ReadAllText(filename);
                }
                XDocument xdocument = XDocument.Load(filename);
                XElement EnergyBalancesRoot = xdocument.Root;
                // Should simplify this.
                IEnumerable<XElement> LinqObject = xdocument.Root.Elements().Elements();

                IEnumerable<XElement> DataCells = from primaryproduction in LinqObject.Elements("Data") select primaryproduction;
                IList<KeyValuePair<string, ICollection<string>>> finalQuery = new List<KeyValuePair<string, ICollection<string>>>();
                // -- Create the query variables and parameters that get passed to IRestDataQuery.
                IList<ISet<string>> dataQuery = new List<ISet<string>>();
                ISet<string> freq = new HashSet<string>();
                freq.Add("A");
                ISet<string> geo = new HashSet<string>();
                geo.Add("124");
                ISet<string> nrgunit = new HashSet<string>();
                nrgunit.Add("WSR");
                nrgunit.Add("HSO");
                ISet<string> nrgprod = new HashSet<string>();
                ISet<string> nrgflow = new HashSet<string>();
                finalQuery = ExtractDataStrings(DataCells);
                // Extracted from the xml file. 
                foreach (KeyValuePair<string, ICollection<string>> queryString in finalQuery)
                {
                    nrgprod.Add(queryString.Value.ElementAt(0));
                    nrgflow.Add(queryString.Value.ElementAt(1));
                }
                dataQuery.Add(freq);
                dataQuery.Add(geo);
                dataQuery.Add(nrgprod);
                dataQuery.Add(nrgflow);
                dataQuery.Add(nrgunit);
                IDictionary<string, string> queryParameters = new Dictionary<string, string>();
                queryParameters.Add("startPeriod", "1999");
                queryParameters.Add("endPeriod", "1999");
                //
                // -- Code for calling the database and receiving a generic sdmx data file
                IHeader dataRequestHeader = new HeaderImpl("IRES", "Cristian Pogolsha");
                IRestDataQuery newDataQuery = new RESTDataQueryCore(dataQuery, queryParameters);
                DataType dataType = GetDataFormat(request);
                ISdmxDataRetrievalWithWriter sqlQuery = new DataRetrieverCore(dataRequestHeader, connectionStrings);
                SdmxDataFormatCore dataFormat = new SdmxDataFormatCore(dataType);
                DataWriterFactory dwf = new DataWriterFactory();
                dwf.GetDataWriterEngine(dataFormat, response.OutputStream);
                IDataWriterManager writerManager = new DataWriterManager(dwf);
                IRestDataQueryManager dataQueryManager = new RestDataQueryManager(sqlQuery, writerManager, retrievalManager);
                Stream sdmx_output = new MemoryStream();
                    //-- Call for Data Query
                    dataQueryManager.ExecuteQuery(newDataQuery, dataFormat, response.OutputStream);
                    //--
                //--
                /*
                sdmx_output.Position = 0;
                StreamReader sdmx_file = new StreamReader(sdmx_output);
                string sdmx_response_string = sdmx_file.ReadToEnd();
                IEnumerable<XElement> NormalizedSDMXResponseString = NormalizeGeneric(sdmx_response_string);

                ///Most likely this will change after I use the standard spreadsheet.
                IEnumerable<XElement> SomethingNew = xdocument.Root
                                                              .Elements()
                                                              .Select(flownode => new XElement(flownode.Name, new object[]
                                                              {
                                                                  flownode.Elements().Select(productnode => new XElement(productnode.Name, new object[]
                                                                  {
                                                                      productnode.Elements().Select(datanode => new XElement(datanode.Name, ExtractDataFromString(datanode.Value, NormalizedSDMXResponseString)))
                                                                  }))
                                                              }));
                //Response xml containing the swappend variables for values from DB.
                response.Write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                response.Write("<Worksheet>");
                foreach (XElement element in SomethingNew)
                {
                    response.Write(element);
                }
                response.Write("</Worksheet>");
                */
            }
            else
            {
                string ErrorResponse = "Request is not of data/dataflow/datastructure/codelist type!";
                response.Write("<error>" + ErrorResponse + "</error>");
            }
            response.End();
        }
        // Check if it's possible to combine ExtractData() and DoArithmetic() into one method? I think i just did that with the ExtractDataFromString()
            public object DoArithmetic(IEnumerable<XElement> SDMXCompactDataReturnQuery, IList<KeyValuePair<string, ICollection<string>>> ProdFlowList)
            {
                float FinalResult = 0;
                foreach (KeyValuePair<string, ICollection<string>> element in ProdFlowList)
                {
                    // Run through the array containing the list of values to query the database.
                    if (element.Key.StartsWith(@"+"))
                    {
                        FinalResult += ToFloat(SDMXDimensionFilter(SDMXCompactDataReturnQuery, element.Value.ElementAt(0), element.Value.ElementAt(1)));
                    }
                    else if (element.Key.StartsWith(@"-"))
                    {
                        FinalResult -= ToFloat(SDMXDimensionFilter(SDMXCompactDataReturnQuery, element.Value.ElementAt(0), element.Value.ElementAt(1)));
                    }
                    else if (element.Key.StartsWith(@"*"))
                    {
                        FinalResult *= ToFloat(SDMXDimensionFilter(SDMXCompactDataReturnQuery, element.Value.ElementAt(0), element.Value.ElementAt(1)));
                    }
                    else if (element.Key.StartsWith(@"/"))
                    {
                        // If the array contains only one value and there is no addition or subtraction, should return n.a.
                        if (ProdFlowList.Count > 4)
                        {
                                FinalResult /= ToFloat(SDMXDimensionFilter(SDMXCompactDataReturnQuery, element.Value.ElementAt(0), element.Value.ElementAt(1)));
                        }
                        else
                        {
                            // This should be returned if a method is defined.
                            return "N/A";
                        }
                    }
                }
                    return FinalResult;
            }



            public object DoArithmeticKV(IEnumerable<XElement> SDMXCompactDataReturnQuery, IList<KeyValuePair<string, ICollection<string>>> ProdFlowList)
            {
                float FinalResult = 0;
                foreach (KeyValuePair<string, ICollection<string>> element in ProdFlowList)
                {
                    KeyValuePair<string, float> ReturnData = new KeyValuePair<string, float>(element.Key, ToFloat(SDMXDimensionFilter(SDMXCompactDataReturnQuery, element.Value.ElementAt(0), element.Value.ElementAt(1))));
                    MakeItVerbose(ReturnData.Key + ": " + ReturnData.Value + "\n", 0);
                    // Run through the array containing the list of values to query the database.
                    if (ReturnData.Key.StartsWith(@"+"))
                    {
                        FinalResult += ReturnData.Value;
                    }
                    else if (ReturnData.Key.StartsWith(@"-"))
                    {
                        FinalResult -= ReturnData.Value;
                    }
                    else if (ReturnData.Key.StartsWith(@"*"))
                    {
                        FinalResult *= ReturnData.Value;
                    }
                    else if (ReturnData.Key.StartsWith(@"/"))
                    {
                        // If the array contains only one value and there is no addition or subtraction, should return n.a. This thing looks really iffy. Check again for logic soundness.
                        if (ProdFlowList.Count > 4)
                        {
                                FinalResult /= ReturnData.Value;
                        }
                        else
                        {
                            // This should be returned if a method is defined.
                            return "N/A";
                        }
                    }
                }
                return FinalResult;
            }
        /* Need to refactor. Should decide how to filter by year. Added an overload method bellow to use with year. Can be removed later once decided on how to deal with the year.
        */
        public float ToFloat(IEnumerable<XElement> FilteredSeries)
        {
            IEnumerable<XElement> DataValues = FilteredSeries.Elements("Data").Elements("Observation")/*.Where(observation => (string)observation.Attribute("TIME_PERIOD") == "1999")*/;
            float[] numericalList = new float[DataValues.Count()];
            for (int index = 0; index < DataValues.Count(); index++)
            {
                float FloatValue = 0;
                if (DataValues.ElementAt(index).Attribute("value").Value != "N.A." && DataValues.ElementAt(index).Attribute("value").Value != "N/A")
                {
                    FloatValue = float.Parse(DataValues.ElementAt(index).Attribute("value").Value);
                }
                // Unit multiplier and any other attribute logic goes here. Should it be left here or handled elsewhere?! Should make a switch with all possible values.
                if ((string)DataValues.ElementAt(index).Element("Attributes").Element("NRG_UNIT_SUFFIX").Attribute("value") == "R")
                {
                    FloatValue *= 1;
                }
                numericalList[index] = FloatValue;
            }
            return numericalList.Sum();
        }

        public float[] ToFloat (IEnumerable<XElement> FilteredSeries, string TimePeriod)
        {
            IEnumerable<XElement> DataValues = FilteredSeries.Elements("Data").Elements("Observation").Where(observation => (string)observation.Attribute("TIME_PERIOD") == TimePeriod);
            float[] numericalList = new float[DataValues.Count()];
            for (int index = 0; index < DataValues.Count(); index++)
            {
                float FloatValue = 0;
                if (DataValues.ElementAt(index).Attribute("value").Value != "N.A." && DataValues.ElementAt(index).Attribute("value").Value != "N/A")
                {
                    FloatValue = float.Parse(DataValues.ElementAt(index).Attribute("value").Value);
                }
                // Unit multiplier and any other attribute logic goes here. Should it be left here or handled elsewhere?! Should make a switch with all possible values.
                if ((string)DataValues.ElementAt(index).Element("Attributes").Element("NRG_UNIT_SUFFIX").Attribute("value") == "R")
                {
                    FloatValue *= 1;
                }
                numericalList[index] = FloatValue;
            }
            return numericalList;
        }

        public IEnumerable<XElement> SDMXDimensionFilter(IEnumerable<XElement> SeriesSet, string EnergyProduct, string EnergyFlow)
        {
            IEnumerable<XElement> observationsSet = from series in SeriesSet 
                                                    from observations in series.Elements("Metadata")
                                                    where ((string)observations.Element("SIEC").Attribute("value") == EnergyProduct && (string)observations.Element("NRG_FLOWS").Attribute("value") == EnergyFlow)
                                                    select series;
            return observationsSet;
        }

        //Returns a XElement IEnumerable set that is easier to filter by using the id attributes as node names. NB For future should refactor to enclose in the DataSet node as return just the node. Should do a request for Compact Data instead of Generic to remove this completely.
        public IEnumerable<XElement> NormalizeGeneric(string XmlString)
        {
            XDocument sdmx_response = XDocument.Parse(XmlString);
            XNamespace message = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message";
            XNamespace generic = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic";
            XElement seriesSet = sdmx_response.Root;
            IEnumerable<XElement> seriesKey = seriesSet.Elements(message + "DataSet").Elements(generic + "Series").Select(series => new XElement("Series", new object[]
            {
                new XElement("Metadata", 
                                series.Elements(generic + "SeriesKey").Elements(generic + "Value").Select(value => new XElement((string)value.Attribute("id"), new XAttribute("value", (string)value.Attribute("value"))))),
                new XElement("Data", 
                                series.Elements(generic + "Obs").Select(observations => new XElement("Observation", new XAttribute((string)observations.Element(generic + "ObsDimension").Attribute("id"), (string)observations.Element(generic + "ObsDimension").Attribute("value")), new XAttribute("value", (string)observations.Element(generic + "ObsValue").Attribute("value")), new XElement("Attributes", observations.Elements(generic + "Attributes").Elements(generic + "Value").Select(attributes => new XElement((string)attributes.Attribute("id"), new XAttribute("value", (string)attributes.Attribute("value"))))))))
            })).ToArray();
            return seriesKey;
        }

        // Returns either a N/A string or a float with the arithmetic value for the data variables. Look into the possibility to shorten the code, remove the the second loop if possible.
        public object ExtractDataFromString(string DataCell, IEnumerable<XElement> SDMXCompactDataReturnQuery)
        {
            string regexsplitProducts = @"\(([^)]*)\)";
            string regexsplitFlows = @"[\+\-\*\\]";
            IList<KeyValuePair<string, ICollection<string>>> finalQuery = new List<KeyValuePair<string, ICollection<string>>>();
            DataCell = Regex.Replace(DataCell, @"\s", "");
            string[] stringValueAr;
            stringValueAr = Regex.Split(DataCell, regexsplitProducts);
            for (int i = 0; i < stringValueAr.Count(); i++)
            {
                if (ObjectUtil.ValidString(stringValueAr[i]) && !stringValueAr[i].StartsWith("+") && !stringValueAr[i].StartsWith("-") && !stringValueAr[i].StartsWith("*") && !stringValueAr[i].StartsWith("/"))
                {
                    string[] flows = Regex.Split(stringValueAr[i], regexsplitFlows);
                    string concatenated = stringValueAr[i - 1] + stringValueAr[i];
                    KeyValuePair<string, ICollection<string>> prefinalQuery = new KeyValuePair<string, ICollection<string>>(concatenated, flows);
                    finalQuery.Add(prefinalQuery);
                }
            }
            float FinalResult = 0;
            foreach (KeyValuePair<string, ICollection<string>> element in finalQuery)
            {
                KeyValuePair<string, float> ReturnData = new KeyValuePair<string, float>(element.Key, ToFloat(SDMXDimensionFilter(SDMXCompactDataReturnQuery, element.Value.ElementAt(0), element.Value.ElementAt(1))));
                MakeItVerbose(ReturnData.Key + ": " + ReturnData.Value + "\n", 0);
                // Run through the array containing the list of values to query the database.
                if (ReturnData.Key.StartsWith(@"+"))
                {
                    FinalResult += ReturnData.Value;
                }
                else if (ReturnData.Key.StartsWith(@"-"))
                {
                    FinalResult -= ReturnData.Value;
                }
                else if (ReturnData.Key.StartsWith(@"*"))
                {
                    FinalResult *= ReturnData.Value;
                }
                else if (ReturnData.Key.StartsWith(@"/"))
                {
                    // If the array contains only one value and there is no addition or subtraction, should return n.a. This thing looks really iffy. Check again for logic soundness.
                    if (finalQuery.Count > 4)
                    {
                            FinalResult /= ReturnData.Value;
                    }
                    else
                    {
                        // This should be returned if a method is defined.
                        return "N/A";
                    }
                }
            }
            return FinalResult;
        }

        public IList<KeyValuePair<string, ICollection<string>>> ExtractDataStrings(IEnumerable<XElement> DataList)
        {
            string[] stringValueAr = new string[DataList.Count()];
            string regexsplitProducts = @"\(([^)]*)\)";
            string regexsplitFlows = @"[\+\-\*\\]";
            IList<KeyValuePair<string, ICollection<string>>> finalQuery = new List<KeyValuePair<string, ICollection<string>>>();
            foreach (XElement DC in DataList)
            {
                DC.Value = Regex.Replace(DC.Value, @"\s", "");
                stringValueAr = Regex.Split(DC.Value, regexsplitProducts);
                for (int i = 0; i < stringValueAr.Count(); i++)
                {
                    if (ObjectUtil.ValidString(stringValueAr[i]) && !stringValueAr[i].StartsWith("+") && !stringValueAr[i].StartsWith("-") && !stringValueAr[i].StartsWith("*") && !stringValueAr[i].StartsWith("/"))
                    {
                        string[] flows = Regex.Split(stringValueAr[i], regexsplitFlows);
                        string concatenated = stringValueAr[i - 1] + stringValueAr[i];
                        KeyValuePair<string, ICollection<string>> prefinalQuery = new KeyValuePair<string, ICollection<string>>(concatenated, flows);
                        finalQuery.Add(prefinalQuery);
                    }
                }
            }
            return finalQuery;

        }

        public void MakeItVerbose (object input, int OutputPipe)
        {
            switch (OutputPipe)
            {
                case 0:
                    Debug.WriteLine(input);
                    break;
                case 1:
                    Console.WriteLine(input);
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private DataType GetDataFormat(HttpRequest request)
        {
            string[] accept = request.AcceptTypes;

            for (int i = 0; i < accept.Length; i++)
            {
                if (accept[i].Contains("application/vnd.sdmx.genericdata+xml"))
                {
                    return DataType.GetFromEnum(DataEnumType.Generic21);
                }
                else if (accept[i].Contains("application/vnd.sdmx.structurespecificdata+xml"))
                {
                    return DataType.GetFromEnum(DataEnumType.Compact21);
                }
                else if (accept[i].Contains("application/xml"))
                {
                    return DataType.GetFromEnum(DataEnumType.Generic21);
                }
            }
            return DataType.GetFromEnum(DataEnumType.Generic21);
        }
    }
}
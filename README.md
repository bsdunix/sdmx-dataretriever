# Overview
This project is concerned with the implementation of the SDMX protocol in 
the energy domain. It contains the web service and API for SDMX-ML
messages queries.

- The sample database can be found in the sql\_data folder. 
- The database has to be installed separately and the csv files imported. 
- Mapping Assistant is found as an archive and installed separately.
- The "SDMX Artifacts" folder contains the codelists, concepts schemes, DSDs
  and Dataflows associated with the energy domain. The contents are subject to
  change based on the agreement of a core base definition.

# Infrastructure
At present the project uses .NET version **4.0**

# Changes from the upstream SDMX Source API
- Added an SDMXPOSTData object to handle the deserialization of JSON strings
- Added the modules from Eurostat that connect Mapping Assistant with the
  database:
	* Custom Requests
	* Data Retriever
	* EstatSdmxSourceExtension
	* MappingStoreRetrieval
	* PcAxis2Csv



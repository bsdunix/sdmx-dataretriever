<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>AnnotationCommandBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/</path>
    <filename>a00109</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval</namespace>
  </compound>
  <compound kind="file">
    <name>AnnotationQueryBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00110</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ArtefactCommandBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00111</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ArtefactParentsSqlBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00112</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ArtefactSqlBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00113</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>AuthArtefactCommandBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00114</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>AuthReferenceCommandBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00115</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>CrossDsdBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00116</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>CrossReferenceChildBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00117</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Builder::CrossReferenceChildBuilder</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>DataflowCommandBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00118</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>IAuthCommandBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00119</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ICommandBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00120</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ICrossReferenceRetrievalBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00121</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Builder::ICrossReferenceRetrievalBuilder</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ICrossReferenceSetBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00122</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Builder::ICrossReferenceSetBuilder</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ISqlQueryInfoBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00123</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ItemCommandBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00124</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ItemSqlQueryBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00125</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ItemTableInfoBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00126</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Builder::ItemTableInfoBuilder</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>PartialCodesCommandBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00127</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ReferencedSqlQueryBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00128</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>StructureReferenceFromMutableBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00129</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Builder::StructureReferenceFromMutableBuilder</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>TableInfoBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Builder/</path>
    <filename>a00130</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Builder::TableInfoBuilder</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
  </compound>
  <compound kind="file">
    <name>ConfigManager.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Config/</path>
    <filename>a00131</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::ConfigManager</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Config</namespace>
  </compound>
  <compound kind="file">
    <name>DatabaseSetting.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Config/</path>
    <filename>a00132</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::DatabaseSetting</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Config</namespace>
  </compound>
  <compound kind="file">
    <name>DatabaseSettingCollection.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Config/</path>
    <filename>a00133</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::DatabaseSettingCollection</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Config</namespace>
  </compound>
  <compound kind="file">
    <name>DataflowConfigurationSection.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Config/</path>
    <filename>a00134</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::DataflowConfigurationSection</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Config</namespace>
  </compound>
  <compound kind="file">
    <name>MappingStoreConfigSection.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Config/</path>
    <filename>a00135</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::MappingStoreConfigSection</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Config</namespace>
  </compound>
  <compound kind="file">
    <name>MappingStoreDefaultConstants.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Config/</path>
    <filename>a00136</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::MappingStoreDefaultConstants</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Config</namespace>
  </compound>
  <compound kind="file">
    <name>MastoreProviderMappingSetting.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Config/</path>
    <filename>a00137</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::MastoreProviderMappingSetting</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Config</namespace>
  </compound>
  <compound kind="file">
    <name>MastoreProviderMappingSettingCollection.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Config/</path>
    <filename>a00138</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::MastoreProviderMappingSettingCollection</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Config</namespace>
  </compound>
  <compound kind="file">
    <name>SettingConstants.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Config/</path>
    <filename>a00139</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Config</namespace>
  </compound>
  <compound kind="file">
    <name>AgencySchemeConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00140</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>AnnotationConstants.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00141</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>AttachmentLevelConstants.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00142</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Constants::AttachmentLevelConstants</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>CategorisationConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00143</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>CategorySchemeConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00144</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>CodeListConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00145</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>ConceptSchemeConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00146</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>ContentConstraintConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00147</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>DataConsumerSchemeConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00148</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>DataflowConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00149</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>DataflowFilter.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00150</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>DataProviderSchemeConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00151</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>DsdConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00152</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>HclConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00153</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>LocalisedStringType.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00154</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Constants::LocalisedStringType</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>LocalisedStringType.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00155</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::LocalisedStringType</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>MappingStoreSqlStatements.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00156</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>OrganisationUnitSchemeConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00157</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>ParameterNameConstants.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00158</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>PeriodCodelist.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00159</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Constants::PeriodCodelist</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>SdmxComponentType.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00160</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
    <member kind="enumeration">
      <type></type>
      <name>SdmxComponentType</name>
      <anchorfile>a00304.html</anchorfile>
      <anchor>a70269d23cc6037a48db5643491a53cab</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>StructureSetConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00161</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>VersionQueryType.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00162</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
    <member kind="enumeration">
      <type></type>
      <name>VersionQueryType</name>
      <anchorfile>a00304.html</anchorfile>
      <anchor>afa61fbfcad193c031a74304a3f5961bf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>WhereState.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Constants/</path>
    <filename>a00163</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
  </compound>
  <compound kind="file">
    <name>ArtefactRetrieverEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00164</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>BaseRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00165</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>CategorisationRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00166</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>CategorySchemeRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00167</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>CodeListRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00168</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>ConceptSchemeRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00169</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>CrossReferenceResolverMutableEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00170</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::CrossReferenceResolverMutableEngine</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>DataflowRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00171</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>DsdRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00172</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>HeaderRetrieverEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00173</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::HeaderRetrieverEngine</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>HierarchicalCodeListRetrievealEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00174</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>HierarchicalItemSchemeRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00175</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>ICrossReferenceResolverMutableEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00176</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::ICrossReferenceResolverMutableEngine</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>IdentifiableAnnotationRetrieverEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00177</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>IRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00178</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::IRetrievalEngine</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>ItemSchemeRetrieverEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00179</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>MaintainableAnnotationRetrieverEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00180</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>ComponentMapping.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00181</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ComponentMapping</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ComponentMapping::ColumnOrdinal</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>ComponentMapping1C.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00182</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>ComponentMapping1N.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00183</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>ComponentMapping1to1.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00184</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>ComponentMapping1to1T.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00185</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>ComponentMappingNto1.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00186</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>IComponentMapping.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00187</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::IComponentMapping</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>IMapping.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00188</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::IMapping</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>ITimeDimension.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00189</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ITimeDimension</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>ITimeDimensionMapping.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00190</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ITimeDimensionMapping</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>TimeDimension1Column.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00191</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>TimeDimension1to1.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00192</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimension1To1</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>TimeDimension2Column.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00193</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>TimeDimensionConstant.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00194</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionConstant</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>TimeDimensionDateType.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00195</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>TimeDimensionMapping.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00196</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionMapping</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>TimeDimensionMultiFrequency.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00197</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>TimeDimensionSingleFrequency.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00198</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionSingleFrequency</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>TimeTranscodingWhereBuilder.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00199</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>TranscodingException.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/Mapping/</path>
    <filename>a00200</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TranscodingException</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
  </compound>
  <compound kind="file">
    <name>MappingSetRetriever.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00201</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::MappingSetRetriever</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>PartialCodeListRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00202</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>SubsetCodelistRetrievalEngine.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Engine/</path>
    <filename>a00203</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
  </compound>
  <compound kind="file">
    <name>ErrorMessages.Designer.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/</path>
    <filename>a00204</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval</namespace>
  </compound>
  <compound kind="file">
    <name>AuthExtensions.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Extensions/</path>
    <filename>a00205</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::AuthExtensions</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Extensions</namespace>
  </compound>
  <compound kind="file">
    <name>DatabaseExtension.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Extensions/</path>
    <filename>a00206</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::DatabaseExtension</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Extensions</namespace>
  </compound>
  <compound kind="file">
    <name>DbCommandExtensions.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Extensions/</path>
    <filename>a00207</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::DbCommandExtension</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Extensions</namespace>
  </compound>
  <compound kind="file">
    <name>HeaderExtensions.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Extensions/</path>
    <filename>a00208</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::HeaderExtensions</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Extensions</namespace>
  </compound>
  <compound kind="file">
    <name>MutableMaintainableExtensions.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Extensions/</path>
    <filename>a00209</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::MutableMaintainableExtensions</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Extensions</namespace>
  </compound>
  <compound kind="file">
    <name>QueryExtensions.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Extensions/</path>
    <filename>a00210</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::QueryExtensions</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Extensions</namespace>
  </compound>
  <compound kind="file">
    <name>SdmxDateExtensions.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Extensions/</path>
    <filename>a00211</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::SdmxDateExtensions</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Extensions</namespace>
  </compound>
  <compound kind="file">
    <name>SdmxStructureTypeExtensions.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Extensions/</path>
    <filename>a00212</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::SdmxStructureTypeExtensions</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Extensions</namespace>
  </compound>
  <compound kind="file">
    <name>SetExtensions.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Extensions/</path>
    <filename>a00213</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Extensions</namespace>
  </compound>
  <compound kind="file">
    <name>AdvancedMutableRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00214</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::AdvancedMutableRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>AuthAdvancedMutableRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00215</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::AuthAdvancedMutableRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>AuthCrossMutableRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00216</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::AuthCrossMutableRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>AuthMutableRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00217</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::AuthMutableRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>CrossMutableRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00218</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::CrossMutableRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>IAdvancedMutableRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00219</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::IAdvancedMutableRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>IAuthAdvancedMutableRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00220</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::IAuthAdvancedMutableRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>IAuthCrossRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00221</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::IAuthCrossRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>IAuthMutableRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00222</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::IAuthMutableRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>ICrossRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00223</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::ICrossRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>IMutableRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00224</filename>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::IMutableRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>MutableRetrievalManagerFactory.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Factory/</path>
    <filename>a00225</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::MutableRetrievalManagerFactory</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
  </compound>
  <compound kind="file">
    <name>ConnectionStringHelper.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Helper/</path>
    <filename>a00226</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Helper::ConnectionStringHelper</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Helper</namespace>
  </compound>
  <compound kind="file">
    <name>DatabaseType.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Helper/</path>
    <filename>a00227</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Helper::DatabaseType</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Helper</namespace>
  </compound>
  <compound kind="file">
    <name>DataReaderHelper.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Helper/</path>
    <filename>a00228</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Helper::DataReaderHelper</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Helper</namespace>
  </compound>
  <compound kind="file">
    <name>MaintainableMutableComparer.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Helper/</path>
    <filename>a00229</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Helper::MaintainableMutableComparer</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Helper</namespace>
  </compound>
  <compound kind="file">
    <name>MappingUtils.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Helper/</path>
    <filename>a00230</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Helper::MappingUtils</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Helper</namespace>
  </compound>
  <compound kind="file">
    <name>RetrievalEngineContainer.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Helper/</path>
    <filename>a00231</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Helper</namespace>
  </compound>
  <compound kind="file">
    <name>SecurityHelper.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Helper/</path>
    <filename>a00232</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Helper</namespace>
  </compound>
  <compound kind="file">
    <name>SqlHelper.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Helper/</path>
    <filename>a00233</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Helper</namespace>
  </compound>
  <compound kind="file">
    <name>IncompleteMappingSetException.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/</path>
    <filename>a00234</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::IncompleteMappingSetException</class>
    <namespace>Estat::Sri::MappingStoreRetrieval</namespace>
  </compound>
  <compound kind="file">
    <name>InformativeMessages.Designer.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/</path>
    <filename>a00235</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval</namespace>
  </compound>
  <compound kind="file">
    <name>AdvancedStructureRetriever.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00236</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AdvancedStructureRetriever</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>AuthAdvancedStructureRetriever.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00237</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthAdvancedStructureRetriever</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>AuthCachedAdvancedStructureRetriever.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00238</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthCachedAdvancedStructureRetriever</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>AuthCachedRetrievalManager.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00239</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthCachedRetrievalManager</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>AuthCrossReferenceRetrievalManager.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00240</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthCrossReferenceRetrievalManager</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>AuthMappingStoreRetrievalManager.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00241</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthMappingStoreRetrievalManager</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>AuthRetrievalManagerBase.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00242</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthRetrievalManagerBase</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>CachedRetrievalManager.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00243</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::CachedRetrievalManager</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>CrossReferenceRetrievalManager.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00244</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::CrossReferenceRetrievalManager</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>Database.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00245</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::Database</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>MappingStoreHeaderRetrievalManager.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00246</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::MappingStoreHeaderRetrievalManager</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>MappingStoreRetrievalManager.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00247</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::MappingStoreRetrievalManager</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>MappingStoreSdmxObjectRetrievalManager.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00248</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::MappingStoreSdmxObjectRetrievalManager</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>RetrievalManagerBase.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00249</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::RetrievalManagerBase</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>SpecialMutableObjectRetrievalManager.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Manager/</path>
    <filename>a00250</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::SpecialMutableObjectRetrievalManager</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
  </compound>
  <compound kind="file">
    <name>MappingStoreException.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/</path>
    <filename>a00251</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::MappingStoreException</class>
    <namespace>Estat::Sri::MappingStoreRetrieval</namespace>
  </compound>
  <compound kind="file">
    <name>ArtefactSqlQuery.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00252</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>ItemSqlQuery.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00253</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>ItemTableInfo.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00254</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::ItemTableInfo</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>ListDictionary.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00255</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::ListDictionary</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>MaintainableDictionary.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00256</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MaintainableDictionary</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>MaintainableReferenceDictionary.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00257</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MaintainableReferenceDictionary</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>ArtefactEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00258</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ArtefactEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>AssignmentStatus.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00259</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
    <member kind="enumeration">
      <type></type>
      <name>AssignmentStatus</name>
      <anchorfile>a00312.html</anchorfile>
      <anchor>ac703ed9113b8c03f3a76c8b2e231e996</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>AttachmentLevel.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00260</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
    <member kind="enumeration">
      <type></type>
      <name>AttachmentLevel</name>
      <anchorfile>a00312.html</anchorfile>
      <anchor>a18ff2d3f3a687592dabe9d130489c906</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CategoryEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00261</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CategoryEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>CategorySchemeEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00262</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CategorySchemeEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>CodeCollection.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00263</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CodeCollection</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>CodeListEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00264</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CodeListEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>CodeSetCollection.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00265</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CodeSetCollection</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>ComponentEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00266</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ComponentEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>ConceptEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00267</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ConceptEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>ConnectionEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00268</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ConnectionEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>CrossSectionalLevels.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00269</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
    <member kind="enumeration">
      <type></type>
      <name>CrossSectionalLevels</name>
      <anchorfile>a00312.html</anchorfile>
      <anchor>a67afe90492e37786a0cb64fdb8437fe0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DataFlowEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00270</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DataflowEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>DatasetColumnEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00271</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DataSetColumnEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>DatasetEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00272</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DataSetEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>DsdEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00273</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DsdEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>GroupEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00274</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::GroupEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>ItemEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00275</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ItemEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>MappingEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00276</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::MappingEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>MappingSetEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00277</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::MappingSetEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>PersistentEntityBase.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00278</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>SdmxQueryTimeVO.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00279</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::SdmxQueryTimeVO</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>TimeExpressionEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00280</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TimeExpressionEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>TimeTranscodingCollection.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00281</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TimeTranscodingCollection</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>TimeTranscodingEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00282</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TimeTranscodingEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>TranscodingEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00283</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TranscodingEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>TranscodingRulesEntity.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/MappingStoreModel/</path>
    <filename>a00284</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TranscodingRulesEntity</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
  </compound>
  <compound kind="file">
    <name>PartialCodesSqlQuery.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00285</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>PeriodObject.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00286</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::PeriodObject</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>PrimaryKeySqlQuery.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00287</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>ReferenceSqlQuery.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00288</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>RetrievalSettings.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00289</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::RetrievalSettings</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>SdmxQueryPeriod.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00290</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::SdmxQueryPeriod</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>SqlQueryBase.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00291</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>SqlQueryInfo.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00292</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>TableInfo.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00293</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::TableInfo</class>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>TimeTranscodingFieldOrdinal.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Model/</path>
    <filename>a00294</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
  </compound>
  <compound kind="file">
    <name>AssemblyInfo.cs</name>
    <path>F:/tmp/sources/mapping_store_retrieval.net/src/mapping_store_retrieval.net/src/MappingStore/Properties/</path>
    <filename>a00295</filename>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::AdvancedMutableRetrievalManagerFactory</name>
    <filename>a00001.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Factory::IAdvancedMutableRetrievalManagerFactory</base>
    <member kind="function">
      <type></type>
      <name>AdvancedMutableRetrievalManagerFactory</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>a86e13aaea55bc9512fd9c60061d0debf</anchor>
      <arglist>(Func&lt; object, IAdvancedSdmxMutableObjectRetrievalManager &gt; factoryMethod)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AdvancedMutableRetrievalManagerFactory</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>aaaf9d8f4b1b4dfd458387997ceb5ff6b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>IAdvancedSdmxMutableObjectRetrievalManager</type>
      <name>GetRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>a2a0e6b2751c7add25c7e3a04ebc5c956</anchor>
      <arglist>(T settings)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::AdvancedStructureRetriever</name>
    <filename>a00002.html</filename>
    <member kind="function">
      <type></type>
      <name>AdvancedStructureRetriever</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a057bfb2b4d18466c10cef0772d434b75</anchor>
      <arglist>(ConnectionStringSettings mappingStoreConnectionStringSettings)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AdvancedStructureRetriever</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>af953657df0f752f76ab3739cd45b0555</anchor>
      <arglist>(Database mappingStoreDB)</arglist>
    </member>
    <member kind="function">
      <type>IAgencySchemeMutableObject</type>
      <name>GetMutableAgencyScheme</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a31f921da29358ccfa8fc4437eaa9a1b2</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IAgencySchemeMutableObject &gt;</type>
      <name>GetMutableAgencySchemeObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a446367d438bd3dcb95b434e890373eaa</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ICategorisationMutableObject</type>
      <name>GetMutableCategorisation</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a952f274b10d1f8691e24e2faa88cbe22</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICategorisationMutableObject &gt;</type>
      <name>GetMutableCategorisationObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>ae9eee9991199567166f660dbc40b6fea</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ICategorySchemeMutableObject</type>
      <name>GetMutableCategoryScheme</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a4faed03052f85dc517a3846348b77bf4</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICategorySchemeMutableObject &gt;</type>
      <name>GetMutableCategorySchemeObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>aecedcea623c447fb8500a10384dfc01c</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ICodelistMutableObject</type>
      <name>GetMutableCodelist</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a4813b33b995b193389f0c7971c826e7d</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>ab15107443668c9f314600a6e1bb59615</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IConceptSchemeMutableObject</type>
      <name>GetMutableConceptScheme</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>af446c5638774f390c3572fecf9598de2</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IConceptSchemeMutableObject &gt;</type>
      <name>GetMutableConceptSchemeObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a28ed2063bbf45a409017b7b57d4260ff</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IContentConstraintMutableObject</type>
      <name>GetMutableContentConstraint</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a52d08b1c41dacf2576bd87d460b7eee0</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IContentConstraintMutableObject &gt;</type>
      <name>GetMutableContentConstraintObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>ac350dc619cd7a26ccbaf62c7aa1adcd2</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataConsumerSchemeMutableObject</type>
      <name>GetMutableDataConsumerScheme</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a9415aa59dc8da5a7b2cd1be4a5708105</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataConsumerSchemeMutableObject &gt;</type>
      <name>GetMutableDataConsumerSchemeObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>aa1fe9e4f1343c44de39a358e5088a027</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataProviderSchemeMutableObject</type>
      <name>GetMutableDataProviderScheme</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a701dff3cb15d9eb7de0165c007da0669</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataProviderSchemeMutableObject &gt;</type>
      <name>GetMutableDataProviderSchemeObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a38ea7e03362bf2a2faed8499a3596a2b</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataStructureMutableObject</type>
      <name>GetMutableDataStructure</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a2b2bc43ee8d29598c1a26378d3ecff2d</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataStructureMutableObject &gt;</type>
      <name>GetMutableDataStructureObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>aa6d78e0f00b1b64c802a86ebac371076</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataflowMutableObject</type>
      <name>GetMutableDataflow</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>ac2677d7d8245cfd884a6d19f88b73df7</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataflowMutableObject &gt;</type>
      <name>GetMutableDataflowObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>ab69e41f98b77db2d8941782fe7df02f2</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IHierarchicalCodelistMutableObject</type>
      <name>GetMutableHierarchicCodeList</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>ac860e77abf3099343b75e2a0d68c8386</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IHierarchicalCodelistMutableObject &gt;</type>
      <name>GetMutableHierarchicCodeListObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a1491ffdfa96de0efe7ab9c9ed6273407</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IMaintainableMutableObject</type>
      <name>GetMutableMaintainable</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a21e0039ebd2349fecd5f7659927dd429</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMaintainableMutableObject &gt;</type>
      <name>GetMutableMaintainables</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a5e56331a59ad60590ab3eb8e5892bb20</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IMetadataStructureDefinitionMutableObject</type>
      <name>GetMutableMetadataStructure</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a6daef14e43f5d271c746cb1c2aea98ed</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMetadataStructureDefinitionMutableObject &gt;</type>
      <name>GetMutableMetadataStructureObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a760d386c62e21f9347a0bad17368814a</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IMetadataFlowMutableObject</type>
      <name>GetMutableMetadataflow</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a9c9c4f09c15728f1e8c870c372abac00</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMetadataFlowMutableObject &gt;</type>
      <name>GetMutableMetadataflowObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a8604e5c6ceaafdb41426b84ce2968e85</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IOrganisationUnitSchemeMutableObject</type>
      <name>GetMutableOrganisationUnitScheme</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>aee8dadefc17112bd95fb955bf6f5f816</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IOrganisationUnitSchemeMutableObject &gt;</type>
      <name>GetMutableOrganisationUnitSchemeObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a4bcae1dbee85d50d18dc9f3cc6f3816b</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IProcessMutableObject</type>
      <name>GetMutableProcessObject</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>ac11065f40db47cea1db4d73c83a0018d</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IProcessMutableObject &gt;</type>
      <name>GetMutableProcessObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a34b908a78c52b0af06e5687ec7003e91</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IProvisionAgreementMutableObject</type>
      <name>GetMutableProvisionAgreement</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>adb70a27c183b09b8c2505e16f393bf8e</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IProvisionAgreementMutableObject &gt;</type>
      <name>GetMutableProvisionAgreementBeans</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a364d27841bca05b27f25bb74fa0d0948</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IReportingTaxonomyMutableObject</type>
      <name>GetMutableReportingTaxonomy</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a58281ba4299bfcc498746fbc65489809</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IReportingTaxonomyMutableObject &gt;</type>
      <name>GetMutableReportingTaxonomyObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>ab3bf081f9e1cdcceb43216a950c98ca2</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IStructureSetMutableObject</type>
      <name>GetMutableStructureSet</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a5b087fc4b4cde4326d0ac4ab4f3be52e</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IStructureSetMutableObject &gt;</type>
      <name>GetMutableStructureSetObjects</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a139c38cbb679e55b4d3e6a68097a8f34</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ArtefactEntity</name>
    <filename>a00003.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function" protection="protected">
      <type></type>
      <name>ArtefactEntity</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>a9fdc571548ea0c95e56e90094ca1220c</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Agency</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>a6d500e4ee375993a459a444506961e47</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Id</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>aec81476baaffd9c1c54638ee18bdf906</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Version</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>a0e1af78c1038fca302cbdebda25380ed</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Constants::AttachmentLevelConstants</name>
    <filename>a00004.html</filename>
    <member kind="variable">
      <type>const string</type>
      <name>DataSet</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>a4cdb867bd16c5c3d6e3c23d0b0ae9386</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>Group</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>a063afb8865686dad7ea0c7a0a943ae2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>Observation</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>a6a04387087e45e5ffa4abce2935f76cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>Series</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>a49ac6d4c73e5f23d290b6ad0802b6680</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::AuthAdvancedMutableRetrievalManagerFactory</name>
    <filename>a00005.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Factory::IAuthAdvancedMutableRetrievalManagerFactory</base>
    <member kind="function">
      <type></type>
      <name>AuthAdvancedMutableRetrievalManagerFactory</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a9deb6f348749cc6d5538b1135d21f2a0</anchor>
      <arglist>(Func&lt; object, IAuthAdvancedSdmxMutableObjectRetrievalManager &gt; factoryMethod)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AuthAdvancedMutableRetrievalManagerFactory</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a80351c101c9121a0bea30aa4c17b85b0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>IAuthAdvancedSdmxMutableObjectRetrievalManager</type>
      <name>GetRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a02810b90b6a863f31a8a98a258c99485</anchor>
      <arglist>(T settings)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::AuthAdvancedStructureRetriever</name>
    <filename>a00006.html</filename>
    <member kind="function">
      <type></type>
      <name>AuthAdvancedStructureRetriever</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a3b912f79dfd1e3ef5698bb381daffa9d</anchor>
      <arglist>(IAdvancedSdmxMutableObjectRetrievalManager retrievalManager, Database mappingStoreDatabase)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AuthAdvancedStructureRetriever</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ac4ed51b37b9a7fd2bef39a4b702dd9f7</anchor>
      <arglist>(ConnectionStringSettings mappingStoreSettings)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AuthAdvancedStructureRetriever</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a8d917a5d3174c974c00fe420dbab7c9a</anchor>
      <arglist>(Database mappingStoreDatabase)</arglist>
    </member>
    <member kind="function">
      <type>IAgencySchemeMutableObject</type>
      <name>GetMutableAgencyScheme</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ae91e0c1eea5504626a0a3d534e66b623</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IAgencySchemeMutableObject &gt;</type>
      <name>GetMutableAgencySchemeObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a387c3116bd7c2af0a846218537c1a598</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ICategorisationMutableObject</type>
      <name>GetMutableCategorisation</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a628b75c494da2c2a8e6ad3f52aada017</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICategorisationMutableObject &gt;</type>
      <name>GetMutableCategorisationObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ac029f6d10a58be19b6e7802dc0566c11</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ICategorySchemeMutableObject</type>
      <name>GetMutableCategoryScheme</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ad8eb23b7c819f7a953da14aa541877fa</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICategorySchemeMutableObject &gt;</type>
      <name>GetMutableCategorySchemeObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a69b338e524e86b169d38b1a2d30af043</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ICodelistMutableObject</type>
      <name>GetMutableCodelist</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ac74aa7d47f2bc613d78ad03ea9765b08</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a001e1f20ae73701c3b778b0938f53628</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IConceptSchemeMutableObject</type>
      <name>GetMutableConceptScheme</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ac7ed5ec73de035af53f1ff79cd9015c8</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IConceptSchemeMutableObject &gt;</type>
      <name>GetMutableConceptSchemeObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a90396218ae11fcbf3b9b00e90586ef18</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IContentConstraintMutableObject</type>
      <name>GetMutableContentConstraint</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a08317667544da54bd6c351c2aee9a8b0</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IContentConstraintMutableObject &gt;</type>
      <name>GetMutableContentConstraintObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a3e96d7e80af7cd1114419ee0dece7b14</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataConsumerSchemeMutableObject</type>
      <name>GetMutableDataConsumerScheme</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a2d36f4f965e986619627646c0aa28ca4</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataConsumerSchemeMutableObject &gt;</type>
      <name>GetMutableDataConsumerSchemeObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a6561758af05d55958cd7329eed8bd225</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataProviderSchemeMutableObject</type>
      <name>GetMutableDataProviderScheme</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a9f266a937081bd36725f198a62bfad5b</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataProviderSchemeMutableObject &gt;</type>
      <name>GetMutableDataProviderSchemeObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a564856c337a929e638f467fa6f6b2a0b</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataStructureMutableObject</type>
      <name>GetMutableDataStructure</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a7e6632c2965887d5b8a7f9c4745fab8d</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataStructureMutableObject &gt;</type>
      <name>GetMutableDataStructureObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a7d4d36a60e6bfa2eb8f24fe963418ea0</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataflowMutableObject</type>
      <name>GetMutableDataflow</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ab5880a781dfd142d7b50f3f9130a16d6</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflow)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataflowMutableObject &gt;</type>
      <name>GetMutableDataflowObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a44708a435a875f8e79b84851daf389d4</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflow)</arglist>
    </member>
    <member kind="function">
      <type>IHierarchicalCodelistMutableObject</type>
      <name>GetMutableHierarchicCodeList</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a8d2fc585d5e75dec17dd318c59aaea7b</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IHierarchicalCodelistMutableObject &gt;</type>
      <name>GetMutableHierarchicCodeListObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ac18075fa42d37a6c7208dcecc389bbf6</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IMaintainableMutableObject</type>
      <name>GetMutableMaintainable</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a26d6a2cdb471d96e7b273b952e1936e7</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMaintainableMutableObject &gt;</type>
      <name>GetMutableMaintainables</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>acc1c83431565903f251a8e130f07c318</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>IMetadataStructureDefinitionMutableObject</type>
      <name>GetMutableMetadataStructure</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a7b12fa30f63ff6ee0f3d8592b5bb597d</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMetadataStructureDefinitionMutableObject &gt;</type>
      <name>GetMutableMetadataStructureObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a119e291f23b9ed68d2d75de4f3ddfe4b</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IMetadataFlowMutableObject</type>
      <name>GetMutableMetadataflow</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a643d394b392002e0ce6d16557514797e</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMetadataFlowMutableObject &gt;</type>
      <name>GetMutableMetadataflowObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>abeb9a29976b8540af42b1287fe6b1e37</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IOrganisationUnitSchemeMutableObject</type>
      <name>GetMutableOrganisationUnitScheme</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a76dc760bcd3b6411a63ffa7571def5da</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IOrganisationUnitSchemeMutableObject &gt;</type>
      <name>GetMutableOrganisationUnitSchemeObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ac0fac18ab385bdb1060e008081ecf6cc</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IProcessMutableObject</type>
      <name>GetMutableProcessObject</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>af407968879b25d3abccb579a172e9e3e</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IProcessMutableObject &gt;</type>
      <name>GetMutableProcessObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>afd6e0ab6c498c8ff30fc186bb2cc1893</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IProvisionAgreementMutableObject</type>
      <name>GetMutableProvisionAgreement</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a3a1441ed0cf2f172c8836461c4508aec</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IProvisionAgreementMutableObject &gt;</type>
      <name>GetMutableProvisionAgreementBeans</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a7f27a25e2929cfc8164028da8dde3f26</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IReportingTaxonomyMutableObject</type>
      <name>GetMutableReportingTaxonomy</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a2ffbe086665103b05aacbb3dc297c0b4</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IReportingTaxonomyMutableObject &gt;</type>
      <name>GetMutableReportingTaxonomyObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a21d5cdd91daaa98fdf153f7d85bc186e</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IStructureSetMutableObject</type>
      <name>GetMutableStructureSet</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a1d25a8641ecb65087c1c40db937db83a</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IStructureSetMutableObject &gt;</type>
      <name>GetMutableStructureSetObjects</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a424c2c7accaa56a306748fc2cdc2fd96</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::AuthCachedAdvancedStructureRetriever</name>
    <filename>a00007.html</filename>
    <member kind="function">
      <type></type>
      <name>AuthCachedAdvancedStructureRetriever</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a074b6e6bfb598fa619267b06ee074b16</anchor>
      <arglist>(IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
    <member kind="function">
      <type>IAgencySchemeMutableObject</type>
      <name>GetMutableAgencyScheme</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ae0789807f77ca13f2179bf9756dc645e</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IAgencySchemeMutableObject &gt;</type>
      <name>GetMutableAgencySchemeObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>afc4c62d855a07c28ad3700c61eb7ac4e</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ICategorisationMutableObject</type>
      <name>GetMutableCategorisation</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>af2db4b6cc4e00c95e6ee4358297cf423</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICategorisationMutableObject &gt;</type>
      <name>GetMutableCategorisationObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a96019ea96c4e48d8faea4758ca057d3f</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ICategorySchemeMutableObject</type>
      <name>GetMutableCategoryScheme</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a19e434e678e2636fcead07e727be24c6</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICategorySchemeMutableObject &gt;</type>
      <name>GetMutableCategorySchemeObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ab9a496c5fa604a8bd5a1c929ab79022a</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ICodelistMutableObject</type>
      <name>GetMutableCodelist</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ab62d3068fcbabdb41c06a8c3f1a89026</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a0b27137b9147272321bb26e9c1759a72</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IConceptSchemeMutableObject</type>
      <name>GetMutableConceptScheme</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>af102fb9c098ad4b8cf552e7d37c58ef5</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IConceptSchemeMutableObject &gt;</type>
      <name>GetMutableConceptSchemeObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ad486051929dca7d8e28383b7cb025700</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IContentConstraintMutableObject</type>
      <name>GetMutableContentConstraint</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>aa314ae8b75d3bcfac7aeb9d73a884394</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IContentConstraintMutableObject &gt;</type>
      <name>GetMutableContentConstraintObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a79503995ed1de23406980ceb5f60f7ac</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataConsumerSchemeMutableObject</type>
      <name>GetMutableDataConsumerScheme</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>af0bbb58d2fd58b026cc8f9569fba4b82</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataConsumerSchemeMutableObject &gt;</type>
      <name>GetMutableDataConsumerSchemeObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ae83fdfca43d296c9da50dde5eaef2842</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataProviderSchemeMutableObject</type>
      <name>GetMutableDataProviderScheme</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a07949815c394b5c8eac4714a8601a245</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataProviderSchemeMutableObject &gt;</type>
      <name>GetMutableDataProviderSchemeObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>abdc0dfe0ed6ac6d61b6c7a0fd0671ea4</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataStructureMutableObject</type>
      <name>GetMutableDataStructure</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ae4c97361e6023f97f0fb2227ddb93ec8</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataStructureMutableObject &gt;</type>
      <name>GetMutableDataStructureObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>afa7bbe1042f10bc10acababd6005dad6</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IDataflowMutableObject</type>
      <name>GetMutableDataflow</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a334f8bbb84ac4bf6df4fd4f900280534</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflow)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IDataflowMutableObject &gt;</type>
      <name>GetMutableDataflowObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a97d9004e2d9864871dd7aae8f90d9f98</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflow)</arglist>
    </member>
    <member kind="function">
      <type>IHierarchicalCodelistMutableObject</type>
      <name>GetMutableHierarchicCodeList</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a06f567aa8170cbcdd16e83a4372323bc</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IHierarchicalCodelistMutableObject &gt;</type>
      <name>GetMutableHierarchicCodeListObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a3040ac9a44a9f9eb32bf79576972f86d</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IMaintainableMutableObject</type>
      <name>GetMutableMaintainable</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a647f7d6542726c5d30d7526024797de6</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMaintainableMutableObject &gt;</type>
      <name>GetMutableMaintainables</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>af59c6b0819cc99bb5fe8f0a418dad7ed</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>IMetadataStructureDefinitionMutableObject</type>
      <name>GetMutableMetadataStructure</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a9ec23470a04f5340a19e19b915475749</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMetadataStructureDefinitionMutableObject &gt;</type>
      <name>GetMutableMetadataStructureObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ab6ce7146ea872703e4dcfcf310e3478b</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IMetadataFlowMutableObject</type>
      <name>GetMutableMetadataflow</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ac5945e5710aace58db45d057758acbd2</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMetadataFlowMutableObject &gt;</type>
      <name>GetMutableMetadataflowObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a76f7b2cf71918170af6621b99df527b3</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IOrganisationUnitSchemeMutableObject</type>
      <name>GetMutableOrganisationUnitScheme</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>aff36c22bf47eea01e4a1c166c7ce44dc</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IOrganisationUnitSchemeMutableObject &gt;</type>
      <name>GetMutableOrganisationUnitSchemeObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ab5961e82ac513a827a80e490dc4b1a01</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IProcessMutableObject</type>
      <name>GetMutableProcessObject</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a1009a4b29320fad6266b8b63f089608d</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IProcessMutableObject &gt;</type>
      <name>GetMutableProcessObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a7e13b800242be0077138401d617d72b2</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IProvisionAgreementMutableObject</type>
      <name>GetMutableProvisionAgreement</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ad3a9669f9ccbc163cc3720beae959b8b</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IProvisionAgreementMutableObject &gt;</type>
      <name>GetMutableProvisionAgreementBeans</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a9d21aba2b9127958bd0749944713c726</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IReportingTaxonomyMutableObject</type>
      <name>GetMutableReportingTaxonomy</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a945336a9a21b57007be5921ec23dfbd9</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IReportingTaxonomyMutableObject &gt;</type>
      <name>GetMutableReportingTaxonomyObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a89a7da46a6d80a52ed562a79ae783224</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>IStructureSetMutableObject</type>
      <name>GetMutableStructureSet</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>af5a8f956813b602b6660000d2772c4a5</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IStructureSetMutableObject &gt;</type>
      <name>GetMutableStructureSetObjects</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a00debecc6c10668ea0dc3ddd655a0176</anchor>
      <arglist>(IComplexStructureReferenceObject complexRef, ComplexStructureQueryDetail returnDetail)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::AuthCachedRetrievalManager</name>
    <filename>a00008.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Manager::AuthRetrievalManagerBase</base>
    <member kind="function">
      <type></type>
      <name>AuthCachedRetrievalManager</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>adbecc3e8cf33f5c712bc3cd7c12810cc</anchor>
      <arglist>(IEnumerable&lt; IMaintainableMutableObject &gt; maintainableMutableObjects, IAuthSdmxMutableObjectRetrievalManager retrievalAuthManager)</arglist>
    </member>
    <member kind="function">
      <type>override IAgencySchemeMutableObject</type>
      <name>GetMutableAgencyScheme</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ab1026f9d4cdcec07e5a89543ec0c5856</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IAgencySchemeMutableObject &gt;</type>
      <name>GetMutableAgencySchemeObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ade6ab43b7e4b14917222ec216b7bf24e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ICategorisationMutableObject</type>
      <name>GetMutableCategorisation</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a59317ae3297add54b2ecde39b0aed15f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICategorisationMutableObject &gt;</type>
      <name>GetMutableCategorisationObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a977a495ddef0906f18d5c8a79f192582</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>override ICategorySchemeMutableObject</type>
      <name>GetMutableCategoryScheme</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a3128298fff5907d7b3ac0f51dfa76263</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICategorySchemeMutableObject &gt;</type>
      <name>GetMutableCategorySchemeObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>aae1d209ff6020e3619c5c9291604e59f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ICodelistMutableObject</type>
      <name>GetMutableCodelist</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a5cc9d3a7768e4979522320b0e8e59e04</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ac8413707b610666c7745e6d45218e520</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IConceptSchemeMutableObject</type>
      <name>GetMutableConceptScheme</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a114ec46d8b4386cca357208e1e901d50</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IConceptSchemeMutableObject &gt;</type>
      <name>GetMutableConceptSchemeObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a37ffddb667f00eec3c9623f6cfad8173</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IContentConstraintMutableObject</type>
      <name>GetMutableContentConstraint</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ab51b0ebc3d43d22c89900732c90622bf</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IContentConstraintMutableObject &gt;</type>
      <name>GetMutableContentConstraintObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a25b9bac28d3e5c8c6faa23bbd324e1c2</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataConsumerSchemeMutableObject</type>
      <name>GetMutableDataConsumerScheme</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a8b9fc1e696340877eba86f6e2d7d3b18</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataConsumerSchemeMutableObject &gt;</type>
      <name>GetMutableDataConsumerSchemeObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a9ff45e5eaf468ef41548d366e522ed9e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataProviderSchemeMutableObject</type>
      <name>GetMutableDataProviderScheme</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a902599d29f8f778dc510e504d7e8701b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataProviderSchemeMutableObject &gt;</type>
      <name>GetMutableDataProviderSchemeObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a1d071cd701502fa877a23751e60f3dde</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataStructureMutableObject</type>
      <name>GetMutableDataStructure</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a93e80bfb1f20af57d290776d32749601</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataStructureMutableObject &gt;</type>
      <name>GetMutableDataStructureObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a7815a211e756755838144fec7c47db60</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataflowMutableObject</type>
      <name>GetMutableDataflow</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a5a29d30af3a8518d64b72a9d432cdcc3</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataflowMutableObject &gt;</type>
      <name>GetMutableDataflowObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a3596c1ce151f0bd5a9877bb18cc9d383</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>override IHierarchicalCodelistMutableObject</type>
      <name>GetMutableHierarchicCodeList</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a57fee11490f7045b40424dc3ce9eebb1</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IHierarchicalCodelistMutableObject &gt;</type>
      <name>GetMutableHierarchicCodeListObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a9cb532fb8577c5a34f768aac9b33dfb5</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IMetadataStructureDefinitionMutableObject</type>
      <name>GetMutableMetadataStructure</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a3f36f950c906cc9983edbd5ce62a1b50</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IMetadataStructureDefinitionMutableObject &gt;</type>
      <name>GetMutableMetadataStructureObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a6b0bddd09f806bdcadf605c6c2fb7e70</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IMetadataFlowMutableObject</type>
      <name>GetMutableMetadataflow</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ad21200cb545851ce242486219271f666</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IMetadataFlowMutableObject &gt;</type>
      <name>GetMutableMetadataflowObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a681abfd9ea931b55cdba2e323551474f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IOrganisationUnitSchemeMutableObject</type>
      <name>GetMutableOrganisationUnitScheme</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a75b9c25d17110776ac270908ba3b5e0d</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IOrganisationUnitSchemeMutableObject &gt;</type>
      <name>GetMutableOrganisationUnitSchemeObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a747f4cb98f5269339bea47a8940c80ff</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IProcessMutableObject</type>
      <name>GetMutableProcessObject</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a3fa5cf1d8f05ab48410147d020dd092e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IProcessMutableObject &gt;</type>
      <name>GetMutableProcessObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a6d2ff58ca64a589de0b560dd4c60d04a</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IProvisionAgreementMutableObject</type>
      <name>GetMutableProvisionAgreement</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a0ad95c7db33c51530876e790bf79b04d</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IProvisionAgreementMutableObject &gt;</type>
      <name>GetMutableProvisionAgreementObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a3864689e329b34056eaf0349b7236879</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IReportingTaxonomyMutableObject</type>
      <name>GetMutableReportingTaxonomy</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>af95004ee7688db42398750e6044fb520</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IReportingTaxonomyMutableObject &gt;</type>
      <name>GetMutableReportingTaxonomyObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a0dbc2996c84bc316c4512c939f1f70d0</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IStructureSetMutableObject</type>
      <name>GetMutableStructureSet</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ab482450a2ad54f2cee3c132c6338a02c</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IStructureSetMutableObject &gt;</type>
      <name>GetMutableStructureSetObjects</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a4891f6cef37c716b215fa85cf46507bb</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::AuthCrossMutableRetrievalManagerFactory</name>
    <filename>a00009.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Factory::IAuthCrossRetrievalManagerFactory</base>
    <member kind="function">
      <type></type>
      <name>AuthCrossMutableRetrievalManagerFactory</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a24b0d97d20c1962f0ae93266f3bff587</anchor>
      <arglist>(Func&lt; object, IAuthSdmxMutableObjectRetrievalManager, IAuthCrossReferenceMutableRetrievalManager &gt; factoryMethod, Func&lt; object, IAuthAdvancedSdmxMutableObjectRetrievalManager, IAuthCrossReferenceMutableRetrievalManager &gt; factoryMethodAdvanced)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AuthCrossMutableRetrievalManagerFactory</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a3ddbcb1b382a4adca471289af6899461</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>IAuthCrossReferenceMutableRetrievalManager</type>
      <name>GetCrossRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>aaff13008ac3986f4336c38c590580792</anchor>
      <arglist>(T settings, IAuthSdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
    <member kind="function">
      <type>IAuthCrossReferenceMutableRetrievalManager</type>
      <name>GetCrossRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a39c7da504bad06bc3c6ad15ec557d3d6</anchor>
      <arglist>(T settings, IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::AuthCrossReferenceRetrievalManager</name>
    <filename>a00010.html</filename>
    <member kind="function">
      <type></type>
      <name>AuthCrossReferenceRetrievalManager</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>a8522c629d7f5cdf2a052c3c298723558</anchor>
      <arglist>(IAuthSdmxMutableObjectRetrievalManager retrievalManager, ConnectionStringSettings connectionStringSettings)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AuthCrossReferenceRetrievalManager</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>a8aac62fe2c954bba31125126866421f1</anchor>
      <arglist>(IAuthSdmxMutableObjectRetrievalManager retrievalManager, Database mappingStoreDatabase)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AuthCrossReferenceRetrievalManager</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>ab8aa89e87ecd62e44a2c081021431679</anchor>
      <arglist>(IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalAdvancedManager, Database mappingStoreDatabase)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AuthCrossReferenceRetrievalManager</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>a4fecd8cdd2b47cc76847048fc66e0bd0</anchor>
      <arglist>(IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalAdvancedManager, ConnectionStringSettings connectionStringSettings)</arglist>
    </member>
    <member kind="function">
      <type>IMutableCrossReferencingTree</type>
      <name>GetCrossReferenceTree</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>a526cd328b1b6d42006d376a22e88ed21</anchor>
      <arglist>(IMaintainableMutableObject maintainableObject, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>IList&lt; IMaintainableMutableObject &gt;</type>
      <name>GetCrossReferencedStructures</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>a367035145d83ae50d5b1449a6b4330f4</anchor>
      <arglist>(IStructureReference structureReference, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows, params SdmxStructureType[] structures)</arglist>
    </member>
    <member kind="function">
      <type>IList&lt; IMaintainableMutableObject &gt;</type>
      <name>GetCrossReferencedStructures</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>aa2c005e91617f6020708a2dbcbe2561e</anchor>
      <arglist>(IIdentifiableMutableObject identifiable, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows, params SdmxStructureType[] structures)</arglist>
    </member>
    <member kind="function">
      <type>IList&lt; IMaintainableMutableObject &gt;</type>
      <name>GetCrossReferencingStructures</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>acd6a2da61775e4ede47931d789c7cb79</anchor>
      <arglist>(IStructureReference structureReference, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows, params SdmxStructureType[] structures)</arglist>
    </member>
    <member kind="function">
      <type>IList&lt; IMaintainableMutableObject &gt;</type>
      <name>GetCrossReferencingStructures</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>af32d299133d0ef16846027fd46ae425d</anchor>
      <arglist>(IIdentifiableMutableObject identifiable, bool returnStub, params SdmxStructureType[] structures)</arglist>
    </member>
    <member kind="function">
      <type>IList&lt; IMaintainableMutableObject &gt;</type>
      <name>GetCrossReferencingStructures</name>
      <anchorfile>a00010.html</anchorfile>
      <anchor>a8a6655760c678661e243f9ea0a263abf</anchor>
      <arglist>(IIdentifiableMutableObject identifiable, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows, params SdmxStructureType[] structures)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Extensions::AuthExtensions</name>
    <filename>a00011.html</filename>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>ValidateAuthManager</name>
      <anchorfile>a00011.html</anchorfile>
      <anchor>adbd9a8033412fa87f589e94ca09770f6</anchor>
      <arglist>(this IAuthSdmxMutableObjectRetrievalManager authManager, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>NeedsAuth</name>
      <anchorfile>a00011.html</anchorfile>
      <anchor>ace153d709f66c54681127a03cb5cc44e</anchor>
      <arglist>(this IStructureReference structureType)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::AuthMappingStoreRetrievalManager</name>
    <filename>a00012.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Manager::AuthRetrievalManagerBase</base>
    <member kind="function">
      <type></type>
      <name>AuthMappingStoreRetrievalManager</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a02c9152c03342c97b4055f7c185dedb6</anchor>
      <arglist>(ConnectionStringSettings mappingStoreSettings, ISdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AuthMappingStoreRetrievalManager</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a1e7f377e5d500767a69a804002bd0e31</anchor>
      <arglist>(Database mappingStoreDB, ISdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
    <member kind="function">
      <type>override IAgencySchemeMutableObject</type>
      <name>GetMutableAgencyScheme</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>aa0ae629c1aba8f3c5d8ac5d26d25d7e4</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IAgencySchemeMutableObject &gt;</type>
      <name>GetMutableAgencySchemeObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a269395746dbbd4bed4ff91e6763319ab</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ICategorisationMutableObject</type>
      <name>GetMutableCategorisation</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>aaacfd6ff23cf61fd5fec97790463ede2</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICategorisationMutableObject &gt;</type>
      <name>GetMutableCategorisationObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>ae06a432514ddce9842af5821fef17e4f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>override ICategorySchemeMutableObject</type>
      <name>GetMutableCategoryScheme</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a91aeb1811cff9646f8c74b4f874faa8d</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICategorySchemeMutableObject &gt;</type>
      <name>GetMutableCategorySchemeObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a46bfa6043e6b1b3de3d3551422d02ae7</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ICodelistMutableObject</type>
      <name>GetMutableCodelist</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a0f8a644adadc193c0b7b3c1a1537e1ff</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a09f611634d15d70f1799278e73a60577</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IConceptSchemeMutableObject</type>
      <name>GetMutableConceptScheme</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>af5429ff42696286b00ebba84e0181c64</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IConceptSchemeMutableObject &gt;</type>
      <name>GetMutableConceptSchemeObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a0b4d149da6d92f9ca23b444dfbc5e72b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IContentConstraintMutableObject</type>
      <name>GetMutableContentConstraint</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a69d1427e35af3684cd941de82679a00c</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IContentConstraintMutableObject &gt;</type>
      <name>GetMutableContentConstraintObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a9fefc1aae10404dead55d9768a367579</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataConsumerSchemeMutableObject</type>
      <name>GetMutableDataConsumerScheme</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>ab3c8c08c80e384fe0944bf47545276ac</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataConsumerSchemeMutableObject &gt;</type>
      <name>GetMutableDataConsumerSchemeObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a131386c1428ef0e9391c49e95ca5526c</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataProviderSchemeMutableObject</type>
      <name>GetMutableDataProviderScheme</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>affd55d0767960baf77113f273a4d6d11</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataProviderSchemeMutableObject &gt;</type>
      <name>GetMutableDataProviderSchemeObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a5b6013727efe8b2e93305336a5fb579b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataStructureMutableObject</type>
      <name>GetMutableDataStructure</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>aec30b0f00c26cd3c7c93ad6a047dfb1d</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataStructureMutableObject &gt;</type>
      <name>GetMutableDataStructureObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>ac4b7cd0c2004a0552e4034c060d01e03</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataflowMutableObject</type>
      <name>GetMutableDataflow</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>ac2b31d42c62230dfb1535e617dd38082</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataflowMutableObject &gt;</type>
      <name>GetMutableDataflowObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a812fbd6978a051a5e9a1fb578a3feeb9</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>override IHierarchicalCodelistMutableObject</type>
      <name>GetMutableHierarchicCodeList</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a2dfc028793a36b50280ee21143dc783c</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IHierarchicalCodelistMutableObject &gt;</type>
      <name>GetMutableHierarchicCodeListObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>ab14f127ec6869d79d9f828ed7c66812f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IMetadataStructureDefinitionMutableObject</type>
      <name>GetMutableMetadataStructure</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a2cfdb35dfbae5d12ffe8cff30c276ae8</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IMetadataStructureDefinitionMutableObject &gt;</type>
      <name>GetMutableMetadataStructureObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>acdc7213aa62e9d9851b718ceab64b0a5</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IMetadataFlowMutableObject</type>
      <name>GetMutableMetadataflow</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>afa3ce2397f9f3f56c20fabafe62530f2</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IMetadataFlowMutableObject &gt;</type>
      <name>GetMutableMetadataflowObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>aa50d7fef712318e0b7ed47a5fa829d77</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IOrganisationUnitSchemeMutableObject</type>
      <name>GetMutableOrganisationUnitScheme</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a80f06ae4ffb778b0a4d4a50b7ae7189a</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IOrganisationUnitSchemeMutableObject &gt;</type>
      <name>GetMutableOrganisationUnitSchemeObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>ac93b450ff82bc1af587f5937273ef421</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IProcessMutableObject</type>
      <name>GetMutableProcessObject</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>af9d5179d64306ccd04b5dfd0e76bea37</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IProcessMutableObject &gt;</type>
      <name>GetMutableProcessObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>afaf5873bcf98af876152453edbaa2e7e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IProvisionAgreementMutableObject</type>
      <name>GetMutableProvisionAgreement</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>af214259608f1e0556b1333470e2bd99b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IProvisionAgreementMutableObject &gt;</type>
      <name>GetMutableProvisionAgreementObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>ab122df257e76a1444aa6a9c2a3a8b967</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IReportingTaxonomyMutableObject</type>
      <name>GetMutableReportingTaxonomy</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a23f946fa581f128b28ad7a475f2c1c89</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IReportingTaxonomyMutableObject &gt;</type>
      <name>GetMutableReportingTaxonomyObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>ae17972170fd7b6f36e8db2507eb06706</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IStructureSetMutableObject</type>
      <name>GetMutableStructureSet</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a82a978ca72037089bd3056d3064b0233</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IStructureSetMutableObject &gt;</type>
      <name>GetMutableStructureSetObjects</name>
      <anchorfile>a00012.html</anchorfile>
      <anchor>a40467baf53b12aa4ae518aea3b7942fe</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::AuthMutableRetrievalManagerFactory</name>
    <filename>a00013.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Factory::IAuthMutableRetrievalManagerFactory</base>
    <member kind="function">
      <type></type>
      <name>AuthMutableRetrievalManagerFactory</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>acc0e68418179fb50c658884c01d80c52</anchor>
      <arglist>(Func&lt; object, ISdmxMutableObjectRetrievalManager, IAuthSdmxMutableObjectRetrievalManager &gt; factoryMethod)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>AuthMutableRetrievalManagerFactory</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>a513061c1860e62463b8f62067ee87fdf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>IAuthSdmxMutableObjectRetrievalManager</type>
      <name>GetRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00013.html</anchorfile>
      <anchor>a55361f2bde312e94f914104ece4a4bc4</anchor>
      <arglist>(T settings, ISdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::AuthRetrievalManagerBase</name>
    <filename>a00014.html</filename>
    <member kind="function" virtualness="pure">
      <type>abstract IAgencySchemeMutableObject</type>
      <name>GetMutableAgencyScheme</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a15c0b3f87ae1143df65045fb755c860b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IAgencySchemeMutableObject &gt;</type>
      <name>GetMutableAgencySchemeObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>aa75542d16797b04131342f9ce37e1983</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ICategorisationMutableObject</type>
      <name>GetMutableCategorisation</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>af0f37410c0b689097d720545890b82ae</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; ICategorisationMutableObject &gt;</type>
      <name>GetMutableCategorisationObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a517f510936f40638ad6b0e5607d8dd5e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ICategorySchemeMutableObject</type>
      <name>GetMutableCategoryScheme</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>ad675acc4c70339811e3363521bca69d4</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; ICategorySchemeMutableObject &gt;</type>
      <name>GetMutableCategorySchemeObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a2f5ed9b75bd3e519a3b09548f03157bb</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ICodelistMutableObject</type>
      <name>GetMutableCodelist</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a0c65ce794f0e8d3353200c4b4b629df7</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>af875a56b9851bff818019698dff83301</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IConceptSchemeMutableObject</type>
      <name>GetMutableConceptScheme</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>ae7036e14af481afdd83d220dbb7ab8fe</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IConceptSchemeMutableObject &gt;</type>
      <name>GetMutableConceptSchemeObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>ab8055c26b19fd784171cde12d78d93c0</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IContentConstraintMutableObject</type>
      <name>GetMutableContentConstraint</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a4a1ce7ca2d70e4296f7d8e18ee10c6cd</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IContentConstraintMutableObject &gt;</type>
      <name>GetMutableContentConstraintObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a97994c2519a9f12788b2d2bb35236c58</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IDataConsumerSchemeMutableObject</type>
      <name>GetMutableDataConsumerScheme</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a148a018fbaa37df190ba0c4faeedfd47</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IDataConsumerSchemeMutableObject &gt;</type>
      <name>GetMutableDataConsumerSchemeObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a3404b34217e6d9af6a1eec5f61f23d9d</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IDataProviderSchemeMutableObject</type>
      <name>GetMutableDataProviderScheme</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a3fb442b62f566e7927c1778ffdea779b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IDataProviderSchemeMutableObject &gt;</type>
      <name>GetMutableDataProviderSchemeObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a3dcaea6e36e50ab30596381142451834</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IDataStructureMutableObject</type>
      <name>GetMutableDataStructure</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>ad3ea42961af6ad3f0257556814d26788</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IDataStructureMutableObject &gt;</type>
      <name>GetMutableDataStructureObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a9b67670185c81806f1e9d2ed5f5a255f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IDataflowMutableObject</type>
      <name>GetMutableDataflow</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a6692cb7b6f31668dc11fa1d91a335540</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IDataflowMutableObject &gt;</type>
      <name>GetMutableDataflowObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a886b931bdfd7495927fa36219af1914f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IHierarchicalCodelistMutableObject</type>
      <name>GetMutableHierarchicCodeList</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>ada1b572b6814e80a26354ffbb46327b4</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IHierarchicalCodelistMutableObject &gt;</type>
      <name>GetMutableHierarchicCodeListObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>ac6f3cc81663046ddaff52171304a437b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>IMaintainableMutableObject</type>
      <name>GetMutableMaintainable</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a68a717563fda44530e404d098d8cd177</anchor>
      <arglist>(IStructureReference query, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMaintainableMutableObject &gt;</type>
      <name>GetMutableMaintainables</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a35ce293ee79634800b244cd0d061f50a</anchor>
      <arglist>(IStructureReference query, bool returnLatest, bool returnStub, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IMetadataStructureDefinitionMutableObject</type>
      <name>GetMutableMetadataStructure</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a2b56bd521fab7a4735e43b23f89c9c22</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IMetadataStructureDefinitionMutableObject &gt;</type>
      <name>GetMutableMetadataStructureObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>ae3fd8fbc32e2b1552b8b08cb55c83f1b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IMetadataFlowMutableObject</type>
      <name>GetMutableMetadataflow</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>abf9843612c91d41f3a2683771a680efb</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IMetadataFlowMutableObject &gt;</type>
      <name>GetMutableMetadataflowObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a5ca082113bb7903a760bdab92b0c1579</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IOrganisationUnitSchemeMutableObject</type>
      <name>GetMutableOrganisationUnitScheme</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>ae27522c08729d6f144e97348bf0ee7a2</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IOrganisationUnitSchemeMutableObject &gt;</type>
      <name>GetMutableOrganisationUnitSchemeObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a469eeb0af4fd1f20a699283a55fa4398</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IProcessMutableObject</type>
      <name>GetMutableProcessObject</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>ae742411581f825863961c2bb0ec87b65</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IProcessMutableObject &gt;</type>
      <name>GetMutableProcessObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a88c598f931eda81863d5facabc474913</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IProvisionAgreementMutableObject</type>
      <name>GetMutableProvisionAgreement</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>aa22784b678e52452e4a94e18479ecc9a</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IProvisionAgreementMutableObject &gt;</type>
      <name>GetMutableProvisionAgreementObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>ae092b423cf7b8abcef6f119f8f2a78b4</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IReportingTaxonomyMutableObject</type>
      <name>GetMutableReportingTaxonomy</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a1037d1ece45b3477edad44aea7ede6fa</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IReportingTaxonomyMutableObject &gt;</type>
      <name>GetMutableReportingTaxonomyObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a53c7d1bdf2540f4bcd42bd340f920417</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IStructureSetMutableObject</type>
      <name>GetMutableStructureSet</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a31e323920ba8e7ac4bdca6bb355501e8</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IStructureSetMutableObject &gt;</type>
      <name>GetMutableStructureSetObjects</name>
      <anchorfile>a00014.html</anchorfile>
      <anchor>a6332d68509157d42e981b919df4e7517</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::CachedRetrievalManager</name>
    <filename>a00015.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Manager::RetrievalManagerBase</base>
    <member kind="function">
      <type></type>
      <name>CachedRetrievalManager</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a77c417d5ff3b5f96553aabc09fd66f11</anchor>
      <arglist>(IEnumerable&lt; IMaintainableMutableObject &gt; maintainableMutableObjects, ISdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
    <member kind="function">
      <type>override IAgencySchemeMutableObject</type>
      <name>GetMutableAgencyScheme</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a82a9143d4bb72ac3594c19c50195642a</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IAgencySchemeMutableObject &gt;</type>
      <name>GetMutableAgencySchemeObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a37abed8273db6f2bdc05c87be5d65492</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ICategorisationMutableObject</type>
      <name>GetMutableCategorisation</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a52264481497a4a9a220736f01e4d04ff</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICategorisationMutableObject &gt;</type>
      <name>GetMutableCategorisationObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>af55c6c55db583f188e2c70cea660316d</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ICategorySchemeMutableObject</type>
      <name>GetMutableCategoryScheme</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a6728a684c8af29bdb042289bba40e78f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICategorySchemeMutableObject &gt;</type>
      <name>GetMutableCategorySchemeObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a91114ba4615d40f7479047032f1d7327</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ICodelistMutableObject</type>
      <name>GetMutableCodelist</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a3c423220b82a81b26f618a438d25b6bc</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>ada7b8d79747461b57d445d3692b2bac0</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IConceptSchemeMutableObject</type>
      <name>GetMutableConceptScheme</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a0087e15004df2e8bf560c30ee737a69b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IConceptSchemeMutableObject &gt;</type>
      <name>GetMutableConceptSchemeObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>ab6a89f641481ee807047efe38063c6be</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IContentConstraintMutableObject</type>
      <name>GetMutableContentConstraint</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>abbee7aa18b3f1971d88ce94be167ecc8</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IContentConstraintMutableObject &gt;</type>
      <name>GetMutableContentConstraintObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a127e40f35b04488510d1c6ac3f38a87a</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataConsumerSchemeMutableObject</type>
      <name>GetMutableDataConsumerScheme</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a0fc49e198b3075c303ca67acecb4e199</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataConsumerSchemeMutableObject &gt;</type>
      <name>GetMutableDataConsumerSchemeObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a0341cad154079e6a696f56705926cb0b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataProviderSchemeMutableObject</type>
      <name>GetMutableDataProviderScheme</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a0af8fe4e53bffe0212ea9c838e3612d0</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataProviderSchemeMutableObject &gt;</type>
      <name>GetMutableDataProviderSchemeObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>aa191c7108a15ec4f708d7e7df473a762</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataStructureMutableObject</type>
      <name>GetMutableDataStructure</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a0650e3801cf7af8a9989fcde2c56bca9</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataStructureMutableObject &gt;</type>
      <name>GetMutableDataStructureObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a6449bbfafa474fd4555cc6a592c21b53</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataflowMutableObject</type>
      <name>GetMutableDataflow</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>aa44e97f76ef5e2afb5940a4750e92828</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataflowMutableObject &gt;</type>
      <name>GetMutableDataflowObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a216e23c4213454f6999fe45a77163de7</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IHierarchicalCodelistMutableObject</type>
      <name>GetMutableHierarchicCodeList</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>aad122f7f4f52ea88f2032043cf11e832</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IHierarchicalCodelistMutableObject &gt;</type>
      <name>GetMutableHierarchicCodeListObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a6623851aa485e1489d2ac249ee0299f4</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IMetadataStructureDefinitionMutableObject</type>
      <name>GetMutableMetadataStructure</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a80c550953d8eadfbb54824e13ad4e7b4</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IMetadataStructureDefinitionMutableObject &gt;</type>
      <name>GetMutableMetadataStructureObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a58cd385cb5aabbbd524337adaf6d3b56</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IMetadataFlowMutableObject</type>
      <name>GetMutableMetadataflow</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>abe77f20e987a78b61a7c74acc2aa2a92</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IMetadataFlowMutableObject &gt;</type>
      <name>GetMutableMetadataflowObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>aa5739ee478804974166e38666f16d033</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IOrganisationUnitSchemeMutableObject</type>
      <name>GetMutableOrganisationUnitScheme</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>afef8c43634c0ee3630a82067e3d75028</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IOrganisationUnitSchemeMutableObject &gt;</type>
      <name>GetMutableOrganisationUnitSchemeObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a473442665b271fbfedfe1a76286d7200</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IProcessMutableObject</type>
      <name>GetMutableProcessObject</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a0363d84be98a297a2ebb6697fbc9ce95</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IProcessMutableObject &gt;</type>
      <name>GetMutableProcessObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a70e40dc7858d964fcddbc74b9edd9975</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IProvisionAgreementMutableObject</type>
      <name>GetMutableProvisionAgreement</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a5ed1b4f3e734c3100f76fc8109c10836</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IProvisionAgreementMutableObject &gt;</type>
      <name>GetMutableProvisionAgreementObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>aaa6aa41f005509108a28632f2df1b442</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IReportingTaxonomyMutableObject</type>
      <name>GetMutableReportingTaxonomy</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>ac0213b66d899ca6563d2167ad4172b98</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IReportingTaxonomyMutableObject &gt;</type>
      <name>GetMutableReportingTaxonomyObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a72f59053ff6755165a7ec91de0a4860f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IStructureSetMutableObject</type>
      <name>GetMutableStructureSet</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>af3b30252e693a52f242c9b2578ca8b60</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IStructureSetMutableObject &gt;</type>
      <name>GetMutableStructureSetObjects</name>
      <anchorfile>a00015.html</anchorfile>
      <anchor>a94e27b67973477a76fb5301ad645edc9</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CategoryEntity</name>
    <filename>a00016.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ItemEntity</base>
    <member kind="function">
      <type></type>
      <name>CategoryEntity</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>a6c6fc0b9efd201f75eb9d27c390b4d1f</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; CategoryEntity &gt;</type>
      <name>Categories</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>a89d5ccc1598550da42223f720149abd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; DataflowEntity &gt;</type>
      <name>Dataflows</name>
      <anchorfile>a00016.html</anchorfile>
      <anchor>a3714799ff4e97bcfeac2a17c285083ae</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CategorySchemeEntity</name>
    <filename>a00017.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ArtefactEntity</base>
    <member kind="function">
      <type></type>
      <name>CategorySchemeEntity</name>
      <anchorfile>a00017.html</anchorfile>
      <anchor>a443b57a0cc77058bbc08ff8be3ff98cb</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; CategoryEntity &gt;</type>
      <name>Categories</name>
      <anchorfile>a00017.html</anchorfile>
      <anchor>ac73d63d6ad5fd33e731112b76809c5ef</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CodeCollection</name>
    <filename>a00018.html</filename>
    <member kind="function">
      <type></type>
      <name>CodeCollection</name>
      <anchorfile>a00018.html</anchorfile>
      <anchor>a0558f0c597857c2f63fc3f43210da1d9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CodeCollection</name>
      <anchorfile>a00018.html</anchorfile>
      <anchor>af11a3f94a992daee495adfdb1065d3d0</anchor>
      <arglist>(IList&lt; string &gt; list)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CodeListEntity</name>
    <filename>a00019.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ArtefactEntity</base>
    <member kind="function">
      <type></type>
      <name>CodeListEntity</name>
      <anchorfile>a00019.html</anchorfile>
      <anchor>adcf3a352fff8f881da9599ca2990188b</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; string &gt;</type>
      <name>CodeList</name>
      <anchorfile>a00019.html</anchorfile>
      <anchor>ae42fb69127c56350274cee53c2aaaad0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CodeSetCollection</name>
    <filename>a00020.html</filename>
    <member kind="function">
      <type></type>
      <name>CodeSetCollection</name>
      <anchorfile>a00020.html</anchorfile>
      <anchor>a72ddf5d0d16c79554952b348059cadc1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CodeSetCollection</name>
      <anchorfile>a00020.html</anchorfile>
      <anchor>abb7d41e7ba0464253089da18874ae058</anchor>
      <arglist>(IList&lt; CodeCollection &gt; list)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ComponentMapping::ColumnOrdinal</name>
    <filename>a00021.html</filename>
    <member kind="property">
      <type>int</type>
      <name>ColumnPosition</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>a1a609cdb389c40fda725b58b5a7641b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>DataSetColumnEntity</type>
      <name>Key</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>a9c61a1d33dc71c4bd0424494d6d6f8e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>int</type>
      <name>Value</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>a1313745e1d0d8a240781ed1df0c277b2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ComponentEntity</name>
    <filename>a00022.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function">
      <type></type>
      <name>ComponentEntity</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>a7f44169eedbbd89a7da6016a05887dee</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>AddAttAssignmentGroups</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>aab962ff26c530a6253c3d68a12886132</anchor>
      <arglist>(GroupEntity group)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>SetAssignmentStatus</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>ae4089d64502b65f85da60b50b128555c</anchor>
      <arglist>(string status)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>SetAttachmentLevel</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>a92ca7a8e29a68bf587e605636426b5dc</anchor>
      <arglist>(string level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>SetType</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>abaf0390849d43ad15f0d7d01f02e6270</anchor>
      <arglist>(string type)</arglist>
    </member>
    <member kind="property">
      <type>IList&lt; string &gt;</type>
      <name>AttAttachmentMeasures</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>af9f91109f7a5dbf15e9615cbc45e0458</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; GroupEntity &gt;</type>
      <name>AttAssignmentGroups</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>a47969bc8509cf803b45fbdac1700e55d</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>AssignmentStatus</type>
      <name>AttStatus</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>ac62a8b016483350b83e237e69021eeb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>AttTimeFormat</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>a6c855275e282d126a18afa362df0885b</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>AttachmentLevel</type>
      <name>AttributeAttachmentLevel</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>a2ab4aa6d197bb9ed9c63e4451078cdec</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>CodeListEntity</type>
      <name>CodeList</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>a0e346905c2371a4e17a23d8b92c7c522</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>SdmxComponentType</type>
      <name>ComponentType</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>a95abd0cd4c49e152c3ebe8fc1214c267</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>ConceptEntity</type>
      <name>Concept</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>aab5a43d75f93e14148a7637cbe102c0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Id</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>a2190707bc845770a3604d6fee50bab49</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>CrossSectionalLevels</type>
      <name>CrossSectionalAttachmentLevel</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>acf06e09512489e5865d92a73a1a1b145</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>CrossSectionalLevelDataSet</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>ac63eb97680075593f171aa5c4d84464e</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>CrossSectionalLevelGroup</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>ac622e557db1a243f003b29ceeafe8f87</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>CrossSectionalLevelObs</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>adcd32127b097066d98164b77b8de53bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>CrossSectionalLevelSection</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>ad1a591d767f5c138101ac133e3fb2783</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>CrossSectionalMeasureCode</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>adb5f13f267c2f979fbcdd4cecaaff551</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>CrossSectionalLevels</type>
      <name>EffectiveCrossSectionalAttachmentLevel</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>a4ffbf8cf96087a6bffb8c39fb6e8c2bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>FrequencyDimension</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>a615eaa386a0d609465872d83a9146bf8</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>MeasureDimension</name>
      <anchorfile>a00022.html</anchorfile>
      <anchor>ab34d9f8498ab5d4ce47ce4d9f10f2110</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ComponentMapping</name>
    <filename>a00023.html</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ComponentMapping::ColumnOrdinal</class>
    <member kind="function" static="yes">
      <type>static IComponentMapping</type>
      <name>CreateComponentMapping</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>a2f9f011cdc6ca9fb899cc4a01dc5da93</anchor>
      <arglist>(ComponentEntity component, MappingEntity mapping)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>BuildOrdinals</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>ac942a6f2dd7094f0e6201cb44b5460c2</anchor>
      <arglist>(IDataReader reader)</arglist>
    </member>
    <member kind="function" protection="protected" static="yes">
      <type>static string</type>
      <name>EscapeString</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>ae0017050c934d5907a97127fd36db9df</anchor>
      <arglist>(string input)</arglist>
    </member>
    <member kind="function" protection="protected" static="yes">
      <type>static string</type>
      <name>GetColumnName</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>a4cb6702aff47809c503376d5f59a31f6</anchor>
      <arglist>(MappingEntity mapping, long dateColumnSysId)</arglist>
    </member>
    <member kind="function" protection="protected" static="yes">
      <type>static string</type>
      <name>SqlOperatorComponent</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>a9b134c618972a84ecac7a03c696bec6d</anchor>
      <arglist>(string mappedId, string mappedValue, string operatorValue)</arglist>
    </member>
    <member kind="property">
      <type>ComponentEntity</type>
      <name>Component</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>a19a74b90ea9f0708da6f4879ac292ea7</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>MappingEntity</type>
      <name>Mapping</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>a174b1280d30f015fc17524ac10f89a39</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" protection="protected">
      <type>IList&lt; ColumnOrdinal &gt;</type>
      <name>ColumnOrdinals</name>
      <anchorfile>a00023.html</anchorfile>
      <anchor>a8ff952436b1b0927d77c173d0894df57</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ConceptEntity</name>
    <filename>a00024.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ItemEntity</base>
    <member kind="function">
      <type></type>
      <name>ConceptEntity</name>
      <anchorfile>a00024.html</anchorfile>
      <anchor>a35c6c91cc6f8e61d45ac150400446993</anchor>
      <arglist>(long sysId)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Config::ConfigManager</name>
    <filename>a00025.html</filename>
    <member kind="property" static="yes">
      <type>static MappingStoreConfigSection</type>
      <name>Config</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>a993d575df53029adc6ffed10e4f9049e</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" static="yes">
      <type>static ConfigManager</type>
      <name>Instance</name>
      <anchorfile>a00025.html</anchorfile>
      <anchor>a61cf96c59783a91b761fd5913a8a46b5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ConnectionEntity</name>
    <filename>a00026.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function">
      <type></type>
      <name>ConnectionEntity</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>ac2e23959646a11c2846dedd01364b530</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>DBOwner</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>a87afdcb03923edf415dd808931c5d3f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>AdoConnectionString</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>a658116f5452e71735ea864f535a98834</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>DBName</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>ae2faf9216a0f2c803eaca17227929a9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>DBPassword</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>a8792f5710765ae01270a7b8e723559d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>int</type>
      <name>DBPort</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>afee97e30d42b6d8dfd90b0120ccad1ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>DBServer</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>a3e9adf57c1496eea25902c47bc96f3bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>DBType</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>ae843e89899b74e02b794bf6c9a118e5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>DBUser</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>a90c9ffd96459d4257725cc6fcdca6dec</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>JdbcConnectionString</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>a28fbc2d28810a08e03292ca7ac80dce9</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Name</name>
      <anchorfile>a00026.html</anchorfile>
      <anchor>a9713cc72a9fb4094a900a4bebb499364</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Helper::ConnectionStringHelper</name>
    <filename>a00027.html</filename>
    <member kind="function">
      <type>void</type>
      <name>Save</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>a2c8f76e6ccf6a79ff02bc7f43ff9e3a7</anchor>
      <arglist>(string connectionString, string providerName)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>HasConnectionString</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>a8194648b31bbbd5ecbae84e7570bd0cc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>ConnectionStringName</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>a9f0388d86ada7066d597dc2061771b96</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" static="yes">
      <type>static ConnectionStringHelper</type>
      <name>Instance</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>a7641049a1668d774642d0ea6778ccfb2</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>ConnectionStringSettings</type>
      <name>MappingStoreConnectionStringSettings</name>
      <anchorfile>a00027.html</anchorfile>
      <anchor>a0fa0d1985b45bc9a4fbea0ae447b0d15</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::CrossMutableRetrievalManagerFactory</name>
    <filename>a00028.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Factory::ICrossRetrievalManagerFactory</base>
    <member kind="function">
      <type></type>
      <name>CrossMutableRetrievalManagerFactory</name>
      <anchorfile>a00028.html</anchorfile>
      <anchor>ad9f5ac8022b088e4ba360d1ef4b65476</anchor>
      <arglist>(Func&lt; object, ISdmxMutableObjectRetrievalManager, ICrossReferenceMutableRetrievalManager &gt; factoryMethod)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CrossMutableRetrievalManagerFactory</name>
      <anchorfile>a00028.html</anchorfile>
      <anchor>a4f015ec57d46411ddfe06aea782c806f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ICrossReferenceMutableRetrievalManager</type>
      <name>GetCrossRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00028.html</anchorfile>
      <anchor>a22f7c36ccf7c1cb4224f2f7d1d1fa616</anchor>
      <arglist>(T settings, ISdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Builder::CrossReferenceChildBuilder</name>
    <filename>a00029.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Builder::ICrossReferenceSetBuilder</base>
    <member kind="function">
      <type>ISet&lt; IStructureReference &gt;</type>
      <name>Build</name>
      <anchorfile>a00029.html</anchorfile>
      <anchor>a4fc8c5e098787354a8b6bbaeaa7d41a2</anchor>
      <arglist>(IIdentifiableMutableObject identifiable)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::CrossReferenceResolverMutableEngine</name>
    <filename>a00030.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Engine::ICrossReferenceResolverMutableEngine</base>
    <member kind="function">
      <type></type>
      <name>CrossReferenceResolverMutableEngine</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>a48cd7a4cebfbfa4705ecdc585519421f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CrossReferenceResolverMutableEngine</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>a47ee231d137933949ba868bf024b3cf3</anchor>
      <arglist>(IEnumerable&lt; IMaintainableMutableObject &gt; objects, params SdmxStructureType[] sdmxStructureTypes)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CrossReferenceResolverMutableEngine</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>a0ea7006ec54174c9c5d1026cf775a933</anchor>
      <arglist>(IEnumerable&lt; SdmxStructureType &gt; structures)</arglist>
    </member>
    <member kind="function">
      <type>MaintainableReferenceDictionary</type>
      <name>GetMissingCrossReferences</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>aa38722373fd23225a87016f66a947fb1</anchor>
      <arglist>(IMutableObjects beans, int numberLevelsDeep, Func&lt; IStructureReference, IMaintainableMutableObject &gt; retrievalManager)</arglist>
    </member>
    <member kind="function">
      <type>MaintainableDictionary&lt; IMaintainableMutableObject &gt;</type>
      <name>ResolveReferences</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>a81f9e3396dd6a94053d1e7a6d00c4af1</anchor>
      <arglist>(IMutableObjects beans, int numberLevelsDeep, Func&lt; IStructureReference, IMaintainableMutableObject &gt; retrievalManager)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMaintainableMutableObject &gt;</type>
      <name>ResolveReferences</name>
      <anchorfile>a00030.html</anchorfile>
      <anchor>a3366eded1cf5b7ba9b204afa4eb2453a</anchor>
      <arglist>(IMaintainableMutableObject artefact, int numberLevelsDeep, Func&lt; IStructureReference, IMaintainableMutableObject &gt; retrievalManager)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::CrossReferenceRetrievalManager</name>
    <filename>a00031.html</filename>
    <member kind="function">
      <type></type>
      <name>CrossReferenceRetrievalManager</name>
      <anchorfile>a00031.html</anchorfile>
      <anchor>a8fabf9180cf6130b7641501dac413209</anchor>
      <arglist>(ISdmxMutableObjectRetrievalManager retrievalManager, ConnectionStringSettings connectionStringSettings)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CrossReferenceRetrievalManager</name>
      <anchorfile>a00031.html</anchorfile>
      <anchor>a1f62055f48948a6ac08bd670b161609a</anchor>
      <arglist>(ISdmxMutableObjectRetrievalManager retrievalManager, Database mappingStoreDatabase)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CrossReferenceRetrievalManager</name>
      <anchorfile>a00031.html</anchorfile>
      <anchor>aac3de77d67abf628ac8a935e5a66c909</anchor>
      <arglist>(IAuthCrossReferenceMutableRetrievalManager authCrossReferenceManager)</arglist>
    </member>
    <member kind="function">
      <type>IMutableCrossReferencingTree</type>
      <name>GetCrossReferenceTree</name>
      <anchorfile>a00031.html</anchorfile>
      <anchor>af834e8508c5fcd7229e6ae47ebb9b88f</anchor>
      <arglist>(IMaintainableMutableObject maintainableObject)</arglist>
    </member>
    <member kind="function">
      <type>IList&lt; IMaintainableMutableObject &gt;</type>
      <name>GetCrossReferencedStructures</name>
      <anchorfile>a00031.html</anchorfile>
      <anchor>af59ac862e6d65b2d5bef5e9171b54556</anchor>
      <arglist>(IStructureReference structureReference, bool returnStub, params SdmxStructureType[] structures)</arglist>
    </member>
    <member kind="function">
      <type>IList&lt; IMaintainableMutableObject &gt;</type>
      <name>GetCrossReferencedStructures</name>
      <anchorfile>a00031.html</anchorfile>
      <anchor>a8a4e797a190fa108fb933c31d99ceadd</anchor>
      <arglist>(IIdentifiableMutableObject identifiable, bool returnStub, params SdmxStructureType[] structures)</arglist>
    </member>
    <member kind="function">
      <type>IList&lt; IMaintainableMutableObject &gt;</type>
      <name>GetCrossReferencingStructures</name>
      <anchorfile>a00031.html</anchorfile>
      <anchor>a0ff182689c3376ef9756c7b8d69874e5</anchor>
      <arglist>(IStructureReference structureReference, bool returnStub, params SdmxStructureType[] structures)</arglist>
    </member>
    <member kind="function">
      <type>IList&lt; IMaintainableMutableObject &gt;</type>
      <name>GetCrossReferencingStructures</name>
      <anchorfile>a00031.html</anchorfile>
      <anchor>a2cc594cef0fc638fded0e6eab28b68cb</anchor>
      <arglist>(IIdentifiableMutableObject identifiable, bool returnStub, params SdmxStructureType[] structures)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::Database</name>
    <filename>a00032.html</filename>
    <member kind="function">
      <type></type>
      <name>Database</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a3eccb5b70b1af27400d30d4accf8534c</anchor>
      <arglist>(Database database, DbTransaction transaction)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Database</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>ac0fdb37b4cdcd983fd0ef4065b656c29</anchor>
      <arglist>(ConnectionStringSettings connectionStringSettings)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CancelSafe</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a94ee2b1c322af726cd2542cd29439eba</anchor>
      <arglist>(DbCommand command)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>AddInParameter</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a933381c7a9925e1ebd84bfad28fb0ac6</anchor>
      <arglist>(DbCommand command, string name, DbType dbType, object value)</arglist>
    </member>
    <member kind="function">
      <type>string</type>
      <name>BuildParameterName</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a32bad6171dbafbf1d55c133090c8cd5c</anchor>
      <arglist>(string name)</arglist>
    </member>
    <member kind="function">
      <type>string</type>
      <name>BuildQuery</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>aaa3d89109dd86357bb797977405411ab</anchor>
      <arglist>(string queryFormat, params DbParameter[] parameters)</arglist>
    </member>
    <member kind="function">
      <type>DbCommand</type>
      <name>CreateCommand</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a24e87c1a400d4226ad2ba71e5cc1431f</anchor>
      <arglist>(CommandType commandType, string commandText)</arglist>
    </member>
    <member kind="function">
      <type>DbConnection</type>
      <name>CreateConnection</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a97f85c35eeb4fab76d751bd551f992ea</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>DbParameter</type>
      <name>CreateInParameter</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>acd8db57b07d00ddf14e7715ef52ea59f</anchor>
      <arglist>(string name, DbType dbType)</arglist>
    </member>
    <member kind="function">
      <type>DbParameter</type>
      <name>CreateInParameter</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>aa786f599381239eedc2ec23060d10cb1</anchor>
      <arglist>(string name, DbType dbType, object value)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>ExecuteNonQuery</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>aff927d6a0c713a042a186f3633c62bba</anchor>
      <arglist>(string sqlStatement, IList&lt; DbParameter &gt; parameters)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>ExecuteNonQuery</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a2fb7eeb8413688e74b85186e2eb9862a</anchor>
      <arglist>(string sqlStatement, DbTransaction transaction, IList&lt; DbParameter &gt; parameters)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>ExecuteNonQueryFormat</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a0ab8ee7d37b5e148e0ea38f2f1e0b87b</anchor>
      <arglist>(string sqlStatement, DbTransaction transaction, params DbParameter[] parameters)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>ExecuteNonQueryFormat</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>ad4bdead54fa8d65172fae5d82e359a24</anchor>
      <arglist>(string sqlStatement, params DbParameter[] parameters)</arglist>
    </member>
    <member kind="function">
      <type>IDataReader</type>
      <name>ExecuteReader</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a53437fd7a9bd8feba10064cfbd359a37</anchor>
      <arglist>(DbCommand command)</arglist>
    </member>
    <member kind="function">
      <type>object</type>
      <name>ExecuteScalar</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a72ef63871df13b581d4e41f0ebab00a0</anchor>
      <arglist>(string query, IList&lt; DbParameter &gt; parameters)</arglist>
    </member>
    <member kind="function">
      <type>object</type>
      <name>ExecuteScalar</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>ae183314728b485f20ec0993de4b89510</anchor>
      <arglist>(string query, DbTransaction transaction, IList&lt; DbParameter &gt; parameters)</arglist>
    </member>
    <member kind="function">
      <type>object</type>
      <name>ExecuteScalarFormat</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>ac92fe048ee4187baab1aeebfc0fcd586</anchor>
      <arglist>(string query, params DbParameter[] parameters)</arglist>
    </member>
    <member kind="function">
      <type>object</type>
      <name>ExecuteScalarFormat</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>acac850b2f71e82a5df8dd46c01e50679</anchor>
      <arglist>(string query, DbTransaction transaction, params DbParameter[] parameters)</arglist>
    </member>
    <member kind="function">
      <type>DbCommand</type>
      <name>GetSqlStringCommand</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a499dc87a7725c2f8406e476a876be71b</anchor>
      <arglist>(string query)</arglist>
    </member>
    <member kind="function">
      <type>DbCommand</type>
      <name>GetSqlStringCommand</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>ac42b4b5cc3974d5d82bb503cc1b7c528</anchor>
      <arglist>(string query, IList&lt; DbParameter &gt; parameters)</arglist>
    </member>
    <member kind="function">
      <type>DbCommand</type>
      <name>GetSqlStringCommandFormat</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a1cf0dafc824f5170c53758b6e5f65591</anchor>
      <arglist>(string queryFormat, params DbParameter[] parameters)</arglist>
    </member>
    <member kind="function">
      <type>DbCommand</type>
      <name>GetSqlStringCommandParam</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a0b17076064cb0870b077b9b930abd4cc</anchor>
      <arglist>(string query, params DbParameter[] parameters)</arglist>
    </member>
    <member kind="function">
      <type>string</type>
      <name>NormalizeQuerySchema</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a6b9b10122e36847b28515bd6b343de6a</anchor>
      <arglist>(string query)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static IDataReader</type>
      <name>ExecuteReader</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>a2349e53f94796ae0381499ffac94e327</anchor>
      <arglist>(DbConnection connection, DbCommand command)</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>ProviderName</name>
      <anchorfile>a00032.html</anchorfile>
      <anchor>aa6fa16059b8c6681ae19bf302929d9bf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Extensions::DatabaseExtension</name>
    <filename>a00033.html</filename>
    <member kind="function" static="yes">
      <type>static object</type>
      <name>ToDbValue</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>a36dadffe1f14e9644e05233e06096444</anchor>
      <arglist>(this long value)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static object</type>
      <name>ToDbValue</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>ad6f1690014c33cb89bb7be6e8a6d5c27</anchor>
      <arglist>(this long value, object defaultValue)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static object</type>
      <name>ToDbValue&lt; T &gt;</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>aba4100131775561bf7715b4158d6b907</anchor>
      <arglist>(this T?value)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static object</type>
      <name>ToDbValue&lt; T &gt;</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>a9893e3a44a0fdbf5de10e400126bfeb3</anchor>
      <arglist>(this T?value, object defaultValue)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>HasFieldName</name>
      <anchorfile>a00033.html</anchorfile>
      <anchor>a78e32c80ec5a1e89839f1aeb50b4d0ad</anchor>
      <arglist>(this IDataReader reader, string fieldName)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Config::DatabaseSetting</name>
    <filename>a00034.html</filename>
    <member kind="function">
      <type></type>
      <name>DatabaseSetting</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>a04e641f7197c183ca66a0613642f5f61</anchor>
      <arglist>(string providerName)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DatabaseSetting</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>a378f9df8f8a1b84b8daa5386ac076528</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>CastToString</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>a6d4018fb8d4121afcbd2ac577b544f74</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>DateCast</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>afab45937f286e236b61598531f14b9fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Provider</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>a07e3745febbe808179036878dea0e49d</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>SubstringCommand</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>aa7547101411a358a91ce8ec8a9bed536</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>SubstringCommandRequiresLength</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>ae47edc3e23811b0489517541e17ffe20</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>SubQueryOrderByAllowed</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>a1a9c0424d4a3fb3ef94a9b099e284757</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>ParameterMarkerFormat</name>
      <anchorfile>a00034.html</anchorfile>
      <anchor>ad4909e67f39bbbbf6342cbe17fe0ed31</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Config::DatabaseSettingCollection</name>
    <filename>a00035.html</filename>
    <member kind="function">
      <type></type>
      <name>DatabaseSettingCollection</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a656586c36e63d8a2e995400f42a08496</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Add</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a62e31ffa120de46cb0a1dc0a7d4ed4ae</anchor>
      <arglist>(DatabaseSetting item)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Clear</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a9d1ac9a9a968d0895ff0f5c0b372e019</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>Contains</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>aac3da7d32aa3cdabd5b1fd2f24d4f548</anchor>
      <arglist>(DatabaseSetting item)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CopyTo</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a73a7bf6d0b406b64f8a1f2552ee1122d</anchor>
      <arglist>(DatabaseSetting[] array, int arrayIndex)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>Remove</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>aa5aaa1298fcc87a2a5a759dcf8ba4faa</anchor>
      <arglist>(DatabaseSetting item)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Remove</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a30e02d038552c666a0a757283b7404e7</anchor>
      <arglist>(string name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>RemoveAt</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a2da9df83b7ea1dd1cd1e976e6b2484a5</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>override ConfigurationElement</type>
      <name>CreateNewElement</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a05b9b70c0e8aac9b3b33f72fd56afe1c</anchor>
      <arglist>(string elementName)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>override ConfigurationElement</type>
      <name>CreateNewElement</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a0160ca18ab3a8c6b4a2bf3666a93aaf3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>override object</type>
      <name>GetElementKey</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a31191d30922def2e79847dbc0a6a686b</anchor>
      <arglist>(ConfigurationElement element)</arglist>
    </member>
    <member kind="property">
      <type>new bool</type>
      <name>IsReadOnly</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a88b047d18043816e94c1335d4f8ca60d</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>DatabaseSetting</type>
      <name>this[int index]</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a1f597569d3b7783f872d8b5d07dd3597</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>new DatabaseSetting</type>
      <name>this[string name]</name>
      <anchorfile>a00035.html</anchorfile>
      <anchor>a4a9f1d1aeecdfe2730c631ffabe82ab8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Helper::DatabaseType</name>
    <filename>a00036.html</filename>
    <member kind="function" static="yes">
      <type>static string</type>
      <name>GetProviderName</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>acb95839ee0b91ead73dbe991f1a6eb05</anchor>
      <arglist>(string databaseType)</arglist>
    </member>
    <member kind="property" static="yes">
      <type>static DatabaseType</type>
      <name>Instance</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>a6ad06d5e434b14fc75ba4334958c6d06</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" static="yes">
      <type>static MastoreProviderMappingSettingCollection</type>
      <name>Mappings</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>aae7505870fce3cf7e56a30b6c9f6f9d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" static="yes">
      <type>static DatabaseSettingCollection</type>
      <name>DatabaseSettings</name>
      <anchorfile>a00036.html</anchorfile>
      <anchor>a4b2e5dda65a8f648110ae04fe1ce0799</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Config::DataflowConfigurationSection</name>
    <filename>a00037.html</filename>
    <member kind="function">
      <type>override bool</type>
      <name>IsReadOnly</name>
      <anchorfile>a00037.html</anchorfile>
      <anchor>a2daa9bbc1015d192cee5a951f72cebca</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>IgnoreProductionForData</name>
      <anchorfile>a00037.html</anchorfile>
      <anchor>ad69ea34f4d0586a680fc665bb8136027</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>IgnoreProductionForStructure</name>
      <anchorfile>a00037.html</anchorfile>
      <anchor>a8864420a8bc73aa89b8903540759627f</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>IgnoreExternalUsage</name>
      <anchorfile>a00037.html</anchorfile>
      <anchor>ac06983642308731086ac272871d386ae</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DataflowEntity</name>
    <filename>a00038.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ArtefactEntity</base>
    <member kind="function">
      <type></type>
      <name>DataflowEntity</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>a0daa5d0db500934c96ff7a77c0738fe0</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>DsdEntity</type>
      <name>Dsd</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>a34121b1b1aae9977d4b06017c98c66c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>MappingSetEntity</type>
      <name>MappingSet</name>
      <anchorfile>a00038.html</anchorfile>
      <anchor>a71d5309bbb51ebf6d18d2edf1eedbde4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Helper::DataReaderHelper</name>
    <filename>a00039.html</filename>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>GetBoolean</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>a196496964a136c6b266d6106665f632c</anchor>
      <arglist>(IDataRecord dataReader, string fieldName)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>GetBoolean</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>a38f7a719731f412d4cb1e12857049d6e</anchor>
      <arglist>(IDataRecord dataReader, int index)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static short</type>
      <name>GetInt16</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>ae64e362f8f2c3c0052d167cae66eeeb6</anchor>
      <arglist>(IDataRecord dataReader, string fieldName)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static short</type>
      <name>GetInt16</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>af3abdace0c987018549c8797c1b2eea0</anchor>
      <arglist>(IDataRecord dataReader, int index)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>GetInt32</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>a0404de323665a61077eefb26304940de</anchor>
      <arglist>(IDataRecord dataReader, string fieldName)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>GetInt32</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>aaff14cc015b88524f3e46f86cabcc624</anchor>
      <arglist>(IDataRecord dataReader, int index)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static long</type>
      <name>GetInt64</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>ab12408ce015e207a52d809ff39612026</anchor>
      <arglist>(IDataRecord dataReader, string fieldName)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static long</type>
      <name>GetInt64</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>aeb695343e5d94f9d266a7434accf1d33</anchor>
      <arglist>(IDataRecord dataReader, int index)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static string</type>
      <name>GetString</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>a0bd3ed9e762833790da3aa849e7e582c</anchor>
      <arglist>(IDataRecord dataReader, string fieldName)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static string</type>
      <name>GetString</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>a099b9a21b625037b714683cef1651eab</anchor>
      <arglist>(IDataRecord dataReader, int index)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static DateTime</type>
      <name>GetStringDate</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>a2bb646031a5ab42c5625a7f0dfaad7ce</anchor>
      <arglist>(IDataRecord dataReader, int ordinal)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static TertiaryBool</type>
      <name>GetTristate</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>a70deab20afcefae9d2157567eccbb735</anchor>
      <arglist>(IDataRecord dataReader, string fieldName)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static TertiaryBool</type>
      <name>GetTristate</name>
      <anchorfile>a00039.html</anchorfile>
      <anchor>a4f6de8e76fe4515c545dd10ea6040400</anchor>
      <arglist>(IDataRecord dataReader, int index)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DataSetColumnEntity</name>
    <filename>a00040.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function">
      <type></type>
      <name>DataSetColumnEntity</name>
      <anchorfile>a00040.html</anchorfile>
      <anchor>ae6b2bb425c390d1b238f6cfef90f62ea</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Description</name>
      <anchorfile>a00040.html</anchorfile>
      <anchor>a2a86e525f4405ca28c2892669048bbba</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Name</name>
      <anchorfile>a00040.html</anchorfile>
      <anchor>ab4f5500279afdec78a8cf64757616caa</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DataSetEntity</name>
    <filename>a00041.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function">
      <type></type>
      <name>DataSetEntity</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>a822be83d0e963796c913e8aeeaa45d67</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>ConnectionEntity</type>
      <name>Connection</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>aa84cbacdf2e1241cda9fd5318f2e0de8</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Description</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>a4d70aa904b9bff50b1cd228363a216c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Name</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>a78a13e34d991e6f89ad8fa7b7ee4cc63</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Query</name>
      <anchorfile>a00041.html</anchorfile>
      <anchor>a9308de01793dd3aa9839da63ef7802c9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Extensions::DbCommandExtension</name>
    <filename>a00042.html</filename>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>SafeCancel</name>
      <anchorfile>a00042.html</anchorfile>
      <anchor>aadd0f6801dde58a16452dc40c7b94175</anchor>
      <arglist>(this DbCommand command)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DsdEntity</name>
    <filename>a00043.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ArtefactEntity</base>
    <member kind="function">
      <type></type>
      <name>DsdEntity</name>
      <anchorfile>a00043.html</anchorfile>
      <anchor>ad4002fb06010ba744d399188446866a8</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>AddGroup</name>
      <anchorfile>a00043.html</anchorfile>
      <anchor>ac340d243943229d15cd0ab314e5a9fbf</anchor>
      <arglist>(GroupEntity group)</arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; ComponentEntity &gt;</type>
      <name>Attributes</name>
      <anchorfile>a00043.html</anchorfile>
      <anchor>a1e1ad32659b90eaf0f734bea6ad7cfbc</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; ComponentEntity &gt;</type>
      <name>CrossSectionalMeasures</name>
      <anchorfile>a00043.html</anchorfile>
      <anchor>a161be98258097c3531116aa54fd06a82</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; ComponentEntity &gt;</type>
      <name>Dimensions</name>
      <anchorfile>a00043.html</anchorfile>
      <anchor>a94c857fb4c2a6c9c1bf88d5f5f12153f</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; GroupEntity &gt;</type>
      <name>Groups</name>
      <anchorfile>a00043.html</anchorfile>
      <anchor>aca44ed798da2d53670ea1d136f51bc6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>ComponentEntity</type>
      <name>PrimaryMeasure</name>
      <anchorfile>a00043.html</anchorfile>
      <anchor>a1d1397a5a41e1e90a90e48cee3f42482</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>ComponentEntity</type>
      <name>TimeDimension</name>
      <anchorfile>a00043.html</anchorfile>
      <anchor>abe8d1f9d9fe6471d37e539d2bc64430b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::GroupEntity</name>
    <filename>a00044.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function">
      <type></type>
      <name>GroupEntity</name>
      <anchorfile>a00044.html</anchorfile>
      <anchor>a679656c4be1cfa6fb9f77e0b5d47e297</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>AddDimensions</name>
      <anchorfile>a00044.html</anchorfile>
      <anchor>adb4fbcaea0d007d105077b0837a591e6</anchor>
      <arglist>(ComponentEntity dimension)</arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; ComponentEntity &gt;</type>
      <name>Dimensions</name>
      <anchorfile>a00044.html</anchorfile>
      <anchor>a7d17d6bbf5276142aca73d8f8fcebc75</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Id</name>
      <anchorfile>a00044.html</anchorfile>
      <anchor>a97408369cfc81af56ff94deb7e4e6738</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Extensions::HeaderExtensions</name>
    <filename>a00045.html</filename>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>AddCoordinateType</name>
      <anchorfile>a00045.html</anchorfile>
      <anchor>a670f60120b07bab1a15b15a6baa435c8</anchor>
      <arglist>(this IContactMutableObject contact, string coordinateType, string coordinateValue)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::HeaderRetrieverEngine</name>
    <filename>a00046.html</filename>
    <member kind="function">
      <type></type>
      <name>HeaderRetrieverEngine</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>aabcd65a8aa01062d6806c040789e8416</anchor>
      <arglist>(ConnectionStringSettings settings)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>HeaderRetrieverEngine</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>a36fd8615cd328d8602df8f1711f6d619</anchor>
      <arglist>(Database mappingStoreDb)</arglist>
    </member>
    <member kind="function">
      <type>IHeader</type>
      <name>GetHeader</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>acbc428b41107a29c0fe86c89205d5590</anchor>
      <arglist>(IDataQuery dataQuery, DateTime?beginDate, DateTime?endDate)</arglist>
    </member>
    <member kind="function">
      <type>IHeader</type>
      <name>GetHeader</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>ada032bd775d67bf53c70c58f31858986</anchor>
      <arglist>(IDataflowObject dataflow, DateTime?beginDate, DateTime?endDate)</arglist>
    </member>
    <member kind="function">
      <type>IHeader</type>
      <name>GetHeader</name>
      <anchorfile>a00046.html</anchorfile>
      <anchor>a115950181ef69f0462fc266abf283637</anchor>
      <arglist>(DataflowEntity dataflow, DateTime?beginDate, DateTime?endDate)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::IAdvancedMutableRetrievalManagerFactory</name>
    <filename>a00047.html</filename>
    <member kind="function">
      <type>IAdvancedSdmxMutableObjectRetrievalManager</type>
      <name>GetRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00047.html</anchorfile>
      <anchor>aa35b33c44dd52f5665a5d7831a4901aa</anchor>
      <arglist>(T settings)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::IAuthAdvancedMutableRetrievalManagerFactory</name>
    <filename>a00048.html</filename>
    <member kind="function">
      <type>IAuthAdvancedSdmxMutableObjectRetrievalManager</type>
      <name>GetRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00048.html</anchorfile>
      <anchor>a2a652ab3e7455e155fcfa04f18ac919e</anchor>
      <arglist>(T settings)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::IAuthCrossRetrievalManagerFactory</name>
    <filename>a00049.html</filename>
    <member kind="function">
      <type>IAuthCrossReferenceMutableRetrievalManager</type>
      <name>GetCrossRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00049.html</anchorfile>
      <anchor>a63b960f1398ab831a6360c0363aa05f1</anchor>
      <arglist>(T settings, IAuthSdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
    <member kind="function">
      <type>IAuthCrossReferenceMutableRetrievalManager</type>
      <name>GetCrossRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00049.html</anchorfile>
      <anchor>a7a127660f72b49b2eef59c42e54792de</anchor>
      <arglist>(T settings, IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::IAuthMutableRetrievalManagerFactory</name>
    <filename>a00050.html</filename>
    <member kind="function">
      <type>IAuthSdmxMutableObjectRetrievalManager</type>
      <name>GetRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00050.html</anchorfile>
      <anchor>adea46678d26a47b0fa477dc5645bed3c</anchor>
      <arglist>(T settings, ISdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::IComponentMapping</name>
    <filename>a00051.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::IMapping</base>
    <member kind="function">
      <type>string</type>
      <name>GenerateComponentWhere</name>
      <anchorfile>a00051.html</anchorfile>
      <anchor>af77f29b82e98a9bfd73172e0d75648cf</anchor>
      <arglist>(string conditionValue, string operatorValue=&quot;=&quot;)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::ICrossReferenceResolverMutableEngine</name>
    <filename>a00052.html</filename>
    <member kind="function">
      <type>MaintainableReferenceDictionary</type>
      <name>GetMissingCrossReferences</name>
      <anchorfile>a00052.html</anchorfile>
      <anchor>aa7c359daf7996b5882bb9913ade2aeca</anchor>
      <arglist>(IMutableObjects beans, int numberLevelsDeep, Func&lt; IStructureReference, IMaintainableMutableObject &gt; retriever)</arglist>
    </member>
    <member kind="function">
      <type>MaintainableDictionary&lt; IMaintainableMutableObject &gt;</type>
      <name>ResolveReferences</name>
      <anchorfile>a00052.html</anchorfile>
      <anchor>a0295b31b4ffd871ceee94314b5a3b7a3</anchor>
      <arglist>(IMutableObjects beans, int numberLevelsDeep, Func&lt; IStructureReference, IMaintainableMutableObject &gt; retriever)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMaintainableMutableObject &gt;</type>
      <name>ResolveReferences</name>
      <anchorfile>a00052.html</anchorfile>
      <anchor>a87b59b82dd913b76268555a48bd05b65</anchor>
      <arglist>(IMaintainableMutableObject artefact, int numberLevelsDeep, Func&lt; IStructureReference, IMaintainableMutableObject &gt; retriever)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Builder::ICrossReferenceRetrievalBuilder</name>
    <filename>a00053.html</filename>
    <member kind="function">
      <type>ICrossReferenceMutableRetrievalManager</type>
      <name>BuildStub</name>
      <anchorfile>a00053.html</anchorfile>
      <anchor>a759a1c8525a4e5145be2050da8503e31</anchor>
      <arglist>(ISdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
    <member kind="function">
      <type>IAuthCrossReferenceMutableRetrievalManager</type>
      <name>BuildStub</name>
      <anchorfile>a00053.html</anchorfile>
      <anchor>ad06c2dd80bcd12a09ba3b7efb0f9f72b</anchor>
      <arglist>(ISdmxMutableObjectRetrievalManager retrievalManager, IAuthSdmxMutableObjectRetrievalManager retrievalAuthManager)</arglist>
    </member>
    <member kind="function">
      <type>IAuthCrossReferenceMutableRetrievalManager</type>
      <name>Build</name>
      <anchorfile>a00053.html</anchorfile>
      <anchor>a6766a86f6adcfcd5a2a869d0bc8f4da6</anchor>
      <arglist>(ISdmxMutableObjectRetrievalManager retrievalManager, IAuthSdmxMutableObjectRetrievalManager retrievalAuthManager)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Builder::ICrossReferenceSetBuilder</name>
    <filename>a00054.html</filename>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::ICrossRetrievalManagerFactory</name>
    <filename>a00055.html</filename>
    <member kind="function">
      <type>ICrossReferenceMutableRetrievalManager</type>
      <name>GetCrossRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00055.html</anchorfile>
      <anchor>abce2e61a903178a1d047726e1bcd7c37</anchor>
      <arglist>(T settings, ISdmxMutableObjectRetrievalManager retrievalManager)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::IMapping</name>
    <filename>a00056.html</filename>
    <member kind="function">
      <type>string</type>
      <name>MapComponent</name>
      <anchorfile>a00056.html</anchorfile>
      <anchor>a9b970189dc9bcc601cea1ce9ff933226</anchor>
      <arglist>(IDataReader reader)</arglist>
    </member>
    <member kind="property">
      <type>ComponentEntity</type>
      <name>Component</name>
      <anchorfile>a00056.html</anchorfile>
      <anchor>a66ecced7bbb1c4d74943fbad54d1739f</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>MappingEntity</type>
      <name>Mapping</name>
      <anchorfile>a00056.html</anchorfile>
      <anchor>a424156353ce076edcea4e1bfd4d8191d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::IMutableRetrievalManagerFactory</name>
    <filename>a00057.html</filename>
    <member kind="function">
      <type>ISdmxMutableObjectRetrievalManager</type>
      <name>GetRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00057.html</anchorfile>
      <anchor>a4eb91cc696d77cc14c8e6897ecba99e1</anchor>
      <arglist>(T settings)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::IncompleteMappingSetException</name>
    <filename>a00058.html</filename>
    <member kind="function">
      <type></type>
      <name>IncompleteMappingSetException</name>
      <anchorfile>a00058.html</anchorfile>
      <anchor>a308118faab2b391668f60b5cba78f599</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IncompleteMappingSetException</name>
      <anchorfile>a00058.html</anchorfile>
      <anchor>ad3160bb4e325784466bcb89146bced6f</anchor>
      <arglist>(string message)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IncompleteMappingSetException</name>
      <anchorfile>a00058.html</anchorfile>
      <anchor>a912dcb5988958c8ed393d3402fb35774</anchor>
      <arglist>(Exception innerException, string message)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>IncompleteMappingSetException</name>
      <anchorfile>a00058.html</anchorfile>
      <anchor>af790a26af55c8e789ebe50153287d196</anchor>
      <arglist>(SerializationInfo info, StreamingContext context)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::IRetrievalEngine</name>
    <filename>a00059.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a4bd29711f87902d507b3d6f3acce6a63</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>adc933c42c5e1e624777c8e08bf8f4552</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>ae40407960c4f0af755645a7eb9724b7a</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55f1402d725829cade4c8a6473904bf2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55cc6a0d32aed49f4dc45136b01226b2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a66ab9deedb40dc5993ba64cbef81fe40</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>IRetrievalEngine&lt; ICategorisationMutableObject &gt;</name>
    <filename>a00059.html</filename>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a4bd29711f87902d507b3d6f3acce6a63</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55f1402d725829cade4c8a6473904bf2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>adc933c42c5e1e624777c8e08bf8f4552</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55cc6a0d32aed49f4dc45136b01226b2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>ae40407960c4f0af755645a7eb9724b7a</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a66ab9deedb40dc5993ba64cbef81fe40</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>IRetrievalEngine&lt; ICategorySchemeMutableObject &gt;</name>
    <filename>a00059.html</filename>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a4bd29711f87902d507b3d6f3acce6a63</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55f1402d725829cade4c8a6473904bf2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>adc933c42c5e1e624777c8e08bf8f4552</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55cc6a0d32aed49f4dc45136b01226b2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>ae40407960c4f0af755645a7eb9724b7a</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a66ab9deedb40dc5993ba64cbef81fe40</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>IRetrievalEngine&lt; ICodelistMutableObject &gt;</name>
    <filename>a00059.html</filename>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a4bd29711f87902d507b3d6f3acce6a63</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55f1402d725829cade4c8a6473904bf2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>adc933c42c5e1e624777c8e08bf8f4552</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55cc6a0d32aed49f4dc45136b01226b2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>ae40407960c4f0af755645a7eb9724b7a</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a66ab9deedb40dc5993ba64cbef81fe40</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>IRetrievalEngine&lt; IConceptSchemeMutableObject &gt;</name>
    <filename>a00059.html</filename>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a4bd29711f87902d507b3d6f3acce6a63</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55f1402d725829cade4c8a6473904bf2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>adc933c42c5e1e624777c8e08bf8f4552</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55cc6a0d32aed49f4dc45136b01226b2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>ae40407960c4f0af755645a7eb9724b7a</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a66ab9deedb40dc5993ba64cbef81fe40</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>IRetrievalEngine&lt; IDataflowMutableObject &gt;</name>
    <filename>a00059.html</filename>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a4bd29711f87902d507b3d6f3acce6a63</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55f1402d725829cade4c8a6473904bf2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>adc933c42c5e1e624777c8e08bf8f4552</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55cc6a0d32aed49f4dc45136b01226b2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>ae40407960c4f0af755645a7eb9724b7a</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a66ab9deedb40dc5993ba64cbef81fe40</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>IRetrievalEngine&lt; IDataStructureMutableObject &gt;</name>
    <filename>a00059.html</filename>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a4bd29711f87902d507b3d6f3acce6a63</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55f1402d725829cade4c8a6473904bf2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>adc933c42c5e1e624777c8e08bf8f4552</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55cc6a0d32aed49f4dc45136b01226b2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>ae40407960c4f0af755645a7eb9724b7a</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a66ab9deedb40dc5993ba64cbef81fe40</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>IRetrievalEngine&lt; IHierarchicalCodelistMutableObject &gt;</name>
    <filename>a00059.html</filename>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a4bd29711f87902d507b3d6f3acce6a63</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55f1402d725829cade4c8a6473904bf2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>adc933c42c5e1e624777c8e08bf8f4552</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55cc6a0d32aed49f4dc45136b01226b2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>ae40407960c4f0af755645a7eb9724b7a</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a66ab9deedb40dc5993ba64cbef81fe40</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>IRetrievalEngine&lt; T &gt;</name>
    <filename>a00059.html</filename>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a4bd29711f87902d507b3d6f3acce6a63</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>Retrieve</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55f1402d725829cade4c8a6473904bf2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, VersionQueryType versionConstraints, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>adc933c42c5e1e624777c8e08bf8f4552</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>TMaint</type>
      <name>RetrieveLatest</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a55cc6a0d32aed49f4dc45136b01226b2</anchor>
      <arglist>(IMaintainableRefObject maintainableRef, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>ae40407960c4f0af755645a7eb9724b7a</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; TMaint &gt;</type>
      <name>RetrieveFromReferenced</name>
      <anchorfile>a00059.html</anchorfile>
      <anchor>a66ab9deedb40dc5993ba64cbef81fe40</anchor>
      <arglist>(IStructureReference referencedStructure, ComplexStructureQueryDetailEnumType detail, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ItemEntity</name>
    <filename>a00060.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function" protection="protected">
      <type></type>
      <name>ItemEntity</name>
      <anchorfile>a00060.html</anchorfile>
      <anchor>ad927a25076cbb0297b095b826db4db49</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Id</name>
      <anchorfile>a00060.html</anchorfile>
      <anchor>a596e03083cbffe27d5db2f9f7c880cc8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::ItemTableInfo</name>
    <filename>a00061.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::TableInfo</base>
    <member kind="function">
      <type></type>
      <name>ItemTableInfo</name>
      <anchorfile>a00061.html</anchorfile>
      <anchor>adfe3a800c08314ae4afc740d40601e1d</anchor>
      <arglist>(SdmxStructureEnumType structureType)</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>ForeignKey</name>
      <anchorfile>a00061.html</anchorfile>
      <anchor>a7d55f02f83e11ad70b5bc0ac45028bbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>ParentItem</name>
      <anchorfile>a00061.html</anchorfile>
      <anchor>a7d83d3fbba2c4042d0f30a78b1cf130f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Builder::ItemTableInfoBuilder</name>
    <filename>a00062.html</filename>
    <member kind="function">
      <type>ItemTableInfo</type>
      <name>Build</name>
      <anchorfile>a00062.html</anchorfile>
      <anchor>a5dfae48ca44a48ee73f2971e6e75663e</anchor>
      <arglist>(SdmxStructureEnumType buildFrom)</arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ITimeDimension</name>
    <filename>a00063.html</filename>
    <member kind="function">
      <type>string</type>
      <name>GenerateWhere</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>a3c867b98bf3a95b1d515738a87fc77c1</anchor>
      <arglist>(ISdmxDate dateFrom, ISdmxDate dateTo, string frequencyValue)</arglist>
    </member>
    <member kind="function">
      <type>string</type>
      <name>MapComponent</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>a5e3831b9eb732d003cca593074efcf41</anchor>
      <arglist>(IDataReader reader, string frequencyValue)</arglist>
    </member>
    <member kind="property">
      <type>ComponentEntity</type>
      <name>Component</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>a81602c6b2b1aafeb9b61f907dc0598c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>MappingEntity</type>
      <name>Mapping</name>
      <anchorfile>a00063.html</anchorfile>
      <anchor>ab2b7e5972c202f66981ae365da654e7b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="interface">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ITimeDimensionMapping</name>
    <filename>a00064.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::IMapping</base>
    <member kind="function">
      <type>string</type>
      <name>GenerateWhere</name>
      <anchorfile>a00064.html</anchorfile>
      <anchor>a3986f7eba80ff83b51669f31f7e9efbb</anchor>
      <arglist>(ISdmxDate dateFrom, ISdmxDate dateTo)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::ListDictionary</name>
    <filename>a00065.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>ListDictionary</name>
      <anchorfile>a00065.html</anchorfile>
      <anchor>aa44efca8f54718698508e09718b2859a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ListDictionary</name>
      <anchorfile>a00065.html</anchorfile>
      <anchor>a82c4bb079e13eea206354f85567403ae</anchor>
      <arglist>(IEqualityComparer&lt; TKey &gt; comparer)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Add</name>
      <anchorfile>a00065.html</anchorfile>
      <anchor>aac1488b4b58db9bdbd9cbc5b78f1bb60</anchor>
      <arglist>(TKey key, TValue value)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>ContainsKey</name>
      <anchorfile>a00065.html</anchorfile>
      <anchor>aefb5f7aaa0fcefeb515de1c192cc1100</anchor>
      <arglist>(TKey key)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>TryGetValue</name>
      <anchorfile>a00065.html</anchorfile>
      <anchor>a618f109a5258669e2586653613f12d94</anchor>
      <arglist>(TKey key, out TValue value)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>override TKey</type>
      <name>GetKeyForItem</name>
      <anchorfile>a00065.html</anchorfile>
      <anchor>ae5fa8e5068fef2437adeed4bf5ca7571</anchor>
      <arglist>(KeyValuePair&lt; TKey, TValue &gt; item)</arglist>
    </member>
    <member kind="property">
      <type>ICollection&lt; TKey &gt;</type>
      <name>Keys</name>
      <anchorfile>a00065.html</anchorfile>
      <anchor>a23b850135a4bc614a4a3cfd3a95cc1b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>ICollection&lt; TValue &gt;</type>
      <name>Values</name>
      <anchorfile>a00065.html</anchorfile>
      <anchor>a5ad64b7053ac5b1164f675e5842226cd</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::LocalisedStringType</name>
    <filename>a00066.html</filename>
    <member kind="variable">
      <type>const string</type>
      <name>Name</name>
      <anchorfile>a00066.html</anchorfile>
      <anchor>a8195c71bc89459556facb9b44df5a1ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>Desc</name>
      <anchorfile>a00066.html</anchorfile>
      <anchor>a42fca7ad40b83191a7099e0138e7dcda</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Constants::LocalisedStringType</name>
    <filename>a00067.html</filename>
    <member kind="variable">
      <type>const string</type>
      <name>Name</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>ad79e70ca04aa0071ecac923c261b4025</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>Desc</name>
      <anchorfile>a00067.html</anchorfile>
      <anchor>a118188d51215f972dc738bb44ef99bb4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MaintainableDictionary</name>
    <filename>a00068.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>MaintainableDictionary</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>ab4aa0399014c8b8fb5562bb664015c1a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MaintainableDictionary</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>af31d4f72e072b644dbeb3eaa97643a3d</anchor>
      <arglist>(int capacity)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MaintainableDictionary</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>a8a8ee299fa45cf1637b9ae0d146d5279</anchor>
      <arglist>(IDictionaryOfSets&lt; IMaintainableMutableObject, TValue &gt; dictionary)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>MaintainableDictionary</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>a553e4cda5110c1aaee3754e961838c78</anchor>
      <arglist>(SerializationInfo info, StreamingContext context)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>MaintainableDictionary&lt; IStructureReference &gt;</name>
    <filename>a00068.html</filename>
    <member kind="function">
      <type></type>
      <name>MaintainableDictionary</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>ab4aa0399014c8b8fb5562bb664015c1a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MaintainableDictionary</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>af31d4f72e072b644dbeb3eaa97643a3d</anchor>
      <arglist>(int capacity)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MaintainableDictionary</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>a8a8ee299fa45cf1637b9ae0d146d5279</anchor>
      <arglist>(IDictionaryOfSets&lt; IMaintainableMutableObject, TValue &gt; dictionary)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>MaintainableDictionary</name>
      <anchorfile>a00068.html</anchorfile>
      <anchor>a553e4cda5110c1aaee3754e961838c78</anchor>
      <arglist>(SerializationInfo info, StreamingContext context)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Helper::MaintainableMutableComparer</name>
    <filename>a00069.html</filename>
    <member kind="function">
      <type>bool</type>
      <name>Equals</name>
      <anchorfile>a00069.html</anchorfile>
      <anchor>ae801ae2fabc31a0dc76538250008cbce</anchor>
      <arglist>(IMaintainableMutableObject x, IMaintainableMutableObject y)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>GetHashCode</name>
      <anchorfile>a00069.html</anchorfile>
      <anchor>a8487b54d162c5ec9b470988fbf705e97</anchor>
      <arglist>(IMaintainableMutableObject obj)</arglist>
    </member>
    <member kind="property" static="yes">
      <type>static MaintainableMutableComparer</type>
      <name>Instance</name>
      <anchorfile>a00069.html</anchorfile>
      <anchor>a7a433a514fa576fc3869d9a04d60b68e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MaintainableReferenceDictionary</name>
    <filename>a00070.html</filename>
    <base>MaintainableDictionary&lt; IStructureReference &gt;</base>
    <member kind="function">
      <type></type>
      <name>MaintainableReferenceDictionary</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>a54496fef5ebf2ba8000426cb3876fae5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MaintainableReferenceDictionary</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>acbeea5f63d6cd8d2e21df204b93579c7</anchor>
      <arglist>(int capacity)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MaintainableReferenceDictionary</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>abd25e77071f3fc2a4274caabf7f259f6</anchor>
      <arglist>(IDictionaryOfSets&lt; IMaintainableMutableObject, IStructureReference &gt; dictionary)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>MaintainableReferenceDictionary</name>
      <anchorfile>a00070.html</anchorfile>
      <anchor>aff8544a1839d18448100342fe4b9aad6</anchor>
      <arglist>(SerializationInfo info, StreamingContext context)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::MappingEntity</name>
    <filename>a00071.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function">
      <type></type>
      <name>MappingEntity</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>a762ee9bd0baef5f4a0ec5122f3ac26d6</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; DataSetColumnEntity &gt;</type>
      <name>Columns</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>a1aaccf764c8d06c0db9e0a8b46c4c9f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; ComponentEntity &gt;</type>
      <name>Components</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>af0682531f22062d89706519a491d0493</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Constant</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>a4207c558225d6deeba70a7a11842d562</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>MappingType</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>a1455186af670c467bcc44d389840d8f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>TranscodingEntity</type>
      <name>Transcoding</name>
      <anchorfile>a00071.html</anchorfile>
      <anchor>a4e85a1b2a86171025febad900c4cf9ae</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::MappingSetEntity</name>
    <filename>a00072.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function">
      <type></type>
      <name>MappingSetEntity</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>ac907aea4e4dcbb06e3dbb4af1b11b2c3</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>DataSetEntity</type>
      <name>DataSet</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>a5b6cf35ff78d35d89ba07ac5b35f16df</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>DataflowEntity</type>
      <name>Dataflow</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>ab2d40fc9cc365c7400ca86f94ece9973</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Description</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>ab7ee93c2a80331a420727b58fa7ba20c</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Id</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>a65e7e312321e2b2f9ec0f1ec29ab97d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>Collection&lt; MappingEntity &gt;</type>
      <name>Mappings</name>
      <anchorfile>a00072.html</anchorfile>
      <anchor>a59dbf2fa1a2c915a39e3e93bda680424</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::MappingSetRetriever</name>
    <filename>a00073.html</filename>
    <member kind="function" static="yes">
      <type>static MappingSetEntity</type>
      <name>GetMappingSet</name>
      <anchorfile>a00073.html</anchorfile>
      <anchor>a63485f433fc87c8f7489c191ce6ee646</anchor>
      <arglist>(ConnectionStringSettings connectionStringSettings, string dataflowId, string dataflowVersion, string dataflowAgency, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static MappingSetEntity</type>
      <name>GetMappingSet</name>
      <anchorfile>a00073.html</anchorfile>
      <anchor>a39a6c41a5efce55418d82f23991da8b9</anchor>
      <arglist>(ConnectionStringSettings connectionStringSettings, IMaintainableRefObject maintainableRef, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="property" static="yes">
      <type>static bool</type>
      <name>ProductionDataflowOnly</name>
      <anchorfile>a00073.html</anchorfile>
      <anchor>a81fed1e6cd67c82af69f77dbf2ab3a3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" static="yes">
      <type>static bool</type>
      <name>InUsageDataflowOnly</name>
      <anchorfile>a00073.html</anchorfile>
      <anchor>ad6c8c2409101928ea2a20174dc7032f8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Config::MappingStoreConfigSection</name>
    <filename>a00074.html</filename>
    <member kind="property">
      <type>DataflowConfigurationSection</type>
      <name>DataflowConfiguration</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>a92355e2f8df71a0f238d9d186f82fcc1</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>MastoreProviderMappingSettingCollection</type>
      <name>DisseminationDatabaseSettings</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>a9b09b1489125293f1b45f6aa45e6cd11</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>DatabaseSettingCollection</type>
      <name>GeneralDatabaseSettings</name>
      <anchorfile>a00074.html</anchorfile>
      <anchor>a790681149fc992cb5667d00700276930</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Config::MappingStoreDefaultConstants</name>
    <filename>a00075.html</filename>
    <member kind="variable">
      <type>const string</type>
      <name>MySqlName</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>a43a70e225f5af2a6de61ec650b6bfafa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>MySqlProvider</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>a897d87054c960f8e2709cd22117552ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>OracleName</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>a3ee21be5479b353731304455ab19e464</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>OracleProvider</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>a16b37b5974c05fcadcefa16e22c8ee2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>OracleProviderOdp</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>ab18b9d62fd80b97cf155f55b244c2587</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>PCAxisName</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>a57f5de17248c418bc6ffe41312ae603d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>PCAxisProvider</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>acac85b6eecb6777dceb25672c45b9466</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>SqlServerName</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>a5f410ff533b943c2fc54995fc0217393</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>SqlServerProvider</name>
      <anchorfile>a00075.html</anchorfile>
      <anchor>ade74c7a92312635d84c7c2cc612d4472</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::MappingStoreException</name>
    <filename>a00076.html</filename>
    <member kind="function">
      <type></type>
      <name>MappingStoreException</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>a5835c0f9f5e2509eab3ec61ed350bc59</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MappingStoreException</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>a14ae3101fc852a5fe18153205db31fcb</anchor>
      <arglist>(string message)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MappingStoreException</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>adf2e2d34686275ea3314087d42f77739</anchor>
      <arglist>(string message, Exception innerException)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>MappingStoreException</name>
      <anchorfile>a00076.html</anchorfile>
      <anchor>a4d6766ac28adfa61b379b9cf53bd193d</anchor>
      <arglist>(SerializationInfo info, StreamingContext context)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::MappingStoreHeaderRetrievalManager</name>
    <filename>a00077.html</filename>
    <member kind="function">
      <type></type>
      <name>MappingStoreHeaderRetrievalManager</name>
      <anchorfile>a00077.html</anchorfile>
      <anchor>aff7bd41d0150586340846060590d0261</anchor>
      <arglist>(ConnectionStringSettings connectionStringSettings, Func&lt; HeaderRetrieverEngine, IHeader &gt; getHeader)</arglist>
    </member>
    <member kind="property">
      <type>IHeader</type>
      <name>Header</name>
      <anchorfile>a00077.html</anchorfile>
      <anchor>a7358ba415a9cfb798e18c8129dd0ebec</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::MappingStoreRetrievalManager</name>
    <filename>a00078.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Manager::RetrievalManagerBase</base>
    <member kind="function">
      <type></type>
      <name>MappingStoreRetrievalManager</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>acb4781a4303ae7cab63e98f2d847e9c8</anchor>
      <arglist>(ConnectionStringSettings mappingStoreSettings)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MappingStoreRetrievalManager</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a60279a6bda57ef8af3e85858780edbc6</anchor>
      <arglist>(Database mappingStoreDB)</arglist>
    </member>
    <member kind="function">
      <type>override IAgencySchemeMutableObject</type>
      <name>GetMutableAgencyScheme</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a591228bfb13248ae90bb9cd780d018da</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IAgencySchemeMutableObject &gt;</type>
      <name>GetMutableAgencySchemeObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a895055b6feb463a12d1a4967774d5b9e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ICategorisationMutableObject</type>
      <name>GetMutableCategorisation</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a164701d8ca7fa33e62aa5ef31a9d9aec</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICategorisationMutableObject &gt;</type>
      <name>GetMutableCategorisationObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>ae634dd3ec6b7a04d67707ad81f1cc418</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ICategorySchemeMutableObject</type>
      <name>GetMutableCategoryScheme</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a99e036dbae3599843dcc6c06c42413ab</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICategorySchemeMutableObject &gt;</type>
      <name>GetMutableCategorySchemeObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>af77668345619e0f3374926f080dff498</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ICodelistMutableObject</type>
      <name>GetMutableCodelist</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a3c69acef86612c32247bebc7695b1519</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a1adeb0af21e4c10c7e6c825d99d2f81e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IConceptSchemeMutableObject</type>
      <name>GetMutableConceptScheme</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a44b9a2ca35af3be5b9598e66da41b224</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IConceptSchemeMutableObject &gt;</type>
      <name>GetMutableConceptSchemeObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>ab226ac9c5b99814f831b0ad3d54f68c4</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IContentConstraintMutableObject</type>
      <name>GetMutableContentConstraint</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a0bbd4edfecd2ec4e15f7ee1cca9cb2cb</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IContentConstraintMutableObject &gt;</type>
      <name>GetMutableContentConstraintObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>acc1d4823de9cf91e2d5ab34f7a3905e1</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataConsumerSchemeMutableObject</type>
      <name>GetMutableDataConsumerScheme</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>af15b43d426b98d887f9b0b739e42c97f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataConsumerSchemeMutableObject &gt;</type>
      <name>GetMutableDataConsumerSchemeObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a4c116e6051b220644aed3b37bc7f8f1f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataProviderSchemeMutableObject</type>
      <name>GetMutableDataProviderScheme</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a8d9f4c2e202d50ea33bc2c68493bde1e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataProviderSchemeMutableObject &gt;</type>
      <name>GetMutableDataProviderSchemeObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>aa78a2a34d90791acba6df958bfceb05a</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataStructureMutableObject</type>
      <name>GetMutableDataStructure</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a5dcc0b5d7f10889e1a49089f6ab0051e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataStructureMutableObject &gt;</type>
      <name>GetMutableDataStructureObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a3dc6900c1d16c92deb2191e15c7de8ad</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IDataflowMutableObject</type>
      <name>GetMutableDataflow</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>acc7073b80cf398203c45bda8e2b22b0f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataflowMutableObject &gt;</type>
      <name>GetMutableDataflowObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a6e7654563a55c1024fecd39554a44409</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IHierarchicalCodelistMutableObject</type>
      <name>GetMutableHierarchicCodeList</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a8686265cb392996cdbe5c0f612a00974</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IHierarchicalCodelistMutableObject &gt;</type>
      <name>GetMutableHierarchicCodeListObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>ae706fe3daed60a62484653f8f075c3bb</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IMetadataStructureDefinitionMutableObject</type>
      <name>GetMutableMetadataStructure</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a20f90965e8d8ac68f451aa1e881571b7</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IMetadataStructureDefinitionMutableObject &gt;</type>
      <name>GetMutableMetadataStructureObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a06f1486cf9a5a94dab49cb166096b68d</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IMetadataFlowMutableObject</type>
      <name>GetMutableMetadataflow</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a498dbe0507f383912900bbdb43599d7a</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IMetadataFlowMutableObject &gt;</type>
      <name>GetMutableMetadataflowObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a40d59e1edec9d1be49af6441aadd35e3</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IOrganisationUnitSchemeMutableObject</type>
      <name>GetMutableOrganisationUnitScheme</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a5d8493c6cab76178621d119d82fbec57</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IOrganisationUnitSchemeMutableObject &gt;</type>
      <name>GetMutableOrganisationUnitSchemeObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>ace8ce5b429ffb1b55ba00604b6beddf8</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IProcessMutableObject</type>
      <name>GetMutableProcessObject</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>ad00f704d592f93a70cb8f786f0f72cfd</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IProcessMutableObject &gt;</type>
      <name>GetMutableProcessObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a326238ba50aecc34def2353f674b4a4c</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IProvisionAgreementMutableObject</type>
      <name>GetMutableProvisionAgreement</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a62203555ed79435d019e932c08840574</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IProvisionAgreementMutableObject &gt;</type>
      <name>GetMutableProvisionAgreementObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a0a84b9912a78eb26f4025ff48bf00713</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IReportingTaxonomyMutableObject</type>
      <name>GetMutableReportingTaxonomy</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a9578ff832a7e158d8224f8cf0e6e5fee</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IReportingTaxonomyMutableObject &gt;</type>
      <name>GetMutableReportingTaxonomyObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>a26865e429c5f08b008ccd3599fe1ac33</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override IStructureSetMutableObject</type>
      <name>GetMutableStructureSet</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>af8400d528f0a3e24a9e8b26e786d2ef0</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IStructureSetMutableObject &gt;</type>
      <name>GetMutableStructureSetObjects</name>
      <anchorfile>a00078.html</anchorfile>
      <anchor>ad9731fa994f94991e643c32c395b9d1e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::MappingStoreSdmxObjectRetrievalManager</name>
    <filename>a00079.html</filename>
    <member kind="function">
      <type></type>
      <name>MappingStoreSdmxObjectRetrievalManager</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>aa8a37a8487a6de347ef253b8e3d5a769</anchor>
      <arglist>(ConnectionStringSettings connectionStringSettings)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MappingStoreSdmxObjectRetrievalManager</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a95178f0c464148cd5a1c8dbe2a69b003</anchor>
      <arglist>(ISdmxMutableObjectRetrievalManager mutableObjectRetrievalManager)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IAgencyScheme &gt;</type>
      <name>GetAgencySchemeObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>ac0ba5ef8c807be037c77c512657efd45</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IAttachmentConstraintObject &gt;</type>
      <name>GetAttachmentConstraints</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a439c52a9f9bc4bcfb4bac3db24f06ea9</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICategorisationObject &gt;</type>
      <name>GetCategorisationObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>ab88862677a9c63a679dbeaf15c2767b4</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICategorySchemeObject &gt;</type>
      <name>GetCategorySchemeObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a15e2fdca1ecbffae17591da16a13c809</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; ICodelistObject &gt;</type>
      <name>GetCodelistObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>ac7c59cf30ece6a64dc8cda50e6ee6ce5</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IConceptSchemeObject &gt;</type>
      <name>GetConceptSchemeObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>af79e1bd2fbd6b36ab37ae5a7f776da76</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IContentConstraintObject &gt;</type>
      <name>GetContentConstraints</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a1c91c4eaa32b9d8ba1de53df41619833</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataConsumerScheme &gt;</type>
      <name>GetDataConsumerSchemeObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a8d582346b3e5feb1c095e02cb215b3f4</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataProviderScheme &gt;</type>
      <name>GetDataProviderSchemeObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a4ce103a4ea90228c37b8739199bce88f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataStructureObject &gt;</type>
      <name>GetDataStructureObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a0baa293a81c9e7744544d21d458db8bf</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IDataflowObject &gt;</type>
      <name>GetDataflowObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>afe48245c482794f5a5ec74b21891f825</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IHierarchicalCodelistObject &gt;</type>
      <name>GetHierarchicCodeListObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a7a397a7af79ec8cf07540298438eb13f</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IMetadataStructureDefinitionObject &gt;</type>
      <name>GetMetadataStructureObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a894c8d56ebd197e2108fafa2919f59f0</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IMetadataFlow &gt;</type>
      <name>GetMetadataflowObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>ac4bcae642ecb1b342e24298acbc3c3b1</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IOrganisationUnitSchemeObject &gt;</type>
      <name>GetOrganisationUnitSchemeObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a3e6d1b34319cac58c86155f2c4a7302a</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IProcessObject &gt;</type>
      <name>GetProcessObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>ac3ca1502400e91beb5e740f4aef5328b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IProvisionAgreementObject &gt;</type>
      <name>GetProvisionAgreementObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>aa4ad2774c09952058a0bd2ced98d09a1</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IReportingTaxonomyObject &gt;</type>
      <name>GetReportingTaxonomyObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>a9c7081684e67a9536a7df3d738e73c8a</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>override ISet&lt; IStructureSetObject &gt;</type>
      <name>GetStructureSetObjects</name>
      <anchorfile>a00079.html</anchorfile>
      <anchor>ac7245b5a5f06599e9d7e8fafd2ef32c0</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Helper::MappingUtils</name>
    <filename>a00080.html</filename>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>IsMappingSetComplete</name>
      <anchorfile>a00080.html</anchorfile>
      <anchor>a3beaa7f16ad34e88580d99ce5c6f41e6</anchor>
      <arglist>(MappingSetEntity mappingSet)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>IsMappingSetComplete</name>
      <anchorfile>a00080.html</anchorfile>
      <anchor>afe622a42763e1196c2f0f9c8ae384cb8</anchor>
      <arglist>(DsdEntity dsd, Dictionary&lt; ComponentEntity, MappingEntity &gt; componentMapping)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Config::MastoreProviderMappingSetting</name>
    <filename>a00081.html</filename>
    <member kind="function">
      <type></type>
      <name>MastoreProviderMappingSetting</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>a307c9fee7a0ae0fa4973ca8dc18a5c96</anchor>
      <arglist>(string name)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MastoreProviderMappingSetting</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>aa62a38eb324c44a1f3762ffee7da3987</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>override bool</type>
      <name>IsReadOnly</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>a707be6c32f01a9a636798a6024d21587</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Name</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>a29cfdb263f4b4bbb3e7cb6dc3eaa115f</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Provider</name>
      <anchorfile>a00081.html</anchorfile>
      <anchor>a37a4cb3b23bdc566eec337e5e852bf4f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Config::MastoreProviderMappingSettingCollection</name>
    <filename>a00082.html</filename>
    <member kind="function">
      <type></type>
      <name>MastoreProviderMappingSettingCollection</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a6809686d1886c9debfcc66f1b023c5c0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Add</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a4b90880b5e2e41392dcdbfb591939e4b</anchor>
      <arglist>(MastoreProviderMappingSetting item)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Clear</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a524c9005111e025029a02df6dd091b5d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>Contains</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>ad85a15cddfba6a325fcae2bd8f05ff5c</anchor>
      <arglist>(MastoreProviderMappingSetting item)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CopyTo</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a069e3043e62793a7f70afdd6f9752374</anchor>
      <arglist>(MastoreProviderMappingSetting[] array, int arrayIndex)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>Remove</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a6ff64f1af0c63d5395ebbcc684060cd6</anchor>
      <arglist>(MastoreProviderMappingSetting item)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Remove</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a7bf59fca4dabba7b79834cedd6bde9b8</anchor>
      <arglist>(string name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>RemoveAt</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>ae5da511ba24b6d5a0e6a13cb3e4e2b77</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>override ConfigurationElement</type>
      <name>CreateNewElement</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a36f128ba38200222dc255a9eae7ea787</anchor>
      <arglist>(string elementName)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>override ConfigurationElement</type>
      <name>CreateNewElement</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>ab6dbfc67e12f445be097de5549ef322f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>override object</type>
      <name>GetElementKey</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>aaac87843c0cbf45562776c52eb9e4eec</anchor>
      <arglist>(ConfigurationElement element)</arglist>
    </member>
    <member kind="property">
      <type>new bool</type>
      <name>IsReadOnly</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a4fe1de1913a9cb7f674c282f05867ea3</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>MastoreProviderMappingSetting</type>
      <name>this[int index]</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a9729ed8a7ca8ece0b639aba6249ba9c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>new MastoreProviderMappingSetting</type>
      <name>this[string name]</name>
      <anchorfile>a00082.html</anchorfile>
      <anchor>a575e8f818b98671f41adfe4e354a006e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Extensions::MutableMaintainableExtensions</name>
    <filename>a00083.html</filename>
    <member kind="function" static="yes">
      <type>static TInterface</type>
      <name>CloneAsStub&lt; TInterface, TImplementation &gt;</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>a4b701bc6ddbe4fbedbd010e8a297ee9f</anchor>
      <arglist>(this TInterface maintainable)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>ConvertToStub</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>af30287d970a3eff385809ea4fa9882d7</anchor>
      <arglist>(this ICrossSectionalDataStructureMutableObject crossDsd)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>NormalizeSdmxv20DataStructure</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>a6160fd2fadb13c221291d87c4e2b2895</anchor>
      <arglist>(this ICrossSectionalDataStructureMutableObject crossDsd)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>NormalizeSdmxv20DataStructure</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>a5e9d6a2f697d0d3a83f2092561536f45</anchor>
      <arglist>(this IDataStructureMutableObject dataStructure)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>NormalizeSdmxv20DataStructure</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>a1c986a9c0e6348a6205dedfa26d5e3ea</anchor>
      <arglist>(this IMaintainableMutableObject dataStructure)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>NormalizeSdmxv20DataStructures</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>afaf2b97530268d2736759e4f581d2735</anchor>
      <arglist>(this IEnumerable&lt; IDataStructureMutableObject &gt; dataStructures)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static IStructureReference</type>
      <name>GetEnumeratedRepresentation</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>af34df0d45aa45ff585057fde67d56507</anchor>
      <arglist>(this IComponentMutableObject component)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static IStructureReference</type>
      <name>GetEnumeratedRepresentation</name>
      <anchorfile>a00083.html</anchorfile>
      <anchor>a6c69579651e15ecfefc11ef650dbff1d</anchor>
      <arglist>(this IDimensionMutableObject dimension, IDataStructureMutableObject dsd)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Factory::MutableRetrievalManagerFactory</name>
    <filename>a00084.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Factory::IMutableRetrievalManagerFactory</base>
    <member kind="function">
      <type></type>
      <name>MutableRetrievalManagerFactory</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>a960a913f3933e1e96cf3f3f544b7dd5e</anchor>
      <arglist>(Func&lt; object, ISdmxMutableObjectRetrievalManager &gt; factoryMethod)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MutableRetrievalManagerFactory</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>a375ec66047e0252fefad9d736e79e076</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ISdmxMutableObjectRetrievalManager</type>
      <name>GetRetrievalManager&lt; T &gt;</name>
      <anchorfile>a00084.html</anchorfile>
      <anchor>ae35a7375c7f49d4adf43ebe67e6e242c</anchor>
      <arglist>(T settings)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Constants::PeriodCodelist</name>
    <filename>a00085.html</filename>
    <member kind="variable">
      <type>const string</type>
      <name>Agency</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>a02e57c211563056bbba75a37a961e1cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const string</type>
      <name>Version</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>a6481ee67b645c5fe6cf27edd917b7bab</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" static="yes">
      <type>static IDictionary&lt; string, PeriodObject &gt;</type>
      <name>PeriodCodelistIdMap</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>a462f967fc9f633caa4ad40b5e07ceef2</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" static="yes">
      <type>static IList&lt; string &gt;</type>
      <name>SupportedPeriodFrequencies</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>a4c7da079fdeb063640ea96713c552743</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" static="yes">
      <type>static IList&lt; int?&gt;</type>
      <name>VersionParticles</name>
      <anchorfile>a00085.html</anchorfile>
      <anchor>a5b4236992736f7f5b1d426d9c2f90e29</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::PeriodObject</name>
    <filename>a00086.html</filename>
    <member kind="function">
      <type></type>
      <name>PeriodObject</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>afa00ba43ee55c871aca6035629e5606c</anchor>
      <arglist>(int periodLength, string periodFormat, string id)</arglist>
    </member>
    <member kind="property">
      <type>IList&lt; string &gt;</type>
      <name>Codes</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>acc1a07e32fb5f10f1fa638b6777110e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Id</name>
      <anchorfile>a00086.html</anchorfile>
      <anchor>a811ba915ed908c1ae6c2777792b9a5b9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</name>
    <filename>a00087.html</filename>
    <member kind="function">
      <type>override bool</type>
      <name>Equals</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>a1067b6e30691caa5c66645c14edca5ae</anchor>
      <arglist>(object obj)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>Equals</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>ab6f8f5a016c915db5e34f97374ca8ec4</anchor>
      <arglist>(PersistentEntityBase other)</arglist>
    </member>
    <member kind="function">
      <type>override int</type>
      <name>GetHashCode</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>a2fcea553806aa3b7351b8829f3149de7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>PersistentEntityBase</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>adde4e5126d22df52ffb1a9e9a367c75b</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>long</type>
      <name>SysId</name>
      <anchorfile>a00087.html</anchorfile>
      <anchor>a6766f07a69d805178db06e898169d3fd</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Extensions::QueryExtensions</name>
    <filename>a00088.html</filename>
    <member kind="function" static="yes">
      <type>static VersionQueryType</type>
      <name>GetVersionConstraints</name>
      <anchorfile>a00088.html</anchorfile>
      <anchor>a02df382c75eb42da5e2afde6c75578eb</anchor>
      <arglist>(this IComplexStructureReferenceObject complexStructureReferenceObject)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static VersionQueryType</type>
      <name>GetVersionConstraints</name>
      <anchorfile>a00088.html</anchorfile>
      <anchor>af328fd91f1f98fa661a5a05d4bcb07ae</anchor>
      <arglist>(this bool returnLatest)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::RetrievalManagerBase</name>
    <filename>a00089.html</filename>
    <member kind="function" virtualness="pure">
      <type>abstract IAgencySchemeMutableObject</type>
      <name>GetMutableAgencyScheme</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a7d33c6c1d3bdafbac10081228c8ec05e</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IAgencySchemeMutableObject &gt;</type>
      <name>GetMutableAgencySchemeObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>ad0c895890e72aa078e73d9d56cb5caf5</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ICategorisationMutableObject</type>
      <name>GetMutableCategorisation</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>aa5b2736c66df24c00f0bfab46d792c30</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; ICategorisationMutableObject &gt;</type>
      <name>GetMutableCategorisationObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a9da514b88cf282ef4c4cfcde844994c1</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ICategorySchemeMutableObject</type>
      <name>GetMutableCategoryScheme</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>aa6790d8ddb3cad2ca855be7c90423f0b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; ICategorySchemeMutableObject &gt;</type>
      <name>GetMutableCategorySchemeObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>af56c5f0b2ab5e8ffcbcf5ede060378b8</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ICodelistMutableObject</type>
      <name>GetMutableCodelist</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a5096417b10bcdf6757985f8297ccc087</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a471213153b5af1ac09ff4dffbcaca5a1</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IConceptSchemeMutableObject</type>
      <name>GetMutableConceptScheme</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a43593ae0a53fa43b067bf27d1e090490</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IConceptSchemeMutableObject &gt;</type>
      <name>GetMutableConceptSchemeObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>aeae23b8481690fa0a748a29de7c45189</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IContentConstraintMutableObject</type>
      <name>GetMutableContentConstraint</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a5204e7af6a8c7be27025270b1794fc79</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IContentConstraintMutableObject &gt;</type>
      <name>GetMutableContentConstraintObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>ad9eb133fc0629ad07a8419f18dd22a27</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IDataConsumerSchemeMutableObject</type>
      <name>GetMutableDataConsumerScheme</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>ab936170973826e98b12073281c074ea7</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IDataConsumerSchemeMutableObject &gt;</type>
      <name>GetMutableDataConsumerSchemeObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>ac8c5004ee5babe504071668dbdb5abda</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IDataProviderSchemeMutableObject</type>
      <name>GetMutableDataProviderScheme</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a1f17290df5609bc49fa68c7ad652c34d</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IDataProviderSchemeMutableObject &gt;</type>
      <name>GetMutableDataProviderSchemeObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a7ef0e034796c060e0d9f28f50d3badc0</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IDataStructureMutableObject</type>
      <name>GetMutableDataStructure</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a46fdee698fc5b2b72ab41f29af2278f9</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IDataStructureMutableObject &gt;</type>
      <name>GetMutableDataStructureObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a9783a1d4230b3cbd6640bda65874a707</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IDataflowMutableObject</type>
      <name>GetMutableDataflow</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a16d585cd3bc6a14cf909b46ac3db4a81</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IDataflowMutableObject &gt;</type>
      <name>GetMutableDataflowObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a9812a2b82d1f60816f0a8a4eb862fada</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IHierarchicalCodelistMutableObject</type>
      <name>GetMutableHierarchicCodeList</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a9e81e1fbca4109b54596b32290e93b79</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IHierarchicalCodelistMutableObject &gt;</type>
      <name>GetMutableHierarchicCodeListObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a6c713250beb62fe07ed3eefb3e13133c</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>IMaintainableMutableObject</type>
      <name>GetMutableMaintainable</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>af2485c0e486491cbd06cdaa641b79386</anchor>
      <arglist>(IStructureReference query, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; IMaintainableMutableObject &gt;</type>
      <name>GetMutableMaintainables</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a720b2dd46f649188d998ec504c662e57</anchor>
      <arglist>(IStructureReference query, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IMetadataStructureDefinitionMutableObject</type>
      <name>GetMutableMetadataStructure</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a05add72e78ed9fc26456cdd36d11925a</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IMetadataStructureDefinitionMutableObject &gt;</type>
      <name>GetMutableMetadataStructureObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a090bc19d708c9bb654d276c27666a947</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IMetadataFlowMutableObject</type>
      <name>GetMutableMetadataflow</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a09e47649c02fec3e840a896174ae1d71</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IMetadataFlowMutableObject &gt;</type>
      <name>GetMutableMetadataflowObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>aab53bb7c7c2e27872a26d0ce6d7edfdd</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IOrganisationUnitSchemeMutableObject</type>
      <name>GetMutableOrganisationUnitScheme</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a56bab6b5932a473d0b2697b92dd8e19b</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IOrganisationUnitSchemeMutableObject &gt;</type>
      <name>GetMutableOrganisationUnitSchemeObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a1938698720804e4a8f59e7472a40a767</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IProcessMutableObject</type>
      <name>GetMutableProcessObject</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a66d92f8867af56268068e5a462b19840</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IProcessMutableObject &gt;</type>
      <name>GetMutableProcessObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>ad752862b5ff0f1f341c655d530918cdd</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IProvisionAgreementMutableObject</type>
      <name>GetMutableProvisionAgreement</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a64dca9d93f2f3d8f5620786b25e66be8</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IProvisionAgreementMutableObject &gt;</type>
      <name>GetMutableProvisionAgreementObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a516ff07edba7312b68dccbcbcd182a22</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IReportingTaxonomyMutableObject</type>
      <name>GetMutableReportingTaxonomy</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a63820742c131025c7e47a3556a546ca3</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IReportingTaxonomyMutableObject &gt;</type>
      <name>GetMutableReportingTaxonomyObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>ac4d040202f6a0e969c2d3e94e2443f94</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract IStructureSetMutableObject</type>
      <name>GetMutableStructureSet</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a7b3d30d47cb3d8c48dfa04a9992aa367</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>abstract ISet&lt; IStructureSetMutableObject &gt;</type>
      <name>GetMutableStructureSetObjects</name>
      <anchorfile>a00089.html</anchorfile>
      <anchor>a02debc9a16c8e66fe78acfa9001df392</anchor>
      <arglist>(IMaintainableRefObject xref, bool returnLatest, bool returnStub)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::RetrievalSettings</name>
    <filename>a00090.html</filename>
    <member kind="function">
      <type></type>
      <name>RetrievalSettings</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>a5a3059cc4d0c7134c0136d60cac2537b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RetrievalSettings</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>a2f32cc4a23a55a57a8d122ad79b8619e</anchor>
      <arglist>(StructureQueryDetail queryDetail)</arglist>
    </member>
    <member kind="property">
      <type>StructureQueryDetail</type>
      <name>QueryDetail</name>
      <anchorfile>a00090.html</anchorfile>
      <anchor>ac24a841bbbc82d70d7357446abc89181</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Extensions::SdmxDateExtensions</name>
    <filename>a00091.html</filename>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>StartsBefore</name>
      <anchorfile>a00091.html</anchorfile>
      <anchor>a48c14d9adaa976c559d5d856d083b58c</anchor>
      <arglist>(this ISdmxDate thisSdmxDate, ISdmxDate otherSdmxDate)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>EndsAfter</name>
      <anchorfile>a00091.html</anchorfile>
      <anchor>ae67c8b320bf74b02071e56c5db12ab6e</anchor>
      <arglist>(this ISdmxDate thisSdmxDate, ISdmxDate otherSdmxDate)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static string</type>
      <name>FormatAsDateString</name>
      <anchorfile>a00091.html</anchorfile>
      <anchor>ae85cb74da00282cd5248943949a0ce74</anchor>
      <arglist>(this ISdmxDate sdmxDate, bool startOfPeriod)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static SdmxQueryPeriod</type>
      <name>ToQueryPeriod</name>
      <anchorfile>a00091.html</anchorfile>
      <anchor>a50c8ec414aea42048738be712c3b7bc8</anchor>
      <arglist>(this ISdmxDate sdmxDate, IPeriodicity periodicity)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::SdmxQueryPeriod</name>
    <filename>a00092.html</filename>
    <member kind="property">
      <type>int</type>
      <name>Year</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>a1f40ee324f656efed7b76dea0a038f1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>int</type>
      <name>Period</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>a3dda6f1dd7401fa7ecd898eae8ef79b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>HasPeriod</name>
      <anchorfile>a00092.html</anchorfile>
      <anchor>ac2538c8aea3e13602d867af85ac62da6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::SdmxQueryTimeVO</name>
    <filename>a00093.html</filename>
    <member kind="property">
      <type>int</type>
      <name>EndPeriod</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>afdcf1322efbbeab94d3ec34178348452</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>int</type>
      <name>EndYear</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>a0f153ada267cf4362a6742e3e5ebb141</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>HasEndPeriod</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>a22713bdb524584453e0a58831e90d5f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>HasStartPeriod</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>a7686510cddfcad78e3fd4e38efa0cb87</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>int</type>
      <name>StartPeriod</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>add5f37c7c84e1792ffb8f25b9b91df6a</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>int</type>
      <name>StartYear</name>
      <anchorfile>a00093.html</anchorfile>
      <anchor>adaa8e141e1a58951d81d36de1311638f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Extensions::SdmxStructureTypeExtensions</name>
    <filename>a00094.html</filename>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>IsOneOf</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>af7a2e0aa4bb990a623cb97e7cdee2d90</anchor>
      <arglist>(this SdmxStructureEnumType structureType, params SdmxStructureEnumType[] structureTypes)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>IsOneOf</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>ab0ae2b860d213ef2ff060404010232ee</anchor>
      <arglist>(this SdmxStructureType structureType, params SdmxStructureEnumType[] structureTypes)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>NeedsCategorisation</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>a9853219a3778d5014ae3bf04fd13eafe</anchor>
      <arglist>(this IList&lt; IStructureReference &gt; queries)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>NeedsCategorisation</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>a6940f7d454040c00084d9ace78dc0fe5</anchor>
      <arglist>(this SdmxStructureType structureType)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static IEnumerable&lt; T &gt;</type>
      <name>GetSdxmV21&lt; T &gt;</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>a0c2aaa6d628df6e3e4e43a45e0689dea</anchor>
      <arglist>(this IEnumerable&lt; T &gt; maintainables)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>IsSdmxV20Only</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>a81431f047c0b6a0ecd1d073eee8ccbd2</anchor>
      <arglist>(this IMaintainableMutableObject maintainable)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>NeedsAuth</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>abe75e66a716cad1d90438da05dd1c422</anchor>
      <arglist>(this SdmxStructureEnumType structureType)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>NeedsAuth</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>a9f1cdf999c9dcb6ef9d8f72ece2bc268</anchor>
      <arglist>(this SdmxStructureType structureType)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>IsDefault&lt; T &gt;</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>a8e3a095dde208bb0d7ed6526ff560079</anchor>
      <arglist>(this T artefact)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static IList&lt; long?&gt;</type>
      <name>SplitVersion</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>a4a1694b18a47e4fc1e2d0e2fec7f47c7</anchor>
      <arglist>(this IMaintainableRefObject maintainableRefObject)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static IList&lt; long?&gt;</type>
      <name>SplitVersion</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>a6298181a3ce5c09acf11d8cc62fbf889</anchor>
      <arglist>(this IMaintainableRefObject maintainableRefObject, int size)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static string</type>
      <name>GenerateVersionParameters</name>
      <anchorfile>a00094.html</anchorfile>
      <anchor>a4c327c9572e94871ba068a559f0d6f33</anchor>
      <arglist>(this IMaintainableRefObject maintainableRefObject, Database database, IList&lt; DbParameter &gt; parameters, string versionFieldPrefix, Func&lt; string, string &gt; parameterNameBuilder)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Manager::SpecialMutableObjectRetrievalManager</name>
    <filename>a00095.html</filename>
    <member kind="function">
      <type></type>
      <name>SpecialMutableObjectRetrievalManager</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>ab6edf60186a87bfb2227926891e6bc8f</anchor>
      <arglist>(ConnectionStringSettings connectionStringSettings)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>af16ed795baa9f6d34130c9c6acfbf2cf</anchor>
      <arglist>(IMaintainableRefObject codelistReference, IMaintainableRefObject dataflowReference, string componentConceptRef, bool isTranscoded, IList&lt; IMaintainableRefObject &gt; allowedDataflows)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>a40cb8b4e3670ddc5dc4b8884441d9f75</anchor>
      <arglist>(IMaintainableRefObject codelistReference, IList&lt; string &gt; subset)</arglist>
    </member>
    <member kind="function">
      <type>ISet&lt; ICodelistMutableObject &gt;</type>
      <name>GetMutableCodelistObjects</name>
      <anchorfile>a00095.html</anchorfile>
      <anchor>aaf66c82804fc886e237c1a9e2788b78d</anchor>
      <arglist>(IMaintainableRefObject codelistReference)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Builder::StructureReferenceFromMutableBuilder</name>
    <filename>a00096.html</filename>
    <member kind="function">
      <type>IStructureReference</type>
      <name>Build</name>
      <anchorfile>a00096.html</anchorfile>
      <anchor>a46339866703ddf890ad4a7f8be066a8a</anchor>
      <arglist>(IMaintainableMutableObject buildFrom)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::TableInfo</name>
    <filename>a00097.html</filename>
    <member kind="function">
      <type></type>
      <name>TableInfo</name>
      <anchorfile>a00097.html</anchorfile>
      <anchor>a0a62904f52a21bcbfe3297dfcda59794</anchor>
      <arglist>(SdmxStructureEnumType structureType)</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>PrimaryKey</name>
      <anchorfile>a00097.html</anchorfile>
      <anchor>a3bf406728ba77e5e4eaab056bdd2bfb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Table</name>
      <anchorfile>a00097.html</anchorfile>
      <anchor>a5521610d68b26f63e198fc6ba04f0502</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>ExtraFields</name>
      <anchorfile>a00097.html</anchorfile>
      <anchor>a5b7ff3f502617c136bae87a26d00ccca</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>SdmxStructureEnumType</type>
      <name>StructureType</name>
      <anchorfile>a00097.html</anchorfile>
      <anchor>a923f7446384d35f6151c337d88154c34</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Builder::TableInfoBuilder</name>
    <filename>a00098.html</filename>
    <member kind="function">
      <type>TableInfo</type>
      <name>Build</name>
      <anchorfile>a00098.html</anchorfile>
      <anchor>a6884ada37be2e2ef63abd942428c8224</anchor>
      <arglist>(SdmxStructureEnumType buildFrom)</arglist>
    </member>
    <member kind="function">
      <type>TableInfo</type>
      <name>Build</name>
      <anchorfile>a00098.html</anchorfile>
      <anchor>ac8f2aad47b2b111cae32344c3271d54d</anchor>
      <arglist>(Type buildFrom)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimension1To1</name>
    <filename>a00099.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionMapping</base>
    <base>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ITimeDimensionMapping</base>
    <member kind="function">
      <type></type>
      <name>TimeDimension1To1</name>
      <anchorfile>a00099.html</anchorfile>
      <anchor>a2f9a894946364fcb2f7d63fe607f00ea</anchor>
      <arglist>(MappingEntity mapping, TimeExpressionEntity expression, string databaseType)</arglist>
    </member>
    <member kind="function">
      <type>string</type>
      <name>GenerateWhere</name>
      <anchorfile>a00099.html</anchorfile>
      <anchor>af386683a31c0f23b26391a048bbbca8c</anchor>
      <arglist>(ISdmxDate dateFrom, ISdmxDate dateTo)</arglist>
    </member>
    <member kind="function">
      <type>string</type>
      <name>MapComponent</name>
      <anchorfile>a00099.html</anchorfile>
      <anchor>afc1af9b7caf6ca947723c400f77b0c5e</anchor>
      <arglist>(IDataReader reader)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionConstant</name>
    <filename>a00100.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionMapping</base>
    <base>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ITimeDimensionMapping</base>
    <member kind="function">
      <type></type>
      <name>TimeDimensionConstant</name>
      <anchorfile>a00100.html</anchorfile>
      <anchor>ae9124cf3b365e7bc03d42dd1b2ed7df4</anchor>
      <arglist>(MappingEntity mapping, TimeExpressionEntity expression, string databaseType)</arglist>
    </member>
    <member kind="function">
      <type>string</type>
      <name>GenerateWhere</name>
      <anchorfile>a00100.html</anchorfile>
      <anchor>aec5a601a849a4945a78345d8687bacf4</anchor>
      <arglist>(ISdmxDate dateFrom, ISdmxDate dateTo)</arglist>
    </member>
    <member kind="function">
      <type>string</type>
      <name>MapComponent</name>
      <anchorfile>a00100.html</anchorfile>
      <anchor>a09f97e051a04a58b7aaafaea2f64b5f2</anchor>
      <arglist>(IDataReader reader)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionMapping</name>
    <filename>a00101.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ComponentMapping</base>
    <member kind="function" static="yes">
      <type>static ITimeDimension</type>
      <name>Create</name>
      <anchorfile>a00101.html</anchorfile>
      <anchor>a68ff7bd3f4875b9b08832a5b00662c7d</anchor>
      <arglist>(MappingEntity mapping, IComponentMapping frequencyComponentMapping, string databaseType)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>TimeDimensionMapping</name>
      <anchorfile>a00101.html</anchorfile>
      <anchor>ac274a4ad90ab4ecd299e15112e4019a7</anchor>
      <arglist>(MappingEntity mapping, TimeExpressionEntity expression, string databaseType)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>string</type>
      <name>CreateSubStringClause</name>
      <anchorfile>a00101.html</anchorfile>
      <anchor>ae204e545eee4b7c6ac4684ca50825283</anchor>
      <arglist>(string columnName, int start, int length, string sqlOperator)</arglist>
    </member>
    <member kind="property" protection="protected" static="yes">
      <type>static IFormatProvider</type>
      <name>FormatProvider</name>
      <anchorfile>a00101.html</anchorfile>
      <anchor>a3e8662aaec3f205d3423a0c37eb2e850</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" protection="protected">
      <type>TimeExpressionEntity</type>
      <name>Expression</name>
      <anchorfile>a00101.html</anchorfile>
      <anchor>a5fcbc6cd87d2c1f8328a6ca8764bf7af</anchor>
      <arglist></arglist>
    </member>
    <member kind="property" protection="protected">
      <type>IPeriodicity</type>
      <name>Periodicity</name>
      <anchorfile>a00101.html</anchorfile>
      <anchor>af0ba830530d6badb361fc85a1bef121f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionSingleFrequency</name>
    <filename>a00102.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ITimeDimension</base>
    <member kind="function">
      <type></type>
      <name>TimeDimensionSingleFrequency</name>
      <anchorfile>a00102.html</anchorfile>
      <anchor>a24ffee26b269a74b57876a1817ce3d5b</anchor>
      <arglist>(ITimeDimensionMapping timeDimensionMapping)</arglist>
    </member>
    <member kind="function">
      <type>string</type>
      <name>GenerateWhere</name>
      <anchorfile>a00102.html</anchorfile>
      <anchor>adec417a55529f3f9c03d272020092775</anchor>
      <arglist>(ISdmxDate dateFrom, ISdmxDate dateTo, string frequencyValue)</arglist>
    </member>
    <member kind="function">
      <type>string</type>
      <name>MapComponent</name>
      <anchorfile>a00102.html</anchorfile>
      <anchor>af2efff6c3855ee171a9616dc01307b29</anchor>
      <arglist>(IDataReader reader, string frequencyValue)</arglist>
    </member>
    <member kind="property">
      <type>ComponentEntity</type>
      <name>Component</name>
      <anchorfile>a00102.html</anchorfile>
      <anchor>af2b6d3967fbf167c3edca109bb377b8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>MappingEntity</type>
      <name>Mapping</name>
      <anchorfile>a00102.html</anchorfile>
      <anchor>a00c124317efd70f2951f6eeeb81b6fbd</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TimeExpressionEntity</name>
    <filename>a00103.html</filename>
    <member kind="function" static="yes">
      <type>static TimeExpressionEntity</type>
      <name>CreateExpression</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>ac9ab9af11b3d3230a306c4d13937436c</anchor>
      <arglist>(TimeTranscodingEntity timeTranscoding)</arglist>
    </member>
    <member kind="property" static="yes">
      <type>static Regex</type>
      <name>TimeExpressionRegex</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>ab3ba33f08959aeecd6f9e8f98e1758c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>long</type>
      <name>DateColumnSysId</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a6e068ed2e777f2de00563a8b853d234b</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>TimeFormatEnumType</type>
      <name>Freq</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a11c001aa4215be432133d5b52f43af41</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>IsDateTime</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a815f0bb43e628297692aaf028d8b610f</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>bool</type>
      <name>OneColumnMapping</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>ad690e9b03cbff0f9a394579e8e2c0f1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>long</type>
      <name>PeriodColumnSysId</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a1eb7ebb7a92c05a9851af88d6d94a04b</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>int</type>
      <name>PeriodLength</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a31d957087fb5f7a359442a9bd2187805</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>int</type>
      <name>PeriodStart</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>ad3b10f84639d024014cb507b79741123</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>long</type>
      <name>YearColumnSysId</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a5d23bd2447ef6d8fe2c1af9e63dadc57</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>int</type>
      <name>YearLength</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a9f1f98a32c6319afd6250165622d0e61</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>int</type>
      <name>YearStart</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a3c2656c24abc43442118d2e042a9485a</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>TranscodingRulesEntity</type>
      <name>TranscodingRules</name>
      <anchorfile>a00103.html</anchorfile>
      <anchor>a63cab9d759dc708560b904f7b208fde7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TimeTranscodingCollection</name>
    <filename>a00104.html</filename>
    <member kind="function">
      <type></type>
      <name>TimeTranscodingCollection</name>
      <anchorfile>a00104.html</anchorfile>
      <anchor>a11eac176d051b2d15d1dd91ee90d35fe</anchor>
      <arglist>(long transcodingId)</arglist>
    </member>
    <member kind="function">
      <type>TimeTranscodingEntity</type>
      <name>Add</name>
      <anchorfile>a00104.html</anchorfile>
      <anchor>a399a26f625b55bacda44967c96595105</anchor>
      <arglist>(string frequencyValue)</arglist>
    </member>
    <member kind="function">
      <type>TimeTranscodingCollection</type>
      <name>DeepCopy</name>
      <anchorfile>a00104.html</anchorfile>
      <anchor>a6ee70938228301415fff5a6aae02b4fe</anchor>
      <arglist>(long transcodingId)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>override string</type>
      <name>GetKeyForItem</name>
      <anchorfile>a00104.html</anchorfile>
      <anchor>abea642536ffd49e36db980d6f96610c2</anchor>
      <arglist>(TimeTranscodingEntity item)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>override void</type>
      <name>InsertItem</name>
      <anchorfile>a00104.html</anchorfile>
      <anchor>aa69f8e921fa6a7dcb2681adb660aadb5</anchor>
      <arglist>(int index, TimeTranscodingEntity item)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>override void</type>
      <name>SetItem</name>
      <anchorfile>a00104.html</anchorfile>
      <anchor>a038b79005b2e3882c1d76d86010519be</anchor>
      <arglist>(int index, TimeTranscodingEntity item)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TimeTranscodingEntity</name>
    <filename>a00105.html</filename>
    <member kind="function">
      <type></type>
      <name>TimeTranscodingEntity</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>a676bc95348afd38e640398f449fd5a49</anchor>
      <arglist>(string frequencyValue, long transcodingId)</arglist>
    </member>
    <member kind="function">
      <type>TimeTranscodingEntity</type>
      <name>DeepCopy</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>a3ed75704da488d9d26dd01e6dab514d3</anchor>
      <arglist>(long transcodingId)</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>FrequencyValue</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>acfe71e39df8723b832861d6d5182424c</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>long</type>
      <name>TranscodingId</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>ac45a61d4f6c192fe59d840c78ecaef4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Expression</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>a1bf8d47a80dc5c39e999aee54171154d</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>long</type>
      <name>PeriodColumnId</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>a7a4c05967bb9e55c61b74d34b0091569</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>long</type>
      <name>YearColumnId</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>a7cfa523a7c875c7491025e99bafe8012</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>long</type>
      <name>DateColumnId</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>a6523895a467c486889250c677285c1c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>TranscodingRulesEntity</type>
      <name>TranscodingRules</name>
      <anchorfile>a00105.html</anchorfile>
      <anchor>a68a318b9d0be58ec588695de7fedbd52</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TranscodingEntity</name>
    <filename>a00106.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function">
      <type></type>
      <name>TranscodingEntity</name>
      <anchorfile>a00106.html</anchorfile>
      <anchor>a84baf3dba0a9e46dd8a6dec70f84a23e</anchor>
      <arglist>(long sysId)</arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>Expression</name>
      <anchorfile>a00106.html</anchorfile>
      <anchor>aff6d8b0805c69d0ff946c15b5855b648</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>TranscodingRulesEntity</type>
      <name>TranscodingRules</name>
      <anchorfile>a00106.html</anchorfile>
      <anchor>a1f9b505c0ff915373b74902db2bf52b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>TimeTranscodingCollection</type>
      <name>TimeTranscodingCollection</name>
      <anchorfile>a00106.html</anchorfile>
      <anchor>a0e8ccae00cc8adea15ca12c9e82c3d19</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TranscodingException</name>
    <filename>a00107.html</filename>
    <member kind="function">
      <type></type>
      <name>TranscodingException</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>a0c1413906a37e74ea8d52a0aea657aed</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>TranscodingException</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>a723c5fe907dc3838621c4df3b80e15ff</anchor>
      <arglist>(string message)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>TranscodingException</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>a49a8403b21e7cd8cf389bac7ac426322</anchor>
      <arglist>(string message, Exception innerException)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>TranscodingException</name>
      <anchorfile>a00107.html</anchorfile>
      <anchor>a23c6721be148ac9065270cd667d98fe3</anchor>
      <arglist>(SerializationInfo info, StreamingContext context)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TranscodingRulesEntity</name>
    <filename>a00108.html</filename>
    <base>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</base>
    <member kind="function">
      <type></type>
      <name>TranscodingRulesEntity</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>aca1bf5986af7987007fd6824c367df25</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Add</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>a0f7c64c94f2dee490317e57d0aa96a6f</anchor>
      <arglist>(CodeCollection localCodes, CodeCollection dsdCodes)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>AddColumn</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>a36f87105b6893011af0d0891785eaf4a</anchor>
      <arglist>(long name, int pos)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>AddComponent</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>adddb673dc55087f77dab49d1e24def1b</anchor>
      <arglist>(long name, int pos)</arglist>
    </member>
    <member kind="function">
      <type>CodeCollection</type>
      <name>GetDsdCodes</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>adea278e39784e77a98fe3f8980e46773</anchor>
      <arglist>(CodeCollection localCodes)</arglist>
    </member>
    <member kind="function">
      <type>CodeSetCollection</type>
      <name>GetLocalCodes</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>aae6fca7b606d8aa6677ca9eaea371bfc</anchor>
      <arglist>(CodeCollection dsdCodes)</arglist>
    </member>
    <member kind="function">
      <type>IEnumerable&lt; string &gt;</type>
      <name>GetLocalCodes</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>a5b605ec64a88de6c34b22615592831f8</anchor>
      <arglist>(long component, string dsdCode)</arglist>
    </member>
    <member kind="property">
      <type>Dictionary&lt; long, int &gt;</type>
      <name>ColumnAsKeyPosition</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>a6a443956fbe7f1fd1951693827e33476</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>Dictionary&lt; long, int &gt;</type>
      <name>ComponentAsKeyPosition</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>aad329c47f20b7e6bb91de5e9f156b770</anchor>
      <arglist></arglist>
    </member>
    <member kind="property">
      <type>string</type>
      <name>PeriodCodelist</name>
      <anchorfile>a00108.html</anchorfile>
      <anchor>aeaf99090aa802b3827f58e97c57d159d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>Estat</name>
    <filename>a00296.html</filename>
    <namespace>Estat::Sri</namespace>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sdmxsource::Extension::Constant</name>
    <filename>a00297.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sdmxsource::Extension::Extension</name>
    <filename>a00298.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sdmxsource::Extension::Manager</name>
    <filename>a00299.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri</name>
    <filename>a00300.html</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval</namespace>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval</name>
    <filename>a00301.html</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Builder</namespace>
    <namespace>Estat::Sri::MappingStoreRetrieval::Config</namespace>
    <namespace>Estat::Sri::MappingStoreRetrieval::Constants</namespace>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine</namespace>
    <namespace>Estat::Sri::MappingStoreRetrieval::Extensions</namespace>
    <namespace>Estat::Sri::MappingStoreRetrieval::Factory</namespace>
    <namespace>Estat::Sri::MappingStoreRetrieval::Helper</namespace>
    <namespace>Estat::Sri::MappingStoreRetrieval::Manager</namespace>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model</namespace>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::IncompleteMappingSetException</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::MappingStoreException</class>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Builder</name>
    <filename>a00302.html</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Builder::CrossReferenceChildBuilder</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Builder::ICrossReferenceRetrievalBuilder</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Builder::ICrossReferenceSetBuilder</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Builder::ItemTableInfoBuilder</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Builder::StructureReferenceFromMutableBuilder</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Builder::TableInfoBuilder</class>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Config</name>
    <filename>a00303.html</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::ConfigManager</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::DatabaseSetting</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::DatabaseSettingCollection</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::DataflowConfigurationSection</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::MappingStoreConfigSection</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::MappingStoreDefaultConstants</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::MastoreProviderMappingSetting</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Config::MastoreProviderMappingSettingCollection</class>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Constants</name>
    <filename>a00304.html</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Constants::AttachmentLevelConstants</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Constants::LocalisedStringType</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Constants::PeriodCodelist</class>
    <member kind="enumeration">
      <type></type>
      <name>SdmxComponentType</name>
      <anchorfile>a00304.html</anchorfile>
      <anchor>a70269d23cc6037a48db5643491a53cab</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>VersionQueryType</name>
      <anchorfile>a00304.html</anchorfile>
      <anchor>afa61fbfcad193c031a74304a3f5961bf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Engine</name>
    <filename>a00305.html</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</namespace>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::CrossReferenceResolverMutableEngine</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::HeaderRetrieverEngine</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::ICrossReferenceResolverMutableEngine</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::IRetrievalEngine</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::MappingSetRetriever</class>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Engine::Mapping</name>
    <filename>a00306.html</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ComponentMapping</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::IComponentMapping</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::IMapping</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ITimeDimension</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::ITimeDimensionMapping</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimension1To1</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionConstant</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionMapping</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TimeDimensionSingleFrequency</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Engine::Mapping::TranscodingException</class>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Extensions</name>
    <filename>a00307.html</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::AuthExtensions</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::DatabaseExtension</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::DbCommandExtension</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::HeaderExtensions</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::MutableMaintainableExtensions</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::QueryExtensions</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::SdmxDateExtensions</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Extensions::SdmxStructureTypeExtensions</class>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Factory</name>
    <filename>a00308.html</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::AdvancedMutableRetrievalManagerFactory</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::AuthAdvancedMutableRetrievalManagerFactory</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::AuthCrossMutableRetrievalManagerFactory</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::AuthMutableRetrievalManagerFactory</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::CrossMutableRetrievalManagerFactory</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::IAdvancedMutableRetrievalManagerFactory</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::IAuthAdvancedMutableRetrievalManagerFactory</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::IAuthCrossRetrievalManagerFactory</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::IAuthMutableRetrievalManagerFactory</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::ICrossRetrievalManagerFactory</class>
    <class kind="interface">Estat::Sri::MappingStoreRetrieval::Factory::IMutableRetrievalManagerFactory</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Factory::MutableRetrievalManagerFactory</class>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Helper</name>
    <filename>a00309.html</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Helper::ConnectionStringHelper</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Helper::DatabaseType</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Helper::DataReaderHelper</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Helper::MaintainableMutableComparer</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Helper::MappingUtils</class>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Manager</name>
    <filename>a00310.html</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AdvancedStructureRetriever</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthAdvancedStructureRetriever</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthCachedAdvancedStructureRetriever</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthCachedRetrievalManager</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthCrossReferenceRetrievalManager</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthMappingStoreRetrievalManager</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::AuthRetrievalManagerBase</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::CachedRetrievalManager</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::CrossReferenceRetrievalManager</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::Database</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::MappingStoreHeaderRetrievalManager</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::MappingStoreRetrievalManager</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::MappingStoreSdmxObjectRetrievalManager</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::RetrievalManagerBase</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Manager::SpecialMutableObjectRetrievalManager</class>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Model</name>
    <filename>a00311.html</filename>
    <namespace>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</namespace>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::ItemTableInfo</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::ListDictionary</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MaintainableDictionary</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MaintainableReferenceDictionary</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::PeriodObject</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::RetrievalSettings</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::SdmxQueryPeriod</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::TableInfo</class>
  </compound>
  <compound kind="namespace">
    <name>Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel</name>
    <filename>a00312.html</filename>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ArtefactEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CategoryEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CategorySchemeEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CodeCollection</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CodeListEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::CodeSetCollection</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ComponentEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ConceptEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ConnectionEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DataflowEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DataSetColumnEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DataSetEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::DsdEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::GroupEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::ItemEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::LocalisedStringType</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::MappingEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::MappingSetEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::PersistentEntityBase</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::SdmxQueryTimeVO</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TimeExpressionEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TimeTranscodingCollection</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TimeTranscodingEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TranscodingEntity</class>
    <class kind="class">Estat::Sri::MappingStoreRetrieval::Model::MappingStoreModel::TranscodingRulesEntity</class>
    <member kind="enumeration">
      <type></type>
      <name>AssignmentStatus</name>
      <anchorfile>a00312.html</anchorfile>
      <anchor>ac703ed9113b8c03f3a76c8b2e231e996</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>AttachmentLevel</name>
      <anchorfile>a00312.html</anchorfile>
      <anchor>a18ff2d3f3a687592dabe9d130489c906</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>CrossSectionalLevels</name>
      <anchorfile>a00312.html</anchorfile>
      <anchor>a67afe90492e37786a0cb64fdb8437fe0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>log4net</name>
    <filename>a00313.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmx::Resources::SdmxMl::Schemas::V21::Common</name>
    <filename>a00314.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Builder</name>
    <filename>a00315.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Constants</name>
    <filename>a00316.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Constants::InterfaceConstant</name>
    <filename>a00317.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Exception</name>
    <filename>a00318.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Manager::Retrieval</name>
    <filename>a00319.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Manager::Retrieval::Mutable</name>
    <filename>a00320.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Base</name>
    <filename>a00321.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Data::Query</name>
    <filename>a00322.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Header</name>
    <filename>a00323.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable</name>
    <filename>a00324.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable::Base</name>
    <filename>a00325.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable::CategoryScheme</name>
    <filename>a00326.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable::Codelist</name>
    <filename>a00327.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable::ConceptScheme</name>
    <filename>a00328.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable::DataStructure</name>
    <filename>a00329.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable::Mapping</name>
    <filename>a00330.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable::MetadataStructure</name>
    <filename>a00331.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable::Process</name>
    <filename>a00332.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable::Reference</name>
    <filename>a00333.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Mutable::Registry</name>
    <filename>a00334.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::Base</name>
    <filename>a00335.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::CategoryScheme</name>
    <filename>a00336.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::Codelist</name>
    <filename>a00337.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::ConceptScheme</name>
    <filename>a00338.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::DataStructure</name>
    <filename>a00339.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::Mapping</name>
    <filename>a00340.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::MetadataStructure</name>
    <filename>a00341.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::Process</name>
    <filename>a00342.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::Reference</name>
    <filename>a00343.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::Reference::Complex</name>
    <filename>a00344.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Model::Objects::Registry</name>
    <filename>a00345.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Api::Util</name>
    <filename>a00346.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::SdmxObjects::Model::Header</name>
    <filename>a00347.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::SdmxObjects::Model::Mutable::Base</name>
    <filename>a00348.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::SdmxObjects::Model::Mutable::CategoryScheme</name>
    <filename>a00349.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::SdmxObjects::Model::Mutable::Codelist</name>
    <filename>a00350.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::SdmxObjects::Model::Mutable::ConceptScheme</name>
    <filename>a00351.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::SdmxObjects::Model::Mutable::DataStructure</name>
    <filename>a00352.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::SdmxObjects::Model::Mutable::MetadataStructure</name>
    <filename>a00353.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::SdmxObjects::Model::Mutable::Reference</name>
    <filename>a00354.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::SdmxObjects::Model::Objects::Base</name>
    <filename>a00355.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::StructureRetrieval::Manager</name>
    <filename>a00356.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Util::Date</name>
    <filename>a00357.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Util::Objects</name>
    <filename>a00358.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Util::Objects::Annotation</name>
    <filename>a00359.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Util::Objects::Container</name>
    <filename>a00360.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Sdmx::Util::Objects::Reference</name>
    <filename>a00361.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Util</name>
    <filename>a00362.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Util::Collections</name>
    <filename>a00363.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Org::Sdmxsource::Util::Extensions</name>
    <filename>a00364.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System</name>
    <filename>a00365.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Collections</name>
    <filename>a00366.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Collections::Concurrent</name>
    <filename>a00367.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Collections::Generic</name>
    <filename>a00368.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Collections::ObjectModel</name>
    <filename>a00369.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Configuration</name>
    <filename>a00370.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Data</name>
    <filename>a00371.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Data::Common</name>
    <filename>a00372.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Diagnostics</name>
    <filename>a00373.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Globalization</name>
    <filename>a00374.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Linq</name>
    <filename>a00375.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Reflection</name>
    <filename>a00376.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Resources</name>
    <filename>a00377.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Runtime::CompilerServices</name>
    <filename>a00378.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Runtime::InteropServices</name>
    <filename>a00379.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Runtime::Serialization</name>
    <filename>a00380.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Text</name>
    <filename>a00381.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Text::RegularExpressions</name>
    <filename>a00382.html</filename>
  </compound>
  <compound kind="namespace">
    <name>System::Xml</name>
    <filename>a00383.html</filename>
  </compound>
  <compound kind="namespace">
    <name>TextType</name>
    <filename>a00384.html</filename>
  </compound>
  <compound kind="namespace">
    <name>VersionQueryType</name>
    <filename>a00385.html</filename>
  </compound>
</tagfile>

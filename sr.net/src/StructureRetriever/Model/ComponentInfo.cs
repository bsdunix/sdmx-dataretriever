// -----------------------------------------------------------------------
// <copyright file="ComponentInfo.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Model
{
    using Estat.Sri.MappingStoreRetrieval.Engine.Mapping;
    using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// A value object class for holding component related information
    /// </summary>
    internal class ComponentInfo
    {
        #region Constants and Fields

        /// <summary>
        ///   A reference to the code list used by the component
        /// </summary>
        private readonly MaintainableRefObjectImpl _codelistRef = new MaintainableRefObjectImpl();

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the reference to the code list used by the component
        /// </summary>
        public MaintainableRefObjectImpl CodelistRef
        {
            get
            {
                return this._codelistRef;
            }
        }

        /// <summary>
        ///   Gets or sets the <see cref="IComponentMapping" /> information of the component
        /// </summary>
        public IComponentMapping ComponentMapping { get; set; }

        /// <summary>
        ///   Gets or sets the <see cref="MappingEntity" /> used by the component
        /// </summary>
        public MappingEntity Mapping { get; set; }

        #endregion
    }
}
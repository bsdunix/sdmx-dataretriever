﻿// -----------------------------------------------------------------------
// <copyright file="AuthAdvancedMutableStructureSearchManagerBase.cs" company="EUROSTAT">
//   Date Created : 2013-09-20
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Manager
{
    using System.Collections.Generic;

    using Estat.Sdmxsource.Extension.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    /// The base class for <see cref="IAuthAdvancedMutableStructureSearchManager"/>
    /// </summary>
    public class AuthAdvancedMutableStructureSearchManagerBase
    {
        /// <summary>
        /// Retrieve the structures referenced by <paramref name="structureQuery" /> and populate the
        /// <paramref name="mutableObjects" />
        /// </summary>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="mutableObjects">The mutable objects.</param>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="allowedDataflows">The allowed Dataflows.</param>
        /// <param name="crossReferenceMutableRetrievalManager">The cross reference mutable retrieval manager.</param>
        protected virtual void PopulateMutables(
            IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalManager,
            IMutableObjects mutableObjects,
            IComplexStructureQuery structureQuery,
            IList<IMaintainableRefObject> allowedDataflows,
            IAuthCrossReferenceMutableRetrievalManager crossReferenceMutableRetrievalManager)
        {
            //// changes here might also apply to AuthMutableStructureSearchManagerBase and/or AuthStructureRetrieverV21 
            var complexStructureQueryDetail = structureQuery.StructureQueryMetadata != null ? structureQuery.StructureQueryMetadata.StructureQueryDetail : ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full);
            mutableObjects.AddIdentifiables(retrievalManager.GetMutableMaintainables(structureQuery.StructureReference, complexStructureQueryDetail, allowedDataflows));
        }
    }
}
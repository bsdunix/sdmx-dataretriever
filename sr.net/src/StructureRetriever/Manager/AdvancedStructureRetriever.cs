﻿// -----------------------------------------------------------------------
// <copyright file="AdvancedStructureRetriever.cs" company="EUROSTAT">
//   Date Created : 2013-09-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Manager
{
    using System.Configuration;

    using Estat.Nsi.StructureRetriever.Factory;
    using Estat.Sdmxsource.Extension.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    /// The advanced mutable structure search manager.
    /// </summary>
    public class AdvancedStructureRetriever : IAdvancedMutableStructureSearchManager
    {
        #region Fields

        /// <summary>
        /// The AUTH advanced mutable structure search manager.
        /// </summary>
        private readonly IAuthAdvancedMutableStructureSearchManager _authAdvancedMutableStructureSearchManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedStructureRetriever"/> class. 
        /// </summary>
        /// <param name="connectionStringSettings">
        /// The connection String Settings.
        /// </param>
        public AdvancedStructureRetriever(ConnectionStringSettings connectionStringSettings)
            : this(null, connectionStringSettings)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedStructureRetriever"/> class. 
        /// </summary>
        /// <param name="factory">
        /// The factory.
        /// </param>
        /// <param name="connectionStringSettings">
        /// The connection String Settings.
        /// </param>
        public AdvancedStructureRetriever(IStructureSearchManagerFactory<IAuthAdvancedMutableStructureSearchManager> factory, ConnectionStringSettings connectionStringSettings)
        {
            factory = factory ?? new AuthAdvancedMutableStructureSearchManagerFactory();
            this._authAdvancedMutableStructureSearchManager = factory.GetStructureSearchManager(connectionStringSettings, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get maintainables.
        /// </summary>
        /// <param name="structureQuery">
        /// The structure query.
        /// </param>
        /// <returns>
        /// The <see cref="IMutableObjects"/>.
        /// </returns>
        public IMutableObjects GetMaintainables(IComplexStructureQuery structureQuery)
        {
            return this._authAdvancedMutableStructureSearchManager.GetMaintainables(structureQuery, null);
        }

        #endregion
    }
}
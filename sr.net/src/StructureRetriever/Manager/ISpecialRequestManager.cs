// -----------------------------------------------------------------------
// <copyright file="ISpecialRequestManager.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Manager
{
    using System;

    using Estat.Nsi.StructureRetriever.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// The special request manager interface.
    /// </summary>
    internal interface ISpecialRequestManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Retrieve the codelist that is referenced by the given <paramref name="codelistRef"/> . 
        /// </summary>
        /// <param name="codelistRef">
        /// The codelist reference containing the id, agency and version of the requested dimension. Can be empty for time dimension 
        /// </param>
        /// <param name="info">
        /// The current structure retrieval state. 
        /// </param>
        /// <returns>
        /// The partial codelist 
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="codelistRef"/>
        ///   is null
        /// </exception>
        ICodelistMutableObject RetrieveAvailableData(IMaintainableRefObject codelistRef, StructureRetrievalInfo info);

        #endregion
    }
}
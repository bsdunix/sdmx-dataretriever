﻿// -----------------------------------------------------------------------
// <copyright file="IStructureSearchManagerFactory.cs" company="EUROSTAT">
//   Date Created : 2013-09-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Factory
{
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The StructureSearchManagerFactory interface.
    /// </summary>
    /// <typeparam name="TStructureSearch">
    /// The type of the structure search manager.
    /// </typeparam>
    public interface IStructureSearchManagerFactory<out TStructureSearch>
    {
        /// <summary>
        /// Returns an instance of the structure search manager created using the specified
        ///     <paramref name="settings"/>
        /// </summary>
        /// <typeparam name="T">
        /// The type of the settings
        /// </typeparam>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="sdmxSchemaVersion">
        /// The SDMX Schema Version.
        /// </param>
        /// <returns>
        /// The structure search manager.
        /// </returns>
        TStructureSearch GetStructureSearchManager<T>(T settings, SdmxSchema sdmxSchemaVersion);
    }
}
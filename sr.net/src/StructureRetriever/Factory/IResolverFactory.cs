﻿// -----------------------------------------------------------------------
// <copyright file="IResolverFactory.cs" company="EUROSTAT">
//   Date Created : 2013-09-16
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Factory
{
    using Estat.Nsi.StructureRetriever.Engines.Resolver;
    using Estat.Sdmxsource.Extension.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The <see cref="IResolver"/> Factory interface.
    /// </summary>
    public interface IResolverFactory
    {
        /// <summary>
        /// Returns the <see cref="IResolver" /> for the specified <paramref name="referenceDetailType"/>.
        /// </summary>
        /// <param name="referenceDetailType">Type of the reference detail.</param>
        /// <param name="crossReferenceManager">The cross reference manager.</param>
        /// <param name="specificStructureTypes">The specific object structure types.</param>
        /// <returns>The <see cref="IResolver" /> for the specified <paramref name="referenceDetailType"/>; otherwise null</returns>
        IResolver GetResolver(StructureReferenceDetail referenceDetailType, IAuthCrossReferenceMutableRetrievalManager crossReferenceManager, params SdmxStructureType[] specificStructureTypes);
    }
}
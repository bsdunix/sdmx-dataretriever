﻿// -----------------------------------------------------------------------
// <copyright file="StructureSearchManagerFactoryBase.cs" company="EUROSTAT">
//   Date Created : 2013-09-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Factory
{
    using System;
    using System.Collections.Generic;

    using Estat.Sri.MappingStoreRetrieval.Extensions;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The structure search manager base.
    /// </summary>
    /// <typeparam name="TStructureSearch">
    /// The structure search type
    /// </typeparam>
    public abstract class StructureSearchManagerFactoryBase<TStructureSearch> : IStructureSearchManagerFactory<TStructureSearch>
    {
        #region Fields

        /// <summary>
        ///     The user provided factory method.
        /// </summary>
        private readonly Func<object, TStructureSearch> _factoryMethod;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureSearchManagerFactoryBase{TStructureSearch}"/> class.
        /// </summary>
        /// <param name="factoryMethod">
        /// The factory method.
        /// </param>
        protected StructureSearchManagerFactoryBase(Func<object, TStructureSearch> factoryMethod)
        {
            this._factoryMethod = factoryMethod;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Returns an instance of the structure search manager created using the specified
        ///     <paramref name="settings"/>
        /// </summary>
        /// <typeparam name="T">
        /// The type of the settings
        /// </typeparam>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="sdmxSchemaVersion">
        /// The SDMX Schema Version.
        /// </param>
        /// <returns>
        /// The structure search manager.
        /// </returns>
        public TStructureSearch GetStructureSearchManager<T>(T settings, SdmxSchema sdmxSchemaVersion)
        {
            TStructureSearch manager = default(TStructureSearch);
            if (this._factoryMethod != null)
            {
                manager = this._factoryMethod(settings);
            }

            var isDefault = manager.IsDefault();
            if (isDefault)
            {
                Func<object, SdmxSchema, TStructureSearch> method;
                if (this.GetFactories().TryGetValue(typeof(T), out method))
                {
                    manager = method(settings, sdmxSchemaVersion);
                }
            }

            return manager;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the factories.
        /// </summary>
        /// <returns>The type based dictionary.</returns>
        protected abstract IDictionary<Type, Func<object, SdmxSchema, TStructureSearch>> GetFactories();

        #endregion
    }
}
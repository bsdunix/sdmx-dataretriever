// -----------------------------------------------------------------------
// <copyright file="AuthAdvancedMutableStructureSearchManagerFactory.cs" company="EUROSTAT">
//   Date Created : 2013-09-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sdmxsource.Extension.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;

    /// <summary>
    /// The authorization aware advanced mutable structure search manager factory.
    /// </summary>
    public class AuthAdvancedMutableStructureSearchManagerFactory : StructureSearchManagerFactoryBase<IAuthAdvancedMutableStructureSearchManager>
    {
        #region Static Fields

        /// <summary>
        ///     The default factory methods.
        /// </summary>
        private static readonly IDictionary<Type, Func<object, SdmxSchema, IAuthAdvancedMutableStructureSearchManager>> _factoryMethods =
            new Dictionary<Type, Func<object, SdmxSchema, IAuthAdvancedMutableStructureSearchManager>> { { typeof(ConnectionStringSettings), OnConnectionStringSettings } };

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthAdvancedMutableStructureSearchManagerFactory" /> class.
        /// </summary>
        public AuthAdvancedMutableStructureSearchManagerFactory()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthAdvancedMutableStructureSearchManagerFactory"/> class.
        /// </summary>
        /// <param name="factoryMethod">
        /// The factory method.
        /// </param>
        public AuthAdvancedMutableStructureSearchManagerFactory(Func<object, IAuthAdvancedMutableStructureSearchManager> factoryMethod)
            : base(factoryMethod)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the factories.
        /// </summary>
        /// <returns>The type based dictionary.</returns>
        protected override IDictionary<Type, Func<object, SdmxSchema, IAuthAdvancedMutableStructureSearchManager>> GetFactories()
        {
            return _factoryMethods;
        }

        /// <summary>
        /// The on connection string settings factory method.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="sdmxSchema">
        /// The SDMX schema.
        /// </param>
        /// <returns>
        /// The <see cref="IMutableStructureSearchManager"/>.
        /// </returns>
        private static IAuthAdvancedMutableStructureSearchManager OnConnectionStringSettings(object settings, SdmxSchema sdmxSchema)
        {
            var connectionStringSettings = settings as ConnectionStringSettings;
            if (settings == null)
            {
                return null;
            }

            SdmxSchemaEnumType sdmxSchemaEnumType = sdmxSchema != null ? sdmxSchema.EnumType : SdmxSchemaEnumType.Null;
            switch (sdmxSchemaEnumType)
            {
                case SdmxSchemaEnumType.VersionTwo:
                case SdmxSchemaEnumType.VersionOne:
                    return null;
                default:
                    return new AuthAdvancedStructureRetriever(connectionStringSettings);
            }
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="StructureRetrieverErrorTypes.cs" company="EUROSTAT">
//   Date Created : 2010-08-13
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever
{
    /// <summary>
    /// This enumeration contains the StructureRetrieverException error types.
    /// </summary>
    public enum StructureRetrieverErrorTypes
    {
        /// <summary>
        ///   Error parsing the input RegistryInterfaceBean
        /// </summary>
        ParsingError,

        /// <summary>
        ///   The mapping store DB takes too long to respond
        /// </summary>
        MappingStoreTimeout,

        /// <summary>
        ///   Could not establish a connection to the mapping store DB
        /// </summary>
        MappingStoreConnectionError,

        /// <summary>
        ///   Could not establish a connection to the mapping store DB, invalid credentials
        /// </summary>
        MappingStoreInvalidCredentials,

        /// <summary>
        ///   Requested structure not found
        /// </summary>
        MissingStructure,

        /// <summary>
        ///   A reference of the requested structure was not found and IsResolveReferences = true
        /// </summary>
        MissingStructureRef,

        /// <summary>
        ///   Could not establish a connection to the dissemination DB
        /// </summary>
        DdbConnectionError
    }
}
// -----------------------------------------------------------------------
// <copyright file="SimpleCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using Estat.Nsi.StructureRetriever.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// A simple mapping store codelists retrieval engine
    /// </summary>
    internal class SimpleCodeListRetrievalEngine : ICodeListRetrievalEngine
    {
        #region Public Methods and Operators

        /// <summary>
        /// Retrieve Codelist
        /// </summary>
        /// <param name="info">
        /// The current StructureRetrieval state 
        /// </param>
        /// <returns>
        /// A <see cref="CodeListBean"/> 
        /// </returns>
        public ICodelistMutableObject GetCodeList(StructureRetrievalInfo info)
        {
            ISet<ICodelistMutableObject> codeLists =
                info.MastoreAccess.GetMutableCodelistObjects(new MaintainableRefObjectImpl(info.CodelistRef.AgencyId, 
                                                                                            info.CodelistRef.MaintainableId,
                                                                                            info.CodelistRef.Version));
            return CodeListHelper.GetFirstCodeList(codeLists);
        }

        #endregion
    }
}
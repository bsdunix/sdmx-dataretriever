// -----------------------------------------------------------------------
// <copyright file="TranscodedCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;

    using Estat.Nsi.StructureRetriever.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Codelist retrieval engine that gets the codelist from transcoding configuration. It implies that no criteria
    /// </summary>
    internal class TranscodedCodeListRetrievalEngine : ICodeListRetrievalEngine
    {
        #region Constants and Fields

        /// <summary>
        /// The _is transcoded.
        /// </summary>
        private readonly bool _isTranscoded;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TranscodedCodeListRetrievalEngine"/> class.
        /// </summary>
        /// <param name="isTranscoded">
        /// The is transcoded.
        /// </param>
        public TranscodedCodeListRetrievalEngine(bool isTranscoded)
        {
            this._isTranscoded = isTranscoded;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Retrieve Codelist
        /// </summary>
        /// <param name="info">
        /// The current StructureRetrieval state 
        /// </param>
        /// <returns>
        /// A <see cref="CodeListBean"/> 
        /// </returns>
        public ICodelistMutableObject GetCodeList(StructureRetrievalInfo info)
        {
            var dataflowrRef = new MaintainableRefObjectImpl(info.MappingSet.Dataflow.Agency, info.MappingSet.Dataflow.Id, info.MappingSet.Dataflow.Version);
            ISet<ICodelistMutableObject> codeLists = info.MastoreAccess.GetMutableCodelistObjects(info.CodelistRef,dataflowrRef, info.RequestedComponent, this._isTranscoded, info.AllowedDataflows);

            return CodeListHelper.GetFirstCodeList(codeLists);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="TranscodedCodeListRetrievalEngine"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="TranscodedCodeListRetrievalEngine"/>.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}(isTranscoded={1})", base.ToString(), this._isTranscoded);
        }

        #endregion
    }
}
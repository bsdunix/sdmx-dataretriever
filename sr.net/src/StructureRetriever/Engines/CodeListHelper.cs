﻿// -----------------------------------------------------------------------
// <copyright file="CodeListHelper.cs" company="EUROSTAT">
//   Date Created : 2012-04-06
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;

    /// <summary>
    /// This class contains helper method for codelist retrieval engines
    /// </summary>
    internal static class CodeListHelper
    {
        /// <summary>
        /// Gets the first codelist from <paramref name="codeLists"/> if any
        /// </summary>
        /// <param name="codeLists">
        /// The code lists.
        /// </param>
        /// <returns>
        /// The first codelist from <paramref name="codeLists"/>; otherwise null
        /// </returns>
        public static ICodelistMutableObject GetFirstCodeList(ISet<ICodelistMutableObject> codeLists)
        {
            ICodelistMutableObject firstCL = null;
            foreach (ICodelistMutableObject cl in codeLists)
            {
                firstCL = cl;
                break;
            }
            return codeLists != null && codeLists.Count != 0 ? firstCL : null;
        }
    }
}
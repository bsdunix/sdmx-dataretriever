// -----------------------------------------------------------------------
// <copyright file="PartialCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.Common;

    using Estat.Nsi.StructureRetriever.Builders;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.MappingStoreRetrieval.Engine.Mapping;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// This <see cref="ICodeListRetrievalEngine"/> retrieves partial codelists from DDB and Mastore.
    /// </summary>
    internal class PartialCodeListRetrievalEngine : ICodeListRetrievalEngine
    {
        #region Public Methods and Operators

        /// <summary>
        /// Retrieve Codelist
        /// </summary>
        /// <param name="info">
        /// The current StructureRetrieval state 
        /// </param>
        /// <returns>
        /// A <see cref="CodeListBean"/> 
        /// </returns>
        public ICodelistMutableObject GetCodeList(StructureRetrievalInfo info)
        {
            if (info.RequestedComponentInfo == null)
            {
                return null;
            }

            var codesSet = new Dictionary<string, object>();
            IComponentMapping cmap = info.RequestedComponentInfo.ComponentMapping;
            using (DbConnection ddbConnection = DDbConnectionBuilder.Instance.Build(info))
            {
                using (DbCommand cmd = ddbConnection.CreateCommand())
                {
                    cmd.CommandText = info.SqlQuery;
                    cmd.CommandTimeout = 0;
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string dsdCode = cmap.MapComponent(reader);
                            if (dsdCode != null && !codesSet.ContainsKey(dsdCode))
                            {
                                codesSet.Add(dsdCode, null);
                            }
                        }
                    }
                }
            }

            if (codesSet.Count > 0)
            {
                var subset = new List<string>(codesSet.Keys);
                ISet<ICodelistMutableObject> codeLists =
                    info.MastoreAccess.GetMutableCodelistObjects(
                        new MaintainableRefObjectImpl(info.CodelistRef.AgencyId, 
                                                        info.CodelistRef.MaintainableId,
                                                        info.CodelistRef.Version
                                                        ),
                                                        subset);

                return CodeListHelper.GetFirstCodeList(codeLists);
            }

            return null;
        }

        #endregion
    }
}
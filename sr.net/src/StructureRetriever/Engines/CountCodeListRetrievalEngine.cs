// -----------------------------------------------------------------------
// <copyright file="CountCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines
{
    using System;
    using System.Data.Common;
    using System.Globalization;

    using Estat.Nsi.StructureRetriever.Builders;
    using Estat.Nsi.StructureRetriever.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

    /// <summary>
    /// The COUNT code list retrieval
    /// </summary>
    internal class CountCodeListRetrievalEngine : ICodeListRetrievalEngine
    {
        #region Public Methods and Operators

        /// <summary>
        /// Retrieve Codelist
        /// </summary>
        /// <param name="info">
        /// The current StructureRetrieval state 
        /// </param>
        /// <returns>
        /// A <see cref="ICodelistMutableObject"/> 
        /// </returns>
        public ICodelistMutableObject GetCodeList(StructureRetrievalInfo info)
        {
            var codelist = new CodelistMutableCore();
            var name = new TextTypeWrapperMutableCore { Locale = CustomCodelistConstants.Lang, Value = CustomCodelistConstants.CountCodeListName, };
            codelist.Names.Add(name);
            codelist.Id = CustomCodelistConstants.CountCodeList;
            codelist.AgencyId = CustomCodelistConstants.Agency;
            codelist.Version = CustomCodelistConstants.Version;
            int xsMeasureMult = 1;

            if (info.MeasureComponent != null)
            {
                info.Logger.Info("|-- Get XS Measure count");
                xsMeasureMult = GetXsMeasureCount(info);
            }

            object value = ExecuteSql(info);

            // setup count codelist
            var countCode = new CodeMutableCore();
            var text = new TextTypeWrapperMutableCore { Locale = CustomCodelistConstants.Lang, Value = CustomCodelistConstants.CountCodeDescription, };
            countCode.Names.Add(text);

            // normally count(*) should always return a number. Checking just in case I missed something.
            if (value != null && !Convert.IsDBNull(value))
            {
                // in .net, oracle will return 128bit decimal, sql server 32bit int, while mysql & sqlite 64bit long.
                long count = Convert.ToInt64(value, CultureInfo.InvariantCulture);

                // check if there are XS measure mappings. In this case there could be multiple measures/obs per row. 
                // even if they are not, then will be static mappings
                count *= xsMeasureMult;

                countCode.Id = count.ToString(CultureInfo.InvariantCulture);
                codelist.AddItem(countCode);
            }
            else
            {
                countCode.Id = CustomCodelistConstants.CountCodeDefault;
            }

            return codelist;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Execute the <see cref="StructureRetrievalInfo.SqlQuery"/> against the DDB and get a single value
        /// </summary>
        /// <param name="info">
        /// The current Structure retrieval state 
        /// </param>
        /// <returns>
        /// The scalar value 
        /// </returns>
        /// <exception cref="DbException">
        /// DDB communication error
        /// </exception>
        private static object ExecuteSql(StructureRetrievalInfo info)
        {
            object value;
            using (DbConnection ddbConnection = DDbConnectionBuilder.Instance.Build(info))
            {
                using (DbCommand cmd = ddbConnection.CreateCommand())
                {
                    cmd.CommandText = info.SqlQuery;
                    cmd.CommandTimeout = 0;
                    value = cmd.ExecuteScalar();
                }
            }

            return value;
        }

        /// <summary>
        /// Get CrossSectional Measure count.
        /// </summary>
        /// <param name="info">
        /// The current Structure retrieval state 
        /// </param>
        /// <returns>
        /// The CrossSectional Measure count. 
        /// </returns>
        private static int GetXsMeasureCount(StructureRetrievalInfo info)
        {
            int xsMeasureCount = info.XSMeasureDimensionConstraints.Count;
            foreach (var member in info.Criteria)
            {
                if (member.Id.Equals(info.MeasureComponent))
                {
                    // get the unmapped measure dimension XS codes to display
                    xsMeasureCount = 0;
                    foreach (string value in member.Values)
                    {
                        if (value != null
                            && info.XSMeasureDimensionConstraints.ContainsKey(value))
                        {
                            xsMeasureCount++;
                        }
                    }
                }
            }

            return xsMeasureCount == 0 ? 1 : xsMeasureCount;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="TimeDimensionCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines
{
    using System.Data;
    using System.Data.Common;

    using Estat.Nsi.StructureRetriever.Builders;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    /// Time Dimension start and optionally end code list retrieval engine
    /// </summary>
    internal class TimeDimensionCodeListRetrievalEngine : ICodeListRetrievalEngine
    {
        #region Public Methods and Operators

        /// <summary>
        /// Retrieve Codelist
        /// </summary>
        /// <param name="info">
        /// The current StructureRetrieval state 
        /// </param>
        /// <returns>
        /// A <see cref="ICodelistMutableObject"/> 
        /// </returns>
        public ICodelistMutableObject GetCodeList(StructureRetrievalInfo info)
        {
            ISdmxDate minPeriod = new SdmxDateCore(CustomCodelistConstants.TimePeriodMax);
            ISdmxDate maxPeriod = new SdmxDateCore(CustomCodelistConstants.TimePeriodMin);

            var frequencyMapping = info.FrequencyInfo != null ? info.FrequencyInfo.ComponentMapping : null;
            var timeTranscoder = info.TimeTranscoder;
            using (DbConnection ddbConnection = DDbConnectionBuilder.Instance.Build(info))
            using (DbCommand cmd = ddbConnection.CreateCommand())
            {
                cmd.CommandTimeout = 0;
                cmd.CommandText = info.SqlQuery;
                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string frequency = frequencyMapping != null ? frequencyMapping.MapComponent(reader) : null; 
                        string period = timeTranscoder.MapComponent(reader, frequency);
                        if (!string.IsNullOrEmpty(period))
                        {
                            ISdmxDate currentPeriod = new SdmxDateCore(period);
                            if (currentPeriod.StartsBefore(minPeriod))
                            {
                                minPeriod = currentPeriod;
                            }

                            if (currentPeriod.EndsAfter(maxPeriod))
                            {
                                maxPeriod = currentPeriod;
                            }
                        }
                    }
                }
            }

            return TimeDimensionCodeListHelper.BuildTimeCodelist(minPeriod, maxPeriod);
        }

        #endregion
    }
}
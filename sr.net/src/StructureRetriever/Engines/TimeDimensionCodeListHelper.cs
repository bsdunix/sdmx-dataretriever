﻿// -----------------------------------------------------------------------
// <copyright file="TimeDimensionCodeListHelper.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines
{
    using Estat.Sri.MappingStoreRetrieval.Extensions;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

    /// <summary>
    /// A collection of helper methods for Time Dimension Codelist
    /// </summary>
    internal static class TimeDimensionCodeListHelper
    {
        #region Public Methods and Operators

        /// <summary>
        /// Setup a CodelistBean object as a TimeDimension codelist containing two codes, startTime and endTime and using as codelist id the <see cref="CustomCodelistConstants.TimePeriodCodeList"/> .
        /// </summary>
        /// <param name="startCode">
        /// The code that will contain the start Time period 
        /// </param>
        /// <param name="endCode">
        /// The code that will contain the end Time period 
        /// </param>
        /// <param name="timeCodeList">
        /// The codelist to setup 
        /// </param>
        public static void SetupTimeCodelist(ICodeMutableObject startCode, ICodeMutableObject endCode, ICodelistMutableObject timeCodeList)
        {
            timeCodeList.Id = CustomCodelistConstants.TimePeriodCodeList;
            timeCodeList.AgencyId = CustomCodelistConstants.Agency;
            timeCodeList.Version = CustomCodelistConstants.Version;
            timeCodeList.Names.Add(
                new TextTypeWrapperMutableCore { Locale = CustomCodelistConstants.Lang, Value = CustomCodelistConstants.TimePeriodCodeListName });
            startCode.Names.Add(
                new TextTypeWrapperMutableCore { Locale = CustomCodelistConstants.Lang, Value = CustomCodelistConstants.TimePeriodStartDescription });
            timeCodeList.AddItem(startCode);

            if (endCode != null)
            {
                endCode.Names.Add(
                    new TextTypeWrapperMutableCore { Locale = CustomCodelistConstants.Lang, Value = CustomCodelistConstants.TimePeriodEndDescription });

                timeCodeList.AddItem(endCode);
            }
        }

        #endregion

        /// <summary>
        /// Builds the time codelist.
        /// </summary>
        /// <param name="minPeriod">The minimum period.</param>
        /// <param name="maxPeriod">The maximum period.</param>
        /// <returns>the time codelist.</returns>
        public static ICodelistMutableObject BuildTimeCodelist(ISdmxDate minPeriod, ISdmxDate maxPeriod)
        {
            ICodelistMutableObject timeCodeList = new CodelistMutableCore();
            var startDate = minPeriod.FormatAsDateString(true);
            ICodeMutableObject startCode = new CodeMutableCore { Id = startDate };
            var endDate = maxPeriod.FormatAsDateString(false);
            ICodeMutableObject endCode = !startDate.Equals(endDate) ? new CodeMutableCore { Id = endDate } : null;
            SetupTimeCodelist(startCode, endCode, timeCodeList);
            return timeCodeList;
        }
    }
}
// -----------------------------------------------------------------------
// <copyright file="ComponentSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Builders
{
    using System.Globalization;
    using System.IO.IsolatedStorage;
    using System.Text;

    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;

    /// <summary>
    /// SQL Builder for Components
    /// </summary>
    internal class ComponentSqlBuilder : SqlBuilderBase
    {
        #region Public Methods and Operators

        /// <summary>
        /// Generate the SQL for executing on the DDB
        /// </summary>
        /// <param name="info">
        /// The current structure retrieval information 
        /// </param>
        /// <returns>
        /// The generated sql. 
        /// </returns>
        public override string GenerateSql(StructureRetrievalInfo info)
        {
            MappingEntity[] mapping;

            if (info.RequestedComponentInfo != null)
            {
                mapping = new[] { info.RequestedComponentInfo.Mapping };
            }
            else if (info.RequestedComponent.Equals(info.TimeDimension))
            {
                mapping = info.FrequencyInfo != null ? new[] { info.TimeMapping, info.FrequencyInfo.Mapping } : new[] { info.TimeMapping };
            }
            else
            {
                return null;
            }

            var columnList = ToColumnNameString(mapping);

            return string.Format(
                CultureInfo.InvariantCulture,
                "SELECT DISTINCT {2} \n FROM ({0}) virtualDataset {1} \n ORDER BY {2} ",
                info.InnerSqlQuery,
                GenerateWhere(info),
                columnList);
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="IStructureRetrievalInfoBuilder.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.CustomRequests.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// The <see cref="StructureRetrievalInfo"/> Builder interface
    /// </summary>
    internal interface IStructureRetrievalInfoBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        /// Build a <see cref="StructureRetrievalInfo"/> from the specified parameters
        /// </summary>
        /// <param name="dataflow">
        /// The datflow to get the available data for 
        /// </param>
        /// <param name="connectionStringSettings">
        /// The Mapping Store connection string settings 
        /// </param>
        /// <param name="logger">
        /// The logger to log information, warnings and errors. It can be null 
        /// </param>
        /// <param name="allowedDataflows">
        /// The collection of allowed dataflows 
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// connectionStringSettings is null
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// dataflow is null
        /// </exception>
        /// <exception cref="StructureRetrieverException">
        /// Parsing error or mapping store exception error
        /// </exception>
        /// <returns>
        /// a <see cref="StructureRetrievalInfo"/> from the specified parameters 
        /// </returns>
        StructureRetrievalInfo Build(
            IConstrainableStructureReference dataflow,
            ConnectionStringSettings connectionStringSettings,
            IList<IMaintainableRefObject> allowedDataflows);

        #endregion
    }
}
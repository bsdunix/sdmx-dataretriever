﻿// -----------------------------------------------------------------------
// <copyright file="IBuilder.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Builders
{
    /// <summary>
    /// A generic builder interface which builds a <typeparamref name="T"/> from a <typeparamref name="TE"/> using the <see cref="Build"/> method
    /// </summary>
    /// <typeparam name="T">
    /// The output type 
    /// </typeparam>
    /// <typeparam name="TE">
    /// The input type 
    /// </typeparam>
    internal interface IBuilder<T, TE>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The method that builds a <typeparamref name="T"/> from a <typeparamref name="TE"/>
        /// </summary>
        /// <param name="info">
        /// The input as <typeparamref name="TE"/> . 
        /// </param>
        /// <returns>
        /// The output as <typeparamref name="T"/> 
        /// </returns>
        T Build(TE info);

        #endregion
    }
}
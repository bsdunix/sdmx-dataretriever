// -----------------------------------------------------------------------
// <copyright file="CountSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Builders
{
    using System.Globalization;

    using Estat.Nsi.StructureRetriever.Model;

    /// <summary>
    /// Build SQL query for Count requests
    /// </summary>
    internal class CountSqlBuilder : SqlBuilderBase
    {
        #region Public Methods and Operators

        /// <summary>
        /// Generate the SQL for executing on the DDB
        /// </summary>
        /// <param name="info">
        /// The current structure retrieval information 
        /// </param>
        /// <returns>
        /// The generated sql. 
        /// </returns>
        public override string GenerateSql(StructureRetrievalInfo info)
        {
            return string.Format(
                CultureInfo.InvariantCulture,
                "SELECT COUNT(*) \n FROM ({0}) virtualDataset {1}",
                info.InnerSqlQuery,
                GenerateWhere(info));
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="DDbConnectionBuilder.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Builders
{
    using System.Configuration;
    using System.Data.Common;

    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    /// Dissemination database connection builder
    /// </summary>
    internal class DDbConnectionBuilder : IBuilder<DbConnection, StructureRetrievalInfo>
    {
        #region Constants and Fields

        /// <summary>
        ///   The sigleton instance
        /// </summary>
        private static readonly DDbConnectionBuilder _instance = new DDbConnectionBuilder();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Prevents a default instance of the <see cref="DDbConnectionBuilder"/> class from being created. 
        ///   Prevents a default instance of the <see cref="DDbConnectionBuilder"/> class from being created.
        /// </summary>
        private DDbConnectionBuilder()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the singleton instance of this class
        /// </summary>
        public static DDbConnectionBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The method that builds a <see cref="DbConnection"/> from the specified <paramref name="info"/>
        /// </summary>
        /// <param name="info">
        /// The current Data retrieval state 
        /// </param>
        /// <returns>
        /// The <see cref="DbConnection"/> 
        /// </returns>
        public DbConnection Build(StructureRetrievalInfo info)
        {
            string dissdbConnectionString = info.MappingSet.DataSet.Connection.AdoConnectionString;
            string providerName = DatabaseType.GetProviderName(info.MappingSet.DataSet.Connection.DBType);
            var disseminationDb = new Database(new ConnectionStringSettings(info.MappingSet.DataSet.Connection.Name, dissdbConnectionString, providerName));
            DbConnection dbConnection = disseminationDb.CreateConnection();
            dbConnection.Open();
            return dbConnection;
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ComplexDataQueryBuilderManager.cs" company="EUROSTAT">
//   Date Created : 2013-08-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    using Estat.Sri.CustomRequests.Builder;
    using Estat.Sri.CustomRequests.Factory;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ComplexDataQueryBuilderManager : IComplexDataQueryBuilderManager
    {
        private readonly IComplexDataQueryFactory _factory;
        /// <summary>
        /// 
        /// </summary>
        public ComplexDataQueryBuilderManager(IComplexDataQueryFactory factory)
        {
            _factory = factory;
        }

        public XDocument BuildComplexDataQuery(IComplexDataQuery complexDataQuery, IDataQueryFormat<XDocument> dataQueryFormat)
        {
            IComplexDataQueryBuilder builder = _factory.GetComplexDataQueryBuilder(dataQueryFormat);
            if (builder != null)
            {
                return builder.BuildComplexDataQuery(complexDataQuery);
            }
            throw new SdmxUnauthorisedException("Unsupported ComplexDataQueryFormat: " + dataQueryFormat);
        }
    }
}

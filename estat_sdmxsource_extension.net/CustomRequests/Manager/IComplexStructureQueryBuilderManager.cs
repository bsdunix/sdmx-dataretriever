﻿// -----------------------------------------------------------------------
// <copyright file="IComplexStructureQueryBuilderManager.cs" company="EUROSTAT">
//   Date Created : 2013-08-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    /// Builds a StructureQuery in the required format 
    /// </summary>
    public interface IComplexStructureQueryBuilderManager<T>
    {
        /// <summary>
        /// Builds a complex structure query in the requested format
        /// </summary>
        /// <param name="complexStructureQuery">complexStructureQuery the query to build a representation of</param>
        /// <param name="structureQueryFormat">the required format</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>Representation of query in the desired format.</returns>
        T BuildComplexStructureQuery(IComplexStructureQuery complexStructureQuery, IStructureQueryFormat<T> structureQueryFormat);
    }
}

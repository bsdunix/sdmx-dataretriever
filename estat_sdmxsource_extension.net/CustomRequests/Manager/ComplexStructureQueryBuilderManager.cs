﻿// -----------------------------------------------------------------------
// <copyright file="ComplexStructureQueryBuilderManager.cs" company="EUROSTAT">
//   Date Created : 2013-08-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Manager
{
    using Estat.Sri.CustomRequests.Factory;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ComplexStructureQueryBuilderManager<T> : IComplexStructureQueryBuilderManager<T>
    {
        private readonly IComplexStructureQueryFactory<T> _factory;

       /// <summary>
       /// Checks each ComplexStructureQueryFactory registered to the Spring beans framework asking each one in turn to
       /// obtain a query builder.  The StructureQueryFactory to respond with a not null value, will be returned 
       /// </summary>
       /// <param name="factory"></param>
       public ComplexStructureQueryBuilderManager(IComplexStructureQueryFactory<T> factory)
       {
           this._factory = factory;
       }

        public T BuildComplexStructureQuery(IComplexStructureQuery query, IStructureQueryFormat<T> structureQueryFormat)
        {
            var builder =  this._factory.GetComplexStructureQueryBuilder(structureQueryFormat);
			if(builder != null) {
                return builder.BuildComplexStructureQuery(query);
			}
		
		throw new SdmxUnauthorisedException("Unsupported ComplexStructureQueryFormat: " + structureQueryFormat);
        }
    }
}

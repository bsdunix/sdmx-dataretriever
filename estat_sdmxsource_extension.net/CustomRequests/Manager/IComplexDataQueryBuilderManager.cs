﻿// -----------------------------------------------------------------------
// <copyright file="IComplexDataQueryBuilderManager.cs" company="EUROSTAT">
//   Date Created : 2013-08-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Manager
{
    using System.Xml.Linq;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// Builds a DataQuery in the required format
    /// </summary>
    public interface IComplexDataQueryBuilderManager
    {
        /// <summary>
        /// Builds a complex data query in the requested format
        /// </summary>
        /// <param name="complexDataQuery">The query to build a representation of</param>
        /// <param name="dataQueryFormat">The required format</param>
        /// <returns>Query in the desired format</returns>
        XDocument BuildComplexDataQuery(IComplexDataQuery complexDataQuery, IDataQueryFormat<XDocument> dataQueryFormat);
    }
}

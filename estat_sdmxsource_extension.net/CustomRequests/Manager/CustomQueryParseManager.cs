﻿// -----------------------------------------------------------------------
// <copyright file="CustomQueryParseManager.cs" company="EUROSTAT">
//   Date Created : 2013-03-28
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Manager
{
    using Estat.Sri.CustomRequests.Builder;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.Query;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;

    /// <summary>
    /// A <c>QueryStructureRequest</c> and structure query parser. Supports Constraints in <c>Dataflow</c> references in <c>SDMX</c> v2.0 <c>QueryStructureRequest</c>.
    /// </summary>
    /// <remarks>
    /// This class extends <see cref="QueryParsingManager"/> and uses the <see cref="ConstrainQueryBuilderV2"/> for SDMX v2.0 Query Structure parsing.
    /// </remarks>
    public class CustomQueryParseManager : QueryParsingManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomQueryParseManager"/> class.
        /// </summary>
        /// <param name="sdmxSchema">
        /// The SDMX schema.
        /// </param>
        public CustomQueryParseManager(SdmxSchemaEnumType sdmxSchema)
            : base(sdmxSchema, new QueryBuilder(null, new ConstrainQueryBuilderV2(), null))
        {
        }
    }
}
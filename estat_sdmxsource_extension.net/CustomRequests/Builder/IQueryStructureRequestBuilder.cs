﻿// -----------------------------------------------------------------------
// <copyright file="IQueryStructureRequestBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-03-28
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// Responsible for building a Query Structure Request that can be used to query services external to the 
    /// <c>SdmxSource</c> framework
    /// </summary>
    /// <typeparam name="T">The output query type</typeparam>
    public interface IQueryStructureRequestBuilder<out T>
    {
        /// <summary>
        /// Builds a <c>QueryStructureRequest</c> that matches the passed in format
        /// </summary>
        /// <param name="queries">
        ///     The queries.
        /// </param>
        /// <param name="resolveReferences">
        /// Set to <c>True</c> to resolve references.
        /// </param>
        /// <returns>
        /// The <typeparamref name="T"/> from <paramref name="queries"/>.
        /// </returns>
        T BuildStructureQuery(IEnumerable<IStructureReference> queries, bool resolveReferences);
    }
}
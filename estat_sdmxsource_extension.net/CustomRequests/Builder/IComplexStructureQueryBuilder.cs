﻿// -----------------------------------------------------------------------
// <copyright file="IComplexStructureQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-08-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    /// Responsible for building a Complex Structure Query
    /// that can be used to query services external to the SdmxSource framework
    /// </summary>
    public interface IComplexStructureQueryBuilder<T>
    {
        /// <summary>
        /// Builds a ComplexStructureQuery that matches the passed in format
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        T BuildComplexStructureQuery(IComplexStructureQuery query);

    }
}

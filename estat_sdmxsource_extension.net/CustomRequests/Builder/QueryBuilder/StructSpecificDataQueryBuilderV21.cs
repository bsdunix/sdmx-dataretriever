﻿// -----------------------------------------------------------------------
// <copyright file="StructSpecificDataQueryBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-08-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder.QueryBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21;

    using DataQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.DataQueryType;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class StructSpecificDataQueryBuilderV21 : IComplexDataQueryBuilder
    {

        public XDocument BuildComplexDataQuery(IComplexDataQuery query)
        {
            var queryMessageType = new DataQueryType();

            queryMessageType.Header = new BasicHeaderType();
            V21Helper.SetHeader(queryMessageType.Header, null);

            var queryType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.DataQueryType();

            var coreBuilder = new ComplexDataQueryCoreBuilderV21();
            coreBuilder.FillDataQueryType(queryType, query);
            queryMessageType.BaseDataQueryType = queryType;
            var queryMessageDocument = new StructureSpecificDataQuery(queryMessageType);

            return new XDocument(queryMessageDocument.Untyped);
        }
    }
}

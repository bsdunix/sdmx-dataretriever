﻿// -----------------------------------------------------------------------
// <copyright file="ParameterBuilderOr.cs" company="EUROSTAT">
//   Date Created : 2014-10-31
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder.QueryBuilder
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;

    /// <summary>
    ///     The parameter builder for OR.
    /// </summary>
    internal class ParameterBuilderOr : IParameterBuilder
    {
        #region Fields

        /// <summary>
        ///     The _data parameters or type
        /// </summary>
        private DataParametersOrType _dataParametersOrType;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ParameterBuilderOr" /> class.
        /// </summary>
        public ParameterBuilderOr()
        {
            this.Reset();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Adds the attribute.
        /// </summary>
        /// <param name="attributeValueType">
        /// Type of the attribute value.
        /// </param>
        public void AddAttribute(AttributeValueType attributeValueType)
        {

            this._dataParametersOrType.AttributeValue.Add(attributeValueType);
        }

        /// <summary>
        /// Adds the dimension.
        /// </summary>
        /// <param name="dimension">
        /// The dimension.
        /// </param>
        public void AddDimension(DimensionValueType dimension)
        {
            this._dataParametersOrType.DimensionValue.Add(dimension);
        }

        /// <summary>
        /// Populates the AND parameter.
        /// </summary>
        /// <param name="andParameter">
        /// The and parameter.
        /// </param>
        public void PopulateAndParameter(DataParametersAndType andParameter)
        {
            andParameter.Or.Add(this._dataParametersOrType);
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
            this._dataParametersOrType = new DataParametersOrType();
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ParameterBuilderAnd.cs" company="EUROSTAT">
//   Date Created : 2014-10-31
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Builder.QueryBuilder
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The parameter builder for AND.
    /// </summary>
    internal class ParameterBuilderAnd : IParameterBuilder
    {
        #region Fields

        /// <summary>
        ///     The _attributes
        /// </summary>
        private readonly List<AttributeValueType> _attributes = new List<AttributeValueType>();

        /// <summary>
        ///     The _dimensions
        /// </summary>
        private readonly List<DimensionValueType> _dimensions = new List<DimensionValueType>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Adds the attribute.
        /// </summary>
        /// <param name="attributeValueType">
        /// Type of the attribute value.
        /// </param>
        public void AddAttribute(AttributeValueType attributeValueType)
        {
            this._attributes.Add(attributeValueType);
        }

        /// <summary>
        /// Adds the dimension.
        /// </summary>
        /// <param name="dimension">
        /// The dimension.
        /// </param>
        public void AddDimension(DimensionValueType dimension)
        {
            this._dimensions.Add(dimension);
        }

        /// <summary>
        /// Populates the and parameter.
        /// </summary>
        /// <param name="andParameter">
        /// The and parameter.
        /// </param>
        public void PopulateAndParameter(DataParametersAndType andParameter)
        {
            andParameter.DimensionValue.AddAll(this._dimensions);
            andParameter.AttributeValue.AddAll(this._attributes);
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
        }

        #endregion
    }
}
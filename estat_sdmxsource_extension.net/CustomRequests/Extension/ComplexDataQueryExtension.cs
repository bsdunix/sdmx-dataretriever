﻿// -----------------------------------------------------------------------
// <copyright file="ComplexDataQueryExtension.cs" company="EUROSTAT">
//   Date Created : 2014-10-31
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Extension
{
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

    /// <summary>
    /// The complex data query extension.
    /// </summary>
    public static class ComplexDataQueryExtension
    {
        /// <summary>
        /// Gets a value to determine if we should use the AND operator.
        /// </summary>
        /// <param name="selection">The selection.</param>
        /// <returns><c>true</c> if the values should be <c>AND'ed</c>; otherwise false</returns>
        public static bool ShouldUseAnd(this IComplexDataQuerySelection selection)
        {
            if (selection == null)
            {
                return false;
            }

            var values = selection.HasMultipleValues() ? selection.Values : new HashSet<IComplexComponentValue>() { selection.Value };
            return values.All(value => value.OrderedOperator != null && value.OrderedOperator == OrderedOperatorEnumType.NotEqual) || values.All(value => value.TextSearchOperator != null && value.TextSearchOperator == TextSearchEnumType.NotEqual);
        }
    }
}
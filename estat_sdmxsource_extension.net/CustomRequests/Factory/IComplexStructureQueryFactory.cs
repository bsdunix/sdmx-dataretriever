﻿// -----------------------------------------------------------------------
// <copyright file="IComplexStructureQueryFactory.cs" company="EUROSTAT">
//   Date Created : 2013-08-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Estat.Sri.CustomRequests.Builder;

    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// A ComplexStructureQueryFactory is responsible fore creating a ComplexStructureQueryBuilder
    /// that build a query that can be used for services external to the SdmxSource framework
    /// </summary>
    public interface IComplexStructureQueryFactory<T>
    {
        /// <summary>
        /// Returns a ComplexStructureQueryBuilder only if this factory understands the StructureQueryFormat.
        /// If the format is unknown, null will be returned
        /// </summary>
        /// <param name="format">format</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>ComplexStructureQueryBuilder is this factory knows how to build this query format, or null if it doesn't</returns>
        IComplexStructureQueryBuilder<T> GetComplexStructureQueryBuilder(IStructureQueryFormat<T> format);
    }
}

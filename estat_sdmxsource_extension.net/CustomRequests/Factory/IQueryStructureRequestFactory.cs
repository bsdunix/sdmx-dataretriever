﻿// -----------------------------------------------------------------------
// <copyright file="IQueryStructureRequestFactory.cs" company="EUROSTAT">
//   Date Created : 2013-03-28
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.CustomRequests.Factory
{
    using Estat.Sri.CustomRequests.Builder;

    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// A  query structure request factory is a factory which is responsible fore creating a builder that can build
    /// a <c>QueryStructureRequest</c> message in the format defined by the <see cref="IStructureQueryFormat{T}"/> 
    /// </summary>
    public interface IQueryStructureRequestFactory
    {
        /// <summary>
        /// Returns a <see cref="IQueryStructureRequestBuilder{T}"/> only if this factory understands the <see cref="IStructureQueryFormat{T}"/>.  If the format is unknown, null will be returned
        /// </summary>
        /// <typeparam name="T">generic type parameter</typeparam>
        /// <param name="format">The <see cref="IStructureQueryFormat{T}"/>.</param>
        /// <returns><see cref="IQueryStructureRequestBuilder{T}"/> if this factory knows how to build this query format, or null if it doesn't</returns>
        IQueryStructureRequestBuilder<T> GetStructureQueryBuilder<T>(IStructureQueryFormat<T> format);
    }
}
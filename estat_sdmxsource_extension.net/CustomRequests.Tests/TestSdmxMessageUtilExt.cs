﻿// -----------------------------------------------------------------------
// <copyright file="TestSdmxMessageUtilExt.cs" company="EUROSTAT">
//   Date Created : 2015-02-02
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using System.Linq;

    using Estat.Sdmxsource.Extension.Util;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Test unit for <see cref="SdmxMessageUtilExt"/>
    /// </summary>
    [TestFixture]
    public class TestSdmxMessageUtilExt
    {

        /// <summary>
        /// Test unit for <see cref="SdmxMessageUtilExt.ParseSdmxFooterMessage" />
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="code">The code.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="firstText">The first text.</param>
        [TestCase(@"tests\V21\data-with-footer.xml", "510", Severity.Error, "Response size exceeds service limit : Reached configured limit : 10 observations")]
        public void TestParseSdmxFooterMessage(string fileName, string code, Severity severity, string firstText)
        {
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(fileName))
            {
                var sdmxFooterMessage = SdmxMessageUtilExt.ParseSdmxFooterMessage(dataLocation);
                Assert.IsNotNull(sdmxFooterMessage);
                Assert.AreEqual(sdmxFooterMessage.Code, code);
                Assert.AreEqual(sdmxFooterMessage.Severity, severity); 
                Assert.IsNotEmpty(sdmxFooterMessage.FooterText);
                Assert.AreEqual(firstText, sdmxFooterMessage.FooterText.First().Value);
            }
        }
    }
}
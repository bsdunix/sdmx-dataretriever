﻿// -----------------------------------------------------------------------
// <copyright file="TestComplexDataQuery2DataQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-09-20
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace CustomRequests.Tests
{
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sri.CustomRequests.Factory;
    using Estat.Sri.CustomRequests.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Test unit for <see cref="ComplexDataQuery2DataQueryBuilder"/>
    /// </summary>
    [TestFixture]
    public class TestComplexDataQuery2DataQueryBuilder
    {

        /// <summary>
        /// The _builder
        /// </summary>
        private readonly ComplexDataQuery2DataQueryBuilder _builder = new ComplexDataQuery2DataQueryBuilder();

        /// <summary>
        /// Test unit for <see cref="ComplexDataQuery2DataQueryBuilder.Build"/> 
        /// </summary>
        [Test]
        public void TestBuild()
        {
            IStructureParsingManager manager = new StructureParsingManager();
            IDataQueryParseManager queryParseManager = new DataQueryParseManager(SdmxSchemaEnumType.VersionTwoPointOne);
            IComplexDataQueryBuilderManager dataQueryBuilderManager = new ComplexDataQueryBuilderManager(new ComplexDataQueryFactoryV21());
            
            IDataflowObject dataFlow;
            IDataStructureObject dsd;
            using (var readable = new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+SSTSCONS_PROD_M+2.0.xml"))
            {
                var structureWorkspace = manager.ParseStructures(readable);
                dataFlow = structureWorkspace.GetStructureObjects(false).Dataflows.First();
            }

            using (var readable = new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml"))
            {
                var structureWorkspace = manager.ParseStructures(readable);
                dsd = structureWorkspace.GetStructureObjects(false).DataStructures.First();
            }

            ISet<IComplexDataQuerySelection> sections = new HashSet<IComplexDataQuerySelection>();
            var freqCriteria = new ComplexDataQuerySelectionImpl("FREQ", new IComplexComponentValue[] { new ComplexComponentValueImpl("M", OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal), SdmxStructureEnumType.Dimension), new ComplexComponentValueImpl("A", OrderedOperator.GetFromEnum(OrderedOperatorEnumType.NotEqual), SdmxStructureEnumType.Dimension), new ComplexComponentValueImpl("B", OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal), SdmxStructureEnumType.Dimension) });
            sections.Add(freqCriteria);
            var adjustmentCriteria = new ComplexDataQuerySelectionImpl("ADJUSTMENT", new IComplexComponentValue[] { new ComplexComponentValueImpl("01", OrderedOperator.GetFromEnum(OrderedOperatorEnumType.NotEqual), SdmxStructureEnumType.Dimension) });
            sections.Add(adjustmentCriteria);
            var titleCriteria = new ComplexDataQuerySelectionImpl(
                "TITLE", 
                new IComplexComponentValue[] { new ComplexComponentValueImpl("PAOKARA", TextSearch.GetFromEnum(TextSearchEnumType.Contains), SdmxStructureEnumType.DataAttribute),  new ComplexComponentValueImpl("ARIS", TextSearch.GetFromEnum(TextSearchEnumType.DoesNotContain), SdmxStructureEnumType.DataAttribute), });
            sections.Add(titleCriteria);
            OrderedOperator equalOperator = OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal);
            var dateFrom = new SdmxDateCore("2000-01");
            var dateFromOperator = OrderedOperator.GetFromEnum(OrderedOperatorEnumType.GreaterThanOrEqual);
            var primaryMeasureValue = new ComplexComponentValueImpl("200.20", OrderedOperator.GetFromEnum(OrderedOperatorEnumType.GreaterThan), SdmxStructureEnumType.PrimaryMeasure);
            ICollection<IComplexDataQuerySelectionGroup> collection = new[] { new ComplexDataQuerySelectionGroupImpl(sections, dateFrom, dateFromOperator, null, equalOperator, new HashSet<IComplexComponentValue> { primaryMeasureValue }) };

            var complexDataQueryImpl = new ComplexDataQueryImpl(
                null, 
                null, 
                null, 
                dsd, 
                dataFlow, 
                null, 
                null, 
                0, 
                null, 
                false, 
                null, 
                DimensionAtObservation.GetFromEnum(DimensionAtObservationEnumType.Time).Value, 
                false, 
                DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full), 
                collection);

            var dataQuery = this._builder.Build(complexDataQueryImpl);
            Assert.NotNull(dataQuery);
        }
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="AbstractAdvancedSdmxDataRetrievalWithWriter.cs" company="EUROSTAT">
//   Date Created : 2014-07-08
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Manager.Data
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

    /// <summary>
    /// The decorator class for <see cref="IAdvancedSdmxDataRetrievalWithWriter" />
    /// </summary>
    public abstract class AbstractAdvancedSdmxDataRetrievalWithWriter : IAdvancedSdmxDataRetrievalWithWriter
    {
        /// <summary>
        /// The _advanced SDMX data retrieval with writer
        /// </summary>
        private readonly IAdvancedSdmxDataRetrievalWithWriter _advancedSdmxDataRetrievalWithWriter;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractAdvancedSdmxDataRetrievalWithWriter"/> class.
        /// </summary>
        /// <param name="advancedSdmxDataRetrievalWithWriter">The advanced SDMX data retrieval with writer.</param>
        protected AbstractAdvancedSdmxDataRetrievalWithWriter(IAdvancedSdmxDataRetrievalWithWriter advancedSdmxDataRetrievalWithWriter)
        {
            if (advancedSdmxDataRetrievalWithWriter == null)
            {
                throw new ArgumentNullException("advancedSdmxDataRetrievalWithWriter");
            }

            this._advancedSdmxDataRetrievalWithWriter = advancedSdmxDataRetrievalWithWriter;
        }

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        public virtual void GetData(IComplexDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            this._advancedSdmxDataRetrievalWithWriter.GetData(dataQuery, dataWriter);
        }
    }
}
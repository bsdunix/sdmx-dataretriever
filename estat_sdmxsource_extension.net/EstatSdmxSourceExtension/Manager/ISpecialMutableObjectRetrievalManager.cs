﻿// -----------------------------------------------------------------------
// <copyright file="ISpecialMutableObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-06-12
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Manager
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// The SpecialMutableObjectRetrievalManager interface is used to serve special requests.
    /// </summary>
    public interface ISpecialMutableObjectRetrievalManager
    {
        /// <summary>
        /// Get a list of CodeList matching the given id, version and agencyID and dataflow/component information.
        /// </summary>
        /// <param name="codelistReference">The codelist reference</param>
        /// <param name="dataflowReference">The dataflow reference</param>
        /// <param name="componentConceptRef">
        ///     The component in the dataflow that this codelist is used and for which partial codes are requests.
        /// </param>
        /// <param name="isTranscoded"> 
        ///     if true will get the codes from the transcoding rules. Otherwise from locally stored codes.
        /// </param>
        /// <param name="allowedDataflows">The list of allowed dataflows.</param>
        /// <returns>
        /// The list of matched CodeListBean objects 
        /// </returns>
        /// <remarks>I.e. codes used for the given dataflow.
        /// It can look codes actually used from transcoding rule or from locally stored codes.
        /// If any of the Codelist identification parameters is null or empty it will act as wildcard.
        /// </remarks>
        ISet<ICodelistMutableObject> GetMutableCodelistObjects(IMaintainableRefObject codelistReference, IMaintainableRefObject dataflowReference, string componentConceptRef, bool isTranscoded, IList<IMaintainableRefObject> allowedDataflows);

        /// <summary>
        /// Get a list of CodeList matching the given id, version and agencyID. If any of the parameter is null or empty it will act as wildcard
        /// </summary>
        /// <param name="codelistReference">The codelist reference</param>
        /// <param name="subset">
        ///     The list of items to retrieve 
        /// </param>
        /// <returns>
        /// The list of matched CodeListBean objects 
        /// </returns>
        ISet<ICodelistMutableObject> GetMutableCodelistObjects(IMaintainableRefObject codelistReference, IList<string> subset);

        /// <summary>
        /// Gets CodelistObjects that match the parameters in the ref @object.  If the ref @object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="codelistReference">The codelist reference.</param>
        /// <returns>
        /// list of sdmxObjects that match the search criteria
        /// </returns>
        ISet<ICodelistMutableObject> GetMutableCodelistObjects(IMaintainableRefObject codelistReference);
    }
}
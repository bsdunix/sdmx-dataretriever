﻿// -----------------------------------------------------------------------
// <copyright file="IAuthAdvancedMutableStructureSearchManager.cs" company="EUROSTAT">
//   Date Created : 2013-06-13
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Manager
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    ///  Implementations of this interface are able to process a complex structure query and return the <see cref="IMutableObjects"/> containing <see cref="IMaintainableMutableObject"/> which correspond to the query parameters
    /// </summary>
    public interface IAuthAdvancedMutableStructureSearchManager
    {
        /// <summary>
        /// Process the specified <paramref name="structureQuery"/> returning an <see cref="IMutableObjects"/> container which contains the Maintainable Structure hat correspond to the <paramref name="structureQuery"/> query parameters.
        /// </summary>
        /// <param name="structureQuery">
        /// The structure query.
        /// </param>
        /// <param name="allowedDataflows">
        /// The allowed Dataflows.
        /// </param>
        /// <returns>
        /// The <see cref="IMutableObjects"/>.
        /// </returns>
        IMutableObjects GetMaintainables(IComplexStructureQuery structureQuery, IList<IMaintainableRefObject> allowedDataflows);
    }
}
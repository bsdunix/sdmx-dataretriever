﻿// -----------------------------------------------------------------------
// <copyright file="ComplexDataQuery2DataQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2014-01-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Builder
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;

    /// <summary>
    /// The complex data query 2 data query builder.
    /// </summary>
    public class ComplexDataQuery2DataQueryBuilder : IBuilder<IDataQuery, IComplexDataQuery>
    {
        /// <summary>
        /// Builds an <see cref="IDataQuery"/> from a <see cref="IComplexDataQuery"/>.
        /// Only the subset supported from <see cref="IDataQuery"/> is converted. The rest is ignored.
        /// The supported subset is operator "equal" and for time less or greater or equal
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from</param>
        /// <returns>
        /// A <see cref="IDataQuery"/>
        /// </returns>
        /// <exception cref="T:Org.Sdmxsource.Sdmx.Api.Exception.SdmxException">- If anything goes wrong during the build process</exception>
        public IDataQuery Build(IComplexDataQuery buildFrom)
        {
            ISet<IDataQuerySelectionGroup> selectionGroups = new HashSet<IDataQuerySelectionGroup>();
            foreach (var complexDataQuerySelectionGroup in buildFrom.SelectionGroups)
            {
                ISdmxDate dateFrom = null;
                ISdmxDate dateTo = null;
                if (complexDataQuerySelectionGroup.DateFromOperator == null || complexDataQuerySelectionGroup.DateFromOperator == OrderedOperatorEnumType.GreaterThanOrEqual)
                {
                    dateFrom = complexDataQuerySelectionGroup.DateFrom;
                }

                if (complexDataQuerySelectionGroup.DateToOperator == null || complexDataQuerySelectionGroup.DateToOperator == OrderedOperatorEnumType.LessThanOrEqual)
                {
                    dateTo = complexDataQuerySelectionGroup.DateTo;
                }

                ISet<IDataQuerySelection> selections = new HashSet<IDataQuerySelection>();
                foreach (var complexDataQuerySelection in complexDataQuerySelectionGroup.Selections)
                {
                    var values = new HashSet<string>();
                    foreach (var complexComponentValue in complexDataQuerySelection.Values)
                    {
                        if ((complexComponentValue.OrderedOperator == null || complexComponentValue.OrderedOperator == OrderedOperatorEnumType.Equal)
                            && (complexComponentValue.TextSearchOperator == null || complexComponentValue.TextSearchOperator == TextSearchEnumType.Equal))
                        {
                            values.Add(complexComponentValue.Value);
                        }
                    }

                    if (values.Count > 0)
                    {
                        selections.Add(new DataQueryDimensionSelectionImpl(complexDataQuerySelection.ComponentId, values));
                    }
                }

                selectionGroups.Add(new DataQuerySelectionGroupImpl(selections, dateFrom, dateTo));
            }

            return new DataQueryImpl(buildFrom.DataStructure, null, buildFrom.DataQueryDetail, buildFrom.FirstNObservations, buildFrom.LastNObservations, buildFrom.DataProvider, buildFrom.Dataflow, buildFrom.DimensionAtObservation, selectionGroups);
        }
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="SdmxMessageUtilExt.cs" company="EUROSTAT">
//   Date Created : 2015-02-02
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sdmxsource.Extension.Util
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.Sdmx.Util.Xml;

    /// <summary>
    ///     Additional methods to the <see cref="SdmxMessageUtil" />
    /// </summary>
    public static class SdmxMessageUtilExt
    {
        #region Public Methods and Operators

        /// <summary>
        /// Parses the SDMX footer message.
        /// </summary>
        /// <param name="dataLocation">
        /// The data location.
        /// </param>
        /// <returns>
        /// The <see cref="IFooterMessage"/> if there is one; otherwise null.
        /// </returns>
        public static IFooterMessage ParseSdmxFooterMessage(IReadableDataLocation dataLocation)
        {
            using (var stream = dataLocation.InputStream)
            using (XmlReader reader = XmlReader.Create(stream))
            {
                if (!StaxUtil.SkipToNode(reader, ElementNameTable.Footer.ToString()))
                {
                    return null;
                }

                var severity = Severity.Error;
                string text = null;
                string lang = null;
                string code = null;
                IList<ITextTypeWrapper> textType = new List<ITextTypeWrapper>();
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            {
                                string nodeName = reader.LocalName;
                                if (nodeName.Equals("Message"))
                                {
                                    code = reader.GetAttribute("code");

                                    var attribute = reader.GetAttribute("severity");
                                    if (attribute != null)
                                    {
                                        Enum.TryParse(attribute, out severity);
                                    }
                                }

                                if (nodeName.Equals(ElementNameTable.AnnotationText.ToString()))
                                {
                                    lang = reader.XmlLang;
                                }

                                break;
                            }

                        case XmlNodeType.Text:
                            text = reader.Value;
                            break;

                        case XmlNodeType.EndElement:
                            {
                                string nodeName = reader.LocalName;
                                if (nodeName.Equals(ElementNameTable.AnnotationText.ToString()))
                                {
                                    textType.Add(new TextTypeWrapperImpl(lang, text, null));
                                }

                                break;
                            }
                    }
                }

                return new FooterMessageCore(code, severity, textType);
            }
        }

        #endregion
    }
}
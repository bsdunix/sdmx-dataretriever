// -----------------------------------------------------------------------
// <copyright file="PrefixConstants.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxMlConstants.
// 
//     SdmxMlConstants is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxMlConstants is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxMlConstants.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxXmlConstants
{
    /// <summary>
    /// The prefix constants.
    /// </summary>
    public static class PrefixConstants
    {
        #region Constants

        /// <summary>
        /// The common prefix.
        /// </summary>
        public const string Common = "common";

        /// <summary>
        /// The generic prefix.
        /// </summary>
        public const string Generic = "generic";

        /// <summary>
        /// The message prefix.
        /// </summary>
        public const string Message = "message";

        /// <summary>
        /// The query prefix.
        /// </summary>
        public const string Query = "query";

        /// <summary>
        /// The registry prefix.
        /// </summary>
        public const string Registry = "registry";

        /// <summary>
        /// The structure prefix.
        /// </summary>
        public const string Structure = "structure";

        /// <summary>
        /// The dataset data structure specific prefix. Note this depends on the <c>DSD</c> and applies to all SDMX versions.
        /// </summary>
        public const string DataSetStructureSpecific = "ns";

        /// <summary>
        /// The structure specific prefix. Note this about <c>http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific</c>
        /// </summary>
        public const string StructureSpecific21 = "ss";

        /// <summary>
        /// The footer prefix (2.1 only)
        /// </summary>
        public const string Footer = "footer";

        #endregion
    }
}
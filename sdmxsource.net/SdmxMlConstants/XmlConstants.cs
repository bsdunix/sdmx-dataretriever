// -----------------------------------------------------------------------
// <copyright file="XmlConstants.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxMlConstants.
// 
//     SdmxMlConstants is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxMlConstants is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxMlConstants.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxXmlConstants
{
    /// <summary>
    ///     This class contains XML Standard constants
    /// </summary>
    public static class XmlConstants
    {
        #region Constants

        /// <summary>
        ///     The lang attribute
        /// </summary>
        /// <remarks>Designed for identifying the human language used in the scope of the element to which it's attached.</remarks>
        public const string LangAttribute = "lang";

        /// <summary>
        ///     XML Not A Number constant
        /// </summary>
        public const string NaN = "NaN";

        /// <summary>
        ///     The XML Schema schemaLocation
        /// </summary>
        public const string SchemaLocation = "schemaLocation";

        /// <summary>
        ///     The xml prefix
        /// </summary>
        /// <remarks>Namespace http://www.w3.org/XML/1998/namespace is bound by definition to the prefix xml:</remarks>
        public const string XmlPrefix = "xml";

        /// <summary>
        ///     The W3.org Schema XSD 1.0 namespace: http://www.w3.org/2001/XMLSchema-instance
        /// </summary>
        public const string XmlNamespace = "http://www.w3.org/XML/1998/namespace";

        /// <summary>
        ///     The W3.org Schema XSD 1.0 namespace: http://www.w3.org/2001/XMLSchema-instance
        /// </summary>
        public const string XmlSchemaNS = "http://www.w3.org/2001/XMLSchema-instance";

        /// <summary>
        ///     The W3.org Schema XSD 1.0 prefix: <c>xsi</c>
        /// </summary>
        public const string XmlSchemaPrefix = "xsi";

        /// <summary>
        ///     The attribute for setting XML namespaces
        /// </summary>
        public const string Xmlns = "xmlns";

        #endregion
    }
}
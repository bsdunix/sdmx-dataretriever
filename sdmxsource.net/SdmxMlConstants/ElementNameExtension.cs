﻿// -----------------------------------------------------------------------
// <copyright file="ElementNameExtension.cs" company="EUROSTAT">
//   Date Created : 2014-05-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxMlConstants.
// 
//     SdmxMlConstants is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxMlConstants is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxMlConstants.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxXmlConstants
{
    using System.Xml;

    /// <summary>
    /// The element name extension.
    /// </summary>
    public static class ElementNameExtension
    {
        /// <summary>
        /// Determines whether the <paramref name="localName"/> is the specified <paramref name="element"/>
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="localName">The element local name as an object.</param>
        /// <returns><c>true</c> if the <paramref name="localName"/> is the specified <paramref name="element"/>; otherwise false.</returns>
        public static bool Is(this ElementNameTable element, object localName)
        {
            return NameTableCache.IsElement(localName, element);
        }

        /// <summary>
        /// Get a string representation of the <paramref name="element"/> 
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// The string representation of the <paramref name="element"/> 
        /// </returns>
        public static string FastToString(this ElementNameTable element)
        {
            return NameTableCache.GetElementName(element);
        }

        /// <summary>
        /// Get a string representation of the <paramref name="attribute"/> 
        /// </summary>
        /// <param name="attribute">
        /// The attribute.
        /// </param>
        /// <returns>
        /// The string representation of the <paramref name="attribute"/> 
        /// </returns>
        public static string FastToString(this AttributeNameTable attribute)
        {
            return NameTableCache.GetAttributeName(attribute);
        }

        /// <summary>
        /// Gets the attribute.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="attribute">
        /// The attribute.
        /// </param>
        /// <returns>
        /// The attribute value if it exists; otherwise null.
        /// </returns>
        public static string GetAttribute(this XmlReader reader, AttributeNameTable attribute)
        {
            return reader.GetAttribute(attribute.FastToString());
        }
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="XmlReaderBuilder.cs" company="EUROSTAT">
//   Date Created : 2014-07-16
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxMlConstants.
// 
//     SdmxMlConstants is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxMlConstants is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxMlConstants.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxXmlConstants.Builder
{
    using System.IO;
    using System.Xml;

    /// <summary>
    /// Build an XML reader using the <see cref="NameTableCache" />
    /// </summary>
    public class XmlReaderBuilder : IXmlReaderBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds the <see cref="XmlReader"/> from the specified stream.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="XmlReader"/>
        /// </returns>
        public XmlReader Build(Stream stream)
        {
            return XmlReader.Create(
                stream, 
                new XmlReaderSettings { IgnoreComments = true, IgnoreProcessingInstructions = true, IgnoreWhitespace = true, NameTable = NameTableCache.Instance.NameTable });
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="AttachmentLevelTypeConstants.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V10.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V10 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V10 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V10.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V10.structure
{
    /// <summary>
    /// The attachment level type constants.
    /// </summary>
    public static class AttachmentLevelTypeConstants
    {
        #region Constants

        /// <summary>
        /// The data set.
        /// </summary>
        public const string DataSet = "DataSet";

        /// <summary>
        /// The group.
        /// </summary>
        public const string Group = "Group";

        /// <summary>
        /// The observation.
        /// </summary>
        public const string Observation = "Observation";

        /// <summary>
        /// The series.
        /// </summary>
        public const string Series = "Series";

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="TextTypeConstants.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V10.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V10 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V10 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V10.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V10.structure
{
    /// <summary>
    /// <para>
    /// TextTypeType provides an enumerated list of the types of characters allowed in a TextFormat field.
    /// </para>
    /// This class contains TextTypeType constants to be used by other software since the auto-generated code doesn't.
    /// </summary>
    public static class TextTypeConstants
    {
        #region Constants

        /// <summary>
        /// The alpha.
        /// </summary>
        public const string Alpha = "Alpha";

        /// <summary>
        /// The alpha fixed.
        /// </summary>
        public const string AlphaFixed = "AlphaFixed";

        /// <summary>
        /// The alpha numeric.
        /// </summary>
        public const string AlphaNum = "AlphaNum";

        /// <summary>
        /// The alpha numeric fixed.
        /// </summary>
        public const string AlphaNumFixed = "AlphaNumFixed";

        /// <summary>
        /// The numeric.
        /// </summary>
        public const string Num = "Num";

        /// <summary>
        /// The numeric fixed.
        /// </summary>
        public const string NumFixed = "NumFixed";

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ISchemaWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Factory
{

    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    #endregion

    /// <summary>
    /// Returns a SchemaWriterEngine if it understands the SchemaFormat
    /// </summary>
    public interface ISchemaWriterFactory
    {
        #region Public Methods and Operators

        /// <summary>
        /// Returns the SchemaWriterEngine that can write schema in the given format, returns null if the format is unknown to this factory
        /// </summary>
        /// <param name="format">
        /// The format to write the schema in
        /// </param>
        /// <returns>
        /// The schema writer engine
        /// </returns>
        ISchemaWriterEngine GetSchemaWriterEngine(ISchemaFormat format);

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="IStructureWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2013-02-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Factory
{
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// A StructureWriterFactory operates as a plugin to a Manager which can request a StructureWritingEngine, to which the implementation will
    /// respond with an appropriate StructureWritingEngine if it is able, otherwise it will return null
    /// </summary>
    public interface IStructureWriterFactory
    {
        /// <summary>
        /// Obtains a StructureWritingEngine engine for the given output format
        /// </summary>
        /// <param name="structureFormat">An implementation of the StructureFormat to describe the output format for the structures (required)</param>
        /// <param name="streamWriter">The output stream to write to (can be null if it is not required)</param>
        /// <returns>Null if this factory is not capable of creating a data writer engine in the requested format</returns>
        IStructureWriterEngine GetStructureWriterEngine(IStructureFormat structureFormat, Stream streamWriter);
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="IStructureParserFactory.cs" company="EUROSTAT">
//   Date Created : 2014-03-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Factory
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    /// Parses a stream of information to build <see cref="ISdmxObjects"/>
    /// </summary>
    public interface IStructureParserFactory
    {
        /// <summary>
        /// Parses the <see cref="IReadableDataLocation"/> to build the <see cref="ISdmxObjects"/>, returns null if the structure format can not be determined from 
        /// the information provided in the <see cref="IReadableDataLocation"/> (or from the underlying data)
        /// </summary>
        /// <param name="readableDataLocation">The readable data location.</param>
        /// <returns>returns null if the structure format can not be determined from 
        /// the information provided in the <see cref="IReadableDataLocation"/> (or from the underlying data)</returns>
        ISdmxObjects GetSdmxObjects(IReadableDataLocation readableDataLocation);
    }
}
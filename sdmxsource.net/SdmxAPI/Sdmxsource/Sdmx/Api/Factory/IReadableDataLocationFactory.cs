﻿// -----------------------------------------------------------------------
// <copyright file="IReadableDataLocationFactory.cs" company="EUROSTAT">
//   Date Created : 2013-02-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Factory
{
    using System;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    /// Used to create ReadableDataLocations from various sources of information
    /// </summary>
    public interface IReadableDataLocationFactory
    {
        /// <summary>
        /// Create a readable data location from a String, this may represent a URI (file or URL) 
        /// </summary>
        /// <param name="uriStr"></param>
        /// <returns></returns>
        IReadableDataLocation GetReadableDataLocation(String uriStr);
        /// <summary>
        /// Create a readable data location from a String, this may represent a URI (file or URL) 
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        IReadableDataLocation GetReadableDataLocation(byte[] bytes);
        /// <summary>
        /// Create a readable data location from a String, this may represent a URI (file or URL)
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        IReadableDataLocation GetReadableDataLocation(FileInfo file);

        /// <summary>
        /// Create a readable data location from a String, this may represent a URI (file or URL) 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        IReadableDataLocation GetReadableDataLocation(Uri url);
        /// <summary>
        /// Create a readable data location from a String, this may represent a URI (file or URL) 
        /// </summary>
        /// <param name="streamReader"></param>
        /// <returns></returns>
        IReadableDataLocation GetReadableDataLocation(Stream streamReader);
    }
}

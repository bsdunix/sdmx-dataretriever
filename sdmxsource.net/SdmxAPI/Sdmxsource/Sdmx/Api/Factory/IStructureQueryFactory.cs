﻿// -----------------------------------------------------------------------
// <copyright file="IStructureQueryFactory.cs" company="EUROSTAT">
//   Date Created : 2013-02-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Factory
{
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// A structure query factory is a factory which is responsible fore creating a builder that can build
    /// a StructureQuery message in the format defined by the StructureQueryFormat 
    /// </summary>
    public interface IStructureQueryFactory
    {
     /// <summary>
        /// Returns a StructureQueryBuilder only if this factory understands the StructureQueryFormat.  If the format is unknown, null will be returned
     /// </summary>
     /// <typeparam name="T">generic type parameter</typeparam>
     /// <param name="format">Format</param>
        /// <returns>StructureQueryBuilder is this factory knows how to build this query format, or null if it doesn't</returns>
        IStructureQueryBuilder<T> GetStructureQueryBuilder<T>(IStructureQueryFormat<T> format);
    }
}

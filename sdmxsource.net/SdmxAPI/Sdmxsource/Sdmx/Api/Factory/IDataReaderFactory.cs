﻿// -----------------------------------------------------------------------
// <copyright file="IDataReaderFactory.cs" company="EUROSTAT">
//   Date Created : 2014-05-16
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Factory
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    /// Creates <see cref="IDataReaderEngine"/> from Source data, DataStructure (or <see cref="ISdmxObjectRetrievalManager"/> used to get DataStructure(s))
    /// </summary>
    public interface IDataReaderFactory
    {
        /// <summary>
        /// Obtains a DataReaderEngine that is capable of reading the data which is exposed via the ReadableDataLocation
        /// </summary>
        /// <param name="sourceData">The source data, giving access to an InputStream of the data.</param>
        /// <param name="dsd">The Data Structure Definition, describes the data in terms of the dimensionality.</param>
        /// <param name="dataflow">The dataflow (optional). Provides further information about the data.</param>
        /// <returns>The <see cref="IDataReaderEngine"/>; otherwise null if the <paramref name="sourceData"/> cannot be read.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="sourceData"/> is null -or- <paramref name="dsd"/> is null</exception>
        IDataReaderEngine GetDataReaderEngine(IReadableDataLocation sourceData, IDataStructureObject dsd, IDataflowObject dataflow);

        /// <summary>
        /// Obtains a DataReaderEngine that is capable of reading the data which is exposed via the ReadableDataLocation
        /// </summary>
        /// <param name="sourceData">
        /// The source data, giving access to an InputStream of the data.
        /// </param>
        /// <param name="retrievalManager">
        /// The retrieval Manager. It is used to obtain the <see cref="IDataStructureObject"/> that describe the data.
        /// </param>
        /// <returns>
        /// The <see cref="IDataReaderEngine"/>; otherwise null if the <paramref name="sourceData"/> cannot be read.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="sourceData"/> is null -or- <paramref name="retrievalManager"/> is null
        /// </exception>
        IDataReaderEngine GetDataReaderEngine(IReadableDataLocation sourceData, ISdmxObjectRetrievalManager retrievalManager);
    }
}
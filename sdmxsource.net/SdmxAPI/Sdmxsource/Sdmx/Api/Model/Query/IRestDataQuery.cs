﻿// -----------------------------------------------------------------------
// <copyright file="IRestDataQuery.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Query
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// This interface holds a SDMX REST Data Query.  
    /// </summary>
    public interface IRestDataQuery
    {
        /// <summary>
        /// Gets a String representation of this query, in SDMX REST format starting from Data/.
        /// <example>
        /// Example Data/ACY,FLOW,1.0/M.Q+P....L/ALL?detail=seriesKeysOnly
        /// </example>
        /// </summary>
        string RestQuery { get; }

        /// <summary>
        /// Gets the dataflow reference
        /// </summary>
        IStructureReference FlowRef { get; }

        /// <summary>
        /// Gets the data provider reference, or null if ALL
        /// </summary>
        IStructureReference ProviderRef { get; }

        /// <summary>
        /// Gets the start date to  the data from, or null if undefined
        /// </summary>
        ISdmxDate StartPeriod { get; }

        /// <summary>
        /// Gets the end date to  the data from, or null if undefined
        /// </summary>
        ISdmxDate EndPeriod { get; }

        /// <summary>
        /// Gets the updated after date to  the data from, or null if undefined
        /// </summary>
        ISdmxDate UpdatedAfter { get; }

        /// <summary>
        /// Gets the first 'n' observations, for each series key, to return as a result of a data query.  
        /// </summary>
        int? LastNObsertations { get; }

        /// <summary>
        /// Gets the last 'n' observations, for each series key,  to return as a result of a data query
        /// </summary>
        int? FirstNObservations { get; }

        /// <summary>
        /// Gets the level of detail for the returned data, or null if undefined
        /// </summary>
        DataQueryDetail QueryDetail { get; }

        /// <summary>
        /// Gets the dimension to , or null if undefined
        /// </summary>
        string DimensionAtObservation { get; }

        /// <summary>
        /// Gets the list of dimension code id filters, in the same order as the dimensions are defined by the DataStructure
        /// </summary>
        IList<ISet<string>> QueryList { get; }
    }
}

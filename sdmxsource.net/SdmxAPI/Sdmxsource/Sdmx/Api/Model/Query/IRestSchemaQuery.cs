﻿// -----------------------------------------------------------------------
// <copyright file="IRestSchemaQuery.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Query
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    /// Defines a schema query that contains information defined by the RESTful API
    /// </summary>
    public interface IRestSchemaQuery : IDisposable
    {
        #region Public Properties

        /// <summary>
        /// Returns the reference to the structure the query is for, this can be:
        /// <ul>
        ///  <li>Data Structure</li>
        ///  <li>Dataflow</li>
        ///  <li>Provision Agreement</li>
        /// </summary>
        IStructureReference Reference
        {
            get;
        }

        /// <summary>
        /// Returns the dimension at observation
        /// </summary>
        string DimAtObs
        {
            get;
        }

        #endregion


        #region Public Methods and Operators

        /// <summary>
        /// For cross-sectional data validation, indicates whether observations are strongly typed
        /// </summary>
        /// <returns>
        /// The boolean
        /// </returns>
        bool IsExplicitMeasure();

        #endregion
    }
}

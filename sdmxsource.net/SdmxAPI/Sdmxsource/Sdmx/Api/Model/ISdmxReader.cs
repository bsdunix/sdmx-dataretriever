// -----------------------------------------------------------------------
// <copyright file="ISdmxReader.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    #endregion

    /// <summary>
    ///     The SdmxReader is a technology independent definition of a reader for SDMX-ML files
    /// </summary>
    public interface ISdmxReader
    {
        /**
         * Reads the next element enforcing it is one of the following elements
         * @param expectedElement this can not be null
         * @return will return a value that matches one of the expected values, response can never be null
         * @throws SdmxSemmanticException if the next element does not match the expected element
         */

        // string readNextElement(string...expectedElement) throws SdmxSemmanticException;

        /**
         * Takes a peek at the next element without moving the parser forward
         * @return
         */

        // string peek();
        #region Public Properties

        /// <summary>
        ///     Gets a map of all the attributes present on the current node
        /// </summary>
        /// <value> </value>
        IDictionary<string, string> Attributes { get; }

        /// <summary>
        ///     Gets the name of the currentElement
        /// </summary>
        /// <value> </value>
        string CurrentElement { get; }

        /// <summary>
        ///     Gets the value in the current element - returns null if there is no value
        /// </summary>
        /// <value> </value>
        string CurrentElementValue { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Closes any underlying resource
        /// </summary>
        void Close();

        /// <summary>
        /// Gets an attribute value with the given name, if mandatory is true then will enforce the response is not null
        /// </summary>
        /// <param name="attributeName">The attribute name. </param>
        /// <param name="mandatory">Mandatory parameter. </param>
        /// <returns>
        /// The <see cref="string"/> .
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        /// if mandatory is true and the attribute is not present
        /// </exception>
        string GetAttributeValue(string attributeName, bool mandatory);

        /// <summary>
        ///     Reads the next element returning the value.  The response is null if there is no more elements to read
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool MoveNextElement();

        /// <summary>
        /// Moves to the position in the file with the element name, returns true if the move was successful, false if no element was found
        /// </summary>
        /// <param name="element">Element parameter. </param>
        /// <returns>
        /// The <see cref="bool"/> .
        /// </returns>
        bool MoveToElement(string element);

        /// <summary>
        /// The move to element.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="doNoMovePast">
        /// The do no move past.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> .
        /// </returns>
        bool MoveToElement(string element, string doNoMovePast);

        #endregion
    }
}
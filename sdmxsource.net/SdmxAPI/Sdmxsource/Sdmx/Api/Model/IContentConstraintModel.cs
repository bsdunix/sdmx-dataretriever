﻿// -----------------------------------------------------------------------
// <copyright file="IContentConstraintModel.cs" company="EUROSTAT">
//   Date Created : 2013-03-11
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IContentConstraintModel
    {
        /// <summary>
        /// Returns true if the observation is valid with respect to the constraint
        /// </summary>
        /// <param name="obs"></param>
        /// <returns></returns>
        bool IsValidObservation(IObservation obs);

        /// <summary>
        /// Returns true if the key is valid with respect to the constraint
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool IsValidKey(IKeyable key);

        /// <summary>
        /// Returns true if the key value pair are valid with respect to the constraint
        /// </summary>
        /// <param name="kv"></param>
        /// <returns></returns>
        bool IsValidKeyValue(IKeyValue kv);
    }
}

// -----------------------------------------------------------------------
// <copyright file="IHeader.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Header
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     A Header Contains any information to go at the beginning of an exchange message.
    ///     Methods are provided to write and read values.
    /// </summary>
    public interface IHeader
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the action.
        /// </summary>
        DatasetAction Action { get; set; }

        /// <summary>
        ///     Gets a map of any additional attributes that are stored in the header - the key is the field, such as Header.DSD_REF, the value is the value of that field
        /// </summary>
        /// <value> </value>
        IDictionary<string, string> AdditionalAttribtues { get; }

        /// <summary>
        ///     Gets or sets the data provider reference.
        /// </summary>
        IStructureReference DataProviderReference { get; set; }

        /// <summary>
        ///     Gets or sets the dataset id.
        /// </summary>
        string DatasetId { get; set; }

        /// <summary>
        ///     Gets the embargo date.
        /// </summary>
        DateTime? EmbargoDate { get; }

        /// <summary>
        ///     Gets the extracted.
        /// </summary>
        DateTime? Extracted { get; }

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        IList<ITextTypeWrapper> Name { get; }

        /// <summary>
        ///     Gets the prepared.
        /// </summary>
        DateTime? Prepared { get; }

        /// <summary>
        ///     Gets the receiver.
        /// </summary>
        IList<IParty> Receiver { get; }

        /// <summary>
        ///     Gets or sets the reporting begin.
        /// </summary>
        DateTime? ReportingBegin { get; set; }

        /// <summary>
        ///     Gets or sets the reporting end.
        /// </summary>
        DateTime? ReportingEnd { get; set; }

        /// <summary>
        ///     Gets or sets the sender.
        /// </summary>
        IParty Sender { get; set; }

        /// <summary>
        ///     Gets the source.
        /// </summary>
        IList<ITextTypeWrapper> Source { get; }

        /// <summary>
        ///     Gets the structures.
        /// </summary>
        IList<IDatasetStructureReference> Structures { get; }

        /// <summary>
        ///     Gets or sets a value indicating whether test.
        /// </summary>
        bool Test { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the header value for a given field - for example getHeaderValue(Header.DSD_REF) would return the value given for the keyFamilyRef (SDMX 2.0)
        /// </summary>
        /// <param name="headerField">The field.
        /// </param>
        /// <returns>
        /// The <see cref="string"/> .
        /// </returns>
        string GetAdditionalAttribtue(string headerField);

        /// <summary>
        /// Gets the structure for the given id
        /// </summary>
        /// <param name="structureId">Structure Id
        /// </param>
        /// <returns>
        /// The <see cref="IDatasetStructureReference"/> .
        /// </returns>
        IDatasetStructureReference GetStructureById(string structureId);

        /// <summary>
        /// Gets a value indicating whether the Header instance has a value stored for this property in its additional attributes @See getAdditionalAttribtues()
        /// </summary>
        /// <param name="headerField">The field.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> .
        /// </returns>
        bool HasAdditionalAttribtue(string headerField);

        /// <summary>
        /// Adds a receiver to the list of recivers
        /// </summary>
        /// <param name="recevier"></param>
        void AddReciever(IParty recevier);

        /// <summary>
        /// Adds a source to the list of sources
        /// </summary>
        /// <param name="recevier"></param>
        void AddSource(ITextTypeWrapper source);

        /// <summary>
        /// Adds a dataset structure reference to the list
        /// </summary>
        /// <param name="datasetStructureReference"></param>
        void AddStructure(IDatasetStructureReference datasetStructureReference);

        /// <summary>
        /// Adds a name to the list of names
        /// </summary>
        /// <param name="recevier"></param>
        void AddName(ITextTypeWrapper name);

        #endregion
    }
}
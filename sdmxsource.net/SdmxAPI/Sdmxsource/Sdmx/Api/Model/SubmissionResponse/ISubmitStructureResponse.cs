// -----------------------------------------------------------------------
// <copyright file="ISubmitStructureResponse.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.SubmissionResponse
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Created after a submission of a document to a SDMX web service
    /// </summary>
    public interface ISubmitStructureResponse
    {
        #region Public Properties

        /// <summary>
        ///     Gets a value indicating whether the getErrorList() returns a not null ErrorList, and the returned ErrorList is of type error (not warning)
        /// </summary>
        /// <value> </value>
        bool IsError { get; }

        /// <summary>
        ///     Gets the list of errors, returns null if there were no errors for this response (if it was a success)
        /// </summary>
        /// <value> </value>
        IErrorList ErrorList { get; }

        /// <summary>
        ///     Gets the structure that this response is for - this may be null if there were errors in the submission
        /// </summary>
        /// <value> </value>
        IStructureReference StructureReference { get; }

        #endregion
    }
}
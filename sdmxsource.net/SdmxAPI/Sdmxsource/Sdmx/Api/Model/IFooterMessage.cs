﻿// -----------------------------------------------------------------------
// <copyright file="IFooterMessage.cs" company="EUROSTAT">
//   Date Created : 2013-02-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// Provides extra detail on close, used to inform users of things such as truncated messages, or errors while processing.
    /// </summary>
    public interface IFooterMessage
    {
        /// <summary>
        /// Mandatory Field - use to describe the error/warning
        /// </summary>
        string Code { get; }

        /// <summary>
        /// Optional - describes severity of problem
        /// </summary>
        Severity Severity { get; }

        /// <summary>
        /// Any text associated with the footer, there must be at least one text message
        /// </summary>
        IList<ITextTypeWrapper> FooterText { get; }
    }
}

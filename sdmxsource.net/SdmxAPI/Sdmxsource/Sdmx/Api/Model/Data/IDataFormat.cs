﻿// -----------------------------------------------------------------------
// <copyright file="IDataFormat.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data
{
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Data format is an Interface which wrappers a strongly typed and non-extensible <see cref="DataType"/> enumeration.  The purpose of the wrapper is to 
    /// allow for additional implementations to be provided which may describe non-sdmx formats which are not supported by the <see cref="DataType"/> enumeration.
    /// </summary>
    public interface IDataFormat
    {
        /// <summary>
        /// Gets the sdmx data format that this interface is describing.
        /// If this is not describing an SDMX message then this will return null and the implementation class will be expected to expose additional methods to understand the data
        /// </summary>
        DataType SdmxDataFormat { get; }

        /// <summary>
        /// Gets a string representation of the format, that can be used for auditing and debugging purposes.
        /// <p/>
        /// This is expected to return a not null response.
        /// </summary>
        string FormatAsString { get; }
    }
}
// -----------------------------------------------------------------------
// <copyright file="IDataQuerySelection.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data.Query
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     A DataQuerySelection represents a single dimension, with one or more code selections for it.
    /// </summary>
    public interface IDataQuerySelection
    {
        #region Public Properties

        /// <summary>
        ///   Gets the component Id that the code selection(s) are for.  
        /// The component Id is either a referenece to a dimension or an attribute
        /// </summary>
        /// <value> </value>
        string ComponentId { get; }

        /// <summary>
        ///     Gets the value of the code that has been selected for the dimension.
        ///     <p />
        ///     If more then one value exists then calling this method will result in a excpetion.
        ///     Check that hasMultipleValues() returns false before calling this method
        /// </summary>
        /// <value> </value>
        string Value { get; }

        /// <summary>
        ///     Gets all the code values that have been selected for this dimension
        /// </summary>
        /// <value> </value>
        ISet<string> Values { get; }

        #endregion

     
        /// <summary>
        ///     Gets a value indicating whether the more then one value exists for this dimension
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasMultipleValues { get; }

    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="IComplexDataQuerySelectionGroup.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    #endregion

    /// <summary>
    /// A DataQuerySelectionGroup contains a set of DataQuerySelections which are implicitly ANDED together. 
    /// Each DataQuerySelection contains a concept along with one or more selection values.
    /// </p> 
    /// For example a DataQuerySelection could be FREQ (=A), (=B) which would equate to FREQ = A OR B.  <br>
    /// or could be FREQ (>A), (<M) which would equate to FREQ > A AND FREQ <M.  
    ///  </p>
    ///  When there are more than one DataQuerySelections they are ANDED together. For example the DataQuerySelections:
    ///  <pre>
    ///  DataQuerySelection FREQ=A,B
    ///  DataQuerySelection COUNTRY=UK
    ///  </pre>
    ///  Equate to:
    ///  <pre>(FREQ = A OR B) AND (COUNTRY = UK)
    ///  </pre>
    /// </summary>
    public interface IComplexDataQuerySelectionGroup : IDisposable
    {
        #region Public Properties

        /// <summary>
        /// Returns the set of selections for this group. These DataQuerySelections are implicitly ANDED together. 
        /// </summary>
        ISet<IComplexDataQuerySelection> Selections { get; }

        /// <summary>
        /// Returns the "date from" in this selection group.
        /// </summary>
        ISdmxDate DateFrom { get; }

        /// <summary>
        /// Returns the operator for the dateFrom
        /// The operator cannot take the 'NOT_EQUAL' value
        /// </summary>
        OrderedOperator DateFromOperator { get; }

        /// <summary>
        /// Returns the "date to" in this selection group.
        /// </summary>
        ISdmxDate DateTo { get; }

        /// <summary>
        /// Returns the operator for the dateTo
        /// The operator cannot take the 'NOT_EQUAL' value
        /// </summary>
        OrderedOperator DateToOperator { get; }

        /// <summary>
        /// Returns the component value (s) for a primary measure value.
        /// </summary>
        ISet<IComplexComponentValue> PrimaryMeasureValue { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Returns the selection(s) for the given component id (dimension or attribute) or returns null if no selection exists for the component id.
        /// </summary>
        /// <param name="componentId">
        /// The component id
        /// </param>
        /// <returns>
        /// The selection(s)
        /// </returns>
        IComplexDataQuerySelection GetSelectionsForConcept(string componentId);

        /// <summary>
        /// Returns true if selections exist for this dimension Id.
        /// </summary>
        /// <param name="componentId">
        /// The dimension id
        /// </param>
        /// <returns>
        /// The boolean
        /// </returns>
        bool HasSelectionForConcept(string componentId);

        #endregion

    }
}

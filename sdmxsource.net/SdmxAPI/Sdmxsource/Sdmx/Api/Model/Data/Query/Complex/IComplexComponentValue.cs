﻿// -----------------------------------------------------------------------
// <copyright file="IComplexComponentValue.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Constants;

    #endregion

    /// <summary>
    /// It models the dimension or attribute or primary measure or time value for which data is requested.<br>
    /// The value of the component is accompanied by the operator to apply to the string value. 
    /// </summary>
    public interface IComplexComponentValue
    {
        #region Public Properties

        /// <summary>
        /// Returns the value of the dimension or attribute
        /// </summary>
        string Value
        {
            get;
        }

        /// <summary>
        /// Returns the operator to apply. Does not concern a dimension or time value
        /// </summary>
        TextSearch TextSearchOperator
        {
            get;
        }

        /// <summary>
        /// Returns the operator to apply.
        /// </summary>
        OrderedOperator OrderedOperator
        {
            get;
        }

        #endregion
    }
}

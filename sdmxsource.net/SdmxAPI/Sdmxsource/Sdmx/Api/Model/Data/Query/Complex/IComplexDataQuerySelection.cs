﻿// -----------------------------------------------------------------------
// <copyright file="IComplexDataQuerySelection.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// It represents a single component (dimension or attribute), <br>
    /// with one or more code selections for it along with an operator. 
    /// </summary>
    public interface IComplexDataQuerySelection : IDisposable
    {
        #region Public Properties

        /// <summary>
        /// Returns the component Id that the code selection(s) are for.  The component Id is either a referenece to a dimension
        /// or an attribute
        /// </summary>
        string ComponentId
        {
            get;
        }

        /// <summary>
        /// Returns the component value of the code that has been selected for the component.
        /// <p/>
        /// If more then one value exists then calling this method will result in a exception.
        /// Check that hasMultipleValues() returns false before calling this method
        /// </summary>
        IComplexComponentValue Value
        {
            get;
        }

        /// <summary>
        /// Returns all the code values that have been selected for this component
        /// </summary>
        ISet<IComplexComponentValue> Values
        {
            get;
        }

        #endregion


        #region Public Methods and Operators

        /// <summary>
        /// Returns true if more then one value exists for this component
        /// </summary>
        /// <returns>
        /// The boolean
        /// </returns>
        bool HasMultipleValues();

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="IComplexDataQuery.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    /// It is a representation of a SOAP DataQuery 2.1
    /// </summary>
    public interface IComplexDataQuery : IBaseDataQuery
    {
        #region Public Properties

        /// <summary>
        /// Returns the suggested maximum response size or null if unspecified
        /// </summary>
        int? DefaultLimit
        {
            get;
        }

        /// <summary>
        /// Returns the type of observations to be returned.<br>
        /// Defaults to ACTIVE
        /// </summary>
        ObservationAction ObservationAction
        {
            get;
        }

        /// <summary>
        /// Returns true or false depending on the existence of explicit measures
        /// Defaults to false
        /// </summary>
        /// <returns>
        /// The boolean
        /// </returns>
        bool HasExplicitMeasures();

        /// <summary>
        /// Returns the id of the dataset from which data will be returned or null if unspecified
        /// </summary>
        string DatasetId
        {
            get;
        }

        /// <summary>
        /// The id of the dataset requested should satisfy the condition implied by the operator this method returns. <br>
        /// For instance if the operator is starts_with then data from a dataset with id that starts with the getDatasetId() result
        /// should be returned.
        /// Defaults to EQUAL
        /// </summary>
        TextSearch DatasetIdOperator
        {
            get;
        }

        /// <summary>
        /// This method accomplishes the following criteria :match data based on when they were last updated <br>
        /// The date points to the start date or the range that the queried date must occur within and/ or to the end period of the range <br>
        /// It returns null if unspecified.
        /// </summary>
        IList<ITimeRange> LastUpdatedDateTimeRange
        {
            get;
        }

        /// <summary>
        /// Returns a list of selection groups. The list is empty if no selection groups have been added.
        /// </summary>
        IList<IComplexDataQuerySelectionGroup> SelectionGroups
        {
            get;
        }

        /// <summary>
        /// Returns the Provision Agreement that the query is returning data for or null if unspecified
        /// </summary>
        IProvisionAgreementObject ProvisionAgreement
        {
            get;
        }

        #endregion
    }
}

// -----------------------------------------------------------------------
// <copyright file="ICategorySchemeObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Represents an SDMX Category Scheme
    /// </summary>
    public interface ICategorySchemeObject : IItemSchemeObject<ICategoryObject>
    {
        #region Public Properties

        /// <summary>
        ///     Gets a representation of itself in a @object which can be modified, modifications to the mutable @object
        ///     are not reflected in the instance of the IMaintainableObject.
        /// </summary>
        /// <value> </value>
        new ICategorySchemeMutableObject MutableInstance { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the category with the given id, additional ids are used to represent the categories parents if it is in a hierarchy.
        ///     <p/>
        ///     Gets null if the category can not be found with the given id
        /// </summary>
        /// <param name="id">Category id array
        /// </param>
        /// <returns>
        /// The <see cref="ICategoryObject"/> .
        /// </returns>
        ICategoryObject GetCategory(params string[] id);

        /// <summary>
        /// Gets a stub reference of itself.
        ///     <p/>
        ///     A stub @object only contains Maintainable and Identifiable Attributes, not the composite Objects that are
        ///     contained within the Maintainable
        /// </summary>
        /// <param name="actualLocation">
        /// The actual Location.
        /// </param>
        /// <param name="isServiceUrl">
        /// The is Service Uri.
        /// </param>
        /// <returns>
        /// The <see cref="ICategorySchemeObject"/> .
        /// </returns>
        new ICategorySchemeObject GetStub(Uri actualLocation, bool isServiceUrl);

        #endregion
    }
}
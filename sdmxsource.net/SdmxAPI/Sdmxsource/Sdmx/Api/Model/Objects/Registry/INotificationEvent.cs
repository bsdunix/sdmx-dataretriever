// -----------------------------------------------------------------------
// <copyright file="INotificationEvent.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    #endregion

    /// <summary>
    ///     Defines a notification event sent out from a registry.  The notification contains reference to the attachmentConstraint that
    ///     caused the notification to occur.  In the case of an identifiable the attachmentConstraint urn is provided, from which the attachmentConstraint
    ///     can be queries.  In the case of a Registration, which has no URN the registration itself is sent inside the NotificationEvent.
    /// </summary>
    public interface INotificationEvent
    {
        #region Public Properties

        /// <summary>
        ///     Gets the action on the attachmentConstraint that this event was triggered from
        /// </summary>
        /// <value> </value>
        DatasetAction Action { get; }

        /// <summary>
        ///     Gets the structures contained in this subscription
        /// </summary>
        /// <value> </value>
        ISdmxObjects Objects { get; }

        /// <summary>
        ///     Gets the time that the event was created
        /// </summary>
        /// <value> </value>
        DateTime? EventTime { get; }

        /// <summary>
        ///     Gets the urn of the attachmentConstraint that caused the notification event to be created (no applicable for registrations)
        /// </summary>
        /// <value> </value>
        Uri ObjectUrn { get; }

        /// <summary>
        ///     Gets the Registration that triggered this event.  This can be null if it was not a registration that triggered the
        ///     event.
        /// </summary>
        /// <value> </value>
        IRegistrationObject Registration { get; }

        /// <summary>
        ///     Gets the urn of the subscription that this notification event was triggered from
        /// </summary>
        /// <value> </value>
        Uri SubscriptionUrn { get; }

        #endregion
    }
}
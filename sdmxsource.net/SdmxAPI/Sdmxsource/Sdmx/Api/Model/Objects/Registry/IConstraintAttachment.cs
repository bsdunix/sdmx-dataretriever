// -----------------------------------------------------------------------
// <copyright file="IConstraintAttachment.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     The ConstraintAttachment interface.
    /// </summary>
    public interface IConstraintAttachment : ISdmxStructure
    {
        #region Public Properties

        /// <summary>
        ///   Gets the data provider and dataset id  If this constraint is built from a dataset/metadataset.
        /// </summary>
        /// <value> </value>
        IDataAndMetadataSetReference DataOrMetadataSetReference { get; }

        /// <summary>
        ///     Gets the datasource(s) that this constraint is built from
        /// </summary>
        /// <value> not null </value>
        IList<IDataSource> DataSources { get; }

        /// <summary>
        ///    Gets the structures that this constraint is attached to, this can be one or more of the following:
        ///  <ul>
        /// <li>Data Provider</li>
        /// <li>Data Structure</li>
        /// <li>Metadata Structure</li>
        /// <li>Data Flow</li>
        /// <li>Metadata Flow</li>
        /// <li>Provision Agreement</li>
        /// <li>Registration Object</li>
        /// </ul>  
        /// <b>NOTE: </b> This list of cross references can not be a mixed bag, it can be one or more OF THE SAME TYPE.
        /// </summary>
        ISet<ICrossReference> StructureReference { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The create mutable instance.
        /// </summary>
        /// <returns>
        ///     The <see cref="IConstraintAttachmentMutableObject" /> .
        /// </returns>
        IConstraintAttachmentMutableObject CreateMutableInstance();

        #endregion
    }
}
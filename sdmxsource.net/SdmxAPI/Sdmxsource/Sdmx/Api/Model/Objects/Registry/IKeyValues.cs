// -----------------------------------------------------------------------
// <copyright file="IKeyValues.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Represents an SDMX IKeyValues as defined in a SDMX ContentConstraint
    /// </summary>
    public interface IKeyValues : ISdmxStructure
    {
        #region Public Properties

        /// <summary>
        ///     Gets the identifier of the component that the values are for
        /// </summary>
        /// <value> </value>
        string Id { get; }

        /// <summary>
        ///     Gets the time range that is being constrained, this is mutually exclusive with the <see cref="Values"/>
        /// </summary>
        /// <value> </value>
        ITimeRange TimeRange { get; }

        /// <summary>
        ///     Gets a copy of the list of the allowed / disallowed values in the constraint.
        ///     <p />
        ///     This is mutually exclusive with the <see cref="TimeRange"/> method, and will return an empty list if <see cref="TimeRange"/> returns a non-null value
        /// </summary>
        /// <value> </value>
        IList<string> Values { get; }

        /// <summary>
        /// Gets the cascade values.
        /// </summary>
        /// <value>
        /// Returns a list of all the values which are cascade values
        /// </value>
        IList<string> CascadeValues { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Where the values are coded, and the codes have child codes, if a value if to be cascaded, then include all the child
        /// codes in the constraint for inclusion / exclusion
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// true if the value is set to be cascaded
        /// </returns>
        bool IsCascadeValue(string value);

        #endregion
    }
}
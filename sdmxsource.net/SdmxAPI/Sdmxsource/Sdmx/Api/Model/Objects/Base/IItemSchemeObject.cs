// -----------------------------------------------------------------------
// <copyright file="IItemSchemeObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// An ItemScheme contains a list of items
    /// </summary>
    /// <typeparam name="T">
    /// The item type. Which is a <see cref="IItemObject"/> based class
    /// </typeparam>
    public interface IItemSchemeObject<T> : IMaintainableObject
        where T : IItemObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the list of items contained within this scheme.
        ///     <p />
        ///     <b>NOTE</b>The list is a copy so modify the returned set will not
        ///     be reflected in the IItemSchemeObject instance
        /// </summary>
        //// TODO Use ReadOnlyCollection<T> to avoid copying the list.
        IList<T> Items { get; } 

        /// <summary>
        ///     Gets a value indicating whether this is a partial item scheme.
        ///     Gets a value indicating whether the is a partial item scheme.
        /// </summary>
        bool Partial { get; }

        /// <summary>
        /// Filters the items.
        /// </summary>
        /// <param name="filterIds">The filter ids.</param>
        /// <param name="isKeepSet">if set to <c>true</c> [is keep set].</param>
        /// <returns>A new <see cref="IItemSchemeObject{T}"/> with filtered items</returns>
        IItemSchemeObject<T> FilterItems(ICollection<string> filterIds, bool isKeepSet);

        #endregion
    }
}
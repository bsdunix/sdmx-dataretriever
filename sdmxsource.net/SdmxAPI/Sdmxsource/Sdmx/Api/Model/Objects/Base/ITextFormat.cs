// -----------------------------------------------------------------------
// <copyright file="ITextFormat.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    #endregion

    /// <summary>
    ///     ITextFormat gives information about the restrictions on a text value
    /// </summary>
    public interface ITextFormat : ISdmxObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the number of decimal places, if the type is number
        ///     <p />
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value> </value>
        long? Decimals { get; }

        /// <summary>
        ///     Gets the latest end time the value can take -
        /// </summary>
        /// <value> null if not defined </value>
        ISdmxDate EndTime { get; }

        /// <summary>
        ///     Gets the end value of text field that the value must take
        ///     <p />
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value> </value>
        decimal? EndValue { get; }

        /// <summary>
        ///     Gets the interval of for the text values, this is typical given if isInterval() is true
        ///     <p />
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value> </value>
        decimal? Interval { get; }

        /// <summary>
        ///     Gets the is multi lingual.
        /// </summary>
        TertiaryBool Multilingual { get; }

        /// <summary>
        ///     Gets the maximum length of text field that the value must take
        ///     <p />
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value> </value>
        long? MaxLength { get; }

        /// <summary>
        ///     Gets the maximum value that the numerical value must take
        ///     <p />
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value> </value>
        decimal? MaxValue { get; }

        /// <summary>
        ///     Gets the minimum length of text field that the value must take
        ///     <p />
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value> </value>
        long? MinLength { get; }

        /// <summary>
        ///     Gets the maximum value that the numerical value must take
        ///     <p />
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value> </value>
        decimal? MinValue { get; }

        /// <summary>
        ///     Gets the pattern that the reported value must adhere to
        /// </summary>
        /// <value> </value>
        string Pattern { get; }

        /// <summary>
        ///     Gets the earliest start time the value can take -
        /// </summary>
        /// <value> null if not defined </value>
        ISdmxDate StartTime { get; }

        /// <summary>
        ///     Gets the start value of text field that the value must take
        ///     <p />
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value> </value>
        decimal? StartValue { get; }

        /// <summary>
        ///     Gets the type the values the text value can take
        /// </summary>
        /// <value> null if not defined </value>
        TextType TextType { get; }

        /// <summary>
        ///     Gets the interval of for the text values, this is typically given if isInterval() is true
        ///     <p />
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value> </value>
        string TimeInterval { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Indicates whether the values are intended to be ordered, and it may work in combination with the interval attribute
        ///     <p />
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value> The &lt; see cref= &quot; TertiaryBool &quot; / &gt; . </value>
        TertiaryBool Sequence { get; }

        #endregion
    }
}
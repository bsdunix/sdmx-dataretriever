﻿// -----------------------------------------------------------------------
// <copyright file="IContact.cs" company="EUROSTAT">
//   Date Created : 2013-02-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represents a contact (individual or organisation)
    /// </summary>
    public interface IContact : ISdmxObject
    {
        /// <summary>
        /// Gets the id of the contact the id, or null if there is no id
        /// </summary>
        string Id{ get; }

        /// <summary>
        /// Gets the names of the contact list of names, or an empty list if none exist
        /// </summary>
        IList<ITextTypeWrapper> Name{ get; }

       
        /// <summary>
        /// Gets the roles of the contact
        /// </summary>
        IList<ITextTypeWrapper> Role{ get; }

        /// <summary>
        /// Gets the departments of the contact
        /// </summary>
        IList<ITextTypeWrapper> Departments{ get; }

     
        /// <summary>
        /// Gets the email of the contact
        /// </summary>
        IList<string> Email{ get; }

    
        /// <summary>
        /// Gets the fax of the contact
        /// </summary>
        IList<string> Fax{ get; }

     
        /// <summary>
        /// Gets the telephone of the contact
        /// </summary>
        IList<string> Telephone{ get; }
      
        /// <summary>
        /// Gets the uris of the contact
        /// </summary>
        IList<string> Uri{ get; }

    
        /// <summary>
        /// Gets the x400 of the contact
        /// </summary>
        IList<string> X400{ get; }
    }
}

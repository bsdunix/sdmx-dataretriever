// -----------------------------------------------------------------------
// <copyright file="IConstrainableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     A constrainable @object is one which can have constraints associated with it.
    ///     <p />
    ///     Constraints are not `by composition` but `by reference` therefore a constraint can only reference a constrainable artifact,
    ///     a constrainable artifact does not contain the constraint.
    /// </summary>
    public interface IConstrainableObject : IIdentifiableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the cross referenced constrainables.
        /// <li>Registration would return reference to a provision agreement</li>
        /// <li>Provision Agreement would return a reference to a dataflow and a Data Provider</li>
        /// <li>Dataflow would return a reference to a data structure</li>
        /// <li>Data Structure would return null</li>
        /// (example a provision agreement would return a reference to a dataflow) 
        /// </summary>
        IList<ICrossReference> CrossReferencedConstrainables { get; }

        #endregion
    }
}
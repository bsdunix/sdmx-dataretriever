// -----------------------------------------------------------------------
// <copyright file="ISdmxStructure.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     An ISdmxStructure is a @object which represents an SDMX Structural metadata artefact, such as a Code / Codelist etc.
    /// </summary>
    public interface ISdmxStructure : ISdmxObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets a set of identifiable that are contained within this identifiable
        /// </summary>
        /// <value> </value>
        ISet<IIdentifiableObject> IdentifiableComposites { get; }

        /// <summary>
        ///     Gets the first identifiable parent of this SDMXObject
        ///     <p />
        ///     If this is a MaintainableObject, then there will be no parent to return, so will return a value of null
        /// </summary>
        /// <value> </value>
        IIdentifiableObject IdentifiableParent { get; }

        /// <summary>
        ///     Gets the maintainable parent, by recurring up the parent tree to find
        ///     If this is a maintainable then it will return a reference to itself.
        /// </summary>
        /// <value> </value>
        IMaintainableObject MaintainableParent { get; }

        /// <summary>
        ///     Gets the parent that this SdmxObject belongs to.
        ///     If this is a Maintainable Object, then there will be no parent to return, so will return a value of null
        /// </summary>
        /// <value> </value>
        new ISdmxStructure Parent { get; }

        #endregion
    }
}
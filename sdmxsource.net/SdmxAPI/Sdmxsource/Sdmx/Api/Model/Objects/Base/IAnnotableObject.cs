// -----------------------------------------------------------------------
// <copyright file="IAnnotableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     An AnnotableObject Object is one which can contain annotations
    ///     <p />
    ///     This is an immutable @object - this @object can not be modified
    /// </summary>
    public interface IAnnotableObject : ISdmxStructure
    {
        #region Public Properties

        /// <summary>
        ///     Gets the list of annotations
        ///     <p />
        ///     <b>NOTE</b>The list is a copy so modify the returned list will not
        ///     be reflected in the AnnotableObject instance
        /// </summary>
        /// <value> </value>
        IList<IAnnotation> Annotations { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets a value indicating whether the @object has an annotation with the given type
        /// </summary>
        /// <param name="annoationType">Anotation type
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> .
        /// </returns>
        bool HasAnnotationType(string annoationType);

        /// <summary>
        /// Returns the annotations with the given type, returns an empty Set is no annotations exist that have a type which
        /// matches the given string
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        ISet<IAnnotation> GetAnnotationsByType(string type);

        /// <summary>
        /// Returns the annotations with the given title, returns an empty Set is no annotations exist that have a type which
        /// matches the given string
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        ISet<IAnnotation> GetAnnotationsByTitle(string title);

        #endregion
    }
}
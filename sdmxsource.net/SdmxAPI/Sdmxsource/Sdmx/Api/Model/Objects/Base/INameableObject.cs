// -----------------------------------------------------------------------
// <copyright file="INameableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     Nameable Objects carry a mandatory name and an optional description, Nameable Objects are also Identifiable
    /// </summary>
    public interface INameableObject : IIdentifiableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the description in the default locale
        /// </summary>
        /// <value> first locale value it finds or null if there are none </value>
        string Description { get; }

        /// <summary>
        ///     Gets a list of descriptions for this component
        ///     <p />
        ///     <b>NOTE:</b>The list is a copy so modifying the returned list will not
        ///     be reflected in the IIdentifiableObject instance
        /// </summary>
        /// <value> </value>
        IList<ITextTypeWrapper> Descriptions { get; }

        /// <summary>
        ///     Gets the name in the default locale
        /// </summary>
        /// <value> </value>
        string Name { get; }

        /// <summary>
        ///     Gets a list of names for this component - will return an empty list if no Names exist.
        ///     <p />
        ///     <b>NOTE:</b>The list is a copy so modifying the returned list will not
        ///     be reflected in the IIdentifiableObject instance
        /// </summary>
        /// <value> first locale value it finds or null if there are none </value>
        IList<ITextTypeWrapper> Names { get; }

        #endregion
    }
}
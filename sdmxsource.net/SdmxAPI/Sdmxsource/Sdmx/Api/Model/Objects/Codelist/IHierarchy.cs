// -----------------------------------------------------------------------
// <copyright file="IHierarchy.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Represents an SDMX Hierarchy, belonging to a HierarchicalCodelist
    /// </summary>
    /// <seealso cref="T:Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist.IHierarchicalCodelistObject" />
    public interface IHierarchy : IItemObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the hierarchical code Objects.
        /// </summary>
        /// <value> the child IHierarchical Codes for this hierarchy, returns an empty list if there are no children </value>
        IList<IHierarchicalCode> HierarchicalCodeObjects { get; }

        /// <summary>
        ///     Gets the level.
        /// </summary>
        /// <value> the level for this hierarchy, returns null if there is no level </value>
        ILevelObject Level { get; }

        /// <summary>
        ///     Gets the maintainable parent.
        /// </summary>
        new IHierarchicalCodelistObject MaintainableParent { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the ILevelObject at the position indicated, by recursing the ILevelObject hierarchy of this Heirarchy @object, returns null if there is no level
        /// </summary>
        /// <param name="levelPos">The level position
        /// </param>
        /// <returns>
        /// the level at the given hierarchical position (0 indexed) - returns null if there is no level at that position
        /// </returns>
        ILevelObject GetLevelAtPosition(int levelPos);

        /// <summary>
        ///     If true this indicates that the hierarchy has formal levels. In this case, every code should have a level associated with it.
        ///     If false this does not have formal levels.  This hierarchy may still have levels, getLevel() may still return a value, the levels are not formal and the call to a code for its level may return null.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasFormalLevels();

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ITimeRange.cs" company="EUROSTAT">
//   Date Created : 2013-03-04
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface ITimeRange
    {
        /**
	 * If true then start date and end date both have a value, and the range is between the start and end dates.
	 * <p/>
	 * If false, then only the start date or end date will be populated, if the start date is populated then it this period refers
	 * to dates before the start date. If the end date is populated then it refers to dates after the end date.
	 * @return
	 */
        /// <summary>
        /// If true then start date and end date both have a value, and the range is between the start and end dates.
        /// 
        /// </summary>
        bool IsRange{ get; }

        /**
         * Returns the Start Date - if range is true, or the Before date if range is false
         * @return
         */
        /// <summary>
        /// Returns the Start Date - if range is true, or the Before date if range is false
        /// </summary>
        ISdmxDate StartDate{ get; }

        /**
         * Returns the End Date - if range is true, or the After date if range is false
         * @return
         */
        ISdmxDate EndDate{ get; }

        /**
         * Returns true if the start date is included in the range
         * @return
         */
        /// <summary>
        /// 
        /// </summary>
        bool IsStartInclusive{ get; }

        /**
         * Returns true if the end date is included in the range
         * @return
         */
        /// <summary>
        /// 
        /// </summary>
        bool IsEndInclusive{ get; }
    }
}

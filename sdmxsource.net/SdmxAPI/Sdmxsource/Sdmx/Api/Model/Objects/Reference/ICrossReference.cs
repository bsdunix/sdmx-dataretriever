// -----------------------------------------------------------------------
// <copyright file="ICrossReference.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     A Cross Reference is a fully formed (all parameters present) immutable structure reference.
    ///     <p />
    ///     A Cross Reference uniquely references a single structure
    /// </summary>
    public interface ICrossReference : IStructureReference
    {
        #region Public Properties

        /// <summary>
        ///     Gets the structure that this cross reference belongs to
        /// </summary>
        /// <value> </value>
        ISdmxObject ReferencedFrom { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Creates a mutable instance of this cross reference
        /// </summary>
        /// <returns>
        ///     The <see cref="IStructureReference" /> .
        /// </returns>
        IStructureReference CreateMutableInstance();

        /// <summary>
        /// Gets a value indicating whether the the identifiable @object is a match for this cross reference
        /// </summary>
        /// <param name="identifiableObject">Identifiable Object
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> .
        /// </returns>
        bool IsMatch(IIdentifiableObject identifiableObject);

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="IIdentifiableRefObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Used to reference an SDMX identifiable artifact
    /// </summary>
    public interface IIdentifiableRefObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets a reference to the child identifiable artifact that is being referenced.
        ///     returns null if there is no child identifiable being referenced (i.e. this is the end of the reference chain)
        /// </summary>
        /// <value> </value>
        IIdentifiableRefObject ChildReference { get; }

        /// <summary>
        ///     Gets the id of the identifiable that is being referenced (at this level of the referencing hierarchy)
        /// </summary>
        /// <value> </value>
        string Id { get; }

        /// <summary>
        ///     Gets a reference to the parent identifiable artifact (not maintainable) that this identifiable
        ///     artifact is a child of, returns null if the identifiable artifact has no parent identifiable artifact
        /// </summary>
        /// <value> </value>
        IIdentifiableRefObject ParentIdentifiableReference { get; }

        /// <summary>
        ///     Gets a reference to the parent maintainable that this identifiable is a child of, this will never be null
        /// </summary>
        /// <value> </value>
        IStructureReference ParentMaintainableReferece { get; }

        /// <summary>
        ///     Gets the identifiable structure that is being referenced (at this level of the referencing hierarchy)
        /// </summary>
        /// <value> </value>
        SdmxStructureType StructureEnumType { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the matched identifiable @object, from the given identifiable - returns null if no match is found
        /// </summary>
        /// <param name="reference">The referemce. </param>
        /// <returns>
        /// The <see cref="IIdentifiableObject"/> .
        /// </returns>
        IIdentifiableObject GetMatch(IIdentifiableObject reference);

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="IDataAndMetadataSetReference.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;

    #endregion

    /// <summary>
    ///     The DataAndMetadataSetReference interface.
    /// </summary>
    public interface IDataAndMetadataSetReference
    {
        #region Public Properties

        /// <summary>
        ///     Gets the dataset referenced by data flow/provider/provision
        /// </summary>
        /// <value> </value>
        ICrossReference DataSetReference { get; }

        /// <summary>
        ///     Gets a value indicating whether the is a dataset reference, false if it is a metadata set reference
        /// </summary>
        /// <value> </value>
        bool IsDataSetReference { get; }

        /// <summary>
        ///     Gets the id of the data or metadata set
        /// </summary>
        /// <value> </value>
        string SetId { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Gets a mutable version
        /// </summary>
        /// <returns>
        ///     The <see cref="IDataAndMetadataSetMutableReference" /> .
        /// </returns>
        IDataAndMetadataSetMutableReference CreateMutableInstance();

        #endregion
    }
}
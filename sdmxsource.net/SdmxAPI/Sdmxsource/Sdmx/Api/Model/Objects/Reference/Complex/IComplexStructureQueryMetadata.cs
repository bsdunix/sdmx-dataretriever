﻿// -----------------------------------------------------------------------
// <copyright file="IComplexStructureQueryMetadata.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    #endregion

    /// <summary>
    /// Contains information about a Complex Structure Query.  The information includes the response detail, 
    /// and which referenced artefacts should be referenced in the response
    /// </summary>
    public interface IComplexStructureQueryMetadata
    {
        #region Public Methods and Operators

        /// <summary>
        /// Return true if the matched artefact should be returned in the query result besides the referenced artefacts.
        /// </summary>
        /// <returns>
        /// The return value
        /// </returns>
        bool IsReturnedMatchedArtefact();

        /// <summary>
        /// Returns the query detail for this structure query, can not be null
        /// </summary>
        ComplexStructureQueryDetail StructureQueryDetail
        {
            get;
        }

        /// <summary>
        /// Returns the query details for the resolved references, can not be null
        /// </summary>
        ComplexMaintainableQueryDetail ReferencesQueryDetail
        {
            get;
        }

        /// <summary>
        /// Returns the reference detail for this structure query, can not be null
        /// </summary>
        StructureReferenceDetail StructureReferenceDetail
        {
            get;
        }

        /// <summary>
        /// If STRUCTURE_REFERENCE_DETAIL == SPECIFIC, this method will return the specific structures for getting references, returns null otherwise
        /// </summary>
        IList<SdmxStructureType> ReferenceSpecificStructures
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether the attribute processConstraints is set to true. Triggers potential creation of partial structures.
        /// </summary>
        /// <value><c>true</c> if the attribute processConstraints is set to true. otherwise, <c>false</c>.</value>
        bool IsProcessConstraints { get; }

        #endregion
    }
}

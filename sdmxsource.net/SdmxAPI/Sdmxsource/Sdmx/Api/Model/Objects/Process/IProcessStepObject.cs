// -----------------------------------------------------------------------
// <copyright file="IProcessStepObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Process
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Represents an SDMX Process Step
    /// </summary>
    public interface IProcessStepObject : INameableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the computation.
        /// </summary>
        IComputationObject Computation { get; }

        /// <summary>
        ///     Gets the inputs of this Process Step as a copy of the underlying list.
        ///     <p />
        ///     Gets an empty list if no inputs exist
        /// </summary>
        /// <value> </value>
        IList<IInputOutputObject> Input { get; }

        /// <summary>
        ///     Gets the outputs of this process step as a copy of the underlying list.
        ///     <p />
        ///     Gets an empty list if no outputs exist
        /// </summary>
        /// <value> </value>
        IList<IInputOutputObject> Output { get; }

        /// <summary>
        ///     Gets the child Process Steps as a copy of the underlying list.
        ///     <p />
        ///     Gets an empty list if no child process steps exist
        /// </summary>
        /// <value> </value>
        IList<IProcessStepObject> ProcessSteps { get; }

        /// <summary>
        ///     Gets TransitionObjects as a copy of the underlying list.
        ///     <p />
        ///     Gets an empty list if no ITransition exist
        /// </summary>
        /// <value> </value>
        IList<ITransition> Transitions { get; }

        #endregion
    }
}
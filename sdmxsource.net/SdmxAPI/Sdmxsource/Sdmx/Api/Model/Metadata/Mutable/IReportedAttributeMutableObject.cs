// -----------------------------------------------------------------------
// <copyright file="IReportedAttributeMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Metadata.Mutable
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     The ReportedAttributeMutableObject interface.
    /// </summary>
    public interface IReportedAttributeMutableObject : IIdentifiableMutableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the reported attributes.
        /// </summary>
        /// <value> child attributes </value>
        IList<IReportedAttributeObject> ReportedAttributes { get; }

        /// <summary>
        ///    Gets the structured Text is XHTML
        /// </summary>
        /// <value> the structured text in the default locale </value>
        string StructuredText { get; }

        /// <summary>
        ///     Gets the structured Text is XHTML
        /// </summary>
        /// <value> a list of structured texts for this component - will return an empty list if no Texts exist. </value>
        IList<ITextTypeWrapper> StructuredTexts { get; }

        /// <summary>
        ///     Gets the text.
        /// </summary>
        /// <value> the text in the default locale </value>
        string Text { get; }

        /// <summary>
        ///    Gets the texts.
        /// </summary>
        /// <value> a list of texts for this component - will return an empty list if no Texts exist. </value>
        IList<ITextTypeWrapper> Texts { get; }

        #endregion

        // /**
        // * @return a list of texts for this component - will return an empty list if no Texts exist.
        // */
        // void setTexts(List<ITextTypeWrapper> texts);
        // /**
        // * @return the text in the default locale
        // */
        // string getText();
        // /**
        // * Structured Text is XHTML
        // * @return a list of structured texts for this component - will return an empty list if no Texts exist.
        // */
        // List<ITextTypeWrapper> getStructuredTexts();
        // /**
        // * Structured Text is XHTML
        // * @return the structured text in the default locale
        // */
        // string getStructuredText();
        // /**
        // * @return child attributes
        // */
        // List<IReportedAttributeObject> getReportedAttributes();
    }
}
// -----------------------------------------------------------------------
// <copyright file="IMetadataSet.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Metadata
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     The MetadataSet interface.
    /// </summary>
    public interface IMetadataSet : ISdmxObject
    {
        #region Public Properties


        /// <summary>
        /// Gets a list of metadata sets, built from this metadata set and metadata reports.  Each metadata set has one metadata report in it.
        /// </summary>
        IList<IMetadataSet> SplitReports { get; }

        /// <summary>
        /// Gets an identification for the metadata set
        /// </summary>
        string SetId { get; }

        /// <summary>
        /// Gets a list of names for this component - will return an empty list if no Names exist.
        /// <remarks>
        /// The list is a copy so modifying the returned list will not be reflected in the IdentifiableBean instance
        /// </remarks>
        /// </summary>
        /// <returns></returns>
        IList<ITextTypeWrapper> Names { get; }


        /// <summary>
        ///     Gets reference to the DataProvider
        /// </summary>
        /// <value> </value>
        ICrossReference DataProviderReference { get; }

        /// <summary>
        ///     Gets a reference to the metadata structure that defines the structure of this metadata set. Mandatory
        /// </summary>
        ICrossReference MsdReference { get; }

        /// <summary>
        ///     Gets  the publication period
        /// </summary>
        /// <value> </value>
        object PublicationPeriod { get; }

        /// <summary>
        ///     Gets the four digit ISo 8601 year of publication
        /// </summary>
        ISdmxDate PublicationYear { get; }

        /// <summary>
        ///     Gets the inclusive start time of the data reported in the metadata set
        /// </summary>
        /// <value> </value>
        ISdmxDate ReportingBeginDate { get; }

        /// <summary>
        ///     Gets  the inclusive end time of the data reported in the metadata set
        /// </summary>
        /// <value> </value>
        ISdmxDate ReportingEndDate { get; }

        /// <summary>
        ///     Gets the reports.
        /// </summary>
        IList<IMetadataReport> Reports { get; }

        /// <summary>
        ///     Gets the inclusive start time of the validity of the info in the metadata set
        /// </summary>
        /// <value> </value>
        ISdmxDate ValidFromDate { get; }

        /// <summary>
        ///     Gets the inclusive end time of the validity of the info in the metadata set.
        /// </summary>
        /// <value>  </value>
        ISdmxDate ValidToDate { get; }

        #endregion
    }
}
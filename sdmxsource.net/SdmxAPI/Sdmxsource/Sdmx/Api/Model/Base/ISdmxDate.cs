// -----------------------------------------------------------------------
// <copyright file="ISdmxDate.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Base
{
    #region Using directives

    using System;
    using System.Globalization;
    using System.Runtime.Serialization;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    #endregion

    /// <summary>
    ///     An SDMX date contains a Java Date, and gives access to the string representation and TIME_FORMAT which is used to define the date
    /// </summary>
    public interface ISdmxDate : ISerializable
    {
        #region Public Properties

        /// <summary>
        ///     Gets a copy of the date  - this can never be null
        /// </summary>
        /// <value> </value>
        DateTime? Date { get; }

        /// <summary>
        ///     Gets the Date in SDMX format
        /// </summary>
        string DateInSdmxFormat { get; }

        /// <summary>
        ///     Gets the time format for the date - returns null if this information is not present
        /// </summary>
        /// <value> </value>
        TimeFormat TimeFormatOfDate { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets a value indicating whether the date is later then the date provided
        /// </summary>
        /// <param name="date">Sdmx date
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> .
        /// </returns>
        bool IsLater(ISdmxDate date);

        #endregion
    }
}
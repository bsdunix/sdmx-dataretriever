// -----------------------------------------------------------------------
// <copyright file="IAnnotableMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     The AnnotableMutableObject interface.
    /// </summary>
    public interface IAnnotableMutableObject : IMutableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the annotations.
        /// </summary>
        IList<IAnnotationMutableObject> Annotations { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add annotation.
        /// </summary>
        /// <param name="annotation">
        /// The annotation.
        /// </param>
        void AddAnnotation(IAnnotationMutableObject annotation);


        /// <summary>
        /// Adds an annotation and returns it 
        /// </summary>
        /// <param name="title">The title</param>
        /// <param name="type">The type</param>
        /// <param name="url">The URL</param>
        /// <returns></returns>
        IAnnotationMutableObject AddAnnotation(string title, string type, string url);
        #endregion
    }
}
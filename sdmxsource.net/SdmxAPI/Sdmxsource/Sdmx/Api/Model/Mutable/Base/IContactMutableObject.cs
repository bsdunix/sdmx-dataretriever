﻿// -----------------------------------------------------------------------
// <copyright file="IContactMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-03-11
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IContactMutableObject : IMutableObject
    {
        /// <summary>
        /// Gets or sets the id of the contact
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// Gets the names of the contact.
        /// </summary>
        /// <value>
        /// list of names, or an empty list if none exist
        /// </value>
        IList<ITextTypeWrapperMutableObject> Names { get; }

        /// <summary>
        /// Adds a new name to the list, creates a new list if it is null
        /// </summary>
        /// <param name="name">The name.</param>
        void AddName(ITextTypeWrapperMutableObject name);

        /// <summary>
        /// Gets or sets the roles of the contact
        /// </summary>
        /// <value>
        /// The roles.
        /// </value>
        IList<ITextTypeWrapperMutableObject> Roles { get; }

        /// <summary>
        /// Adds a new role to the list, creates a new list if it is null
        /// </summary>
        /// <param name="role"></param>
        void AddRole(ITextTypeWrapperMutableObject role);


        /// <summary>
        /// Gets or sets the departments of the contact
        /// </summary>
        IList<ITextTypeWrapperMutableObject> Departments { get; }

        /// <summary>
        /// Adds a new department to the list, creates a new list if it is null
        /// </summary>
        /// <param name="dept"></param>
        void AddDepartment(ITextTypeWrapperMutableObject dept);

        /// <summary>
        /// Gets the email of the contact
        /// </summary>
        IList<string> Email { get; } 

        /// <summary>
        /// Adds a new email to the list, creates a new list if it is null
        /// </summary>
        /// <param name="email"></param>
        void AddEmail(string email);

        /// <summary>
        /// Gets the fax of the contact
        /// </summary>
        IList<string> Fax { get; }

        /// <summary>
        /// Adds a new fax to the list, creates a new list if it is null
        /// </summary>
        /// <param name="fax"></param>
        void AddFax(string fax);

        /// <summary>
        /// Gets the telephone of the contact
        /// </summary>
        IList<string> Telephone { get; }

        /// <summary>
        /// Adds a new telephone to the list, creates a new list if it is null
        /// </summary>
        /// <param name="telephone"></param>
        void AddTelephone(string telephone);
        /**
         * Returns the uris of the contact
         * @return list of uris, or an empty list if none exist
         */
        /// <summary>
        /// 
        /// </summary>
        IList<string> Uri { get; }

        /// <summary>
        /// Adds a new uri to the list, creates a new list if it is null
        /// </summary>
        /// <param name="uri"></param>
        void AddUri(string uri);

        /// <summary>
        /// Returns the x400 of the contact
        /// </summary>
        IList<string> X400 { get; }

        /// <summary>
        /// Adds a new X400 to the list, creates a new list if it is null
        /// </summary>
        /// <param name="x400"></param>
        void AddX400(string x400);
    }
}

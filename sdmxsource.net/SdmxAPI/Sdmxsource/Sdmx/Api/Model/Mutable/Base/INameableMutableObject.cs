// -----------------------------------------------------------------------
// <copyright file="INameableMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     The NameableMutableObject interface.
    /// </summary>
    public interface INameableMutableObject : IIdentifiableMutableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the description.
        /// </summary>
        IList<ITextTypeWrapperMutableObject> Descriptions { get; }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        IList<ITextTypeWrapperMutableObject> Names { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Adds a description, edits the current value if the locale already exists
        /// </summary>
        /// <param name="locale">The locale
        /// </param>
        /// <param name="name">The description name
        /// </param>
        void AddDescription(string locale, string name);

        /// <summary>
        /// Adds a name, edits the current value if the locale already exists
        /// </summary>
        /// <param name="locale">The locale
        /// </param>
        /// <param name="name">The name
        /// </param>
        void AddName(string locale, string name);

        /// <summary>
        /// The get name.
        /// </summary>
        /// <param name="defaultIfNull">
        /// The default if null.
        /// </param>
        /// <returns>
        /// The <see cref="string"/> .
        /// </returns>
        string GetName(bool defaultIfNull);

        #endregion
    }
}
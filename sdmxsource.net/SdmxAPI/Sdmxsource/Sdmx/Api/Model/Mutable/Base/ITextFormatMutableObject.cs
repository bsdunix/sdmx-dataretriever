// -----------------------------------------------------------------------
// <copyright file="ITextFormatMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Constants;

    #endregion

    /// <summary>
    ///     The TextFormatMutableObject interface.
    /// </summary>
    public interface ITextFormatMutableObject : IMutableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the decimals.
        /// </summary>
        long? Decimals { get; set; }

        /// <summary>
        ///     Gets or sets the end value.
        /// </summary>
        decimal? EndValue { get; set; }

        /// <summary>
        ///     Gets or sets the interval.
        /// </summary>
        decimal? Interval { get; set; }

        /// <summary>
        ///     Gets or sets the max length.
        /// </summary>
        long? MaxLength { get; set; }

        /// <summary>
        ///     Gets or sets the max value.
        /// </summary>
        decimal? MaxValue { get; set; }

        /// <summary>
        ///     Gets or sets the min length.
        /// </summary>
        long? MinLength { get; set; }

        /// <summary>
        ///     Gets or sets the min value.
        /// </summary>
        decimal? MinValue { get; set; }

        /// <summary>
        ///     Gets or sets the multi lingual.
        /// </summary>
        TertiaryBool Multilingual { get; set; }

        /// <summary>
        ///     Gets or sets the pattern.
        /// </summary>
        string Pattern { get; set; }

        /// <summary>
        ///     Gets or sets the sequence.
        /// </summary>
        TertiaryBool Sequence { get; set; }

        /// <summary>
        ///     Gets or sets the start value.
        /// </summary>
        decimal? StartValue { get; set; }

        /// <summary>
        ///     Gets or sets the text type.
        /// </summary>
        TextType TextType { get; set; }

        /// <summary>
        ///     Gets or sets the time interval.
        /// </summary>
        string TimeInterval { get; set; }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="IProcessStepMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    #endregion

    /// <summary>
    ///     The ProcessStepMutableObject interface.
    /// </summary>
    public interface IProcessStepMutableObject : INameableMutableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the computation.
        /// </summary>
        IComputationMutableObject Computation { get; set; }

        /// <summary>
        ///     Gets the input.
        /// </summary>
        IList<IInputOutputMutableObject> Input { get; }

        /// <summary>
        ///     Gets the output.
        /// </summary>
        IList<IInputOutputMutableObject> Output { get; }

        /// <summary>
        ///     Gets the process steps.
        /// </summary>
        IList<IProcessStepMutableObject> ProcessSteps { get; }

        /// <summary>
        ///     Gets the transitions.
        /// </summary>
        IList<ITransitionMutableObject> Transitions { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add process step.
        /// </summary>
        /// <param name="processStep">
        /// The process step.
        /// </param>
        void AddProcessStep(IProcessStepMutableObject processStep);

        /// <summary>
        /// The add transition.
        /// </summary>
        /// <param name="transition">
        /// The transition.
        /// </param>
        void AddTransition(ITransitionMutableObject transition);

        #endregion
    }
}
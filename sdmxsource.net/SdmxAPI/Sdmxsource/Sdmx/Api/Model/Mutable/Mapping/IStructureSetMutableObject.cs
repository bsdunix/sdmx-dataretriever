// -----------------------------------------------------------------------
// <copyright file="IStructureSetMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;

    #endregion

    /// <summary>
    ///     The StructureSetMutableObject interface.
    /// </summary>
    public interface IStructureSetMutableObject : IMaintainableMutableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the category scheme map list.
        /// </summary>
        IList<ICategorySchemeMapMutableObject> CategorySchemeMapList { get; }

        /// <summary>
        ///     Gets the codelist map list.
        /// </summary>
        IList<ICodelistMapMutableObject> CodelistMapList { get; }

        /// <summary>
        ///     Gets the concept scheme map list.
        /// </summary>
        IList<IConceptSchemeMapMutableObject> ConceptSchemeMapList { get; }

        /// <summary>
        ///     Gets a representation of itself in a @object which can not be modified, modifications to the mutable @object
        ///     are not reflected in the returned instance of the IMaintainableObject.
        /// </summary>
        /// <value> </value>
        new IStructureSetObject ImmutableInstance { get; }

        /// <summary>
        ///     Gets the organisation scheme map list.
        /// </summary>
        IList<IOrganisationSchemeMapMutableObject> OrganisationSchemeMapList { get; }

        /// <summary>
        ///     Gets or sets the related structures.
        /// </summary>
        IRelatedStructuresMutableObject RelatedStructures { get; set; }

        /// <summary>
        ///     Gets the structure map list.
        /// </summary>
        IList<IStructureMapMutableObject> StructureMapList { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add category scheme map.
        /// </summary>
        /// <param name="categorySchemeMap">
        /// The category scheme map.
        /// </param>
        void AddCategorySchemeMap(ICategorySchemeMapMutableObject categorySchemeMap);

        /// <summary>
        /// The add codelist map.
        /// </summary>
        /// <param name="codelistMap">
        /// The codelist map.
        /// </param>
        void AddCodelistMap(ICodelistMapMutableObject codelistMap);

        /// <summary>
        /// The add concept scheme map.
        /// </summary>
        /// <param name="conceptSchemeMap">
        /// The concept scheme map.
        /// </param>
        void AddConceptSchemeMap(IConceptSchemeMapMutableObject conceptSchemeMap);

        /// <summary>
        /// The add organisation scheme map.
        /// </summary>
        /// <param name="organisationSchemeMap">
        /// The organisation scheme map.
        /// </param>
        void AddOrganisationSchemeMap(IOrganisationSchemeMapMutableObject organisationSchemeMap);

        /// <summary>
        /// The add structure map.
        /// </summary>
        /// <param name="structureMap">
        /// The structure map.
        /// </param>
        void AddStructureMap(IStructureMapMutableObject structureMap);

        #endregion
    }
}
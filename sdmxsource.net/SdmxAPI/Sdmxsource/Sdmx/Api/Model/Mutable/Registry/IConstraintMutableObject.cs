// -----------------------------------------------------------------------
// <copyright file="IConstraintMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    #endregion

    /// <summary>
    ///     The ConstraintMutableObject interface.
    /// </summary>
    public interface IConstraintMutableObject : IMaintainableMutableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the constraint attachment.
        /// </summary>
        IConstraintAttachmentMutableObject ConstraintAttachment { get; set; }

        /// <summary>
        ///     Gets or sets the excluded series keys.
        /// </summary>
        IConstraintDataKeySetMutableObject ExcludedSeriesKeys { get; set; }

        /// <summary>
        ///     Gets or sets the included series keys.
        /// </summary>
        IConstraintDataKeySetMutableObject IncludedSeriesKeys { get; set; }

        /// <summary>
        /// Gets or sets the metadata keys that this constraint defines at ones that either have data, or are allowed to have data (depending on IsDefiningActualDataPresent value)
        /// </summary>
        IConstraintDataKeySetMutableObject IncludedMetadataKeys { get; set; }

        /// <summary>
        /// Gets or sets the metadata keys that this constraint defines at ones that either do not have data, or are not allowed to have data (depending on IsDefiningActualDataPresent value)
        /// </summary>
        IConstraintDataKeySetMutableObject ExcludedMetadataKeys { get; set; }

        #endregion
    }
}
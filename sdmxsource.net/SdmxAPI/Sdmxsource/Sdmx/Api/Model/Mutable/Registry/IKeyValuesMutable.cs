// -----------------------------------------------------------------------
// <copyright file="IKeyValuesMutable.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    #endregion

    /// <summary>
    ///     The KeyValuesMutable interface.
    /// </summary>
    public interface IKeyValuesMutable : IMutableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the cascade.
        /// </summary>
        IList<string> Cascade { get; }

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        ///     Gets the key values.
        /// </summary>
        IList<string> KeyValues { get; }

        /// <summary>
        ///     Gets or sets the time range.
        /// </summary>
        ITimeRangeMutableObject TimeRange { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add cascade.
        /// </summary>
        /// <param name="value_ren">
        /// The value_ren.
        /// </param>
        void AddCascade(string value_ren);

        /// <summary>
        /// The add value.
        /// </summary>
        /// <param name="value_ren">
        /// The value_ren.
        /// </param>
        void AddValue(string value_ren);

        /// <summary>
        /// The is cascade value.
        /// </summary>
        /// <param name="value_ren">
        /// The value_ren.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> .
        /// </returns>
        bool IsCascadeValue(string value_ren);

        #endregion
    }
}
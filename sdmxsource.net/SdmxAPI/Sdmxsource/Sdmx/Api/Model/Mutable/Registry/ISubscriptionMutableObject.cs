// -----------------------------------------------------------------------
// <copyright file="ISubscriptionMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    ///     The SubscriptionMutableObject interface.
    /// </summary>
    public interface ISubscriptionMutableObject : IMaintainableMutableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets the http post to.
        /// </summary>
        IList<string> HttpPostTo { get; }

        /// <summary>
        ///     Gets a representation of itself in a @object which can not be modified, modifications to the mutable @object
        ///     are not reflected in the returned instance of the IMaintainableObject.
        /// </summary>
        /// <value> </value>
        new ISubscriptionObject ImmutableInstance { get; }

        /// <summary>
        ///     Gets the mail to.
        /// </summary>
        IList<string> MailTo { get; }

        /// <summary>
        ///     Gets or sets the owner.
        /// </summary>
        IStructureReference Owner { get; set; }

        /// <summary>
        ///     Gets the references.
        /// </summary>
        IList<IStructureReference> References { get; }

        /// <summary>
        ///     Gets or sets the subscription type.
        /// </summary>
        SubscriptionEnumType SubscriptionType { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add reference.
        /// </summary>
        /// <param name="reference">
        /// The reference.
        /// </param>
        void AddReference(IStructureReference reference);

        #endregion
    }
}
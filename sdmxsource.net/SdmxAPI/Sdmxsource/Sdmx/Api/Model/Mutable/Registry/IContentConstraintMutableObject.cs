// -----------------------------------------------------------------------
// <copyright file="IContentConstraintMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    ///     The ContentConstraintMutableObject interface.
    /// </summary>
    public interface IContentConstraintMutableObject : IConstraintMutableObject
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the metadata target region
        /// </summary>
        IMetadataTargetRegionMutableObject MetadataTargetRegion { get; set; }

        /// <summary>
        ///     Gets or sets the excluded cube region.
        /// </summary>
        ICubeRegionMutableObject ExcludedCubeRegion { get; set; }

        /// <summary>
        ///     Gets the immutable instance.
        /// </summary>
        new IContentConstraintObject ImmutableInstance { get; }

        /// <summary>
        ///     Gets or sets the included cube region.
        /// </summary>
        ICubeRegionMutableObject IncludedCubeRegion { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is defining actual data present.
        /// </summary>
        bool IsDefiningActualDataPresent { get; set; }

        /// <summary>
        ///     Gets or sets the reference period.
        /// </summary>
        IReferencePeriodMutableObject ReferencePeriod { get; set; }

        /// <summary>
        ///     Gets or sets the release calendar.
        /// </summary>
        IReleaseCalendarMutableObject ReleaseCalendar { get; set; }

        #endregion
    }
}
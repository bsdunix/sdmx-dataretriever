// -----------------------------------------------------------------------
// <copyright file="IRegistrationMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    ///     The RegistrationMutableObject interface.
    /// </summary>
    public interface IRegistrationMutableObject : IMaintainableMutableObject
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the data source.
        /// </summary>
        IDataSourceMutableObject DataSource { get; set; }

        /// <summary>
        ///     Gets a representation of itself in a @object which can not be modified, modifications to the mutable @object
        ///     are not reflected in the returned instance of the RegistrationObject.
        /// </summary>
        /// <value> </value>
        new IRegistrationObject ImmutableInstance { get; }

        /// <summary>
        ///     Gets or sets the index attributes.
        /// </summary>
        TertiaryBool IndexAttributes { get; set; }

        /// <summary>
        ///     Gets or sets the index dataset.
        /// </summary>
        TertiaryBool IndexDataset { get; set; }

        /// <summary>
        ///     Gets or sets the index reporting period.
        /// </summary>
        TertiaryBool IndexReportingPeriod { get; set; }

        /// <summary>
        ///     Gets or sets the index time series.
        /// </summary>
        TertiaryBool IndexTimeseries { get; set; }

        /// <summary>
        ///     Gets or sets the last updated.
        /// </summary>
        DateTime? LastUpdated { get; set; }

        /// <summary>
        ///     Gets or sets the provision agreement ref.
        /// </summary>
        IStructureReference ProvisionAgreementRef { get; set; }

        /// <summary>
        ///     Gets or sets the valid from.
        /// </summary>
        DateTime? ValidFrom { get; set; }

        /// <summary>
        ///     Gets or sets the valid to.
        /// </summary>
        DateTime? ValidTo { get; set; }

        #endregion
    }
}
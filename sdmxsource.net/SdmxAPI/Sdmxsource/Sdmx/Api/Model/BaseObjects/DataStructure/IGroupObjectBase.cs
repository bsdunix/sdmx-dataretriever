// -----------------------------------------------------------------------
// <copyright file="IGroupObjectBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.DataStructure
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     A group is a subset of the dimensionality of the full key.  It is purely to attach attributes.
    /// </summary>
    public interface IGroupObjectBase : IIdentifiableObjectBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets the attachment constraint ref.
        /// </summary>
        ICrossReference AttachmentConstraintRef { get; }

        /// <summary>
        ///     Gets the dimension.
        /// </summary>
        IList<IDimensionObjectBase> Dimensions { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get dimension by id.
        /// </summary>
        /// <param name="conceptId">
        /// The concept id.
        /// </param>
        /// <returns>
        /// The <see cref="IDimensionObjectBase"/> .
        /// </returns>
        IDimensionObjectBase GetDimensionById(string conceptId);

        #endregion
    }
}
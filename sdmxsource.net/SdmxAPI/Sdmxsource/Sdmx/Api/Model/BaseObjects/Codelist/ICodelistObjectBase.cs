// -----------------------------------------------------------------------
// <copyright file="ICodelistObjectBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Codelist
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    #endregion

    /// <summary>
    ///     A ICodelistObjectBase is a container for codes.
    /// </summary>
    public interface ICodelistObjectBase : IMaintainableObjectBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value> The ICodelistObject that this ICodelistObjectBase was built from. </value>
        new ICodelistObject BuiltFrom { get; }

        /// <summary>
        ///     Gets the codes in this codelist. As codes are hierarchical only the top level codes, with no parents, will be returned.
        /// </summary>
        /// <value> the codes in this codelist. </value>
        IList<ICodeObjectBase> Codes { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Iterates through the code hierarchy and returns the ICodeObjectBase that has the same id as that supplied.
        /// </summary>
        /// <param name="id">
        /// the id of a ICodeObjectBase to search for.
        /// </param>
        /// <returns>
        /// the matching ICodeObjectBase or null if there was no match.
        /// </returns>
        ICodeObjectBase GetCodeByValue(string id);

        #endregion
    }
}
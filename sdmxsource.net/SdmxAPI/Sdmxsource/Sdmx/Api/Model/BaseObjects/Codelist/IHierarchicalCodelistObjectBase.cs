// -----------------------------------------------------------------------
// <copyright file="IHierarchicalCodelistObjectBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Codelist
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Base;

    #endregion

    /// <summary>
    ///     A hierarchical codelist is made up of one or more hierarchies,
    ///     each containing codes, which can be hierarchical, where the definition
    ///     of each code is in a SDMX codelist.
    ///     <p />
    ///     Note that the hierarchical codelist allows the same code to be used in more than one hierarchy.
    /// </summary>
    public interface IHierarchicalCodelistObjectBase : IMaintainableObjectBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets the set of hierarchies that this hierarchical codelist
        ///     has reference to
        /// </summary>
        IHierarchyObjectBaseSet<IHierarchicalCodelistObjectBase> Hierarchies { get; }

        #endregion
    }
}
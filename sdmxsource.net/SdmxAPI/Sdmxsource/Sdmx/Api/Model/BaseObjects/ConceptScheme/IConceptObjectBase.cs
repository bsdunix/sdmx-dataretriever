// -----------------------------------------------------------------------
// <copyright file="IConceptObjectBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.ConceptScheme
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;

    #endregion

    /// <summary>
    ///     A concept is a characteristic.  It is the basic building block of data and metadata structures e.g all dimensions, attributes and
    ///     dimensions in a data structure must reference a concept.
    /// </summary>
    public interface IConceptObjectBase : INameableObjectBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        new IConceptObject BuiltFrom { get; }

        /// <summary>
        ///     Gets the representation of this concept - this will return null if <see cref="HasRepresentation" /> is false
        /// </summary>
        /// <value> </value>
        ICodelistObjectBase CoreRepresentation { get; }

        /// <summary>
        ///     Gets a value indicating whether this concept has core representation
        /// </summary>
        bool HasRepresentation { get; }

        /// <summary>
        ///     Gets a value indicating whether the concept is a stand alone concept
        /// </summary>
        /// <value> </value>
        bool StandAloneConcept { get; }

        /// <summary>
        ///     Gets the text format.
        /// </summary>
        ITextFormat TextFormat { get; }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="IAnnotationObjectBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Base
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Globalization;

    #endregion

    /// <summary>
    ///     An Annotation is a piece of information that can be held against an Annotable Object.
    ///     <p />
    ///     It can be thought of as a placeholder for any extraneous information that needs to be stored, where there is no
    ///     other appropriate place to store this.
    /// </summary>
    public interface IAnnotationObjectBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets the text of the annotation in the default locale
        /// </summary>
        /// <value> </value>
        string Text { get; }

        /// <summary>
        ///     Gets the text of the annotation in the default locale
        /// </summary>
        /// <value> </value>
        IDictionary<CultureInfo, string> Texts { get; }

        /// <summary>
        ///     Gets the title of the annotation
        /// </summary>
        /// <value> </value>
        string Title { get; }

        /// <summary>
        ///     Gets the type of the annotation
        /// </summary>
        /// <value> </value>
        string Type { get; }

        /// <summary>
        ///     Gets the Uri of the annotation
        /// </summary>
        /// <value> </value>
        Uri Url { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the text of the annotation in the given locale
        /// </summary>
        /// <param name="locale">The locale
        /// </param>
        /// <returns>
        /// The <see cref="string"/> .
        /// </returns>
        string GetText(CultureInfo locale);

        #endregion
    }
}
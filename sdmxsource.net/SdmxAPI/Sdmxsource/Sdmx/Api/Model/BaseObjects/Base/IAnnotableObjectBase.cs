// -----------------------------------------------------------------------
// <copyright file="IAnnotableObjectBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Base
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     An Annotable Object is one which can hold annotations against it.
    /// </summary>
    public interface IAnnotableObjectBase : IObjectBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets a list of annotations that exist for this Annotable Object
        /// </summary>
        /// <value> </value>
        ISet<IAnnotationObjectBase> Annotations { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets an annotations with the given title, this returns null if no annotation exists with
        ///     the given type
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <returns>
        /// The annotations with the given title, this returns null if no annotation exists with
        ///     the given type
        /// </returns>
        ISet<IAnnotationObjectBase> GetAnnotationByTitle(string title);

        /// <summary>
        /// Gets an annotation with the given type, this returns null if no annotation exists with
        ///     the given type
        /// </summary>
        /// <param name="type">The type. </param>
        /// <returns>
        /// The <see cref="IAnnotationObjectBase"/> .
        /// </returns>
        IAnnotationObjectBase GetAnnotationByType(string type);

        /// <summary>
        /// Gets an annotations with the given url, this returns null if no annotation exists with
        ///     the given type
        /// </summary>
        /// <param name="url">
        /// The url.
        /// </param>
        /// <returns>
        /// The annotations with the given url, this returns null if no annotation exists with
        ///     the given type
        /// </returns>
        ISet<IAnnotationObjectBase> GetAnnotationByUrl(Uri url);

        /// <summary>
        ///     Gets a value indicating whether the annotations exist for this Annotable Object
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasAnnotations();

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="INameableObjectBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Base
{
    #region Using directives

    using System.Collections.Generic;
    using System.Globalization;

    #endregion

    /// <summary>
    ///     The NameableObjectBase interface.
    /// </summary>
    public interface INameableObjectBase : IIdentifiableObjectBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets the Description of the Identifiable object for a the default Locale
        /// </summary>
        /// <value> </value>
        /// <value> </value>
        string Description { get; }

        /// <summary>
        ///     Gets the descriptions.
        /// </summary>
        IDictionary<CultureInfo, string> Descriptions { get; }

        /// <summary>
        ///     Gets the Name of the Identifiable object for a the default Locale
        /// </summary>
        /// <value> </value>
        /// <value> </value>
        string Name { get; }

        /// <summary>
        ///     Gets the names.
        /// </summary>
        IDictionary<CultureInfo, string> Names { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the description of the Identifiable Object for the given Locale
        /// </summary>
        /// <param name="loc">The locale
        /// </param>
        /// <returns>
        /// The <see cref="string"/> .
        /// </returns>
        string GetDescription(CultureInfo loc);

        /// <summary>
        /// Gets the Name of the Identifiable object for a given Locale
        /// </summary>
        /// <param name="loc">The locale
        /// </param>
        /// <returns>
        /// The <see cref="string"/> .
        /// </returns>
        string GetName(CultureInfo loc);

        #endregion
    }
}
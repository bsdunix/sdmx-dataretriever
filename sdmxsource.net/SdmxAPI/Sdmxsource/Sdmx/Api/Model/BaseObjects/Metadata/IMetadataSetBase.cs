// -----------------------------------------------------------------------
// <copyright file="IMetadataSetBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Metadata
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;

    #endregion

    /// <summary>
    ///     The MetadataSetBase interface.
    /// </summary>
    public interface IMetadataSetBase : IObjectBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets the IMetadataSet that was used to build this Base Object.
        ///     Override from parent
        /// </summary>
        new IMetadataSet BuiltFrom { get; }

        /// <summary>
        ///     Gets DataProvider
        /// </summary>
        IDataProvider DataProvider { get; }

        /// <summary>
        ///     Gets  the metadata structure that defines the structure of this metadata set.
        ///     Mandatory
        /// </summary>
        IMetadataStructureDefinitionObject MetadataStructure { get; }

        /// <summary>
        ///     Gets the publication period
        /// </summary>
        object PublicationPeriod { get; }

        /// <summary>
        ///     Gets the four digit ISo 8601 year of publication
        /// </summary>
        ISdmxDate PublicationYear { get; }

        /// <summary>
        ///     Gets  the inclusive start time of the data reported in the metadata set
        /// </summary>
        ISdmxDate ReportingBeginDate { get; }

        /// <summary>
        ///     Gets the inclusive end time of the data reported in the metadata set
        /// </summary>
        ISdmxDate ReportingEndDate { get; }

        /// <summary>
        ///     Gets the reports.
        /// </summary>
        IList<IMetadataReportBase> Reports { get; }

        /// <summary>
        ///     Gets the inclusive start time of the validity of the info in the metadata set
        /// </summary>
        ISdmxDate ValidFromDate { get; }

        /// <summary>
        ///     Gets the inclusive end time of the validity of the info in the metadata set
        /// </summary>
        ISdmxDate ValidToDate { get; }

        #endregion
    }
}
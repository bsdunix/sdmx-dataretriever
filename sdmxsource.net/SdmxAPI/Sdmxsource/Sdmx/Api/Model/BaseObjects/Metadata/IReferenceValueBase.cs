// -----------------------------------------------------------------------
// <copyright file="IReferenceValueBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Metadata
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    ///     ReferenceValue contains a value for a target reference object reference.
    ///     <p />
    ///     When this is taken with its sibling elements, they identify the object or objects to which the reported metadata apply.
    ///     The content of this will either be a reference to an identifiable object, a data key, a reference to a data set, or a report period.
    /// </summary>
    public interface IReferenceValueBase : IObjectBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets the IReferenceValue that was used to build this Base Object - Override from parent
        /// </summary>
        new IReferenceValue BuiltFrom { get; }

        /// <summary>
        ///     Gets the content constraint that this structure references, returns null if there is no reference
        /// </summary>
        /// <value> </value>
        IContentConstraintObject ContentConstraintReference { get; }

        /// <summary>
        ///     Gets a value indicating whether the reference is a content constraint reference, if true getContentConstraintReference() will return a not null value
        /// </summary>
        /// <value> </value>
        bool ContentConstriantReference { get; }

        /// <summary>
        ///     Gets a list of data keys, will return an empty list if isDatasetReference() is false
        /// </summary>
        /// <value> </value>
        IList<IDataKey> DataKeys { get; }

        /// <summary>
        ///     Gets a value indicating whether the the reference value is a datakey reference, if true getDataKeys() will return 1 or more items
        /// </summary>
        /// <value> </value>
        bool DatakeyReference { get; }

        /// <summary>
        ///     Gets the dataset id.
        /// </summary>
        string DatasetId { get; }

        /// <summary>
        ///     Gets a value indicating whether the is a dataset reference, if true GetIdentifiableReference() AND getDatasetId() will NOT be null
        /// </summary>
        /// <value> </value>
        bool DatasetReference { get; }

        /// <summary>
        ///     Gets the id of this reference value
        /// </summary>
        /// <value> </value>
        string Id { get; }

        /// <summary>
        ///     Gets identifiable reference.
        /// </summary>
        IIdentifiableObject IdentifiableReference { get; }

        /// <summary>
        ///    Gets a value indicating whether this is an identifiable structure reference, if true GetIdentifiableReference() will NOT be null
        /// </summary>
        /// <value> </value>
        bool IsIdentifiableReference { get; }

        #endregion
    }
}
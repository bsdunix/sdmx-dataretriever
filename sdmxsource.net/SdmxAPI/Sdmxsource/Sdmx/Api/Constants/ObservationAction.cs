﻿// -----------------------------------------------------------------------
// <copyright file="ObservationAction.cs" company="EUROSTAT">
//   Date Created : 2013-05-17
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    #endregion


    /// <summary>
    ///     For a 2.1 REST data query, this enum contains a list of the parameters.
    ///     <p />
    ///     Observable action; possible options are:
    ///     <ul>
    ///      <li>active - Active observations, regardless of when they were added or updated will be returned</li>
    ///      <li>added - Newly added observations will be returned</li>
    ///      <li>updated - Only updated observations will be returned</li>
    ///      <li>deleted - Only deleted observations will be returned</li>
    ///     </ul>
    /// </summary>
    public enum ObservationActionEnumType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0,

        /// <summary>
        ///     Active observations, regardless of when they were added or updated will be returned
        /// </summary>
        Active,

        /// <summary>
        ///     Newly added observations will be returned
        /// </summary>
        Added,

        /// <summary>
        ///     Only updated observations will be returned
        /// </summary>
        Updated,

        /// <summary>
        ///     Only deleted observations will be returned
        /// </summary>
        Deleted
    }

    /// <summary>
    /// The observable action
    /// </summary>
    public class ObservationAction : BaseConstantType<ObservationActionEnumType>
    {

        #region Static Fields

        /// <summary>
        ///     The _instances.
        /// </summary>
        private static readonly Dictionary<ObservationActionEnumType, ObservationAction> Instances =
            new Dictionary<ObservationActionEnumType, ObservationAction>
                {
                    {
                        ObservationActionEnumType.Active, 
                        new ObservationAction(
                        ObservationActionEnumType.Active, 
                        "active")
                    }, 
                    {
                        ObservationActionEnumType.Added, 
                        new ObservationAction(
                        ObservationActionEnumType.Added, 
                        "added")
                    }, 
                    {
                        ObservationActionEnumType.Updated, 
                        new ObservationAction(
                        ObservationActionEnumType.Updated, 
                        "updated")
                    }, 
                    {
                        ObservationActionEnumType.Deleted, 
                        new ObservationAction(
                        ObservationActionEnumType.Deleted, 
                        "deleted")
                    }
                };

        #endregion

        #region Fields

        /// <summary>
        ///     The _obsAction.
        /// </summary>
        private readonly string _obsAction;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ObservationAction"/> class.
        /// </summary>
        /// <param name="enumType">
        /// The enum type
        /// </param>
        /// <param name="obsAction">
        /// The obsAction
        /// </param>
        private ObservationAction(ObservationActionEnumType enumType, string obsAction)
            : base(enumType)
        {
            this._obsAction = obsAction;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the instances of <see cref="ObservationAction" />
        /// </summary>
        public static IEnumerable<ObservationAction> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        ///     Gets the obsAction.
        /// </summary>
        public string ObsAction
        {
            get
            {
                return this._obsAction;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the instance of <see cref="ObservationAction"/> mapped to <paramref name="enumType"/>
        /// </summary>
        /// <param name="enumType">
        /// The <c>enum</c> type
        /// </param>
        /// <returns>
        /// the instance of <see cref="ObservationAction"/> mapped to <paramref name="enumType"/>
        /// </returns>
        public static ObservationAction GetFromEnum(ObservationActionEnumType enumType)
        {
            ObservationAction output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        /// Parses the string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The <see cref="ObservationAction" /> .
        /// </returns>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException">Unknown Parameter  + value +  allowed parameters:  + sb.ToString()</exception>
        /// <exception cref="SdmxSemmanticException">Throws Validation Exception</exception>
        public static ObservationAction ParseString(string value)
        {
            foreach (ObservationAction currentQueryDetail in Values)
            {
                if (currentQueryDetail.ObsAction.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return currentQueryDetail;
                }
            }

            var sb = new StringBuilder();
            string concat = string.Empty;

            foreach (ObservationAction currentQueryDetail in Values)
            {
                sb.Append(concat + currentQueryDetail.ObsAction);
                concat = ", ";
            }

            throw new SdmxSemmanticException("Unknown Parameter " + value + " allowed parameters: " + sb.ToString());

        }

        #endregion

    }
}

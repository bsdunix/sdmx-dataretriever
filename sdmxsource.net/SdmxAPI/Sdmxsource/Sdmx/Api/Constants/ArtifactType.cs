// -----------------------------------------------------------------------
// <copyright file="ArtifactType.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    /// <summary>
    ///     Contains a high level set of SDMX Artifact types
    /// </summary>
    public enum ArtifactType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     Relates to SDMX Data Messages
        /// </summary>
        Data, 

        /// <summary>
        ///     Relates to SDMX Data Metadata Set Messages
        /// </summary>
        Metadata, 

        /// <summary>
        ///     Relates to SDMX Provision Messages (only relevant in SDMX v2.0)
        /// </summary>
        Provision, 

        /// <summary>
        ///     Relates to SDMX Registration Messages (Could be Submission or Query Response)
        /// </summary>
        Registration, 

        /// <summary>
        ///     Relates to SDMX Structure Messages including Registry Interface submissions and query structure responses
        /// </summary>
        Structure, 

        /// <summary>
        ///     Relates to SDMX Notification Messages
        /// </summary>
        Notification, 

        /// <summary>
        ///     Relates to SDMX Subscription Messages - either submission or query response
        /// </summary>
        Subscription
    }
}
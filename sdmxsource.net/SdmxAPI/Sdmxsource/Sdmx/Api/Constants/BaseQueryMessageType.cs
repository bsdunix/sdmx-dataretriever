// -----------------------------------------------------------------------
// <copyright file="BaseQueryMessageType.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     Contains the base query types, without concern for the detail of the query.
    ///     <p />
    ///     The types are structure query, data query or metadata query
    /// </summary>
    public enum BaseQueryMessageEnumType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     Query for Structures
        /// </summary>
        Structure, 

        /// <summary>
        ///     Query for Data
        /// </summary>
        Data, 

        /// <summary>
        ///     Query for Metadata
        /// </summary>
        Metadata, 

        /// <summary>
        ///     Query for Schemas
        /// </summary>
        Schema
    }

    /// <summary>
    ///     Contains the base query types, without concern for the detail of the query.
    ///     The types are structure query, data query or metadata query
    /// </summary>
    public class BaseQueryMessageType : BaseConstantType<BaseQueryMessageEnumType>
    {
        #region Static Fields

        /// <summary>
        ///     The _instances.
        /// </summary>
        private static readonly Dictionary<BaseQueryMessageEnumType, BaseQueryMessageType> Instances =
            new Dictionary<BaseQueryMessageEnumType, BaseQueryMessageType>
                {
                    {
                        BaseQueryMessageEnumType.Structure, 
                        new BaseQueryMessageType(
                        BaseQueryMessageEnumType.Structure, 
                        "Structure")
                    }, 
                    {
                        BaseQueryMessageEnumType.Data, 
                        new BaseQueryMessageType(
                        BaseQueryMessageEnumType.Data, "Data")
                    }, 
                    {
                        BaseQueryMessageEnumType.Metadata, 
                        new BaseQueryMessageType(
                        BaseQueryMessageEnumType.Metadata, 
                        "Metadata")
                    }, 
                    {
                        BaseQueryMessageEnumType.Schema, 
                        new BaseQueryMessageType(
                        BaseQueryMessageEnumType.Schema, 
                        "Schema")
                    }
                };

        #endregion

        #region Fields

        /// <summary>
        ///     The _type.
        /// </summary>
        private readonly string _queryType;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseQueryMessageType"/> class.
        /// </summary>
        /// <param name="enumType">
        /// The enum type.
        /// </param>
        /// <param name="queryType">
        /// The type.
        /// </param>
        private BaseQueryMessageType(BaseQueryMessageEnumType enumType, string queryType)
            : base(enumType)
        {
            this._queryType = queryType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the instances of <see cref="BaseQueryMessageType" />
        /// </summary>
        public static IEnumerable<BaseQueryMessageType> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        ///     Gets the type.
        /// </summary>
        /// <remarks>This is the corresponds to <c>getType()</c> in Java. Renamed to <c>QueryType</c> to conform to <c>FxCop</c> rule CA1721</remarks>
        public string QueryType
        {
            get
            {
                return this._queryType;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the instance of <see cref="BaseQueryMessageType"/> mapped to <paramref name="enumType"/>
        /// </summary>
        /// <param name="enumType">
        /// The <c>enum</c> type
        /// </param>
        /// <returns>
        /// the instance of <see cref="BaseQueryMessageType"/> mapped to <paramref name="enumType"/>
        /// </returns>
        public static BaseQueryMessageType GetFromEnum(BaseQueryMessageEnumType enumType)
        {
            BaseQueryMessageType output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        #endregion
    }
}
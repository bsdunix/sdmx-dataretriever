// -----------------------------------------------------------------------
// <copyright file="DatasetPosition.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    /// <summary>
    ///     Contains a list of possible position in a dataset
    /// </summary>
    public enum DatasetPosition
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     Position is at the dataset level
        /// </summary>
        Dataset, 

        /// <summary>
        ///     Position is at the dataset attribute level
        /// </summary>
        DatasetAttribute, 

        /// <summary>
        ///     Position is at the series level
        /// </summary>
        Series, 

        /// <summary>
        ///     Position is at the series key level
        /// </summary>
        SeriesKey, 

        /// <summary>
        ///     Position is at the series key attribute level
        /// </summary>
        SeriesKeyAttribute, 

        /// <summary>
        ///     Position is at the group level
        /// </summary>
        Group, 

        /// <summary>
        ///     Position is at the group key level
        /// </summary>
        GroupKey, 

        /// <summary>
        ///     Position is at the group key attribute level
        /// </summary>
        GroupKeyAttribute, 

        // GROUP_SERIES_KEY,
        // GROUP_SERIES_KEY_ATTRIBUTE,

        /// <summary>
        ///     Position is at the observation level
        /// </summary>
        Observation, 

        /// <summary>
        ///     Position is at the observation attribute level
        /// </summary>
        ObservationAttribute, 

        /// <summary>
        ///     Position is at the series level, when the series also contains the observation information (SDMX 2.1 only)
        /// </summary>
        ObservationAsSeries
    }
}
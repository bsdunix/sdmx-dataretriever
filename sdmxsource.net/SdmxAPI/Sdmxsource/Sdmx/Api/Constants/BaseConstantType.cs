﻿// -----------------------------------------------------------------------
// <copyright file="BaseConstantType.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    #region Using directives

    using System;

    #endregion

    /// <summary>
    /// Base class for Constant classes which glues them with the specified <typeparamref name="T"/>
    /// </summary>
    /// <typeparam name="T">
    /// The <c>enum</c> type
    /// </typeparam>
    public abstract class BaseConstantType<T>
        where T : struct, IConvertible
    {
        #region Fields

        /// <summary>
        ///     The <c>enum</c> value.
        /// </summary>
        private readonly T _enumType;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseConstantType{T}"/> class.
        /// </summary>
        /// <param name="enumType">
        /// The <c>enum</c> Type.
        /// </param>
        protected BaseConstantType(T enumType)
        {
            this._enumType = enumType;
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the <c>enum</c> value
        /// </summary>
        public T EnumType
        {
            get
            {
                return this._enumType;
            }
        }

        #endregion

        public T ToEnumType()
        {
            return this.EnumType;
        }

        /// <summary>
        /// Attributes the specified constant type.
        /// </summary>
        /// <param name="constantType">Type of the constant.</param>
        /// <returns></returns>
        public static implicit operator T(BaseConstantType<T> constantType)
        {
            return constantType == null ? default(T) : constantType.EnumType;
        }
    }
}
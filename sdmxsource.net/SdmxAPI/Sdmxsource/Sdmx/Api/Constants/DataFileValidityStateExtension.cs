﻿// -----------------------------------------------------------------------
// <copyright file="DataFileValidityStateExtension.cs" company="EUROSTAT">
//   Date Created : 2014-03-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    /// <summary>
    ///     The data file validity state extension.
    /// </summary>
    public static class DataFileValidityStateExtension
    {
        #region Public Methods and Operators

        /// <summary>
        /// Determines whether the specified data file validity state is set.
        /// </summary>
        /// <param name="dataFileValidityState">
        /// State of the data file validity.
        /// </param>
        /// <returns>
        /// True if it is UnSet
        /// </returns>
        public static bool IsSet(this DataFileValidityState dataFileValidityState)
        {
            return dataFileValidityState != DataFileValidityState.Unset;
        }

        /// <summary>
        /// Determines whether the specified data file validity state is unset.
        /// </summary>
        /// <param name="dataFileValidityState">
        /// State of the data file validity.
        /// </param>
        /// <returns>
        /// True if it is UnSet
        /// </returns>
        public static bool IsUnset(this DataFileValidityState dataFileValidityState)
        {
            return dataFileValidityState == DataFileValidityState.Unset;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="AttributeAttachmentLevel.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    /// <summary>
    ///     Defines all the attachment levels of attributes in a key family
    /// </summary>
    public enum AttributeAttachmentLevel
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     Attribute is relevant to the entire dataset
        /// </summary>
        DataSet, 

        /// <summary>
        ///     Attribute is relevant to a Group, in which case the group identifier is needed to define the group the attribute attaches to
        /// </summary>
        Group, 

        /// <summary>
        ///     Attribute is relevant to a Group of Dimensions (no formally specified in a Group)
        /// </summary>
        DimensionGroup, 

        /// <summary>
        ///     Attribute is relevant to a single Observation
        /// </summary>
        Observation
    }
}
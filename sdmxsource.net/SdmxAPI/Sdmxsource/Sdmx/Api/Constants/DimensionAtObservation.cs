﻿// -----------------------------------------------------------------------
// <copyright file="DimensionAtObservation.cs" company="EUROSTAT">
//   Date Created : 2013-02-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public enum DimensionAtObservationEnumType
    {
        /// <summary>
        /// All
        /// </summary>
        All,
        /// <summary>
        /// Time
        /// </summary>
        Time
    }

     /// <summary>
     /// 
     /// </summary>
     public sealed class DimensionAtObservation : BaseConstantType<DimensionAtObservationEnumType>
     {
         private readonly string _value;

         /// <summary>
         /// The instances.
         /// </summary>
         private static readonly IDictionary<DimensionAtObservationEnumType, DimensionAtObservation> Instances =
             new Dictionary<DimensionAtObservationEnumType, DimensionAtObservation>
                 {
                     {
                         DimensionAtObservationEnumType.All,
                         new DimensionAtObservation(DimensionAtObservationEnumType.All, "AllDimensions")
                     },
                     {
                         DimensionAtObservationEnumType.Time,
                         new DimensionAtObservation(
                         DimensionAtObservationEnumType.Time, DimensionObject.TimeDimensionFixedId)
                     }
                 };

         /// <summary>
         /// 
         /// </summary>
         /// <param name="enumType"></param>
         /// <param name="value"></param>
         public DimensionAtObservation(DimensionAtObservationEnumType enumType, string value)
             : base(enumType)
         {
             this._value = value;
         }


         /// <summary>
         ///     Gets all instances
         /// </summary>
         public static IEnumerable<DimensionAtObservation> Values
         {
             get
             {
                 return Instances.Values;
             }
         }

         /// <summary>
         /// The value
         /// </summary>
         public string Value
         {
             get
             {
                 return this._value;
             }
         }

         #region Public Methods and Operators

         /// <summary>
         /// Gets the instance of <see cref="DimensionAtObservation"/> mapped to <paramref name="enumType"/>
         /// </summary>
         /// <param name="enumType">
         /// The <c>enum</c> type
         /// </param>
         /// <returns>
         /// the instance of <see cref="DimensionAtObservation"/> mapped to <paramref name="enumType"/>
         /// </returns>
         public static DimensionAtObservation GetFromEnum(DimensionAtObservationEnumType enumType)
         {
             DimensionAtObservation output;
             if (Instances.TryGetValue(enumType, out output))
             {
                 return output;
             }

             return null;
         }

         #endregion

     }
}

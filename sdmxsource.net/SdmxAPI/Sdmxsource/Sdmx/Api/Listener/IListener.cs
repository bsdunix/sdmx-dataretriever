// -----------------------------------------------------------------------
// <copyright file="IListener.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Listener
{
    /// <summary>
    /// A class that can listen for actions and be invoked.
    /// </summary>
    /// <typeparam name="T">
    /// The type to listen
    /// </typeparam>
    public interface IListener<in T>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Listens for changes to object of type T and is notified on change and is passed in T.
        /// </summary>
        /// <param name="obj">
        /// The type of object to listen to.
        /// </param>
        void Invoke(T obj);

        #endregion
    }
}
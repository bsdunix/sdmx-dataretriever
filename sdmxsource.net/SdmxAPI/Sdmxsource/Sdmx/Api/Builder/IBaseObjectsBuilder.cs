// -----------------------------------------------------------------------
// <copyright file="IBaseObjectsBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Builder
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;

    #endregion

    /// <summary>
    ///     Builds a BaseObjects container from the contents of the SdmxObjects container.
    /// </summary>
    public interface IBaseObjectsBuilder : IBuilder<IObjectsBase, ISdmxObjects>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds base objects from the input objects, obtains any additional required objects from the retrieval manager
        /// </summary>
        /// <param name="buildFrom">
        /// The source SDMX Object
        /// </param>
        /// <param name="exitingObjects">
        /// if any base objects exist then they should be passed in here in order to reuse - new objects will be added to this container
        /// </param>
        /// <param name="objectRetrievalManager">The Retrieval Manager
        /// </param>
        /// <returns>
        /// The <see cref="IObjectsBase"/> .
        /// </returns>
        /// <exception cref="SdmxException">
        /// Could not build object
        /// </exception>
        IObjectsBase Build(ISdmxObjects buildFrom, IObjectsBase exitingObjects, ISdmxObjectRetrievalManager objectRetrievalManager);

        #endregion
    }
}
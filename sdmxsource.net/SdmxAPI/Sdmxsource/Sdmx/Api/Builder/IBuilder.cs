// -----------------------------------------------------------------------
// <copyright file="IBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Builder
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Exception;

    #endregion

    /// <summary>
    /// Any Classes implementing this interface are capable of building an object of one type <typeparamref name="TK"/>
    ///     from an object of another type <typeparamref name="TV"/>
    /// </summary>
    /// <typeparam name="TK">
    /// - The type of object that gets built by the builder
    /// </typeparam>
    /// <typeparam name="TV">
    /// - The type of object that the builder requires to build the object from
    /// </typeparam>
    public interface IBuilder<out TK, in TV>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds an object of type <typeparamref name="TK"/>
        /// </summary>
        /// <param name="buildFrom">
        /// An Object to build the output object from
        /// </param>
        /// <returns>
        /// Object of type <typeparamref name="TK"/>
        /// </returns>
        /// <exception cref="SdmxException">
        /// - If anything goes wrong during the build process
        /// </exception>
        TK Build(TV buildFrom);

        #endregion
    }
}
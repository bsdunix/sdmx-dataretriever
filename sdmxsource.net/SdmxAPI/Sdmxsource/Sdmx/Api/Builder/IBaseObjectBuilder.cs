// -----------------------------------------------------------------------
// <copyright file="IBaseObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Builder
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    /// Base interface for specific base object builder classes, to be built from corresponding plain objects.
    /// </summary>
    /// <typeparam name="TOutput">
    /// structure base object (to)
    /// </typeparam>
    /// <typeparam name="TInput">
    /// corresponding object (from)
    /// </typeparam>
    public interface IBaseObjectBuilder<out TOutput, in TInput>
        where TOutput : IObjectBase where TInput : ISdmxObject
    {
        #region Public Methods and Operators

        /// <summary>
        /// Build a BaseObject from a SdmxObject and a corresponding SdmxObjectRetrievalManager (optional) to resolve any cross referenced artifacts
        /// </summary>
        /// <param name="buildFrom">
        /// - the SdmxObject to build from
        /// </param>
        /// <param name="objectRetrievalManager">
        /// - to resolve any cross referenced artifacts declared by V - the buildFrom argument
        /// </param>
        /// <param name="exitingObjects">
        /// - can be null if none already exit
        /// </param>
        /// <returns>
        /// K - the Sdmx base object representation of the SdmxObject argument
        /// </returns>
        TOutput Build(TInput buildFrom, ISdmxObjectRetrievalManager objectRetrievalManager, IObjectsBase exitingObjects);

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="IReadableDataLocation.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Util
{
    #region Using directives

    using System;
    using System.IO;

    #endregion

    /// <summary>
    ///     ReadableDataLocation is capable of reading and re-reading the same source of data many times
    /// </summary>
    /// <example>
    ///     A sample implementation in C# of <see cref="IReadableDataLocation" />
    ///     <code source="..\ReUsingExamples\DataQuery\ReUsingDataQueryParsingManager.cs" lang="cs" />
    /// </example>
    public interface IReadableDataLocation : IDisposable
    {
        #region Public Properties

        /// <summary>
        ///     Gets a new stream. This method is guaranteed to return a new input stream on each method call.
        ///     The input stream will be reading the same underlying data source.
        /// </summary>
        Stream InputStream { get; }

        /// <summary>
        /// Gets the name. If this ReadableDataLocation originated from a file, then this will be the original file name, regardless of where the stream is held.
        /// This may return null if it is not relevant.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Closes (and removes if appropriate) any resources that are held open
        /// </summary>
        void Close();

        #endregion
    }
}
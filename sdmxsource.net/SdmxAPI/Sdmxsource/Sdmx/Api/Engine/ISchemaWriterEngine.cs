﻿// -----------------------------------------------------------------------
// <copyright file="ISchemaWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Engine
{

    #region Using directives

    using System.Collections.Generic;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.DataStructure;

    #endregion

    /// <summary>
    /// Writes the schema in the format which is defined by this engine
    /// </summary>
    public interface ISchemaWriterEngine
    {
        #region Public Methods and Operators

        /// <summary>
        /// Generates a time-series schema in the format specified by the data type for the key family
        /// which is written to the specified OutputStream.
        /// </summary>
        /// <param name="outPutStream">
        /// The OutputStream to write the schema to  this is closed on completion
        /// </param>
        /// <param name="dsd">
        /// The DSD to generate the schema for
        /// </param>
        /// <param name="targetNamespace">
        /// The target namespace to use
        /// </param>
        /// <param name="constraintsMap">
        /// The map key is the dimension id, values is the valid codes for the dimension
        /// </param>
        void GenerateSchema(Stream outPutStream, IDataStructureObjectBase dsd, string targetNamespace, IDictionary<string, ISet<string>> constraintsMap);

        /// <summary>
        /// Generates a cross-sectional schema in the format specified by the data type for the key family
        /// which is written to the specified OutputStream.
        /// </summary>
        /// <param name="outPutStream">
        /// The OutputStream to write the schema to  this is closed on completion
        /// </param>
        /// <param name="dsd">
        /// The DSD to generate the schema for
        /// </param>
        /// <param name="targetNamespace">
        /// The target namespace to use
        /// </param>
        /// <param name="crossSectionalDimensionId">
        /// The id of the dimension that will be used at the observation level
        /// </param>
        /// <param name="constraintsMap">
        /// The map key is the dimension id, values is the valid codes for the dimension
        /// </param>
        void GenerateCrossSectionalSchema(Stream outPutStream, IDataStructureObjectBase dsd, string targetNamespace, string crossSectionalDimensionId, IDictionary<string, ISet<string>> constraintsMap);

        #endregion

    }
}

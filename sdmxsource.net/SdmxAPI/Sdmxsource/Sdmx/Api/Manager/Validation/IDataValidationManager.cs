﻿// -----------------------------------------------------------------------
// <copyright file="IDataValidationManager.cs" company="EUROSTAT">
//   Date Created : 2013-05-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Validation
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    #endregion

    /// <summary>
    /// The DataValidationManager is responsible for validating data read from a DataReaderEngine
    /// </summary>
    public interface IDataValidationManager
    {
        #region Public Methods and Operators
        	
        /// <summary>
        /// Validates the data read from the DataReaderEngine 
        /// </summary>
        /// <param name="dre">
        /// The reader engine used to read the dataset.  The DataValidationManager will reset the DataReaderEngine 
        /// </param>
        /// <param name="exceptionHandler">
        /// The exception handler
        /// </param>
        /// <exception cref="SdmxSyntaxException">
        /// if the data is not syntactically valid
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        /// if the data is syntactically valid, however it contains invalid content
        /// </exception>
        void ValidateData(IDataReaderEngine dre, IExceptionHandler exceptionHandler);

        #endregion
    }
}

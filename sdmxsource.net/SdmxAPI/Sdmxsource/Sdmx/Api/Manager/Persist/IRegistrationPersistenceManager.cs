// -----------------------------------------------------------------------
// <copyright file="IRegistrationPersistenceManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Persist
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    ///     Manages the persistence of registrations
    /// </summary>
    public interface IRegistrationPersistenceManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Deletes a registration with the same urn of the passed in Registrations - if any registrations could not be found then an error will NOT be issued, the
        ///     registration will be ignored
        /// </summary>
        /// <param name="registration">The registration. </param>
        void DeleteRegistration(IRegistrationObject registration);

        /// <summary>
        /// Persists the Registration, treated as DatasetAction.REPLACE
        ///     <p/>
        ///     REPLACE action will replace any registration that matches the URN of the passed in Registration
        /// </summary>
        /// <param name="registration">The registration. </param>
        void ReplaceRegistration(IRegistrationObject registration);

        /// <summary>
        /// Persists the Registration, treated as DatasetAction.APPEND
        ///     <p/>
        ///     Will not overwrite a Registration if one already exists with the same URN
        /// </summary>
        /// <param name="registration">The registration.  </param>
        void SaveRegistration(IRegistrationObject registration);

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="IStructurePersistenceManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Persist
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Manages the persistence of structures
    /// </summary>
    public interface IStructurePersistenceManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Saves the maintainable 
        /// </summary>
        /// <param name="maintainable"></param>
        void SaveStructure(IMaintainableObject maintainable);

        /// <summary>
        /// Saves the maintainable structures in the supplied sdmxObjects
        /// </summary>
        /// <param name="sdmxObjects"> SDMX objects
        /// </param>
        void SaveStructures(ISdmxObjects sdmxObjects);

        /// <summary>
        /// Deletes the maintainable structures in the supplied sdmxObjects
        /// </summary>
        /// <param name="sdmxObjects">SDMX objects
        /// </param>
        void DeleteStructures(ISdmxObjects sdmxObjects);

        /// <summary>
        /// Deletes the maintainable structures in the supplied objects
        /// </summary>
        /// <param name="maintainable"></param>
        void DeleteStructure(IMaintainableObject maintainable);


        #endregion
    }
}
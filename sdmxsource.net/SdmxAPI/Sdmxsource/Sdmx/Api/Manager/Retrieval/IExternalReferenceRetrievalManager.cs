﻿// -----------------------------------------------------------------------
// <copyright file="IExternalReferenceRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2014-03-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval
{
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// This interface is responsible for providing the capabilities of resolving externally referenced structures
    /// </summary>
    public interface IExternalReferenceRetrievalManager
    {
        /// <summary>
        /// Resolves the full structure from the specified stub structure.
        /// </summary>
        /// <param name="externalStructure">The external structure.</param>
        /// <returns>The full structure from the stub structure</returns>
        /// <remarks>If the external structure has IsExternalReference.IsTrue == false, then it is not externally maintained and no 
        /// action will be taken, the same structure will be passed back.  Otherwise the external structure will be resolved using
        /// the StructureURL or ServiceURL obtained from the structure</remarks>
        /// <exception cref="SdmxNoResultsException">if the maintainable could not be resolved from the given endpoint</exception>
        IMaintainableObject ResolveFullStructure(IMaintainableObject externalStructure);
    }
}
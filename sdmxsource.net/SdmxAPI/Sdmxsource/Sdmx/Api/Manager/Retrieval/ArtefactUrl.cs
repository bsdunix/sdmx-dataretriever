// -----------------------------------------------------------------------
// <copyright file="ArtefactUrl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval
{
    #region Using directives

    using System;

    #endregion

    /// <summary>
    ///     The artefact Uri contains the url to resolve the artefact, and a boolean defining if it is a service Uri or a structure Uri
    /// </summary>
    public class ArtefactUrl
    {
        #region Fields

        /// <summary>
        ///     The _service url.
        /// </summary>
        private readonly bool _serviceUrl;

        /// <summary>
        ///     The _url.
        /// </summary>
        private readonly Uri _url;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ArtefactUrl"/> class.
        /// </summary>
        /// <param name="url">
        /// The url.
        /// </param>
        /// <param name="serviceUrl">
        /// The service url.
        /// </param>
        public ArtefactUrl(Uri url, bool serviceUrl)
        {
            this._url = url;
            this._serviceUrl = serviceUrl;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets a value indicating whether service url.
        /// </summary>
        public bool ServiceUrl
        {
            get
            {
                return this._serviceUrl;
            }
        }

        /// <summary>
        ///     Gets the url.
        /// </summary>
        public Uri Url
        {
            get
            {
                return this._url;
            }
        }

        #endregion
    }
}
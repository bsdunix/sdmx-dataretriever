﻿// -----------------------------------------------------------------------
// <copyright file="ICrossReferencingRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.CrossReference
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    /// TODO
    /// </summary>
    public interface ICrossReferencingRetrievalManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Returns a set of MaintainableObject that cross reference the structure(s) that match the reference parameter
        /// </summary>
        /// <param name="structureReference">
        /// The structureReference Who References Me?
        /// </param>
        /// <param name="returnStub">
        /// ReturnStub if true, then will return the stubs that reference the structure
        /// </param>
        /// <param name="structures">
        /// Structures an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        /// The MaintainableObjects
        /// </returns>
        ISet<IMaintainableObject> GetCrossReferencingStructures(IStructureReference structureReference, bool returnStub, params SdmxStructureType[] structures);

        /// <summary>
        /// Returns a set of MaintainableObject that cross reference the given identifiable structure
        /// </summary>
        /// <param name="identifiable">
        /// The identifiable bean to retrieve the references for - Who References Me?
        /// </param>
        /// <param name="returnStub">
        /// ReturnStub if true, then will return the stubs that reference the structure
        /// </param>
        /// <param name="structures">
        /// Structures an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        /// The MaintainableObjects
        /// </returns>
        ISet<IMaintainableObject> GetCrossReferencingStructures(IIdentifiableObject identifiable, bool returnStub, params SdmxStructureType [] structures);

        #endregion
    }
}

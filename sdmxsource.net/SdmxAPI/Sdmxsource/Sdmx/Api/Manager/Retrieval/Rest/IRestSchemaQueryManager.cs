﻿// -----------------------------------------------------------------------
// <copyright file="IRestSchemaQueryManager.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Rest
{
    #region Using directives

    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    #endregion

    /// <summary>
    /// Queries for and writes the schema in the given format to the output stream
    /// </summary>
    public interface IRestSchemaQueryManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="schemaQuery">
        /// The schema query
        /// </param>
        /// <param name="format">
        /// the format
        /// </param>
        /// <param name="outPutStream">
        /// The output stream
        /// </param>
        void WriteSchema(IRestSchemaQuery schemaQuery, ISchemaFormat format, Stream outPutStream);

        #endregion
    }
}

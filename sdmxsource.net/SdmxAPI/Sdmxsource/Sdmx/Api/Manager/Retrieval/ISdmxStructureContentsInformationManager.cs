﻿// -----------------------------------------------------------------------
// <copyright file="ISdmxStructureContentsInformationManager.cs" company="EUROSTAT">
//   Date Created : 2013-02-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;

    /// <summary>
    /// Provides meta information about the contents of a structure source
    /// </summary>
    public interface ISdmxStructureContentsInformationManager
    {
        /// <summary>
        /// Returns a count of maintainable structures for the given structure type and agency.
        /// </summary>
        /// <param name="agencyId">The agency to count the structures for</param>
        /// <param name="structureType">The maintainable structure type</param>
        /// <returns>A count of the number of maintainables for the given structure type and agency.</returns>
        int GetCountOfMaintainables(String agencyId, SdmxStructureType structureType);

        /// <summary>
        /// Gets a list of all available languages available at this structure source
        /// </summary>
        IList<string> AllLanguages { get; }
        
        /// <summary>
        /// Gets a List of AgencyMetadata objects containing information about the Agencies in the system
        /// </summary>
        IList<IAgencyMetadata> AllAgencies { get; }
    }
}

// -----------------------------------------------------------------------
// <copyright file="IProvisionRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    ///     Manages the retrieval of provision agreements using simple reference of structures that directly reference a provision
    /// </summary>
    public interface IProvisionRetrievalManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets the provision agreement that the registration is referencing
        /// </summary>
        /// <param name="registration">The Registration Object
        /// </param>
        /// <returns>
        /// The <see cref="IProvisionAgreementObject"/> .
        /// </returns>
        IProvisionAgreementObject GetProvision(IRegistrationObject registration);

        /// <summary>
        /// Gets a list of provisions that match the structure reference.
        ///     <p/>
        ///     The structure reference can either be referencing a Provision structure, a Data or MetadataFlow, or a DataProvider.
        /// </summary>
        /// <param name="provisionRef">
        /// The provision Ref.
        /// </param>
        /// <returns>
        /// The <see cref="ISet{IProvisionAgreementObject}"/> .
        /// </returns>
        ISet<IProvisionAgreementObject> GetProvisions(IStructureReference provisionRef);

        /// <summary>
        /// Gets all the provision Agreements that are referencing the given dataflow
        /// </summary>
        /// <param name="dataflow">Dataflow Object
        /// </param>
        /// <returns>
        /// The <see cref="ISet{IProvisionAgreementObject}"/> .
        /// </returns>
        ISet<IProvisionAgreementObject> GetProvisions(IDataflowObject dataflow);

        /// <summary>
        /// Gets all the provision Agreements that are referencing the given metadataflow
        /// </summary>
        /// <param name="metadataflow">
        /// The metadataflow.
        /// </param>
        /// <returns>
        /// The <see cref="ISet{IProvisionAgreementObject}"/> .
        /// </returns>
        ISet<IProvisionAgreementObject> GetProvisions(IMetadataFlow metadataflow);

        #endregion
    }
}
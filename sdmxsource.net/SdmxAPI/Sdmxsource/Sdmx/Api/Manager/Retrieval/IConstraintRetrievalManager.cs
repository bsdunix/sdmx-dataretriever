// -----------------------------------------------------------------------
// <copyright file="IConstraintRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    ///     The constraint retrieval manager is used to retrieve constraints for the purpose of building a data query.
    /// </summary>
    public interface IConstraintRetrievalManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets a IContentConstraintObject defining the data that is allowed for the IConstrainableObject.
        ///     Will merge constraints attached to the child constrainableObject structures of the input constrainableObject.
        /// </summary>
        /// <param name="constrainableObject">Constrainable Object
        /// </param>
        /// <returns>
        /// The <see cref="IContentConstraintObject"/> .
        /// </returns>
        IContentConstraintObject GetConstraintDefiningAllowedData(IConstrainableObject constrainableObject);

        /// <summary>
        /// Gets a IContentConstraintObject defining the data present for the IConstrainableObject.
        ///     Gets null if no constraint exists.
        /// </summary>
        /// <param name="constrainableObject">Constrainable Object
        /// </param>
        /// <returns>
        /// The <see cref="IContentConstraintObject"/> .
        /// </returns>
        IContentConstraintObject GetContentConstraintDefiningDataPresent(IConstrainableObject constrainableObject);

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="ISdmxSubscriptionRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    ///     Manages the retrieval of subscriptions
    /// </summary>
    public interface ISdmxSubscriptionRetrievalManager
    {

        /// <summary>
        /// Gets all the subscription count in the system..
        /// </summary>
        /// <value>
        /// The subscription count.
        /// </value>
        int SubscriptionCount { get; }

        #region Public Methods and Operators

        /// <summary>
        /// Gets a set of all the subscriptions which are owned by the a given organisation.
        ///     <p/>
        ///     Gets an empty set if no subscriptions are found
        /// </summary>
        /// <param name="organisation">
        /// The organisation.
        /// </param>
        /// <returns>
        /// The <see cref="ISet{ISubscriptionObject}"/> .
        /// </returns>
        ISet<ISubscriptionObject> GetSubscriptions(IOrganisation organisation);

        /// <summary>
        /// Gets a set of all the subscriptions for a given organisation (Data Consumer, Data Provider or Agency)
        /// </summary>
        /// <param name="organisationReference">
        /// this will be validated that the reference is to a data consumer, and it a full reference (contains all reference parameters)
        /// </param>
        /// <returns>
        /// The <see cref="ISet{ISubscriptionObject}"/> .
        /// </returns>
        /// <exception cref="CrossReferenceException">
        /// if the IStructureReference does not reference a valid Organisation
        /// </exception>
        ISet<ISubscriptionObject> GetSubscriptions(IStructureReference organisationReference);

        /// <summary>
        /// Gets a set of subscriptions of the given type for the trigger identifiable
        /// </summary>
        /// <param name="identifiableObject">The identifiable that triggers this event </param>
        /// <param name="subscriptionType">This defines the type of subscriptions to return </param>
        /// <returns></returns>
        ISet<ISubscriptionObject> GetSubscriptionsForEvent(IIdentifiableObject identifiableObject, SubscriptionType subscriptionType);

        #endregion
    }
}
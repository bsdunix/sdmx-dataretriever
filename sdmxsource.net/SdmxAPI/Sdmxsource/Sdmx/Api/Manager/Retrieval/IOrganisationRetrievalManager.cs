// -----------------------------------------------------------------------
// <copyright file="IOrganisationRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxAPI.
// 
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Used to retrieve Organisations
    /// </summary>
    public interface IOrganisationRetrievalManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets the agency with the given id
        /// </summary>
        /// <param name="agencyId">
        /// a period separated agency id, where the periods are used to define sub Agencies
        /// </param>
        /// <returns>
        /// DataProvider, or null if one can not be found with the given reference parameters
        /// </returns>
        IAgency GetAgency(string agencyId);

        /// <summary>
        /// Gets the data consumer belonging to the given agency, with the given id
        /// </summary>
        /// <param name="agencyId">
        /// a period separated agency id, where the periods are used to define sub Agencies
        /// </param>
        /// <param name="id">
        /// id of the data provider to return
        /// </param>
        /// <returns>
        /// DataProvider, or null if one can not be found with the given reference parameters
        /// </returns>
        IDataConsumer GetDataConsumerObject(string agencyId, string id);

        /// <summary>
        /// Gets the data provider belongning to the given agency, with the given id
        /// </summary>
        /// <param name="agencyId">
        /// a period separated agency id, where the periods are used to define sub Agencies
        /// </param>
        /// <param name="id">
        /// id of the data provider to return
        /// </param>
        /// <returns>
        /// DataProvider, or null if one can not be found with the given reference parameters
        /// </returns>
        IDataProvider GetDataProvider(string agencyId, string id);

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="CrossReferencedRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2015-05-15
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureRetrieval.
// 
//     SdmxStructureRetrieval is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureRetrieval is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrieval.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.StructureRetrieval.Manager.CrossReference
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.CrossReference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Util.Collections;

    /// <summary>
    /// The cross referenced retrieval manager.
    /// </summary>
    public class CrossReferencedRetrievalManager : ICrossReferencedRetrievalManager
    {
        /// <summary>
        /// The _SDMX object retrieval manager
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrievalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="CrossReferencedRetrievalManager"/> class.
        /// </summary>
        /// <param name="sdmxObjectRetrievalManager">The SDMX object retrieval manager.</param>
        public CrossReferencedRetrievalManager(ISdmxObjectRetrievalManager sdmxObjectRetrievalManager)
        {
            if (sdmxObjectRetrievalManager == null)
            {
                throw new ArgumentNullException("sdmxObjectRetrievalManager");
            }

            this._sdmxObjectRetrievalManager = sdmxObjectRetrievalManager;
        }

        /// <summary>
        /// Returns a set of MaintainableObjects that are cross referenced by the structure(s) that match the reference parameter
        /// </summary>
        /// <param name="structureReference">
        /// The structureReference  - What Do I Reference?
        /// </param>
        /// <param name="returnStub">
        /// returnStub if true, then will return the stubs that reference the structure
        /// </param>
        /// <param name="structures">
        /// structures an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        /// The MaintainableObjects
        /// </returns>
        public ISet<IMaintainableObject> GetCrossReferencedStructures(IStructureReference structureReference, bool returnStub, params SdmxStructureType[] structures)
        {
            return this.GetCrossReferencedStructures(this._sdmxObjectRetrievalManager.GetMaintainableObject(structureReference), returnStub, structures);
        }

        /// <summary>
        /// Returns a set of MaintainableObject that are cross referenced by the given identifiable structure
        /// </summary>
        /// <param name="identifiable">
        /// The identifiable object to retrieve the references for - What Do I Reference?
        /// </param>
        /// <param name="returnStub">
        /// ReturnStub if true, then will return the stubs that reference the structure
        /// </param>
        /// <param name="structures">
        /// Structures an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        /// The MaintainableObjects
        /// </returns>
        public ISet<IMaintainableObject> GetCrossReferencedStructures(IIdentifiableObject identifiable, bool returnStub, params SdmxStructureType[] structures)
        {
            ISet<IMaintainableObject> returnList = new HashSet<IMaintainableObject>();
            IDictionaryOfSets<SdmxStructureEnumType, IMaintainableRefObject> map = new DictionaryOfSets<SdmxStructureEnumType, IMaintainableRefObject>();
            foreach (var crossReference in identifiable.CrossReferences)
            {
                ISet<IMaintainableRefObject> references;
                if (!map.TryGetValue(crossReference.MaintainableStructureEnumType.EnumType, out references))
                {
                    references = new HashSet<IMaintainableRefObject>();
                    map.Add(crossReference.MaintainableStructureEnumType.EnumType, references);
                }

                if (references.Contains(crossReference.MaintainableReference))
                {
                    continue;
                }

                references.Add(crossReference.MaintainableReference);

                IMaintainableObject maint = this._sdmxObjectRetrievalManager.GetMaintainableObject(crossReference, returnStub, false);
                if (!returnList.Contains(maint))
                {
                    returnList.Add(maint);
                }
            }

            return returnList;
        }
    }
}
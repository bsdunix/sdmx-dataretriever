﻿// -----------------------------------------------------------------------
// <copyright file="HeaderRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2014-03-25
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureRetrieval.
// 
//     SdmxStructureRetrieval is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureRetrieval is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrieval.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.StructureRetrieval.Manager
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;

    public class HeaderRetrievalManager : IHeaderRetrievalManager
    {
        /// <summary>
        ///     The sender id.
        /// </summary>
        private string _senderId = "unknown";

        /// <summary>
        /// Gets and sets the sender id.
        /// </summary>
        public string SenderId
        {
            get { return _senderId; }
            set { _senderId = value; }
        }

        /// <summary>
        ///     Gets the header object
        /// </summary>
        public IHeader Header
        {
            get { return new HeaderImpl("IDREF" + Guid.NewGuid().ToString(), _senderId); }
        }

    }
}

// -----------------------------------------------------------------------
// <copyright file="Validator.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxParseBase.
// 
//     SdmxParseBase is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxParseBase is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxParseBase.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxParseBase.Helper
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;

    using Estat.Sri.SdmxParseBase.Properties;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     This is a utility class for checking a DSD for compliance
    ///     with producing Compact or Cross-Sectional Data.
    /// </summary>
    public static class Validator
    {
        #region Public Methods and Operators

        /// <summary>
        /// This method checks a DSD for compliance with producing Compact Data.
        ///     In detail, it checks that if a TimeDimension is present and at least
        ///     one dimension is frequency dimension. If there is none an error message
        ///     is returned to the caller
        /// </summary>
        /// <param name="keyFamily">
        /// The <see cref="IDataStructureObject"/>of the DSD to be checked
        /// </param>
        /// <returns>
        /// The error messages in case of invalid DSD or an empty string in case a valid DSD
        /// </returns>
        public static string ValidateForCompact(IDataStructureObject keyFamily)
        {
            string text = string.Empty;
            bool isFrequency = false;

            foreach (IDimension dimension in keyFamily.DimensionList.Dimensions)
            {
                if (dimension.FrequencyDimension)
                {
                    isFrequency = true;
                    break;
                }
            }

            if (keyFamily.TimeDimension == null)
            {
                text = string.Format(
                    CultureInfo.InvariantCulture, Resources.ErrorNoTimeDimensionFormat2, keyFamily.Id, keyFamily.Version);
            }
            else if (!isFrequency)
            {
                // normally it should never reach here
                text = "DSD " + keyFamily.Id + " v" + keyFamily.Version
                       + " does not have a Frequency dimension. According SDMX v2.0: Any DSD which uses the Time dimension must also declare a frequency dimension.";
            }

            return text;
        }

        /// <summary>
        /// This method checks a DSD for compliance with producing  Cross-Sectional Data.
        ///     In detail, it checks all Dimensions and all Attributes for having or not having
        ///     cross-sectional attachment group. If there are components with no attachment level,
        ///     it returns a list with them in the message.
        /// </summary>
        /// <param name="keyFamily">
        /// The <see cref="IDataStructureObject"/>of the DSD to be checked
        /// </param>
        /// <returns>
        /// The error messages in case of invalid DSD or an empty string in case a valid DSD
        /// </returns>
        public static string ValidateForCrossSectional(IDataStructureObject keyFamily)
        {
            var crossSectionalDataStructure = keyFamily as ICrossSectionalDataStructureObject;

            if (crossSectionalDataStructure == null)
            {
                return "Not a cross-sectional DSD";
            }

            return string.Empty;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="WriterHelper.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxParseBase.
// 
//     SdmxParseBase is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxParseBase is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxParseBase.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxParseBase.Helper
{
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    ///     This class contains static helper methods
    /// </summary>
    public static class WriterHelper
    {
        #region Public Methods and Operators

        /// <summary>
        /// Check if the specified <paramref name="errors"/> contains any errors. i.e. is not empty. If it is not empty an
        ///     <see cref="SdmxNotImplementedException"/>
        ///     is thrown
        /// </summary>
        /// <param name="errors">
        /// The string containing
        /// </param>
        /// <exception cref="SdmxNotImplementedException">
        /// the specified
        ///     <paramref name="errors"/>
        ///     is not empty and contains errors
        /// </exception>
        public static void ValidateErrors(string errors)
        {
            if (!string.IsNullOrEmpty(errors))
            {
                throw new SdmxNotImplementedException(errors);
            }
        }

        #endregion
    }
}
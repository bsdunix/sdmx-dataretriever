// -----------------------------------------------------------------------
// <copyright file="SdmxNamespaces.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxParseBase.
// 
//     SdmxParseBase is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxParseBase is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxParseBase.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxParseBase.Model
{
    /// <summary>
    ///     The set of namespaces used to generate the xml SDMX messages
    /// </summary>
    public class SdmxNamespaces
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the URL of the common namespace
        /// </summary>
        public NamespacePrefixPair Common { get; set; }

        /// <summary>
        ///     Gets or sets the URL of the compact namespace
        /// </summary>
        public NamespacePrefixPair Compact { get; set; }

        /// <summary>
        ///     Gets or sets the URL of the cross namespace
        /// </summary>
        public NamespacePrefixPair Cross { get; set; }

        /// <summary>
        ///     Gets or sets the URL of the generic namespace
        /// </summary>
        public NamespacePrefixPair Generic { get; set; }

        /// <summary>
        ///     Gets or sets the URL of the message namespace
        /// </summary>
        public NamespacePrefixPair Message { get; set; }

        /// <summary>
        ///     Gets or sets the URL of the query namespace
        /// </summary>
        public NamespacePrefixPair Query { get; set; }

        /// <summary>
        ///     Gets or sets the  URL of the registry message
        /// </summary>
        public NamespacePrefixPair Registry { get; set; }

        /// <summary>
        ///     Gets or sets the schema location
        /// </summary>
        public string SchemaLocation { get; set; }

        /// <summary>
        ///     Gets or sets the URL of the structure namespace
        /// </summary>
        public NamespacePrefixPair Structure { get; set; }

        /// <summary>
        ///     Gets or sets the URL of the structure specific dataset namespace. Note this depends on the <c>DSD</c> and applies to all SDMX versions.
        /// </summary>
        public NamespacePrefixPair DataSetStructureSpecific { get; set; }

        /// <summary>
        ///     Gets or sets the URL of the structure specific dataset namespace. Note this about <c>http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific</c>
        /// </summary>
        public NamespacePrefixPair StructureSpecific21 { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the ESTAT URN format should be used.
        /// </summary>
        public bool UseEstatUrn { get; set; }

        /// <summary>
        ///     Gets or sets the URL of the utility namespace
        /// </summary>
        public NamespacePrefixPair Utility { get; set; }

        /// <summary>
        ///     Gets or sets the URL of the XSI namespace
        /// </summary>
        public NamespacePrefixPair Xsi { get; set; }

        /// <summary>
        /// Gets or sets URI of the footer namespace.
        /// </summary>
        public NamespacePrefixPair Footer { get; set; }

        /// <summary>
        ///     Gets or sets the URI of the XML namespace
        /// </summary>
        public NamespacePrefixPair Xml { get; set; }

        #endregion
    }
}
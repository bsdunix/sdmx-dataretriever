// -----------------------------------------------------------------------
// <copyright file="NamespacePrefixPair.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxParseBase.
// 
//     SdmxParseBase is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxParseBase is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxParseBase.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxParseBase.Model
{
    using System;

    /// <summary>
    ///     This class holds a namespace and prefix pair
    /// </summary>
    public class NamespacePrefixPair
    {
        #region Static Fields

        /// <summary>
        ///     The empty
        /// </summary>
        private static readonly NamespacePrefixPair _empty = new NamespacePrefixPair();

        #endregion

        #region Fields

        /// <summary>
        ///     The namespace.
        /// </summary>
        private readonly string _namespace;

        /// <summary>
        ///     The prefix.
        /// </summary>
        private readonly string _prefix;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NamespacePrefixPair"/> class.
        /// </summary>
        /// <param name="ns">
        /// The namespace
        /// </param>
        /// <param name="prefix">
        /// The prefix.
        /// </param>
        public NamespacePrefixPair(Uri ns, string prefix)
        {
            this._namespace = ns != null ? ns.OriginalString : null;
            this._prefix = prefix;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NamespacePrefixPair"/> class.
        /// </summary>
        /// <param name="ns">
        /// The namespace
        /// </param>
        /// <param name="prefix">
        /// The prefix.
        /// </param>
        public NamespacePrefixPair(string ns, string prefix)
            : this(new Uri(ns), prefix)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NamespacePrefixPair"/> class.
        /// </summary>
        /// <param name="prefix">
        /// The prefix.
        /// </param>
        public NamespacePrefixPair(string prefix)
        {
            this._prefix = prefix;
        }

        /// <summary>
        ///     Prevents a default instance of the <see cref="NamespacePrefixPair" /> class from being created.
        /// </summary>
        private NamespacePrefixPair()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets an empty <see cref="NamespacePrefixPair" />
        /// </summary>
        public static NamespacePrefixPair Empty
        {
            get
            {
                return _empty;
            }
        }

        /// <summary>
        ///     Gets the namespace
        /// </summary>
        public string NS
        {
            get
            {
                return this._namespace;
            }
        }

        /// <summary>
        ///     Gets the prefix.
        /// </summary>
        public string Prefix
        {
            get
            {
                return this._prefix;
            }
        }

        #endregion
    }
}
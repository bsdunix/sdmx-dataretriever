﻿// -----------------------------------------------------------------------
// <copyright file="ConceptRepresentation.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure
{
    using System.Xml.Linq;

    /// <summary>
    /// <para>
    /// ConceptRepresentation defines the core representation that are allowed for a concept. The text format allowed for a concept is that which is allowed for any non-target object component.
    /// </para>
    /// </summary>
    public partial class ConceptRepresentation
    {

        /// <summary>
        /// <para>
        /// Enumeration references an item scheme that enumerates the allowable values for this representation.
        /// </para>
        /// <para>
        /// Occurrence: required
        /// </para>
        /// <para>
        /// Setter: Appends
        /// </para>
        /// <para>
        /// Regular expression: (TextFormat | (Enumeration, EnumerationFormat?))
        /// </para>
        /// </summary>
        public new Common.CodelistReferenceType Enumeration
        {
            get
            {
                XElement x = this.GetElement(XName.Get("Enumeration", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure"));
                return (Common.CodelistReferenceType)x;
            }
            set
            {
                this.SetElement(XName.Get("Enumeration", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure"), value);
            }
        }
        
    }
}
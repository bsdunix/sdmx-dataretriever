﻿// -----------------------------------------------------------------------
// <copyright file="ItemSchemeMapType.cs" company="EUROSTAT">
//   Date Created : 2014-10-15
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure
{
    using System.Xml.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;

    using Xml.Schema.Linq;

    /// <summary>
    /// <para>
    /// ItemSchemeMapType is an abstract base type which forms the basis for mapping items between item schemes of the same type.
    /// </para>
    /// <para>
    /// Regular expression: (Annotations?, Name+, Description*, Source, Target, (ItemAssociation)+)
    /// </para>
    /// </summary>
    public abstract partial class ItemSchemeMapType
    {
        /// <summary>
        /// The _source
        /// </summary>
        private static readonly XName _source = XName.Get("Source", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure");

        /// <summary>
        /// The _target
        /// </summary>
        private static readonly XName _target = XName.Get("Target", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure");

        /// <summary>
        /// Gets the typed source.
        /// </summary>
        /// <typeparam name="T">The <see cref="ItemSchemeReferenceBaseType"/> based type.</typeparam>
        /// <returns>he <see cref="ItemSchemeReferenceBaseType"/> based instance.</returns>
        public T GetTypedSource<T>() where T : ItemSchemeReferenceBaseType
        {
            XElement x = this.GetElement(_source);
            return XTypedServices.ToXTypedElement<T>(x, LinqToXsdTypeManager.Instance);
        }

        /// <summary>
        /// Gets the typed source.
        /// </summary>
        /// <typeparam name="T">The <see cref="ItemSchemeReferenceBaseType"/> based type.</typeparam>
        /// <returns>he <see cref="ItemSchemeReferenceBaseType"/> based instance.</returns>
        public T GetTypedTarget<T>() where T : ItemSchemeReferenceBaseType
        {
            XElement x = this.GetElement(_target);
            return XTypedServices.ToXTypedElement<T>(x, LinqToXsdTypeManager.Instance);
        }
    }
}
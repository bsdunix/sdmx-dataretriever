﻿// -----------------------------------------------------------------------
// <copyright file="StructureMapType.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;

    using Xml.Schema.Linq;

    /// <summary>
    /// <para>
    /// StructureMapType defines the structure for mapping components of one structure to components of another structure. A structure may be referenced directly meaning the map applies wherever the structure is used, or it may be a reference via a structure usage meaning the map only applies within the context of that usage. Using the related structures, one can make extrapolations between maps. For example, if key families, A, B, and C, are all grouped in a related structures container, then a map from key family A to C and a map from key family B to C could be used to infer a relation between key family A to C.
    /// </para>
    /// <para>
    /// Regular expression: (Annotations?, Name+, Description*, Source, Target, ComponentMap)
    /// </para>
    /// </summary>
    public partial class StructureMapType
    {
        private XTypedList<ComponentMapType> _componentMaps;

        public IList<ComponentMapType> ComponentMapTypes
        {
            get {
                if ((this._componentMaps == null)) {
                    this._componentMaps = new XTypedList<ComponentMapType>(this, LinqToXsdTypeManager.Instance, XName.Get("ComponentMap", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure"));
                }
                return this._componentMaps;
            }
            set {
                if ((value == null)) {
                    this._componentMaps = null;
                }
                else {
                    if ((this._componentMaps == null)) {
                        this._componentMaps = XTypedList<ComponentMapType>.Initialize(this, LinqToXsdTypeManager.Instance, value, XName.Get("ComponentMap", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure"));
                    }
                    else {
                        XTypedServices.SetList(this._componentMaps, value);
                    }
                }
            }
        }
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ComponentType.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure
{
    using System.Xml.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;

    using Xml.Schema.Linq;

    /// <summary>
    ///     The component type.
    /// </summary>
    public partial class ComponentType
    {
        #region Static Fields

        /// <summary>
        ///     The reference element name
        /// </summary>
        private static readonly XName _referenceName = XName.Get(
            "LocalRepresentation", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure");

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     <para>
        ///         Gets the LocalRepresentation references item schemes that may be used to create the representation of a component. The type of this must be refined such that a concrete item scheme reference is used.
        ///     </para>
        ///     <para>
        ///         Occurrence: optional
        ///     </para>
        ///     <para>
        ///         Regular expression: (Annotations?, ConceptIdentity?, LocalRepresentation?)
        ///     </para>
        /// </summary>
        /// <typeparam name="T">
        ///     The type of the return value
        /// </typeparam>
        /// <returns>
        ///     The <see cref="T" />.
        /// </returns>
        public T GetTypedLocalRepresentation<T>() where T : RepresentationType
        {
            XElement x = this.GetElement(_referenceName);
            return XTypedServices.ToXTypedElement<T>(x, LinqToXsdTypeManager.Instance);
        }

        /// <summary>
        /// <para>
        /// Sets the LocalRepresentation references item schemes that may be used to create the representation of a component. The type of this must be refined such that a concrete item scheme reference is used.
        ///     </para>
        /// <para>
        /// Occurrence: optional
        ///     </para>
        /// <para>
        /// Regular expression: (Annotations?, ConceptIdentity?, LocalRepresentation?)
        ///     </para>
        /// </summary>
        /// <typeparam name="T">
        /// The type of <paramref name="value"/>
        /// </typeparam>
        /// <param name="value">
        /// The value.
        /// </param>
        public void SetTypedLocalRepresentation<T>(T value) where T : RepresentationType
        {
            this.SetElement(_referenceName, value);
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="Structure.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message
{
    using System.Xml;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;

    using Xml.Schema.Linq;

    /// <summary>
    /// The structure.
    /// </summary>
    public partial class Structure
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the footer.
        /// </summary>
        public Footer.Footer Footer
        {
            get
            {
                return this.ContentField.Footer;
            }

            set
            {
                this.ContentField.Footer = value;
            }
        }

        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        public StructureHeaderType Header
        {
            get
            {
                return this.ContentField.Header;
            }

            set
            {
                this.ContentField.Header = value;
            }
        }

        /// <summary>
        /// Gets or sets the structures.
        /// </summary>
        public StructuresType Structures
        {
            get
            {
                return this.ContentField.Structures;
            }

            set
            {
                this.ContentField.Structures = value;
            }
        }

        #endregion

        /// <summary>
        /// Load error message to a LINQ2XSD <see cref="Structure"/> object
        /// </summary>
        /// <param name="xmlFile">The input file</param>
        /// <returns>a LINQ2XSD <see cref="Structure"/> object</returns>
        public static Structure Load(XmlReader xmlFile)
        {
            return XTypedServices.Load<Structure, StructureType>(xmlFile, LinqToXsdTypeManager.Instance);
        }
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ObjectTypeCodelistTypeConstants.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common
{
    public static class ObjectTypeCodelistTypeConstants
    {

        public const string Any = "Any";
        public const string Agency = "Agency";
        public const string AgencyScheme = "AgencyScheme";
        public const string AttachmentConstraint = "AttachmentConstraint";
        public const string Attribute = "Attribute";
        public const string AttributeDescriptor = "AttributeDescriptor";
        public const string Categorisation = "Categorisation";
        public const string Category = "Category";
        public const string CategorySchemeMap = "CategorySchemeMap";
        public const string CategoryScheme = "CategoryScheme";
        public const string Code = "Code";
        public const string CodeMap = "CodeMap";
        public const string Codelist = "Codelist";
        public const string CodelistMap = "CodelistMap";
        public const string ComponentMap = "ComponentMap";
        public const string Concept = "Concept";
        public const string ConceptMap = "ConceptMap";
        public const string ConceptScheme = "ConceptScheme";
        public const string ConceptSchemeMap = "ConceptSchemeMap";
        public const string Constraint = "Constraint";
        public const string ConstraintTarget = "ConstraintTarget";
        public const string ContentConstraint = "ContentConstraint";
        public const string Dataflow = "Dataflow";
        public const string DataConsumer = "DataConsumer";
        public const string DataConsumerScheme = "DataConsumerScheme";
        public const string DataProvider = "DataProvider";
        public const string DataProviderScheme = "DataProviderScheme";
        public const string DataSetTarget = "DataSetTarget";
        public const string DataStructure = "DataStructure";
        public const string Dimension = "Dimension";
        public const string DimensionDescriptor = "DimensionDescriptor";
        public const string DimensionDescriptorValuesTarget = "DimensionDescriptorValuesTarget";
        public const string GroupDimensionDescriptor = "GroupDimensionDescriptor";
        public const string HierarchicalCode = "HierarchicalCode";
        public const string HierarchicalCodelist = "HierarchicalCodelist";
        public const string Hierarchy = "Hierarchy";
        public const string HybridCodelistMap = "HybridCodelistMap";
        public const string HybridCodeMap = "HybridCodeMap";
        public const string IdentifiableObjectTarget = "IdentifiableObjectTarget";
        public const string Level = "Level";
        public const string MeasureDescriptor = "MeasureDescriptor";
        public const string MeasureDimension = "MeasureDimension";
        public const string Metadataflow = "Metadataflow";
        public const string MetadataAttribute = "MetadataAttribute";
        public const string MetadataSet = "MetadataSet";
        public const string MetadataStructure = "MetadataStructure";
        public const string MetadataTarget = "MetadataTarget";
        public const string Organisation = "Organisation";
        public const string OrganisationMap = "OrganisationMap";
        public const string OrganisationScheme = "OrganisationScheme";
        public const string OrganisationSchemeMap = "OrganisationSchemeMap";
        public const string OrganisationUnit = "OrganisationUnit";
        public const string OrganisationUnitScheme = "OrganisationUnitScheme";
        public const string PrimaryMeasure = "PrimaryMeasure";
        public const string Process = "Process";
        public const string ProcessStep = "ProcessStep";
        public const string ProvisionAgreement = "ProvisionAgreement";
        public const string ReportingCategory = "ReportingCategory";
        public const string ReportingCategoryMap = "ReportingCategoryMap";
        public const string ReportingTaxonomy = "ReportingTaxonomy";
        public const string ReportingTaxonomyMap = "ReportingTaxonomyMap";
        public const string ReportingYearStartDay = "ReportingYearStartDay";
        public const string ReportPeriodTarget = "ReportPeriodTarget";
        public const string ReportStructure = "ReportStructure";
        public const string StructureMap = "StructureMap";
        public const string StructureSet = "StructureSet";
        public const string TimeDimension = "TimeDimension";
        public const string Transition = "Transition";
    }
}

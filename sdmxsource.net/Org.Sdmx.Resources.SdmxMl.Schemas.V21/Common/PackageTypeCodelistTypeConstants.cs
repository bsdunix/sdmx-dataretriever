﻿// -----------------------------------------------------------------------
// <copyright file="PackageTypeCodelistTypeConstants.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common
{
    /// <summary>
    /// The <see cref="PackageTypeCodelistType"/> constants.
    /// </summary>
    public static class PackageTypeCodelistTypeConstants
    {
        #region Constants

        /// <summary>
        /// The base.
        /// </summary>
        public const string Base = "base";

        /// <summary>
        /// The category scheme.
        /// </summary>
        public const string Categoryscheme = "categoryscheme";

        /// <summary>
        /// The codelist.
        /// </summary>
        public const string Codelist = "codelist";

        /// <summary>
        /// The concept scheme.
        /// </summary>
        public const string Conceptscheme = "conceptscheme";

        /// <summary>
        /// The data structure.
        /// </summary>
        public const string Datastructure = "datastructure";

        /// <summary>
        /// The mapping.
        /// </summary>
        public const string Mapping = "mapping";

        /// <summary>
        /// The metadata structure.
        /// </summary>
        public const string Metadatastructure = "metadatastructure";

        /// <summary>
        /// The process.
        /// </summary>
        public const string Process = "process";

        /// <summary>
        /// The registry.
        /// </summary>
        public const string Registry = "registry";

        #endregion

    }
}
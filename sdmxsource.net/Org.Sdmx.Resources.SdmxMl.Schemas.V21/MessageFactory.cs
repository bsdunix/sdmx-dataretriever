// -----------------------------------------------------------------------
// <copyright file="MessageFactory.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21
{
    using System.Xml;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;

    using Xml.Schema.Linq;

    /// <summary>
    /// Load SDMX Messages into memory objects
    /// </summary>
    public static class MessageFactory
    {
        #region Public Methods and Operators

        /// <summary>
        /// Load SDMX-ML v2.1 from <paramref name="reader"/> and return it in <typeparamref name="TW"/>
        /// </summary>
        /// <typeparam name="TW">
        /// The element CLR type
        /// </typeparam>
        /// <typeparam name="T">
        /// The element XSD type CLR type
        /// </typeparam>
        /// <param name="reader">
        /// The input <see cref="XmlReader"/>
        /// </param>
        /// <returns>
        /// The SDMX-ML v2.1 from <paramref name="reader"/> as <typeparamref name="TW"/>
        /// </returns>
        public static TW Load<TW, T>(XmlReader reader)
            where T : MessageType
            where TW : XTypedElement
        {
            return XTypedServices.Load<TW, T>(reader, LinqToXsdTypeManager.Instance);
        }

        #endregion
    }
}
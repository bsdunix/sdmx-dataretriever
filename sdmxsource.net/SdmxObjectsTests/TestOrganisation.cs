﻿// -----------------------------------------------------------------------
// <copyright file="TestOrganisation.cs" company="EUROSTAT">
//   Date Created : 2014-12-12
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjectsTests.
// 
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxObjectsTests
{
    using System.Linq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    /// Test unit for organisation related SDMX Objects
    /// </summary>
    [TestFixture]
    public class TestOrganisation
    {
        /// <summary>
        /// Test unit for <see cref="OrganisationUnitMutableCore"/> 
        /// </summary>
        [TestCase("TEST_PARENT", true)]
        [TestCase("", false)]
        [TestCase(null, false)]
        public void TestOrganisationUnitMutableCoreSetParentUnit(string parentUnit, bool hasParent)
        {
            var organisationScheme = new OrganisationUnitSchemeMutableCore() { Id = "TEST", AgencyId = "TEST", Version = "1.0" };
            organisationScheme.AddName("en", "Test organisation unit scheme");
            var unit = organisationScheme.CreateItem("TEST_UNIT1", "Test organisation unit");
            unit.ParentUnit = parentUnit;
            if (!string.IsNullOrWhiteSpace(parentUnit))
            {
                organisationScheme.CreateItem(parentUnit, parentUnit);
            }

            var immutable = organisationScheme.ImmutableInstance;
            var first = immutable.Items.First(organisationUnit => organisationUnit.Id.Equals(unit.Id));
            Assert.AreEqual(hasParent, first.HasParentUnit, first.ParentUnit);
            if (first.HasParentUnit)
            {
                Assert.AreEqual(parentUnit, first.ParentUnit);
            }
        }
    }
}
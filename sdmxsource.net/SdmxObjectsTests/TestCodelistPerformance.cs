﻿// -----------------------------------------------------------------------
// <copyright file="TestCodelistPerformance.cs" company="EUROSTAT">
//   Date Created : 2014-06-13
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjectsTests.
// 
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxObjectsTests
{
    using System.Diagnostics;
    using System.Globalization;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

    [TestFixture]
    public class TestCodelistPerformance
    {
        [TestCase(10000)]
        public void TestMutableToImmutable(int size)
        {
            ICodelistMutableObject mutable = BuildMutable(size);
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            ICodelistObject immutableInstance = mutable.ImmutableInstance;
            stopwatch.Stop();
            Assert.NotNull(immutableInstance);
            Assert.LessOrEqual(stopwatch.ElapsedMilliseconds, size >> 5);
        }

        private static ICodelistMutableObject BuildMutable(int size)
        {
            var codelist = new CodelistMutableCore() { Id = "TEST", AgencyId = "TEST_AGENCY", Version = "1.0" };
            codelist.AddName("en", "Test name");
            string lastCode = null;
            for (int i = 0; i < size; i++)
            {
                string codeId = string.Format(CultureInfo.InvariantCulture, "ID{0}", i);
                var code = codelist.CreateItem(codeId, codeId);
                if (lastCode != null && (i % 2) == 0)
                {
                    code.ParentCode = lastCode;
                }

                if ((i % 6) == 0)
                {
                    lastCode = codeId;
                }
            }

            return codelist;
        }
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ReferenceInfo.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureMutableParser.
// 
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Model
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     A class that hold the SDMX v2.0 <c>QueryStructureRequest</c> reference information
    /// </summary>
    public class ReferenceInfo : IReferenceInfo
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ReferenceInfo"/> class.
        /// </summary>
        /// <param name="sdmxStructure">
        /// The sdmx structure.
        /// </param>
        public ReferenceInfo(SdmxStructureEnumType sdmxStructure)
        {
            this.SdmxStructure = sdmxStructure;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the agency ID
        /// </summary>
        public string AgencyId { get; set; }

        /// <summary>
        ///     Gets or sets the ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        ///     Gets or sets the reference from.
        /// </summary>
        public INameableMutableObject ReferenceFrom { get; set; }

        /// <summary>
        ///     Gets the sdmx structure.
        /// </summary>
        public SdmxStructureEnumType SdmxStructure { get; private set; }

        /// <summary>
        ///     Gets or sets the urn.
        /// </summary>
        public Uri URN { get; set; }

        /// <summary>
        ///     Gets or sets the version.
        /// </summary>
        public string Version { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Create and returns a <see cref="IStructureReference"/> from the <see cref="ID"/>, <see cref="AgencyId"/>,
        ///     <see cref="Version"/>
        ///     and <see cref="SdmxStructure"/>
        /// </summary>
        /// <param name="items">
        /// Optional. Items
        /// </param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        public IStructureReference CreateReference(params string[] items)
        {
            return new StructureReferenceImpl(this.AgencyId, this.ID, this.Version, this.SdmxStructure, items);
        }

        #endregion
    }
}
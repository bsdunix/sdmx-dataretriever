﻿// -----------------------------------------------------------------------
// <copyright file="IReferenceInfo.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureMutableParser.
// 
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Model
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     An interface for holding the SDMX v2.0 <c>QueryStructureRequest</c> reference information
    /// </summary>
    public interface IReferenceInfo
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the agency ID
        /// </summary>
        string AgencyId { get; set; }

        /// <summary>
        ///     Gets or sets the ID
        /// </summary>
        string ID { get; set; }

        /// <summary>
        ///     Gets or sets the reference from artefact.
        /// </summary>
        INameableMutableObject ReferenceFrom { get; set; }

        /// <summary>
        ///     Gets the SDMX structure.
        /// </summary>
        SdmxStructureEnumType SdmxStructure { get; }

        /// <summary>
        ///     Gets or sets the urn.
        /// </summary>
        Uri URN { get; set; }

        /// <summary>
        ///     Gets or sets the version.
        /// </summary>
        string Version { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Create and returns a <see cref="IStructureReference"/> from the <see cref="ReferenceInfo.ID"/>,
        ///     <see cref="ReferenceInfo.AgencyId"/>
        ///     ,
        ///     <see cref="ReferenceInfo.Version"/>
        ///     and <see cref="ReferenceInfo.SdmxStructure"/>
        /// </summary>
        /// <param name="items">
        /// Optional. Items
        /// </param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        IStructureReference CreateReference(params string[] items);

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="StructureMutableWritingManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureMutableParser.
// 
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Manager
{
    using System.Xml;

    using Estat.Sri.SdmxStructureMutableParser.Factory;

    using Org.Sdmxsource.Sdmx.Structureparser.Manager;

    /// <summary>
    ///     The structure writing manager implementation.
    ///     It supports only a small subset of SDMX v2.0.
    /// </summary>
    /// <remarks>
    ///     Only the following structures are supported:
    ///     - Category Schemes (<c>MetaDataflow Ref</c> are ignored)
    ///     - Codelists
    ///     - Concept schemes
    ///     - Dataflows
    ///     - Hierarchical Codelists
    ///     - KeyFamilies (DSD)
    /// </remarks>
    /// <example>
    ///     A sample implementation in C# of <see cref="StructureMutableWritingManager" />.
    ///     <code source="..\ReUsingExamples\Structure\ReUsingStructureWritingManagerFast.cs" lang="cs" />
    /// </example> 
    public class StructureMutableWritingManager : StructureWriterManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StructureMutableWritingManager"/> class.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        public StructureMutableWritingManager(XmlWriter writer)
            : base(new SdmxStructureWriterV2Factory(writer))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureMutableWritingManager"/> class.
        /// </summary>
        public StructureMutableWritingManager()
            : base(new SdmxStructureWriterV2Factory())
        {
        }
    }
}
// -----------------------------------------------------------------------
// <copyright file="QueryStructureResponseWriterV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureMutableParser.
// 
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Engine.V2
{
    using System;
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Model;
    using Estat.Sri.SdmxStructureMutableParser.Model;
    using Estat.Sri.SdmxXmlConstants;

    /// <summary>
    ///     The query structure response writer v 2.
    /// </summary>
    internal class QueryStructureResponseWriterV2 : RegistryInterfaceWriterBaseV2, IRegistryInterfaceWriter
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryStructureResponseWriterV2"/> class.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        /// <param name="namespaces">
        /// The namespaces.
        /// </param>
        public QueryStructureResponseWriterV2(XmlWriter writer, SdmxNamespaces namespaces)
            : base(writer, namespaces)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Write the specified <paramref name="registry"/>
        /// </summary>
        /// <param name="registry">
        /// The <see cref="IRegistryInfo"/> object
        /// </param>
        public void Write(IRegistryInfo registry)
        {
            if (registry == null)
            {
                throw new ArgumentNullException("registry");
            }

            if (registry.QueryStructureResponse != null)
            {
                this.WriteQueryStructureResponse(registry.QueryStructureResponse);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Write QueryStructureResponse
        /// </summary>
        /// <param name="response">
        /// The QueryStructureResponseBean to write
        /// </param>
        private void WriteQueryStructureResponse(IQueryStructureResponseInfo response)
        {
            this.WriteStartElement(ElementNameTable.QueryStructureResponse);

            this.WriteStatusMessage(response.StatusMessage);

            var structureWriter = new StructureWriterV2(this.SdmxMLWriter);
            structureWriter.SetTopElementsNS(this.Namespaces.Registry);
            structureWriter.WriteStructure(response.Structure, null);

            this.WriteEndElement(); // </QueryStructureResponse>
        }

        #endregion
    }
}
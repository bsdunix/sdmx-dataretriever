// -----------------------------------------------------------------------
// <copyright file="CodeListWriterV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureMutableParser.
// 
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Engine.V2
{
    using System.Collections.Generic;
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Model;
    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;

    /// <summary>
    ///     The code list writer v 2.
    /// </summary>
    internal class CodeListWriterV2 : StructureWriterBaseV2, IMutableWriter
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeListWriterV2"/> class.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        /// <param name="namespaces">
        /// The namespaces.
        /// </param>
        public CodeListWriterV2(XmlWriter writer, SdmxNamespaces namespaces)
            : base(writer, namespaces)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The write.
        /// </summary>
        /// <param name="structure">
        /// The structure.
        /// </param>
        public void Write(IMutableObjects structure)
        {
            this.WriteCodeLists(structure.Codelists);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Write the element Code using the given code
        /// </summary>
        /// <param name="item">
        /// The <see cref="ICodeMutableObject"/> object to write
        /// </param>
        private void WriteCode(ICodeMutableObject item)
        {
            this.WriteStartElement(this.DefaultPrefix, ElementNameTable.Code);
            this.WriteAttributeString(AttributeNameTable.value, item.Id);
            this.TryWriteAttribute(AttributeNameTable.urn, item.Urn);
            this.TryWriteAttribute(AttributeNameTable.parentCode, item.ParentCode);

            //// NOTE in SDMX 2.0 Code has only description. The Common API expects it to have Name. So we put Code description to Name.
            foreach (ITextTypeWrapperMutableObject textTypeBean in item.Names)
            {
                this.WriteTextType(this.DefaultNS, textTypeBean, ElementNameTable.Description);
            }

            this.WriteAnnotations(ElementNameTable.Annotations, item.Annotations);
            this.WriteEndElement();
        }

        /// <summary>
        /// Write the codelists inside the <paramref name="codeLists"/>
        /// </summary>
        /// <param name="codeLists">
        /// The <see cref="ICodelistMutableObject"/> collection
        /// </param>
        private void WriteCodeLists(IEnumerable<ICodelistMutableObject> codeLists)
        {
            this.WriteStartElement(this.RootNamespace, ElementNameTable.CodeLists);
            foreach (ICodelistMutableObject codelist in codeLists)
            {
                this.WriteMaintainableArtefact(ElementNameTable.CodeList, codelist);
                foreach (ICodeMutableObject code in codelist.Items)
                {
                    this.WriteCode(code);
                }

                this.WriteAnnotations(ElementNameTable.Annotations, codelist.Annotations);
                this.WriteEndElement();
            }

            this.WriteEndElement();
        }

        #endregion
    }
}
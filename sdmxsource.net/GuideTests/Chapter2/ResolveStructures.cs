﻿// -----------------------------------------------------------------------
// <copyright file="ResolveStructures.cs" company="EUROSTAT">
//   Date Created : 2013-04-19
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of GuideTests.
// 
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.Chapter2
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
	/// Description of ResolveStructures.
	/// </summary>
	public class ResolveStructures
	{
		private readonly IStructureParsingManager _structureParsingManager;

        private readonly IReadableDataLocationFactory _dataLocationFactory;
		
		public ResolveStructures()
		{
			this._structureParsingManager = new StructureParsingManager();
            this._dataLocationFactory = new ReadableDataLocationFactory();
		}
		
		public void Resolve(FileInfo codelistsConceptsFile, FileInfo dsdFile) 
        {
		    ISdmxObjectRetrievalManager retManager;
		    using (IReadableDataLocation codelistRdl = this._dataLocationFactory.GetReadableDataLocation(codelistsConceptsFile))
		    {
		        retManager = new InMemoryRetrievalManager(codelistRdl, this._structureParsingManager, null);
		    }

		    Console.WriteLine("In memory objects built!!!");

		    IStructureWorkspace workspace;
		    using (IReadableDataLocation dsdRdl = this._dataLocationFactory.GetReadableDataLocation(dsdFile))
		    {
		        ResolutionSettings settings = new ResolutionSettings(ResolveExternalSetting.Resolve, ResolveCrossReferences.ResolveExcludeAgencies);
		        workspace = this._structureParsingManager.ParseStructures(dsdRdl, settings, retManager);
		    }

		    ISdmxObjects sdmxObjects = workspace.GetStructureObjects(true);
			
			ISet<IMaintainableObject> maintainables =  sdmxObjects.GetAllMaintainables();
			foreach (IMaintainableObject m in maintainables) {
				Console.WriteLine(m.Urn);
				Console.WriteLine("{0} - {1}", m.StructureType.StructureType, m.Name);
				Console.WriteLine(" --- ");
			}
		}
	}
}

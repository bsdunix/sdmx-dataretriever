﻿// -----------------------------------------------------------------------
// <copyright file="ReadingStructures.cs" company="EUROSTAT">
//   Date Created : 2015-04-08
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of GuideTests.
// 
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.Chapter2
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;

    internal class ReadingStructures
    {
        private readonly IStructureParsingManager _structureParsingManager;

        private readonly IReadableDataLocationFactory _dataLocationFactory;

        public ReadingStructures()
        {
            this._structureParsingManager = new StructureParsingManager();
            this._dataLocationFactory = new ReadableDataLocationFactory();
        }

        public void ReadStructures(FileInfo structureFile)
        {
            IStructureWorkspace workspace;
            using (IReadableDataLocation rdl = this._dataLocationFactory.GetReadableDataLocation(structureFile))
            {
                workspace = this._structureParsingManager.ParseStructures(rdl);
            }

            ISdmxObjects sdmxObjects = workspace.GetStructureObjects(false);

            ISet<IMaintainableObject> maintainable = sdmxObjects.GetAllMaintainables();
            foreach (IMaintainableObject m in maintainable)
            {
                Console.WriteLine(m.Urn);
                Console.WriteLine("{0} - {1}", m.StructureType.StructureType, m.Name);
                Console.WriteLine(" --- ");
            }
        }

        public void ReadStructures(Stream stream)
        {
            IStructureWorkspace workspace;
            using (IReadableDataLocation rdl = this._dataLocationFactory.GetReadableDataLocation(stream))
            {
                // ...
            }
        }
    

    /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            ReadingStructures rs = new ReadingStructures();

            rs.ReadStructures(new FileInfo("output/structures.xml"));

            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}
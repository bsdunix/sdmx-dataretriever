﻿// -----------------------------------------------------------------------
// <copyright file="HtmlStructureWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2015-05-14
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of GuideTests.
// 
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.Chapter8
{
    using System.Globalization;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    public class HtmlStructureWriterEngine : IStructureWriterEngine
    {
        private readonly TextWriter _writer;

        public HtmlStructureWriterEngine(Stream stream)
        {
            this._writer = new StreamWriter(stream);
            this.PrintHtmlTable();
        }
        public void WriteStructure(IMaintainableObject maintainableObject)
        {
            PrintMaintainable(maintainableObject);
            Close();
        }
        public void WriteStructures(ISdmxObjects sdmxObjects)
        {
            foreach (var maintainableObject in sdmxObjects.GetAllMaintainables())
            {
                PrintMaintainable(maintainableObject);
            }

            Close();
        }
        private void Close()
        {
            this._writer.WriteLine("</table>");
            this._writer.Close();
        }
        private void PrintMaintainable(IMaintainableObject maintainableObject)
        {
            this._writer.WriteLine("<tr><td>{0}</td>", maintainableObject.AgencyId);
            this._writer.WriteLine("<td>{0}</td>", maintainableObject.StructureType.StructureType);
            this._writer.WriteLine("<td>{0}</td>", maintainableObject.Name);
            this._writer.WriteLine("<td>{0}</td>", maintainableObject.IdentifiableComposites.Count);
            this._writer.WriteLine("</tr>");
        }
        private void PrintHtmlTable()
        {
            this._writer.WriteLine("<table border='1' width='100%' id='output'>");
            this._writer.WriteLine("<th>Agency Id</th>");
            this._writer.WriteLine("<th>Structure Type</th>");
            this._writer.WriteLine("<th>Structure Name</th>");
            this._writer.WriteLine("<th># Identifiable Composites</th>");
        }
    }
}
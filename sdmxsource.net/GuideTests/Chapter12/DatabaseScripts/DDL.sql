-- -- Uncomment the following to rebuild the DB.
 revert;
 use master;
 go
 drop login uc1user;
 drop database USECASE1;
 go

-- Create Database, login and user.
-- requires admin user rights to create the db and new login and users.
create database USECASE1;
GO
create login uc1user with password ='123', DEFAULT_DATABASE=USECASE1;
go
use USECASE1;
GO
create user uc1user FOR LOGIN uc1user;
GO

grant select, control, alter,insert on Schema::dbo to uc1user;
grant CREATE TABLE to uc1user;

EXECUTE AS USER = 'uc1user';
go

-- DDL requires rights create table and insert/select data.

create table CODELIST (
    CL_ID bigint IDENTITY(1,1) PRIMARY KEY NOT NULL,
    ID varchar(50) NOT NULL,
    AGENCY varchar(50) NOT NULL,
    CVERSION varchar(50) NOT NULL DEFAULT '1.0',
    URI varchar(255) NULL DEFAULT NULL,
    VALID_FROM datetime NULL,
	VALID_TO datetime NULL,
	IS_FINAL BIT NOT NULL DEFAULT 0,
    IS_PARTIAL BIT NOT NULL DEFAULT 0
);
go
create table CODE (
    C_ID bigint IDENTITY(1,1) PRIMARY KEY NOT NULL,
    CL_ID bigint FOREIGN KEY REFERENCES CODELIST (CL_ID) ON DELETE CASCADE,
    ID varchar(50) NOT NULL,
    URI varchar(255) NULL DEFAULT NULL,
    PARENT_CODE_ID varchar(50)
);
go
create table ANNOTATION (
    AN_ID bigint IDENTITY(1,1) PRIMARY KEY NOT NULL,
    ID nvarchar(50) NULL,
    TITLE nvarchar(255) NULL,
    ATYPE nvarchar(50) NULL,
    URI varchar(255) NULL
);
go
create table LOCALISED_STRING (
    LS_ID bigint identity(1,1) PRIMARY KEY NOT NULL,
	LANGUAGE varchar(50) NOT NULL,
	TEXT nvarchar(4000) NOT NULL
);
go
create table LOCALISED_CODELIST (
    LS_ID bigint PRIMARY KEY NOT NULL FOREIGN KEY REFERENCES LOCALISED_STRING (LS_ID) ,
    CL_ID bigint FOREIGN KEY REFERENCES CODELIST (CL_ID),
	IS_DESCRIPTION BIT NOT NULL DEFAULT 0
);
go
create table LOCALISED_CODE (
    LS_ID bigint PRIMARY KEY NOT NULL FOREIGN KEY REFERENCES LOCALISED_STRING (LS_ID) ,
    C_ID bigint FOREIGN KEY REFERENCES CODE (C_ID),
	IS_DESCRIPTION BIT NOT NULL DEFAULT 0
);
go

create table LOCALISED_ANNOTATION (
    LS_ID bigint PRIMARY KEY NOT NULL FOREIGN KEY REFERENCES LOCALISED_STRING (LS_ID),
    AN_ID bigint FOREIGN KEY REFERENCES ANNOTATION (AN_ID)
);
go

create table ANNOTATION_CODELIST (
    AN_ID bigint PRIMARY KEY NOT NULL FOREIGN KEY REFERENCES ANNOTATION (AN_ID),
    CL_ID bigint FOREIGN KEY REFERENCES CODELIST (CL_ID)
);
go
create table ANNOTATION_CODE (
    AN_ID bigint PRIMARY KEY NOT NULL FOREIGN KEY REFERENCES ANNOTATION (AN_ID),
    C_ID bigint FOREIGN KEY REFERENCES CODE (C_ID)
);


-- data
begin transaction


declare @clpk bigint;
declare @cpk bigint;
declare @lpk bigint;
declare @apk bigint;

INSERT INTO CODELIST (ID, AGENCY, CVERSION, URI, VALID_FROM, VALID_TO, IS_FINAL, IS_PARTIAL) VALUES
('TEST', 'TEST_AGENCY', '1.2', DEFAULT, '2014-03-10', '2014-03-12', 0, 0);
set @clpk=SCOPE_IDENTITY();
-- English code list name
INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('en', 'Code list name in English');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_CODELIST (LS_ID, CL_ID, IS_DESCRIPTION) VALUES (@lpk, @clpk, 0);

-- English code list description
INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('en', 'Code list description in English');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_CODELIST (LS_ID, CL_ID, IS_DESCRIPTION) VALUES (@lpk, @clpk, 1);

-- Italian code list name
INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('en', 'Code list nome in Italiano');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_CODELIST (LS_ID, CL_ID, IS_DESCRIPTION) VALUES (@lpk, @clpk, 0);

-- add an annotation to the codelist.
INSERT INTO ANNOTATION (ATYPE, ID, TITLE, URI) VALUES ('CUSTOM TYPE1', 'CUSTOM_ID', 'A title', DEFAULT);
set @apk = SCOPE_IDENTITY();
INSERT INTO ANNOTATION_CODELIST (AN_ID, CL_ID) VALUES (@apk, @clpk);

INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('en', 'Annotation text in English');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_ANNOTATION (AN_ID, LS_ID) VALUES (@apk, @lpk);

INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('it', 'Annotation text in Italiano');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_ANNOTATION (AN_ID, LS_ID) VALUES (@apk, @lpk);

-- insert code
INSERT INTO CODE (CL_ID, ID, URI, PARENT_CODE_ID) VALUES (@clpk, 'T1', 'http://somelink.to/documentation/T1.html', null);
set @cpk=SCOPE_IDENTITY();
-- English code name
INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('en', 'Code name in English');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_CODE (LS_ID, C_ID, IS_DESCRIPTION) VALUES (@lpk, @cpk, 0);

-- Italian code name
INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('en', 'Code nome in Italiano');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_CODE (LS_ID, C_ID, IS_DESCRIPTION) VALUES (@lpk, @cpk, 0);

-- add an annotation to the code.
INSERT INTO ANNOTATION (ATYPE, ID, TITLE, URI) VALUES ('CUSTOM TYPE2', 'CUSTOM_ID', 'A title', 'http://link.to/some/document');
set @apk = SCOPE_IDENTITY();
INSERT INTO ANNOTATION_CODE (AN_ID, C_ID) VALUES (@apk, @cpk);

-- insert code
INSERT INTO CODE (CL_ID, ID, URI, PARENT_CODE_ID) VALUES (@clpk, 'T2', 'http://somelink.to/documentation/T2.html', 'T1');
set @cpk=SCOPE_IDENTITY();
-- English code name
INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('en', 'Code name in English');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_CODE (LS_ID, C_ID, IS_DESCRIPTION) VALUES (@lpk, @cpk, 0);

-- Italian code name
INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('en', 'Code nome in Italiano');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_CODE (LS_ID, C_ID, IS_DESCRIPTION) VALUES (@lpk, @cpk, 0);

-- insert code
INSERT INTO CODE (CL_ID, ID, URI, PARENT_CODE_ID) VALUES (@clpk, 'T3', 'http://somelink.to/documentation/T2.html', null);
set @cpk=SCOPE_IDENTITY();
-- English code name
INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('en', 'Code name in English');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_CODE (LS_ID, C_ID, IS_DESCRIPTION) VALUES (@lpk, @cpk, 0);

-- Italian code name
INSERT INTO LOCALISED_STRING (LANGUAGE, TEXT) VALUES ('en', 'Code nome in Italiano');
set @lpk=SCOPE_IDENTITY();
INSERT INTO LOCALISED_CODE (LS_ID, C_ID, IS_DESCRIPTION) VALUES (@lpk, @cpk, 0);
commit transaction;

select case when count(*) > 5 then '>>>>>> Success' else '>>>>>>>> Failure' end from LOCALISED_CODE;

﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="EUROSTAT">
//   Date Created : 2015-04-08
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of GuideTests.
// 
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SkeletonApplication
{
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     The use case 1.
    /// </summary>
    public static class Program
    {
        #region Public Methods and Operators

        /// <summary>
        /// The main method.
        /// </summary>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        public static void Main(string[] args)
        {
            // Steps 1 - 4 are performed in the new class DatabaseRetrievalManager, an implementation of the ISdmxObjectRetrievalManager interface.
            // DatabaseRetrievalManager: Step 1. Creating an implementation of  the ISdmxObjectRetrievalManager interface
            // DatabaseRetrievalManager: Step 2. Implementing ISdmxObjectRetrievalManager GetMaintainableObjects<> method.
            // DatabaseRetrievalManager: Step 3. Retrieving codelists from database. 
            // DatabaseRetrievalManager: Step 4. Retrieving Codes from the database.

            // Step 5. Create a ISdmxObjectRetrievalManager instance
            ISdmxObjectRetrievalManager retrievalManager = new DatabaseRetrievalManager();

            // Step 6. Create an IHeader  instance.
            // Create a new HeaderImpl instance with ID “IDREF0001”, Sender ID “ZZ9” 
            IHeader header = new HeaderImpl("IDREF001", "ZZ9"); // can be an instance or even a static field.

            // Step 7. Create the query
            IMaintainableRefObject query = new MaintainableRefObjectImpl("TEST_AGENCY", "TEST", "1.2");

            // Step 8. Get the codelist objects
            var codelistObjects = retrievalManager.GetMaintainableObjects<ICodelistObject>(query);

            // Step 9. Build the immutable object container.
            // Create an immutable container with the header and the codelist objects.
            // Note that immutable container can be modified.
            ISdmxObjects immutableObjects = new SdmxObjectsImpl(header, codelistObjects);

            // Step 10. Create an instance of the IStructureWriterManager.
            IStructureWriterManager structureWriterManager = new StructureWriterManager(); // can be an instance or even static field.

            // Step 11. Create a SdmxStructureFormat instance with StructureOutputFormat SdmxV21StructureDocument
            // SDMX v2.1. Create a SdmxStructureFormat instance with StructureOutputFormat SdmxV21StructureDocument. It can also be an instance or even a static field.
            IStructureFormat formatV21 = new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument));

            // Step 12. Create a SdmxStructureFormat instance with StructureOutputFormat SdmxV2StructureDocument
            // SDMX v2.0. Create a SdmxStructureFormat instance with StructureOutputFormat SdmxV2StructureDocument. It can also be an instance or even a static field.
            IStructureFormat formatV20 = new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV2StructureDocument));


            // Step 13. Write to  the SDMX v2.1 structure file.
            using (Stream fileStream = File.Create(@"..\..\Exercise Output\outputv21.xml"))
            {
                // Write the objects in SDMX v2.1 format. Then flush the output.
                structureWriterManager.WriteStructures(immutableObjects, formatV21, fileStream);
                fileStream.Flush();
            }

            // Step 14. Write to  the SDMX v2.0 structure file. 
            using (Stream fileStream = File.Create(@"..\..\Exercise Output\outputv20.xml"))
            {
                // Write the objects in SDMX v2.0 format. Then flush the output.
                structureWriterManager.WriteStructures(immutableObjects, formatV20, fileStream);
                fileStream.Flush();
            }
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ReadingStructuresFromStream.cs" company="EUROSTAT">
//   Date Created : 2015-04-08
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of GuideTests.
// 
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.Chapter12.DataRetrievalExample
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Io;

    class ReadingStructuresFromStream
    {

        private IStructureParsingManager spm;
        public void ReadingStructures(String structureFile, SdmxSchemaEnumType schema, String output){

            // Step 2. Implementing a helper class that parses DSD, Dataflow and writes datasets.
            //1)	Call the method getXmlWriter
            XmlWriter xmlWriter = this.getXmlWriter(output);

            //2)	Obtain IDataWriterEngine
            IDataWriterEngine compactWriter = new CompactDataWriterEngine(xmlWriter,SdmxSchema.GetFromEnum(schema));

            //3)	Read a structure file from a stream
            this.spm = new StructureParsingManager();
            ISdmxObjects structureObjects = new SdmxObjectsImpl();

            using (FileStream stream = File.Open(structureFile, FileMode.Open, FileAccess.Read))
            {
                using (IReadableDataLocation rdl = new ReadableDataLocationTmp(stream))
                {
                    IStructureWorkspace structureWorkspace = this.spm.ParseStructures(rdl);
                    structureObjects = structureWorkspace.GetStructureObjects(false);
                }
            }

            //4)	The IMaintainableRefObject is used to reference structures that extend the IMaintainableObject Interface.  
            IMaintainableRefObject dsdRef = new MaintainableRefObjectImpl("ESTAT", "STS", "2.2");
            IMaintainableRefObject flowRef = new MaintainableRefObjectImpl("ESTAT", "SSTSCONS_PROD_A", "1.0");

            //5)	Get the DataStructure and the Dataflow
            ISet<IDataStructureObject> dsd = structureObjects.GetDataStructures(dsdRef);
            ISet<IDataflowObject> dataflow = structureObjects.GetDataflows(flowRef);

            //6)	After obtaining our IDataWriterEngine, and the IDataStructureObject that we wish to create data for, we can write the dataset
            SampleDataWriter sampleDataWriter = new SampleDataWriter();
            sampleDataWriter.writeSampleData(dsd.FirstOrDefault(), dataflow.FirstOrDefault(), compactWriter);
            xmlWriter.Close();
        }

        public  XmlWriter getXmlWriter(String output){
            Stream stream = new FileStream(output, FileMode.Create);
            return XmlWriter.Create(stream, new XmlWriterSettings {Indent = true});
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ReferenceExamples.cs" company="EUROSTAT">
//   Date Created : 2015-05-13
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of GuideTests.
// 
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.Chapter0
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    public class ReferenceExamples
    {
        public IStructureReference CreateCategorySchemeReference()
        {
            IStructureReference reference = new StructureReferenceImpl(
                "SDMX", 
                "SDMXStatMatDomainsWD1", 
                "1.0", 
                SdmxStructureEnumType.CategoryScheme);
            return reference;
        }

        public IStructureReference CreateCategorySchemeReferenceFromUrn()
        {
            Uri urn = new Uri("urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=SDMX:SDMXStatMatDomainsWD1(1.0)");
            IStructureReference reference = new StructureReferenceImpl(urn);
            return reference;
        }

        public IStructureReference CreateCategoryReference()
        {
            IStructureReference reference = new StructureReferenceImpl(
                "SDMX",
                "SDMXStatMatDomainsWD1",
                "1.0",
                SdmxStructureEnumType.Category,
                "1",
                "1");
            return reference;
        }

        public IStructureReference CreateSdmxCodelistsReference()
        {
            IStructureReference reference = new StructureReferenceImpl("SDMX", null, null, SdmxStructureEnumType.CodeList);
            return reference;
        }
    }
}
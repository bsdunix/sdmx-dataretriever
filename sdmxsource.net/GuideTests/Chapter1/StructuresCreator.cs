﻿// -----------------------------------------------------------------------
// <copyright file="StructuresCreator.cs" company="EUROSTAT">
//   Date Created : 2014-04-28
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of GuideTests.
// 
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.Chapter1
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///    Creating Structures example
    /// </summary>
    public class StructuresCreator
    {
        #region Public Methods and Operators


        /// <summary>
        /// Builds the agency scheme.
        /// </summary>
        /// <returns>
        /// The <see cref="IAgencyScheme"/>.
        /// </returns>
        public IAgencyScheme BuildAgencyScheme()
        {
            IAgencyScheme defScheme = AgencySchemeCore.CreateDefaultScheme();
            IAgencySchemeMutableObject mutableDefScheme = defScheme.MutableInstance;

            mutableDefScheme.CreateItem("SDMXSOURCE", "Sdmx Source");
            return mutableDefScheme.ImmutableInstance;
        }


        /// <summary>
        /// Builds the concept scheme.
        /// </summary>
        /// <returns>
        /// The <see cref="IConceptSchemeObject"/>.
        /// </returns>
        public IConceptSchemeObject BuildConceptScheme()
        {
            IConceptSchemeMutableObject conceptSchemeMutable = new ConceptSchemeMutableCore();
            conceptSchemeMutable.AgencyId = "SDMXSOURCE";
            conceptSchemeMutable.Id = "CONCEPTS";
            conceptSchemeMutable.Version = "1.0";
            conceptSchemeMutable.AddName("en", "Web Service Concepts");

            conceptSchemeMutable.CreateItem("COUNTRY", "Country");
            conceptSchemeMutable.CreateItem("INDICATOR", "World Developement Indicators");
            conceptSchemeMutable.CreateItem("TIME", "Time");
            conceptSchemeMutable.CreateItem("OBS_VALUE", "Observation Value");

            return conceptSchemeMutable.ImmutableInstance;
        }

        /// <summary>
        /// Builds the country codelist.
        /// </summary>
        /// <returns>
        /// The <see cref="ICodelistObject" />.
        /// </returns>
        public ICodelistObject BuildCountryCodelist()
        {
            ICodelistMutableObject codelistMutable = new CodelistMutableCore();
            codelistMutable.AgencyId = "SDMXSOURCE";
            codelistMutable.Id = "CL_COUNTRY";
            codelistMutable.Version = "1.0";
            codelistMutable.AddName("en", "Country");

            codelistMutable.CreateItem("UK", "United Kingdom");
            codelistMutable.CreateItem("FR", "France");
            codelistMutable.CreateItem("DE", "Germany");

            return codelistMutable.ImmutableInstance;
        }

        /// <summary>
        /// Builds the data structure.
        /// </summary>
        /// <returns>
        /// The <see cref="IDataStructureObject" />.
        /// </returns>
        public IDataStructureObject BuildDataStructure()
        {
            IDataStructureMutableObject dsd = new DataStructureMutableCore();
            dsd.AgencyId = "SDMXSOURCE";
            dsd.Id = "WDI";
            dsd.AddName("en", "World Development Indicators");

            dsd.AddDimension(CreateConceptReference("COUNTRY"), CreateCodelistReference("CL_COUNTRY"));
            dsd.AddDimension(CreateConceptReference("INDICATOR"), CreateCodelistReference("CL_INDICATOR"));
            IDimensionMutableObject timeDim = dsd.AddDimension(CreateConceptReference("TIME"), null);
            timeDim.TimeDimension = true;
            dsd.AddPrimaryMeasure(CreateConceptReference("OBS_VALUE"));

            return dsd.ImmutableInstance;
        }

        /// <summary>
        /// Builds a dataflow that references a dsd
        /// </summary>
        /// <param name="id">
        /// the id of the dataflow
        /// </param>
        /// <param name="name">
        /// the english name of the dataflow
        /// </param>
        /// <param name="dsd">
        /// the data structure that is being referenced by the dsd.
        /// </param>
        /// <returns>
        /// the newly created dataflow.
        /// </returns>
        public IDataflowObject BuildDataflow(string id, string name, IDataStructureObject dsd)
        {
            IDataflowMutableObject dataflow = new DataflowMutableCore();
            dataflow.AgencyId = "SDMXSOURCE";
            dataflow.Id = id;
            dataflow.AddName("en", name);

            dataflow.DataStructureRef = dsd.AsReference;

            return dataflow.ImmutableInstance;
        }

        /// <summary>
        /// The build indicator codelist.
        /// </summary>
        /// <returns>
        /// The <see cref="ICodelistObject"/>.
        /// </returns>
        public ICodelistObject BuildIndicatorCodelist()
        {
            ICodelistMutableObject codelistMutable = new CodelistMutableCore();
            codelistMutable.AgencyId = "SDMXSOURCE";
            codelistMutable.Id = "CL_INDICATOR";
            codelistMutable.Version = "1.0";
            codelistMutable.AddName("en", "World Developement Indicators");

            ICodeMutableObject code;
            codelistMutable.CreateItem("E", "Environment");
            code = codelistMutable.CreateItem("E_A", "Agriculture land");
            code.ParentCode = "E";
            code = codelistMutable.CreateItem("E_P", "Population");
            code.ParentCode = "E";

            codelistMutable.CreateItem("H", "HEALTH");
            code = codelistMutable.CreateItem("H_B", "Birth Rate");
            code.ParentCode = "H";
            code = codelistMutable.CreateItem("H_C", "Children (0-14) living with HIV");
            code.ParentCode = "H";

            return codelistMutable.ImmutableInstance;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create codelist reference.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        private static IStructureReference CreateCodelistReference(string id)
        {
            return new StructureReferenceImpl("SDMXSOURCE", id, "1.0", SdmxStructureEnumType.CodeList);
        }

        /// <summary>
        /// The create concept reference.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IStructureReference"/>.
        /// </returns>
        private static IStructureReference CreateConceptReference(string id)
        {
            return new StructureReferenceImpl("SDMXSOURCE", "CONCEPTS", "1.0", SdmxStructureEnumType.Concept, id);
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="SampleDataRetrieval.cs" company="EUROSTAT">
//   Date Created : 2015-04-08
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of GuideTests.SdmxWebService.
// 
//     GuideTests.SdmxWebService is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     GuideTests.SdmxWebService is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.SdmxWebService.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.SdmxWebService
{
    using Chapter3WritingData;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

    /// <summary>
	/// Description of SampleDataRetrieval.
	/// </summary>
	public class SampleDataRetrieval : ISdmxDataRetrievalWithWriter
	{
	    public void GetData(IDataQuery dataQuery, IDataWriterEngine writerEngine) {
			// process query clauses
			// ...
			// write sample response of query results based on Chapter3 data writing
			SampleDataWriter writer = new SampleDataWriter();
			writer.WriteSampleData(dataQuery.DataStructure, dataQuery.Dataflow, writerEngine);
		}
	}
}

﻿// -----------------------------------------------------------------------
// <copyright file="TestRESTSdmxObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2015-07-02
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureRetrievalTests.
// 
//     SdmxStructureRetrievalTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureRetrievalTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrievalTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxStructureRetrievalTests
{
    using System.Collections.Generic;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Builder;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Test unit for <see cref="RESTSdmxObjectRetrievalManager"/>
    /// </summary>
    [TestFixture]
    public class TestRESTSdmxObjectRetrievalManager
    {

        /// <summary>
        /// Test unit for <see cref="RESTSdmxObjectRetrievalManager.GetMaintainableObjects(Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.IMaintainableRefObject,bool,bool)"/> 
        /// </summary>
        [Test]
        public void TestGetMaintainableObjects()
        {
            ISdmxObjectRetrievalManager retrievalManager = new RESTSdmxObjectRetrievalManager("https://registry.sdmx.org/ws/rest", new StructureQueryBuilderRest(), new StructureParsingManager(), new ReadableDataLocationFactory());
            ISet<ICodelistObject> maintainableObjects = retrievalManager.GetMaintainableObjects<ICodelistObject>(new MaintainableRefObjectImpl(null, "CL_FREQ", null), false, true);
            Assert.IsNotEmpty(maintainableObjects);
        }
    }
}
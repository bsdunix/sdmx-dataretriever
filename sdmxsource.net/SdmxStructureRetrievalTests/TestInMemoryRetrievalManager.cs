﻿// -----------------------------------------------------------------------
// <copyright file="TestInMemoryRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2015-05-15
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureRetrievalTests.
// 
//     SdmxStructureRetrievalTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureRetrievalTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrievalTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxStructureRetrievalTests
{
    using System.IO;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test unit for <see cref="InMemoryRetrievalManager"/>
    /// </summary>
    [TestFixture]
    public class TestInMemoryRetrievalManager
    {
        /// <summary>
        /// The _structure parsing manager
        /// </summary>
        private readonly StructureParsingManager _structureParsingManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestInMemoryRetrievalManager"/> class.
        /// </summary>
        public TestInMemoryRetrievalManager()
        {
            this._structureParsingManager = new StructureParsingManager();
        }

        /// <summary>
        /// Test unit for <see cref="InMemoryRetrievalManager.SaveStructure"/> 
        /// </summary>
        [Test]
        public void TestSaveStructure()
        {
            IDataflowMutableObject dataflow = new DataflowMutableCore { Id = "TEST_DF", AgencyId = "TEST_AGENCY", DataStructureRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_DSD", "1.0", SdmxStructureEnumType.Dsd) };
            dataflow.AddName("en", "A test DF");
            
            var retrievalManager = new InMemoryRetrievalManager();
            Assert.AreEqual(0, retrievalManager.GetMaintainableObjects<IDataflowObject>().Count);
            retrievalManager.SaveStructure(dataflow.ImmutableInstance);
            Assert.AreEqual(1, retrievalManager.GetMaintainableObjects<IDataflowObject>().Count);
        }

        /// <summary>
        /// Tests the get maintainable with rest query.
        /// </summary>
        /// <param name="structureFilePath">The structure file path.</param>
        /// <param name="restQuery">The rest query.</param>
        [TestCase(@"tests\v21\structures.xml", "datastructure/SDMXSOURCE/WDI/ALL?references=children")]
        public void TestGetMaintainablesRest(string structureFilePath, string restQuery)
        {
            ISdmxObjects objects = new FileInfo(structureFilePath).GetSdmxObjects(this._structureParsingManager);
            var retrievalManager = new InMemoryRetrievalManager(objects);
            ISdmxObjects sdmxObjects = retrievalManager.GetMaintainables(new RESTStructureQueryCore(restQuery));
            Assert.NotNull(sdmxObjects);
            Assert.IsNotEmpty(sdmxObjects.GetAllMaintainables());
        }
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="TestSubmitStructureResponseBuilder.cs" company="EUROSTAT">
//   Date Created : 2015-03-06
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParserTests.
// 
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxStructureParserTests
{
    using System.Globalization;
    using System.IO;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.XmlHelper;

    /// <summary>
    /// Test unit for <see cref="SubmitStructureResponseBuilder"/>
    /// </summary>
    [TestFixture]
    public class TestSubmitStructureResponseBuilder
    {
        /// <summary>
        /// Test unit for <see cref="SubmitStructureResponseBuilder.BuildSuccessResponse" />
        /// </summary>
        /// <param name="version">The version.</param>
        [TestCase(SdmxSchemaEnumType.VersionTwo)]
        [TestCase(SdmxSchemaEnumType.VersionTwoPointOne)]
        public void TestBuildSuccessResponse(SdmxSchemaEnumType version)
        {
            var responseBuilder = new SubmitStructureResponseBuilder();
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            var codelist = new CodelistMutableCore() { Id = "CL_TEST", Version = "1.0", AgencyId = "TEST" };
            codelist.AddName("en", "Test Codelist");
            for (int i = 0; i < 10; i++)
            {
                ICodeMutableObject item = new CodeMutableCore() { Id = "TEST_" + i.ToString(CultureInfo.InvariantCulture) };
                item.AddName("en", "Name for " + item.Id);
                codelist.AddItem(item);
            }

            sdmxObjects.AddCodelist(codelist.ImmutableInstance);
            var output = responseBuilder.BuildSuccessResponse(sdmxObjects, SdmxSchema.GetFromEnum(version));
            var fileName = "TestBuildSuccessResponse" + version + ".xml";
            output.Untyped.Save(fileName);
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(fileName))
            {
                XMLParser.ValidateXml(dataLocation, version);
            }
        }

        [TestCase(SdmxSchemaEnumType.VersionTwoPointOne, @"tests\v21\Structure\NA_PENS+ESTAT+1.0.xml")]
        public void TestBuildSuccessResponseFile(SdmxSchemaEnumType version, string file)
        {
            var responseBuilder = new SubmitStructureResponseBuilder();
            ISdmxObjects sdmxObjects;
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(file))
            {
                StructureParsingManager parsingManager = new StructureParsingManager();
                var structureWorkspace = parsingManager.ParseStructures(dataLocation);
                sdmxObjects = structureWorkspace.GetStructureObjects(false);
            }

            var output = responseBuilder.BuildSuccessResponse(sdmxObjects, SdmxSchema.GetFromEnum(version));
            var fileName = "TestBuildSuccessResponse" + version + ".xml";
            output.Untyped.Save(fileName);
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(fileName))
            {
                XMLParser.ValidateXml(dataLocation, version);
            }
        }

        /// <summary>
        /// Test unit for <see cref="SubmitStructureResponseBuilder.BuildSuccessResponse" />
        /// </summary>
        /// <param name="version">The version.</param>
        [TestCase(SdmxSchemaEnumType.VersionTwo)]
        [TestCase(SdmxSchemaEnumType.VersionTwoPointOne)]
        public void TestBuildErrorResponse(SdmxSchemaEnumType version)
        {
            var responseBuilder = new SubmitStructureResponseBuilder();
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            var codelist = new CodelistMutableCore() { Id = "CL_TEST", Version = "1.0", AgencyId = "TEST"};
            codelist.AddName("en", "Test Codelist");
            for (int i = 0; i < 10; i++)
            {
                ICodeMutableObject item = new CodeMutableCore() { Id = "TEST_" + i.ToString(CultureInfo.InvariantCulture) };
                item.AddName("en", "Name for " + item.Id);
                codelist.AddItem(item);
            }
            
            sdmxObjects.AddCodelist(codelist.ImmutableInstance);
            const string ErrorMessage = "Invalid syntax";
            var output = responseBuilder.BuildErrorResponse(new SdmxSyntaxException(ErrorMessage), new StructureReferenceImpl("TEST", "CL_TEST", "1.0", SdmxStructureEnumType.CodeList), SdmxSchema.GetFromEnum(version));
            var fileName = "TestBuildErrorResponse" + version + ".xml";
            output.Untyped.Save(fileName);
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(fileName))
            {
                XMLParser.ValidateXml(dataLocation, version);
            }

            Assert.IsTrue(File.ReadAllText(fileName).Contains(ErrorMessage));
        }
    }
}
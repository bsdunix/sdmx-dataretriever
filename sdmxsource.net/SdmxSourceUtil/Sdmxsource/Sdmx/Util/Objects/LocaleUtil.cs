// -----------------------------------------------------------------------
// <copyright file="LocaleUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The locale util.
    /// </summary>
    //// TODO Not used anywhere. Do we need this ?
    public class LocaleUtil
    {
        // TODO should this be user defined?
        #region Static Fields

        /// <summary>
        ///     The default locales.
        /// </summary>
        private static IList<CultureInfo> _defaultLocales = new List<CultureInfo> { CultureInfo.GetCultureInfo("en") };

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the default locales.
        /// </summary>
        public static IList<CultureInfo> DefaultLocales
        {
            get
            {
                return _defaultLocales;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The build local map.
        /// </summary>
        /// <param name="textTypes">
        /// The text types.
        /// </param>
        /// <returns>
        /// The local map
        /// </returns>
        public static IDictionary<CultureInfo, string> BuildLocalMap(IList<ITextTypeWrapper> textTypes)
        {
            if (textTypes == null)
            {
                return null;
            }

            IDictionary<CultureInfo, string> buildLocalMap = new Dictionary<CultureInfo, string>();

            /* foreach */
            foreach (ITextTypeWrapper currentTextType in textTypes)
            {
                string lang = currentTextType.Locale;
                string valueRen = currentTextType.Value;
                var loc = new CultureInfo(lang);
                buildLocalMap.Add(loc, valueRen);
            }

            return buildLocalMap;
        }

        /// <summary>
        /// The get string by default locale.
        /// </summary>
        /// <param name="localToStringMap">
        /// The local to string map.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// Throws ArgumentException
        /// </exception>
        public static string GetStringByDefaultLocale(IDictionary<CultureInfo, string> localToStringMap)
        {
            if (!ObjectUtil.ValidCollection(DefaultLocales))
            {
                throw new ArgumentException("No Default Locale found");
            }

            /* foreach */
            foreach (CultureInfo currentLocale in DefaultLocales)
            {
                string str;
                if (localToStringMap.TryGetValue(currentLocale, out str))
                {
                    if (string.IsNullOrWhiteSpace(str))
                    {
                        return str;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// The set default locale.
        /// </summary>
        /// <param name="defaultLocales0">
        /// The default locales_0.
        /// </param>
        public void SetDefaultLocale(IList<CultureInfo> defaultLocales0)
        {
            _defaultLocales = defaultLocales0;
        }

        #endregion
    }
}
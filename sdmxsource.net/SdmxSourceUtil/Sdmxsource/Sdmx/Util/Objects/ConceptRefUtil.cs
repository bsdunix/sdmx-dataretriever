// -----------------------------------------------------------------------
// <copyright file="ConceptRefUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Objects
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    #endregion


    /// <summary>
    /// The concept ref util.
    /// </summary>
    public class ConceptRefUtil
    {
        #region Public Methods and Operators

        /// <summary>
        /// The build concept ref.
        /// </summary>
        /// <param name="referencedFrom">
        /// The referenced from.
        /// </param>
        /// <param name="conceptSchemeAgency">
        /// The concept scheme agency.
        /// </param>
        /// <param name="conceptSchemeId">
        /// The concept scheme id.
        /// </param>
        /// <param name="conceptSchemeVersion">
        /// The concept scheme version1.
        /// </param>
        /// <param name="conceptAgency">
        /// The concept agency.
        /// </param>
        /// <param name="conceptId">
        /// The concept id.
        /// </param>
        /// <returns>
        /// The <see cref="ICrossReference"/>.
        /// </returns>
        /// <exception cref="ArgumentException">Throws ArgumentOutOfRangeException
        /// </exception>
        public static ICrossReference BuildConceptRef(
            IIdentifiableObject referencedFrom, 
            string conceptSchemeAgency, 
            string conceptSchemeId, 
            string conceptSchemeVersion, 
            string conceptAgency, 
            string conceptId)
        {
            bool isFreeStanding = string.IsNullOrWhiteSpace(conceptSchemeId);

            if (ObjectUtil.ValidOneString(conceptId, conceptSchemeAgency, conceptSchemeId, conceptSchemeVersion))
            {
                if (string.IsNullOrWhiteSpace(conceptSchemeAgency))
                {
                    conceptSchemeAgency = conceptAgency;
                }

                if (isFreeStanding)
                {
                    return new CrossReferenceImpl(
                        referencedFrom, 
                        conceptSchemeAgency, 
                        ConceptSchemeObject.DefaultSchemeId, 
                        ConceptSchemeObject.DefaultSchemeVersion, 
                        SdmxStructureEnumType.Concept, 
                        conceptId);
                }
                return new CrossReferenceImpl(
                    referencedFrom, 
                    conceptSchemeAgency, 
                    conceptSchemeId, 
                    conceptSchemeVersion, 
                    SdmxStructureEnumType.Concept, 
                    conceptId);
            }

            // TODO Exception
            throw new ArgumentException("Concept Reference missing parameters");
        }

        /// <summary>
        /// The get concept id.
        /// </summary>
        /// <param name="structureReference">
        /// The s ref.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Throws Validation Exception
        /// </exception>
        public static string GetConceptId(IStructureReference structureReference)
        {
            if (structureReference.TargetReference.EnumType == SdmxStructureEnumType.Concept)
            {
                return structureReference.ChildReference.Id;
            }

            throw new SdmxSemmanticException("Expecting a Concept Reference got : " + structureReference.TargetReference.GetType());
        }

        #endregion
    }
}
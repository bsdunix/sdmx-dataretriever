﻿// -----------------------------------------------------------------------
// <copyright file="CodedTimeDimensionAnnotationBuilder.cs" company="EUROSTAT">
//   Date Created : 2014-12-09
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Objects.Annotation
{
    using System.Globalization;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// The coded time dimension annotation builder.
    /// </summary>
    /// <typeparam name="TImpl">
    /// The concrete implementation of <see cref="IAnnotationMutableObject"/>
    /// </typeparam>
    public class CodedTimeDimensionAnnotationBuilder<TImpl> : IAnnotationBuilder<IMaintainableRefObject>
        where TImpl : IAnnotationMutableObject, new()
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds an <see cref="IAnnotationMutableObject"/> from the specified <paramref name="buildFrom"/>
        /// </summary>
        /// <param name="buildFrom">
        /// An Object to build the output object from
        /// </param>
        /// <returns>
        /// The <see cref="IAnnotableMutableObject"/>
        /// </returns>
        /// <exception cref="SdmxException">
        /// - If anything goes wrong during the build process
        /// </exception>
        public IAnnotationMutableObject Build(IMaintainableRefObject buildFrom)
        {
            var impl = new TImpl { Title = AnnotationConstant.CodeTimeDimensionTitle, Type = string.Format(CultureInfo.InvariantCulture, "{0}:{1}({2})", buildFrom.AgencyId, buildFrom.MaintainableId, buildFrom.Version) };
            impl.AddText("en", "The TimeDimension has coded representation. But SDMX v2.1 does not allow coded representation for TimeDimenions.");
            return impl;
        }

        #endregion
    }
}
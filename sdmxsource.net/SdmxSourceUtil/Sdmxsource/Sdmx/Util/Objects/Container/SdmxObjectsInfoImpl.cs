// -----------------------------------------------------------------------
// <copyright file="SdmxObjectsInfoImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Objects.Container
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects;

    /// <summary>
    /// The sdmx objects info impl.
    /// </summary>
    public class SdmxObjectsInfoImpl : ISdmxObjectsInfo
    {
        #region Fields

        /// <summary>
        /// The _agency metadata.
        /// </summary>
        private IList<IAgencyMetadata> _agencyMetadata;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxObjectsInfoImpl"/> class.
        /// </summary>
        public SdmxObjectsInfoImpl()
        {
            this._agencyMetadata = new List<IAgencyMetadata>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the agency metadata.
        /// </summary>
        public IList<IAgencyMetadata> AgencyMetadata
        {
            get
            {
                return this._agencyMetadata;
            }

            set
            {
                if (value == null)
                {
                    this._agencyMetadata = new List<IAgencyMetadata>();
                }
                else
                {
                    this._agencyMetadata = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the number maintainables.
        /// </summary>
        public int NumberMaintainables
        {
            get
            {
                int i = 0;

                /* foreach */
                foreach (IAgencyMetadata currentAgencyMetadata in this.AgencyMetadata)
                {
                    i += currentAgencyMetadata.NumberMaintainables;
                }

                return i;
            }

            set
            {
                // DO NOTHING - this is here for passing to external applications
            }
        }

        #endregion
    }
}
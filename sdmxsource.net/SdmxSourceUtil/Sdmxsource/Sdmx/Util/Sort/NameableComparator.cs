﻿// -----------------------------------------------------------------------
// <copyright file="NameableComparator.cs" company="EUROSTAT">
//   Date Created : 2013-03-25
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Sort
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// The Nameable Comparator.
    /// </summary>
    public class NameableComparator : IComparer<INameableObject>
    {
        /// <summary>
        /// Compares two nameables by name or by Urn
        /// </summary>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x"/> and <paramref name="y"/>, as shown in the following table.Value Meaning Less than zero<paramref name="x"/> is less than <paramref name="y"/>.Zero<paramref name="x"/> equals <paramref name="y"/>.Greater than zero<paramref name="x"/> is greater than <paramref name="y"/>.
        /// </returns>
        /// <param name="x">The first INameableObject to compare.</param>
        /// <param name="y">The second INameableObject to compare.</param>
        public int Compare(INameableObject x, INameableObject y)
        {
            string id1 = x.Id;
            string id2 = y.Id;

            int compare = String.Compare(id1, id2, System.StringComparison.Ordinal);

            if (compare == 0)
                return Uri.Compare(x.Urn, y.Urn, UriComponents.AbsoluteUri, UriFormat.SafeUnescaped, System.StringComparison.Ordinal);

            return compare;
        }
    }
}

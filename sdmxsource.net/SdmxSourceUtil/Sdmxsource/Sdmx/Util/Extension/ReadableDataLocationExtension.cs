﻿// -----------------------------------------------------------------------
// <copyright file="ReadableDataLocationExtension.cs" company="EUROSTAT">
//   Date Created : 2015-05-15
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Extension
{
    using System;
    using System.Diagnostics;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Utility extension methods for <see cref="IReadableDataLocation"/> and parsing SdmxObjecs in general.
    /// </summary>
    public static class ReadableDataLocationExtension
    {
        /// <summary>
        /// The _data location factory
        /// </summary>
        private static readonly IReadableDataLocationFactory _dataLocationFactory;

        /// <summary>
        /// Initializes static members of the <see cref="ReadableDataLocationExtension"/> class.
        /// </summary>
        static ReadableDataLocationExtension()
        {
            _dataLocationFactory = new ReadableDataLocationFactory();
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="dataLocation">The data location.</param>
        /// <param name="parsingManager">The parsing manager.</param>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        public static ISdmxObjects GetSdmxObjects(this IReadableDataLocation dataLocation, IStructureParsingManager parsingManager)
        {
            IStructureWorkspace structureWorkspace = parsingManager.ParseStructures(dataLocation);
            return structureWorkspace.GetStructureObjects(false);
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="parsingManager">The parsing manager.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        public static ISdmxObjects GetSdmxObjects(this Stream stream, IStructureParsingManager parsingManager)
        {
            using (IReadableDataLocation dataLocation = _dataLocationFactory.GetReadableDataLocation(stream))
            {
                return dataLocation.GetSdmxObjects(parsingManager);
            }
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="parsingManager">The parsing manager.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        public static ISdmxObjects GetSdmxObjects(this FileInfo file, IStructureParsingManager parsingManager)
        {
            using (IReadableDataLocation dataLocation = _dataLocationFactory.GetReadableDataLocation(file))
            {
                return dataLocation.GetSdmxObjects(parsingManager);
            }
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="url">The url.</param>
        /// <param name="parsingManager">The parsing manager.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        public static ISdmxObjects GetSdmxObjects(this Uri url, IStructureParsingManager parsingManager)
        {
            using (IReadableDataLocation dataLocation = _dataLocationFactory.GetReadableDataLocation(url))
            {
                return dataLocation.GetSdmxObjects(parsingManager);
            }
        }
    }
}
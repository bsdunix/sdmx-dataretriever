﻿// -----------------------------------------------------------------------
// <copyright file="DateFormat.cs" company="EUROSTAT">
//   Date Created : 2014-07-24
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Globalization;

    /// <summary>
    /// The date format.
    /// </summary>
    /// <remarks>Note the <see cref="DateFormat"/> is a very simple port from JDK DateFormat because no such class exists in .NET framework. It only supports Parse and Format.</remarks>
    public class DateFormat
    {
        #region Fields

        /// <summary>
        /// The _format string.
        /// </summary>
        private readonly string _formatString;

        /// <summary>
        /// The _invariant culture.
        /// </summary>
        private readonly CultureInfo _invariantCulture;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DateFormat"/> class.
        /// </summary>
        /// <param name="formatString">
        /// The format string.
        /// </param>
        public DateFormat(string formatString)
        {
            this._invariantCulture = CultureInfo.InvariantCulture;
            this._formatString = formatString;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Formats the specified input.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The specified <see cref="DateTime"/> as text.
        /// </returns>
        public string Format(DateTime input)
        {
            return input.ToString(this._formatString, this._invariantCulture);
        }

        /// <summary>
        /// Parses the specified input.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        public DateTime Parse(string input)
        {
            return DateTime.ParseExact(input, this._formatString, this._invariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal | DateTimeStyles.AllowWhiteSpaces);
        }

        #endregion
    }
}
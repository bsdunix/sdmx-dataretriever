// -----------------------------------------------------------------------
// <copyright file="PeriodicityHelper.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Utils for periodicity
    /// </summary>
    public static class PeriodicityHelper
    {
        #region Constants

        /// <summary>
        /// The format used in frequencies with prefix and contain two or more months in each period
        /// </summary>
        private const string PrefixFormatString = "{0:yyyy}-{1}{2}";

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Convert an SDMX Time Period type to a System.DateTime object
        /// </summary>
        /// <param name="sdmxPeriod">
        /// A string with the SDMX Time period 
        /// </param>
        /// <param name="months">
        /// The number of months in a period 
        /// </param>
        /// <param name="start">
        /// If it is true it will expand to the start of the period else towards the end e.g. if it is true 2001-04 will become 2001-04-01 else it will become 2001-04-30 
        /// </param>
        /// <param name="digitStart">
        /// The first digit in the period 
        /// </param>
        /// <returns>
        /// A <see cref="DateTime"/> object 
        /// </returns>
        public static DateTime ConvertToDateTime(string sdmxPeriod, byte months, bool start, byte digitStart)
        {
            var ret = new DateTime();
            if (!string.IsNullOrEmpty(sdmxPeriod))
            {
                string[] dateFields = sdmxPeriod.Split(new[] { '-' }, 3);
                if (dateFields.Length == 2)
                {
                    short period = Convert.ToInt16(dateFields[1].Substring(digitStart), CultureInfo.InvariantCulture);
                    short year = Convert.ToInt16(dateFields[0].Substring(0, 4), CultureInfo.InvariantCulture);
                    int day = 1;
                    int endMonth = 0;
                    if (start)
                    {
                        checked
                        {
                            endMonth = months - 1;
                        }
                    }

                    int month = (period * months) - endMonth;
                    if (!start)
                    {
                        day = DateTime.DaysInMonth(year, month);
                    }

                    ret = new DateTime(year, month, day);
                }
            }

            return ret;
        }

        /// <summary>
        /// Convert the specified <paramref name="time"/> to SDMX Time Period type representation
        /// </summary>
        /// <param name="time">
        /// The <see cref="DateTime"/> object to convert 
        /// </param>
        /// <param name="months">
        /// The number of months in a period 
        /// </param>
        /// <param name="prefix">
        /// The periodicity prefix 
        /// </param>
        /// <returns>
        /// A string with the SDMX Time Period 
        /// </returns>
        public static string ConvertToString(DateTime time, int months, char prefix)
        {
            IFormatProvider fmt = CultureInfo.InvariantCulture;
            return string.Format(fmt, PrefixFormatString, time, prefix, ((time.Month - 1) / months) + 1);
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="EdiTimeFormat.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    /// <summary>
    /// The GESMES (EDI) period code.
    /// </summary>
    public enum EdiTimeFormat
    {
        /// <summary>
        /// None (default)
        /// </summary>
        None = 0, 

        /// <summary>
        /// Monthly CCYYMM
        /// </summary>
        Month = 610, 

        /// <summary>
        /// Annual CCYY
        /// </summary>
        Year = 602, 

        /// <summary>
        /// Quarterly CCYYQ
        /// </summary>
        QuarterOfYear = 608, 

        /// <summary>
        /// Biannual CCYYB
        /// </summary>
        HalfOfYear = 604,

        /// <summary>
        /// Daily YYMMDD
        /// </summary>
        DailyTwoDigYear = 101, 

        /// <summary>
        /// Daily CCYYMMDD
        /// </summary>
        DailyFourDigYear = 102, 

        /// <summary>
        /// Weekly CCYYW
        /// </summary>
        Week = 616, 

        /// <summary>
        /// Up to minute detail YYDDHHMM
        /// </summary>
        MinuteTwoDigYear = 201,

        /// <summary>
        /// Up to minute detail CCYYMMDDHHMM (used in DTM+242 tag)
        /// </summary>
        MinuteFourDigYear = 203, 

        /// <summary>
        /// Monthly CCYYMM
        /// </summary>
        RangeMonthly = 710, 

        /// <summary>
        /// Annual CCYY
        /// </summary>
        RangeYear = 702, 

        /// <summary>
        /// Quarterly CCYYQ
        /// </summary>
        RangeQuarterOfYear = 708, 

        /// <summary>
        /// Biannual CCYYB
        /// </summary>
        RangeHalfOfYear = 704, 

        /// <summary>
        /// Daily CCYYMMDD
        /// </summary>
        RangeDaily = 711, 

        /// <summary>
        /// Weekly CCYYW
        /// </summary>
        RangeWeekly = 716
    }
}
// -----------------------------------------------------------------------
// <copyright file="PeriodicityFactory.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The periodicity factory.
    /// </summary>
    public static class PeriodicityFactory
    {
        /// <summary>
        /// The time format to periodicities dictionary
        /// </summary>
        private static readonly IDictionary<TimeFormatEnumType, IPeriodicity> _periodicities = new Dictionary<TimeFormatEnumType, IPeriodicity>();

        /// <summary>
        /// Initializes static members of the <see cref="PeriodicityFactory"/> class.
        /// </summary>
        static PeriodicityFactory()
        {
            _periodicities.Add(TimeFormatEnumType.Year, new Annual());
            _periodicities.Add(TimeFormatEnumType.Month, new Monthly());
            _periodicities.Add(TimeFormatEnumType.QuarterOfYear, new Quarterly());
            _periodicities.Add(TimeFormatEnumType.HalfOfYear, new Semester());
            _periodicities.Add(TimeFormatEnumType.Date, new Daily());
            _periodicities.Add(TimeFormatEnumType.DateTime, new Hourly());
            _periodicities.Add(TimeFormatEnumType.Hour, new Hourly());
            _periodicities.Add(TimeFormatEnumType.ThirdOfYear, new TriAnnual());
            _periodicities.Add(TimeFormatEnumType.Week, new Weekly());
        }

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="timeFormat">
        /// The time format.
        /// </param>
        /// <returns>
        /// The <see cref="IPeriodicity"/>.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">Throws ArgumentOutOfRangeException
        /// </exception>
        public static IPeriodicity Create(TimeFormatEnumType timeFormat)
        {
            IPeriodicity value;
            if (_periodicities.TryGetValue(timeFormat, out value))
            {
                return value;
            }

            throw new ArgumentOutOfRangeException("timeFormat", timeFormat, "Not supported periodicity.");
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="SchemaValidationException.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Exception
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    #endregion


    /// <summary>
    /// The schema validation exception.
    /// </summary>
    [Serializable]
    public class SchemaValidationException : SdmxSyntaxException
    {
        #region Constants

        /// <summary>
        /// The serial version1 uid.
        /// </summary>
        private const long SerialVersionUid = 1L;

        #endregion

        #region Fields

        private readonly IList<string> _validationErrors = new List<string>();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SchemaValidationException"/> class.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public SchemaValidationException(Exception args)
            : base(args)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="SchemaValidationException"/> class.
        /// </summary>
        /// <param name="errors">
        /// The errors
        /// </param>
        public SchemaValidationException(IList<string> errors)
            : base(MergeErrors(errors))
        {
		    foreach(string currentError in errors) 
            {
			    _validationErrors.Add(currentError);
		    }
	    }

        #endregion


        #region Public Methods and Operators

        /// <summary>
        /// Return the Validation error IList
        /// </summary>
        /// <returns>
        /// The IList of string
        /// </returns>
        public IList<string> GetValidationErrors()
        {
            return new List<string>(this._validationErrors);
        }

        #endregion


        #region Methods

        /// <summary>
        /// Return a string based on error list parameters
        /// </summary>
        /// <param name="errors">
        /// The list of errors
        /// </param>
        /// <returns>
        /// The error string
        /// </returns>
        private static string MergeErrors(IList<string> errors) {
		    var sb = new StringBuilder();

		    foreach(string currentError in errors) 
            {
			    sb.Append(currentError);
		    }

		    return sb.ToString();
	    }


        #endregion

    }
}
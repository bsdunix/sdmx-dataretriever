// -----------------------------------------------------------------------
// <copyright file="LoggingUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Log
{
    using log4net;

    /// <summary>
    ///     The logging util.
    /// </summary>
    /// TODO remove this as direct use of log4net is better.
    public class LoggingUtil
    {
        #region Public Methods and Operators

        /// <summary>
        /// The debug.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Debug(ILog log, string message)
        {
            if (log != null && log.IsDebugEnabled)
            {
                log.Debug(message);
            }
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Error(ILog log, string message)
        {
            if (log != null)
            {
                log.Error(message);
            }
        }

        /// <summary>
        /// The info.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Info(ILog log, string message)
        {
            if (log != null && log.IsInfoEnabled)
            {
                log.Info(message);
            }
        }

        /// <summary>
        /// The warn.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Warn(ILog log, string message)
        {
            if (log != null)
            {
                log.Warn(message);
            }
        }

        #endregion
    }
}
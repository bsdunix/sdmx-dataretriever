// -----------------------------------------------------------------------
// <copyright file="ReflectUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Reflect
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Reflection;

    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    /// The reflect util.
    /// </summary>
    /// <typeparam name="T">Generic type param
    /// </typeparam>
    public class ReflectUtil<T>
        where T : class
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets composite objects from the properties of <paramref name="inputObject"/> excluding <paramref name="ignoreProperties"/>
        /// </summary>
        /// <param name="inputObject">
        /// The input <see cref="object"/> . 
        /// </param>
        /// <param name="ignoreProperties">
        /// The ignored properties. 
        /// </param>
        /// <returns>
        ///  Returns a <see cref="ISet{T}"/> of the composite objects from the properties of <paramref name="inputObject"/> excluding <paramref name="ignoreProperties"/>
        /// </returns>
        public ISet<T> GetCompositeObjects(object inputObject, params PropertyInfo[] ignoreProperties)
        {
            ISet<T> returnSet = new HashSet<T>();
            Type referencedClass = typeof(T);

            var currentProperties = inputObject.GetType().GetPublicProperties();
            foreach (PropertyInfo currentProperty in currentProperties)
            {
                if (!Contains(currentProperty, ignoreProperties))
                {
                    MethodInfo currentMethod = currentProperty.GetGetMethod();
                    Type returnClass = currentMethod.ReturnType;

                    if (typeof(IEnumerable).IsAssignableFrom(returnClass))
                    {
                        Type returnType = currentMethod.ReturnType;
                        if (returnType.IsGenericType)
                        {
                            Type[] typeArguments = returnType.GetGenericArguments();

                            foreach (Type typeArgument in typeArguments)
                            {
                                if (referencedClass.IsAssignableFrom(typeArgument))
                                {
                                    var colection = (IEnumerable<T>)currentMethod.Invoke(inputObject, null);
                                    if (colection != null)
                                    {
                                        returnSet.UnionWith(colection);
                                    }

                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (referencedClass.IsAssignableFrom(returnClass))
                        {
                            var claz = (T)currentMethod.Invoke(inputObject, null);
                            if (claz != null && !returnSet.Contains(claz))
                            {
                                returnSet.Add(claz);
                            }
                        }
                    }
                }
            }

            return returnSet;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Checks if a property with the name of <paramref name="property"/> is contained in <paramref name="properties"/>
        /// </summary>
        /// <param name="property">
        /// The property. 
        /// </param>
        /// <param name="properties">
        /// The properties. 
        /// </param>
        /// <returns>
        /// True if a property with the name of <paramref name="property"/> is contained in <paramref name="properties"/> ; otherwise false 
        /// </returns>
        private static bool Contains(PropertyInfo property, IEnumerable<PropertyInfo> properties)
        {
            if (properties == null)
            {
                return false;
            }

            foreach (PropertyInfo currentProperty in properties)
            {
                if (currentProperty.Name.Equals(property.Name))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        ////public static bool IsGetter(MethodInfo method)
        ////{
        ////  if (!method.Name.StartsWith("get")) return false;
        ////  if (new ILOG.J2CsMapping.Reflect.IlrMethodInfoAdapter(method.GetParameters()).GetTypes().Length != 0) return false;
        ////  if (typeof(void).Equals(method.ReturnType)) return false;
        ////  return true;
        ////}
    }
}
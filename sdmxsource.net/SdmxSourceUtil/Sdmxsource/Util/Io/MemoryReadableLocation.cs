﻿// -----------------------------------------------------------------------
// <copyright file="MemoryReadableLocation.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System;
    using System.IO;
    using System.Net;

    /// <summary>
    /// Memory based buffer
    /// </summary>
    public class MemoryReadableLocation : BaseReadableDataLocation
    {
        #region Fields

        /// <summary>
        /// The buffer.
        /// </summary>
        private readonly byte[] _buffer;

        #endregion


        #region Public Properties

        /// <summary>
        /// Gets a guaranteed new input stream on each Property call.  
        /// The input stream will be reading the same underlying data source.
        /// </summary>
        public override Stream InputStream
        {
            get
            {
                var memoryStream = new MemoryStream(this._buffer);
                this.AddDisposable(memoryStream);
                return memoryStream;
            }
        }

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryReadableLocation"/> class.
        /// </summary>
        /// <param name="buffer">
        /// The buffer. 
        /// </param>
        public MemoryReadableLocation(byte[] buffer)
            : this(buffer, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryReadableLocation"/> class.
        /// </summary>
        /// <param name="buffer">
        /// The buffer. 
        /// </param>
        /// /// <param name="name">
        /// The name. 
        /// </param>
        public MemoryReadableLocation(byte[] buffer, string name)
        {
            this._buffer = buffer;
            this.Name = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryReadableLocation"/> class.
        /// </summary>
        /// <param name="uriStr">The URI string.</param>
        /// <exception cref="System.ArgumentNullException">Can not create StreamSourceData - uriStr can not be null</exception>
        public MemoryReadableLocation(string uriStr)
        {
            if (uriStr == null)
            {
                throw new ArgumentNullException("uriStr", "Can not create StreamSourceData - uriStr can not be null");
            }
            this.Name = uriStr;
            try
            {
                Uri uri = new Uri(uriStr);
                if (uri.IsAbsoluteUri)
                {
                    if (!uri.IsFile)
                    {
                        _buffer = StreamUtil.ToByteArray(URLUtil.GetInputStream(uri.AbsolutePath));
                    }
                    else
                    {
                        _buffer = FileUtil.ReadFileAsBytes(uri.LocalPath);
                    }
                }
            }
            catch (UriFormatException e)
            {
                Console.Error.WriteLine(e.StackTrace);
            }
            catch (WebException e)
            {
                Console.Error.WriteLine(e.StackTrace);
            }
        }
        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="BaseReadableDataLocation.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System;
    using System.Collections.Concurrent;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    /// The base readable data location.
    /// </summary>
    public abstract class BaseReadableDataLocation : IReadableDataLocation
    {
        #region Fields

        /// <summary>
        /// The disposables
        /// </summary>
        private readonly ConcurrentQueue<IDisposable> _disposables = new ConcurrentQueue<IDisposable>();

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a guaranteed new input stream on each Property call.  
        /// The input stream will be reading the same underlying data source.
        /// </summary>
        public abstract Stream InputStream { get; }

        /// <summary>
        /// Gets the name. If this ReadableDataLocation originated from a file, then this will be the original file name, regardless of where the stream is held.
        /// This may return null if it is not relevant.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; protected set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Closes (and removes if appropriate) any resources that are held open
        /// </summary>
        public void Close()
        {
            this.Dispose();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this); 
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a disposable.
        /// </summary>
        /// <param name="disposable">
        /// The disposable. 
        /// </param>
        protected void AddDisposable(IDisposable disposable)
        {
            this._disposables.Enqueue(disposable);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="dispose">
        /// If set to true managed resources will be disposed as well. 
        /// </param>
        /// <filterpriority>2</filterpriority>
        protected virtual void Dispose(bool dispose)
        {
            if (dispose)
            {
                while (this._disposables.Count > 0)
                {
                    IDisposable disposable;
                    if (this._disposables.TryDequeue(out disposable))
                    {
                        disposable.Dispose();
                    }
                }
            }
        }

        #endregion
    }
}
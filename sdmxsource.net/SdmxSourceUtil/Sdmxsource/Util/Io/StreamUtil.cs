// -----------------------------------------------------------------------
// <copyright file="StreamUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    /// <summary>
    /// The stream util.
    /// </summary>
    public class StreamUtil
    {
        // HACK - THIS IS FOR INPUT STREAMS THAT ARE NOT UTF*, BUT IF THE STREAM IS UTF- THIS METHOD BREAKS THE ENCODING 
        // BY CONVERTING AN INT TO A CHAR
        /*
        private static void CopyNonUTF8InputStream(Stream inputStream, Stream outputStream) {
            TextWriter bos = null;
            BufferedStream bis = null;
            try {
    
                bis = new BufferedStream(inputStream);
                bos = ILOG.J2CsMapping.IO.IOUtility.NewStreamWriter(new BufferedStream(outputStream),System.Text.Encoding.GetEncoding("UTF-8"));
    
                byte[] bytes = new byte[1024];
                int i;
                while ((i = bis.Read(bytes,0,bytes.Length)) > 0) {
                    bos.Write(new String(bytes, 0, i));
                }
                bos.Flush();
    
            } catch (Exception th) {
                throw new Exception(th.Message, th);
            } finally {
                try {
                    inputStream.Close();
                    outputStream.Flush();
                    outputStream.Close();
                    if (bos != null) {
                        bos.Close();
                    }
                    if (bis != null) {
                        bis.Close();
                    }
                } catch (Exception th_0) {
                    throw new Exception(th_0.Message, th_0);
                }
            }
        }
*/
        #region Public Methods and Operators

        /// <summary>
        /// Closes all of the supplied InputStreams.
        /// </summary>
        /// <param name="inputStreams">
        /// The input streams 
        /// </param>
        public static void CloseStream(params Stream[] inputStreams)
        {
            if (inputStreams == null)
            {
                return;
            }

            foreach (Stream currentIn in inputStreams)
            {
                currentIn.Close();
            }
        }

        /// <summary>
        /// Copies the first 'x' number of lines to a list, each list element represents a line.
        /// </summary>
        /// <param name="stream">
        /// The input stream 
        /// </param>
        /// <param name="numLines">
        /// The number of lines 
        /// </param>
        /// <returns>
        /// the first 'x' number of lines to a list, each list element represents a line. 
        /// </returns>
        public static IList<string> CopyFirstXLines(Stream stream, int numLines)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            TextReader reader = new StreamReader(stream);
            string line;
            var firstXRows = new List<string>();
            while (firstXRows.Count < numLines && (line = reader.ReadLine()) != null)
            {
                firstXRows.Add(line);
            }

            return firstXRows;
        }

        /// <summary>
        /// Copies the supplied <paramref name="inputStream"/> to the supplied <paramref name="outputStream"/>.
        /// Converts the OutputStream to UTF-8.
        /// Both streams are closed on completion, uses a buffer of 1Kb
        /// </summary>
        /// <param name="inputStream">
        /// The input stream. 
        /// </param>
        /// <param name="outputStream">
        /// the OutputStream to write to. 
        /// </param>
        public static void CopyStream(Stream inputStream, Stream outputStream)
        {
            if (inputStream == null)
            {
                throw new ArgumentNullException("inputStream");
            }

            if (outputStream == null)
            {
                throw new ArgumentNullException("outputStream");
            }

            using (inputStream)
            {
                using (outputStream)
                {
                    //// TODO 1024 is smallish 
                    inputStream.CopyTo(outputStream, 1024);
                    outputStream.Flush();
                }
            }
        }

        /// <summary>
        /// Create a byte[] from the supplied InputStream.
        /// The InputStream is closed after use.
        /// </summary>
        /// <param name="inputStream">
        /// The input stream. 
        /// </param>
        /// <returns>
        /// An array of <see cref="byte"/>
        /// </returns>
        public static byte[] ToByteArray(Stream inputStream)
        {
            var bytes = new byte[1024];
            using (inputStream)
            using (var bos = new MemoryStream())
            {
                int i;
                while ((i = inputStream.Read(bytes, 0, bytes.Length)) > 0)
                {
                    bos.Write(bytes, 0, i);
                }

                bos.Flush();
                return bos.ToArray();
            }
        }

        #endregion
    }
}
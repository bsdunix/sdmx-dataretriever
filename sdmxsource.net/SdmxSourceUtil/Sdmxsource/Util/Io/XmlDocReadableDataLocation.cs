﻿// -----------------------------------------------------------------------
// <copyright file="XmlDocReadableDataLocation.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System.IO;
    using System.Text;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    /// A <see cref="IReadableDataLocation" /> implementation that reads data from a
    /// </summary>
    public class XmlDocReadableDataLocation : BaseReadableDataLocation
    {
        #region Fields

        /// <summary>
        /// The contents of the input document <see cref="XmlNode" /> or <see cref="XmlDocument" /> document
        /// </summary>
        private readonly byte[] _document;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlDocReadableDataLocation"/> class.
        /// </summary>
        /// <param name="document">
        /// The input <see cref="XmlNode"/> or <see cref="XmlDocument"/> document. 
        /// </param>
        public XmlDocReadableDataLocation(XmlNode document)
        {
            this._document = Encoding.UTF8.GetBytes(document.OuterXml);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// This methods is guaranteed to return a new input stream on each method call.  
        /// The input stream will be reading the same underlying data source.
        /// </summary>
        public override Stream InputStream
        {
            get
            {
                var stream = new MemoryStream(this._document);
                this.AddDisposable(stream);
                return stream;
            }
        }

        #endregion
    }
}
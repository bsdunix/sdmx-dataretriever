﻿// -----------------------------------------------------------------------
// <copyright file="ISdmxXmlStream.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The interface used for XML streaming and carrying SDMX-ML message information
    /// </summary>
    /// <example>
    ///     A sample implementation in C# of <see cref="ISdmxXmlStream" />.
    ///     <code source="..\ReUsingExamples\Structure\ReUsingStructureParsingManagerFast.cs" lang="cs" />
    /// </example> 
    public interface ISdmxXmlStream : IDisposable
    {
        #region Public Properties

        /// <summary>
        ///     Gets a value indicating whether it has reader.
        /// </summary>
        bool HasReader { get; }

        /// <summary>
        ///     Gets a value indicating whether it has writer.
        /// </summary>
        bool HasWriter { get; }

        /// <summary>
        ///     Gets the message type.
        /// </summary>
        MessageEnumType MessageType { get; }

        /// <summary>
        ///     Gets the query message type.
        /// </summary>
        IList<QueryMessageEnumType> QueryMessageTypes { get; }

        /// <summary>
        ///     Gets the reader.
        /// </summary>
        XmlReader Reader { get; }

        /// <summary>
        ///     Gets the registry type.
        /// </summary>
        RegistryMessageEnumType RegistryType { get; }

        /// <summary>
        ///     Gets the schema enum type.
        /// </summary>
        SdmxSchemaEnumType SdmxVersion { get; }

        /// <summary>
        ///     Gets the writer.
        /// </summary>
        XmlWriter Writer { get; }

        #endregion
    }
}
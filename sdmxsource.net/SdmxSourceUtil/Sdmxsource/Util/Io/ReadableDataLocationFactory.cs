﻿// -----------------------------------------------------------------------
// <copyright file="ReadableDataLocationFactory.cs" company="EUROSTAT">
//   Date Created : 2013-02-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Util;

    public class ReadableDataLocationFactory : IReadableDataLocationFactory
    {
        /// <summary>
        /// Create a readable data location from a String, this may represent a URI (file or URL) 
        /// </summary>
        /// <param name="uriStr"></param>
        /// <returns></returns>
        public IReadableDataLocation GetReadableDataLocation(string uriStr)
        {
            return new ReadableDataLocationTmp(uriStr);
        }

        /// <summary>
        /// Create a readable data location from a String, this may represent a URI (file or URL) 
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public IReadableDataLocation GetReadableDataLocation(byte[] bytes)
        {
            return new MemoryReadableLocation(bytes);
        }

        /// <summary>
        /// Create a readable data location from a String, this may represent a URI (file or URL) 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public IReadableDataLocation GetReadableDataLocation(FileInfo file)
        {
            return new FileReadableDataLocation(file);
        }

        /// <summary>
        /// Create a readable data location from a String, this may represent a URI (file or URL) 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public IReadableDataLocation GetReadableDataLocation(Uri url)
        {
            return new ReadableDataLocationTmp(url);
        }

        /// <summary>
        /// Create a readable data location from a String, this may represent a URI (file or URL) 
        /// </summary>
        /// <param name="streamReader"></param>
        /// <returns></returns>
        public IReadableDataLocation GetReadableDataLocation(Stream streamReader)
        {
            return new ReadableDataLocationTmp(streamReader);
        }
    }
}
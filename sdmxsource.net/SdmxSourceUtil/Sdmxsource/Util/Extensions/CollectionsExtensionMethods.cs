﻿// -----------------------------------------------------------------------
// <copyright file="CollectionsExtensionMethods.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Extensions
{
    using System;
    using System.Collections.Generic;

    /// <summary> Extension methods
    /// </summary>
    public static class CollectionsExtensionMethods
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add all.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <param name="items">
        /// The items.
        /// </param>
        /// <typeparam name="T">Generic type param
        /// </typeparam>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException
        /// </exception>
        public static void AddAll<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items", "New collection cannot be null");
            }

            if (collection == null)
            {
                throw new ArgumentNullException("collection", "Parameter cannot be null");
            }

            foreach (T item in items)
            {
                collection.Add(item);
            }
        }

        /// <summary>
        /// The remove item list.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <param name="items">
        /// The items.
        /// </param>
        /// <typeparam name="T">Generic type param
        /// </typeparam>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException
        /// </exception>
        public static void RemoveItemList<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items", "New collection cannot be null");
            }

            if (collection == null)
            {
                throw new ArgumentNullException("collection", "Parameter cannot be null");
            }

            foreach (T item in items)
            {
                collection.Remove(item);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="items"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static bool ContainsAll<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items", "New collection cannot be null");
            }

            if (collection == null)
            {
                throw new ArgumentNullException("collection", "Parameter cannot be null");
            }

            foreach (T item in items)
            {
                if (!collection.Contains(item))
                    return false;
            }

            return true;
        }



        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="UriExtensionMethods.cs" company="EUROSTAT">
//   Date Created : 2014-03-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Url
{
    using System;

    /// <summary>
    /// The URI extension methods.
    /// </summary>
    public static class UriExtensionMethods
    {
        /// <summary>
        /// Convert to URI.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="Uri"/>.
        /// </returns>
        public static Uri ToUri(this string value)
        {
            return !string.IsNullOrWhiteSpace(value) ? new Uri(value) : null;
        }
    }
}
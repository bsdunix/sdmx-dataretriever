﻿// -----------------------------------------------------------------------
// <copyright file="MessageDecoder.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.ResourceBundle
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Resources;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    #endregion

    /// <summary>
    /// TODO
    /// </summary>
    public class MessageDecoder : IMessageResolver
    {
        #region Static Fields

        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(MessageDecoder));

        private static ResourceManager _messageSource;

        private static CultureInfo _loc = new CultureInfo("en");

        #endregion


        #region Public Methods and Operators

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDecoder"/> class
        /// </summary>
        public MessageDecoder() 
        {
            SdmxException.SetMessageResolver(this);

            _messageSource = ExceptionMessages.ResourceManager;

        }

        #endregion


        #region Public Properties

        /// <summary>
        /// The _messageSource
        /// </summary>
        public static ResourceManager MessageSource
        {
            get
            {
                return _messageSource;
            }
        }

        #endregion


        #region Public Methods and Operators

        /// <summary>
        /// Add a new base name
        /// </summary>
        /// <param name="baseName">
        /// The base name
        /// </param>
        public static void AddBaseName(string baseName) 
        {
        }	

        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="id">
        /// The id
        /// </param>
        /// <param name="args">
        /// The args
        /// </param>
        /// <returns>
        /// The message
        /// </returns>
        public static string DecodeMessage(string id, params object[] args) 
        {

            if(_messageSource == null) 
            {
                return id;
            }

            return string.Format(GetString(id, _loc), args);
        }

        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="id">
        /// The id
        /// </param>
        /// <param name="args">
        /// The args
        /// </param>
        /// <returns>
        /// The message
        /// </returns>
        public static string DecodeMessageDefaultLocale(string id, params object[] args) 
        {
            return string.Format(GetString(id, _loc), args);
        }

        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="id">
        /// The id
        /// </param>
        /// <param name="lang">
        /// The lang
        /// </param>
        /// <param name="args">
        /// The args
        /// </param>
        /// <returns>
        /// The message
        /// </returns>
        public static string DecodeMessageGivenLocale(string id, string lang, params object[] args) 
        {

            if(_messageSource == null) 
            {
                return id;
            }

            var format = GetString(id, _loc);
            return string.Format(format, args);
        }

        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="messageCode">
        /// The message code
        /// </param>
        /// <param name="locale">
        /// The locale
        /// </param>
        /// <param name="args">
        /// The args
        /// </param>
        /// <returns>
        /// The message
        /// </returns>
        public string ResolveMessage(string messageCode, CultureInfo locale, params object[] args)
        {
            string format = null;
            try
            {
                format = GetString(messageCode, locale);
                if (args != null && args.Length > 0)
                {
                    if (format != null)
                    {
                        return string.Format(format, args);
                    }

                    return messageCode + " - " + string.Join(" - ", args.Where(o => o != null).Select(o => o.ToString()));
                }
                
                if (format != null)
                {
                    return format;
                }

                return messageCode;
            }
            catch (FormatException th)
            {
                _log.ErrorFormat(CultureInfo.InvariantCulture, "{0} - {1} - {2} - {3}", messageCode, format, locale, args);
                _log.Error("While ResolveMessage", th);
                return string.Format("{0} - {1}", messageCode, format ?? string.Empty);
            }
            catch (Exception th) 
            {
                _log.ErrorFormat(CultureInfo.InvariantCulture, "{0} - {1} - {2} - {3}", messageCode, format, locale, args);
                _log.Error("While ResolveMessage", th);
                return messageCode;
            }
        }

        /// <summary>
        /// Add new basenames in the basename list
        /// </summary>
        /// <param name="basenames">
        /// The base names
        /// </param>
        public void SetBasenames(ISet<string> basenames) 
        {
        }

        #endregion

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <param name="messageCode">The message code.</param>
        /// <param name="locale">The locale.</param>
        /// <returns>the message for this locale;otherwise null.</returns>
        private static string GetString(string messageCode, CultureInfo locale)
        {
            var name = "ID" + messageCode;
            if (locale == null)
            {
                return ExceptionMessages.ResourceManager.GetString(name);
            }

            return ExceptionMessages.ResourceManager.GetString(name, locale);
        }
    }
}


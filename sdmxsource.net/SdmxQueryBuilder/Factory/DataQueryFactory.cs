﻿// -----------------------------------------------------------------------
// <copyright file="DataQueryFactory.cs" company="EUROSTAT">
//   Date Created : 2013-05-27
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxQueryBuilder.
// 
//     SdmxQueryBuilder is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxQueryBuilder is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxQueryBuilder.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Factory
{
    #region Using Directives

    using System.Xml.Linq;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Builder;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Model;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2;

    #endregion

    /// <summary>
    /// The data query factory implementation.
    /// </summary>
    public class DataQueryFactory : IDataQueryFactory
    {
        #region Constants

        /// <summary>
        ///     The unknown id.
        /// </summary>
        private const string UnknownId = "UNKNOWN";

        #endregion


        #region Fields

        private readonly DataQueryBuilderRest _dataQueryBuilderRest = new DataQueryBuilderRest();

        /// <summary>
        /// The _default header
        /// </summary>
        private readonly IHeader _defaultHeader;

        #endregion


        /// <summary>
        /// Initializes a new instance of the <see cref="DataQueryFactory"/> class.
        /// </summary>
        /// <param name="defaultHeader">The default header.</param>
        public DataQueryFactory(IHeader defaultHeader)
        {
            //// TODO header factory.
            this._defaultHeader = defaultHeader;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataQueryFactory"/> class.
        /// </summary>
        public DataQueryFactory()
        {
            this._defaultHeader = new HeaderImpl(UnknownId, UnknownId);
        }

        #region Public Methods and Operators

        /// <summary>
        /// Returns a DataQueryBuilder only if this factory understands the DataQueryFormat.  If the format is unknown, null will be returned
        /// </summary>
        /// <typeparam name="T">
        /// Generic type parameter
        /// </typeparam>
        /// <param name="format">
        /// Format
        /// </param>
        /// <returns>
        /// DataQueryBuilder is this factory knows how to build this query format, or null if it doesn't
        /// </returns>
        public IDataQueryBuilder<T> GetDataQueryBuilder<T>(IDataQueryFormat<T> format)
        {
            if (format is RestQueryFormat)
            {
                return (IDataQueryBuilder<T>)_dataQueryBuilderRest;
            }

            if (format is QueryMessageV2Format)
            {
                IDataQueryBuilder<XDocument> dataQueryBuilder =
                    new XDocumentDataQueryBuilderV2(this._defaultHeader, new StructureHeaderXmlBuilder());
                return dataQueryBuilder as IDataQueryBuilder<T>;
            }

            return null;
        }

        #endregion
    }
}

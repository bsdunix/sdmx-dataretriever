﻿// -----------------------------------------------------------------------
// <copyright file="RestStructureQueryFactory.cs" company="EUROSTAT">
//   Date Created : 2013-05-17
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxQueryBuilder.
// 
//     SdmxQueryBuilder is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxQueryBuilder is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxQueryBuilder.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Factory
{

    #region Using Directives

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Builder;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Model;

    #endregion

    /// <summary>
    /// If the required format is RestQueryFormat, then will returns a builder that can build REST
    /// </summary>
    public class RestStructureQueryFactory : IStructureQueryFactory
    {
        #region Fields

        private readonly StructureQueryBuilderRest _structureQueryBuilderRest = new StructureQueryBuilderRest();

        #endregion


        #region Public Methods and Operators

        /// <summary>
        /// Returns a StructureQueryBuilder only if this factory understands the StructureQueryFormat.  If the format is unknown, null will be returned
        /// </summary>
        /// <typeparam name="T">generic type parameter</typeparam>
        /// <param name="format">Format</param>
        /// <returns>StructureQueryBuilder is this factory knows how to build this query format, or null if it doesn't</returns>
        public IStructureQueryBuilder<T> GetStructureQueryBuilder<T>(IStructureQueryFormat<T> format)
        {
		    if(format is RestQueryFormat) {
			    return (IStructureQueryBuilder<T>)_structureQueryBuilderRest;
		    }
		    return null;
        }

        #endregion
    }
}

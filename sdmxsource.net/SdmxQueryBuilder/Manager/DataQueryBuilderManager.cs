﻿// -----------------------------------------------------------------------
// <copyright file="DataQueryBuilderManager.cs" company="EUROSTAT">
//   Date Created : 2013-05-27
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxQueryBuilder.
// 
//     SdmxQueryBuilder is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxQueryBuilder is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxQueryBuilder.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Manager
{
    #region Using Directives

    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Factory;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    /// TODO
    /// </summary>
    public class DataQueryBuilderManager : IDataQueryBuilderManager
    {
        #region Fields

        private readonly IDataQueryFactory[] _dataDataQueryFactory;

        #endregion


        #region Constructors and Destructors

        /// <summary>
        /// Set the data query factory
        /// </summary>
        /// <param name="dataQueryFactory">
        /// The data query factory
        /// </param>
        public DataQueryBuilderManager(params IDataQueryFactory[] dataQueryFactory)
        {
            this._dataDataQueryFactory = ObjectUtil.ValidArray(dataQueryFactory) ? dataQueryFactory : new IDataQueryFactory[] { new DataQueryFactory(), };
        }

        #endregion


        #region Public Methods and Operators

        /// <summary>
        /// Builds a data query in the requested format
        /// </summary>
        /// <typeparam name="T">
        /// Generic type parameter.
        /// </typeparam>
        /// <param name="dataQuery">
        /// The query to build a representation of
        /// </param>
        /// <param name="dataQueryFormat">
        /// The required format
        /// </param>
        /// <returns>
        /// Representation of query in the desired format.
        /// </returns>
        public T BuildDataQuery<T>(IDataQuery dataQuery, IDataQueryFormat<T> dataQueryFormat)
        {
            foreach (var dataQueryFactory in this._dataDataQueryFactory)
            {
                var builder = dataQueryFactory.GetDataQueryBuilder(dataQueryFormat);
                if (builder != null)
                {
                    return builder.BuildDataQuery(dataQuery);
                }
            }

            return default(T);
        }

        #endregion

    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TestSdmxException.cs" company="EUROSTAT">
//   Date Created : 2014-04-25
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxApiTests.
// 
//     SdmxApiTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxApiTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxApiTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxApiTests
{
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Util.ResourceBundle;

    /// <summary>
    /// Test unit for <see cref="SdmxException"/>
    /// </summary>
    [TestFixture]
    public class TestSdmxException
    {

        /// <summary>
        /// Test unit for <see cref="SdmxException.Message"/> 
        /// </summary>
        [Test]
        public void TestMessage()
        {
            var exception = new SdmxNotImplementedException(ExceptionCode.Unsupported, "Any");
            Assert.AreEqual("Not Implemented - 405", exception.Message);
        }

        /// <summary>
        /// Test unit for <see cref="SdmxException.Message"/> 
        /// </summary>
        [Test]
        public void TestMessageNoArgs()
        {
            var exception = new SdmxNotImplementedException();
            Assert.AreEqual("Not Implemented", exception.Message);
        }

        /// <summary>
        /// Test unit for <see cref="SdmxException.Message"/> 
        /// </summary>
        [Test]
        public void TestMessageResolver()
        {
            var exception = new SdmxNotImplementedException(ExceptionCode.Unsupported, "Any");
            SdmxException.SetMessageResolver(new MessageDecoder());
            Assert.AreEqual("Not Implemented - Unsupported Any", exception.Message);
        }

        /// <summary>
        /// Test unit for <see cref="SdmxException.Message"/> 
        /// </summary>
        [Test]
        public void TestMessageResolver1Arg()
        {
            var exception = new SdmxNotImplementedException("Any");
            SdmxException.SetMessageResolver(new MessageDecoder());
            Assert.AreEqual("Any", exception.Message);
        }

        /// <summary>
        /// Test unit for <see cref="SdmxException.Message"/> 
        /// </summary>
        [Test]
        public void TestMessageResolverNoArgs()
        {
            var exception = new SdmxNotImplementedException();
            SdmxException.SetMessageResolver(new MessageDecoder());
            Assert.AreEqual("Not Implemented", exception.Message);
        }
        /// <summary>
        /// Test unit for <see cref="SdmxException.Message"/> 
        /// </summary>
        [Test]
        public void TestMessageResolverWrongArgs()
        {
            var exception = new SdmxNotImplementedException(ExceptionCode.ReferenceErrorUnexpectedResultsCount, "Any");
            SdmxException.SetMessageResolver(new MessageDecoder());
            Assert.AreEqual("Not Implemented - 607 - Unexpected number of results for {0} with arguments {1} expected {2} got {3} ", exception.Message);
        }
    }
}
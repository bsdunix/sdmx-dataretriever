﻿// -----------------------------------------------------------------------
// <copyright file="DataReaderManager.cs" company="EUROSTAT">
//   Date Created : 2014-07-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Factory;
    using Org.Sdmxsource.Sdmx.EdiParser.Manager;

    /// <summary>
    ///     The data reader manager.
    /// </summary>
    public class DataReaderManager : IDataReaderManager
    {
        #region Fields

        /// <summary>
        ///     The _engines
        /// </summary>
        private readonly IDataReaderFactory[] _engines;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataReaderManager"/> class.
        /// </summary>
        /// <param name="engines">
        ///     The engines.
        /// </param>
        public DataReaderManager(params IDataReaderFactory[] engines)
        {
            if (engines == null || engines.Length == 0)
            {
                this._engines = new IDataReaderFactory[] { new SdmxDataReaderFactory(new DataInformationManager(), new EdiParseManager()) };
            }
            else
            {
                this._engines = engines;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Obtains a DataReaderEngine that is capable of reading the data which is exposed via the ReadableDataLocation
        /// </summary>
        /// <param name="sourceData">
        /// SourceData - giving access to an InputStream of the data
        /// </param>
        /// <param name="dsd">
        /// Describes the data in terms of the dimensionality
        /// </param>
        /// <param name="dataflowObject">
        /// The data flow object (optional)
        /// </param>
        /// <returns>
        /// The data reader engine
        /// </returns>
        /// throws SdmxNotImplementedException if ReadableDataLocation or DataStructureBean is null, also if additioanlInformation is not of the expected type
        public IDataReaderEngine GetDataReaderEngine(IReadableDataLocation sourceData, IDataStructureObject dsd, IDataflowObject dataflowObject)
        {
            foreach (var currentFactory in this._engines)
            {
                IDataReaderEngine dre = currentFactory.GetDataReaderEngine(sourceData, dsd, dataflowObject);
                if (dre != null)
                {
                    return dre;
                }
            }

            throw new SdmxNotImplementedException("Data format is either not supported, or has an invalid syntax");
        }

        /// <summary>
        /// Obtains a DataReaderEngine that is capable of reading the data which is exposed via the ReadableDataLocation
        /// </summary>
        /// <param name="sourceData">
        /// SourceData - giving access to an InputStream of the data
        /// </param>
        /// <param name="retrievalManager">
        /// RetrievalManager - used to obtain the DataStructure(s) that describe the data
        /// </param>
        /// <returns>
        /// The data reader engine
        /// </returns>
        /// throws SdmxNotImplementedException if ReadableDataLocation or DataStructureBean is null, also if additioanlInformation is not of the expected type
        public IDataReaderEngine GetDataReaderEngine(IReadableDataLocation sourceData, ISdmxObjectRetrievalManager retrievalManager)
        {
            foreach (var currentFactory in this._engines)
            {
                IDataReaderEngine dre = currentFactory.GetDataReaderEngine(sourceData, retrievalManager);
                if (dre != null)
                {
                    return dre;
                }
            }

            throw new SdmxNotImplementedException("Data format is either not supported, or has an invalid syntax");
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="IDataWriterManager.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    #region Using directives

    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    #endregion

    /// <summary>
    /// The data writer manager 
    /// </summary>
    public interface IDataWriterManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Returns a {@link DataWriterEngine} that can write the given {@link DATA_TYPE}.
        /// </summary>
        /// <param name="dataFormat">
        /// DataFormat defines the data format to write the output in
        /// </param>
        /// <param name="outPutStream">
        /// The {@link OutputStream} to write the data to
        /// </param>
        /// <returns>
        /// The data writer engine
        /// </returns>
        /// throws SdmxNotImplementedException if no {@link DataWriterEngine} could be found to write the requested data
        IDataWriterEngine GetDataWriterEngine(IDataFormat dataFormat, Stream outPutStream);

        #endregion
    }
}

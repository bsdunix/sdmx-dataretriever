﻿// -----------------------------------------------------------------------
// <copyright file="IDataParseManager.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    #region Using directives

    using System.Collections.Generic;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;

    #endregion

    /// <summary>
    ///     The data Manager is responsible for transforming data to adhere to one SMDX schema type to another.
    /// </summary>
    public interface IDataParseManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Performs a transform from one data format to another data format.
        /// </summary>
        /// <param name="sourceData">
        /// The location of the input stream which reads the data to transform
        /// </param>
        /// <param name="outPutStream">
        /// OutputStream to write the transform results to
        /// </param>
        /// <param name="dataFormat">
        /// The format to transform to
        /// </param>
        /// <param name="retrievalManager">
        /// RetrievalManager used to get any data structures needed to interpret the data
        /// </param>
        void PerformTransform(IReadableDataLocation sourceData, Stream outPutStream, IDataFormat dataFormat, ISdmxObjectRetrievalManager retrievalManager);

        /// <summary>
        /// Performs a transform from one data format to another data format.
        /// </summary>
        /// <param name="sourceData">
        /// The location of the input stream which reads the data to transform
        /// </param>
        /// <param name="outPutStream">
        /// OutputStream to write the transform results to
        /// </param>
        /// <param name="dataFormat">
        /// The format to transform to
        /// </param>
        /// <param name="dsd">
        /// The Structure object that specifies the data format
        /// </param>
        /// <param name="dataflowObject">
        /// DataflowObject (optional) provides extra information about the data
        /// </param>
        void PerformTransform(IReadableDataLocation sourceData, Stream outPutStream, IDataFormat dataFormat, IDataStructureObject dsd, IDataflowObject dataflowObject);

        /// <summary>
        /// Performs a transform from one data format to another data format.
        /// </summary>
        /// <param name="sourceData">
        /// The location of the input stream which reads the data to transform
        /// </param>
        /// <param name="dsdLocation">
        /// The location of the structure used to transform the data
        /// </param>
        /// <param name="outPutStream">
        /// OutputStream to write the transform results to
        /// </param>
        /// <param name="dataFormat">
        /// The format to transform to
        /// </param>
        void PerformTransform(IReadableDataLocation sourceData, IReadableDataLocation dsdLocation, Stream outPutStream, IDataFormat dataFormat);

        /// <summary>
        /// Performs a transform from one data format to another data format.
        /// </summary>
        /// <param name="sourceData">
        /// SourceData giving access to the input stream which reads the data to transform
        /// </param>
        /// <param name="dataFormat">
        /// The format to transform to
        /// </param>
        /// <param name="objectRetrievalManager">
        /// The object retrieval manager
        /// </param>
        /// <returns>
        /// The readable data location
        /// </returns>
        IReadableDataLocation PerformTransform(IReadableDataLocation sourceData, IDataFormat dataFormat, ISdmxObjectRetrievalManager objectRetrievalManager);

        /// <summary>
        /// Performs a transform from one data format to another data format.
        /// </summary>
        /// <param name="sourceData">
        /// The source data
        /// </param>
        /// <param name="dataFormat">
        /// The data format
        /// </param>
        /// <param name="dsd">
        /// The data structure object
        /// </param>
        /// <param name="dataflowObject">
        /// The data flow object
        /// </param>
        /// <returns>
        /// The readable data location
        /// </returns>
        IReadableDataLocation PerformTransform(IReadableDataLocation sourceData, IDataFormat dataFormat, IDataStructureObject dsd, IDataflowObject dataflowObject);

        /// <summary>
        /// Performs the transform and split.
        /// </summary>
        /// <param name="sourceData">
        /// The source data
        /// </param>
        /// <param name="dsdLocation">
        /// The readable data location
        /// </param>
        /// <param name="dataFormat">
        /// The data format
        /// </param>
        /// <returns>
        /// The list of readable data location
        /// </returns>
        IList<IReadableDataLocation> PerformTransformAndSplit(IReadableDataLocation sourceData, IReadableDataLocation dsdLocation, IDataFormat dataFormat);

        /// <summary>
        /// Performs the transform and split.
        /// </summary>
        /// <param name="sourceData">
        /// The source data
        /// </param>
        /// <param name="dataFormat">
        /// The data format
        /// </param>
        /// <param name="objectRetrievalManager">
        /// The object retrieval manager
        /// </param>
        /// <returns>
        /// The list of readable data location
        /// </returns>
        IList<IReadableDataLocation> PerformTransformAndSplit(IReadableDataLocation sourceData, IDataFormat dataFormat, ISdmxObjectRetrievalManager objectRetrievalManager);

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ISdmxDataRetrievalWithCrossWriter.cs" company="EUROSTAT">
//   Date Created : 2013-04-11
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;

    /// <summary>
    /// The SDMX Data Retrieval interface for writing the output to <c>SDMX v2.0</c> CrossSectional Datasets.
    /// </summary>
    public interface ISdmxDataRetrievalWithCrossWriter
    {
        /// <summary>
        /// Retrieves data requested in <paramref name="dataQuery"/> and write it to <paramref name="dataWriter"/>
        /// </summary>
        /// <param name="dataQuery">
        /// The data query.
        /// </param>
        /// <param name="dataWriter">
        /// The data writer.
        /// </param>
        void GetData(IDataQuery dataQuery, ICrossSectionalWriterEngine dataWriter);
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="IDataReaderPositionManager.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    #endregion

    /// <summary>
    /// Moves the data reader position backwards or forwards
    /// </summary>
    public interface IDataReaderPositionManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Moves the data reader engine to the position at the given index
        /// </summary>
        /// <param name="dataReaderEngine">
        /// The data reader engine
        /// </param>
        /// <param name="datasetIndex">
        /// The dataset to read the key for
        /// </param>
        /// <param name="keyPosition">
        /// The key index in the dataset
        /// </param>
        /// <returns>
        /// The position
        /// </returns>
        IKeyable MoveToPosition(IDataReaderEngine dataReaderEngine, int datasetIndex, int keyPosition);

        /// <summary>
        /// Moves the data reader engine to the observation at the given index, from the key obtained by the given index
        /// </summary>
        /// <param name="dataReaderEngine">
        /// The data reader engine
        /// </param>
        /// <param name="datasetIndex">
        /// The dataset to read the key for
        /// </param>
        /// <param name="keyPosition">
        /// The key index that the observation belongs to
        /// </param>
        /// <param name="obsNum">
        /// The observation index in the key
        /// </param>
        /// <returns>
        /// The observation
        /// </returns>
        IObservation MoveToObs(IDataReaderEngine dataReaderEngine, int datasetIndex, int keyPosition, int obsNum);

        #endregion
    }
}

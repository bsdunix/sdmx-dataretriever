﻿// -----------------------------------------------------------------------
// <copyright file="IDataInformationManager.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Model;

    #endregion

    /// <summary>
    /// The purpose of the data information manager is to get metadata off the dataset.
    /// </summary>
    public interface IDataInformationManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Returns the data type for the sourceData
        /// </summary>
        /// <param name="sourceData">
        /// The readable data location
        /// </param>
        /// <returns>
        /// The data type for the sourceData
        /// </returns>
        IDataFormat GetDataType(IReadableDataLocation sourceData);

        /// <summary>
        /// Returns the target namespace of the dataset
        /// </summary>
        /// <param name="sourceData">
        /// The readable data location
        /// </param>
        /// <returns>
        /// The target namespace of the dataset
        /// </returns>
        string GetTargetNamepace(IReadableDataLocation sourceData);

        /// <summary>
        /// Returns DataInformation about the data, this processes the entire dataset to give an overview of what is in the dataset.
        /// </summary>
        /// <param name="dre">
        /// The data reader engine
        /// </param>
        /// <returns>
        /// The DataInformation
        /// </returns>
        DataInformation GetDataInformation(IDataReaderEngine dre);

        /// <summary>
        /// Returns an ordered list of all the unique dates for each time format in the dataset.
        /// <p/>
        /// This list is ordered with the earliest date first.
        /// <p/>
        /// This method will call <code>reset()</code> on the dataReaderEngine before and after the 
        /// information has been gathered
        /// </summary>
        /// <param name="dataReaderEngine">
        /// The data reader engine
        /// </param>
        /// <returns>
        /// The dictionary of time format to the list of dates that are contained for the time format
        /// </returns>
        IDictionary<TimeFormat, IList<string>> GetAllReportedDates(IDataReaderEngine dataReaderEngine);

        /// <summary>
        /// Returns a list of dimension - value pairs where there is only a single value in the data for the dimension.  For example if the entire
        /// dataset had FREQ=A then one of the returned KeyValue pairs would be FREQ,A.  If FREQ=A and Q this would not be returned.
        /// <p/>
        /// <b>Note : an initial call to DataReaderEngine.reset will be made</b>
        /// </summary>
        /// <param name="dre">
        /// The data reader engine
        /// </param>
        /// <param name="includeObs">
        /// The include observation
        /// </param>
        /// <param name="includeAttributes">
        /// If true will also report the attributes that have only one value in the entire dataset
        /// </param>
        /// <returns>
        /// The list of dimension
        /// </returns>
        IList<IKeyValue> GetFixedConcepts(IDataReaderEngine dre, bool includeObs, bool includeAttributes);

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="DataWriterManager.cs" company="EUROSTAT">
//   Date Created : 2013-06-05
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    #region Using directives

    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.DataParser.Factory;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    ///     The data writer manager.
    /// </summary>
    public class DataWriterManager : IDataWriterManager
    {
         #region Fields

         /// <summary>
         ///     The factory
         /// </summary>
         private readonly IDataWriterFactory[] _factory;

         #endregion

         #region Constructors and Destructors

         /// <summary>
         /// Initializes a new instance of the <see cref="DataWriterManager"/> class.
         /// </summary>
         /// <param name="factory">
         /// The factory.
         /// </param>
         public DataWriterManager(params IDataWriterFactory[] factory)
         {
             this._factory = ObjectUtil.ValidArray(factory) ? factory : new IDataWriterFactory[] { new DataWriterFactory() };
         }

         #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get data writer engine.
        /// </summary>
        /// <param name="dataFormat">
        /// The data format. 
        /// </param>
        /// <param name="outPutStream">
        /// The output stream. 
        /// </param>
        /// <returns>
        /// The <see cref="IDataWriterEngine"/>.
        /// </returns>
        public virtual IDataWriterEngine GetDataWriterEngine(IDataFormat dataFormat, Stream outPutStream) 
        {
            foreach (IDataWriterFactory dwf in this._factory)
            {
                IDataWriterEngine dwe = dwf.GetDataWriterEngine(dataFormat, outPutStream);
                if (dwe != null)
                {
                    return dwe;
                }
            }

            throw new SdmxNotImplementedException("Could not write data out in type: " + dataFormat);
        }

        #endregion
    }
}

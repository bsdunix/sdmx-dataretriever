﻿// -----------------------------------------------------------------------
// <copyright file="ReportedDateEngine.cs" company="EUROSTAT">
//   Date Created : 2014-07-16
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;

    /// <summary>
    /// The reported date engine.
    /// </summary>
    public class ReportedDateEngine : IReportedDateEngine
    {
        /// <summary>
        /// Gets all reported dates.
        /// </summary>
        /// <param name="dataReaderEngine">The data reader engine</param>
        /// <returns>
        /// The reported dates
        /// </returns>
        public IDictionary<TimeFormat, IList<string>> GetAllReportedDates(IDataReaderEngine dataReaderEngine)
        {
            dataReaderEngine.Reset();
            var processedDates = new HashSet<string>(StringComparer.Ordinal);

            var timeFormatToSortedMap = new Dictionary<TimeFormatEnumType, IDictionary<DateTime, string>>();
            try
            {
                while (dataReaderEngine.MoveNextKeyable())
                {
                    var obs = dataReaderEngine.CurrentObservation;
                    var obsTime = obs.ObsTime;

                    // Check we have not already processed this date
                    if (processedDates.Contains(obsTime))
                    {
                        var obsTimeFormat = obs.ObsTimeFormat;
                        IDictionary<DateTime, string> sortedMap;

                        // Get the correct sorted map (or create it if it does not yet exist)
                        if (!timeFormatToSortedMap.TryGetValue(obsTimeFormat, out sortedMap))
                        {
                            sortedMap = new SortedDictionary<DateTime, string>();
                            timeFormatToSortedMap.Add(obsTimeFormat, sortedMap);
                        }

                        Debug.Assert(obs.ObsAsTimeDate.HasValue, "Observation time was null. Ported from Java where null is valid key for HashMap.");
                        sortedMap.Add(obs.ObsAsTimeDate.Value, obsTime);
                        processedDates.Add(obsTime);
                    }
                }

                // Create the response map. TODO split method.
                var responseMap = new Dictionary<TimeFormat, IList<string>>();
                foreach (var keyValuePair in timeFormatToSortedMap)
                {
                    var sortedDateList = new List<string>();
                    responseMap.Add(TimeFormat.GetFromEnum(keyValuePair.Key), sortedDateList);
                    var sortedMap = keyValuePair.Value;
                    sortedDateList.AddRange(sortedMap.Values);
                }

                return responseMap;
            }
            finally
            {
                dataReaderEngine.Reset();
            }
        }
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="FixedConceptEngine.cs" company="EUROSTAT">
//   Date Created : 2014-07-16
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

    /// <summary>
    /// The fixed concept engine.
    /// </summary>
    public class FixedConceptEngine : IFixedConceptEngine
    {
        /// <summary>
        /// Gets the fixed concepts.
        /// </summary>
        /// <param name="dre">The data reader engine</param>
        /// <param name="includeObs">The include observation</param>
        /// <param name="includeAttributes">The include attributes</param>
        /// <returns>
        /// The list of key values
        /// </returns>
        public IList<IKeyValue> GetFixedConcepts(IDataReaderEngine dre, bool includeObs, bool includeAttributes)
        {
            dre.Reset();
            var conceptMap = new Dictionary<string, string>(StringComparer.Ordinal);
            var skipConcepts = new HashSet<string>(StringComparer.Ordinal);

            while (dre.MoveNextKeyable())
            {
                var key = dre.CurrentKey;
                if (includeAttributes)
                {
                    ProcessKeyValues(key.Attributes, conceptMap, skipConcepts);
                }

                ProcessKeyValues(key.Key, conceptMap, skipConcepts);
                if (includeObs)
                {
                    while (dre.MoveNextObservation())
                    {
                        var obs = dre.CurrentObservation;
                        if (includeAttributes)
                        {
                            ProcessKeyValues(obs.Attributes, conceptMap, skipConcepts);
                        }

                        if (obs.CrossSection)
                        {
                            ProcessKeyValue(obs.CrossSectionalValue, conceptMap, skipConcepts);
                        }
                    }
                }
            }

            var fixedKeyValues = new List<IKeyValue>(conceptMap.Select(pair => new KeyValueImpl(pair.Value, pair.Key)));
            return fixedKeyValues;
        }

        /// <summary>
        /// Processes the key value.
        /// </summary>
        /// <param name="keyValue">The key value.</param>
        /// <param name="conceptMap">The concept map.</param>
        /// <param name="skipConcepts">The skip concepts.</param>
        private static void ProcessKeyValue(IKeyValue keyValue, IDictionary<string, string> conceptMap, ISet<string> skipConcepts)
        {
            string currentConcept = keyValue.Concept;
            if (skipConcepts.Contains(currentConcept))
            {
                return;
            }

            string value;
            if (!conceptMap.TryGetValue(currentConcept, out value))
            {
                conceptMap.Add(currentConcept, keyValue.Code);
            } 
            else if (!value.Equals(keyValue.Code))
            {
                conceptMap.Remove(currentConcept);
                skipConcepts.Add(currentConcept);
            }
        }

        /// <summary>
        /// Processes the key values.
        /// </summary>
        /// <param name="keyValues">The key values.</param>
        /// <param name="conceptMap">The concept map.</param>
        /// <param name="skipConcepts">The skip concepts.</param>
        private static void ProcessKeyValues(IEnumerable<IKeyValue> keyValues, IDictionary<string, string> conceptMap, ISet<string> skipConcepts)
        {
            foreach (var keyValue in keyValues)
            {
                ProcessKeyValue(keyValue, conceptMap, skipConcepts);
            }
        }
    }
}
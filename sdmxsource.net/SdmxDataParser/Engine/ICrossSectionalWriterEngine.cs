// -----------------------------------------------------------------------
// <copyright file="ICrossSectionalWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     The interface for SDMX Cross Sectional Data Writers
    /// </summary>
    /// <example>
    ///     A sample in C# for <see cref="ICrossSectionalWriterEngine" />
    ///     <code source="..\ReUsingExamples\DataWriting\ReUsingCrossWriter.cs" lang="cs" />
    /// </example>
    public interface ICrossSectionalWriterEngine : IWriterEngine
    {
        #region Public Methods and Operators

        /// <summary>
        /// Starts a dataset with the data conforming to the <paramref name="dsd"/>
        /// </summary>
        /// <param name="dataflow">
        /// The <see cref="IDataflowObject"/>
        /// </param>
        /// <param name="dsd">
        /// The <see cref="ICrossSectionalDataStructureObject"/>
        /// </param>
        /// <param name="header">
        /// The Dataset attributes
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// if the <paramref name="dsd"/> is null
        /// </exception>
        void StartDataset(IDataflowObject dataflow, ICrossSectionalDataStructureObject dsd, IDatasetHeader header);

        /// <summary>
        ///     Start a Cross Sectional Section
        /// </summary>
        void StartSection();

        /// <summary>
        ///     Start a Cross Sectional group
        /// </summary>
        void StartXSGroup();

        /// <summary>
        /// Write a Cross Sectional Measure with <paramref name="measure"/> and <paramref name="value"/>
        /// </summary>
        /// <param name="measure">
        /// The measure code
        /// </param>
        /// <param name="value">
        /// The measure value
        /// </param>
        void StartXSObservation(string measure, string value);

        /// <summary>
        /// Write an <paramref name="attribute"/> and the <paramref name="value"/>
        /// </summary>
        /// <param name="attribute">
        /// The attribute concept id
        /// </param>
        /// <param name="value">
        /// The value
        /// </param>
        void WriteAttributeValue(string attribute, string value);

        /// <summary>
        /// Write a Cross Sectional section <paramref name="key"/> and the <paramref name="value"/>
        /// </summary>
        /// <param name="key">
        /// The key. i.e. the dimension
        /// </param>
        /// <param name="value">
        /// The value
        /// </param>
        void WriteSectionKeyValue(string key, string value);

        /// <summary>
        /// Write a Cross Sectional DataSet <paramref name="key"/> and the <paramref name="value"/>
        /// </summary>
        /// <param name="key">
        /// The key. i.e. the dimension
        /// </param>
        /// <param name="value">
        /// The value
        /// </param>
        void WriteDataSetKeyValue(string key, string value);

        /// <summary>
        /// Write a Cross Sectional Group <paramref name="key"/> and the <paramref name="value"/>
        /// </summary>
        /// <param name="key">
        /// The key. i.e. the dimension
        /// </param>
        /// <param name="value">
        /// The value
        /// </param>
        void WriteXSGroupKeyValue(string key, string value);

        /// <summary>
        /// Write a Cross Sectional measure <paramref name="key"/> and the <paramref name="value"/>
        /// </summary>
        /// <param name="key">
        /// The key. i.e. the dimension
        /// </param>
        /// <param name="value">
        /// The value
        /// </param>
        void WriteXSObservationKeyValue(string key, string value);

        /// <summary>
        /// Closes the writer. Makes to close any opened XSObservation/Section/XSGroup and write the closing dataset/message elements.
        /// </summary>
        void Close();

        #endregion
    }
}
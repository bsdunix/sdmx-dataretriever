﻿// -----------------------------------------------------------------------
// <copyright file="DataInformation.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Model
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    #endregion

    /// <summary>
    /// The Data Information model.
    /// </summary>
    [Serializable]
    public class DataInformation
    {
        #region Fields

        /// <summary>
        /// The _groups
        /// </summary>
        private readonly int _groups;

        /// <summary>
        /// The _keys
        /// </summary>
        private readonly int _keys;

        /// <summary>
        /// The _observations
        /// </summary>
        private readonly int _observations;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataInformation"/> class.
        /// </summary>
        /// <param name="dre">
        /// The data reader engine
        /// </param>
        public DataInformation(IDataReaderEngine dre)
        {
            if (dre == null)
            {
                throw new ArgumentException("No DataReaderEngine specified.");
            }

            dre.Reset();
            ISet<string> keySet = new HashSet<string>();
            ISet<string> groupSet = new HashSet<string>();

            while (dre.MoveNextKeyable())
            {
                IKeyable currentKey = dre.CurrentKey;

                if (currentKey.Series)
                {
                    keySet.Add(currentKey.ShortCode);
                }
                else
                {
                    groupSet.Add(currentKey.ShortCode);
                }

                while (dre.MoveNextObservation())
                {
                    this._observations++;
                }
            }

            this._keys = keySet.Count;
            this._groups = groupSet.Count;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the number of series keys
        /// </summary>
        public int NumberOfSeriesKeys
        {
            get
            {
                return this._keys;
            }
        }

        /// <summary>
        /// Gets the number of groups
        /// </summary>
        public int Groups
        {
            get
            {
                return this._groups;
            }
        }

        /// <summary>
        /// Gets the number of observations
        /// </summary>
        public int NumberOfObservations
        {
            get
            {
                return this._observations;
            }
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="DataParseMetadata.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Model
{
    #region Using directives

    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;

    #endregion

    /// <summary>
    ///     Contains information required for data parsing.
    /// </summary>
    public class DataParseMetadata
    {
        #region Fields

        /// <summary>
        /// The _data structure.
        /// </summary>
        private readonly IDataStructureObject _dataStructure;

        /// <summary>
        /// The _out put stream.
        /// </summary>
        private readonly Stream _outPutStream;

        /// <summary>
        /// The _output schema version.
        /// </summary>
        private readonly SdmxSchema _outputSchemaVersion;

        /// <summary>
        /// The _source data.
        /// </summary>
        private readonly IReadableDataLocation _sourceData;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataParseMetadata"/> class.
        /// </summary>
        /// <param name="sourceData">
        /// The readable data location
        /// </param>
        /// <param name="outPutStream">
        /// The output stream
        /// </param>
        /// <param name="outputSchemaVersion">
        /// The output schema version
        /// </param>
        /// <param name="keyFamily">
        /// The key family
        /// </param>
        public DataParseMetadata(IReadableDataLocation sourceData, Stream outPutStream, SdmxSchema outputSchemaVersion, IDataStructureObject keyFamily)
        {
            this._sourceData = sourceData;
            this._outPutStream = outPutStream;
            this._dataStructure = keyFamily;
            this._outputSchemaVersion = outputSchemaVersion;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the data structure
        /// </summary>
        public IDataStructureObject DataStructure
        {
            get
            {
                return this._dataStructure;
            }
        }

        /// <summary>
        ///     Gets the output stream
        /// </summary>
        public Stream OutPutStream
        {
            get
            {
                return this._outPutStream;
            }
        }

        /// <summary>
        ///     Gets the output schema version
        /// </summary>
        public SdmxSchema OutputSchemaVersion
        {
            get
            {
                return this._outputSchemaVersion;
            }
        }

        /// <summary>
        ///     Gets the source data
        /// </summary>
        public IReadableDataLocation SourceData
        {
            get
            {
                return this._sourceData;
            }
        }

        #endregion
    }
}
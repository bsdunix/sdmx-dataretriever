﻿// -----------------------------------------------------------------------
// <copyright file="IDataTransform.cs" company="EUROSTAT">
//   Date Created : 2013-05-10
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Transform
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.DataParser.Model;

    #endregion

    /// <summary>
    /// The DataTransform interface.
    /// </summary>
    public interface IDataTransform
    {
        #region Public Methods and Operators

        /// <summary>
        /// Transforms the specified data parse metadata.
        /// </summary>
        /// <param name="dataParseMetadata">The data parse metadata</param>
        void Transform(DataParseMetadata dataParseMetadata);

        #endregion
    }
}

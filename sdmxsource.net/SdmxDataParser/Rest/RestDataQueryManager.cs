﻿// -----------------------------------------------------------------------
// <copyright file="RestDataQueryManager.cs" company="EUROSTAT">
//   Date Created : 2015-05-14
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Rest
{
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Rest;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;

    /// <summary>
    /// This class obtains a DataWriterEngine for a DataFormat, builds a DataQuery from a RESTDataQuery
    /// and passes this information on to a SdmxDataRetrievalWithWriter to fulfill the query
    /// </summary>
    public class RestDataQueryManager : IRestDataQueryManager
    {
        /// <summary>
        /// The _data writer manager
        /// </summary>
        private readonly IDataWriterManager _dataWriterManager;

        /// <summary>
        /// The _object retrieval manager
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _objectRetrievalManager;

        /// <summary>
        /// The _data retrieval with writer
        /// </summary>
        private readonly ISdmxDataRetrievalWithWriter _dataRetrievalWithWriter;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestDataQueryManager"/> class.
        /// </summary>
        /// <param name="dataRetrievalWithWriter">The data retrieval with writer.</param>
        /// <param name="dataWriterManager">The data writer manager.</param>
        /// <param name="objectRetrievalManager">The object retrieval manager.</param>
        public RestDataQueryManager(ISdmxDataRetrievalWithWriter dataRetrievalWithWriter, IDataWriterManager dataWriterManager, ISdmxObjectRetrievalManager objectRetrievalManager)
        {
            this._dataRetrievalWithWriter = dataRetrievalWithWriter;
            this._dataWriterManager = dataWriterManager;
            this._objectRetrievalManager = objectRetrievalManager;
        }

        /// <summary>
        /// Execute the rest query, write the response out to the output stream based on the data format
        /// </summary>
        /// <param name="dataQuery">
        /// The data query
        /// </param>
        /// <param name="dataFormat">
        /// The data format
        /// </param>
        /// <param name="outPutStream">
        /// The output stream
        /// </param>
        public void ExecuteQuery(IRestDataQuery dataQuery, IDataFormat dataFormat, Stream outPutStream)
        {
            IDataQuery query = new DataQueryImpl(dataQuery, this._objectRetrievalManager);
            IDataWriterEngine dataWriterEngine = this._dataWriterManager.GetDataWriterEngine(dataFormat, outPutStream);
            this._dataRetrievalWithWriter.GetData(query, dataWriterEngine);
        }
    }
}
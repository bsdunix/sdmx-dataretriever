// -----------------------------------------------------------------------
// <copyright file="SubmitStructureReponseBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.SubmissionResponse;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Response;

    /// <summary>
    ///     The submit structure response builder.
    /// </summary>
    public class SubmitStructureReponseBuilder : AbstractResponseBuilder<ISubmitStructureResponse>
    {
        #region Static Fields

        /// <summary>
        ///     The _registry message type.
        /// </summary>
        private static readonly RegistryMessageType _registryMessageType =
            RegistryMessageType.GetFromEnum(RegistryMessageEnumType.SubmitStructureResponse);

        #endregion

        #region Fields

        /// <summary>
        ///     The response builder v 21.
        /// </summary>
        private readonly SubmitStructureResponseBuilderV21 _responseBuilderV21 = new SubmitStructureResponseBuilderV21();

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the expected message type.
        /// </summary>
        internal override RegistryMessageType ExpectedMessageType
        {
            get
            {
                return _registryMessageType;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Build <see cref="SubmitStructureResponse"/> list from the specified <paramref name="registryInterface"/>
        /// </summary>
        /// <param name="registryInterface">
        /// The registry Interface message
        /// </param>
        /// <returns>
        /// The <see cref="SubmitStructureResponse"/> list from the specified <paramref name="registryInterface"/>
        /// </returns>
        internal override IList<ISubmitStructureResponse> BuildInternal(RegistryInterface registryInterface)
        {
            return this._responseBuilderV21.Build(registryInterface);
        }

        #endregion
    }
}
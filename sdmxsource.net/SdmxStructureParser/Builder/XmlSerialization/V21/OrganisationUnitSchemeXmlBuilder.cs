// -----------------------------------------------------------------------
// <copyright file="OrganisationUnitSchemeXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    using OrganisationUnit = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.OrganisationUnit;

    /// <summary>
    ///     The organisation unit scheme xml bean builder.
    /// </summary>
    public class OrganisationUnitSchemeXmlBuilder : ItemSchemeAssembler, 
                                                    IBuilder<OrganisationUnitSchemeType, IOrganisationUnitSchemeObject>
    {
        private OrganisationXmlAssembler _organisationXmlAssembler = new OrganisationXmlAssembler();

        #region Public Methods and Operators

        /// <summary>
        /// Build a <see cref="OrganisationUnitSchemeType"/> from <paramref name="buildFrom"/> and return it
        /// </summary>
        /// <param name="buildFrom">
        /// The <see cref="IOrganisationUnitSchemeObject"/>
        /// </param>
        /// <returns>
        /// The <see cref="OrganisationUnitSchemeType"/>.
        /// </returns>
        public virtual OrganisationUnitSchemeType Build(IOrganisationUnitSchemeObject buildFrom)
        {
            var returnType = new OrganisationUnitSchemeType();
            this.AssembleItemScheme(returnType, buildFrom);
            if (buildFrom.Items != null)
            {
                /* foreach */
                foreach (IOrganisationUnit item in buildFrom.Items)
                {
                    var organisationUnitType = new OrganisationUnit();
                    returnType.Organisation.Add(organisationUnitType);
                    _organisationXmlAssembler.Assemble(organisationUnitType, item);
                    this.AssembleNameable(organisationUnitType.Content, item);
                    if (item.HasParentUnit)
                    {
                        LocalItemReferenceType parent = new LocalAgencyReferenceType();
                        parent.Ref = new LocalOrganisationUnitRefType();
                        parent.Ref.id = item.ParentUnit;
                        organisationUnitType.SetTypedParent(parent);
                    }
                }
            }

            return returnType;
        }

        #endregion
    }
}
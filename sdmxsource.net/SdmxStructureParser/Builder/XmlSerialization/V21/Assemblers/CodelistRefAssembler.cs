// -----------------------------------------------------------------------
// <copyright file="CodelistRefAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The codelist ref bean assembler.
    /// </summary>
    public class CodelistRefAssembler : AbstractAssembler, IAssembler<IncludedCodelistReferenceType, ICodelistRef>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Assemble from <paramref name="assembleFrom"/> into <paramref name="assembleInto"/>.
        /// </summary>
        /// <param name="assembleInto">
        /// The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        /// The assemble from.
        /// </param>
        public virtual void Assemble(IncludedCodelistReferenceType assembleInto, ICodelistRef assembleFrom)
        {
            // Populate it from inherited super
            ICrossReference codelistReference = assembleFrom.CodelistReference;
            if (codelistReference != null)
            {
                var codelistRefType = new CodelistRefType();
                assembleInto.SetTypedRef(codelistRefType);
                this.SetReference(codelistRefType, codelistReference);
            }

            // Populate it using this class's specifics
            string value = assembleFrom.Alias;
            if (!string.IsNullOrWhiteSpace(value))
            {
                assembleInto.alias = assembleFrom.Alias;
            }
        }

        #endregion
    }
}
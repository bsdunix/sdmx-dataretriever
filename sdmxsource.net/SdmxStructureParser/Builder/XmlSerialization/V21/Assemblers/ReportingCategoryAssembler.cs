// -----------------------------------------------------------------------
// <copyright file="ReportingCategoryAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util;

    using ReportingCategory = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.ReportingCategory;

    /// <summary>
    ///     The reporting category bean assembler.
    /// </summary>
    public class ReportingCategoryAssembler : MaintainableAssembler, 
                                              IAssembler<ReportingCategoryType, IReportingCategoryObject>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The assemble.
        /// </summary>
        /// <param name="assembleInto">
        /// The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        /// The assemble from.
        /// </param>
        public virtual void Assemble(ReportingCategoryType assembleInto, IReportingCategoryObject assembleFrom)
        {
            var stack = new Stack<KeyValuePair<ReportingCategoryType, IReportingCategoryObject>>();
            stack.Push(new KeyValuePair<ReportingCategoryType, IReportingCategoryObject>(assembleInto, assembleFrom));

            while (stack.Count > 0)
            {
                KeyValuePair<ReportingCategoryType, IReportingCategoryObject> pair = stack.Pop();
                assembleInto = pair.Key;
                assembleFrom = pair.Value;

                // Populate it from inherited super - NOTE downgraded to identifiable from nameable
                this.AssembleNameable(assembleInto, assembleFrom);
                if (ObjectUtil.ValidCollection(assembleFrom.StructuralMetadata))
                {
                    /* foreach */
                    foreach (ICrossReference crossReference in assembleFrom.StructuralMetadata)
                    {
                        var structureReferenceType = new StructureReferenceType();
                        assembleInto.StructuralMetadata.Add(structureReferenceType);
                        var structureRefType = new StructureRefType();
                        structureReferenceType.SetTypedRef(structureRefType);
                        this.SetReference(structureRefType, crossReference);
                    }
                }

                if (ObjectUtil.ValidCollection(assembleFrom.ProvisioningMetadata))
                {
                    /* foreach */
                    foreach (ICrossReference crossReference in assembleFrom.ProvisioningMetadata)
                    {
                        var structureUsageReferenceType = new StructureUsageReferenceType();
                        assembleInto.ProvisioningMetadata.Add(structureUsageReferenceType);
                        var structureUsageRefType = new StructureUsageRefType();
                        structureUsageReferenceType.SetTypedRef(structureUsageRefType);
                        this.SetReference(structureUsageRefType, crossReference);
                    }
                }

                /* foreach */
                foreach (IReportingCategoryObject eachReportingCategoryBean in assembleFrom.Items)
                {
                    var eachReportingCategory = new ReportingCategory();
                    assembleInto.Item.Add(eachReportingCategory);
                    stack.Push(
                        new KeyValuePair<ReportingCategoryType, IReportingCategoryObject>(
                            eachReportingCategory.Content, eachReportingCategoryBean));

                    ////this.Assemble(eachReportingCategory.Content, eachReportingCategoryBean);
                }
            }
        }

        #endregion
    }
}
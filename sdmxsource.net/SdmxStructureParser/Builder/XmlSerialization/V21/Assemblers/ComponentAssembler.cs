// -----------------------------------------------------------------------
// <copyright file="ComponentAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// The component assembler.
    /// </summary>
    /// <typeparam name="T">
    /// The representation type
    /// </typeparam>
    public sealed class ComponentAssembler<T> : IdentifiableAssembler
        where T : RepresentationType, new()
    {
        #region Fields

        /// <summary>
        ///     The representation xml bean builder.
        /// </summary>
        /// <typeparam name="T">
        ///     The representation type
        /// </typeparam>
        private readonly RepresentationXmlBuilder<T> _representationXmlBuilder = new RepresentationXmlBuilder<T>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Assemble component.
        /// </summary>
        /// <param name="builtObj">
        /// The destination component.
        /// </param>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        public void AssembleComponent(ComponentType builtObj, IComponent buildFrom)
        {
            this.AssembleIdentifiable(builtObj, buildFrom);
            if (buildFrom.ConceptRef != null)
            {
                var conceptReference = new ConceptReferenceType();
                builtObj.ConceptIdentity = conceptReference;
                var conceptRefType = new ConceptRefType();
                conceptReference.SetTypedRef(conceptRefType);
                this.SetReference(conceptRefType, buildFrom.ConceptRef);
            }

            if (buildFrom.Representation != null)
            {
                var representationType = new T();
                builtObj.LocalRepresentation = representationType;
                this._representationXmlBuilder.Assemble(representationType, buildFrom.Representation);
            }
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="RepresentationXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// The representation xml bean builder.
    /// </summary>
    /// <typeparam name="TRepresenation">
    /// The concrete representation type
    /// </typeparam>
    public class RepresentationXmlBuilder<TRepresenation> : AbstractAssembler, 
                                                            IAssembler<TRepresenation, IRepresentation>
        where TRepresenation : RepresentationType, new()
    {
        #region Fields

        /// <summary>
        ///     The text format assembler.
        /// </summary>
        private readonly TextFormatAssembler _textFormatAssembler = new TextFormatAssembler();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The assemble.
        /// </summary>
        /// <param name="assembleInto">
        /// The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        /// The assemble from.
        /// </param>
        public virtual void Assemble(TRepresenation assembleInto, IRepresentation assembleFrom)
        {
            if (assembleFrom.Representation != null)
            {
                var itemSchemeRefType = new ItemSchemeReferenceType();
                assembleInto.Enumeration = itemSchemeRefType;
                var schemeRefType = new ItemSchemeRefType();
                itemSchemeRefType.SetTypedRef(schemeRefType);
                this.SetReference(schemeRefType, assembleFrom.Representation);
                if (assembleFrom.TextFormat != null)
                {
                    var codededTextFormatType = new CodededTextFormatType();
                    this._textFormatAssembler.Assemble(codededTextFormatType, assembleFrom.TextFormat);
                    assembleInto.EnumerationFormat = codededTextFormatType;
                }
            }
            else if (assembleFrom.TextFormat != null)
            {
                this._textFormatAssembler.Assemble(assembleInto.AddNewTextFormatType(), assembleFrom.TextFormat);
            }
        }

        #endregion
    }
}
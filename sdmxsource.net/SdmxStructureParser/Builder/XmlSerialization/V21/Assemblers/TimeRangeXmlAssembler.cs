// -----------------------------------------------------------------------
// <copyright file="TimeRangeXmlAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The time range xml assembler.
    /// </summary>
    public class TimeRangeXmlAssembler : IAssembler<TimeRangeValueType, ITimeRange>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The assemble.
        /// </summary>
        /// <param name="assembleInto">
        /// The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        /// The assemble from.
        /// </param>
        public virtual void Assemble(TimeRangeValueType assembleInto, ITimeRange assembleFrom)
        {
            if (assembleFrom.Range)
            {
                SetDate(
                    assembleInto.StartPeriod = new TimePeriodRangeType(), 
                    assembleFrom.StartDate, 
                    assembleFrom.StartInclusive);
                SetDate(
                    assembleInto.EndPeriod = new TimePeriodRangeType(), assembleFrom.EndDate, assembleFrom.EndInclusive);
            }
            else
            {
                if (assembleFrom.StartDate != null)
                {
                    SetDate(
                        assembleInto.StartPeriod = new TimePeriodRangeType(), 
                        assembleFrom.StartDate, 
                        assembleFrom.StartInclusive);
                }
                else
                {
                    SetDate(
                        assembleInto.EndPeriod = new TimePeriodRangeType(), 
                        assembleFrom.EndDate, 
                        assembleFrom.EndInclusive);
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the date.
        /// </summary>
        /// <param name="timePeriodRangeType">
        /// The time Period Range Type.
        /// </param>
        /// <param name="sdmxDate">
        /// The sdmxDate.
        /// </param>
        /// <param name="inclusive">
        /// The inclusive.
        /// </param>
        private static void SetDate(TimePeriodRangeType timePeriodRangeType, ISdmxDate sdmxDate, bool inclusive)
        {
            timePeriodRangeType.isInclusive = inclusive;
            timePeriodRangeType.TypedValue = sdmxDate.DateInSdmxFormat;
        }

        #endregion
    }
}
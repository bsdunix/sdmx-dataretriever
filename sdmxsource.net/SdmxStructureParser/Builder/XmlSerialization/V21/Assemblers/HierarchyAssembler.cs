// -----------------------------------------------------------------------
// <copyright file="HierarchyAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    /// <summary>
    ///     The hierarchy hierarchicalCodelist assembler.
    /// </summary>
    public class HierarchyAssembler : NameableAssembler, IAssembler<HierarchyType, IHierarchy>
    {
        #region Fields

        /// <summary>
        ///     The hierarchical code hierarchicalCodelist assembler.
        /// </summary>
        private readonly HierarchicalCodeAssembler _hierarchicalCodeAssembler = new HierarchicalCodeAssembler();

        /// <summary>
        ///     The level assembler.
        /// </summary>
        private readonly LevelAssembler _levelAssembler = new LevelAssembler();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The assemble.
        /// </summary>
        /// <param name="assembleInto">
        /// The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        /// The assemble from.
        /// </param>
        public virtual void Assemble(HierarchyType assembleInto, IHierarchy assembleFrom)
        {
            // Populate it from inherited super
            this.AssembleNameable(assembleInto, assembleFrom);

            // Populate it using this class's specifics
            foreach (IHierarchicalCode eachCodeRefBean in assembleFrom.HierarchicalCodeObjects)
            {
                var eachHierarchicalCode = new HierarchicalCodeType();
                assembleInto.HierarchicalCode.Add(eachHierarchicalCode);
                this._hierarchicalCodeAssembler.Assemble(eachHierarchicalCode, eachCodeRefBean);
            }

            if (assembleFrom.Level != null)
            {
                this._levelAssembler.Assemble(assembleInto.Level = new LevelType(), assembleFrom.Level);
            }

            assembleInto.leveled = assembleFrom.HasFormalLevels();
        }

        #endregion
    }
}
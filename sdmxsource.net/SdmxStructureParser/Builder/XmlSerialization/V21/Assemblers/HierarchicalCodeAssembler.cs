// -----------------------------------------------------------------------
// <copyright file="HierarchicalCodeAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The hierarchical code bean assembler.
    /// </summary>
    public class HierarchicalCodeAssembler : IdentifiableAssembler, IAssembler<HierarchicalCodeType, IHierarchicalCode>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Assemble from <paramref name="assembleFrom"/> to <paramref name="assembleInto"/>.
        /// </summary>
        /// <param name="assembleInto">
        /// The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        /// The assemble from.
        /// </param>
        public virtual void Assemble(HierarchicalCodeType assembleInto, IHierarchicalCode assembleFrom)
        {
            var codes = new Stack<KeyValuePair<HierarchicalCodeType, IHierarchicalCode>>();
            codes.Push(new KeyValuePair<HierarchicalCodeType, IHierarchicalCode>(assembleInto, assembleFrom));
            while (codes.Count > 0)
            {
                KeyValuePair<HierarchicalCodeType, IHierarchicalCode> pair = codes.Pop();
                assembleInto = pair.Key;
                assembleFrom = pair.Value;

                // Populate it from inherited super
                this.AssembleIdentifiable(assembleInto, assembleFrom);
                if (assembleFrom.CodelistAliasRef != null)
                {
                    assembleInto.CodelistAliasRef = assembleFrom.CodelistAliasRef;
                    var localRef = new LocalCodeReferenceType();
                    assembleInto.CodeID = localRef;
                    var xref = new LocalCodeRefType();
                    localRef.SetTypedRef(xref);
                    xref.id = assembleFrom.CodeId;
                }
                else
                {
                    ICrossReference crossReference = assembleFrom.CodeReference;
                    var code = new CodeReferenceType();
                    assembleInto.Code = code;
                    var codeRefType = new CodeRefType();
                    code.SetTypedRef(codeRefType);

                    this.SetReference(codeRefType, crossReference);
                }

                // Children
                foreach (IHierarchicalCode eachCodeRefBean in assembleFrom.CodeRefs)
                {
                    var eachHierarchicalCode = new HierarchicalCodeType();
                    assembleInto.HierarchicalCode.Add(eachHierarchicalCode);
                    codes.Push(
                        new KeyValuePair<HierarchicalCodeType, IHierarchicalCode>(eachHierarchicalCode, eachCodeRefBean));

                    //// this.Assemble(eachHierarchicalCode, eachCodeRefBean);
                }

                // LEVEL
                if (assembleFrom.GetLevel(false) != null)
                {
                    var ref0 = new LocalLevelReferenceType();
                    assembleInto.Level = ref0;
                    assembleInto.Level.SetTypedRef(
                        new LocalLevelRefType { id = assembleFrom.GetLevel(false).GetFullIdPath(false) });
                }
            }
        }

        #endregion
    }
}
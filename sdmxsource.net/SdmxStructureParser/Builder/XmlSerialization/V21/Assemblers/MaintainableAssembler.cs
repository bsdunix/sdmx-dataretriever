// -----------------------------------------------------------------------
// <copyright file="MaintainableAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The maintainable bean assembler.
    /// </summary>
    public class MaintainableAssembler : NameableAssembler
    {
        #region Public Methods and Operators

        /// <summary>
        /// The assemble maintainable.
        /// </summary>
        /// <param name="builtObj">
        /// The object to populate.
        /// </param>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        public void AssembleMaintainable(MaintainableType builtObj, IMaintainableObject buildFrom)
        {
            // Populate it from inherited super
            this.AssembleNameable(builtObj, buildFrom);

            // Populate it using this class's specifics
            string str1 = buildFrom.AgencyId;
            if (!string.IsNullOrWhiteSpace(str1))
            {
                builtObj.agencyID = buildFrom.AgencyId;
            }

            string str0 = buildFrom.Version;
            if (!string.IsNullOrWhiteSpace(str0))
            {
                builtObj.version = buildFrom.Version;
            }

            if (buildFrom.StartDate != null)
            {
                builtObj.validFrom = buildFrom.StartDate.Date;
            }

            if (buildFrom.EndDate != null)
            {
                builtObj.validTo = buildFrom.EndDate.Date;
            }

            if (buildFrom.IsExternalReference.IsSet())
            {
                builtObj.isExternalReference = buildFrom.IsExternalReference.IsTrue;
            }

            if (buildFrom.IsFinal.IsSet())
            {
                builtObj.isFinal = buildFrom.IsFinal.IsTrue;
            }

            if (buildFrom.StructureUrl != null)
            {
                builtObj.structureURL = buildFrom.StructureUrl;
            }

            if (buildFrom.ServiceUrl != null)
            {
                builtObj.serviceURL = buildFrom.ServiceUrl;
            }
        }

        #endregion
    }
}
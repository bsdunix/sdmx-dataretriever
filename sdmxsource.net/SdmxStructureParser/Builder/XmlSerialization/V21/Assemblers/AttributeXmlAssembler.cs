// -----------------------------------------------------------------------
// <copyright file="AttributeXmlAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The attribute xml assembler.
    /// </summary>
    public class AttributeXmlAssembler : IAssembler<AttributeType, IAttributeObject>
    {
        #region Fields

        /// <summary>
        ///     The component assembler.
        /// </summary>
        private readonly ComponentAssembler<SimpleDataStructureRepresentationType> _componentAssembler =
            new ComponentAssembler<SimpleDataStructureRepresentationType>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The assemble.
        /// </summary>
        /// <param name="assembleInto">
        /// The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        /// The assemble from.
        /// </param>
        public virtual void Assemble(AttributeType assembleInto, IAttributeObject assembleFrom)
        {
            this._componentAssembler.AssembleComponent(assembleInto, assembleFrom);

            if (assembleFrom.ConceptRoles != null)
            {
                foreach (ICrossReference currentConceptRole in assembleFrom.ConceptRoles)
                {
                    var conceptRef = new ConceptReferenceType();
                    assembleInto.ConceptRole.Add(conceptRef);
                    var conceptRefType = new ConceptRefType();
                    conceptRef.SetTypedRef(conceptRefType);
                    this._componentAssembler.SetReference(conceptRefType, currentConceptRole);
                }
            }

            if (assembleFrom.AssignmentStatus != null)
            {
                switch (assembleFrom.AssignmentStatus)
                {
                    case UsageStatusTypeConstants.Conditional:
                    case UsageStatusTypeConstants.Mandatory:
                        assembleInto.assignmentStatus = assembleFrom.AssignmentStatus;
                        break;
                }
            }

            var attributeRelationship = new AttributeRelationshipType();
            assembleInto.AttributeRelationship = attributeRelationship;
            if (ObjectUtil.ValidCollection(assembleFrom.DimensionReferences))
            {
                /* foreach */
                foreach (string currentDimensionRef in assembleFrom.DimensionReferences)
                {
                    var dimRef = new LocalDimensionReferenceType();
                    attributeRelationship.Dimension.Add(dimRef);

                    var localDimensionRefType = new LocalDimensionRefType();
                    dimRef.SetTypedRef(localDimensionRefType);
                    localDimensionRefType.id = currentDimensionRef;
                }
            }
            else
            {
                string value = assembleFrom.AttachmentGroup;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    // TODO do we add this to AttachmentGroup or Group ??? Used Group because this is the case in Java 0.9.1
                    attributeRelationship.Group = new LocalGroupKeyDescriptorReferenceType();
                    attributeRelationship.Group.SetTypedRef(
                        new LocalGroupKeyDescriptorRefType { id = assembleFrom.AttachmentGroup });
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(assembleFrom.PrimaryMeasureReference))
                    {
                        attributeRelationship.PrimaryMeasure = new LocalPrimaryMeasureReferenceType();
                        attributeRelationship.PrimaryMeasure.SetTypedRef(
                            new LocalPrimaryMeasureRefType { id = assembleFrom.PrimaryMeasureReference });
                    }
                    else
                    {
                        attributeRelationship.None = new EmptyType();
                    }
                }
            }
        }

        #endregion
    }
}
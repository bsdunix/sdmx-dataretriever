// -----------------------------------------------------------------------
// <copyright file="HierarchicalCodelistXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    /// <summary>
    ///     The hierarchical codelist xml builder.
    /// </summary>
    public class HierarchicalCodelistXmlBuilder : MaintainableAssembler, 
                                                  IBuilder<HierarchicalCodelistType, IHierarchicalCodelistObject>
    {
        #region Fields

        /// <summary>
        ///     The codelist ref bean assembler bean.
        /// </summary>
        private readonly CodelistRefAssembler _codelistRefAssemblerBean = new CodelistRefAssembler();

        /// <summary>
        ///     The hierarchy bean assembler bean.
        /// </summary>
        private readonly HierarchyAssembler _hierarchyAssemblerBean = new HierarchyAssembler();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="HierarchicalCodelistType"/>.
        /// </returns>
        public virtual HierarchicalCodelistType Build(IHierarchicalCodelistObject buildFrom)
        {
            // Create outgoing build
            var builtObj = new HierarchicalCodelistType();

            // Populate it from inherited super
            this.AssembleMaintainable(builtObj, buildFrom);

            // Populate it using this class's specifics
            // Code refs
            foreach (ICodelistRef eachCodelistRef in buildFrom.CodelistRef)
            {
                var newCodelistRef = new IncludedCodelistReferenceType();
                builtObj.IncludedCodelist.Add(newCodelistRef);
                this._codelistRefAssemblerBean.Assemble(newCodelistRef, eachCodelistRef);
            }

            // Hierarchies
            if (buildFrom.Hierarchies != null)
            {
                /* foreach */
                foreach (IHierarchy eachHierarchy in buildFrom.Hierarchies)
                {
                    var newValueHierarchy = new HierarchyType();
                    builtObj.Hierarchy.Add(newValueHierarchy);
                    this._hierarchyAssemblerBean.Assemble(newValueHierarchy, eachHierarchy);
                }
            }

            return builtObj;
        }

        #endregion
    }
}
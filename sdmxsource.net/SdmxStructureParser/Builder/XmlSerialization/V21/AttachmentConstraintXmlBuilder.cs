// -----------------------------------------------------------------------
// <copyright file="AttachmentConstraintXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    /// <summary>
    ///     attachments the constraint xml bean builder.
    /// </summary>
    public class AttachmentConstraintXmlBuilder : ConstraintAssembler,
                                                  IBuilder<AttachmentConstraintType, IAttachmentConstraintObject>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="buildFrom">
        /// builds the from.
        /// </param>
        /// <returns>
        /// The <see cref="AttachmentConstraintType"/>.
        /// </returns>
        public virtual AttachmentConstraintType Build(IAttachmentConstraintObject buildFrom)
        {
            var returnType = new AttachmentConstraintType();

            this.Assemble(returnType, buildFrom);

            return returnType;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a <see cref="DataKeySetType"/> to <paramref name="constraintType"/> and return it.
        /// </summary>
        /// <param name="constraintType">
        /// constraints the type.
        /// </param>
        /// <returns>
        /// The <see cref="DataKeySetType"/>.
        /// </returns>
        private static DataKeySetType AddDataKeySetType(ConstraintType constraintType)
        {
            var dataKeySetType = new DataKeySetType();
            constraintType.DataKeySet.Add(dataKeySetType);
            return dataKeySetType;
        }

        /// <summary>
        ///     builds the metadata key set.
        /// </summary>
        private void BuildMetadataKeySet()
        {
            // FUNC 2.1 buildMetadataKeySet
        }

        #endregion
    }
}
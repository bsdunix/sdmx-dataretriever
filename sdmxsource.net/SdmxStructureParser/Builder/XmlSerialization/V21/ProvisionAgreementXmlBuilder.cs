// -----------------------------------------------------------------------
// <copyright file="ProvisionAgreementXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    /// <summary>
    ///     The provision agreement xml bean builder.
    /// </summary>
    public class ProvisionAgreementXmlBuilder : MaintainableAssembler, 
                                                IBuilder<ProvisionAgreementType, IProvisionAgreementObject>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Build a <see cref="ProvisionAgreementType"/> from <paramref name="buildFrom"/> and return it
        /// </summary>
        /// <param name="buildFrom">
        /// The <see cref="IProvisionAgreementObject"/>
        /// </param>
        /// <returns>
        /// The <see cref="ProvisionAgreementType"/>.
        /// </returns>
        public virtual ProvisionAgreementType Build(IProvisionAgreementObject buildFrom)
        {
            // Create outgoing build
            var builtObj = new ProvisionAgreementType();

            // Populate it from inherited super
            this.AssembleMaintainable(builtObj, buildFrom);
            if (buildFrom.StructureUseage != null)
            {
                var structureUSeageType = new StructureUsageReferenceType();
                builtObj.StructureUsage = structureUSeageType;
                var structureUsageRefType = new StructureUsageRefType();
                structureUSeageType.SetTypedRef(structureUsageRefType);
                this.SetReference(structureUsageRefType, buildFrom.StructureUseage);
            }

            if (buildFrom.DataproviderRef != null)
            {
                var dataProviderRef = new DataProviderReferenceType();
                builtObj.DataProvider = dataProviderRef;
                var dataProviderRefType = new DataProviderRefType();
                dataProviderRef.SetTypedRef(dataProviderRefType);
                this.SetReference(dataProviderRefType, buildFrom.DataproviderRef);
            }

            return builtObj;
        }

        #endregion
    }
}
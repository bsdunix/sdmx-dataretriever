// -----------------------------------------------------------------------
// <copyright file="CategorySchemeXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    /// <summary>
    ///     The category scheme xml bean builder.
    /// </summary>
    public class CategorySchemeXmlBuilder : ItemSchemeAssembler, IBuilder<CategorySchemeType, ICategorySchemeObject>
    {
        #region Fields

        /// <summary>
        ///     The category bean assembler bean.
        /// </summary>
        private readonly CategoryAssembler _categoryAssemblerBean = new CategoryAssembler();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="CategorySchemeType"/>.
        /// </returns>
        public virtual CategorySchemeType Build(ICategorySchemeObject buildFrom)
        {
            // Create outgoing build
            var builtObj = new CategorySchemeType();

            // Populate it from inherited super
            this.AssembleItemScheme(builtObj, buildFrom);

            // Populate it using this class's specifics
            foreach (ICategoryObject eachCat in buildFrom.Items)
            {
                var newCat = new Category();
                builtObj.Item.Add(newCat);
                this._categoryAssemblerBean.Assemble(newCat.Content, eachCat);
                this.AssembleChildCategories(newCat.Content, eachCat);
            }

            return builtObj;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Recursively pickup and assemble child Categories of Categories
        /// </summary>
        /// <param name="catType">
        /// parent destination Category xml bean
        /// </param>
        /// <param name="catBean">
        /// parent source Category bean
        /// </param>
        private void AssembleChildCategories(CategoryType catType, ICategoryObject catBean)
        {
            var stack = new Stack<KeyValuePair<CategoryType, ICategoryObject>>();
            stack.Push(new KeyValuePair<CategoryType, ICategoryObject>(catType, catBean));

            while (stack.Count > 0)
            {
                 var current = stack.Pop();
                catType = current.Key;
                catBean = current.Value;
                foreach (ICategoryObject eachCat in catBean.Items)
                {
                    var newCat = new Category();
                    catType.Item.Add(newCat);
                    this._categoryAssemblerBean.Assemble(newCat.Content, eachCat);
                    stack.Push(new KeyValuePair<CategoryType, ICategoryObject>(newCat.Content, eachCat));

                    ////this.AssembleChildCategories(newCat, eachCat);
                }
            }
        }

        #endregion
    }
}
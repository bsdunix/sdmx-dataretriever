// -----------------------------------------------------------------------
// <copyright file="AgencySchemeXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    /// <summary>
    ///     The agency scheme xml bean builder.
    /// </summary>
    public class AgencySchemeXmlBuilder : ItemSchemeAssembler, IBuilder<AgencySchemeType, IAgencyScheme>
    {
        /// <summary>
        /// The organisation xml assembler.
        /// </summary>
        private readonly OrganisationXmlAssembler organisationXmlAssembler = new OrganisationXmlAssembler(); //TODO: In java the field is not instantieted

        #region Public Methods and Operators

        /// <summary>
        /// Builds and returns the
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="AgencySchemeType"/>.
        /// </returns>
        public virtual AgencySchemeType Build(IAgencyScheme buildFrom)
        {
            var returnType = new AgencySchemeType();
            this.AssembleItemScheme(returnType, buildFrom);
            if (buildFrom.Items != null)
            {
                foreach (IAgency currentBean in buildFrom.Items)
                {
                    var agencyType = new Agency();
                    returnType.Item.Add(agencyType);
                    organisationXmlAssembler.Assemble(agencyType, currentBean);
                    this.AssembleNameable(agencyType.Content, currentBean);
                }
               
            }

            return returnType;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="SubmitRegistrationResponseBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Response
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.SubmissionResponse;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SubmissionResponse;

    using StatusMessageType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.StatusMessageType;

    /// <summary>
    ///     The submit registration response builder v 21.
    /// </summary>
    public class SubmitRegistrationResponseBuilderV21 : IBuilder<IList<ISubmitRegistrationResponse>, RegistryInterface>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds <see cref="ISubmitRegistrationResponse"/> list from <paramref name="registryInterface"/>
        /// </summary>
        /// <param name="registryInterface">
        /// The registryInterface.
        /// </param>
        /// <returns>
        /// The list of <see cref="ISubmitRegistrationResponse"/>
        /// </returns>
        public virtual IList<ISubmitRegistrationResponse> Build(RegistryInterface registryInterface)
        {
            // TODO REFACTOR - THIS IS VERY SIMILAR TO SUBMIT SUBSCRIPTION RESPONSE
            IList<ISubmitRegistrationResponse> returnList = new List<ISubmitRegistrationResponse>();

            foreach (
                RegistrationStatusType registrationStatusType in
                    registryInterface.Content.SubmitRegistrationsResponse.RegistrationStatus)
            {
                if (registrationStatusType.StatusMessage != null && registrationStatusType.StatusMessage.status != null)
                {
                    IList<string> messages = new List<string>();
                    if (registrationStatusType.StatusMessage.MessageText != null)
                    {
                        // TODO Message Codes and Multilingual
                        foreach (StatusMessageType statusMessageType in registrationStatusType.StatusMessage.MessageText)
                        {
                            if (statusMessageType.Text != null)
                            {
                                foreach (TextType textType in statusMessageType.Text)
                                {
                                    messages.Add(textType.TypedValue);
                                }
                            }
                        }
                    }

                    IErrorList errors = null;
                    switch (registrationStatusType.StatusMessage.status)
                    {
                        case StatusTypeConstants.Failure:
                            errors = new ErrorListCore(messages, false);
                            break;
                        case StatusTypeConstants.Warning:
                            errors = new ErrorListCore(messages, true);
                            break;
                    }

                    IRegistrationObject registration = new RegistrationObjectCore(registrationStatusType.Registration);
                    returnList.Add(new SubmitRegistrationResponseImpl(registration, errors));
                }
            }

            return returnList;
        }

        #endregion
    }
}
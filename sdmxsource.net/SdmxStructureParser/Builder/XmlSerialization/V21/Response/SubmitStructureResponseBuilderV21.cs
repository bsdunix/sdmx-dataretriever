// -----------------------------------------------------------------------
// <copyright file="SubmitStructureResponseBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Response
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SubmissionResponse;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SubmissionResponse;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;

    using StatusMessageType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.StatusMessageType;

    /// <summary>
    ///     The submit structure response builder v 21.
    /// </summary>
    public class SubmitStructureResponseBuilderV21
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds the list of <see cref="ISubmitStructureResponse"/> from <paramref name="registryInterface"/>
        /// </summary>
        /// <param name="registryInterface">
        /// The registry Interface.
        /// </param>
        /// <returns>
        /// The list of <see cref="ISubmitStructureResponse"/> from <paramref name="registryInterface"/>
        /// </returns>
        public IList<ISubmitStructureResponse> Build(RegistryInterface registryInterface)
        {
            // TODO REFACTOR - THIS IS VERY SIMILAR TO SUBMIT SUBSCRIPTION RESPONSE
            IList<ISubmitStructureResponse> returnList = new List<ISubmitStructureResponse>();

            /* foreach */
            foreach (
                SubmissionResultType resultType in registryInterface.Content.SubmitStructureResponse.SubmissionResult)
            {
                IStructureReference structureReference =
                    RefUtil.CreateReference(resultType.SubmittedStructure.MaintainableObject);
                if (resultType.StatusMessage != null && resultType.StatusMessage.status != null)
                {
                    IList<string> messages = new List<string>();
                    if (resultType.StatusMessage.MessageText != null)
                    {
                        // TODO Message Codes and Multilingual
                        foreach (StatusMessageType statusMessageType in resultType.StatusMessage.MessageText)
                        {
                            if (statusMessageType.Text != null)
                            {
                                /* foreach */
                                foreach (TextType tt in statusMessageType.Text)
                                {
                                    messages.Add(tt.TypedValue);
                                }
                            }
                        }
                    }

                    IErrorList errors = null;
                    switch (resultType.StatusMessage.status)
                    {
                        case StatusTypeConstants.Failure:
                            errors = new ErrorListCore(messages, false);
                            break;
                        case StatusTypeConstants.Warning:
                            errors = new ErrorListCore(messages, true);
                            break;
                    }

                    returnList.Add(new SubmitStructureResponseImpl(structureReference, errors));
                }
            }

            return returnList;
        }

        #endregion
    }
}
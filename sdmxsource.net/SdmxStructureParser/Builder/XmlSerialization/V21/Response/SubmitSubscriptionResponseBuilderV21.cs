// -----------------------------------------------------------------------
// <copyright file="SubmitSubscriptionResponseBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Response
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SubmissionResponse;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SubmissionResponse;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    using StatusMessageType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.StatusMessageType;

    /// <summary>
    ///     Parses and SDMX Document to build the submit subscription response, can either be a RegistryInterfaceDocument or a
    ///     SubmitSubscriptionResponse Document
    /// </summary>
    public class SubmitSubscriptionResponseBuilderV21 : IBuilder<IList<ISubmitSubscriptionResponse>, RegistryInterface>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds and returns a list of <see cref="ISubmitSubscriptionResponse"/> from the specified
        ///     <paramref name="registryInterface"/>
        /// </summary>
        /// <param name="registryInterface">
        /// The registry Interface.
        /// </param>
        /// <returns>
        /// returns a list of <see cref="ISubmitSubscriptionResponse"/> from the specified <paramref name="registryInterface"/>
        /// </returns>
        public virtual IList<ISubmitSubscriptionResponse> Build(RegistryInterface registryInterface)
        {
            IList<ISubmitSubscriptionResponse> returnList = new List<ISubmitSubscriptionResponse>();

            foreach (SubscriptionStatusType subscriptionStatusType in
                registryInterface.Content.SubmitSubscriptionsResponse.SubscriptionStatus)
            {
                if (subscriptionStatusType.StatusMessage != null && subscriptionStatusType.StatusMessage.status != null)
                {
                    IList<string> messages = new List<string>();
                    if (subscriptionStatusType.StatusMessage.MessageText != null)
                    {
                        // TODO Message Codes and Multilingual
                        foreach (StatusMessageType statusMessageType in subscriptionStatusType.StatusMessage.MessageText)
                        {
                            if (statusMessageType.Text != null)
                            {
                                foreach (TextType textType in statusMessageType.Text)
                                {
                                    messages.Add(textType.TypedValue);
                                }
                            }
                        }
                    }

                    IErrorList errors = null;
                    switch (subscriptionStatusType.StatusMessage.status)
                    {
                        case StatusTypeConstants.Failure:
                            errors = new ErrorListCore(messages, false);
                            break;
                        case StatusTypeConstants.Warning:
                            errors = new ErrorListCore(messages, true);
                            break;
                    }

                    Uri urn = subscriptionStatusType.SubscriptionURN;
                    IStructureReference structureReference = null;
                    if (urn != null)
                    {
                        structureReference = new StructureReferenceImpl(urn);
                    }

                    returnList.Add(
                        new SubmitSubscriptionResponseImpl(
                            structureReference, errors, subscriptionStatusType.SubscriberAssignedID));
                }
            }

            return returnList;
        }

        #endregion
    }
}
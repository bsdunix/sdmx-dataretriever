// -----------------------------------------------------------------------
// <copyright file="ProcessXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    /// <summary>
    ///     The process xml bean builder.
    /// </summary>
    public class ProcessXmlBuilder : MaintainableAssembler, IBuilder<ProcessType, IProcessObject>
    {
        #region Fields

        /// <summary>
        ///     The process step bean assembler bean.
        /// </summary>
        private readonly ProcessStepAssembler _processStepAssemblerBean = new ProcessStepAssembler();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Build a <see cref="ProcessType"/> from <paramref name="buildFrom"/> and return it
        /// </summary>
        /// <param name="buildFrom">
        /// The <see cref="IProcessObject"/>
        /// </param>
        /// <returns>
        /// The <see cref="ProcessType"/>.
        /// </returns>
        public virtual ProcessType Build(IProcessObject buildFrom)
        {
            // Create outgoing build
            var builtObj = new ProcessType();

            // Populate it from inherited super
            this.AssembleMaintainable(builtObj, buildFrom);

            // Populate it using this class's specifics
            foreach (IProcessStepObject eachProcessStepBean in buildFrom.ProcessSteps)
            {
                var newProcessStepType = new ProcessStepType();
                builtObj.ProcessStep.Add(newProcessStepType);
                this._processStepAssemblerBean.Assemble(newProcessStepType, eachProcessStepBean);
            }

            return builtObj;
        }

        #endregion
    }
}
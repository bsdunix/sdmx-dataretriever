// -----------------------------------------------------------------------
// <copyright file="DataProviderSchemeXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    /// <summary>
    ///     The data provider scheme xml bean builder.
    /// </summary>
    public class DataProviderSchemeXmlBuilder : ItemSchemeAssembler,
                                                IBuilder<DataProviderSchemeType, IDataProviderScheme>
    {
        /// <summary>
        /// The organisation XML assembler
        /// </summary>
        private readonly OrganisationXmlAssembler _organisationXmlAssembler;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataProviderSchemeXmlBuilder"/> class.
        /// </summary>
        public DataProviderSchemeXmlBuilder()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataProviderSchemeXmlBuilder"/> class.
        /// </summary>
        /// <param name="organisationXmlAssembler">The organisation XML assembler.</param>
        public DataProviderSchemeXmlBuilder(OrganisationXmlAssembler organisationXmlAssembler)
        {
            this._organisationXmlAssembler = organisationXmlAssembler ?? new OrganisationXmlAssembler();
        }

        #region Public Methods and Operators

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="DataProviderSchemeType"/>.
        /// </returns>
        public virtual DataProviderSchemeType Build(IDataProviderScheme buildFrom)
        {
            var returnType = new DataProviderSchemeType();

            if (buildFrom.Partial)
                returnType.isPartial = true;

            this.AssembleItemScheme(returnType, buildFrom);
            if (buildFrom.Items != null)
            {
                /* foreach */
                foreach (IDataProvider item in buildFrom.Items)
                {
                    var dataProviderType = new DataProvider();
                    returnType.Item.Add(dataProviderType);
                    _organisationXmlAssembler.Assemble(dataProviderType, item);
                    this.AssembleNameable(dataProviderType.Content, item);
                }
            }

            return returnType;
        }

        #endregion
    }
}
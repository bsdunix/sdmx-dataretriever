// -----------------------------------------------------------------------
// <copyright file="AbstractResponseBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.registry;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The abstract response builder.
    /// </summary>
    public abstract class AbstractResponseBuilder
    {
        // DEFAULT CONSTRUCTOR
        #region Public Methods and Operators

        /// <summary>
        /// Add data source from <paramref name="datasourceBean"/> to <paramref name="datasourceType"/>
        /// </summary>
        /// <param name="datasourceBean">
        /// The data source SDMX Object.
        /// </param>
        /// <param name="datasourceType">
        /// The data source LINQ2XSD object.
        /// </param>
        public void AddDatasource(IDataSource datasourceBean, DatasourceType datasourceType)
        {
            if (datasourceBean.SimpleDatasource)
            {
                if (datasourceBean.DataUrl != null)
                {
                    datasourceType.SimpleDatasource = datasourceBean.DataUrl;
                }
            }
            else
            {
                var queryableDatasourceType = new QueryableDatasourceType();
                datasourceType.QueryableDatasource = queryableDatasourceType;
                if (datasourceBean.DataUrl != null)
                {
                    queryableDatasourceType.DataUrl = datasourceBean.DataUrl;
                }

                queryableDatasourceType.isRESTDatasource = datasourceBean.RESTDatasource;
                queryableDatasourceType.isWebServiceDatasource = datasourceBean.WebServiceDatasource;
                if (datasourceBean.WsdlUrl != null)
                {
                    queryableDatasourceType.WSDLUrl = datasourceBean.WsdlUrl;
                }
            }
        }

        /// <summary>
        /// Add status message from <paramref name="ex"/> to <paramref name="statusMessage"/>
        /// </summary>
        /// <param name="statusMessage">
        /// The status message
        /// </param>
        /// <param name="ex">
        /// The exception
        /// </param>
        public void AddStatus(StatusMessageType statusMessage, Exception ex)
        {
            if (ex == null)
            {
                statusMessage.status = StatusTypeConstants.Success;
            }
            else
            {
                statusMessage.status = StatusTypeConstants.Failure;
                var tt = new TextType();
                statusMessage.MessageText.Add(tt);

                var exception = ex as SdmxException;
                tt.TypedValue = exception != null ? exception.FullMessage : ex.Message;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The has annotations.
        /// </summary>
        /// <param name="annotable">
        /// The annotable.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        internal bool HasAnnotations(IAnnotableObject annotable)
        {
            if (ObjectUtil.ValidCollection(annotable.Annotations))
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}
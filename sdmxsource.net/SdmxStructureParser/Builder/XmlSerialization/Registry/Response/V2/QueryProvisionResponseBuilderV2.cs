// -----------------------------------------------------------------------
// <copyright file="QueryProvisionResponseBuilderV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The query provision response builder v 2.
    /// </summary>
    public class QueryProvisionResponseBuilderV2 : AbstractResponseBuilder
    {
        #region Static Fields

        /// <summary>
        ///     The instance.
        /// </summary>
        private static readonly QueryProvisionResponseBuilderV2 _instance = new QueryProvisionResponseBuilderV2();

        #endregion

        #region Fields

        /// <summary>
        ///     The Provision Agreement builder.
        /// </summary>
        private readonly ProvisionAgreementXmlBuilder _provBuilder;

        #endregion

        // PRIVATE CONSTRUCTOR
        #region Constructors and Destructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="QueryProvisionResponseBuilderV2" /> class from being created.
        /// </summary>
        private QueryProvisionResponseBuilderV2()
        {
            this._provBuilder = ProvisionAgreementXmlBuilder.Instance;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static QueryProvisionResponseBuilderV2 Instance
        {
            get
            {
                return _instance;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Build error response.
        /// </summary>
        /// <param name="th">
        /// The exception.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        public RegistryInterface BuildErrorResponse(Exception th)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new QueryProvisioningResponseType();
            regInterface.QueryProvisioningResponse = returnType;
            V2Helper.Header = regInterface;

            returnType.StatusMessage = new StatusMessageType();

            this.AddStatus(returnType.StatusMessage, th);

            return responseType;
        }

        /// <summary>
        /// The build success response.
        /// </summary>
        /// <param name="provisions">
        /// The provisions.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        public RegistryInterface BuildSuccessResponse(ICollection<IProvisionAgreementObject> provisions)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new QueryProvisioningResponseType();
            regInterface.QueryProvisioningResponse = returnType;
            V2Helper.Header = regInterface;

            var statusMessage = new StatusMessageType();
            returnType.StatusMessage = statusMessage;

            this.AddStatus(returnType.StatusMessage, null);

            if (!ObjectUtil.ValidCollection(provisions))
            {
                statusMessage.status = StatusTypeConstants.Warning;
                var tt = new TextType();
                statusMessage.MessageText.Add(tt);

                tt.TypedValue = "No Provisions Match The Query Parameters";
            }
            else
            {
                statusMessage.status = StatusTypeConstants.Success;
                var provTypes = new ProvisionAgreementType[provisions.Count];

                int i = 0;

                /* foreach */
                foreach (IProvisionAgreementObject currentProv in provisions)
                {
                    provTypes[i] = this._provBuilder.Build(currentProv);
                    i++;
                }

                returnType.ProvisionAgreement = provTypes;
            }

            return responseType;
        }

        #endregion
    }
}
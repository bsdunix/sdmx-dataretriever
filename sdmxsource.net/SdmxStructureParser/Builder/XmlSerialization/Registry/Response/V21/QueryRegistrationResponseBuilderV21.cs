// -----------------------------------------------------------------------
// <copyright file="QueryRegistrationResponseBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21;
    using Org.Sdmxsource.Util;

    using QueryRegistrationResponseType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.QueryRegistrationResponseType;
    using StatusMessageType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.StatusMessageType;

    /// <summary>
    ///     The query registration response builder v 21.
    /// </summary>
    public class QueryRegistrationResponseBuilderV21 : AbstractResponseBuilder
    {
        #region Fields

        /// <summary>
        ///     The registration xml bean builder.
        /// </summary>
        private readonly RegistrationXmlBuilder _registrationXmlBuilder = new RegistrationXmlBuilder();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Builds the error response.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        public RegistryInterface BuildErrorResponse(Exception exception)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new QueryRegistrationResponseType();
            regInterface.QueryRegistrationResponse = returnType;
            V21Helper.Header = regInterface;

            var statusMessage = new StatusMessageType();
            returnType.StatusMessage = statusMessage;
            this.AddStatus(statusMessage, exception);

            return responseType;
        }

        /// <summary>
        /// Builds the success response.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the <paramref name="registrations"/> collection
        /// </typeparam>
        /// <param name="registrations">
        /// The registrations.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        public RegistryInterface BuildSuccessResponse<T>(ICollection<T> registrations)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new QueryRegistrationResponseType();
            regInterface.QueryRegistrationResponse = returnType;
            V21Helper.Header = regInterface;
            var statusMessage = new StatusMessageType();
            returnType.StatusMessage = statusMessage;
            if (!ObjectUtil.ValidCollection(registrations))
            {
                // FUNC 2.1 - add warning to header?
                statusMessage.status = StatusTypeConstants.Warning;
                var statusMessageType = new Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.StatusMessageType();
                statusMessage.MessageText.Add(statusMessageType);
                var textType = new TextType();
                statusMessageType.Text.Add(textType);
                textType.TypedValue = "No Registrations Match The Query Parameters";
            }
            else
            {
                statusMessage.status = StatusTypeConstants.Success;

                /* foreach */
                foreach (IRegistrationObject currentRegistration in registrations)
                {
                    var queryResult = new QueryResultType();
                    returnType.QueryResult.Add(queryResult);
                    queryResult.timeSeriesMatch = false; // FUNC 1 - when is this true?  Also We need MetadataResult

                    var resultType = new ResultType();
                    queryResult.DataResult = resultType;
                    RegistrationType registrationType = this._registrationXmlBuilder.Build(currentRegistration);
                    resultType.Registration = registrationType;
                }
            }

            return responseType;
        }

        #endregion
    }
}
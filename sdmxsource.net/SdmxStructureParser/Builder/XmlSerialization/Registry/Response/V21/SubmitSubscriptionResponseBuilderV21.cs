// -----------------------------------------------------------------------
// <copyright file="SubmitSubscriptionResponseBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    using SubmitSubscriptionsResponseType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.SubmitSubscriptionsResponseType;

    /// <summary>
    ///     The submit subscription response builder v 21.
    /// </summary>
    public class SubmitSubscriptionResponseBuilderV21 : AbstractResponseBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds the error response.
        /// </summary>
        /// <param name="notification">
        /// The notification.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        public RegistryInterface BuildErrorResponse(ISubscriptionObject notification, Exception exception)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            V21Helper.Header = regInterface;
            var returnType = new SubmitSubscriptionsResponseType();
            regInterface.SubmitSubscriptionsResponse = returnType;
            var subscriptionStatus = new SubscriptionStatusType();
            returnType.SubscriptionStatus.Add(subscriptionStatus);
            if (notification != null)
            {
                subscriptionStatus.SubscriptionURN = notification.Urn;
            }

            var statusMessageType = new StatusMessageType();
            subscriptionStatus.StatusMessage = statusMessageType;
            this.AddStatus(statusMessageType, exception);
            return responseType;
        }

        /// <summary>
        /// The build success response.
        /// </summary>
        /// <param name="notifications">
        /// The notifications.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        public RegistryInterface BuildSuccessResponse(ICollection<ISubscriptionObject> notifications)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            V21Helper.Header = regInterface;

            /* foreach */
            foreach (ISubscriptionObject currentNotification in notifications)
            {
                var returnType = new SubmitSubscriptionsResponseType();
                regInterface.SubmitSubscriptionsResponse = returnType;
                var subscriptionStatus = new SubscriptionStatusType();
                returnType.SubscriptionStatus.Add(subscriptionStatus);
                var statusMessageType = new StatusMessageType();
                subscriptionStatus.StatusMessage = statusMessageType;

                this.AddStatus(statusMessageType, null);

                subscriptionStatus.SubscriptionURN = currentNotification.Urn;
            }

            return responseType;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="SubmitRegistrationResponseBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    using QueryableDataSourceType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.QueryableDataSourceType;
    using StatusMessageType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.StatusMessageType;
    using SubmitRegistrationsResponseType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.SubmitRegistrationsResponseType;

    /// <summary>
    ///     The submit registration response builder v 21.
    /// </summary>
    public class SubmitRegistrationResponseBuilderV21 : AbstractResponseBuilder
    {
        #region Static Fields

        /// <summary>
        ///     The instance.
        /// </summary>
        private static readonly SubmitRegistrationResponseBuilderV21 _instance =
            new SubmitRegistrationResponseBuilderV21();

        #endregion

        // PRIVATE CONSTRUCTOR
        #region Constructors and Destructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="SubmitRegistrationResponseBuilderV21" /> class from being created.
        /// </summary>
        private SubmitRegistrationResponseBuilderV21()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static SubmitRegistrationResponseBuilderV21 Instance
        {
            get
            {
                return _instance;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The build error response.
        /// </summary>
        /// <param name="th">
        /// The exception.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        public RegistryInterface BuildErrorResponse(Exception th)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new SubmitRegistrationsResponseType();
            regInterface.SubmitRegistrationsResponse = returnType;
            V21Helper.Header = regInterface;
            var registrationStatusType = new RegistrationStatusType();
            returnType.RegistrationStatus.Add(registrationStatusType);
            this.AddStatus(registrationStatusType.StatusMessage = new StatusMessageType(), th);
            return responseType;
        }

        /// <summary>
        /// The build response.
        /// </summary>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        public RegistryInterface BuildResponse(IDictionary<IRegistrationObject, Exception> response)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new SubmitRegistrationsResponseType();
            regInterface.SubmitRegistrationsResponse = returnType;
            V21Helper.Header = regInterface;

            foreach (KeyValuePair<IRegistrationObject, Exception> registration in response)
            {
                this.ProcessResponse(returnType, registration.Key, registration.Value);
            }

            return responseType;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process the response.
        /// </summary>
        /// <param name="returnType">
        /// The return type.
        /// </param>
        /// <param name="registrationBean">
        /// The registration bean.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        private void ProcessResponse(
            SubmitRegistrationsResponseType returnType, IRegistrationObject registrationBean, Exception exception)
        {
            var registrationStatusType = new RegistrationStatusType();
            returnType.RegistrationStatus.Add(registrationStatusType);
            var statusMessageType = new StatusMessageType();
            registrationStatusType.StatusMessage = statusMessageType;

            this.AddStatus(statusMessageType, exception);
            var registrationType = new RegistrationType();
            registrationStatusType.Registration = registrationType;
            registrationType.id = registrationBean.Id;
            if (registrationBean.DataSource != null)
            {
                IDataSource datasourceBean = registrationBean.DataSource;
                var datasourceType = new DataSourceType();
                registrationType.Datasource = datasourceType;

                if (datasourceBean.SimpleDatasource)
                {
                    Uri simpleDatasourceType = datasourceBean.DataUrl;
                    datasourceType.SimpleDataSource.Add(simpleDatasourceType);
                }
                else
                {
                    var queryableDatasource = new QueryableDataSourceType();
                    datasourceType.QueryableDataSource.Add(queryableDatasource);
                    queryableDatasource.isRESTDatasource = datasourceBean.RESTDatasource;
                    queryableDatasource.isWebServiceDatasource = datasourceBean.WebServiceDatasource;
                    queryableDatasource.DataURL = datasourceBean.DataUrl;
                    if (datasourceBean.WsdlUrl != null)
                    {
                        queryableDatasource.WSDLURL = datasourceBean.WsdlUrl;
                    }

                    if (datasourceBean.WadlUrl != null)
                    {
                        queryableDatasource.WADLURL = datasourceBean.WadlUrl;
                    }
                }
            }

            if (registrationBean.ProvisionAgreementRef != null)
            {
                var refType = new ProvisionAgreementReferenceType();
                registrationType.ProvisionAgreement = refType;
                refType.URN.Add(registrationBean.ProvisionAgreementRef.TargetUrn);
            }
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="ErrorResponseBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21
{
    using System;
    using System.Globalization;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.TextType;

    /// <summary>
    ///     The error response builder.
    /// </summary>
    public class ErrorResponseBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        /// Build error response.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <returns>
        /// The <see cref="Error"/>.
        /// </returns>
        public Error BuildErrorResponse(Exception exception)
        {
            var errorDoc = new Error();
            ErrorType error = errorDoc.Content;

            // FUNC Standard error codes
            var errorMessage = new CodedStatusMessageType();
            error.ErrorMessage.Add(errorMessage);
            errorMessage.code = "1000";
            var text = new TextType();
            errorMessage.Text.Add(text);
            var uncheckedException = exception as SdmxException;
            text.TypedValue = uncheckedException != null ? uncheckedException.FullMessage : exception.Message;

            return errorDoc;
        }

        /// <summary>
        /// Build error response.
        /// </summary>
        /// <param name="errorCode">
        /// The error code.
        /// </param>
        /// <returns>
        /// The <see cref="Error"/>.
        /// </returns>
        public Error BuildErrorResponse(SdmxErrorCodeEnumType errorCode)
        {
            return this.BuildErrorResponse(SdmxErrorCode.GetFromEnum(errorCode));
        }

        /// <summary>
        /// Build error response.
        /// </summary>
        /// <param name="errorCode">
        /// The error code.
        /// </param>
        /// <returns>
        /// The <see cref="Error"/>.
        /// </returns>
        public Error BuildErrorResponse(SdmxErrorCode errorCode)
        {
            var errorDoc = new Error();
            ErrorType error = errorDoc.Content;

            // FUNC Standard error codes
            var errorMessage = new CodedStatusMessageType();
            error.ErrorMessage.Add(errorMessage);
            errorMessage.code = errorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture);
            var text = new TextType();
            errorMessage.Text.Add(text);
            text.TypedValue = errorCode.ErrorString;
            return errorDoc;
        }

        #endregion
    }
}
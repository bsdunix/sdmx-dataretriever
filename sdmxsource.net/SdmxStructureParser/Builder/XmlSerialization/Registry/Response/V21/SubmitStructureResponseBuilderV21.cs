// -----------------------------------------------------------------------
// <copyright file="SubmitStructureResponseBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21;
    using Org.Sdmxsource.Util;

    using StatusMessageType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.StatusMessageType;
    using SubmitStructureResponseType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.SubmitStructureResponseType;

    /// <summary>
    ///     The submit structure response builder v 21.
    /// </summary>
    public class SubmitStructureResponseBuilderV21 : AbstractResponseBuilder
    {
        #region Fields

        /// <summary>
        ///     The header xml beans builder.
        /// </summary>
        private readonly StructureHeaderXmlBuilder<BasicHeaderType> _headerXmlsBuilder =
            new StructureHeaderXmlBuilder<BasicHeaderType>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Build  the error response.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="errorBean">
        /// The error bean.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        /// <exception cref="BuilderException">
        /// Registry could not determine Maintainable Artefact in error
        /// </exception>
        public RegistryInterface BuildErrorResponse(Exception exception, IStructureReference errorBean)
        {
            if (errorBean == null)
            {
                throw new SdmxSemmanticException("Registry could not determine Maintainable Artefact in error", exception);
            }

            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            V21Helper.Header = regInterface;
            var returnType = new SubmitStructureResponseType();
            regInterface.SubmitStructureResponse = returnType;
            this.AddSubmissionResult(returnType, errorBean, exception);
            return responseType;
        }

        /// <summary>
        /// The build success response.
        /// </summary>
        /// <param name="beans">
        /// The beans.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        public RegistryInterface BuildSuccessResponse(ISdmxObjects beans)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            regInterface.Header = this._headerXmlsBuilder.Build(beans.Header);
            var returnType = new SubmitStructureResponseType();
            regInterface.SubmitStructureResponse = returnType;


            this.ProcessMaintainables(returnType, beans.GetAllMaintainables());
            return responseType;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds submission result.
        /// </summary>
        /// <param name="returnType">
        /// The return type.
        /// </param>
        /// <param name="structureReference">
        /// The structure reference.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        private void AddSubmissionResult(
            SubmitStructureResponseType returnType, IStructureReference structureReference, Exception exception)
        {
            var submissionResult = new SubmissionResultType();
            returnType.SubmissionResult.Add(submissionResult);
            var statusMessageType = new StatusMessageType();
            submissionResult.StatusMessage = statusMessageType;
            this.AddStatus(statusMessageType, exception);
            var submittedStructure = new SubmittedStructureType();
            submissionResult.SubmittedStructure = submittedStructure;
            var refType = new MaintainableReferenceType();
            submittedStructure.MaintainableObject = refType;
            if (ObjectUtil.ValidString(structureReference.MaintainableUrn))
            {
                refType.URN.Add(structureReference.MaintainableUrn);
            }
            else
            {
                var xref = new MaintainableRefType();
                refType.SetTypedRef(xref);
                IMaintainableRefObject maintainableReference = structureReference.MaintainableReference;
                string value = maintainableReference.AgencyId;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    xref.agencyID = maintainableReference.AgencyId;
                }

                string value1 = maintainableReference.MaintainableId;
                if (!string.IsNullOrWhiteSpace(value1))
                {
                    xref.agencyID = maintainableReference.MaintainableId;
                }

                string value2 = maintainableReference.Version;
                if (!string.IsNullOrWhiteSpace(value2))
                {
                    xref.agencyID = maintainableReference.Version;
                }
            }
        }

        /// <summary>
        /// Process the maintainable.
        /// </summary>
        /// <param name="returnType">
        /// The return type.
        /// </param>
        /// <param name="maintainableObjects">
        /// The maintainable Objects.
        /// </param>
        private void ProcessMaintainables(
            SubmitStructureResponseType returnType, IEnumerable<IMaintainableObject> maintainableObjects)
        {
            foreach (IMaintainableObject maint in maintainableObjects)
            {
                this.AddSubmissionResult(returnType, maint.AsReference, null);
            }
        }

        #endregion
    }
}
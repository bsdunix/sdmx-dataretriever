// -----------------------------------------------------------------------
// <copyright file="QueryStructureResponseBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21;

    using Xml.Schema.Linq;

    /// <summary>
    ///     Builds error and success responses for querying structures.
    /// </summary>
    public class QueryStructureResponseBuilder : XmlObjectBuilder, IQueryStructureResponseBuilder
    {
        #region Fields

        /// <summary>
        ///     The error response builder 21.
        /// </summary>
        private readonly ErrorResponseBuilder _errorResponseBuilder21 = new ErrorResponseBuilder();

        /// <summary>
        ///     The Query structure request v 2 builder.
        /// </summary>
        private readonly QueryStructureResponseBuilderV2 _queryStructureResponseBuilderV2 =
            new QueryStructureResponseBuilderV2();

        /// <summary>
        ///     The structure v 21 builder.
        /// </summary>
        private readonly StructureXmlBuilder _structV21Builder = new StructureXmlBuilder();

        /// <summary>
        ///     The structure 1 builder.
        /// </summary>
        private readonly XmlSerialization.V1.StructureXmlBuilder _structv1Builder =
            new XmlSerialization.V1.StructureXmlBuilder();

        /// <summary>
        ///     The structure v2 builder.
        /// </summary>
        private readonly XmlSerialization.V2.StructureXmlBuilder _structv2Builder =
            new XmlSerialization.V2.StructureXmlBuilder();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Returns an error response based on the exception
        /// </summary>
        /// <param name="exception">
        /// - the error
        /// </param>
        /// <param name="schemaVersion">
        /// - the version of the schema to output the response in
        /// </param>
        /// <returns>
        /// The <see cref="XTypedElement"/>.
        /// </returns>
        public virtual XTypedElement BuildErrorResponse(Exception exception, SdmxSchemaEnumType schemaVersion)
        {
            XTypedElement response = null;
            switch (schemaVersion)
            {
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    response = this._errorResponseBuilder21.BuildErrorResponse(exception);
                    break;
                case SdmxSchemaEnumType.VersionTwo:
                    response = this._queryStructureResponseBuilderV2.BuildErrorResponse(exception);
                    break;
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, schemaVersion);
            }
            base.WriteSchemaLocation(response, schemaVersion);
            return response;
        }

        /// <summary>
        /// Builds a success response along with the query results
        /// </summary>
        /// <param name="beans">
        /// - the beans that were successfully returned from the query
        /// </param>
        /// <param name="schemaVersion">
        /// - the version of the schema to output the response in
        /// </param>
        /// <param name="returnAsStructureMessage">
        /// returns a structure message if true, otherwise returns as a query structure response
        /// </param>
        /// <returns>
        /// The <see cref="XTypedElement"/>.
        /// </returns>
        public virtual XTypedElement BuildSuccessResponse(
            ISdmxObjects beans, SdmxSchemaEnumType schemaVersion, bool returnAsStructureMessage)
        {
            XTypedElement response = null;
            switch (schemaVersion)
            {
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    if (beans.GetAllMaintainables().Count == 0)
                    {
                        response = this._errorResponseBuilder21.BuildErrorResponse(SdmxErrorCodeEnumType.NoResultsFound);
                    }

                    response = this._structV21Builder.Build(beans);
                    break;
                case SdmxSchemaEnumType.VersionTwo:
                    if (returnAsStructureMessage)
                    {
                        response = this._structv2Builder.Build(beans);
                    }
                    else
                    {
                        response = this._queryStructureResponseBuilderV2.BuildSuccessResponse(beans);
                    }
                    break;
                case SdmxSchemaEnumType.VersionOne:
                    response = this._structv1Builder.Build(beans);
                    break;
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, schemaVersion);
            }
            base.WriteSchemaLocation(response, schemaVersion);
            return response;
        }

        #endregion
    }
}
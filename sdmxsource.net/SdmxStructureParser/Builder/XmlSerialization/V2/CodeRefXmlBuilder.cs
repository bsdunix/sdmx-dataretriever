// -----------------------------------------------------------------------
// <copyright file="CodeRefXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The code ref xml bean builder.
    /// </summary>
    public class CodeRefXmlBuilder : AbstractBuilder, IBuilder<CodeRefType, IHierarchicalCode>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Build <see cref="CodeRefType"/> from <paramref name="buildFrom"/>.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="CodeRefType"/> from <paramref name="buildFrom"/> .
        /// </returns>
        public virtual CodeRefType Build(IHierarchicalCode buildFrom)
        {
            var pairs = new Stack<KeyValuePair<CodeRefType, IHierarchicalCode>>();
            var root = new CodeRefType();
            pairs.Push(new KeyValuePair<CodeRefType, IHierarchicalCode>(root, buildFrom));

            while (pairs.Count > 0)
            {
                KeyValuePair<CodeRefType, IHierarchicalCode> kv = pairs.Pop();
                buildFrom = kv.Value;
                CodeRefType builtObj = kv.Key;

                string value = buildFrom.CodeId;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    builtObj.CodeID = buildFrom.CodeId;
                }

                string value1 = buildFrom.Id;
                if (!string.IsNullOrWhiteSpace(value1))
                {
                    builtObj.NodeAliasID = buildFrom.Id;
                }

                string value2 = buildFrom.CodelistAliasRef;
                if (!string.IsNullOrWhiteSpace(value2))
                {
                    builtObj.CodelistAliasRef = buildFrom.CodelistAliasRef;
                }
                else
                {
                    ICrossReference crossRefernce = buildFrom.CodeReference;
                    if (crossRefernce != null)
                    {
                        builtObj.URN = crossRefernce.TargetUrn;
                    }
                }

                if (buildFrom.ValidFrom != null)
                {
                    builtObj.ValidFrom = buildFrom.ValidFrom.Date;
                }

                if (buildFrom.ValidTo != null)
                {
                    builtObj.ValidTo = buildFrom.ValidTo.Date;
                }

                if (buildFrom.GetLevel(false) != null)
                {
                    builtObj.LevelRef = buildFrom.GetLevel(false).Id;
                }

                if (ObjectUtil.ValidCollection(buildFrom.CodeRefs))
                {
                    foreach (IHierarchicalCode currentCodeRef in buildFrom.CodeRefs)
                    {
                        var item = new CodeRefType();
                        builtObj.CodeRef.Add(item);
                        pairs.Push(new KeyValuePair<CodeRefType, IHierarchicalCode>(item, currentCodeRef));
                    }
                }
            }

            return root;
        }

        #endregion
    }
}
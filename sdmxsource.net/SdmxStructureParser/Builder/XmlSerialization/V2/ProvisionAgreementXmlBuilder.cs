// -----------------------------------------------------------------------
// <copyright file="ProvisionAgreementXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.registry;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The provision agreement xml bean builder.
    /// </summary>
    public class ProvisionAgreementXmlBuilder : AbstractBuilder
    {
        #region Static Fields

        /// <summary>
        ///     The instance.
        /// </summary>
        private static readonly ProvisionAgreementXmlBuilder _instance = new ProvisionAgreementXmlBuilder();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="ProvisionAgreementXmlBuilder" /> class from being created.
        /// </summary>
        private ProvisionAgreementXmlBuilder()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static ProvisionAgreementXmlBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Build <see cref="ProvisionAgreementType"/> from <paramref name="buildFrom"/>.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="ProvisionAgreementType"/> from <paramref name="buildFrom"/> .
        /// </returns>
        public ProvisionAgreementType Build(IProvisionAgreementObject buildFrom)
        {
            var builtObj = new ProvisionAgreementType();

            string value = buildFrom.Id;
            if (!string.IsNullOrWhiteSpace(value))
            {
                builtObj.id = buildFrom.Id;
            }

            if (buildFrom.Uri != null)
            {
                builtObj.uri = buildFrom.Uri;
            }
            else if (buildFrom.StructureUrl != null)
            {
                builtObj.uri = buildFrom.StructureUrl;
            }
            else if (buildFrom.ServiceUrl != null)
            {
                builtObj.uri = buildFrom.StructureUrl;
            }

            if (ObjectUtil.ValidString(buildFrom.Urn))
            {
                builtObj.urn = buildFrom.Urn;
            }

            IList<ITextTypeWrapper> names = buildFrom.Names;
            if (ObjectUtil.ValidCollection(names))
            {
                builtObj.Name = this.GetTextType(names);
            }

            IList<ITextTypeWrapper> descriptions = buildFrom.Descriptions;
            if (ObjectUtil.ValidCollection(descriptions))
            {
                builtObj.Description = this.GetTextType(descriptions);
            }

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            if (buildFrom.StructureUseage != null)
            {
                if (buildFrom.StructureUseage.TargetReference.EnumType == SdmxStructureEnumType.Dataflow)
                {
                    DataflowRefType dataflowRef = builtObj.DataflowRef = new DataflowRefType();
                    this.PopulateDataflowRef(buildFrom.StructureUseage, dataflowRef);
                }
                else if (buildFrom.StructureUseage.TargetReference.EnumType == SdmxStructureEnumType.MetadataFlow)
                {
                    MetadataflowRefType metadataflowRef = builtObj.MetadataflowRef = new MetadataflowRefType();
                    this.PopulateMetadataflowRef(buildFrom.StructureUseage, metadataflowRef);
                }
            }

            if (buildFrom.DataproviderRef != null)
            {
                DataProviderRefType dataProviderRef = builtObj.DataProviderRef = new DataProviderRefType();
                this.PopulateDataproviderRef(buildFrom.DataproviderRef, dataProviderRef);
            }

            return builtObj;
        }

        #endregion
    }
}
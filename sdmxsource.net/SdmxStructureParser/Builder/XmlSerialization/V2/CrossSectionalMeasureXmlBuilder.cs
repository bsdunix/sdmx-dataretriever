// -----------------------------------------------------------------------
// <copyright file="CrossSectionalMeasureXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The cross sectional measure xml bean builder.
    /// </summary>
    public class CrossSectionalMeasureXmlBuilder : AbstractBuilder, 
                                                   IBuilder<CrossSectionalMeasureType, ICrossSectionalMeasure>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Build <see cref="CrossSectionalMeasureType"/> from <paramref name="buildFrom"/>.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="CrossSectionalMeasureType"/> from <paramref name="buildFrom"/> .
        /// </returns>
        public virtual CrossSectionalMeasureType Build(ICrossSectionalMeasure buildFrom)
        {
            var builtObj = new CrossSectionalMeasureType();

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            if (buildFrom.HasCodedRepresentation())
            {
                IMaintainableRefObject maintRef = buildFrom.Representation.Representation.MaintainableReference;
                string str0 = maintRef.MaintainableId;
                if (!string.IsNullOrWhiteSpace(str0))
                {
                    builtObj.codelist = maintRef.MaintainableId;
                }

                string str2 = maintRef.AgencyId;
                if (!string.IsNullOrWhiteSpace(str2))
                {
                    builtObj.codelistAgency = maintRef.AgencyId;
                }

                string str1 = maintRef.Version;
                if (!string.IsNullOrWhiteSpace(str1))
                {
                    builtObj.codelistVersion = maintRef.Version;
                }
            }

            if (buildFrom.ConceptRef != null)
            {
                IMaintainableRefObject conceptScheme = buildFrom.ConceptRef.MaintainableReference;
                string value1 = conceptScheme.AgencyId;
                if (!string.IsNullOrWhiteSpace(value1))
                {
                    builtObj.conceptSchemeAgency = conceptScheme.AgencyId;
                }

                string value = conceptScheme.MaintainableId;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    builtObj.conceptSchemeRef = conceptScheme.MaintainableId;
                }

                string conceptRef = buildFrom.ConceptRef.ChildReference.Id;
                if (!string.IsNullOrWhiteSpace(conceptRef))
                {
                    builtObj.conceptRef = conceptRef;
                }

                // TODO in ESTAT version of SDMX v2.0 we set concept scheme version.
                string value2 = conceptScheme.Version;
                if (!string.IsNullOrWhiteSpace(value2))
                {
                    builtObj.conceptVersion = conceptScheme.Version;
                }
            }

            builtObj.code = buildFrom.Code;
            builtObj.measureDimension = buildFrom.MeasureDimension;
            if (buildFrom.Representation != null && buildFrom.Representation.TextFormat != null)
            {
                var textFormatType = new TextFormatType();
                this.PopulateTextFormatType(textFormatType, buildFrom.Representation.TextFormat);
                builtObj.TextFormat = textFormatType;
            }

            return builtObj;
        }

        #endregion
    }
}
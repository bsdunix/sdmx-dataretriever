// -----------------------------------------------------------------------
// <copyright file="HierarchicalCodelistXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The hierarchical codelist xml bean builder.
    /// </summary>
    public class HierarchicalCodelistXmlBuilder : AbstractBuilder, 
                                                  IBuilder<HierarchicalCodelistType, IHierarchicalCodelistObject>
    {
        #region Fields

        /// <summary>
        ///     The codelist ref xml bean builder.
        /// </summary>
        private readonly CodelistRefXmlBuilder _codelistRefXmlBuilder = new CodelistRefXmlBuilder();

        /// <summary>
        ///     The hierarchy xml bean builder.
        /// </summary>
        private readonly HierarchyXmlBuilder _hierarchyXmlBuilder = new HierarchyXmlBuilder();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Build <see cref="HierarchicalCodelistType"/> from <paramref name="buildFrom"/>.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="HierarchicalCodelistType"/> from <paramref name="buildFrom"/> .
        /// </returns>
        public virtual HierarchicalCodelistType Build(IHierarchicalCodelistObject buildFrom)
        {
            var builtObj = new HierarchicalCodelistType();
            string value1 = buildFrom.AgencyId;
            if (!string.IsNullOrWhiteSpace(value1))
            {
                builtObj.agencyID = buildFrom.AgencyId;
            }

            string value2 = buildFrom.Id;
            if (!string.IsNullOrWhiteSpace(value2))
            {
                builtObj.id = buildFrom.Id;
            }

            if (buildFrom.Uri != null)
            {
                builtObj.uri = buildFrom.Uri;
            }

            if (ObjectUtil.ValidString(buildFrom.Urn))
            {
                builtObj.urn = buildFrom.Urn;
            }

            string value = buildFrom.Version;
            if (!string.IsNullOrWhiteSpace(value))
            {
                builtObj.version = buildFrom.Version;
            }

            if (buildFrom.StartDate != null)
            {
                builtObj.validFrom = buildFrom.StartDate.Date;
            }

            if (buildFrom.EndDate != null)
            {
                builtObj.validTo = buildFrom.EndDate.Date;
            }

            IList<ITextTypeWrapper> names = buildFrom.Names;
            if (ObjectUtil.ValidCollection(names))
            {
                builtObj.Name = this.GetTextType(names);
            }

            IList<ITextTypeWrapper> descriptions = buildFrom.Descriptions;
            if (ObjectUtil.ValidCollection(descriptions))
            {
                builtObj.Description = this.GetTextType(descriptions);
            }

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            if (buildFrom.IsExternalReference.IsSet())
            {
                builtObj.isExternalReference = buildFrom.IsExternalReference.IsTrue;
            }

            if (buildFrom.IsFinal.IsSet())
            {
                builtObj.isFinal = buildFrom.IsFinal.IsTrue;
            }

            IList<IHierarchy> hierarchyBeans = buildFrom.Hierarchies;
            if (ObjectUtil.ValidCollection(hierarchyBeans))
            {
                /* foreach */
                foreach (IHierarchy hierarchyBean in hierarchyBeans)
                {
                    builtObj.Hierarchy.Add(this._hierarchyXmlBuilder.Build(hierarchyBean));
                }
            }

            IList<ICodelistRef> codelistRefBeans = buildFrom.CodelistRef;
            if (ObjectUtil.ValidCollection(codelistRefBeans))
            {
                /* foreach */
                foreach (ICodelistRef codelistRefBean in codelistRefBeans)
                {
                    builtObj.CodelistRef.Add(this._codelistRefXmlBuilder.Build(codelistRefBean));
                }
            }

            return builtObj;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="ConceptXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The concept xml bean builder.
    /// </summary>
    public class ConceptXmlBuilder : AbstractBuilder, IBuilder<ConceptType, IConceptObject>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Build <see cref="ConceptType"/> from <paramref name="buildFrom"/>.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="ConceptType"/> from <paramref name="buildFrom"/> .
        /// </returns>
        public virtual ConceptType Build(IConceptObject buildFrom)
        {
            var builtObj = new ConceptType();

            string str0 = buildFrom.Id;
            if (!string.IsNullOrWhiteSpace(str0))
            {
                builtObj.id = buildFrom.Id;
            }

            if (buildFrom.Uri != null)
            {
                builtObj.uri = buildFrom.Uri;
            }

            builtObj.urn = buildFrom.Urn;

            IList<ITextTypeWrapper> names = buildFrom.Names;
            if (ObjectUtil.ValidCollection(names))
            {
                builtObj.Name = this.GetTextType(names);
            }

            IList<ITextTypeWrapper> descriptions = buildFrom.Descriptions;
            if (ObjectUtil.ValidCollection(descriptions))
            {
                builtObj.Description = this.GetTextType(descriptions);
            }

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            string str3 = buildFrom.ParentConcept;
            if (!string.IsNullOrWhiteSpace(str3))
            {
                builtObj.parent = buildFrom.ParentConcept;
            }

            string str2 = buildFrom.ParentAgency;
            if (!string.IsNullOrWhiteSpace(str2))
            {
                builtObj.parentAgency = buildFrom.ParentAgency;
            }

            if (buildFrom.CoreRepresentation != null)
            {
                if (buildFrom.CoreRepresentation.Representation != null)
                {
                    IMaintainableRefObject maintRef =
                        buildFrom.CoreRepresentation.Representation.MaintainableReference;
                    builtObj.coreRepresentation = maintRef.MaintainableId;
                    builtObj.coreRepresentationAgency = maintRef.AgencyId;
                }

                if (buildFrom.CoreRepresentation.TextFormat != null)
                {
                    var textFormatType = new TextFormatType();
                    this.PopulateTextFormatType(textFormatType, buildFrom.CoreRepresentation.TextFormat);
                    builtObj.TextFormat = textFormatType;
                }
            }

            return builtObj;
        }

        #endregion
    }
}
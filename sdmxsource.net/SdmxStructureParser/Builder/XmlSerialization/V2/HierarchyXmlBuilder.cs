// -----------------------------------------------------------------------
// <copyright file="HierarchyXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The hierarchy xml hierarchicalCodelist builder.
    /// </summary>
    public class HierarchyXmlBuilder : AbstractBuilder, IBuilder<HierarchyType, IHierarchy>
    {
        #region Fields

        /// <summary>
        ///     The code ref xml hierarchicalCodelist builder.
        /// </summary>
        private readonly CodeRefXmlBuilder _codeRefXmlBuilder = new CodeRefXmlBuilder();

        /// <summary>
        ///     The level xml hierarchicalCodelist builder.
        /// </summary>
        private readonly LevelXmlsBuilder _levelXmlBuilder = new LevelXmlsBuilder();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Build <see cref="HierarchyType"/> from <paramref name="buildFrom"/>.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="HierarchyType"/> from <paramref name="buildFrom"/> .
        /// </returns>
        public virtual HierarchyType Build(IHierarchy buildFrom)
        {
            var builtObj = new HierarchyType();
            string str0 = buildFrom.Id;
            if (!string.IsNullOrWhiteSpace(str0))
            {
                builtObj.id = buildFrom.Id;
            }

            builtObj.urn = buildFrom.Urn;

            ICollection<ITextTypeWrapper> collection = buildFrom.Names;
            if (ObjectUtil.ValidCollection(collection))
            {
                builtObj.Name = this.GetTextType(buildFrom.Names);
            }

            ICollection<ITextTypeWrapper> collection1 = buildFrom.Descriptions;
            if (ObjectUtil.ValidCollection(collection1))
            {
                builtObj.Description = this.GetTextType(buildFrom.Descriptions);
            }

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            IList<IHierarchicalCode> hierarchicalCodeBeans = buildFrom.HierarchicalCodeObjects;
            if (ObjectUtil.ValidCollection(hierarchicalCodeBeans))
            {
                /* foreach */
                foreach (IHierarchicalCode currentCodeRef in hierarchicalCodeBeans)
                {
                    builtObj.CodeRef.Add(this._codeRefXmlBuilder.Build(currentCodeRef));
                }
            }

            if (buildFrom.Level != null)
            {
                this._levelXmlBuilder.BuildList(builtObj, buildFrom.Level);
            }

            return builtObj;
        }

        #endregion
    }
}
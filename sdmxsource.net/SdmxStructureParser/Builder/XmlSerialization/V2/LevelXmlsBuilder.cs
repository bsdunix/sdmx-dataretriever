// -----------------------------------------------------------------------
// <copyright file="LevelXmlsBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The level xml beans builder.
    /// </summary>
    public class LevelXmlsBuilder : AbstractBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        /// Populate <see cref="HierarchyType"/> from <paramref name="buildFrom"/>.
        /// </summary>
        /// <param name="hierarchyType">
        /// The hierarchy type.
        /// </param>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        public void BuildList(HierarchyType hierarchyType, ILevelObject buildFrom)
        {
            int order = 1;
            do
            {
                var builtObj = new LevelType();
                hierarchyType.Level.Add(builtObj);

                string value = buildFrom.Id;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    builtObj.id = buildFrom.Id;
                }

                if (ObjectUtil.ValidString(buildFrom.Urn))
                {
                    builtObj.urn = buildFrom.Urn;
                }

                IList<ITextTypeWrapper> names = buildFrom.Names;
                if (ObjectUtil.ValidCollection(names))
                {
                    builtObj.Name = this.GetTextType(names);
                }

                IList<ITextTypeWrapper> descriptions = buildFrom.Descriptions;
                if (ObjectUtil.ValidCollection(descriptions))
                {
                    builtObj.Description = this.GetTextType(descriptions);
                }

                if (this.HasAnnotations(buildFrom))
                {
                    builtObj.Annotations = this.GetAnnotationsType(buildFrom);
                }

                builtObj.Order = order;
                if (buildFrom.CodingFormat != null)
                {
                    var textFormatType = new TextFormatType();
                    this.PopulateTextFormatType(textFormatType, buildFrom.CodingFormat);
                    builtObj.CodingType = textFormatType;
                }

                buildFrom = buildFrom.ChildLevel;
                order++;
            }
            while (buildFrom != null);
        }

        #endregion
    }
}
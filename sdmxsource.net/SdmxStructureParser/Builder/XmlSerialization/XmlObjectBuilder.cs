﻿// -----------------------------------------------------------------------
// <copyright file="XmlObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-06-06
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing;

using Xml.Schema.Linq;

/**
 * Adds schema location to response, if there is a schema location
 *
 */
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization
{
    public abstract class XmlObjectBuilder
    {
        #region Fields

        private readonly SchemaLocationWriter schemaLocationWriter = new SchemaLocationWriter();

        #endregion

	
	    #region  Public Methods and Operators

	    protected void WriteSchemaLocation(XTypedElement doc, SdmxSchemaEnumType schemaVersion) 
        {
		    if (schemaLocationWriter != null)
            {
                List<string> schemaUri = new List<string>();
			    switch(schemaVersion) 
                {
			        case SdmxSchemaEnumType.VersionOne :  
                        schemaUri.Add(SdmxConstants.MessageNs10);
			            break;
			        case SdmxSchemaEnumType.VersionTwo:  
                        schemaUri.Add(SdmxConstants.MessageNs20);
			            break;
			        case SdmxSchemaEnumType.VersionTwoPointOne :  
                        schemaUri.Add(SdmxConstants.MessageNs21);
			            break;
			        default : 
                        throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "Schema Version " + schemaVersion);
			    }
			    schemaLocationWriter.WriteSchemaLocation(doc, schemaUri.ToArray());
		    }
	    }
    }
    #endregion
}
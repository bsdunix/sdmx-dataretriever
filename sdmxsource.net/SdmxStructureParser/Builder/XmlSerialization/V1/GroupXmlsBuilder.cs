// -----------------------------------------------------------------------
// <copyright file="GroupXmlsBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V1
{
    using System.Collections.Generic;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V10.structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The group xml beans builder.
    /// </summary>
    public class GroupXmlsBuilder : AbstractBuilder, IBuilder<GroupType, IGroup>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes static members of the <see cref="GroupXmlsBuilder" /> class.
        /// </summary>
        static GroupXmlsBuilder()
        {
            Log = LogManager.GetLogger(typeof(GroupXmlsBuilder));
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="GroupType"/>.
        /// </returns>
        public virtual GroupType Build(IGroup buildFrom)
        {
            var builtObj = new GroupType();
            string str0 = buildFrom.Id;
            if (!string.IsNullOrWhiteSpace(str0))
            {
                builtObj.id = buildFrom.Id;
            }

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            IList<string> dimensionRefs = buildFrom.DimensionRefs;
            if (ObjectUtil.ValidCollection(dimensionRefs))
            {
                foreach (string dimensionRef in dimensionRefs)
                {
                    builtObj.DimensionRef.Add(dimensionRef);
                }
            }

            return builtObj;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="AttributeXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V1
{
    using System;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V10.structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     Builds a v1 CategorySchemeType from a schema independent ICategorySchemeObject
    /// </summary>
    public class AttributeXmlBuilder : AbstractBuilder, IBuilder<AttributeType, IAttributeObject>
    {
        #region Constants

        /// <summary>
        ///     The time format id.
        /// </summary>
        private const string TimeformatId = "TimeFormat";

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes static members of the <see cref="AttributeXmlBuilder" /> class.
        /// </summary>
        static AttributeXmlBuilder()
        {
            Log = LogManager.GetLogger(typeof(AttributeXmlBuilder));
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="buildFrom">
        /// The build from.
        /// </param>
        /// <returns>
        /// The <see cref="AttributeType"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// Unknown assignment status
        /// </exception>
        public virtual AttributeType Build(IAttributeObject buildFrom)
        {
            var builtObj = new AttributeType();
            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            string assignmentStatus = buildFrom.AssignmentStatus;
            if (!string.IsNullOrWhiteSpace(assignmentStatus))
            {
                switch (buildFrom.AssignmentStatus)
                {
                    case AssignmentStatusTypeConstants.Conditional:
                    case AssignmentStatusTypeConstants.Mandatory:
                        builtObj.assignmentStatus = buildFrom.AssignmentStatus;
                        break;
                    default:
                        throw new ArgumentException("Unknown assignment status: " + buildFrom.AssignmentStatus);
                }
            }

            string value = buildFrom.AttachmentGroup;
            if (!string.IsNullOrWhiteSpace(value))
            {
                var arr = new string[1];
                arr[0] = buildFrom.AttachmentGroup;
                builtObj.AttachmentGroup = arr;
            }

            if (buildFrom.AttachmentLevel != AttributeAttachmentLevel.Null)
            {
                switch (buildFrom.AttachmentLevel)
                {
                    case AttributeAttachmentLevel.DataSet:
                        builtObj.attachmentLevel = AttachmentLevelTypeConstants.DataSet;
                        break;
                    case AttributeAttachmentLevel.DimensionGroup:
                        builtObj.attachmentLevel = AttachmentLevelTypeConstants.Series;
                        break;
                    case AttributeAttachmentLevel.Observation:
                        builtObj.attachmentLevel = AttachmentLevelTypeConstants.Observation;
                        break;
                    case AttributeAttachmentLevel.Group:
                        builtObj.attachmentLevel = AttachmentLevelTypeConstants.Group;
                        break;
                    default:
                        throw new ArgumentException("Unknown attachment level: " + buildFrom.AttachmentLevel);
                }
            }

            if (buildFrom.HasCodedRepresentation())
            {
                builtObj.codelist = buildFrom.Representation.Representation.MaintainableReference.MaintainableId;
            }

            if (buildFrom.ConceptRef != null)
            {
                builtObj.concept = ConceptRefUtil.GetConceptId(buildFrom.ConceptRef);
            }

            if (buildFrom.Id.Equals(TimeformatId))
            {
                builtObj.isTimeFormat = true;
            }

            if (buildFrom.Representation != null && buildFrom.Representation.TextFormat != null)
            {
                var textFormatType = new TextFormatType();
                this.PopulateTextFormatType(textFormatType, buildFrom.Representation.TextFormat);
                builtObj.TextFormat = textFormatType;
            }

            return builtObj;
        }

        #endregion
    }
}
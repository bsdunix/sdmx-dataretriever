// -----------------------------------------------------------------------
// <copyright file="ISdmxObjectsBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects
{
    using System.Xml.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.message;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;

    using Structure = Org.Sdmx.Resources.SdmxMl.Schemas.V10.message.Structure;

    /// <summary>
    ///     SDMXObjects builder builds SDMXObjects from <see cref="XElement" />
    /// </summary>
    public interface ISdmxObjectsBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds a <see cref="ISdmxObjects"/> from a V1 <paramref name="structuresDoc"/>.
        ///     <p/>
        ///     If the <paramref name="structuresDoc"/> contains no Structures then there will be an empty
        ///     <see cref="ISdmxObjects"/>
        ///     Object returned.
        ///     <p/>
        ///     If the <paramref name="structuresDoc"/> contains duplicate maintainable structures (defined by the same URN) - then an exception will be thrown
        /// </summary>
        /// <param name="structuresDoc">
        /// The structure message
        /// </param>
        /// <returns>
        /// a <see cref="ISdmxObjects"/> from a V1 <paramref name="structuresDoc"/>.
        /// </returns>
        ISdmxObjects Build(Structure structuresDoc);

        /// <summary>
        /// Builds a <see cref="ISdmxObjects"/> from a V2 <paramref name="structuresDoc"/>.
        ///     <p/>
        ///     If the <paramref name="structuresDoc"/> contains no Structures then there will be an empty
        ///     <see cref="ISdmxObjects"/>
        ///     Object returned.
        ///     <p/>
        ///     If the <paramref name="structuresDoc"/> contains duplicate maintainable structures (defined by the same URN) - then an exception will be thrown
        /// </summary>
        /// <param name="structuresDoc">
        /// The structure message
        /// </param>
        /// <returns>
        /// a <see cref="ISdmxObjects"/> from a V2 <paramref name="structuresDoc"/>.
        /// </returns>
        ISdmxObjects Build(Org.Sdmx.Resources.SdmxMl.Schemas.V20.message.Structure structuresDoc);

        /// <summary>
        /// Builds a <see cref="ISdmxObjects"/> from a V2 <paramref name="registryInterface"/> QueryStructureRequest.
        ///     <p/>
        ///     If the <paramref name="registryInterface"/> contains no Structures then there will be an empty
        ///     <see cref="ISdmxObjects"/>
        ///     Object returned.
        ///     <p/>
        ///     If the <paramref name="registryInterface"/> contains duplicate maintainable structures (defined by the same URN) - then an exception will be thrown
        /// </summary>
        /// <param name="registryInterface">
        /// The registry interface message containing the QueryStructureRequest
        /// </param>
        /// <returns>
        /// a <see cref="ISdmxObjects"/> from a V2 <paramref name="registryInterface"/> QueryStructureRequest..
        /// </returns>
        ISdmxObjects Build(RegistryInterface registryInterface);

        /// <summary>
        /// Builds a <see cref="ISdmxObjects"/> from a V21 <paramref name="registryInterface"/>.
        ///     <p/>
        ///     If the <paramref name="registryInterface"/> contains no Structures then there will be an empty
        ///     <see cref="ISdmxObjects"/>
        ///     Object returned.
        ///     <p/>
        ///     If the <paramref name="registryInterface"/> contains duplicate maintainable structures (defined by the same URN) - then an exception will be thrown
        /// </summary>
        /// <param name="registryInterface">
        /// The registry interface message
        /// </param>
        /// <returns>
        /// a <see cref="ISdmxObjects"/> from a V2 <paramref name="registryInterface"/>.
        /// </returns>
        ISdmxObjects Build(Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.RegistryInterface registryInterface);

        /// <summary>
        /// Builds a <see cref="ISdmxObjects"/> from a V21 <paramref name="structuresDoc"/>.
        ///     <p/>
        ///     If the <paramref name="structuresDoc"/> contains no Structures then there will be an empty
        ///     <see cref="ISdmxObjects"/>
        ///     Object returned.
        ///     <p/>
        ///     If the <paramref name="structuresDoc"/> contains duplicate maintainable structures (defined by the same URN) - then an exception will be thrown
        /// </summary>
        /// <param name="structuresDoc">
        /// The structure message
        /// </param>
        /// <returns>
        /// a <see cref="ISdmxObjects"/> from a V21 <paramref name="structuresDoc"/>.
        /// </returns>
        ISdmxObjects Build(Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.Structure structuresDoc);

        #endregion
    }
}
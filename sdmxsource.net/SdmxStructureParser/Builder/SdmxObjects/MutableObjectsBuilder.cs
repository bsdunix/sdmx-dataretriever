// -----------------------------------------------------------------------
// <copyright file="MutableObjectsBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects
{
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     This Builder is realized as being able to build Mutable beans and from a collection of beans.
    /// </summary>
    public class MutableObjectsBuilder : IMutableObjectsBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds Mutable beans from a collection of beans
        /// </summary>
        /// <param name="buildFrom">
        /// beans to build from
        /// </param>
        /// <returns>
        /// Mutable bean copies
        /// </returns>
        /// <exception cref="BuilderException">
        /// - If anything goes wrong during the build process
        /// </exception>
        public virtual IMutableObjects Build(ISdmxObjects buildFrom)
        {
            IMutableObjects mutableBeans = new MutableObjectsImpl();

            /* foreach */
            foreach (IMaintainableObject currentMaintainable in buildFrom.GetAllMaintainables())
            {
                mutableBeans.AddIdentifiable(currentMaintainable.MutableInstance);
            }

            return mutableBeans;
        }

        #endregion
    }
}
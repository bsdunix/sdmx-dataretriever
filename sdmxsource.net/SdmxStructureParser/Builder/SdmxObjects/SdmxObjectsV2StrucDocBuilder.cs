// -----------------------------------------------------------------------
// <copyright file="SdmxObjectsV2StrucDocBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.message;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     The sdmx beans v 2 structure builder.
    /// </summary>
    public class SdmxObjectsV2StrucDocBuilder : AbstractSdmxObjectsV2Builder, IBuilder<ISdmxObjects, Structure>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds an <see cref="ISdmxObjects"/> object from the specified <paramref name="structuresDoc"/>
        /// </summary>
        /// <param name="structuresDoc">
        /// An <see cref="Structure"/> to build the output object from
        /// </param>
        /// <returns>
        /// an <see cref="ISdmxObjects"/> object from the specified <paramref name="structuresDoc"/>
        /// </returns>
        /// <exception cref="BuilderException">
        /// - If anything goes wrong during the build process
        /// </exception>
        public virtual ISdmxObjects Build(Structure structuresDoc)
        {
            var urns = new HashSet<Uri>();
            StructureType structures = structuresDoc.Content;

            var beans = new SdmxObjectsImpl(new HeaderImpl(structures.Header));

            // CATEGORY SCHEMES
            this.ProcessCategorySchemes(structures.CategorySchemes, beans);

            // CODELISTS
            this.ProcessCodelists(structures.CodeLists, beans, urns);

            // CONCEPT SCHEMES
            this.ProcessConceptSchemes(structures.Concepts, beans, urns);

            // DATAFLOWS
            this.ProcessDataflows(structures.Dataflows, beans);

            // HIERARCHICAL CODELISTS
            this.ProcessHierarchicalCodelists(structures.HierarchicalCodelists, beans, urns);

            // KEY FAMILIES
            this.ProcessKeyFamilies(structures.KeyFamilies, beans, urns);

            // METADATA FLOWS
            this.ProcessMetadataFlows(structures.Metadataflows, beans);

            // METADATASTRUCTURE DEFINITIONS
            ProcessMetadataStructureDefinitions(structures.MetadataStructureDefinitions, beans, urns);

            // ORGANISATION SCHEMES
            this.ProcessOrganisationSchemes(structures.OrganisationSchemes, beans, urns);

            this.ProcessProcesses(structures.Processes, beans, urns);

            this.ProcessReportingTaxonomies(structures.ReportingTaxonomies, beans, urns);

            this.ProcessStructureSets(structures.StructureSets, beans, urns);

            return beans;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="AbstractSdmxObjectsBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The abstract sdmx beans builder.
    /// </summary>
    public abstract class AbstractSdmxObjectsBuilder
    {
        #region Methods

        /// <summary>
        /// Adds a bean to the container as long as the urn of the bean to add is not contained in the set of URNs.
        ///     <p/>
        ///     If successfully added, will add the bean urn to the set of urns
        /// </summary>
        /// <param name="beans">
        /// container to add to
        /// </param>
        /// <param name="urns">
        /// to exclude
        /// </param>
        /// <param name="bean">
        /// to add to the beans container
        /// </param>
        protected internal void AddIfNotDuplicateURN(ISdmxObjects beans, ISet<Uri> urns, IIdentifiableObject bean)
        {
            if (!urns.Add(bean.Urn))
            {
                throw new SdmxSemmanticException(ExceptionCode.DuplicateUrn, bean.Urn);
            }

            beans.AddIdentifiable(bean);
        }

        #endregion
    }
}
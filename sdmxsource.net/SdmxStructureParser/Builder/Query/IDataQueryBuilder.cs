// -----------------------------------------------------------------------
// <copyright file="IDataQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.Query
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V10.query;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

    using DataWhereType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.query.DataWhereType;

    /// <summary>
    ///     The DataQueryBuilder interface.
    /// </summary>
    public interface IDataQueryBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds a data query from a <c>dataWhereType</c>
        /// </summary>
        /// <param name="queryType">
        /// - the IXML to build the domain object (DataQuery) from
        /// </param>
        /// <param name="structureRetrievalManager">
        /// optional, if supplied the retrieval manager to use to retrieve the Dataflow and Provider defined by the
        ///     <paramref name="queryType"/>
        /// </param>
        /// <returns>
        /// a data query from a <c>dataWhereType</c>
        /// </returns>
        IList<IDataQuery> BuildDataQuery(QueryType queryType, ISdmxObjectRetrievalManager structureRetrievalManager);

        /// <summary>
        /// Builds a data query from a <c>dataWhereType</c>
        /// </summary>
        /// <param name="queryType">
        /// - the IXML to build the domain object (DataQuery) from
        /// </param>
        /// <param name="structureRetrievalManager">
        /// optional, if supplied the retrieval manager to use to retrieve the Dataflow and Provider defined by the
        ///     <paramref name="queryType"/>
        /// </param>
        /// <returns>
        /// a data query from a <c>dataWhereType</c>
        /// </returns>
        IList<IDataQuery> BuildDataQuery(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.query.QueryType queryType,
            ISdmxObjectRetrievalManager structureRetrievalManager);

        /**
         * Builds a data query from a dataQueryType
         * @param dataQueryType - the XMLBean to build the domain object (DataQuery) from
         * @param structureRetrievalManager optional, if supplied the retrieval manager to use to retrieve the DSD, which is stored on the query,
         * if not supplied then the DSD will not be stored on the query
         * @return
         */
        IList<IComplexDataQuery> BuildComplexDataQuery(DataQueryType dataQueryType, ISdmxObjectRetrievalManager beanRetrievalManager);


        /// <summary>
        /// Builds a data query from a <c>dataWhereType</c>
        /// </summary>
        /// <param name="dataWhereType">
        /// - the IXML to build the domain object (DataQuery) from
        /// </param>
        /// <param name="structureRetrievalManager">
        /// optional, if supplied the retrieval manager to use to retrieve the Dataflow and Provider defined by the
        ///     <paramref name="dataWhereType"/>
        /// </param>
        /// <returns>
        /// a data query from a <c>dataWhereType</c>
        /// </returns>
        IDataQuery BuildDataQuery(DataWhereType dataWhereType, ISdmxObjectRetrievalManager structureRetrievalManager);

        /// <summary>
        /// Builds  a data query from a <c>dataWhereType</c>
        /// </summary>
        /// <param name="dataWhereType">
        /// - the IXML to build the domain object (DataQuery) from
        /// </param>
        /// <param name="structureRetrievalManager">
        /// optional, if supplied the retrieval manager to use to retrieve the Dataflow and Provider defined by the
        ///     <paramref name="dataWhereType"/>
        /// </param>
        /// <returns>
        /// a data query from a <c>dataWhereType</c>
        /// </returns>
        IDataQuery BuildDataQuery(
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.query.DataWhereType dataWhereType,
            ISdmxObjectRetrievalManager structureRetrievalManager);

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ErrorWriterEngineV21.cs" company="EUROSTAT">
//   Date Created : 2013-06-06
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Error;

using Xml.Schema.Linq;

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System.Globalization;

    public class ErrorWriterEngineV21 : IErrorWriterEngine
    {
        /// <summary>
        /// The error response builder.
        /// </summary>
        private readonly ErrorResponseBuilder _errorResponseBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorWriterEngineV21"/> class.
        /// </summary>
        public ErrorWriterEngineV21()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorWriterEngineV21"/> class.
        /// </summary>
        /// <param name="errorResponseBuilder">The error response builder.</param>
        public ErrorWriterEngineV21(ErrorResponseBuilder errorResponseBuilder)
        {
            this._errorResponseBuilder = errorResponseBuilder ?? new ErrorResponseBuilder();
        }

        /// <summary>
        /// Writes an error to the output stream
        /// </summary>
        /// <param name="ex">
        /// The error to write
        /// </param>
        /// <param name="outPutStream">
        /// The output stream to write to
        /// </param>
        /// <returns>
        /// The HTTP Status code
        /// </returns>
        public int WriteError(Exception ex, Stream outPutStream)
        {
            XTypedElement error;
            int statusCode;
            if (ex.GetType() == typeof(SdmxException))
            {
                SdmxException sdmxEx = (SdmxException)ex;
                statusCode = sdmxEx.HttpRestErrorCode;
                error = this._errorResponseBuilder.BuildErrorResponse(ex, sdmxEx.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                error = this._errorResponseBuilder.BuildErrorResponse(ex, "500");
                statusCode = 500;
            }

            error.Untyped.Save(outPutStream);

            return statusCode;
        }
    }
}
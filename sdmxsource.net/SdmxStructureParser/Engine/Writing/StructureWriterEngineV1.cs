// -----------------------------------------------------------------------
// <copyright file="StructureWriterEngineV1.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System;
    using System.IO;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V1;

    using Xml.Schema.Linq;

    /// <summary>
    ///     The structure writing engine for SDMX v1.0
    /// </summary>
    public class StructureWriterEngineV1 : AbstractWritingEngine
    {
        #region Fields

        /// <summary>
        ///     The structure xml sdmxObject builder sdmxObject.
        /// </summary>
        private readonly StructureXmlBuilder _structureXmlBuilder = new StructureXmlBuilder();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWriterEngineV1"/> class. 
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="writer"/> is null
        /// </exception>
        public StructureWriterEngineV1(XmlWriter writer)
            : base(writer)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWriterEngineV1"/> class.
        /// </summary>
        /// <param name="outputStream">
        /// The output stream.
        /// </param>
        /// <param name="prettyFy">
        /// controls if output should be pretty (indented and no duplicate namespaces)
        /// </param>
        public StructureWriterEngineV1(Stream outputStream, bool prettyFy)
            : base(SdmxSchemaEnumType.VersionOne, outputStream, prettyFy)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWriterEngineV1"/> class.
        /// </summary>
        /// <param name="outputStream">
        /// The output stream.
        /// </param>
        public StructureWriterEngineV1(Stream outputStream)
            : base(SdmxSchemaEnumType.VersionOne, outputStream, true)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Build the XSD generated class objects from the specified <paramref name="beans"/>
        /// </summary>
        /// <param name="beans">
        /// The beans.
        /// </param>
        /// <returns>
        /// the XSD generated class objects from the specified <paramref name="beans"/>
        /// </returns>
        protected internal override XTypedElement Build(ISdmxObjects beans)
        {
            return this._structureXmlBuilder.Build(beans);
        }

        #endregion
    }
}
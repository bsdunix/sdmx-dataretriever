// -----------------------------------------------------------------------
// <copyright file="StructureWriterEngineEdi.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine
{
    using System;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     Not implemented.
    /// </summary>
    public class StructureWriterEngineEdi : IStructureWriterEngine
    {
        ////private EdiParseManager editParseManager;
        #region Fields

        /// <summary>
        ///     The output stream.
        /// </summary>
        private Stream xout;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWriterEngineEdi"/> class.
        /// </summary>
        /// <param name="xout">
        /// The output stream.
        /// </param>
        public StructureWriterEngineEdi(Stream xout)
        {
            this.xout = xout;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The write structure.
        /// </summary>
        /// <param name="bean">
        /// The maintainable object.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// SDMX EDI for structures is not implemented in this implementation.
        /// </exception>
        public virtual void WriteStructure(IMaintainableObject bean)
        {
            throw new NotImplementedException("SDMX EDI for structures is not implemented in this implementation.");

            ////ISdmxObjects beans = new SdmxObjectsImpl();
            ////beans.AddIdentifiable(bean);
            ////WriteStructures(beans);
        }

        /// <summary>
        /// The write structures.
        /// </summary>
        /// <param name="beans">
        /// The beans.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// SDMX EDI for structures is not implemented in this implementation.
        /// </exception>
        public virtual void WriteStructures(ISdmxObjects beans)
        {
            throw new NotImplementedException("SDMX EDI for structures is not implemented in this implementation.");

            ////if (editParseManager == null) {
            ////    throw new Exception(
            ////            "Required dependancy 'structureReaderEngine' is null, StructureWriterEngineEdi is @Configurable and requires '<context:spring-configured />' to be set");
            ////}
            ////editParseManager.WriteToEDI(beans, xout);
        }

        #endregion
    }
}
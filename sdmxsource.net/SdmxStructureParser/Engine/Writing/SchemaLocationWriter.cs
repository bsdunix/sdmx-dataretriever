﻿// -----------------------------------------------------------------------
// <copyright file="SchemaLocationWriter.cs" company="EUROSTAT">
//   Date Created : 2013-06-06
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Util.Xml;

using Xml.Schema.Linq;

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    public class SchemaLocationWriter
    {
        private static readonly Dictionary<SdmxSchema, string> schemaLocationMap = new Dictionary<SdmxSchema, string>();

	    /**
	     * Writes the schema location(s) to the XML Document
	     * @param doc the XML document to write the schema location(s) to
	     * @param schemaVersion the version of the schema to write in
	     * @param namespaceUri the URI of the namespace to write
	     */
	    public void WriteSchemaLocation(XTypedElement doc, string[] namespaceUri) 
        {
		    if(namespaceUri == null) 
			    return;

		    SdmxSchema schemaVersion;
		    StringBuilder schemaLocation = new StringBuilder();

		    string concat = "";
		    foreach (string currentNamespaceUri in namespaceUri) 
            {
			    schemaVersion = SdmxConstants.GetSchemaVersion(currentNamespaceUri);
			    //Base location of schema for version e.g. http://www.sss.sss/schema/
			    string schemaBaseLocation = GetSchemaLocation(schemaVersion);
			    string schemaName = SdmxConstants.GetSchemaName(currentNamespaceUri);
			    schemaLocation.Append(concat+currentNamespaceUri + " " + concat+schemaBaseLocation+schemaName);
			    concat = "\r\n";// System.getProperty("line.separator");
		    }

            doc.Untyped.SetAttributeValue(XName.Get("http://www.w3.org/2001/XMLSchema-instance","schemaLocation"),schemaLocation.ToString());
	    }

	    public string GetSchemaLocation(SdmxSchema schemaVersion) 
        {
		    return schemaLocationMap[schemaVersion];
	    }

        //public void SetSchemaLocationProperties(Properties properties) 
        //{
        //    IDictionary<string, string> propsMap = PropertiesToMap.createMap(properties);
        //    foreach (string schemaVersion in propsMap.Keys) 
        //    {
        //        schemaLocationMap.Add(SdmxSchema.valueOf(schemaVersion), propsMap[schemaVersion]);
        //    }
        //}
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ComponentCrossReferenceUpdater.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Reversion
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///   TODO: Update summary.
    /// </summary>
    public abstract class ComponentCrossReferenceUpdater : RepresentationCrossReferenceUpdater
    {
        /// <summary>
        /// The update component referneces.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <param name="components">
        /// The components.
        /// </param>
        /// <param name="updateReferences">
        /// The update references.
        /// </param>
        public void UpdateComponentReferneces<T>(
            IEnumerable<T> components, IDictionary<IStructureReference, IStructureReference> updateReferences) where T : IComponentMutableObject
        {
            foreach (T currentComponent in components)
            {
                UpdateComponentReferneces(currentComponent, updateReferences);
            }
        }

        /// <summary>
        /// The update component referneces.
        /// </summary>
        /// <param name="component">
        /// The component.
        /// </param>
        /// <param name="updateReferences">
        /// The update references.
        /// </param>
        public void UpdateComponentReferneces(
            IComponentMutableObject component, IDictionary<IStructureReference, IStructureReference> updateReferences)
        {
            // Update the concept reference
            IStructureReference mapTo = updateReferences[component.ConceptRef];
            if (mapTo != null)
            {
                component.ConceptRef = mapTo;
            }
            // Update the codelist reference
            UpdateRepresentationReference(component.Representation, updateReferences);
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="DataStructureCrossReferenceUpdaterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Reversion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DataStructureCrossReferenceUpdaterEngine : ComponentCrossReferenceUpdater, IDataStructureCrossReferenceUpdaterEngine
    {
        /// <summary>
        /// The update references.
        /// </summary>
        /// <param name="maintianable">
        /// The maintianable.
        /// </param>
        /// <param name="updateReferences">
        /// The update references.
        /// </param>
        /// <param name="newVersionNumber">
        /// The new version number.
        /// </param>
        /// <returns>
        /// The <see cref="IDataStructureObject"/>.
        /// </returns>
        public IDataStructureObject UpdateReferences(IDataStructureObject maintianable, IDictionary<IStructureReference, IStructureReference> updateReferences, String newVersionNumber)
        {
            IDataStructureMutableObject dsd = maintianable.MutableInstance;
            dsd.Version = newVersionNumber;

            if (dsd.DimensionList != null && dsd.DimensionList.Dimensions != null)
            {
                UpdateComponentReferneces(dsd.DimensionList.Dimensions, updateReferences);
            }
            if (dsd.AttributeList != null && dsd.AttributeList.Attributes != null)
            {
                UpdateComponentReferneces(dsd.AttributeList.Attributes, updateReferences);
            }
            if (dsd.MeasureList != null && dsd.MeasureList.PrimaryMeasure != null)
            {
                UpdateComponentReferneces(dsd.MeasureList.PrimaryMeasure, updateReferences);
            }
            return dsd.ImmutableInstance;
        }
    }
}

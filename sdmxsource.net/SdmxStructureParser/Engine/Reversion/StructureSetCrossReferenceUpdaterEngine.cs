﻿// -----------------------------------------------------------------------
// <copyright file="StructureSetCrossReferenceUpdaterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Reversion
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///   TODO: Update summary.
    /// </summary>
    public class StructureSetCrossReferenceUpdaterEngine : IStructureSetCrossReferenceUpdaterEngine
    {
        public IStructureSetObject UpdateReferences(
            IStructureSetObject maintianable,
            IDictionary<IStructureReference, IStructureReference> updateReferences,
            String newVersionNumber)
        {
            IStructureSetMutableObject ss = maintianable.MutableInstance;
            ss.Version = newVersionNumber;

            this.UpdateSchemeMap(ss.CategorySchemeMapList, updateReferences);
            this.UpdateSchemeMap(ss.CodelistMapList, updateReferences);
            this.UpdateSchemeMap(ss.ConceptSchemeMapList, updateReferences);
            this.UpdateSchemeMap(ss.OrganisationSchemeMapList, updateReferences);
            this.UpdateSchemeMap(ss.StructureMapList, updateReferences);

            if (ss.RelatedStructures != null)
            {
                IRelatedStructuresMutableObject relatedStructures = ss.RelatedStructures;
                relatedStructures.CategorySchemeRef = this.UpdateRelatedStructures(
                    relatedStructures.CategorySchemeRef, updateReferences);
                relatedStructures.ConceptSchemeRef = this.UpdateRelatedStructures(
                    relatedStructures.ConceptSchemeRef, updateReferences);
                relatedStructures.HierCodelistRef = this.UpdateRelatedStructures(
                    relatedStructures.HierCodelistRef, updateReferences);
                relatedStructures.DataStructureRef = this.UpdateRelatedStructures(
                    relatedStructures.DataStructureRef, updateReferences);
                relatedStructures.MetadataStructureRef =
                    this.UpdateRelatedStructures(relatedStructures.MetadataStructureRef, updateReferences);
                relatedStructures.OrgSchemeRef = this.UpdateRelatedStructures(
                    relatedStructures.OrgSchemeRef, updateReferences);
            }

            return ss.ImmutableInstance;
        }

        private IList<IStructureReference> UpdateRelatedStructures(
            IList<IStructureReference> sRefList, IDictionary<IStructureReference, IStructureReference> updateReferences)
        {
            IList<IStructureReference> newReferences = new List<IStructureReference>();
            if (sRefList != null)
            {
                foreach (IStructureReference currentSRef in sRefList)
                {
                    IStructureReference updatedRef;
                    if (updateReferences.TryGetValue(currentSRef, out updatedRef))
                    {
                        newReferences.Add(updatedRef);
                    }
                    else
                    {
                        newReferences.Add(currentSRef);
                    }
                }
            }
            return newReferences;
        }

        private void UpdateSchemeMap<T>(
            IList<T> schemeMaps, IDictionary<IStructureReference, IStructureReference> updateReferences)
            where T : class, ISchemeMapMutableObject
        {
            if (schemeMaps != null)
            {
                foreach (T currentMap in schemeMaps)
                {
                    IStructureReference newTarget; 
                    if (updateReferences.TryGetValue(currentMap.SourceRef, out newTarget))
                    {
                        currentMap.SourceRef = newTarget;
                    }

                    if (updateReferences.TryGetValue(currentMap.TargetRef, out newTarget))
                    {
                        currentMap.TargetRef = newTarget;
                    }
                }
            }
        }
    }
}

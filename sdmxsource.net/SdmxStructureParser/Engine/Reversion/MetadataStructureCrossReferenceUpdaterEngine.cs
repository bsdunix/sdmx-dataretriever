﻿// -----------------------------------------------------------------------
// <copyright file="MetadataStructureCrossReferenceUpdaterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Reversion
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///   TODO: Update summary.
    /// </summary>
    public class MetadataStructureCrossReferenceUpdaterEngine : RepresentationCrossReferenceUpdater,
                                                                IMetadataStructureCrossReferenceUpdaterEngine
    {
        /// <summary>
        /// The update references.
        /// </summary>
        /// <param name="maintianable">
        /// The maintianable.
        /// </param>
        /// <param name="updateReferences">
        /// The update references.
        /// </param>
        /// <param name="newVersionNumber">
        /// The new version number.
        /// </param>
        /// <returns>
        /// The <see cref="IMetadataStructureDefinitionObject"/>.
        /// </returns>
        public IMetadataStructureDefinitionObject UpdateReferences(
            IMetadataStructureDefinitionObject maintianable,
            IDictionary<IStructureReference, IStructureReference> updateReferences,
            string newVersionNumber)
        {
            IMetadataStructureDefinitionMutableObject msdMutable = maintianable.MutableInstance;
            msdMutable.Version = newVersionNumber;

            if (msdMutable.MetadataTargets != null)
            {
                foreach (IMetadataTargetMutableObject metadataTarget in msdMutable.MetadataTargets)
                {
                    if (metadataTarget.IdentifiableTarget != null)
                    {
                        foreach (
                            IIdentifiableTargetMutableObject identifiableTarget in
                                metadataTarget.IdentifiableTarget)
                        {
                            UpdateRepresentationReference(identifiableTarget.Representation, updateReferences);

                            if (identifiableTarget.ConceptRef != null)
                            {
                                IStructureReference structureReference = updateReferences[identifiableTarget.ConceptRef];
                                if (structureReference != null)
                                {
                                    identifiableTarget.ConceptRef = structureReference;
                                }
                            }
                        }
                    }
                }
            }
            if (msdMutable.ReportStructures != null)
            {
                foreach (IReportStructureMutableObject rs in msdMutable.ReportStructures)
                {
                    this.UpdateMetadataAttributes(rs.MetadataAttributes, updateReferences);
                }
            }
            return msdMutable.ImmutableInstance;
        }

        /// <summary>
        /// The update metadata attributes.
        /// </summary>
        /// <param name="metadataAttributes">
        /// The metadata attributes.
        /// </param>
        /// <param name="updateReferences">
        /// The update references.
        /// </param>
        private void UpdateMetadataAttributes(
            IList<IMetadataAttributeMutableObject> metadataAttributes,
            IDictionary<IStructureReference, IStructureReference> updateReferences)
        {
            if (metadataAttributes != null)
            {
                foreach (IMetadataAttributeMutableObject currentMa in metadataAttributes)
                {
                    UpdateRepresentationReference(currentMa.Representation, updateReferences);

                    if (currentMa.ConceptRef != null)
                    {
                        IStructureReference structureReference = updateReferences[currentMa.ConceptRef];
                        if (structureReference != null)
                        {
                            currentMa.ConceptRef = structureReference;
                        }
                    }
                }
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="HierarchicCodelistCrossReferenceUpdaterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Reversion
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///   TODO: Update summary.
    /// </summary>
    public class HierarchicCodelistCrossReferenceUpdaterEngine : IHierarchicCodelistCrossReferenceUpdaterEngine
    {
        /// <summary>
        /// The update references.
        /// </summary>
        /// <param name="maintianable">
        /// The maintianable.
        /// </param>
        /// <param name="updateReferences">
        /// The update references.
        /// </param>
        /// <param name="newVersionNumber">
        /// The new version number.
        /// </param>
        /// <returns>
        /// The <see cref="IHierarchicalCodelistObject"/>.
        /// </returns>
        public IHierarchicalCodelistObject UpdateReferences(
            IHierarchicalCodelistObject maintianable,
            IDictionary<IStructureReference, IStructureReference> updateReferences,
            string newVersionNumber)
        {
            IHierarchicalCodelistMutableObject codelistMutableObject = maintianable.MutableInstance;
            codelistMutableObject.Version = newVersionNumber;

            if (codelistMutableObject.CodelistRef != null)
            {
                foreach (ICodelistRefMutableObject codelistRef in codelistMutableObject.CodelistRef)
                {
                    if (codelistRef.CodelistReference != null)
                    {
                        IStructureReference structureReference = updateReferences[codelistRef.CodelistReference];
                        if (structureReference != null)
                        {
                            codelistRef.CodelistReference = structureReference;
                        }
                    }
                }
            }
            if (codelistMutableObject.Hierarchies != null)
            {
                foreach (IHierarchyMutableObject hierarchyMutable in codelistMutableObject.Hierarchies)
                {
                    this.UpdateCodeRefs(hierarchyMutable.HierarchicalCodeObjects, updateReferences);
                }
            }
            return codelistMutableObject.ImmutableInstance;
        }

        /// <summary>
        /// The update code refs.
        /// </summary>
        /// <param name="codeRef">
        /// The code ref.
        /// </param>
        /// <param name="updateReferences">
        /// The update references.
        /// </param>
        private void UpdateCodeRefs(
            IList<ICodeRefMutableObject> codeRef, IDictionary<IStructureReference, IStructureReference> updateReferences)
        {
            if (codeRef != null)
            {
                foreach (ICodeRefMutableObject currentCodeRef in codeRef)
                {
                    this.UpdateCodeRefs(currentCodeRef.CodeRefs, updateReferences);

                    if (currentCodeRef.CodeReference != null)
                    {
                        IStructureReference structureReference = updateReferences[currentCodeRef.CodeReference];
                        if (structureReference != null)
                        {
                            currentCodeRef.CodeReference = structureReference;
                        }
                    }
                }
            }
        }
    }
}

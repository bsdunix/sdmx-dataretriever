// -----------------------------------------------------------------------
// <copyright file="NotificationParsingManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing
{
    using System;
    using System.IO;
    using System.Xml;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
    using Org.Sdmxsource.Util.Log;
    using Org.Sdmxsource.XmlHelper;

    /// <summary>
    ///     The notification parsing manager implementation.
    /// </summary>
    public class NotificationParsingManager : BaseParsingManager, INotificationParsingManager
    {
        #region Static Fields

        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(NotificationParsingManager));

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="NotificationParsingManager" /> class.
        /// </summary>
        public NotificationParsingManager()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationParsingManager"/> class.
        /// </summary>
        /// <param name="sdmxSchema">
        /// The sdmx schema.
        /// </param>
        internal NotificationParsingManager(SdmxSchemaEnumType sdmxSchema)
            : base(sdmxSchema)
        {
        }

        #endregion

        // TODO Test that there is a notification event in this message!
        #region Public Methods and Operators

        /// <summary>
        /// Parses the XML that is retrieved from the URI to create a notification event.
        ///     Makes sure the notification event is valid
        /// </summary>
        /// <param name="dataLocation">
        /// The data location of the SDMX XML file
        /// </param>
        /// <returns>
        /// The <see cref="INotificationEvent"/>.
        /// </returns>
        public virtual INotificationEvent CreateNotificationEvent(IReadableDataLocation dataLocation)
        {
            LoggingUtil.Debug(_log, "Parse Structure request, for xml at location: " + dataLocation);
            INotificationEvent notificationEvent;

            SdmxSchemaEnumType schemaVersion = this.GetSchemaVersion(dataLocation);
            LoggingUtil.Debug(_log, "Schema Version Determined to be : " + schemaVersion);

            ////XMLParser.ValidateXml(dataLocation, schemaVersion);
            LoggingUtil.Debug(_log, "XML VALID");
            using (Stream stream = dataLocation.InputStream)
            {
                using (XmlReader reader = XMLParser.CreateSdmxMlReader(stream, schemaVersion))
                {
                    switch (schemaVersion)
                    {
                        case SdmxSchemaEnumType.VersionTwo:
                            RegistryInterface rid = RegistryInterface.Load(reader);

                            if (rid.NotifyRegistryEvent == null)
                            {
                                throw new ArgumentException(
                                    "Can not parse message as NotifyRegistryEvent, as there are no NotifyRegistryEvent in message");
                            }

                            notificationEvent = new NotificationEventCore(rid.NotifyRegistryEvent);
                            break;
                        case SdmxSchemaEnumType.VersionTwoPointOne:
                            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.RegistryInterface rid21 =
                                Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.RegistryInterface.Load(reader);
                            NotifyRegistryEventType notifyRegistryEventType = rid21.Content.NotifyRegistryEvent;
                            if (notifyRegistryEventType == null)
                            {
                                throw new ArgumentException(
                                    "Can not parse message as NotifyRegistryEvent, as there are no NotifyRegistryEvent in message");
                            }

                            notificationEvent = new NotificationEventCore(notifyRegistryEventType);
                            break;
                        default:
                            throw new SdmxNotImplementedException(
                                ExceptionCode.Unsupported, "Parse NotificationEvent at version: " + schemaVersion);
                    }
                }
            }

            // TODO Validation
            return notificationEvent;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="IDataQueryParseManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    ///     The DataQueryParseManager interface.
    /// </summary>
    /// <example>
    ///     A sample implementation in C# of <see cref="IDataQueryParseManager" />
    ///     <code source="..\ReUsingExamples\DataQuery\ReUsingDataQueryParsingManager.cs" lang="cs" />
    /// </example>
    public interface IDataQueryParseManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Builds a <see cref="IDataQuery"/> list from a message that contains one or more data queries
        /// </summary>
        /// <param name="dataLocation">
        /// The data location
        /// </param>
        /// <param name="beanRetrievalManager">
        /// optional, if given will retrieve the key family bean the query is for
        /// </param>
        /// <returns>
        /// a <see cref="IDataQuery"/> list
        /// </returns>
        IList<IDataQuery> BuildDataQuery(
            IReadableDataLocation dataLocation, ISdmxObjectRetrievalManager beanRetrievalManager);

        /// <summary>
        /// Parse the specified <paramref name="query"/>.
        /// </summary>
        /// <param name="query">
        /// The REST data query.
        /// </param>
        /// <param name="beanRetrievalManager">
        /// The <c>SDMX</c> object retrieval manager.
        /// </param>
        /// <returns>
        /// The <see cref="IDataQuery"/> from <paramref name="query"/>.
        /// </returns>
        IDataQuery ParseRestQuery(string query, ISdmxObjectRetrievalManager beanRetrievalManager);

        /**
	     * Builds Complex Data Queries for 2.1 data query messages
	     * @param dataLocation
	     * @param beanRetrievalManager, if given will retrieve the data structure bean the query is for
	     * @return
	     */
        IList<IComplexDataQuery> BuildComplexDataQuery(IReadableDataLocation dataLocation, ISdmxObjectRetrievalManager beanRetrievalManager);


        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="BaseParsingManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing
{
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.Util.Log;

    /// <summary>
    ///     Base parsing manager.
    /// </summary>
    public abstract class BaseParsingManager
    {
        #region Static Fields

        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(BaseParsingManager));

        #endregion

        #region Fields

        /// <summary>
        ///     If set to a value other than <see cref="MessageEnumType.Null" />, this message type will be assumed for all input
        /// </summary>
        private readonly MessageEnumType _messageType;

        /// <summary>
        ///     If set to a value other than <see cref="SdmxSchemaEnumType.Null" />, this SDMX schema will be assumed for all input
        /// </summary>
        private readonly SdmxSchemaEnumType _sdmxSchema;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="BaseParsingManager" /> class.
        /// </summary>
        protected BaseParsingManager()
            : this(SdmxSchemaEnumType.Null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseParsingManager"/> class.
        /// </summary>
        /// <param name="sdmxSchema">
        /// The sdmx schema.
        /// </param>
        protected BaseParsingManager(SdmxSchemaEnumType sdmxSchema)
            : this(sdmxSchema, MessageEnumType.Null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseParsingManager"/> class.
        /// </summary>
        /// <param name="sdmxSchema">
        /// The sdmx schema.
        /// </param>
        /// <param name="messageType">
        /// The message Type.
        /// </param>
        protected BaseParsingManager(SdmxSchemaEnumType sdmxSchema, MessageEnumType messageType)
        {
            this._sdmxSchema = sdmxSchema;
            this._messageType = messageType;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets message type, if set from the constructor otherwise from the <paramref name="dataLocation"/>
        /// </summary>
        /// <param name="dataLocation">
        /// The data location.
        /// </param>
        /// <returns>
        /// The <see cref="MessageEnumType"/>.
        /// </returns>
        protected virtual MessageEnumType GetMessageType(IReadableDataLocation dataLocation)
        {
            SdmxSchemaEnumType sdmxSchemaEnumType = this.GetSchemaVersion(dataLocation);
            bool readDataLocation = this._messageType == MessageEnumType.Null
                                    && sdmxSchemaEnumType != SdmxSchemaEnumType.Edi;
            MessageEnumType messageType = readDataLocation
                                              ? SdmxMessageUtil.GetMessageType(dataLocation)
                                              : this._messageType;

            if (readDataLocation)
            {
                LoggingUtil.Debug(_log, "Message Type Determined to be : " + messageType);
            }
            else
            {
                LoggingUtil.Debug(_log, "Message Type specified on CTOR : " + messageType);
            }

            return messageType;
        }

        /// <summary>
        /// Gets schema version, if set from the constructor otherwise from the <paramref name="dataLocation"/>
        /// </summary>
        /// <param name="dataLocation">
        /// The data location.
        /// </param>
        /// <returns>
        /// The <see cref="SdmxSchemaEnumType"/>.
        /// </returns>
        protected virtual SdmxSchemaEnumType GetSchemaVersion(IReadableDataLocation dataLocation)
        {
            SdmxSchemaEnumType schemaVersion = this._sdmxSchema == SdmxSchemaEnumType.Null
                                                   ? SdmxMessageUtil.GetSchemaVersion(dataLocation)
                                                   : this._sdmxSchema;

            if (this._sdmxSchema == SdmxSchemaEnumType.Null)
            {
                LoggingUtil.Debug(_log, "Schema Version Determined to be : " + schemaVersion);
            }
            else
            {
                LoggingUtil.Debug(_log, "Using Schema Version specified on CTOR : " + schemaVersion);
            }

            return schemaVersion;
        }

        #endregion
    }
}
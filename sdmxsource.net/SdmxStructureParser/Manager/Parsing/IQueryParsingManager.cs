// -----------------------------------------------------------------------
// <copyright file="IQueryParsingManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing
{
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Workspace;

    /// <summary>
    ///     The QueryParsingManager interface.
    /// </summary>
    /// <example>
    ///     A sample implementation in C# of <see cref="IQueryParsingManager" />.
    ///     <code source="..\ReUsingExamples\StructureQuery\ReUsingQueryParsingManager.cs" lang="cs" />
    /// </example> 
    public interface IQueryParsingManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Processes the SDMX at the given URI and returns a workspace containing the information on what was being queried.
        ///     <p/>
        ///     The Query parsing manager processes queries that are in a RegistryInterface document, this includes queries for
        ///     Provisions, Registrations and Structures.  It also processes Queries that are in a QueryMessage document
        /// </summary>
        /// <param name="dataLocation">
        /// The SDMX URI
        /// </param>
        /// <returns>
        /// The <see cref="IQueryWorkspace"/>.
        /// </returns>
        IQueryWorkspace ParseQueries(IReadableDataLocation dataLocation);

        #endregion
    }
}
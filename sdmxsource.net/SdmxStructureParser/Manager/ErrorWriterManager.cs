﻿// -----------------------------------------------------------------------
// <copyright file="ErrorWriterManager.cs" company="EUROSTAT">
//   Date Created : 2013-06-06
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;

using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Output;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Structureparser.Factory;

namespace Org.Sdmxsource.Sdmx.Structureparser.Manager
{
    public class ErrorWriterManager : IErrorWriterManager
    {
        #region Static Fields

        /// <summary>
        /// The structure writer factory
        /// </summary>
        private readonly IErrorWriterFactory _errorWriterFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWritingManager"/> class.
        /// </summary>
        public ErrorWriterManager()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWritingManager"/> class.
        /// </summary>
        /// <param name="structureWriterFactory">
        /// The structure writer factory. If set to null the default factory will be used: <see cref="SdmxStructureWriterFactory"/>
        /// </param>
        public ErrorWriterManager(IErrorWriterFactory errorWriterFactory)
        {
            this._errorWriterFactory = errorWriterFactory ?? new SdmxErrorWriterFactory();
        }

        #endregion

        public int  WriteError(Exception ex, Stream outPutStream, IErrorFormat outputFormat)
        {
 	        return GetErrorWriterEngine(outputFormat).WriteError(ex, outPutStream);
        }
	
	    private IErrorWriterEngine GetErrorWriterEngine(IErrorFormat outputFormat) 
        {
            IErrorWriterEngine engine = this._errorWriterFactory.GetErrorWriterEngine(outputFormat);
            if (engine != null)
            {
                return engine;
            }
		    throw new SdmxNotImplementedException("Could not write error out in format: " + outputFormat);
	    }
    }
}

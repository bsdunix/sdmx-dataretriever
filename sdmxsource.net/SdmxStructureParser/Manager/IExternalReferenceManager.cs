// -----------------------------------------------------------------------
// <copyright file="IExternalReferenceManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Manager
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;

    /// <summary>
    ///     Manages the retrieval of externally referenced structures.  These are determined by the isExternal attribute of a maintainable artifact being set
    ///     to true, and the URI attribute containing the URI of the full artifact.
    ///     <p />
    /// </summary>
    public interface IExternalReferenceManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// Resolves external references, where the 'externalReference' attribute is set to 'true'.
        ///     The external reference locations are expected to be given by a URI, and the URI is expected to point to the
        ///     location of a valid SDMX document containing the referenced structure.
        ///     <p/>
        ///     External references can be of a different version to those that created the input StructureBeans.
        /// </summary>
        /// <param name="structures">
        /// containing structures which may have the external reference attribute set to `true`
        /// </param>
        /// <param name="isSubstitute">
        /// if set to true, this will substitute the external reference beans for the real beans
        /// </param>
        /// <param name="isLenient">
        /// The is Lenient.
        /// </param>
        /// <returns>
        /// a StructureBeans containing only the externally referenced beans
        /// </returns>
        ISdmxObjects ResolveExternalReferences(ISdmxObjects structures, bool isSubstitute, bool isLenient);

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="SdmxErrorWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2013-06-06
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing;
using Org.Sdmxsource.Sdmx.Structureparser.Model;

namespace Org.Sdmxsource.Sdmx.Structureparser.Factory
{
    /// <summary>
    /// The SDMX error writer factory.
    /// </summary>
    public class SdmxErrorWriterFactory : IErrorWriterFactory
    {
        /// <summary>
        /// The error writer engine.
        /// </summary>
        private readonly ErrorWriterEngineV21 _errorWriterEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxErrorWriterFactory"/> class.
        /// </summary>
        public SdmxErrorWriterFactory()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxErrorWriterFactory"/> class.
        /// </summary>
        /// <param name="errorWriterEngine">
        /// The error writer engine.
        /// </param>
        public SdmxErrorWriterFactory(ErrorWriterEngineV21 errorWriterEngine)
        {
            this._errorWriterEngine = errorWriterEngine ?? new ErrorWriterEngineV21();
        }

        /// <summary>
        /// Returns an error writer in the format specified.  Returns null if the format is unknown by the implementation
        /// </summary>
        /// <param name="format">
        /// The format
        /// </param>
        /// <returns>
        /// Engine, or null if the format is unknown to this factory
        /// </returns>
        public IErrorWriterEngine GetErrorWriterEngine(IErrorFormat format)
        {
            if (format is SdmxErrorFormat) 
            {
			    return this._errorWriterEngine;
		    }
		    return null;
        }
    }
}

// -----------------------------------------------------------------------
// <copyright file="IQueryWorkspace.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Workspace
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    // JAVADOC missing

    /// <summary>
    ///     The QueryWorkspace interface.
    /// </summary>
    public interface IQueryWorkspace
    {
        #region Public Properties

        /// <summary>
        ///     Gets a list of provision references
        /// </summary>
        IStructureReference ProvisionReferences { get; }

        /// <summary>
        /// Gets a ComplexStructureQuery 
        /// </summary>
        IComplexStructureQuery ComplexStructureQuery { get; }

        /// <summary>
        ///     Gets a list of registration references
        /// </summary>
        IStructureReference RegistrationReferences { get; }

        /// <summary>
        ///     Gets a value indicating whether the structure references should be resolved.
        ///     true if structure references should be resolved
        /// </summary>
        bool ResolveReferences { get; }

        /// <summary>
        ///     Gets a list of simple queries, these are queries by agency id, maintainable id version and id.
        ///     <p />
        ///     More complex queries such as query for DSD by dimension concept are not returned from this method call.
        /// </summary>
        IList<IStructureReference> SimpleStructureQueries { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Gets true if getProvisionReferences() returns a not null object
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        bool HasProvisionQueries();

        /// <summary>
        ///     Gets true if getRegistrationReferences() returns a not null object
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        bool HasRegistrationQueries();

        /// <summary>
        ///     Gets true if getSimpleStructureQueries() returns a list of 1 or more
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        bool HasStructureQueries();

        #endregion
    }
}
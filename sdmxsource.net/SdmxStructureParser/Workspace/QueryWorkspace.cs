// -----------------------------------------------------------------------
// <copyright file="QueryWorkspace.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Workspace
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    /// <summary>
    ///     The query workspace implementation
    /// </summary>
    public class QueryWorkspace : IQueryWorkspace
    {
        #region Fields

        /// <summary>
        ///     The provision references.
        /// </summary>
        private readonly IStructureReference _provisionReferences;

        /// <summary>
        ///     The registration references.
        /// </summary>
        private readonly IStructureReference _registrationReferences;

        /// <summary>
        ///     The resolve references.
        /// </summary>
        private readonly bool _resolveReferences;

        /// <summary>
        ///     The simple structure queries.
        /// </summary>
        private readonly IList<IStructureReference> _simpleStructureQueries;

        /// <summary>
        ///     The complex structure query.
        /// </summary>
        private IComplexStructureQuery _complexStructureQuery;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryWorkspace"/> class.
        /// </summary>
        /// <param name="structureReference">
        /// The structure reference.
        /// </param>
        /// <param name="resolveReferences0">
        /// The resolve references 0.
        /// </param>
        public QueryWorkspace(IStructureReference structureReference, bool resolveReferences0)
        {
            this._simpleStructureQueries = new List<IStructureReference> { structureReference };
            this._resolveReferences = resolveReferences0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryWorkspace"/> class.
        /// </summary>
        /// <param name="complexStructureQuery">
        /// The complex structure reference.
        /// </param>
        public QueryWorkspace(IComplexStructureQuery complexStructureQuery)
        {
            this._complexStructureQuery = complexStructureQuery;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryWorkspace"/> class.
        /// </summary>
        /// <param name="provisionReferences0">
        /// The provision references 0.
        /// </param>
        /// <param name="registrationReferences1">
        /// The registration references 1.
        /// </param>
        /// <param name="structureReferences">
        /// The structure references.
        /// </param>
        /// <param name="resolveReferences2">
        /// The resolve references 2.
        /// </param>
        public QueryWorkspace(
            IStructureReference provisionReferences0,
            IStructureReference registrationReferences1,
            IList<IStructureReference> structureReferences,
            bool resolveReferences2)
        {
            this._provisionReferences = provisionReferences0;
            this._registrationReferences = registrationReferences1;
            this._simpleStructureQueries = structureReferences;
            this._resolveReferences = resolveReferences2;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the provision references.
        /// </summary>
        public virtual IStructureReference ProvisionReferences
        {
            get
            {
                return this._provisionReferences;
            }
        }

        public IComplexStructureQuery ComplexStructureQuery
        {
            get
            {
                return this._complexStructureQuery;
            }
        }

        /// <summary>
        ///     Gets the registration references.
        /// </summary>
        public virtual IStructureReference RegistrationReferences
        {
            get
            {
                return this._registrationReferences;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether resolve references.
        /// </summary>
        public virtual bool ResolveReferences
        {
            get
            {
                return this._resolveReferences;
            }
        }

        /// <summary>
        ///     Gets the simple structure queries.
        /// </summary>
        public virtual IList<IStructureReference> SimpleStructureQueries
        {
            get
            {
                return this._simpleStructureQueries;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The has provision queries.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public virtual bool HasProvisionQueries()
        {
            return this._provisionReferences != null;
        }

        /// <summary>
        ///     The has registration queries.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public virtual bool HasRegistrationQueries()
        {
            return this._registrationReferences != null;
        }

        /// <summary>
        ///     The has structure queries.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public virtual bool HasStructureQueries()
        {
            return this._simpleStructureQueries != null && this._simpleStructureQueries.Count > 0;
        }

        #endregion
    }
}
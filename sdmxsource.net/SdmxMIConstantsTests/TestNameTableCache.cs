﻿// -----------------------------------------------------------------------
// <copyright file="TestNameTableCache.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxMlConstantsTests.
// 
//     SdmxMlConstantsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxMlConstantsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxMlConstantsTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxMlConstantsTests
{
    using Estat.Sri.SdmxXmlConstants;

    using NUnit.Framework;

    /// <summary>
    ///     Test unit for <see cref="NameTableCache" />
    /// </summary>
    [TestFixture]
    public class TestNameTableCache
    {
        #region Public Methods and Operators

        /// <summary>
        /// The test is attribute.
        /// </summary>
        /// <param name="localName">
        /// The local name.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="shouldEqual">
        /// The should equal.
        /// </param>
        [Test]
        [TestCase("id", AttributeNameTable.id, true)]
        [TestCase("returnDetails", AttributeNameTable.returnDetails, true)]
        [TestCase("FooStructure", AttributeNameTable.returnDetails, false)]
        public void TestIsAttribute(string localName, AttributeNameTable element, bool shouldEqual)
        {
            string add = NameTableCache.Instance.NameTable.Add(localName);
            Assert.IsTrue(NameTableCache.IsAttribute(add, element) == shouldEqual);
        }

        /// <summary>
        /// Test unit for <see cref="NameTableCache.IsElement"/>
        /// </summary>
        /// <param name="localName">
        /// The local Name.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="shouldEqual">
        /// The should Equal.
        /// </param>
        [Test]
        [TestCase("Structure", ElementNameTable.Structure, true)]
        [TestCase("FooStructure", ElementNameTable.Structure, false)]
        public void TestIsElement(string localName, ElementNameTable element, bool shouldEqual)
        {
            string add = NameTableCache.Instance.NameTable.Add(localName);
            Assert.IsTrue(NameTableCache.IsElement(add, element) == shouldEqual);
        }

        #endregion
    }
}
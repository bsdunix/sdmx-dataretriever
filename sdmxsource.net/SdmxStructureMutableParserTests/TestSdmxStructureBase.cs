﻿// -----------------------------------------------------------------------
// <copyright file="TestSdmxStructureBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureMutableParserTests.
// 
//     SdmxStructureMutableParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureMutableParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxStructureMutableParserTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Xml.Schema;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// The test sdmx structure base.
    /// </summary>
    public abstract class TestSdmxStructureBase
    {
        #region Methods

        /// <summary>
        /// Compare artefacts of type <typeparamref name="T"/>.
        /// </summary>
        /// <param name="newArtefacts">
        /// The new artefacts of type <typeparamref name="T"/>.
        /// </param>
        /// <param name="getOriginal">
        /// The function to get  the original of type <typeparamref name="T"/>.
        /// </param>
        /// <typeparam name="T">
        /// The artefact type
        /// </typeparam>
        protected static void CompareArtefacts<T>(IEnumerable<T> newArtefacts, Func<IMaintainableRefObject, ISet<T>> getOriginal) where T : IMaintainableObject
        {
            foreach (T artefact in newArtefacts)
            {
                ISet<T> originalArtefact = getOriginal(artefact.AsReference.MaintainableReference);
                if (originalArtefact.Count == 1)
                {
                    foreach (T schemeObject in originalArtefact)
                    {
                        Assert.IsTrue(artefact.DeepEquals(schemeObject, true), artefact.AsReference.MaintainableReference.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// The on validation event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        protected static void OnValidationEventHandler(object sender, ValidationEventArgs args)
        {
            Trace.WriteLine(args.Message);
            if (args.Exception == null)
            {
                return;
            }

            Trace.WriteLine(args.Exception);
            Trace.WriteLine(string.Format("{3}:{0} Column: {1}. Error:\n {2}", args.Exception.SourceUri, args.Exception.LineNumber, args.Exception.LinePosition, args.Exception.Message));
            throw args.Exception;
        }

        #endregion
    }
}
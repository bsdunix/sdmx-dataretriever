// -----------------------------------------------------------------------
// <copyright file="TestURIUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System;
    using System.IO;

    using NUnit.Framework;

    using Org.Sdmxsource.Util.Io;

    /// <summary>
    ///     Test unit class for <see cref="URIUtil" />
    /// </summary>
    [TestFixture]
    public class TestUriUtil
    {
        #region Public Methods and Operators

        /// <summary>
        /// Test method for <see cref="URIUtil.GetFileFromStream"/>
        /// </summary>
        /// <param name="testFile">
        /// The test File.
        /// </param>
        [TestCase("tests/TestFile.csv")]
        public void TestGetFileFromStream(string testFile)
        {
            URIUtil tempUriUtil = URIUtil.TempUriUtil;
            Assert.IsNotNull(tempUriUtil);
            FileInfo file = tempUriUtil.GetFileFromStream(File.OpenRead(testFile));
            Assert.NotNull(file);
            Assert.IsTrue(file.Exists);
            FileInfo file2 = tempUriUtil.GetFileFromStream(File.OpenRead(testFile));
            Assert.IsTrue(file2.Exists);
            Assert.AreNotEqual(file2.FullName, file.FullName);
        }

        /// <summary>
        /// Test method for <see cref="URIUtil.GetFileFromUri"/>
        /// </summary>
        /// <param name="location">
        /// The location.
        /// </param>
        [TestCase("https://registry.sdmx.org/schemas/v2_0/SDMXMessage.xsd")]
        public void TestGetFileFromUri(string location)
        {
            URIUtil tempUriUtil = URIUtil.TempUriUtil;
            Assert.IsNotNull(tempUriUtil);
            FileInfo file = tempUriUtil.GetFileFromUri(new Uri(location));
            Assert.IsTrue(file.Exists);
            FileInfo file2 = tempUriUtil.GetFileFromUri(new Uri(location));
            Assert.IsTrue(file2.Exists);
            Assert.AreNotEqual(file2.FullName, file.FullName);
        }

        /// <summary>
        ///     Test method for <see cref="URIUtil.TempUriUtil" />
        /// </summary>
        [Test]
        public void TestTempUriUtil()
        {
            Assert.IsNotNull(URIUtil.TempUriUtil);
            FileInfo tempFile = URIUtil.TempUriUtil.GetTempFile();
            Assert.IsNotNull(tempFile);
            Assert.IsTrue(tempFile.Exists);
        }

        #endregion
    }
}
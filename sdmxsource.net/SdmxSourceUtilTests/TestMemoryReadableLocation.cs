// -----------------------------------------------------------------------
// <copyright file="TestMemoryReadableLocation.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System.Text;

    using NUnit.Framework;

    using Org.Sdmxsource.Util.Io;

    /// <summary> Test unit class for <see cref="MemoryReadableLocation"/> </summary>
    [TestFixture]
    public class TestMemoryReadableLocation
    {
        /// <summary>Test method for <see cref="MemoryReadableLocation.InputStream"/> </summary>
        [Test]
        public void Test()
        {
            const string Buffer = "123456";

            using (var readable = new MemoryReadableLocation(Encoding.UTF8.GetBytes(Buffer)))
            {
                Assert.IsTrue(readable.InputStream.ReadByte() > 0);
                Assert.AreNotSame(readable.InputStream, readable.InputStream);
            }
        }
    }
}
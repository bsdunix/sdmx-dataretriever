// -----------------------------------------------------------------------
// <copyright file="TestIdentifiableRefObjetcImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System;
    using System.ComponentModel;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     Test unit class for <see cref="IdentifiableRefObjetcImpl" />
    /// </summary>
    [TestFixture]
    public class TestIdentifiableRefObjetcImpl
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Test method for <see cref="IdentifiableRefObjetcImpl" />
        /// </summary>
        [Test]
        public void Test()
        {
            foreach (SdmxStructureType sdmxStructureType in SdmxStructureType.Values)
            {
                if (sdmxStructureType.IsIdentifiable)
                {
                    if (sdmxStructureType.ParentStructureType != null
                        && sdmxStructureType.ParentStructureType.IsMaintainable)
                    {
                        Uri urn = sdmxStructureType.ParentStructureType.GenerateUrn("AGENCY", "ID", "2.2");
                        SdmxStructureType targetObj = sdmxStructureType;
                        
                        var result = new IdentifiableRefObjetcImpl(new StructureReferenceImpl(urn), GetID(targetObj), targetObj);
                        Assert.AreEqual(result.StructureEnumType.EnumType, targetObj.EnumType);
                        Assert.AreEqual(GetID(targetObj), result.Id);
                        Assert.IsNull(result.ChildReference);
                        Assert.IsNull(result.ParentIdentifiableReference);
                        Assert.AreEqual(
                            sdmxStructureType.ParentStructureType, 
                            result.ParentMaintainableReferece.MaintainableStructureEnumType);
                        if (!targetObj.HasFixedId)
                        {
                            var ids = new[] { "1", "2", "A", "B" };
                            result = new IdentifiableRefObjetcImpl(new StructureReferenceImpl(urn), ids, targetObj);
                            Assert.AreEqual(result.StructureEnumType.EnumType, targetObj.EnumType);
                            Assert.AreEqual("1", result.Id);
                            Assert.AreEqual("2", result.ChildReference.Id);
                            Assert.IsNotNull(result.ChildReference);
                            Assert.IsNotNull(result.ChildReference.ParentIdentifiableReference);
                            Assert.IsNull(result.ParentIdentifiableReference);
                            Assert.AreEqual(sdmxStructureType.ParentStructureType, result.ParentMaintainableReferece.MaintainableStructureEnumType);
                            IIdentifiableRefObject child = result;
                            for (int i = 0; i < ids.Length; i++)
                            {
                                string id = ids[i];
                                Assert.AreEqual(id, child.Id);
                                child = child.ChildReference;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the unique identifier.
        /// </summary>
        /// <param name="targetObj">
        /// The target object.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetID(SdmxStructureType targetObj)
        {
            return targetObj.HasFixedId ? targetObj.FixedId : "TEST";
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="TestLocaleUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System.Collections.Generic;
    using System.Globalization;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary> Test unit class for <see cref="LocaleUtil"/> </summary>
    [TestFixture]
    public class TestLocaleUtil
    {
        /// <summary>
        /// Test method for <see cref="LocaleUtil.BuildLocalMap"/> 
        /// </summary>
        /// <param name="locale">
        /// The locale.
        /// </param>
        /// <param name="text">
        /// The text.
        /// </param>
        [TestCase("en","This is a test")]
        [TestCase("el", "��� ������")]
        [TestCase("INVALID_LOCALE", "SHOULD FAIL", ExpectedException = typeof(SdmxSemmanticException))]
        public void Test(string locale, string text)
        {
            LocaleUtil.BuildLocalMap(new List<ITextTypeWrapper> { new TextTypeWrapperImpl(locale, text, null) });
        }
    }
}
// -----------------------------------------------------------------------
// <copyright file="TestUtilBase.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;

    using Estat.Sri.SdmxStructureMutableParser.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// The test utility base.
    /// </summary>
    public abstract class TestUtilBase
    {
        #region Methods

        /// <summary>
        /// The read structure workspace.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="test">
        /// The test.
        /// </param>
        protected static void ReadStructureMutable(string file, Action<ISdmxObjects> test)
        {
            IStructureParsingManager parsingManager = new StructureMutableParsingManager();
            var sw = new Stopwatch();
            sw.Start();
            ISdmxObjects structureBeans;
            using (IReadableDataLocation fileReadableDataLocation = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = parsingManager.ParseStructures(fileReadableDataLocation);
                Assert.NotNull(structureWorkspace);
                structureBeans = structureWorkspace.GetStructureObjects(false);
                Assert.NotNull(structureBeans);
            }

            sw.Stop();
            Trace.WriteLine(string.Format(CultureInfo.InvariantCulture, "StructureParser : Reading {0} took {1}", file, sw.Elapsed));
            test(structureBeans);
        }

        /// <summary>
        /// The read structure workspace.
        /// </summary>
        /// <param name="sdmxVersion">
        /// The SDMX Version.
        /// </param>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="test">
        /// The test.
        /// </param>
        protected static void ReadStructureWorkspace(SdmxSchemaEnumType sdmxVersion, string file, Action<ISdmxObjects> test)
        {
            IStructureParsingManager parsingManager = new StructureParsingManager(sdmxVersion);
            var sw = new Stopwatch();
            sw.Start();
            ISdmxObjects structureBeans;
            using (IReadableDataLocation fileReadableDataLocation = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = parsingManager.ParseStructures(fileReadableDataLocation);
                Assert.NotNull(structureWorkspace);
                structureBeans = structureWorkspace.GetStructureObjects(false);
                Assert.NotNull(structureBeans);
            }

            sw.Stop();
            Trace.WriteLine(string.Format(CultureInfo.InvariantCulture, "StructureParser : Reading {0} took {1}", file, sw.Elapsed));
            test(structureBeans);
        }

        /// <summary>
        /// The read structure workspace.
        /// </summary>
        /// <param name="sdmxVersion">
        /// The SDMX Version.
        /// </param>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="test">
        /// The test.
        /// </param>
        protected static void ReadStructureMutable(string file, Action<IMaintainableObject, ISet<IMaintainableObject>> test)
        {
            Action<ISdmxObjects> action = objects =>
            {
                ISet<IMaintainableObject> allMaintinables = objects.GetAllMaintainables();
                CollectionAssert.IsNotEmpty(allMaintinables);
                foreach (IMaintainableObject maintainableObject in allMaintinables)
                {
                    test(maintainableObject, allMaintinables);
                }
            };

            ReadStructureMutable(file, action);
        }

        /// <summary>
        /// The read structure workspace.
        /// </summary>
        /// <param name="sdmxVersion">
        /// The SDMX Version.
        /// </param>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="test">
        /// The test.
        /// </param>
        protected static void ReadStructureWorkspace(SdmxSchemaEnumType sdmxVersion, string file, Action<IMaintainableObject, ISet<IMaintainableObject>> test)
        {
            Action<ISdmxObjects> action = objects =>
                {
                    ISet<IMaintainableObject> allMaintinables = objects.GetAllMaintainables();
                    CollectionAssert.IsNotEmpty(allMaintinables);
                    foreach (IMaintainableObject maintainableObject in allMaintinables)
                    {
                        test(maintainableObject, allMaintinables);
                    }
                };

            ReadStructureWorkspace(sdmxVersion, file, action);
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="TestSdmxObjectUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    /// Test unit for <see cref="SdmxObjectUtil"/>
    /// </summary>
    [TestFixture]
    public class TestSdmxObjectUtil
    {
        /// <summary>
        /// Test unit for <see cref="SdmxObjectUtil.CreateTertiary(bool,bool)"/> 
        /// </summary>
        /// <param name="isSet">
        /// The is Set.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="expectedValue">
        /// The expected Value.
        /// </param>
        [TestCase(false, false, TertiaryBoolEnumType.Unset)]
        [TestCase(false, true, TertiaryBoolEnumType.Unset)]
        [TestCase(true, false, TertiaryBoolEnumType.False)]
        [TestCase(true, true, TertiaryBoolEnumType.True)]
        public void TestCreateTertiary(bool isSet, bool value, TertiaryBoolEnumType expectedValue)
        {
           Assert.AreEqual(expectedValue, SdmxObjectUtil.CreateTertiary(isSet, value).EnumType); 
        }

        /// <summary>
        /// Test unit for <see cref="SdmxObjectUtil.CreateTertiary(bool,bool)"/> 
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="expectedValue">
        /// The expected Value.
        /// </param>
        [TestCase(null, TertiaryBoolEnumType.Unset)]
        [TestCase(false, TertiaryBoolEnumType.False)]
        [TestCase(true, TertiaryBoolEnumType.True)]
        public void TestCreateTertiary2(bool? value, TertiaryBoolEnumType expectedValue)
        {
            Assert.AreEqual(expectedValue, SdmxObjectUtil.CreateTertiary(value).EnumType);
        }
    }
}
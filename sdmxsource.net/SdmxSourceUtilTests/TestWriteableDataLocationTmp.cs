// -----------------------------------------------------------------------
// <copyright file="TestWriteableDataLocationTmp.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System.IO;

    using NUnit.Framework;

    using Org.Sdmxsource.Util.Io;

    /// <summary> Test unit class for <see cref="WriteableDataLocationTmp"/> </summary>
    [TestFixture]
    public class TestWriteableDataLocationTmp
    {
        /// <summary>Test method for <see cref="WriteableDataLocationTmp"/> </summary>
        [Test]
        public void Test()
        {
            using (var writeable = new WriteableDataLocationTmp())
            {
                using (Stream outputStream = writeable.OutputStream)
                {
                    Assert.IsTrue(outputStream.CanWrite);
                    outputStream.WriteByte(0x1);
                    outputStream.Flush();
                }

                using (Stream inputStream = writeable.InputStream)
                {
                    Assert.IsTrue(inputStream.CanRead);
                    int readByte = inputStream.ReadByte();
                    Assert.AreEqual(readByte,0x1);
                }
            }
        }
    }
}
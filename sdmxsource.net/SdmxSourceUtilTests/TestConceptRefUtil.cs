// -----------------------------------------------------------------------
// <copyright file="TestConceptRefUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using Moq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    /// Test unit for <see cref="ConceptRefUtil"/>
    /// </summary>
    [TestFixture]
    public class TestConceptRefUtil
    {

        /// <summary>
        /// Test unit for <see cref="ConceptRefUtil.BuildConceptRef"/> 
        /// </summary>
        [Test]
        public void TestBuildConceptRef()
        {
            var moq = new Mock<IDimension>();
            moq.Setup(dimension => dimension.Id).Returns("DIM1");
            ICrossReference buildConceptRef = ConceptRefUtil.BuildConceptRef(moq.Object, "AGENCYTEST", "TESTCS", "1.2", "AGENCYTEST", "CAT1");
            Assert.NotNull(buildConceptRef);
            Assert.NotNull(buildConceptRef.ChildReference);
            Assert.AreEqual(buildConceptRef.ChildReference.StructureEnumType.EnumType, SdmxStructureEnumType.Concept);
            Assert.AreEqual("urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=AGENCYTEST:TESTCS(1.2).CAT1", buildConceptRef.TargetUrn.ToString());
        }

        /// <summary>
        /// Test unit for <see cref="ConceptRefUtil.GetConceptId"/> 
        /// </summary>
        [Test]
        public void TestGetConceptId()
        {
            var moq = new Mock<IStructureReference>();
            moq.Setup(reference => reference.ChildReference.Id).Returns("CAT1");
            moq.Setup(reference => reference.TargetReference).Returns(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept));
           
            IStructureReference conceptRef = moq.Object;
            string conceptId = ConceptRefUtil.GetConceptId(conceptRef);
            Assert.AreEqual("CAT1", conceptId);
        }
    }
}
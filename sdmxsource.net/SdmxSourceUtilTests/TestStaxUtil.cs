// -----------------------------------------------------------------------
// <copyright file="TestStaxUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System.Xml;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Util.Xml;

    /// <summary>
    ///     Test unit for <see cref="StaxUtil" />
    /// </summary>
    [TestFixture]
    public class TestStaxUtil
    {
        #region Public Methods and Operators

        /// <summary>
        /// Test unit for <see cref="StaxUtil.SkipNode"/>
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [Test]
        [TestCase("tests/v20/queryResponse-estat-sts.xml")]
        [TestCase("tests/v20/QueryResponseDataflowCategories.xml")]
        [TestCase("tests/v20/QueryStructureResponse.xml")]
        public void TestSkipNode(string file)
        {
            using (XmlReader reader = XmlReader.Create(file))
            {
                reader.ReadToDescendant("Header", "http://www.SDMX.org/resources/SDMXML/schemas/v2_0/message");
                StaxUtil.SkipNode(reader);
                reader.MoveToContent();
                Assert.AreEqual("QueryStructureResponse", reader.LocalName);
            }
        }

        /// <summary>
        /// Test unit for <see cref="StaxUtil.SkipToEndNode"/>
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [Test]
        [TestCase("tests/v20/queryResponse-estat-sts.xml")]
        [TestCase("tests/v20/QueryResponseDataflowCategories.xml")]
        [TestCase("tests/v20/QueryStructureResponse.xml")]
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml")]
        [TestCase("tests/v20/CENSUSHUB+ESTAT+1.1_alllevels.xml")]
        [TestCase("tests/v20/CL_SEX_v1.1.xml")]
        [TestCase("tests/v20/ESTAT+DEMOGRAPHY+2.1.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE+2.0.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE_NZ+2.1.xml")]
        [TestCase("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0.xml")]
        [TestCase("tests/v20/ESTAT+STS+2.0.xml")]
        [TestCase("tests/v20/ESTAT+TESTLEVELS+1.0.xml")]
        [TestCase("tests/v20/ESTAT_CPI_v1.0.xml")]
        [TestCase("tests/v20/CENSAGR_CAPOAZ_GEN+IT1+1.3.xml")]
        [TestCase("tests/v20/EGR_1_TS+ESTAT+1.4.xml")]
        public void TestSkipToEndNode(string file)
        {
            using (XmlReader reader = XmlReader.Create(file))
            {
                Assert.IsTrue(StaxUtil.SkipToEndNode(reader, "Header"));
            }
        }

        /// <summary>
        /// Test unit for <see cref="StaxUtil.SkipToNode"/>
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [Test]
        [TestCase("tests/v20/queryResponse-estat-sts.xml")]
        [TestCase("tests/v20/QueryResponseDataflowCategories.xml")]
        [TestCase("tests/v20/QueryStructureResponse.xml")]
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml")]
        [TestCase("tests/v20/CENSUSHUB+ESTAT+1.1_alllevels.xml")]
        [TestCase("tests/v20/CL_SEX_v1.1.xml")]
        [TestCase("tests/v20/ESTAT+DEMOGRAPHY+2.1.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE+2.0.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE_NZ+2.1.xml")]
        [TestCase("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0.xml")]
        [TestCase("tests/v20/ESTAT+STS+2.0.xml")]
        [TestCase("tests/v20/ESTAT+TESTLEVELS+1.0.xml")]
        [TestCase("tests/v20/ESTAT_CPI_v1.0.xml")]
        [TestCase("tests/v20/CENSAGR_CAPOAZ_GEN+IT1+1.3.xml")]
        [TestCase("tests/v20/EGR_1_TS+ESTAT+1.4.xml")]
        public void TestSkipToNode(string file)
        {
            using (XmlReader reader = XmlReader.Create(file))
            {
                Assert.IsTrue(StaxUtil.SkipToNode(reader, "Header"));
            }
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="TestRandomUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using NUnit.Framework;

    using Org.Sdmxsource.Util.Random;

    /// <summary> Test unit class for <see cref="RandomUtil"/> </summary>
    [TestFixture]
    public class TestRandomUtil
    {
        /// <summary>
        /// Test method for <see cref="RandomUtil.GenerateRandomPassword"/> 
        /// </summary>
        /// <param name="len">
        /// The len.
        /// </param>
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(5)]
        [TestCase(15)]
        [TestCase(32)]
        [TestCase(1024)]
        public void TestGenerateRandomPassword(int len)
        {
            string generateRandomPassword = RandomUtil.GenerateRandomPassword(len);
            Assert.IsNotNull(generateRandomPassword);
            Assert.AreEqual(len, generateRandomPassword.Length);
            if (len > 0)
            {
                for (int i = 0; i < len; i++)
                {
                    Assert.AreNotEqual(generateRandomPassword, RandomUtil.GenerateRandomPassword(len));
                }
            }
        }

        /// <summary>Test method for <see cref="RandomUtil.GenerateRandomString"/> </summary>
        [Test]
        public void TestGenerateRandomString()
        {
            string generateRandomString = RandomUtil.GenerateRandomString();
            Assert.IsNotNullOrEmpty(generateRandomString);
            for (int i = 0; i < 10; i++)
            {
                Assert.AreNotEqual(generateRandomString, RandomUtil.GenerateRandomString());
            }
        }
    }
}
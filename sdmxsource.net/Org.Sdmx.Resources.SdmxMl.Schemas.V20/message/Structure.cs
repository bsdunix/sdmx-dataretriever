﻿// -----------------------------------------------------------------------
// <copyright file="Structure.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V20.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V20 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V20 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V20.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V20.message
{
    using System.Xml;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.compact;

    using Xml.Schema.Linq;

    public partial class Structure
    {
        /// <summary>
        /// Load error message to a LINQ2XSD <see cref="Structure"/> object
        /// </summary>
        /// <param name="xmlFile">The input file</param>
        /// <returns>a LINQ2XSD <see cref="Structure"/> object</returns>
        public static Structure Load(XmlReader xmlFile)
        {
            return XTypedServices.Load<Structure, StructureType>(xmlFile, LinqToXsdTypeManager.Instance);
        } 
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="TextTypeTypeConstants.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V20.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V20 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V20 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V20.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V20.structure
{
    /// <summary>
    /// Provides constants for <see cref="TextTypeType"/> because the auto-generated class does not provided them 
    /// </summary>
    public static class TextTypeTypeConstants
    {
        #region Constants

        /// <summary>
        /// The big integer.
        /// </summary>
        public const string BigInteger = "BigInteger";

        /// <summary>
        /// The boolean.
        /// </summary>
        public const string Boolean = "Boolean";

        /// <summary>
        /// The count.
        /// </summary>
        public const string Count = "Count";

        /// <summary>
        /// The date.
        /// </summary>
        public const string Date = "Date";

        /// <summary>
        /// The date time.
        /// </summary>
        public const string DateTime = "DateTime";

        /// <summary>
        /// The day.
        /// </summary>
        public const string Day = "Day";

        /// <summary>
        /// The decimal.
        /// </summary>
        public const string Decimal = "Decimal";

        /// <summary>
        /// The double.
        /// </summary>
        public const string Double = "Double";

        /// <summary>
        /// The duration.
        /// </summary>
        public const string Duration = "Duration";

        /// <summary>
        /// The exclusive value range.
        /// </summary>
        public const string ExclusiveValueRange = "ExclusiveValueRange";

        /// <summary>
        /// The float.
        /// </summary>
        public const string Float = "Float";

        /// <summary>
        /// The inclusive value range.
        /// </summary>
        public const string InclusiveValueRange = "InclusiveValueRange";

        /// <summary>
        /// The incremental.
        /// </summary>
        public const string Incremental = "Incremental";

        /// <summary>
        /// The integer.
        /// </summary>
        public const string Integer = "Integer";

        /// <summary>
        /// The long.
        /// </summary>
        public const string Long = "Long";

        /// <summary>
        /// The month.
        /// </summary>
        public const string Month = "Month";

        /// <summary>
        /// The month day.
        /// </summary>
        public const string MonthDay = "MonthDay";

        /// <summary>
        /// The observational time period.
        /// </summary>
        public const string ObservationalTimePeriod = "ObservationalTimePeriod";

        /// <summary>
        /// The short.
        /// </summary>
        public const string Short = "Short";

        /// <summary>
        /// The string.
        /// </summary>
        public const string String = "String";

        /// <summary>
        /// The time.
        /// </summary>
        public const string Time = "Time";

        /// <summary>
        /// The timespan.
        /// </summary>
        public const string Timespan = "Timespan";

        /// <summary>
        /// The uri.
        /// </summary>
        public const string URI = "URI";

        /// <summary>
        /// The year.
        /// </summary>
        public const string Year = "Year";

        /// <summary>
        /// The year month.
        /// </summary>
        public const string YearMonth = "YearMonth";

        #endregion
    }
}
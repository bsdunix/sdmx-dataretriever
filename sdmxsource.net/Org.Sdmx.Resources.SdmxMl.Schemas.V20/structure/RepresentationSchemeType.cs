﻿// -----------------------------------------------------------------------
// <copyright file="RepresentationSchemeType.cs" company="EUROSTAT">
//   Date Created : 2014-12-15
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V20.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V20 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     Org.Sdmx.Resources.SdmxMl.Schemas.V20 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V20.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V20.structure
{
    using System.Xml.Linq;
    using System.Xml.Schema;

    using Xml.Schema.Linq;

    /// <summary>
    /// The representation scheme type.
    /// </summary>
    public partial class RepresentationSchemeType
    {
        /// <summary>
        /// Gets or sets the representation scheme version. This is an ESTAT extension.
        /// </summary>
        /// <value>
        /// The representation scheme version. This is an ESTAT extension.
        /// </value>
        public string RepresentationSchemeVersionEstat
        {
            get
            {
                XAttribute x = this.Attribute(XName.Get("representationSchemeVersion", string.Empty));
                return XTypedServices.ParseValue<string>(x, XmlSchemaType.GetBuiltInSimpleType(XmlTypeCode.String).Datatype);
            }

            set
            {
                this.SetAttribute(XName.Get("representationSchemeVersion", string.Empty), value, XmlSchemaType.GetBuiltInSimpleType(XmlTypeCode.String).Datatype);
            }
        }
    }
}
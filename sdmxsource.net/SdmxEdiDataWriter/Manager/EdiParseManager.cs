﻿// -----------------------------------------------------------------------
// <copyright file="EdiParseManager.cs" company="EUROSTAT">
//   Date Created : 2014-07-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Manager
{
    using System.Collections.Generic;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.EdiParser.Engine;
    using Org.Sdmxsource.Sdmx.EdiParser.Model;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    ///     The EDI parse manager.
    /// </summary>
    public class EdiParseManager : IEdiParseManager
    {
        #region Fields

        /// <summary>
        /// The _edi parse engine.
        /// </summary>
        private readonly IEdiParseEngine _ediParseEngine;

        /// <summary>
        /// The _writeable data location factory.
        /// </summary>
        private readonly IWriteableDataLocationFactory _writeableDataLocationFactory;

        /// <summary>
        /// The EDI structure engine.
        /// </summary>
        private readonly IEdiStructureWriterEngine _ediStructureEngine;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EdiParseManager"/> class.
        /// </summary>
        public EdiParseManager()
            : this(null, null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EdiParseManager"/> class.
        /// </summary>
        /// <param name="writeableDataLocationFactory">
        /// The writeable data location factory.
        /// </param>
        /// <param name="parseEngine">
        /// The parse engine.
        /// </param>
        /// <param name="ediStructureEngine">
        /// The edi structure engine.
        /// </param>
        public EdiParseManager(IWriteableDataLocationFactory writeableDataLocationFactory, IEdiParseEngine parseEngine, IEdiStructureWriterEngine ediStructureEngine)
        {
            this._ediStructureEngine = ediStructureEngine;
            this._writeableDataLocationFactory = writeableDataLocationFactory ?? new WriteableDataLocationFactory();
            this._ediParseEngine = parseEngine ?? new EdiParseEngine();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Processes an EDI message and returns a workspace containing the SDMX structures and data that were contained in the
        ///     message
        /// </summary>
        /// <param name="ediMessageLocation">
        /// The EDI message location.
        /// </param>
        /// <returns>
        /// The <see cref="IEdiWorkspace"/>.
        /// </returns>
        public IEdiWorkspace ParseEdiMessage(IReadableDataLocation ediMessageLocation)
        {
            return new EdiWorkspace(ediMessageLocation, this._writeableDataLocationFactory, this._ediParseEngine);
        }

        /// <summary>
        /// Writes the <paramref name="objects"/> contents out as EDI-TS to the <paramref name="output"/>
        /// </summary>
        /// <param name="objects">
        /// The objects.
        /// </param>
        /// <param name="output">
        /// The output.
        /// </param>
        public void WriteToEdi(ISdmxObjects objects, Stream output)
        {
            if (this._ediStructureEngine == null)
            {
                throw new SdmxNotImplementedException("IEdiStructureWriterEngine not provided.");
            }

            // Check that what is being written is supported by EDI
            ValidateSupport(objects);

            this._ediStructureEngine.WriteToEDI(objects, output);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates all the Maintainable Artefacts in the beans container are supported by the SDMX v1.0 syntax
        /// </summary>
        /// <param name="beans">
        /// The beans.
        /// </param>
        /// <exception cref="SdmxNotImplementedException">
        /// Structure not supported by SDMX-EDI
        /// </exception>
        private static void ValidateSupport(ISdmxObjects beans)
        {
            IList<SdmxStructureEnumType> supportedStructres = new List<SdmxStructureEnumType>();
            supportedStructres.Add(SdmxStructureEnumType.AgencyScheme);
            supportedStructres.Add(SdmxStructureEnumType.Dsd);
            supportedStructres.Add(SdmxStructureEnumType.ConceptScheme);
            supportedStructres.Add(SdmxStructureEnumType.CodeList);

            foreach (var maintainableBean in beans.GetAllMaintainables())
            {
                if (!supportedStructres.Contains(maintainableBean.StructureType.EnumType))
                {
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, maintainableBean.StructureType.StructureType + " is not a supported by SDMX-EDI");
                }
            }
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="IEdiParseManager.cs" company="EUROSTAT">
//   Date Created : 2014-05-16
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Manager
{
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.EdiParser.Model;

    /// <summary>
    /// The EdiParseManager interface.
    /// </summary>
    public interface IEdiParseManager
    {
        /// <summary>
        /// Writes the <paramref name="objects"/> contents out as EDI-TS to the <paramref name="output"/>
        /// </summary>
        /// <param name="objects">The objects.</param>
        /// <param name="output">The output.</param>
        void WriteToEdi(ISdmxObjects objects, Stream output);

        /// <summary>
        /// Processes an EDI message and returns a workspace containing the SDMX structures and data that were contained in the message
        /// </summary>
        /// <param name="ediMessageLocation">
        /// The EDI message location.
        /// </param>
        /// <returns>
        /// The <see cref="IEdiWorkspace"/>.
        /// </returns>
        IEdiWorkspace ParseEdiMessage(IReadableDataLocation ediMessageLocation);
    }
}
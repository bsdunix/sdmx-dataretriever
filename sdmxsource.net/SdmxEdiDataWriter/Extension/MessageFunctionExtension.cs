﻿// -----------------------------------------------------------------------
// <copyright file="MessageFunctionExtension.cs" company="EUROSTAT">
//   Date Created : 2014-07-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Extension
{
    using System;
    using System.Text;

    using Org.Sdmxsource.Sdmx.EdiParser.Constants;

    /// <summary>
    /// The message function extension.
    /// </summary>
    public static class MessageFunctionExtension
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets the EDI string.
        /// </summary>
        /// <param name="messageFunction">
        /// The message function.
        /// </param>
        /// <returns>
        /// The EDI string
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// messageFunction;Not supported value.
        /// </exception>
        public static string GetEdiString(this MessageFunction messageFunction)
        {
            switch (messageFunction)
            {
                case MessageFunction.Null:
                    return null;
                case MessageFunction.StatisticalDefinitions:
                case MessageFunction.StatisticalData:
                    return messageFunction.ToString("D");
                case MessageFunction.DataSetList:
                    return "DSL";
                default:
                    throw new ArgumentOutOfRangeException("messageFunction", messageFunction, "Not supported value.");
            }
        }

        /// <summary>
        /// Gets from EDI string.
        /// </summary>
        /// <param name="ediStr">
        /// The EDI string.
        /// </param>
        /// <exception cref="System.ArgumentException">
        /// Unknown Message Function
        /// </exception>
        /// <returns>
        /// The <see cref="MessageFunction"/>.
        /// </returns>
        public static MessageFunction GetFromEdiStr(string ediStr)
        {
            Array currentMfs = Enum.GetValues(typeof(MessageFunction));
            foreach (MessageFunction currentMf in currentMfs)
            {
                if (currentMf != MessageFunction.Null)
                {
                    if (currentMf.GetEdiString().Equals(ediStr))
                    {
                        return currentMf;
                    }
                }
            }

            var sb = new StringBuilder();
            string concat = string.Empty;
            foreach (MessageFunction currentMf in currentMfs)
            {
                sb.Append(concat);
                sb.Append(currentMf.GetEdiString());
                concat = ", ";
            }

            throw new ArgumentException("Unknown Message Function : " + ediStr + " (valid types are - " + sb + ")");
        }

        /// <summary>
        /// Determines whether the specified message function defines an Edi message that can contain data
        /// </summary>
        /// <param name="messageFunction">
        /// The message function.
        /// </param>
        /// <returns>
        /// Returns true if the message function defines an Edi message that can contain data.
        /// </returns>
        public static bool IsData(this MessageFunction messageFunction)
        {
            return messageFunction == MessageFunction.StatisticalData || messageFunction == MessageFunction.DataSetList;
        }

        /// <summary>
        /// Determines whether the specified message function is structure.
        /// </summary>
        /// <param name="messageFunction">
        /// The message function.
        /// </param>
        /// <returns>
        /// Returns true if the message function defines an Edi message that can contain structures.
        /// </returns>
        public static bool IsStructure(this MessageFunction messageFunction)
        {
            return messageFunction == MessageFunction.StatisticalDefinitions || messageFunction == MessageFunction.DataSetList;
        }

        #endregion
    }
}
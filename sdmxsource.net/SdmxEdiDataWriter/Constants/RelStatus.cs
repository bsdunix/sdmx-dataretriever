// -----------------------------------------------------------------------
// <copyright file="RelStatus.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxEdiDataWriter.Constants
{
    using System;

    /// <summary>
    ///     Current REL status
    /// </summary>
    [Flags]
    internal enum RelStatus
    {
        /// <summary>
        ///     The none.
        /// </summary>
        None = 0, 

        /// <summary>
        ///     The data set.
        /// </summary>
        DataSet = 1, 

        /// <summary>
        ///     The sibling.
        /// </summary>
        Sibling = 2, 

        /// <summary>
        ///     The series.
        /// </summary>
        Series = 4, 

        /// <summary>
        ///     The observation.
        /// </summary>
        Observation = 5
    }
}
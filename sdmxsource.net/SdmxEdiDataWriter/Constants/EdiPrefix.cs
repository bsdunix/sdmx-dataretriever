﻿// -----------------------------------------------------------------------
// <copyright file="EdiPrefix.cs" company="EUROSTAT">
//   Date Created : 2014-07-23
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Constants
{
    /// <summary>
    /// The EDI prefix.
    /// </summary>
    public enum EdiPrefix
    {
        /// <summary>
        /// The null
        /// </summary>
        Null = 0,

        /// <summary>
        /// The message start.
        /// </summary>
        MessageStart = 1, 

        /// <summary>
        /// The interchange header.
        /// </summary>
        InterchangeHeader, 

        /// <summary>
        /// The message identification.
        /// </summary>
        MessageIdentification, 

        /// <summary>
        /// The message function.
        /// </summary>
        MessageFunction, 

        /// <summary>
        /// The end message administration.
        /// </summary>
        EndMessageAdministration, 

        /// <summary>
        /// The end message.
        /// </summary>
        EndMessage, 

        /// <summary>
        /// The contact information.
        /// </summary>
        ContactInformation, 

        /// <summary>
        /// The communication number.
        /// </summary>
        CommunicationNumber, 

        /// <summary>
        /// The data start.
        /// </summary>
        DataStart, 

        /// <summary>
        /// The dataset action.
        /// </summary>
        DatasetAction, 

        /// <summary>
        /// The dataset date time.
        /// </summary>
        DatasetDatetime, 

        /// <summary>
        /// The dataset send method.
        /// </summary>
        DatasetSendMethod, 

        /// <summary>
        /// The dataset missing value Symbol.
        /// </summary>
        DatasetMissingValueSymbol, 

        /// <summary>
        /// The dataset data.
        /// </summary>
        DatasetData, 

        /// <summary>
        /// The dataset data attribute.
        /// </summary>
        DatasetDataAttribute, 

        /// <summary>
        /// The dataset footnote section.
        /// </summary>
        DatasetFootnoteSection, 

        /// <summary>
        /// The dataset attribute scope.
        /// </summary>
        DatasetAttributeScope, 

        /// <summary>
        /// The dataset attribute coded.
        /// </summary>
        DatasetAttributeCoded, 

        /// <summary>
        /// The message id provided by sender.
        /// </summary>
        MessageIdProvidedBySender, 

        /// <summary>
        /// The dataset attribute un-coded.
        /// </summary>
        DatasetAttributeUncoded, 

        /// <summary>
        /// The DSD reference.
        /// </summary>
        DsdReference, 

        /// <summary>
        /// The message agency.
        /// </summary>
        MessageAgency, 

        /// <summary>
        /// The receiving agency.
        /// </summary>
        ReceivingAgency, 

        /// <summary>
        /// The sending agency.
        /// </summary>
        SendingAgency, 

        /// <summary>
        /// The code list.
        /// </summary>
        Codelist, 

        /// <summary>
        /// The code value.
        /// </summary>
        CodeValue, 

        /// <summary>
        /// The DSD.
        /// </summary>
        Dsd, 

        /// <summary>
        /// The attribute.
        /// </summary>
        Attribute, 

        /// <summary>
        /// The dimension.
        /// </summary>
        Dimension, 

        /// <summary>
        /// The concept.
        /// </summary>
        Concept, 

        /// <summary>
        /// The string.
        /// </summary>
        String, 

        /// <summary>
        /// The field length.
        /// </summary>
        FieldLength, 

        /// <summary>
        /// The usage status.
        /// </summary>
        UseageStatus, 

        /// <summary>
        /// The attribute attachment value.
        /// </summary>
        AttributeAttachmentValue, 

        /// <summary>
        /// The code list reference.
        /// </summary>
        CodelistReference, 
    }
}
// -----------------------------------------------------------------------
// <copyright file="GesmesAttributeGroupReader.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxEdiDataWriter.Engine
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    using Estat.Sri.SdmxEdiDataWriter.Helper;

    /// <summary>
    ///     A binary reader for files written by <see cref="GesmesAttributeGroupWriter" />
    /// </summary>
    internal class GesmesAttributeGroupReader : BinaryReader
    {
        #region Fields

        /// <summary>
        ///     A string builder used as buffer
        /// </summary>
        private readonly StringBuilder _buffer = new StringBuilder();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GesmesAttributeGroupReader"/> class.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        public GesmesAttributeGroupReader(Stream input)
            : base(input, Encoding.UTF8)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Stream this instance as GESMES attributes to <paramref name="writer"/>
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        /// <param name="codedAttributes">
        /// The coded attributes set.
        /// </param>
        /// <param name="count">
        /// The number of attribute groups
        /// </param>
        /// <returns>
        /// The number of segments written
        /// </returns>
        public int StreamToGesmes(TextWriter writer, IDictionary<string, object> codedAttributes, int count)
        {
            // TODO convert codedAttributes to Set<string> when we target 3.5 or later
            int segmentCount = 0;
            while (count > 0)
            {
                count--;
                int dimensionCount = this.ReadInt32();
                var dimensionValues = new string[dimensionCount];
                for (int i = 0; i < dimensionValues.Length; i++)
                {
                    dimensionValues[i] = this.ReadString();
                }

                int attributeCount = this.ReadInt32();
                if (attributeCount > 0)
                {
                    this._buffer.Length = 0;
                    writer.Write(GesmesHelper.ArrSegment(this._buffer, dimensionCount, dimensionValues));
                    segmentCount++;

                    for (int i = 0; i < attributeCount; i++)
                    {
                        this._buffer.Length = 0;
                        string name = this.ReadString();
                        string value = this.ReadString();

                        segmentCount += codedAttributes.ContainsKey(name)
                                            ? GesmesHelper.CodedAttribute(this._buffer, name, value)
                                            : GesmesHelper.UncodedAttribute(this._buffer, name, value);

                        writer.Write(this._buffer);
                    }
                }
            }

            return segmentCount;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="GesmesAttributeGroupWriter.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxEdiDataWriter.Engine
{
    using System.IO;
    using System.Text;

    using Estat.Sri.SdmxEdiDataWriter.Model;

    /// <summary>
    ///     A binary writer for <see cref="GesmesAttributeGroup" />
    /// </summary>
    internal class GesmesAttributeGroupWriter : BinaryWriter
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GesmesAttributeGroupWriter"/> class.
        /// </summary>
        /// <param name="output">
        /// The output.
        /// </param>
        public GesmesAttributeGroupWriter(Stream output)
            : base(output, Encoding.UTF8)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Write the specified <paramref name="gesmesAttributeGroup"/> to output
        /// </summary>
        /// <param name="gesmesAttributeGroup">
        /// The GESMES attribute group that contains a single ARR and all of it's attributes
        /// </param>
        public void Write(GesmesAttributeGroup gesmesAttributeGroup)
        {
            int length = gesmesAttributeGroup.DimensionValues.Length;
            this.Write(length);
            for (int i = 0; i < length; i++)
            {
                this.Write(gesmesAttributeGroup.DimensionValues[i]);
            }

            this.Write(gesmesAttributeGroup.AttributeValues.Count);
            for (int i = 0; i < gesmesAttributeGroup.AttributeValues.Count; i++)
            {
                this.Write(gesmesAttributeGroup.AttributeValues[i].Key);
                this.Write(gesmesAttributeGroup.AttributeValues[i].Value ?? string.Empty);
            }
        }

        #endregion
    }
}
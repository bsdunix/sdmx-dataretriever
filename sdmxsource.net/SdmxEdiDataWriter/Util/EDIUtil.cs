﻿// -----------------------------------------------------------------------
// <copyright file="EDIUtil.cs" company="EUROSTAT">
//   Date Created : 2014-07-22
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Util
{
    using System;
    using System.Globalization;
    using System.Text;
    using System.Text.RegularExpressions;

    using Estat.Sri.SdmxEdiDataWriter.Helper;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.EdiParser.Constants;
    using Org.Sdmxsource.Sdmx.EdiParser.Extension;
    using Org.Sdmxsource.Sdmx.EdiParser.Model.Reader;

    /// <summary>
    /// EDI related utilities.
    /// </summary>
    /// <seealso cref="GesmesHelper"/>
    public static class EDIUtil
    {
        /// <summary>
        /// The _split on plus
        /// </summary>
        private static readonly Regex _splitOnPlus = new Regex("(?<!\\?)\\+", RegexOptions.Compiled);

        /// <summary>
        /// The _split on colon
        /// </summary>
        private static readonly Regex _splitOnColon = new Regex("(?<!\\?):", RegexOptions.Compiled);

        /// <summary>
        /// The sibling group identifier
        /// </summary>
        private static string _siblingGroupId = "Sibling";

        /// <summary>
        /// Gets or sets the sibling group identifier.
        /// </summary>
        /// <value>
        /// The sibling group identifier.
        /// </value>
        public static string SiblingGroupId
        {
            get
            {
                return _siblingGroupId;
            }

            set
            {
                _siblingGroupId = value;
            }
        }

        /// <summary>
        /// Checks the prefix is as expected
        /// <p />
        /// If the prefix is not as expected and <paramref name="errorOnFail"/> is set to true, an exception will be thrown
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="errorOnFail">if set to <c>true</c> will throw an exception on fail.</param>
        /// <returns>true if it the prefix is as expected; otherwise false.</returns>
        /// <exception cref="System.ArgumentException">Expecting prefix : ' <c> prefix.GetPrefix() </c> ' but got ' <c> dataReader.LineType </c> '</exception>
        public static bool AssertPrefix(IEdiReader dataReader, EdiPrefix prefix, bool errorOnFail)
        {
            if (dataReader.LineType != prefix)
            {
                if (errorOnFail)
                {
                    throw new ArgumentException("Expecting prefix : '" + prefix.GetPrefix() + "' but got '" + dataReader.LineType + "'");
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// Splits the specified <paramref name="input"/> on plus character.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>An array split with plus character.</returns>
        public static string[] SplitOnPlus(string input)
        {
            return SplitOnPlus(input, -1);
        }

        /// <summary>
        /// Splits the specified <paramref name="input" /> on plus character.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="expectedNumberOfSplits">The expected number of splits. Set to -1 to disable.</param>
        /// <returns>
        /// An array split with plus character.
        /// </returns>
        public static string[] SplitOnPlus(string input, int expectedNumberOfSplits)
        {
            return SplitOnChar(input, _splitOnPlus, "+", expectedNumberOfSplits);
        }

        /// <summary>
        /// Splits the specified <paramref name="input"/> on colon character.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>An array split with colon character.</returns>
        public static string[] SplitOnColon(string input)
        {
            return SplitOnColon(input, -1);
        }

        /// <summary>
        /// Splits the specified <paramref name="input" /> on colon character.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="expectedNumberOfSplits">The expected number of splits. Set to -1 to disable.</param>
        /// <returns>
        /// An array split with colon character.
        /// </returns>
        public static string[] SplitOnColon(string input, int expectedNumberOfSplits)
        {
            return SplitOnChar(input, _splitOnColon, ":", expectedNumberOfSplits);
        }

        /// <summary>
        /// Takes an Edi string, removes all Edi escape characters
        /// </summary>
        /// <param name="inputString">The input string.</param>
        /// <returns>THe <paramref name="inputString"/> without the escape character.</returns>
        public static string EdiToString(string inputString)
        {
            inputString = Regex.Replace(inputString, "(?<!\\?):", string.Empty);
            inputString = inputString.Replace("?:", EdiConstants.Colon);
            inputString = inputString.Replace("?'", EdiConstants.EndTag);
            inputString = inputString.Replace("?+", EdiConstants.Plus);
            inputString = inputString.Replace("??", EdiConstants.EscapeChar);
            return inputString;
        }

        /// <summary>
        /// Parses the string as <see cref="int"/>.
        /// </summary>
        /// <param name="s">The input string.</param>
        /// <returns>An <see cref="int"/>.from the specified <paramref name="s"/></returns>
        public static int ParseStringAsInt(string s)
        {
            // NOte in .NET all exceptions are unchecked.
            return int.Parse(s, CultureInfo.InvariantCulture);
        }


        /// <summary>
        /// Checks if <paramref name="first"/> and <paramref name="second"/>  without checking the version.
        /// </summary>
        /// <param name="first">The first <see cref="IMaintainableRefObject"/>.</param>
        /// <param name="second">The second <see cref="IMaintainableRefObject"/>.</param>
        /// <returns><c>true</c> if <paramref name="first"/> and <paramref name="second"/> have the same MaintainableId and AgencyId, <c>false</c> otherwise.</returns>
        public static bool CheckEqualsWithNoVersion(IMaintainableRefObject first, IMaintainableRefObject second)
        {
            if (first == null && second == null)
            {
                return true;
            } 
            
            if (first == null || second == null)
            {
                return false;
            }

            if (first.HasMaintainableId() && second.HasMaintainableId())
            {
                if (!first.MaintainableId.Equals(second.MaintainableId))
                {
                    return false;
                }
            } 
            else if (first.HasMaintainableId() || second.HasMaintainableId())
            {
                return false;
            }

            if (first.HasAgencyId() && second.HasAgencyId())
            {
                var firstAgency = NormalizeAgencyId(first);
                var secondAgency = NormalizeAgencyId(second);
                if (!firstAgency.Equals(secondAgency))
                {

                    return false;
                }
            }
            else if (first.HasAgencyId() || second.HasAgencyId())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Normalizes the ESTAT agency identifier.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The normalized agency id.</returns>
        private static string NormalizeAgencyId(IMaintainableRefObject maintainableRef)
        {
            if (EdiConstants.SdmxEstatAgency.Equals(maintainableRef.AgencyId))
            {
                return EdiConstants.GesmesEstatAgency;
            }

            return maintainableRef.AgencyId;
        }

        /// <summary>
        /// Splits the specified <paramref name="input" /> with the specified <paramref name="expression" />
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="character">The character.</param>
        /// <param name="expectedNumberOfSplits">The expected number of splits. Set to -1 to disable.</param>
        /// <returns>
        /// An array split with colon character.
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Unexpected number of characters, expecting <paramref name="expectedNumberOfSplits"/> actual '<c>splitOnPlus.Length</c>'</exception>
        private static string[] SplitOnChar(string input, Regex expression, string character, int expectedNumberOfSplits)
        {
            var splitOnPlus = expression.Split(input);
            if (expectedNumberOfSplits > 0)
            {
                if (splitOnPlus.Length != expectedNumberOfSplits)
                {
                    throw new SdmxSemmanticException("Unexpected number of '" + character + "' characters, expecting " + expectedNumberOfSplits + " actual '" + splitOnPlus.Length + "'");
                }
            }

            return splitOnPlus;
        }
    }
}
// -----------------------------------------------------------------------
// <copyright file="IEdiDataReader.cs" company="EUROSTAT">
//   Date Created : 2014-07-22
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Model.Reader
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;

    /// <summary>
    ///     The EdiDataReader interface.
    /// </summary>
    public interface IEdiDataReader : IEdiAbstractPositionalReader 
    {
        #region Public Properties

        /// <summary>
        ///     Gets the data set header for the data that this reader is reading.
        /// </summary>
        /// <value>
        ///     The data set header object.
        /// </value>
        IDatasetHeader DataSetHeaderObject { get; }

        /// <summary>
        ///     Gets the dataset attributes.
        /// </summary>
        /// <value>
        ///     The dataset attributes.
        /// </value>
        IList<IKeyValue> DatasetAttributes { get; }

        /// <summary>
        ///     Gets the missing value.
        /// </summary>
        /// <value>
        ///     The missing value.
        /// </value>
        string MissingValue { get; }

        #endregion
    }
}
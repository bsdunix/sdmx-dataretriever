// -----------------------------------------------------------------------
// <copyright file="EdiDataReader.cs" company="EUROSTAT">
//   Date Created : 2014-07-28
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Model.Reader
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.EdiParser.Model.Document;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     The EDI data reader.
    /// </summary>
    public class EdiDataReader : EdiAbstractPositionalReader, IEdiDataReader
    {
        #region Fields

        /// <summary>
        ///     The document position.
        /// </summary>
        private readonly IEdiDocumentPosition _documentPosition;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EdiDataReader"/> class.
        /// </summary>
        /// <param name="dataFile">
        /// The data file.
        /// </param>
        /// <param name="documentPosition">
        /// The document position.
        /// </param>
        /// <param name="ediMetadata">
        /// The EDI metadata.
        /// </param>
        public EdiDataReader(IReadableDataLocation dataFile, IEdiDocumentPosition documentPosition, IEdiMetadata ediMetadata)
            : base(dataFile, documentPosition, ediMetadata)
        {
            this._documentPosition = documentPosition;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the data set header for the data that this reader is reading.
        /// </summary>
        /// <value>
        ///     The data set header object.
        /// </value>
        public IDatasetHeader DataSetHeaderObject
        {
            get
            {
                string datasetId = this._documentPosition.DatasetId;
                DatasetAction datasetAction = this._documentPosition.DatasetAction;
                string dsdId = this._documentPosition.DataStructureIdentifier;

                IStructureReference dsdRef = new StructureReferenceImpl(this.MessageAgency, dsdId, MaintainableObject.DefaultVersion, SdmxStructureEnumType.Dsd);

                IDatasetStructureReference structureReference = new DatasetStructureReferenceCore(dsdRef);

                return new DatasetHeaderCore(datasetId, datasetAction, structureReference);
            }
        }

        /// <summary>
        ///     Gets the dataset attributes.
        /// </summary>
        /// <value>
        ///     The dataset attributes.
        /// </value>
        public IList<IKeyValue> DatasetAttributes
        {
            get
            {
                return this._documentPosition.DatasetAttributes;
            }
        }

        /// <summary>
        ///     Gets the missing value.
        /// </summary>
        /// <value>
        ///     The missing value.
        /// </value>
        public string MissingValue
        {
            get
            {
                return this._documentPosition.MissingValue;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only
        ///     unmanaged resources.
        /// </param>
        protected override void Dispose(bool managed)
        {
            if (managed)
            {
                this.DataFile.Close();
            }

            base.Dispose(managed);
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="IEdiAbstractPositionalReader.cs" company="EUROSTAT">
//   Date Created : 2014-07-23
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Model.Reader
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.EdiParser.Model.Document;

    /// <summary>
    ///     The EdiAbstractPositionalReader interface.
    /// </summary>
    public interface IEdiAbstractPositionalReader : IEdiReader
    {
        #region Public Properties

        /// <summary>
        ///     Gets the metadata for the EDI document that this EDI data segment came from
        /// </summary>
        IEdiMetadata EdiDocumentMetadata { get; }

        /// <summary>
        ///     Gets the agency that maintains the structures defined by the data
        /// </summary>
        string MessageAgency { get; }

        /// <summary>
        ///     Gets the preparation date of this EDI data segment
        /// </summary>
        DateTime PreparationDate { get; }

        /// <summary>
        ///     Gets the receiving Agency
        /// </summary>
        string RecievingAgency { get; }

        /// <summary>
        ///     Gets the sending Agency
        /// </summary>
        IParty SendingAgency { get; }

        #endregion
    }
}
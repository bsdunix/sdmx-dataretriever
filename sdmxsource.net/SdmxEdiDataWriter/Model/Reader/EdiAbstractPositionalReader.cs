// -----------------------------------------------------------------------
// <copyright file="EdiAbstractPositionalReader.cs" company="EUROSTAT">
//   Date Created : 2014-07-28
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Model.Reader
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.EdiParser.Model.Document;

    /// <summary>
    ///     The EDI abstract positional reader.
    /// </summary>
    public class EdiAbstractPositionalReader : EdiReader, IEdiAbstractPositionalReader
    {
        #region Fields

        /// <summary>
        /// The document position.
        /// </summary>
        private readonly IEdiDocumentPosition _documentPosition;

        /// <summary>
        /// The edi metadata.
        /// </summary>
        private readonly IEdiMetadata _ediMetadata;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EdiAbstractPositionalReader"/> class.
        /// </summary>
        /// <param name="dataFile">
        /// The data file.
        /// </param>
        /// <param name="documentPosition">
        /// The document position.
        /// </param>
        /// <param name="ediMetadata">
        /// The edi metadata.
        /// </param>
        public EdiAbstractPositionalReader(IReadableDataLocation dataFile, IEdiDocumentPosition documentPosition, IEdiMetadata ediMetadata)
            : base(dataFile)
        {
            this._ediMetadata = ediMetadata;
            this._documentPosition = documentPosition;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the metadata for the EDI document that this EDI data segment came from
        /// </summary>
        public IEdiMetadata EdiDocumentMetadata
        {
            get
            {
                return this._ediMetadata;
            }
        }

        /// <summary>
        ///     Gets the agency that maintains the structures defined by the data
        /// </summary>
        public string MessageAgency
        {
            get
            {
                return this._documentPosition.MessageAgency;
            }
        }

        /// <summary>
        ///     Gets the preparation date of this EDI data segment
        /// </summary>
        public DateTime PreparationDate
        {
            get
            {
                return this._documentPosition.PreparationDate;
            }
        }

        /// <summary>
        ///     Gets the receiving Agency
        /// </summary>
        public string RecievingAgency
        {
            get
            {
                return this._documentPosition.ReceivingAgency;
            }
        }

        /// <summary>
        ///     Gets the sending Agency
        /// </summary>
        public IParty SendingAgency
        {
            get
            {
                return this._documentPosition.SendingAgency;
            }
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="EdiDataDocument.cs" company="EUROSTAT">
//   Date Created : 2014-07-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Model.Document
{
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.EdiParser.Model.Reader;

    /// <summary>
    /// The edi data document.
    /// </summary>
    public class EdiDataDocument : IEdiDataDocument
    {
        #region Fields

        /// <summary>
        /// The data location.
        /// </summary>
        private readonly IReadableDataLocation _dataLocation;

        /// <summary>
        /// The document position.
        /// </summary>
        private readonly IEdiDocumentPosition _documentPosition;

        /// <summary>
        /// The edi metadata.
        /// </summary>
        private readonly IEdiMetadata _ediMetadata;

        /// <summary>
        /// The data reader.
        /// </summary>
        private IEdiDataReader _dataReader;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EdiDataDocument"/> class.
        /// </summary>
        /// <param name="documentUri">
        /// The document URI.
        /// </param>
        /// <param name="documentPosition">
        /// The document position.
        /// </param>
        /// <param name="ediMetadata">
        /// The EDI metadata.
        /// </param>
        public EdiDataDocument(IReadableDataLocation documentUri, IEdiDocumentPosition documentPosition, IEdiMetadata ediMetadata)
        {
            this._dataLocation = documentUri;
            this._documentPosition = documentPosition;
            this._ediMetadata = ediMetadata;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the data reader.
        /// </summary>
        /// <value>
        ///     The data reader.
        /// </value>
        public IEdiDataReader DataReader
        {
            get
            {
                // There should only ever be a single DataReader which is used by all calling instances.  
                if (this._dataReader == null)
                {
                    this._dataReader = new EdiDataReader(this._dataLocation, this._documentPosition, this._ediMetadata);
                }

                return this._dataReader;
            }
        }

        #endregion
    }
}
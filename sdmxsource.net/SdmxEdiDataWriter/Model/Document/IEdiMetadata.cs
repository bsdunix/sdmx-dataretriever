// -----------------------------------------------------------------------
// <copyright file="IEdiMetadata.cs" company="EUROSTAT">
//   Date Created : 2014-07-23
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Model.Document
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The EdiMetadata interface.
    /// </summary>
    public interface IEdiMetadata
    {
        #region Public Properties

        /// <summary>
        ///     Gets the application reference.
        /// </summary>
        /// <value>
        ///     The application reference.
        /// </value>
        string ApplicationReference { get; }

        /// <summary>
        ///     Gets the date of preparation.
        /// </summary>
        /// <value>
        ///     The date of preparation.
        /// </value>
        DateTime DateOfPreparation { get; }

        /// <summary>
        ///     Gets the index of the document.
        /// </summary>
        /// <value>
        ///     The index of the document.
        /// </value>
        IList<IEdiDocumentPosition> DocumentIndex { get; }

        /// <summary>
        ///     Gets the interchange reference.
        /// </summary>
        /// <value>
        ///     The interchange reference.
        /// </value>
        string InterchangeReference { get; }

        /// <summary>
        ///     Gets a value indicating whether this instance is test.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is test; otherwise, <c>false</c>.
        /// </value>
        bool IsTest { get; }

        /// <summary>
        ///     Gets the receiver identification.
        /// </summary>
        /// <value>
        ///     The receiver identification.
        /// </value>
        string ReceiverIdentification { get; }

        /// <summary>
        ///     Gets or sets the reporting begin.
        /// </summary>
        /// <value>
        ///     The reporting begin.
        /// </value>
        DateTime ReportingBegin { get; set; }

        /// <summary>
        ///     Gets or sets the reporting end.
        /// </summary>
        /// <value>
        ///     The reporting end.
        /// </value>
        DateTime ReportingEnd { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Adds the index of the document.
        /// </summary>
        /// <param name="pos">
        /// The position.
        /// </param>
        void AddDocumentIndex(IEdiDocumentPosition pos);

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="IEdiWorkspace.cs" company="EUROSTAT">
//   Date Created : 2014-05-16
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxEdiParser.
// 
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Model
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.EdiParser.Model.Document;

    /// <summary>
    /// The EdiWorkspace interface.
    /// </summary>
    public interface IEdiWorkspace : IDisposable
    {
        #region Public Properties

        /// <summary>
        ///     Gets the dataset headers.
        /// </summary>
        /// <value>
        ///     The dataset headers.
        /// </value>
        IList<IDatasetHeader> DatasetHeaders { get; }

        /// <summary>
        ///     Gets a value indicating whether any <see cref="IEdiDataDocument"/> exist in this workspace.
        /// </summary>
        /// <value>
        ///     <c>true</c> if [has data]; otherwise, <c>false</c>.
        /// </value>
        bool HasData { get; }

        /// <summary>
        ///     Gets a value indicating whether any <see cref="ISdmxObjects"/> exist in this workspace.
        /// </summary>
        /// <value>
        ///     <c>true</c> if [has structures]; otherwise, <c>false</c>.
        /// </value>
        bool HasStructures { get; }

        /// <summary>
        ///     Gets the header.
        /// </summary>
        /// <value>
        ///     The header.
        /// </value>
        IHeader Header { get; }

        /// <summary>
        ///     Gets the merged objects.
        /// </summary>
        /// <value>
        ///     The merged objects.
        /// </value>
        ISdmxObjects MergedObjects { get; }

        /// <summary>
        ///     Gets the objects.
        /// </summary>
        /// <value>
        ///     The objects.
        /// </value>
        IList<ISdmxObjects> Objects { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the data reader.
        /// </summary>
        /// <param name="retrievalManager">
        /// The retrieval manager.
        /// </param>
        /// <returns>
        /// The <see cref="IDataReaderEngine"/> if this <see cref="HasData"/> is <c>True</c>; otherwise null.
        /// </returns>
        IDataReaderEngine GetDataReader(ISdmxObjectRetrievalManager retrievalManager);

        /// <summary>
        /// Gets the data reader.
        /// </summary>
        /// <param name="keyFamily">
        /// The key family.
        /// </param>
        /// <param name="dataflow">
        /// The dataflow (Optional).
        /// </param>
        /// <returns>
        /// The <see cref="IDataReaderEngine"/> if this <see cref="HasData"/> is <c>True</c>; otherwise null.
        /// </returns>
        IDataReaderEngine GetDataReader(IDataStructureObject keyFamily, IDataflowObject dataflow);

        #endregion
    }
}
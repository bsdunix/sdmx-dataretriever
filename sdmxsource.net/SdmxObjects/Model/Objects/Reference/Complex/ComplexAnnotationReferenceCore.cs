﻿// -----------------------------------------------------------------------
// <copyright file="ComplexAnnotationReferenceCore.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    #endregion

    /// <summary>
    ///   The complex annotation reference.
    /// </summary>
    [Serializable]
    public class ComplexAnnotationReferenceCore : IComplexAnnotationReference
    {
        #region Fields

        /// <summary>
        ///   The type ref.
        /// </summary>
        private IComplexTextReference _typeRef;

        /// <summary>
        ///   The title ref.
        /// </summary>
        private IComplexTextReference _titleRef;

        /// <summary>
        ///   The text ref.
        /// </summary>
        private IComplexTextReference _textRef;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ComplexAnnotationReferenceCore"/> class.
        /// </summary>
        /// <param name="typeRef">
        /// The type ref. 
        /// </param>
        /// <param name="titleRef">
        /// The title ref. 
        /// </param>
        /// <param name="textRef">
        /// The text ref. 
        /// </param>
        public ComplexAnnotationReferenceCore(IComplexTextReference typeRef, IComplexTextReference titleRef, IComplexTextReference textRef)
        {
            this._typeRef = typeRef;
            this._titleRef = titleRef;
            this._textRef = textRef;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the type reference.
        /// </summary>
        public virtual IComplexTextReference TypeReference
        {
             get
             {
                 return _typeRef;
             }
        }

        /// <summary>
        ///   Gets the title reference.
        /// </summary>
        public virtual IComplexTextReference TitleReference
        {
            get
            {
                return _titleRef;
            }
        }

        /// <summary>
        ///   Gets the text reference.
        /// </summary>
        public virtual IComplexTextReference TextReference
        {
           get
           {
               return _textRef;
           }
        }

        #endregion

    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TimeRangeCore.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    #endregion

    /// <summary>
    ///   The time range core.
    /// </summary>
    [Serializable]
    public class TimeRangeCore : ITimeRange
    {
        #region Fields

        /// <summary>
        ///   The range.
        /// </summary>
        private bool _range;

        /// <summary>
        ///   The start date.
        /// </summary>
        private ISdmxDate _startDate;

        /// <summary>
        ///   The end date.
        /// </summary>
	   private ISdmxDate _endDate;

       /// <summary>
       ///   The start inclusive.
       /// </summary>
	   private bool _startInclusive = true;

       /// <summary>
       ///   The end inclusive.
       /// </summary>
	   private bool _endInclusive = true;

       #endregion

       #region Constructors and Destructors


       /// <summary>
       /// Initializes a new instance of the <see cref="ComplexAnnotationReferenceCore"/> class.
       /// </summary>
       /// <param name="range">
       /// The range. 
       /// </param>
       /// <param name="startDate">
       /// The start date. 
       /// </param>
       /// <param name="endDate">
       /// The end date. 
       /// </param>
       /// <param name="startInclusive">
       /// The start inclusive. 
       /// </param>
       /// <param name="endInclusive">
       /// The end inclusive. 
       /// </param>
        public TimeRangeCore(bool range, ISdmxDate startDate, ISdmxDate endDate, bool startInclusive, bool endInclusive) 
        {
	  	    this._range = range;
		    this._startDate = startDate;
		    this._endDate = endDate;
		    this._startInclusive = startInclusive;
		    this._endInclusive = endInclusive;
		 
		    if (startDate == null && endDate == null)
            {
			   throw new SdmxSemmanticException("When setting range, cannot have both start/end periods null.");
		    }

		   if (this._range)
           {
			 if (startDate == null || endDate == null)
             {
				throw new SdmxSemmanticException("When range is defined then both start/end periods should be set.");
			 }
		   } 
           else
           {
			  if (startDate != null && endDate != null) 
              {
				throw new SdmxSemmanticException("When it is not a range then not both start/end periods can be set.");
			  }
		   }
  	    }

       #endregion

       #region Public Properties

        /// <summary>
        ///   Gets the range.
        /// </summary>
       public virtual bool IsRange
       {
		  get
		  {
		    return _range;
		  }
       }

       /// <summary>
       ///   Gets the start date.
       /// </summary>
      public virtual ISdmxDate StartDate
      {
		 get
		 {
		    return _startDate;
		 }
      }

      /// <summary>
      ///   Gets the end date.
      /// </summary>
	  public virtual ISdmxDate EndDate
      {
		 get
		 {
		    return _endDate;
		 }
      }

      /// <summary>
      ///   Gets the is start inclusive.
      /// </summary>
	  public virtual bool IsStartInclusive
      {
         get
         {
            return _startInclusive;
         }
      }

      /// <summary>
      ///   Gets the is end inclusive.
      /// </summary>
       public virtual bool IsEndInclusive
       {
		  get
		  {
		    return _endInclusive;
		  }
       }

        #endregion

    }
}

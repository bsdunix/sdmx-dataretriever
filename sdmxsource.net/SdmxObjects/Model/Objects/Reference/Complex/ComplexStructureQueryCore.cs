﻿// -----------------------------------------------------------------------
// <copyright file="ComplexStructureQueryCore.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    #endregion


    /// <summary>
    ///   The complex structure query.
    /// </summary>
    [Serializable]
    public class ComplexStructureQueryCore : IComplexStructureQuery
    {

        #region Fields

        /// <summary>
        ///   The structure ref.
        /// </summary>
        private IComplexStructureReferenceObject _structuRef;

        /// <summary>
        ///   The query metadata.
        /// </summary>
	    private IComplexStructureQueryMetadata _queryMetadata;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ComplexStructureQueryCore"/> class.
        /// </summary>
        /// <param name="structureRef">
        /// The structure ref. 
        /// </param>
        /// <param name="queryMetadata">
        /// The query metadata. 
        /// </param>
        public ComplexStructureQueryCore(IComplexStructureReferenceObject structureRef, IComplexStructureQueryMetadata queryMetadata)
        {
		   if (structureRef == null)
           {
			   throw new SdmxSemmanticException("StructureRefernce cannot be null");
		   }
		   this._structuRef = structureRef;
		
		   if (queryMetadata == null)
           {
		      throw new SdmxSemmanticException("StructureQueryMetadata cannot be null");
		   }
		   this._queryMetadata = queryMetadata;
	    }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the complex structure query metadata.
        /// </summary>
        public virtual IComplexStructureQueryMetadata StructureQueryMetadata
        {
           get
           {
               return _queryMetadata;
           }
        }


        /// <summary>
        ///   Gets the complex structure reference object.
        /// </summary>
	    public virtual IComplexStructureReferenceObject StructureReference
        {
           get
           {
               return _structuRef;
           }
        }

        #endregion
    }
}

// -----------------------------------------------------------------------
// <copyright file="ItemMapCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///   The item map core.
    /// </summary>
    [Serializable]
    public class ItemMapCore : SdmxStructureCore, IItemMap
    {
        #region Fields

        /// <summary>
        ///   The source id.
        /// </summary>
        private string sourceId;

        /// <summary>
        ///   The target id.
        /// </summary>
        private string targetId;

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemMapCore"/> class.
        /// </summary>
        /// <param name="itemMapMutableObject">
        /// The agencyScheme. 
        /// </param>
        /// <param name="parent">
        /// The parent. 
        /// </param>
        public ItemMapCore(IItemMapMutableObject itemMapMutableObject, ISdmxStructure parent)
            : base(itemMapMutableObject, parent)
        {
            this.sourceId = itemMapMutableObject.SourceId;
            this.targetId = itemMapMutableObject.TargetId;
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemMapCore"/> class.
        /// </summary>
        /// <param name="id">
        /// The id. 
        /// </param>
        /// <param name="target">
        /// The target. 
        /// </param>
        /// <param name="parent">
        /// The parent. 
        /// </param>
        public ItemMapCore(string id, string target, ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ItemMap), parent)
        {
            this.sourceId = id;
            this.targetId = target;
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemMapCore"/> class.
        /// </summary>
        /// <param name="alias">
        /// The alias. 
        /// </param>
        /// <param name="id">
        /// The id. 
        /// </param>
        /// <param name="target">
        /// The target. 
        /// </param>
        /// <param name="parent">
        /// The parent. 
        /// </param>
        public ItemMapCore(string alias, string id, string target, ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ItemMap), parent)
        {
            this.sourceId = id;
            this.targetId = target;
            this.Validate();
        }

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        #region Public Properties

        /// <summary>
        ///   Gets the source id.
        /// </summary>
        public virtual string SourceId
        {
            get
            {
                return this.sourceId;
            }
        }

        /// <summary>
        ///   Gets the target id.
        /// </summary>
        public virtual string TargetId
        {
            get
            {
                return this.targetId;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        /// The agencyScheme. 
        /// </param>
        /// <param name="includeFinalProperties"> </param>
        /// <returns>
        /// The <see cref="bool"/> . 
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null) return false;
            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IItemMap)sdmxObject;
                if (!ObjectUtil.Equivalent(this.sourceId, that.SourceId))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this.targetId, that.TargetId))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        #region Methods

        /// <summary>
        ///   The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        protected internal void Validate()
        {
            if (string.IsNullOrWhiteSpace(this.sourceId))
            {
                throw new SdmxSemmanticException(ExceptionCode.ObjectMissingRequiredElement, "ItemMap", "Source Id");
            }

            if (string.IsNullOrWhiteSpace(this.targetId))
            {
                throw new SdmxSemmanticException(ExceptionCode.ObjectMissingRequiredElement, "ItemMap", "Target Id");
            }

            this.sourceId = ValidationUtil.CleanAndValidateId(this.sourceId, true);
            this.targetId = ValidationUtil.CleanAndValidateId(this.targetId, true);
        }

        #endregion
    }
}
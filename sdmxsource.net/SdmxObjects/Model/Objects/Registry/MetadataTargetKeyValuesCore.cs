﻿// -----------------------------------------------------------------------
// <copyright file="MetadataTargetKeyValuesCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    ///   TODO: Update summary.
    /// </summary>
    public class MetadataTargetKeyValuesCore : KeyValuesCore, IMetadataTargetKeyValues
    {
        private readonly IList<ICrossReference> _objectReferences;

        private readonly IList<IDataSetReference> _datasetReferences;

        public MetadataTargetKeyValuesCore(IMetadataTargetKeyValuesMutable mutable, IMetadataTargetRegion parent)
            : base(mutable, parent)
        {
            _objectReferences = new List<ICrossReference>();
            _datasetReferences = new List<IDataSetReference>();

            if (mutable.ObjectReferences != null)
            {
                foreach (IStructureReference sRef in mutable.ObjectReferences)
                {
                    this._objectReferences.Add(new CrossReferenceImpl(this, sRef));
                }
            }
            if (mutable.DatasetReferences != null)
            {
                foreach (IDataSetReferenceMutableObject currentRef in mutable.DatasetReferences)
                {
                    this._datasetReferences.Add(new DataSetReferenceCore(currentRef, this));
                }
            }
        }

        public MetadataTargetKeyValuesCore(ComponentValueSetType keyValueType, IMetadataTargetRegion parent)
            : base(keyValueType, parent)
        {
            _objectReferences = new List<ICrossReference>();
            _datasetReferences = new List<IDataSetReference>();

            if (keyValueType.DataSet != null)
            {
                foreach (SetReferenceType currentDatasetRef in keyValueType.DataSet)
                {
                    this._datasetReferences.Add(new DataSetReferenceCore(currentDatasetRef, this));
                }
            }
            if (keyValueType.Object != null)
            {
                foreach (ObjectReferenceType currentRef in keyValueType.Object)
                {
                    this._objectReferences.Add(RefUtil.CreateReference(this, currentRef));
                }
            }
        }

        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }
            var that = sdmxObject as IMetadataTargetKeyValues;
            if (that != null)
            {
                if (!ObjectUtil.EquivalentCollection(this._objectReferences, that.ObjectReferences))
                {
                    return false;
                }
                if (!base.Equivalent(this._datasetReferences, that.DatasetReferences, includeFinalProperties))
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        #region Implementation of IMetadataTargetKeyValues

        public IList<ICrossReference> ObjectReferences
        {
            get
            {
                return new List<ICrossReference>(_objectReferences);
            }
        }

        public IList<IDataSetReference> DatasetReferences
        {
            get
            {
                return new List<IDataSetReference>(_datasetReferences);
            }
        }

        #endregion

        #region Methods

       ///////////////////////////////////////////////////////////////////////////////////////////////////
	   ////////////COMPOSITES				 //////////////////////////////////////////////////
	   ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// The get composites internal.
        /// </summary>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
		    ISet<ISdmxObject> composites = base.GetCompositesInternal();
		    base.AddToCompositeSet(this._datasetReferences, composites);
		    return composites;
	    }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="DataSetReferenceCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    #region Using directives

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    ///   TODO: Update summary.
    /// </summary>
    public class DataSetReferenceCore : SdmxStructureCore, IDataSetReference
    {
        private readonly string _datasetId;

        private readonly ICrossReference _dataProviderReference;

        public DataSetReferenceCore(IDataSetReferenceMutableObject mutableObject, IMetadataTargetKeyValues parent)
            : base(mutableObject, parent)
        {
            this._datasetId = mutableObject.DatasetId;
            if (mutableObject.DataProviderReference != null)
            {
                this._dataProviderReference = new CrossReferenceImpl(this, mutableObject.DataProviderReference);
                try
                {
                    Validate();
                }
                catch (SdmxSemmanticException e)
                {
                    throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
                }
            }
        }

        public DataSetReferenceCore(SetReferenceType sRefType, IMetadataTargetKeyValues parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DatasetReference), parent)
        {
            this._datasetId = sRefType.ID;
            this._dataProviderReference = RefUtil.CreateReference(this, sRefType.DataProvider);
            try
            {
                Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        private void Validate()
        {
            if (!ObjectUtil.ValidString(_datasetId))
            {
                throw new SdmxSemmanticException("Dataset Reference missing mandatory 'id' identifier");
            }
            if (_dataProviderReference == null)
            {
                throw new SdmxSemmanticException("Dataset Reference missing mandatory 'data provider reference'");
            }
        }

        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }
            var that = sdmxObject as IDataSetReference;
            if (that != null)
            {
                if (!ObjectUtil.Equivalent(this.DataProviderReference, that.DataProviderReference))
                {
                    return false;
                }
                if (!ObjectUtil.Equivalent(this.DatasetId, that.DatasetId))
                {
                    return false;
                }
            }
            return false;
        }

        #region Implementation of IDataSetReference

        public string DatasetId
        {
            get
            {
                return _datasetId;
            }
        }

        public ICrossReference DataProviderReference
        {
            get
            {
                return _dataProviderReference;
            }
        }

        #endregion
    }
}

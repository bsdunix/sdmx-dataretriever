// -----------------------------------------------------------------------
// <copyright file="ErrorListCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///   The error list core.
    /// </summary>
    public class ErrorListCore : IErrorList
    {
        #region Fields

        /// <summary>
        ///   The _error messages.
        /// </summary>
        private readonly IList<string> _errorMessages;

        /// <summary>
        ///   The _is warning.
        /// </summary>
        private readonly bool _isWarning;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorListCore"/> class.
        /// </summary>
        /// <param name="errorMessages">
        /// The error messages. 
        /// </param>
        /// <param name="isWarning">
        /// The is warning. 
        /// </param>
        /// ///
        /// <exception cref="ArgumentException">
        /// Throws ArgumentException.
        /// </exception>
        public ErrorListCore(IList<string> errorMessages, bool isWarning)
        {
            if (!ObjectUtil.ValidCollection(errorMessages))
            {
                throw new ArgumentException("ErrorListCore requires error message to be provided");
            }

            this._errorMessages = errorMessages;
            this._isWarning = isWarning;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the error message.
        /// </summary>
        public virtual IList<string> ErrorMessage
        {
            get
            {
                return new List<string>(this._errorMessages);
            }
        }

        /// <summary>
        ///   Gets a value indicating whether warning.
        /// </summary>
        public virtual bool Warning
        {
            get
            {
                return this._isWarning;
            }
        }

        #endregion
    }
}
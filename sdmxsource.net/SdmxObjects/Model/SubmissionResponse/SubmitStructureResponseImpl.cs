// -----------------------------------------------------------------------
// <copyright file="SubmitStructureResponseImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SubmissionResponse
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SubmissionResponse;

    /// <summary>
    ///   The submit structure response impl.
    /// </summary>
    public class SubmitStructureResponseImpl : ISubmitStructureResponse
    {
        #region Fields

        /// <summary>
        ///   The _error list.
        /// </summary>
        private readonly IErrorList _errorList;

        /// <summary>
        ///   The _structure reference.
        /// </summary>
        private readonly IStructureReference _structureReference;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SubmitStructureResponseImpl"/> class.
        /// </summary>
        /// <param name="structureReference">
        /// The structure reference dataStructureObject. 
        /// </param>
        /// <param name="errorList">
        /// The error list. 
        /// </param>
        /// ///
        /// <exception cref="ArgumentException">
        /// Throws ArgumentException.
        /// </exception>
        public SubmitStructureResponseImpl(IStructureReference structureReference, IErrorList errorList)
        {
            this._structureReference = structureReference;
            this._errorList = errorList;
            if (structureReference != null && structureReference.TargetUrn == null)
            {
                throw new ArgumentException("SubmitStructureResponseImpl expects a complete IStructureReference");
            }

            if (!this.IsError)
            {
                if (structureReference == null)
                {
                    throw new ArgumentException("Sucessful SubmitStructureResponse expects a IStructureReference");
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets a value indicating whether error.
        /// </summary>
        public bool IsError
        {
            get
            {
                return this._errorList != null && !this._errorList.Warning;
            }
        }

        /// <summary>
        ///   Gets the error list.
        /// </summary>
        public virtual IErrorList ErrorList
        {
            get
            {
                return this._errorList;
            }
        }

        /// <summary>
        ///   Gets the structure reference.
        /// </summary>
        public virtual IStructureReference StructureReference
        {
            get
            {
                return this._structureReference;
            }
        }

        #endregion
    }
}
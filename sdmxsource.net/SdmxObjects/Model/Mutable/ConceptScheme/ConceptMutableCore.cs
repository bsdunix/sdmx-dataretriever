// -----------------------------------------------------------------------
// <copyright file="ConceptMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The concept mutable core.
    /// </summary>
    [Serializable]
    public class ConceptMutableCore : ItemMutableCore, IConceptMutableObject
    {
        #region Fields

        /// <summary>
        ///   The core representation.
        /// </summary>
        private IRepresentationMutableObject _coreRepresentation;

        /// <summary>
        ///   The iso concept reference.
        /// </summary>
        private IStructureReference _isoConceptReference;

        /// <summary>
        ///   The parent.
        /// </summary>
        private string _parent;

        /// <summary>
        ///   The parent agency.
        /// </summary>
        private string _parentAgency;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="ConceptMutableCore" /> class.
        /// </summary>
        public ConceptMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConceptMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The agencySchemeMutable target. 
        /// </param>
        public ConceptMutableCore(IConceptObject objTarget)
            : base(objTarget)
        {
            if (objTarget.CoreRepresentation != null)
            {
                this._coreRepresentation = new RepresentationMutableCore(objTarget.CoreRepresentation);
            }

            if (objTarget.IsoConceptReference != null)
            {
                this._isoConceptReference = objTarget.IsoConceptReference.CreateMutableInstance();
            }

            this._parent = objTarget.ParentConcept;
            this._parentAgency = objTarget.ParentAgency;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Creates a new instance with an id and an english name
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="name">
        /// The name. 
        /// </param>
         public static IConceptMutableObject GetInstance(string id, string name)
         {
		    
            IConceptMutableObject newInstance = new ConceptMutableCore();
		    newInstance.Id = id;
		    newInstance.AddName("en", name);
		    return newInstance;
	     }
	

        /// <summary>
        ///   Gets or sets the core representation.
        /// </summary>
        public virtual IRepresentationMutableObject CoreRepresentation
        {
            get
            {
                return this._coreRepresentation;
            }

            set
            {
                this._coreRepresentation = value;
            }
        }

        /// <summary>
        ///   Gets or sets the iso concept reference.
        /// </summary>
        public virtual IStructureReference IsoConceptReference
        {
            get
            {
                return this._isoConceptReference;
            }

            set
            {
                this._isoConceptReference = value;
            }
        }

        /// <summary>
        ///   Gets or sets the parent agency.
        /// </summary>
        public virtual string ParentAgency
        {
            get
            {
                return this._parentAgency;
            }

            set
            {
                this._parentAgency = value;
            }
        }

        /// <summary>
        ///   Gets or sets the parent concept.
        /// </summary>
        public virtual string ParentConcept
        {
            get
            {
                return this._parent;
            }

            set
            {
                this._parent = value;
            }
        }

        /// <summary>
        ///   Gets a value indicating whether stand alone concept.
        /// </summary>
        public virtual bool StandaloneConcept
        {
            get
            {
                return false;
            }
        }

        #endregion
    }
}
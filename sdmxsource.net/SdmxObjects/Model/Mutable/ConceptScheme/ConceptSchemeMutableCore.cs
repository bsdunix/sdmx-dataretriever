// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.ConceptScheme;

    /// <summary>
    ///   The concept scheme mutable core.
    /// </summary>
    [Serializable]
    public class ConceptSchemeMutableCore :
        ItemSchemeMutableCore<IConceptMutableObject, IConceptObject, IConceptSchemeObject>, 
        IConceptSchemeMutableObject
    {
        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="ConceptSchemeMutableCore" /> class.
        /// </summary>
        public ConceptSchemeMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConceptSchemeMutableCore"/> class.
        /// </summary>
        /// <param name="conceptSchemeObject">
        /// The conceptSchemeObject. 
        /// </param>
        public ConceptSchemeMutableCore(IConceptSchemeObject conceptSchemeObject)
            : base(conceptSchemeObject)
        {
            if (conceptSchemeObject.Items != null)
            {
                foreach (IConceptObject icurrent in conceptSchemeObject.Items)
                {
                    this.AddItem(new ConceptMutableCore(icurrent));
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the immutable instance.
        /// </summary>
        public override IConceptSchemeObject ImmutableInstance
        {
            get
            {
                return new ConceptSchemeObjectCore(this);
            }
        }

        #endregion

        #region Overrides of ItemSchemeMutableCore<IConceptMutableObject,IConceptObject,IConceptSchemeObject>

        public override IConceptMutableObject CreateItem(string id, string name)
        {
            IConceptMutableObject concept = new ConceptMutableCore();
            concept.Id = id;
            concept.AddName("en", name);
            AddItem(concept);
            return concept;
        }

        #endregion
    }
}
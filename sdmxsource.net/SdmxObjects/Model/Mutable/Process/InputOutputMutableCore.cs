// -----------------------------------------------------------------------
// <copyright file="InputOutputMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Process
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The input output mutable core.
    /// </summary>
    [Serializable]
    public class InputOutputMutableCore : AnnotableMutableCore, IInputOutputMutableObject
    {
        #region Fields

        /// <summary>
        ///   The _local id.
        /// </summary>
        private string _localId;

        /// <summary>
        ///   The _structure reference.
        /// </summary>
        private IStructureReference _structureReference;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="InputOutputMutableCore" /> class.
        /// </summary>
        public InputOutputMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.InputOutput))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InputOutputMutableCore"/> class.
        /// </summary>
        /// <param name="inputOutputObject">
        /// The inputOutputObject. 
        /// </param>
        public InputOutputMutableCore(IInputOutputObject inputOutputObject)
            : base(inputOutputObject)
        {
            this._localId = inputOutputObject.LocalId;
            if (inputOutputObject.StructureReference != null)
            {
                this._structureReference = inputOutputObject.StructureReference.CreateMutableInstance();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the local id.
        /// </summary>
        public virtual string LocalId
        {
            get
            {
                return this._localId;
            }

            set
            {
                this._localId = value;
            }
        }

        /// <summary>
        ///   Gets or sets the structure reference.
        /// </summary>
        public virtual IStructureReference StructureReference
        {
            get
            {
                return this._structureReference;
            }

            set
            {
                this._structureReference = value;
            }
        }

        #endregion
    }
}
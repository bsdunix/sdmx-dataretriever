// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeMapMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;

    /// <summary>
    ///   The concept scheme map mutable core.
    /// </summary>
    [Serializable]
    public class ConceptSchemeMapMutableCore : ItemSchemeMapMutableCore, IConceptSchemeMapMutableObject
    {
        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="ConceptSchemeMapMutableCore" /> class.
        /// </summary>
        public ConceptSchemeMapMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptSchemeMap))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConceptSchemeMapMutableCore"/> class.
        /// </summary>
        /// <param name="conceptSchemeMapObject">
        /// The icon. 
        /// </param>
        public ConceptSchemeMapMutableCore(IConceptSchemeMapObject conceptSchemeMapObject)
            : base(conceptSchemeMapObject)
        {
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="CategoryMapMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The category map mutable core.
    /// </summary>
    [Serializable]
    public class CategoryMapMutableCore : MutableCore, ICategoryMapMutableObject
    {
        #region Fields

        /// <summary>
        ///     The source id.
        /// </summary>
        private readonly IList<string> _sourceId;

        /// <summary>
        ///     The target id.
        /// </summary>
        private readonly IList<string> _targetId;

        /// <summary>
        ///     The alias.
        /// </summary>
        private string _alias;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryMapMutableCore" /> class.
        /// </summary>
        public CategoryMapMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryMap))
        {
            this._sourceId = new List<string>();
            this._targetId = new List<string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryMapMutableCore"/> class.
        /// </summary>
        /// <param name="catType">
        /// The cat type.
        /// </param>
        public CategoryMapMutableCore(ICategoryMap catType)
            : base(catType)
        {
            this._sourceId = new List<string>();
            this._targetId = new List<string>();
            this._alias = catType.Alias;
            if (catType.SourceId != null)
            {
                this._sourceId = catType.SourceId;
            }

            if (catType.TargetId != null)
            {
                this._targetId = catType.TargetId;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the alias.
        /// </summary>
        public virtual string Alias
        {
            get
            {
                return this._alias;
            }

            set
            {
                this._alias = value;
            }
        }

        /// <summary>
        ///     Gets the source id.
        /// </summary>
        public virtual IList<string> SourceId
        {
            get
            {
                return this._sourceId;
            }
        }

        /// <summary>
        ///     Gets the target id.
        /// </summary>
        public virtual IList<string> TargetId
        {
            get
            {
                return this._targetId;
            }
        }

        #endregion
    }
}
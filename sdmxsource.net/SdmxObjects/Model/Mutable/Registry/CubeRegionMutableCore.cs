// -----------------------------------------------------------------------
// <copyright file="CubeRegionMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///   The cube region mutable core.
    /// </summary>
    [Serializable]
    public class CubeRegionMutableCore : MutableCore, ICubeRegionMutableObject
    {
        #region Fields

        /// <summary>
        ///   The _attribute values.
        /// </summary>
        private IList<IKeyValuesMutable> _attributeValues;

        /// <summary>
        ///   The _key values.
        /// </summary>
        private IList<IKeyValuesMutable> _keyValues;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="CubeRegionMutableCore" /> class.
        /// </summary>
        public CubeRegionMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CubeRegion))
        {
            this._keyValues = new List<IKeyValuesMutable>();
            this._attributeValues = new List<IKeyValuesMutable>();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes a new instance of the <see cref="CubeRegionMutableCore"/> class.
        /// </summary>
        /// <param name="immutable">
        /// The immutable. 
        /// </param>
        public CubeRegionMutableCore(ICubeRegion immutable)
            : base(immutable)
        {
            this._keyValues = new List<IKeyValuesMutable>();
            this._attributeValues = new List<IKeyValuesMutable>();
            if (ObjectUtil.ValidCollection(immutable.KeyValues))
            {
                foreach (IKeyValues mutable in immutable.KeyValues)
                {
                    this._keyValues.Add(new KeyValuesMutableImpl(mutable));
                }
            }

            if (ObjectUtil.ValidCollection(immutable.AttributeValues))
            {
                foreach (IKeyValues keyValues in immutable.AttributeValues)
                {
                    this._attributeValues.Add(new KeyValuesMutableImpl(keyValues));
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the attribute values.
        /// </summary>
        public virtual IList<IKeyValuesMutable> AttributeValues
        {
            get
            {
                return this._attributeValues;
            }
        }

        /// <summary>
        ///   Gets the key values.
        /// </summary>
        public virtual IList<IKeyValuesMutable> KeyValues
        {
            get
            {
                return this._keyValues;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add attribute value.
        /// </summary>
        /// <param name="attvalue">
        /// The attvalue. 
        /// </param>
        public virtual void AddAttributeValue(IKeyValuesMutable attvalue)
        {
            this._attributeValues.Add(attvalue);
        }

        /// <summary>
        /// The add key value.
        /// </summary>
        /// <param name="keyvalue">
        /// The keyvalue. 
        /// </param>
        public virtual void AddKeyValue(IKeyValuesMutable inputValue)
        {
            	if (inputValue == null) {
			return;
		}
		
		IKeyValuesMutable foundKvm = null;
		foreach (IKeyValuesMutable kvm in _keyValues) {
			if (kvm.Id != null && kvm.Id.Equals(inputValue.Id)) {
				foundKvm = kvm;
				break;
			}
		}
		
		if (foundKvm == null) {
			// This id doesn't already exist so just add the KeyValuesMutable
			this._keyValues.Add(inputValue);
		} else {
			// A KeyValuesMutable with this id does already exist so merge the inputVlaue

		   

		    var q = (from x in inputValue.KeyValues select x).Distinct();
		    foreach (var itm in q)
		    {
		        foundKvm.AddValue(itm);
		    }
		    
		    /*foreach (String val in inputValue.KeyValues) {
				
                    foreach (String str in foundKvm.KeyValues) {
                        if (str.Equals(val))
                        {
                            break;
                        }
				
                    foundKvm.AddValue(val);
                }
            }*/
		}
        }

        /// <summary>
        /// The create mutable instance.
        /// </summary>
        /// <param name="parent">
        /// The parent. 
        /// </param>
        /// <returns>
        /// The <see cref="ICubeRegion"/> . 
        /// </returns>
        public ICubeRegion CreateMutableInstance(IContentConstraintObject parent)
        {
            return new CubeRegionCore(this, parent);
        }

        #endregion
    }
}
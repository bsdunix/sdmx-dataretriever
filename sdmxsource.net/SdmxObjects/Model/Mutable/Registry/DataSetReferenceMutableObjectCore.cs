﻿// -----------------------------------------------------------------------
// <copyright file="DataSetReferenceMutableObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DataSetReferenceMutableObjectCore : MutableCore, IDataSetReferenceMutableObject
    {
        private string datasetId;
        private IStructureReference dataProviderRef;

        public DataSetReferenceMutableObjectCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DatasetReference))
        {
        }

        public DataSetReferenceMutableObjectCore(IDataSetReference createdFrom)
            : base(createdFrom)
        {
            if (createdFrom.DataProviderReference != null)
            {
                this.dataProviderRef = new StructureReferenceImpl(createdFrom.DataProviderReference.TargetUrn);
            }
            this.datasetId = createdFrom.DatasetId;
        }

        #region Implementation of IDataSetReferenceMutableObject

        public string DatasetId
        {
            get
            {
                return datasetId;
            }
            set
            {
                datasetId = value;
            }
        }

        public IStructureReference DataProviderReference
        {
            get
            {
                return dataProviderRef;
            }
            set
            {
                dataProviderRef = value;
            }
        }

        #endregion
    }
}

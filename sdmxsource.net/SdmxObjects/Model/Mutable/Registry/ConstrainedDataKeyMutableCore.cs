// -----------------------------------------------------------------------
// <copyright file="ConstrainedDataKeyMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The constrained data key mutable core.
    /// </summary>
    [Serializable]
    public class ConstrainedDataKeyMutableCore : MutableCore, IConstrainedDataKeyMutableObject
    {
        #region Fields

        /// <summary>
        ///   The key values.
        /// </summary>
        private IList<IKeyValue> keyValues;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="ConstrainedDataKeyMutableCore" /> class.
        /// </summary>
        public ConstrainedDataKeyMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstrainedDataKey))
        {
            this.keyValues = new List<IKeyValue>();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes a new instance of the <see cref="ConstrainedDataKeyMutableCore"/> class.
        /// </summary>
        /// <param name="immutable">
        /// The immutable. 
        /// </param>
        public ConstrainedDataKeyMutableCore(IConstrainedDataKey immutable)
            : base(immutable)
        {
            this.keyValues = new List<IKeyValue>();

            foreach (IKeyValue each in immutable.KeyValues)
            {
                this.keyValues.Add(new KeyValueImpl(each.Code, each.Concept));
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the key values.
        /// </summary>
        public virtual IList<IKeyValue> KeyValues
        {
            get
            {
                return new ReadOnlyCollection<IKeyValue>(this.keyValues);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add key value.
        /// </summary>
        /// <param name="keyvalue">
        /// The keyvalue. 
        /// </param>
        public virtual void AddKeyValue(IKeyValue keyvalue)
        {
            if (keyvalue != null)
            {
                this.keyValues.Add(keyvalue);
            }
        }

        #endregion
    }
}
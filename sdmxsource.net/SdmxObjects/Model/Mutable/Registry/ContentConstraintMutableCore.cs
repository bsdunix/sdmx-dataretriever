// -----------------------------------------------------------------------
// <copyright file="ContentConstraintMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;

    /// <summary>
    ///   The content constraint mutable core.
    /// </summary>
    [Serializable]
    public class ContentConstraintMutableCore : ConstraintMutableCore<IContentConstraintObject>, IContentConstraintMutableObject
    {
        #region Fields

        /// <summary>
        ///   The _excluded cube region.
        /// </summary>
        private ICubeRegionMutableObject _excludedCubeRegion;

        /// <summary>
        ///   The _included cube region.
        /// </summary>
        private ICubeRegionMutableObject _includedCubeRegion;

        /// <summary>
        ///   The _is defining actual data present.
        /// </summary>
        private bool _isDefiningActualDataPresent; // Default Value

        /// <summary>
        ///   The _reference period.
        /// </summary>
        private IReferencePeriodMutableObject _referencePeriod;

        /// <summary>
        ///   The _release calendar.
        /// </summary>
        private IReleaseCalendarMutableObject _releaseCalendar;

        private IMetadataTargetRegionMutableObject _metadataTargetRegionBean;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="ContentConstraintMutableCore" /> class.
        /// </summary>
        public ContentConstraintMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ContentConstraint))
        {
            this._referencePeriod = null;
            this._releaseCalendar = null;
            this._isDefiningActualDataPresent = true;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentConstraintMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The obj target. 
        /// </param>
        public ContentConstraintMutableCore(IContentConstraintObject objTarget)
            : base(objTarget)
        {
            this._referencePeriod = null;
            this._releaseCalendar = null;
            this._isDefiningActualDataPresent = true;
            if (objTarget.IncludedCubeRegion != null)
            {
                this._includedCubeRegion = new CubeRegionMutableCore(objTarget.IncludedCubeRegion);
            }

            if (objTarget.ExcludedCubeRegion != null)
            {
                this._excludedCubeRegion = new CubeRegionMutableCore(objTarget.ExcludedCubeRegion);
            }

            if (objTarget.ReferencePeriod != null)
            {
                this._referencePeriod = objTarget.ReferencePeriod.CreateMutableObject();
            }

            if (objTarget.ReleaseCalendar != null)
            {
                this._releaseCalendar = objTarget.ReleaseCalendar.CreateMutableObject();
            }
            if (objTarget.MetadataTargetRegion != null) 
                this._metadataTargetRegionBean = new MetadataTargetRegionMutableObjectCore(objTarget.MetadataTargetRegion);

            this._isDefiningActualDataPresent = objTarget.IsDefiningActualDataPresent;
        }

        #endregion

        #region Public Properties

        public IMetadataTargetRegionMutableObject MetadataTargetRegion
        {
            get
            {
                return _metadataTargetRegionBean;
            }
            set
            {
                _metadataTargetRegionBean = value;
            }
        }

        /// <summary>
        ///   Gets or sets the excluded cube region.
        /// </summary>
        public virtual ICubeRegionMutableObject ExcludedCubeRegion
        {
            get
            {
                return this._excludedCubeRegion;
            }

            set
            {
                this._excludedCubeRegion = value;
            }
        }

        /// <summary>
        ///   Gets the immutable instance.
        /// </summary>
        public override IContentConstraintObject ImmutableInstance
        {
            get
            {
                return new ContentConstraintObjectCore(this);
            }
        }

        /// <summary>
        ///   Gets or sets the included cube region.
        /// </summary>
        public virtual ICubeRegionMutableObject IncludedCubeRegion
        {
            get
            {
                return this._includedCubeRegion;
            }

            set
            {
                this._includedCubeRegion = value;
            }
        }

        /// <summary>
        ///   Gets or sets a value indicating whether is defining actual data present.
        /// </summary>
        public virtual bool IsDefiningActualDataPresent
        {
            get
            {
                return this._isDefiningActualDataPresent;
            }

            set
            {
                this._isDefiningActualDataPresent = value;
            }
        }

        /// <summary>
        ///   Gets or sets the reference period.
        /// </summary>
        public virtual IReferencePeriodMutableObject ReferencePeriod
        {
            get
            {
                return this._referencePeriod;
            }

            set
            {
                this._referencePeriod = value;
            }
        }

        /// <summary>
        ///   Gets or sets the release calendar.
        /// </summary>
        public virtual IReleaseCalendarMutableObject ReleaseCalendar
        {
            get
            {
                return this._releaseCalendar;
            }

            set
            {
                this._releaseCalendar = value;
            }
        }

        #endregion
    }

    
}
// -----------------------------------------------------------------------
// <copyright file="ProvisionAgreementMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;

    /// <summary>
    ///   The provision agreement mutable core.
    /// </summary>
    [Serializable]
    public class ProvisionAgreementMutableCore : MaintainableMutableCore<IProvisionAgreementObject>, 
                                                 IProvisionAgreementMutableObject
    {
        #region Fields

        /// <summary>
        ///   The dataprovider ref.
        /// </summary>
        private IStructureReference dataproviderRef;

        /// <summary>
        ///   The structure useage.
        /// </summary>
        private IStructureReference structureUseage;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="ProvisionAgreementMutableCore" /> class.
        /// </summary>
        public ProvisionAgreementMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ProvisionAgreement))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProvisionAgreementMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The obj target. 
        /// </param>
        public ProvisionAgreementMutableCore(IProvisionAgreementObject objTarget)
            : base(objTarget)
        {
            if (objTarget.StructureUseage != null)
            {
                this.structureUseage = objTarget.StructureUseage.CreateMutableInstance();
            }

            if (objTarget.DataproviderRef != null)
            {
                this.dataproviderRef = objTarget.DataproviderRef.CreateMutableInstance();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the dataprovider ref.
        /// </summary>
        public virtual IStructureReference DataproviderRef
        {
            get
            {
                return this.dataproviderRef;
            }

            set
            {
                this.dataproviderRef = value;
            }
        }

        /// <summary>
        ///   Gets the immutable instance.
        /// </summary>
        public override IProvisionAgreementObject ImmutableInstance
        {
            get
            {
                return new ProvisionAgreementObjectCore(this);
            }
        }

        /// <summary>
        ///   Gets or sets the structure usage.
        /// </summary>
        public virtual IStructureReference StructureUsage
        {
            get
            {
                return this.structureUseage;
            }

            set
            {
                this.structureUseage = value;
            }
        }

        #endregion
    }
}
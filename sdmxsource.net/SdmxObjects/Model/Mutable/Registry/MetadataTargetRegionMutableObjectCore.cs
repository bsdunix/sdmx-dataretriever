﻿// -----------------------------------------------------------------------
// <copyright file="MetadataTargetRegionMutableObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;

    /// <summary>
	/// TODO: Update summary.
	/// </summary>
	public class MetadataTargetRegionMutableObjectCore : MutableCore, IMetadataTargetRegionMutableObject
	{

		private readonly bool include;
		private readonly string report;
		private readonly string metadataTarget;
		private readonly IList<IKeyValuesMutable> attributes = new List<IKeyValuesMutable>();
		private readonly IList<IMetadataTargetKeyValuesMutable> key = new List<IMetadataTargetKeyValuesMutable>();

		public MetadataTargetRegionMutableObjectCore()
			: base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataTargetRegion))
		{
		}

		public MetadataTargetRegionMutableObjectCore(IMetadataTargetRegion createdFrom)
			: base(createdFrom)
		{
			this.include  = createdFrom.IsInclude;
		this.report = createdFrom.Report;
		this.metadataTarget = createdFrom.MetadataTarget;
		foreach(IMetadataTargetKeyValues currentKv in createdFrom.Key) 
		{
			key.Add(new MetadataTargetKeyValuesMutableObjectCore(currentKv));
		}
		foreach(IKeyValues currentKv in createdFrom.Attributes) {
			attributes.Add(new KeyValuesMutableImpl(currentKv));
		}
		}

		#region Implementation of IMetadataTargetRegionMutableObject

		public bool IsInclude
		{
			get
			{
				return include;
			}
		}

		public string Report
		{
			get
			{
				return report;
			}
		}

		public string MetadataTarget
		{
			get
			{
				return metadataTarget;
			}
		}

		public IList<IMetadataTargetKeyValuesMutable> Key
		{
			get
			{
				return key;
			}
		}

		public void AddKey(IMetadataTargetKeyValuesMutable key)
		{
			this.key.Add(key);
		}

		public IList<IKeyValuesMutable> Attributes
		{
			get
			{
				return attributes;
			}
		}

		public void AddAttribute(IKeyValuesMutable attribute)
		{
			this.attributes.Add(attribute);
		}

		#endregion
	}
}

// -----------------------------------------------------------------------
// <copyright file="ReferencePeriodMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The reference period mutable core.
    /// </summary>
    [Serializable]
    public class ReferencePeriodMutableCore : MutableCore, IReferencePeriodMutableObject
    {
        #region Fields

        /// <summary>
        ///   The _end time.
        /// </summary>
        private DateTime? _endTime;

        /// <summary>
        ///   The _start time.
        /// </summary>
        private DateTime? _startTime;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="ReferencePeriodMutableCore" /> class.
        /// </summary>
        public ReferencePeriodMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReferencePeriod))
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes a new instance of the <see cref="ReferencePeriodMutableCore"/> class.
        /// </summary>
        /// <param name="immutable">
        /// The immutable. 
        /// </param>
        public ReferencePeriodMutableCore(IReferencePeriod immutable)
            : base(immutable)
        {
            if (immutable.StartTime != null)
            {
                this._startTime = immutable.StartTime.Date;
            }

            if (immutable.EndTime != null)
            {
                this._endTime = immutable.EndTime.Date;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the end time.
        /// </summary>
        public virtual DateTime? EndTime
        {
            get
            {
                return this._endTime;
            }

            set
            {
                this._endTime = value;
            }
        }

        /// <summary>
        ///   Gets or sets the start time.
        /// </summary>
        public virtual DateTime? StartTime
        {
            get
            {
                return this._startTime;
            }

            set
            {
                this._startTime = value;
            }
        }

        #endregion
    }
}
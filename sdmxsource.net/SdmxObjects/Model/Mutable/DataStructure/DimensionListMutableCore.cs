// -----------------------------------------------------------------------
// <copyright file="DimensionListMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The dimension list mutable core.
    /// </summary>
    [Serializable]
    public class DimensionListMutableCore : IdentifiableMutableCore, IDimensionListMutableObject
    {
        #region Fields

        /// <summary>
        ///   The dimensions.
        /// </summary>
        private IList<IDimensionMutableObject> dimensions;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="DimensionListMutableCore" /> class.
        /// </summary>
        public DimensionListMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DimensionDescriptor))
        {
            this.dimensions = new List<IDimensionMutableObject>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimensionListMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The obj target. 
        /// </param>
        public DimensionListMutableCore(IDimensionList objTarget)
            : base(objTarget)
        {
            this.dimensions = new List<IDimensionMutableObject>();
            if (objTarget.Dimensions != null)
            {
                foreach (IDimension currentDimension in objTarget.Dimensions)
                {
                    this.dimensions.Add(new DimensionMutableCore(currentDimension));
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the dimensions.
        /// </summary>
        public IList<IDimensionMutableObject> Dimensions
        {
            get
            {
                return this.dimensions;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add dimension.
        /// </summary>
        /// <param name="dimension">
        /// The dimension. 
        /// </param>
        public void AddDimension(IDimensionMutableObject dimension)
        {
            if (this.dimensions == null)
            {
                this.dimensions = new List<IDimensionMutableObject>();
            }

            this.dimensions.Add(dimension);
        }

        #endregion
    }
}
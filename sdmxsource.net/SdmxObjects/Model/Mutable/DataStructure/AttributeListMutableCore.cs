// -----------------------------------------------------------------------
// <copyright file="AttributeListMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The attribute list mutable core.
    /// </summary>
    [Serializable]
    public class AttributeListMutableCore : IdentifiableMutableCore, IAttributeListMutableObject
    {
        #region Fields

        /// <summary>
        ///   The attributes.
        /// </summary>
        private readonly IList<IAttributeMutableObject> _attributes;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="AttributeListMutableCore" /> class.
        /// </summary>
        public AttributeListMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AttributeDescriptor))
        {
            this._attributes = new List<IAttributeMutableObject>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeListMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The agencySchemeMutable target. 
        /// </param>
        public AttributeListMutableCore(IAttributeList objTarget)
            : base(objTarget)
        {
            this._attributes = new List<IAttributeMutableObject>();
            if (objTarget.Attributes != null)
            {
                foreach (IAttributeObject currentAttribute in objTarget.Attributes)
                {
                    this._attributes.Add(new AttributeMutableCore(currentAttribute));
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the attributes.
        /// </summary>
        public IList<IAttributeMutableObject> Attributes
        {
            get
            {
                return this._attributes;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Adds the specifiedd attribute.
        /// </summary>
        /// <param name="attribute">
        /// The attribute. 
        /// </param>
        public void AddAttribute(IAttributeMutableObject attribute)
        {
            this._attributes.Add(attribute);
        }

        #endregion
    }
}
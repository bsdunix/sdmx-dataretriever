// -----------------------------------------------------------------------
// <copyright file="CrossSectionalMeasureMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The cross sectional measure mutable core.
    /// </summary>
    [Serializable]
    public class CrossSectionalMeasureMutableCore : ComponentMutableCore, ICrossSectionalMeasureMutableObject
    {
        #region Fields

        /// <summary>
        ///   The code.
        /// </summary>
        private string code;

        /// <summary>
        ///   The measure dimension.
        /// </summary>
        private string measureDimension;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CrossSectionalMeasureMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The obj target. 
        /// </param>
        public CrossSectionalMeasureMutableCore(ICrossSectionalMeasure objTarget)
            : base(objTarget)
        {
            this.measureDimension = objTarget.MeasureDimension;
            this.code = objTarget.Code;
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="CrossSectionalMeasureMutableCore" /> class.
        /// </summary>
        public CrossSectionalMeasureMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CrossSectionalMeasure))
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the code.
        /// </summary>
        public virtual string Code
        {
            get
            {
                return this.code;
            }

            set
            {
                this.code = value;
            }
        }

        /// <summary>
        ///   Gets or sets the measure dimension.
        /// </summary>
        public virtual string MeasureDimension
        {
            get
            {
                return this.measureDimension;
            }

            set
            {
                this.measureDimension = value;
            }
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="GroupMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The group mutable core.
    /// </summary>
    [Serializable]
    public class GroupMutableCore : IdentifiableMutableCore, IGroupMutableObject
    {
        #region Fields

        /// <summary>
        ///   The _dimension ref.
        /// </summary>
        private readonly IList<string> _dimensionRef;

        /// <summary>
        ///   The _attachment constraint ref.
        /// </summary>
        private IStructureReference _attachmentConstraintRef;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="GroupMutableCore" /> class.
        /// </summary>
        public GroupMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Group))
        {
            this._dimensionRef = new List<string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The obj target. 
        /// </param>
        public GroupMutableCore(IGroup objTarget)
            : base(objTarget)
        {
            if (objTarget.AttachmentConstraintRef != null)
            {
                this._attachmentConstraintRef = objTarget.AttachmentConstraintRef;
            }

            this._dimensionRef = objTarget.DimensionRefs;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the attachment constraint ref.
        /// </summary>
        public IStructureReference AttachmentConstraintRef
        {
            get
            {
                return this._attachmentConstraintRef;
            }

            set
            {
                this._attachmentConstraintRef = value;
            }
        }

        /// <summary>
        ///   Gets the dimension ref.
        /// </summary>
        public IList<string> DimensionRef
        {
            get
            {
                return this._dimensionRef;
            }
        }

        #endregion
    }
}
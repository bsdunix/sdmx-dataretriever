// -----------------------------------------------------------------------
// <copyright file="CodelistRefMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Reference
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The codelist ref mutable core.
    /// </summary>
    [Serializable]
    public class CodelistRefMutableCore : MutableCore, ICodelistRefMutableObject
    {
        #region Fields

        /// <summary>
        ///   The alias.
        /// </summary>
        private string alias;

        /// <summary>
        ///   The structure reference.
        /// </summary>
        private IStructureReference structureReference;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="CodelistRefMutableCore" /> class.
        /// </summary>
        public CodelistRefMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeListRef))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CodelistRefMutableCore"/> class.
        /// </summary>
        /// <param name="codelistRef">
        /// The codelistRef. 
        /// </param>
        public CodelistRefMutableCore(ICodelistRef codelistRef)
            : base(codelistRef)
        {
            this.alias = codelistRef.Alias;
            this.structureReference = codelistRef.CodelistReference.CreateMutableInstance();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the alias.
        /// </summary>
        public virtual string Alias
        {
            get
            {
                return this.alias;
            }

            set
            {
                this.alias = value;
            }
        }

        /// <summary>
        ///   Gets or sets the codelist reference.
        /// </summary>
        public virtual IStructureReference CodelistReference
        {
            get
            {
                return this.structureReference;
            }

            set
            {
                this.structureReference = value;
            }
        }

        #endregion
    }
}
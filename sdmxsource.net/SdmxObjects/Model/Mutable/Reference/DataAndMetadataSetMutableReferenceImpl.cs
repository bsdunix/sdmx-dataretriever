// -----------------------------------------------------------------------
// <copyright file="DataAndMetadataSetMutableReferenceImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Reference
{
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///   The data and metadata set mutable reference impl.
    /// </summary>
    public class DataAndMetadataSetMutableReferenceImpl : IDataAndMetadataSetMutableReference
    {
        #region Fields

        /// <summary>
        ///   The _is is data set reference.
        /// </summary>
        private bool _isIsDataSetReference;

        /// <summary>
        ///   The data set reference.
        /// </summary>
        private IStructureReference dataSetReference;

        /// <summary>
        ///   The set id.
        /// </summary>
        private string setId;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="DataAndMetadataSetMutableReferenceImpl" /> class.
        /// </summary>
        public DataAndMetadataSetMutableReferenceImpl()
        {
            this.dataSetReference = null;
            this.setId = null;
            this._isIsDataSetReference = false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes a new instance of the <see cref="DataAndMetadataSetMutableReferenceImpl"/> class.
        /// </summary>
        /// <param name="immutable">
        /// The immutable. 
        /// </param>
        public DataAndMetadataSetMutableReferenceImpl(IDataAndMetadataSetReference immutable)
        {
            this.dataSetReference = null;
            this.setId = null;
            this._isIsDataSetReference = false;
            if (immutable.DataSetReference != null)
            {
                this.dataSetReference = immutable.DataSetReference.CreateMutableInstance();
            }

            this.setId = immutable.SetId;
            this._isIsDataSetReference = immutable.IsDataSetReference;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the data set reference.
        /// </summary>
        public virtual IStructureReference DataSetReference
        {
            get
            {
                return this.dataSetReference;
            }

            set
            {
                this.dataSetReference = value;
            }
        }

        /// <summary>
        ///   Gets or sets a value indicating whether is data set reference.
        /// </summary>
        public virtual bool IsDataSetReference
        {
            get
            {
                return this._isIsDataSetReference;
            }

            set
            {
                this._isIsDataSetReference = value;
            }
        }

        /// <summary>
        ///   Gets or sets the set id.
        /// </summary>
        public virtual string SetId
        {
            get
            {
                return this.setId;
            }

            set
            {
                this.setId = value;
            }
        }

        #endregion
    }
}
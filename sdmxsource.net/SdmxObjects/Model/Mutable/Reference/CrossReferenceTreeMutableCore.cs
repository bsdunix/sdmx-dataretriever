// -----------------------------------------------------------------------
// <copyright file="CrossReferenceTreeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Reference
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///   Used to send to external applications that require a default constructor
    /// </summary>
    public class CrossReferenceTreeMutableCore : ICrossReferenceTreeMutable
    {
        #region Fields

        /// <summary>
        ///   The _referencing objectList.
        /// </summary>
        private readonly IList<ICrossReferenceTreeMutable> _referencingObjects;

        private IMaintainableMutableObject _maintainableMutableObject;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="CrossReferenceTreeMutableCore" /> class.
        /// </summary>
        public CrossReferenceTreeMutableCore()
        {
            this._referencingObjects = new List<ICrossReferenceTreeMutable>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrossReferenceTreeMutableCore"/> class.
        /// </summary>
        /// <param name="crossReferencingTree">
        /// The cross referencing tree. 
        /// </param>
        public CrossReferenceTreeMutableCore(ICrossReferencingTree crossReferencingTree)
            : this(crossReferencingTree, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrossReferenceTreeMutableCore"/> class.
        /// </summary>
        /// <param name="crossReferencingTree">
        /// The cross referencing tree. 
        /// </param>
        /// <param name="serviceRetrievalManager">
        /// The service retrieval manager. 
        /// </param>
        public CrossReferenceTreeMutableCore(
            ICrossReferencingTree crossReferencingTree, IServiceRetrievalManager serviceRetrievalManager)
        {
            this._referencingObjects = new List<ICrossReferenceTreeMutable>();
            if (serviceRetrievalManager != null)
            {
                this._maintainableMutableObject =
                    serviceRetrievalManager.CreateStub(crossReferencingTree.Maintainable).MutableInstance;
            }
            else
            {
                this._maintainableMutableObject = crossReferencingTree.Maintainable.MutableInstance;
            }

            foreach (ICrossReferencingTree currentChildReference in crossReferencingTree.ReferencingStructures)
            {
                this._referencingObjects.Add(new CrossReferenceTreeMutableCore(currentChildReference));
            }
        }

        #endregion

        #region Public Properties

        public virtual IMaintainableMutableObject MaintianableObject
        {
            get
            {
                return this._maintainableMutableObject;
            }
            set
            {
                this._maintainableMutableObject = value;
            }
        }

        /// <summary>
        ///   Gets or sets the maintianable.
        /// </summary>
       

        /// <summary>
        ///   Gets or sets the referencing objectList.
        /// </summary>
        public IList<ICrossReferenceTreeMutable> ReferencingObjects
        {
            get
            {
                return this._referencingObjects;
            }
        }

        #endregion
    }
}
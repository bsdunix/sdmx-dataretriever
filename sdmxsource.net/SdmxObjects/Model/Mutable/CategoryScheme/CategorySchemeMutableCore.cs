// -----------------------------------------------------------------------
// <copyright file="CategorySchemeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.CategoryScheme;

    /// <summary>
    ///   The category scheme mutable core.
    /// </summary>
    [Serializable]
    public class CategorySchemeMutableCore :
        ItemSchemeMutableCore<ICategoryMutableObject, ICategoryObject, ICategorySchemeObject>, 
        ICategorySchemeMutableObject
    {
        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="CategorySchemeMutableCore" /> class.
        /// </summary>
        public CategorySchemeMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CategorySchemeMutableCore"/> class.
        /// </summary>
        /// <param name="categorySchemeObject">
        /// The categorySchemeObject. 
        /// </param>
        public CategorySchemeMutableCore(ICategorySchemeObject categorySchemeObject)
            : base(categorySchemeObject)
        {
            // convert Category list to Mutable Category beans
            if (categorySchemeObject.Items != null)
            {
                foreach (ICategoryObject category in categorySchemeObject.Items)
                {
                    this.AddItem(new CategoryMutableCore(category));
                }
            }
        }

        #endregion

        /*public ICategorySchemeObject ImmutableInstance {
          get {
                return new CategorySchemeObjectCore(this);
            }
        }*/

        // public void setItems(List<ICategoryMutableObject> items) {
        // this.items = items;
        // }
        // // interface specifies ICategoryObject type so change to mutable inside add() 
        // public void addItem(ICategoryMutableObject item) {
        // this.items.add((ICategoryMutableObject)item);
        // }
        // public List<ICategoryMutableObject> getItems() {
        // return items;
        // }
        /* ^^^
        ICategorySchemeObject ICategorySchemeMutableObject.ImmutableInstance
        {
            get { return new CategorySchemeObjectCore(this);}
        }*/

        /* public override ICategorySchemeObject ImmutableInstance
        {
            get { return new CategorySchemeObjectCore(this); }
        }*/
        #region Public Properties

        /// <summary>
        ///   Gets the immutable instance.
        /// </summary>
        public override ICategorySchemeObject ImmutableInstance
        {
            get
            {
                return new CategorySchemeObjectCore(this);
            }
        }

        #endregion

        #region Overrides of ItemSchemeMutableCore<ICategoryMutableObject,ICategoryObject,ICategorySchemeObject>

        public override ICategoryMutableObject CreateItem(string id, string name)
        {
            ICategoryMutableObject cat = new CategoryMutableCore();
            cat.Id = id;
            cat.AddName("en", name);
            AddItem(cat);
            return cat;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="ReportingCategoryMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The reporting category mutable core.
    /// </summary>
    [Serializable]
    public class ReportingCategoryMutableCore : ItemMutableCore, IReportingCategoryMutableObject
    {
        #region Fields

        /// <summary>
        ///   The provisioning metadata.
        /// </summary>
        private IList<IStructureReference> _provisioningMetadata;

        /// <summary>
        ///   The reporting categories.
        /// </summary>
        private readonly IList<IReportingCategoryMutableObject> _reportingCategories;

        /// <summary>
        ///   The structural metadata.
        /// </summary>
        private IList<IStructureReference> _structuralMetadata;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="ReportingCategoryMutableCore" /> class. 
        ///   Default Constructor
        /// </summary>
        public ReportingCategoryMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportingCategory))
        {
            this._structuralMetadata = new List<IStructureReference>();
            this._provisioningMetadata = new List<IStructureReference>();
            this._reportingCategories = new List<IReportingCategoryMutableObject>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportingCategoryMutableCore"/> class. 
        ///   Construct from IReportingCategoryObject Bean
        /// </summary>
        /// <param name="objTarget">The Reporting CategoryObject
        /// </param>
        public ReportingCategoryMutableCore(IReportingCategoryObject objTarget)
            : base(objTarget)
        {
            this._structuralMetadata = new List<IStructureReference>();
            this._provisioningMetadata = new List<IStructureReference>();
            this._reportingCategories = new List<IReportingCategoryMutableObject>();

            if (objTarget.StructuralMetadata != null)
            {
                foreach (ICrossReference crossReference in objTarget.StructuralMetadata)
                {
                    this._structuralMetadata.Add(crossReference.CreateMutableInstance());
                }
            }

            if (objTarget.ProvisioningMetadata != null)
            {
                foreach (ICrossReference p in objTarget.ProvisioningMetadata)
                {
                    this._provisioningMetadata.Add(p.CreateMutableInstance());
                }
            }

            if (objTarget.Items != null)
            {
                foreach (IReportingCategoryObject reportingCategoryObject in objTarget.Items)
                {
                    this._reportingCategories.Add(new ReportingCategoryMutableCore(reportingCategoryObject));
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the items.
        /// </summary>
        public IList<IReportingCategoryMutableObject> Items
        {
            get
            {
                return this._reportingCategories;
            }
       }

        /// <summary>
        ///   Gets the provisioning metadata.
        /// </summary>
        public IList<IStructureReference> ProvisioningMetadata
        {
            get
            {
                return this._provisioningMetadata;
            }

            set
            {
                this._provisioningMetadata = value;
            }
        }

        /// <summary>
        ///   Gets the structural metadata.
        /// </summary>
        public IList<IStructureReference> StructuralMetadata
        {
            get
            {
                return this._structuralMetadata;
            }

            set
            {
                this._structuralMetadata = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add item.
        /// </summary>
        /// <param name="item">
        /// The reporting category. 
        /// </param>
        public void AddItem(IReportingCategoryMutableObject item)
        {
            this._reportingCategories.Add(item);
        }

        #endregion
    }
}
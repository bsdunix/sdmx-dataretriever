// -----------------------------------------------------------------------
// <copyright file="ReportingTaxonomyMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.CategoryScheme;

    /// <summary>
    ///   The reporting taxonomy mutable core.
    /// </summary>
    [Serializable]
    public class ReportingTaxonomyMutableCore :
        ItemSchemeMutableCore<IReportingCategoryMutableObject, IReportingCategoryObject, IReportingTaxonomyObject>, 
        IReportingTaxonomyMutableObject
    {
        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="ReportingTaxonomyMutableCore" /> class.
        /// </summary>
        public ReportingTaxonomyMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportingTaxonomy))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportingTaxonomyMutableCore"/> class.
        /// </summary>
        /// <param name="reportingTaxonomy">
        /// The reportingTaxonomy. 
        /// </param>
        public ReportingTaxonomyMutableCore(IReportingTaxonomyObject reportingTaxonomy)
            : base(reportingTaxonomy)
        {
            // make into a Category mutable list
            if (reportingTaxonomy.Items != null)
            {
                foreach (IReportingCategoryObject categoryObject in reportingTaxonomy.Items)
                {
                    this.AddReportingCategory(new ReportingCategoryMutableCore(categoryObject));
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the immutable instance.
        /// </summary>
        public override IReportingTaxonomyObject ImmutableInstance
        {
            get
            {
                return new ReportingTaxonomyObjectCore(this);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add reporting category.
        /// </summary>
        /// <param name="category">
        /// The category. 
        /// </param>
        public void AddReportingCategory(IReportingCategoryMutableObject category)
        {
            this.AddItem(category);
        }

        #endregion

        #region Overrides of ItemSchemeMutableCore<IReportingCategoryMutableObject,IReportingCategoryObject,IReportingTaxonomyObject>

        public override IReportingCategoryMutableObject CreateItem(string id, string name)
        {
            IReportingCategoryMutableObject rcMut = new ReportingCategoryMutableCore();
            rcMut.Id = id;
            rcMut.AddName("en", name);
            AddItem(rcMut);
            return rcMut;
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="CategoryMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The category mutable core.
    /// </summary>
    [Serializable]
    public sealed class CategoryMutableCore : ItemMutableCore, ICategoryMutableObject
    {
        #region Fields

        /// <summary>
        ///   The _categories.
        /// </summary>
        private readonly IList<ICategoryMutableObject> _categories;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="CategoryMutableCore" /> class.
        /// </summary>
        public CategoryMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category))
        {
            this._categories = new List<ICategoryMutableObject>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The obj target. 
        /// </param>
        public CategoryMutableCore(ICategoryObject objTarget)
            : base(objTarget)
        {
            this._categories = new List<ICategoryMutableObject>();

            // make into mutable category beans
            if (objTarget.Items != null)
            {
                this._categories = new List<ICategoryMutableObject>();

                foreach (ICategoryObject category in objTarget.Items)
                {
                    this.AddItem(new CategoryMutableCore(category));
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the items.
        /// </summary>
        public IList<ICategoryMutableObject> Items
        {
            get
            {
                return this._categories;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add item.
        /// </summary>
        /// <param name="item">
        /// The item. 
        /// </param>
        public void AddItem(ICategoryMutableObject item)
        {
            this._categories.Add(item);
        }

        #endregion
    }
}
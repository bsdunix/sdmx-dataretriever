// -----------------------------------------------------------------------
// <copyright file="DataConsumerSchemeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    ///   The data consumer scheme mutable core.
    /// </summary>
    [Serializable]
    public class DataConsumerSchemeMutableCore :
        ItemSchemeMutableCore<IDataConsumerMutableObject, IDataConsumer, IDataConsumerScheme>, 
        IDataConsumerSchemeMutableObject
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataConsumerSchemeMutableCore"/> class.
        /// </summary>
        /// <param name="dataConsumerScheme">
        /// The dataConsumerScheme. 
        /// </param>
        public DataConsumerSchemeMutableCore(IDataConsumerScheme dataConsumerScheme)
            : base(dataConsumerScheme)
        {
            foreach (IDataConsumer idataConsumer in dataConsumerScheme.Items)
            {
                this.AddItem(new DataConsumerMutableCore(idataConsumer));
            }
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="DataConsumerSchemeMutableCore" /> class.
        /// </summary>
        public DataConsumerSchemeMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataConsumerScheme))
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the immutable instance.
        /// </summary>
        public override IDataConsumerScheme ImmutableInstance
        {
            get
            {
                return new DataConsumerSchemeCore(this);
            }
        }

        #endregion

        #region Overrides of ItemSchemeMutableCore<IDataConsumerMutableObject,IDataConsumer,IDataConsumerScheme>

        public override IDataConsumerMutableObject CreateItem(string id, string name)
        {
            IDataConsumerMutableObject consumerMutableCore = new DataConsumerMutableCore();
            consumerMutableCore.Id = id;
            consumerMutableCore.AddName("en", name);
            AddItem(consumerMutableCore);
            return consumerMutableCore;
        }

        #endregion

     
    }
}
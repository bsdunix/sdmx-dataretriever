// -----------------------------------------------------------------------
// <copyright file="DataSourceMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///   The data source mutable core.
    /// </summary>
    [Serializable]
    public class DataSourceMutableCore : MutableCore, IDataSourceMutableObject
    {
        #region Fields

        /// <summary>
        ///   The data url.
        /// </summary>
        private Uri _dataUrl;

        /// <summary>
        ///   The is rest datasource.
        /// </summary>
        private bool _isRestDatasource;

        /// <summary>
        ///   The is simple datasource.
        /// </summary>
        private bool _isSimpleDatasource;

        /// <summary>
        ///   The is web service datasource.
        /// </summary>
        private bool _isWebServiceDatasource;

        /// <summary>
        ///   The wadl url.
        /// </summary>
        private Uri _wadlUrl;

        /// <summary>
        ///   The wsdl url.
        /// </summary>
        private Uri _wsdlUrl;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="DataSourceMutableCore" /> class.
        /// </summary>
        public DataSourceMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Datasource))
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourceMutableCore"/> class.
        /// </summary>
        /// <param name="datasource">
        /// The idatasource. 
        /// </param>
        public DataSourceMutableCore(IDataSource datasource)
            : base(datasource)
        {
            if (datasource.DataUrl != null)
            {
                this._dataUrl = datasource.DataUrl;
            }

            if (datasource.WsdlUrl != null)
            {
                this._wsdlUrl = datasource.WsdlUrl;
            }

            if (datasource.WadlUrl != null)
            {
                this._wadlUrl = datasource.WadlUrl;
            }

            this._isRestDatasource = datasource.RESTDatasource;
            this._isSimpleDatasource = datasource.SimpleDatasource;
            this._isWebServiceDatasource = datasource.WebServiceDatasource;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the data url.
        /// </summary>
        public virtual Uri DataUrl
        {
            get
            {
                return this._dataUrl;
            }

            set
            {
                this._dataUrl = value;
            }
        }

        /// <summary>
        ///   Gets or sets a value indicating whether rest datasource.
        /// </summary>
        public virtual bool RESTDatasource
        {
            get
            {
                return this._isRestDatasource;
            }

            set
            {
                this._isRestDatasource = value;
            }
        }

        /// <summary>
        ///   Gets or sets a value indicating whether simple datasource.
        /// </summary>
        public virtual bool SimpleDatasource
        {
            get
            {
                return this._isSimpleDatasource;
            }

            set
            {
                this._isSimpleDatasource = value;
            }
        }

        /// <summary>
        ///   Gets or sets the wadl url.
        /// </summary>
        public virtual Uri WADLUrl
        {
            get
            {
                return this._wadlUrl;
            }

            set
            {
                this._wadlUrl = value;
            }
        }

        /// <summary>
        ///   Gets or sets the wsdl url.
        /// </summary>
        public virtual Uri WSDLUrl
        {
            get
            {
                return this._wsdlUrl;
            }

            set
            {
                this._wsdlUrl = value;
            }
        }

        /// <summary>
        ///   Gets or sets a value indicating whether web service datasource.
        /// </summary>
        public virtual bool WebServiceDatasource
        {
            get
            {
                return this._isWebServiceDatasource;
            }

            set
            {
                this._isWebServiceDatasource = value;
            }
        }

        #endregion
    }
}
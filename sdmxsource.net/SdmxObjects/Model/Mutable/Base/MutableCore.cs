// -----------------------------------------------------------------------
// <copyright file="MutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    ///   The mutable core.
    /// </summary>
    [Serializable]
    public abstract class MutableCore : IMutableObject
    {
        #region Fields

        /// <summary>
        ///   The structure type.
        /// </summary>
        private SdmxStructureType structureType;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MutableCore"/> class.
        /// </summary>
        /// <param name="structureType0">
        /// The structure type 0. 
        /// </param>
        protected internal MutableCore(SdmxStructureType structureType0)
        {
            this.structureType = structureType0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MutableCore"/> class.
        /// </summary>
        /// <param name="createdFrom">
        /// The created from. 
        /// </param>
        protected internal MutableCore(ISdmxObject createdFrom)
        {
            this.structureType = createdFrom.StructureType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the structure type.
        /// </summary>
        public virtual SdmxStructureType StructureType
        {
            get
            {
                return this.structureType;
            }

            set
            {
                this.structureType = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create tertiary.
        /// </summary>
        /// <param name="isSet">
        /// The is set. 
        /// </param>
        /// <param name="valueren">
        /// The valueren. 
        /// </param>
        /// <returns>
        /// The <see cref="TertiaryBool"/> . 
        /// </returns>
        protected internal static TertiaryBool CreateTertiary(bool isSet, bool valueren)
        {
            return SdmxObjectUtil.CreateTertiary(isSet, valueren);
        }

        #endregion
    }
}
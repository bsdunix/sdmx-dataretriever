// -----------------------------------------------------------------------
// <copyright file="DataProviderSchemeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    ///   The data provider scheme mutable core.
    /// </summary>
    [Serializable]
    public class DataProviderSchemeMutableCore :
        ItemSchemeMutableCore<IDataProviderMutableObject, IDataProvider, IDataProviderScheme>, 
        IDataProviderSchemeMutableObject
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataProviderSchemeMutableCore"/> class.
        /// </summary>
        /// <param name="dataProviderScheme">
        /// The dataProviderScheme. 
        /// </param>
        public DataProviderSchemeMutableCore(IDataProviderScheme dataProviderScheme)
            : base(dataProviderScheme)
        {
            foreach (IDataProvider dataProvider in dataProviderScheme.Items)
            {
                this.AddItem(new DataProviderMutableCore(dataProvider));
            }
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="DataProviderSchemeMutableCore" /> class.
        /// </summary>
        public DataProviderSchemeMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProviderScheme))
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the immutable instance.
        /// </summary>
        public override IDataProviderScheme ImmutableInstance
        {
            get
            {
                return new DataProviderSchemeCore(this);
            }
        }

        #endregion

        #region Explicit Interface Properties

       
       

        #endregion

        #region Overrides of ItemSchemeMutableCore<IDataProviderMutableObject,IDataProvider,IDataProviderScheme>

        public override IDataProviderMutableObject CreateItem(string id, string name)
        {
            IDataProviderMutableObject dataProviderMutableCore = new DataProviderMutableCore();
            dataProviderMutableCore.Id = id;
            dataProviderMutableCore.AddName("en", name);
            AddItem(dataProviderMutableCore);
            return dataProviderMutableCore;
        }

        #endregion

        
    }
}
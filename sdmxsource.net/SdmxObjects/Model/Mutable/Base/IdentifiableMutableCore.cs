// -----------------------------------------------------------------------
// <copyright file="IdentifiableMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///   The identifiable mutable core.
    /// </summary>
    [Serializable]
    public abstract class IdentifiableMutableCore : AnnotableMutableCore, IIdentifiableMutableObject
    {
        #region Fields

        /// <summary>
        ///   The id.
        /// </summary>
        private string _id;

        /// <summary>
        ///   The uri.
        /// </summary>
        private Uri _uri;

        /// <summary>
        ///   The urn.
        /// </summary>
        private Uri _urn;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentifiableMutableCore"/> class.
        /// </summary>
        /// <param name="structureType">
        /// The structure type. 
        /// </param>
        protected IdentifiableMutableCore(SdmxStructureType structureType)
            : base(structureType)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentifiableMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The obj target. 
        /// </param>
        protected IdentifiableMutableCore(IIdentifiableObject objTarget)
            : base(objTarget)
        {
            this._id = objTarget.Id;
            if (objTarget.Uri != null)
            {
                this._uri = objTarget.Uri;
            }

            this._urn = objTarget.Urn;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return this._id;
            }

            set
            {
                this._id = value;
            }
        }

        /// <summary>
        ///   Gets or sets the uri.
        /// </summary>
        public virtual Uri Uri
        {
            get
            {
                return this._uri;
            }

            set
            {
                this._uri = value;
            }
        }

        /// <summary>
        ///   Gets or sets the urn.
        /// </summary>
        public virtual Uri Urn
        {
            get
            {
                return this._urn;
            }

            set
            {
                this._urn = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The build identifiable attributes.
        /// </summary>
        /// <param name="reader">
        /// The reader. 
        /// </param>
        protected internal void BuildIdentifiableAttributes(ISdmxReader reader)
        {
            this._id = reader.GetAttributeValue("id", true);
            string attributeValue = reader.GetAttributeValue("uri", false);
            if (attributeValue != null)
            {
                this.Uri = new Uri(attributeValue);
            }
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="AgencySchemeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    ///   The agency scheme mutable core.
    /// </summary>
    [Serializable]
    public class AgencySchemeMutableCore : ItemSchemeMutableCore<IAgencyMutableObject, IAgency, IAgencyScheme>, 
                                           IAgencySchemeMutableObject
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AgencySchemeMutableCore"/> class.
        /// </summary>
        /// <param name="agencyScheme">
        /// The agencyScheme. 
        /// </param>
        public AgencySchemeMutableCore(IAgencyScheme agencyScheme)
            : base(agencyScheme)
        {
            foreach (IAgency acy in agencyScheme.Items)
            {
                this.AddItem(new AgencyMutableCore(acy));
            }
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="AgencySchemeMutableCore" /> class.
        /// </summary>
        public AgencySchemeMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AgencyScheme))
        {
        }

        #endregion

        /*
         public override IAgencyScheme ImmutableInstance {
          get {
                return new AgencySchemeCore(this);
            }
        }


         IMaintainableObject IMaintainableObject.ImmutableInstance
        {
            get { return ImmutableInstance; }
        } ^^^ */
        #region Public Properties

        /// <summary>
        ///   Gets the immutable instance.
        /// </summary>
        public override IAgencyScheme ImmutableInstance
        {
            get
            {
                return new AgencySchemeCore(this);
            }
        }

     
        #endregion

        public override IAgencyMutableObject CreateItem(string id, string name)
        {
            IAgencyMutableObject acy = new AgencyMutableCore();
            acy.Id = id;
            acy.AddName("en", name);
            this.AddItem(acy);
            return acy;
        }
    }
}
// -----------------------------------------------------------------------
// <copyright file="ComponentMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///   The component mutable core.
    /// </summary>
    [Serializable]
    public abstract class ComponentMutableCore : IdentifiableMutableCore, IComponentMutableObject
    {
        #region Fields

        /// <summary>
        ///   The concept ref.
        /// </summary>
        private IStructureReference conceptRef;

        /// <summary>
        ///   The representation.
        /// </summary>
        private IRepresentationMutableObject representation;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMutableCore"/> class.
        /// </summary>
        /// <param name="structureType">
        /// The structure type. 
        /// </param>
        public ComponentMutableCore(SdmxStructureType structureType)
            : base(structureType)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The obj target. 
        /// </param>
        public ComponentMutableCore(IComponent objTarget)
            : base(objTarget)
        {
            if (objTarget.Representation != null)
            {
                this.representation = new RepresentationMutableCore(objTarget.Representation);
            }

            if (objTarget.ConceptRef != null)
            {
                this.conceptRef = objTarget.ConceptRef.CreateMutableInstance();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the concept ref.
        /// </summary>
        public virtual IStructureReference ConceptRef
        {
            get
            {
                return this.conceptRef;
            }

            set
            {
                this.conceptRef = value;
            }
        }

        /// <summary>
        ///   Gets or sets the representation.
        /// </summary>
        public virtual IRepresentationMutableObject Representation
        {
            get
            {
                return this.representation;
            }

            set
            {
                this.representation = value;
            }
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="MetadataAttributeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The metadata attributeObject mutable core.
    /// </summary>
    [Serializable]
    public class MetadataAttributeMutableCore : ComponentMutableCore, IMetadataAttributeMutableObject
    {
        #region Fields

        /// <summary>
        ///     The _metadata attributes.
        /// </summary>
        private readonly IList<IMetadataAttributeMutableObject> _metadataAttributes;

        /// <summary>
        ///     The _max occurs.
        /// </summary>
        private int? _maxOccurs;

        /// <summary>
        ///     The _min occurs.
        /// </summary>
        private int? _minOccurs;

        /// <summary>
        ///     The _presentational.
        /// </summary>
        private TertiaryBool _presentational;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataAttributeMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The obj target.
        /// </param>
        public MetadataAttributeMutableCore(IMetadataAttributeObject objTarget)
            : base(objTarget)
        {
            this._metadataAttributes = new List<IMetadataAttributeMutableObject>();
            this._presentational = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);
            this._minOccurs = objTarget.MinOccurs;
            this._maxOccurs = objTarget.MaxOccurs;
            this._presentational = objTarget.Presentational;
            if (objTarget.MetadataAttributes != null)
            {
                foreach (IMetadataAttributeObject currentMetadatAttribute in objTarget.MetadataAttributes)
                {
                    this._metadataAttributes.Add(new MetadataAttributeMutableCore(currentMetadatAttribute));
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataAttributeMutableCore" /> class.
        /// </summary>
        public MetadataAttributeMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataAttribute))
        {
            this._metadataAttributes = new List<IMetadataAttributeMutableObject>();
            this._presentational = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the max occurs.
        /// </summary>
        public virtual int? MaxOccurs
        {
            get
            {
                return this._maxOccurs;
            }

            set
            {
                this._maxOccurs = value;
            }
        }

        /// <summary>
        ///     Gets the metadata attributes.
        /// </summary>
        public virtual IList<IMetadataAttributeMutableObject> MetadataAttributes
        {
            get
            {
                return this._metadataAttributes;
            }
        }

        /// <summary>
        ///     Gets or sets the min occurs.
        /// </summary>
        public virtual int? MinOccurs
        {
            get
            {
                return this._minOccurs;
            }

            set
            {
                this._minOccurs = value;
            }
        }

        /// <summary>
        ///     Gets or sets the presentational.
        /// </summary>
        public virtual TertiaryBool Presentational
        {
            get
            {
                return this._presentational;
            }

            set
            {
                this._presentational = value;
            }
        }

        #endregion
    }
}
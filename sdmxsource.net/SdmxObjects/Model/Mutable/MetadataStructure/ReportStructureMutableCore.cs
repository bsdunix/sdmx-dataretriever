// -----------------------------------------------------------------------
// <copyright file="ReportStructureMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///   The report structure mutable core.
    /// </summary>
    [Serializable]
    public class ReportStructureMutableCore : IdentifiableMutableCore, IReportStructureMutableObject
    {
        #region Fields

        /// <summary>
        ///   The metadata attributes.
        /// </summary>
        private readonly IList<IMetadataAttributeMutableObject> _metadataAttributes;

        /// <summary>
        ///   The target metadatas.
        /// </summary>
        private readonly IList<string> _targetMetadatas;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportStructureMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The agencySchemeMutable target. 
        /// </param>
        public ReportStructureMutableCore(IReportStructure objTarget)
            : base(objTarget)
        {
            this._metadataAttributes = new List<IMetadataAttributeMutableObject>();
            if (objTarget.MetadataAttributes != null)
            {
                foreach (IMetadataAttributeObject metadataAttribute in objTarget.MetadataAttributes)
                {
                    this._metadataAttributes.Add(new MetadataAttributeMutableCore(metadataAttribute));
                }
            }

            this._targetMetadatas = objTarget.TargetMetadatas;
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="ReportStructureMutableCore" /> class.
        /// </summary>
        public ReportStructureMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportStructure))
        {
            this._metadataAttributes = new List<IMetadataAttributeMutableObject>();
            this._targetMetadatas = new List<string>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the metadata attributes.
        /// </summary>
        public IList<IMetadataAttributeMutableObject> MetadataAttributes
        {
            get
            {
                return this._metadataAttributes;
            }
        }

        /// <summary>
        ///   Gets the target metadata.
        /// </summary>
        public IList<string> TargetMetadatas
        {
            get
            {
                return this._targetMetadatas;
            }
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="DataflowMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;

    /// <summary>
    ///   The dataflow mutable core.
    /// </summary>
    [Serializable]
    public class DataflowMutableCore : MaintainableMutableCore<IDataflowObject>, IDataflowMutableObject
    {
        #region Fields

        /// <summary>
        ///   The key family ref.
        /// </summary>
        private IStructureReference dataStructureReference;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="DataflowMutableCore" /> class.
        /// </summary>
        public DataflowMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
        {
        }

        public DataflowMutableCore(IDataStructureObject dataStructure) : base(dataStructure)
        {
            base.StructureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow);
            this.dataStructureReference = dataStructure.AsReference;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataflowMutableCore"/> class.
        /// </summary>
        /// <param name="objTarget">
        /// The obj target. 
        /// </param>
        public DataflowMutableCore(IDataflowObject objTarget)
            : base(objTarget)
        {
            if (objTarget.DataStructureRef != null)
            {
                this.dataStructureReference = objTarget.DataStructureRef.CreateMutableInstance();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the data structure ref.
        /// </summary>
        public virtual IStructureReference DataStructureRef
        {
            get
            {
                return this.dataStructureReference;
            }

            set
            {
                this.dataStructureReference = value;
            }
        }

        /// <summary>
        ///   Gets the immutable instance.
        /// </summary>
        public override IDataflowObject ImmutableInstance
        {
            get
            {
                return new DataflowObjectCore(this);
            }
        }

        #endregion
    }
}
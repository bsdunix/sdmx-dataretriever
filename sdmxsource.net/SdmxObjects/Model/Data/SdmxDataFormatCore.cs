﻿// -----------------------------------------------------------------------
// <copyright file="SdmxDataFormatCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SdmxDataFormatCore : IDataFormat
    {
        private DataType _dataType;
        #region Implementation of IDataFormat

        public DataType SdmxDataFormat
        {
            get
            {
                return this._dataType;
            }
        }

        #endregion

        public SdmxDataFormatCore(DataType dataType)
        {
            if(dataType == null)
                throw new ArgumentException("Data Type can not be null for SdmxDataFormat");

            this._dataType = dataType;
        }

        public override string ToString()
        {
            return _dataType.ToString();
        }

        public virtual string FormatAsString
        {
            get
            {
                return ToString();
            }
        }
    }
}

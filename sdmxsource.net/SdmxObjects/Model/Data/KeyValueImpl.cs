// -----------------------------------------------------------------------
// <copyright file="KeyValueImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;
    using System.IO;
    using System.Runtime.Serialization;

    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    ///   The key value impl.
    /// </summary>
    [Serializable]
    public class KeyValueImpl : IKeyValue
    {
        ///// According to the http://msdn.microsoft.com/en-us/library/system.serializableattribute(v=vs.100).aspx
        ///// ALL we need to do is use the SerializableAttribute which we do. TODO test
        ///// IXmlSerializable //$$$ Externalizable 
        #region Fields

        /// <summary>
        ///   The _code.
        /// </summary>
        private readonly string _code;

        /// <summary>
        ///   The _concept.
        /// </summary>
        private readonly string _concept;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="KeyValueImpl" /> class.
        /// </summary>
        public KeyValueImpl()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyValueImpl"/> class.
        /// </summary>
        /// <param name="code0">
        /// The code 0. 
        /// </param>
        /// <param name="concept1">
        /// The concept 1. 
        /// </param>
        public KeyValueImpl(string code0, string concept1)
        {
            this._code = code0;
            this._concept = concept1;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the code.
        /// </summary>
        public virtual string Code
        {
            get
            {
                return this._code;
            }
        }

        /// <summary>
        ///   Gets the concept.
        /// </summary>
        public virtual string Concept
        {
            get
            {
                return this._concept;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The read external.
        /// </summary>
        /// <param name="formatter">
        /// The formatter. 
        /// </param>
        /// <param name="stream">
        /// The stream. 
        /// </param>
        /// <returns>
        /// The <see cref="KeyValueImpl"/> . 
        /// </returns>
        public static KeyValueImpl ReadExternal(IFormatter formatter, Stream stream)
        {
            return (KeyValueImpl)formatter.Deserialize(stream);
        }

        /// <summary>
        /// The compare to.
        /// </summary>
        /// <param name="other">
        /// Key value to compare to
        /// </param>
        /// <returns>
        /// The <see cref="int"/> . 
        /// </returns>
        public virtual int CompareTo(IKeyValue other)
        {
            if (this._concept.Equals(other.Concept))
            {
                return string.CompareOrdinal(this._code, other.Code);
            }

            return string.CompareOrdinal(this._concept, other.Concept);
        }

        /// <summary>
        /// The equals.
        /// </summary>
        /// <param name="obj">
        /// The obj. 
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> . 
        /// </returns>
        public override bool Equals(object obj)
        {
            var that = obj as IKeyValue;
            if (that != null)
            {
                return this.Concept.Equals(that.Concept) && this.Code.Equals(that.Code);
            }

            return base.Equals(obj);
        }

        /// <summary>
        ///   The get hash code.
        /// </summary>
        /// <returns> The <see cref="int" /> . </returns>
        public override int GetHashCode()
        {
            return (this._concept + this._code).GetHashCode();
        }

        /// <summary>
        ///   The to string.
        /// </summary>
        /// <returns> The <see cref="string" /> . </returns>
        public override string ToString()
        {
            return this._concept + ":" + this._code;
        }

        /// <summary>
        /// The write external.
        /// </summary>
        /// <param name="formatter">
        /// The formatter. 
        /// </param>
        /// <param name="stream">
        /// The stream. 
        /// </param>
        public void WriteExternal(IFormatter formatter, Stream stream)
        {
            formatter.Serialize(stream, this);
        }

        #endregion

        /*    public virtual void WriteExternal(ObjectOutput xout) {
            xout.WriteObject(_concept);
            xout.WriteObject(_code);
        }
    
        public virtual void ReadExternal(ObjectInput ins0) {
            _concept = (string) ins0.ReadObject();
            _code = (string) ins0.ReadObject();
        }*/

        ////public XmlSchema GetSchema()
        ////{
        ////    return null;
        ////}

        ////public void ReadXml(XmlReader reader)
        ////{
        ////    _concept = reader.ReadContentAsString();
        ////    _code = reader.ReadContentAsString();
        ////}

        ////public void WriteXml(XmlWriter writer)
        ////{
        ////    throw new NotImplementedException("Not yet implemented");
        ////    //// TODO $$$ writer.Wqqqq
        ////}
    }
}
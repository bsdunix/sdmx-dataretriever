﻿// -----------------------------------------------------------------------
// <copyright file="FooterMessageCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class FooterMessageCore :IFooterMessage
    {
        #region Implementation of IFooterMessage

        private readonly string _code;

        private readonly Severity _severity;

        private readonly IList<ITextTypeWrapper> _footerText;

        public string Code
        {
            get
            {
                return this._code;
            }
        }

        public Severity Severity
        {
            get
            {
                return this._severity;
            }
        }

        public IList<ITextTypeWrapper> FooterText
        {
            get
            {
                return this._footerText;
            }
        }

        #endregion

        public FooterMessageCore(string code, Severity severity, ITextTypeWrapper textType)
        {
            this._code = code;
            this._severity = severity;
            if (code == null)
            {
                throw new ArgumentException("FooterMessage - Code is mandatory");
            }
            if (textType == null)
            {
                throw new ArgumentException("FooterMessage - At least on e text is required");
            }
            this._footerText = new List<ITextTypeWrapper>();
            this._footerText.Add(textType);
        }

        public FooterMessageCore(string code, Severity severity, IList<ITextTypeWrapper> textType)
        {
            this._code = code;
            this._severity = severity;
            this._footerText = textType;
            if (code == null)
            {
                throw new ArgumentException("FooterMessage - Code is mandatory");
            }
            if (textType == null || textType.Count == 0)
            {
                throw new ArgumentException("FooterMessage - At least on e text is required");
            }
        }
    }
}

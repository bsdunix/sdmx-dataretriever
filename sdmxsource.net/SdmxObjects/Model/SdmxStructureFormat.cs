﻿// -----------------------------------------------------------------------
// <copyright file="SdmxStructureFormat.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    ///   TODO: Update summary.
    /// </summary>
    public class SdmxStructureFormat : IStructureFormat
    {
        private readonly StructureOutputFormat _structureFormat;

        public StructureOutputFormat SdmxOutputFormat
        {
            get
            {
                return this._structureFormat;
            }
        }

        public SdmxStructureFormat(StructureOutputFormat structureFormat)
        {
            if (structureFormat == null)
            {
                throw new ArgumentException("STRUCTURE_OUTPUT_FORMAT can not be null");
            }
            this._structureFormat = structureFormat;
        }

        public override string ToString()
        {
            return this._structureFormat.EnumType.ToString();
        }

        public string FormatAsString 
        {
            get
            {
                return ToString();
            }
        }
    }
}

// -----------------------------------------------------------------------
// <copyright file="DataKeyObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Metadata
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    ///   The data key object core.
    /// </summary>
    [Serializable]
    public class DataKeyObjectCore : SdmxObjectCore, IDataKey
    {
        #region Fields

        /// <summary>
        ///   The included.
        /// </summary>
        private readonly bool included;

        /// <summary>
        ///   The key value.
        /// </summary>
        private readonly IKeyValue keyValue;

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataKeyObjectCore"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent. 
        /// </param>
        /// <param name="type">
        /// The type. 
        /// </param>
        public DataKeyObjectCore(IReferenceValue parent, ComponentValueSetType type)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.KeyValues), parent)
        {
            this.included = type.include;
            this.keyValue = new KeyValueImpl(type.Value[0].TypedValue, type.id);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets a value indicating whether included.
        /// </summary>
        public virtual bool Included
        {
            get
            {
                return this.included;
            }
        }

        /// <summary>
        ///   Gets the key value.
        /// </summary>
        public virtual IKeyValue KeyValue
        {
            get
            {
                return this.keyValue;
            }
        }

        #endregion

        ////////////DEEP EQUALS							 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
	    public  override  bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties) 
        {
            if (sdmxObject == null)
            {
			   return false;
		    }
            if (sdmxObject.StructureType == this.StructureType)
            {
               IDataKey that = (IDataKey)sdmxObject;
			   if(this.included != that.Included)
               {
				   return false;
			   }
			   if(!string.Equals(this.keyValue, that.KeyValue)) 
               {
				   return false;
			   }

			   return base.DeepEqualsInternal(that, includeFinalProperties);
		    }

	      	return false;
	    }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
	    ////////////COMPOSITES		                     //////////////////////////////////////////////////
	    ///////////////////////////////////////////////////////////////////////////////////////////////////
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            return new HashSet<ISdmxObject>();
        }
    }
}
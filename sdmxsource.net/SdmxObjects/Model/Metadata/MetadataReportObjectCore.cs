// -----------------------------------------------------------------------
// <copyright file="MetadataReportObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Metadata
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    using TargetType = Org.Sdmxsource.Sdmx.Api.Constants.TargetType;

    #endregion

    /// <summary>
    ///   The metadata report dataStructureObject core.
    /// </summary>
    [Serializable]
    public class MetadataReportObjectCore : SdmxObjectCore, IMetadataReport
    {
        #region Fields

        /// <summary>
        ///   The id.
        /// </summary>
        private readonly string id;

        /// <summary>
        ///   The reported attributes.
        /// </summary>
        private readonly IList<IReportedAttributeObject> _reportedAttributes;

        /// <summary>
        ///   The _target.
        /// </summary>
        private readonly ITarget _target;

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="MetadataReportObjectCore" /> class.
        /// </summary>
        /// <param name="parent"> The parent. </param>
        /// <param name="report"> The dataStructureObject. </param>
        public MetadataReportObjectCore(IMetadataSet parent, ReportType report)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataReport), parent)
        {
            this._reportedAttributes = new List<IReportedAttributeObject>();
            this.id = report.id;
            if (report.Target != null)
            {
                this._target = new TargetObjectCore(this, report.Target);
            }

            if (report.AttributeSet != null)
            {
                if (ObjectUtil.ValidCollection(report.AttributeSet.ReportedAttribute))
                {
                    this._reportedAttributes.Clear();

                    foreach (ReportedAttributeType each in report.AttributeSet.ReportedAttribute)
                    {
                        this._reportedAttributes.Add(new ReportedAttributeObjectObjectCore(this, each));
                    }
                }
            }

            this.Validate();
        }

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        #region Public Properties

        /// <summary>
        ///   Gets the id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return this.id;
            }
        }

        /// <summary>
        ///   Gets the reported attributes.
        /// </summary>
        public virtual IList<IReportedAttributeObject> ReportedAttributes
        {
            get
            {
                return new List<IReportedAttributeObject>(this._reportedAttributes);
            }
        }

        /// <summary>
        ///   Gets the _target.
        /// </summary>
        public virtual ITarget Target
        {
            get
            {
                return this._target;
            }
        }

        public ISet<TargetType> Targets
        {
            get
            {
                ISet<TargetType> targets = new HashSet<TargetType>();
                foreach (IReferenceValue rv in _target.ReferenceValues)
                {
                    targets.Add(rv.TargetType);
                }
                return targets;
            }
        }

        public string TargetDatasetId
        {
            get
            {
                foreach (IReferenceValue rv in _target.ReferenceValues)
                {
                    if (rv.TargetType == TargetType.Dataset)
                    {
                        return rv.DatasetId;
                    }
                }
                return null;
            }
        }

        public ISdmxDate TargetReportPeriod
        {
            get
            {
                foreach (IReferenceValue rv in _target.ReferenceValues)
                {
                    if (rv.TargetType == TargetType.ReportPeriod)
                    {
                        return rv.ReportPeriod;
                    }
                }
                return null;
            }
        }

        public ICrossReference TargetIdentifiableReference
        {
            get
            {
                foreach (IReferenceValue rv in _target.ReferenceValues)
                {
                    if (rv.TargetType == TargetType.Identifiable)
                    {
                        return rv.IdentifiableReference;
                    }
                }
                return null;
            }
        }

        public ICrossReference TargetContentConstraintReference
        {
            get
            {
                foreach (IReferenceValue rv in _target.ReferenceValues)
                {
                    if (rv.TargetType == TargetType.Constraint)
                    {
                        return rv.ContentConstraintReference;
                    }
                }
                return null;
            }
        }

        public IList<IDataKey> TargetDataKeys
        {
            get
            {
                foreach (IReferenceValue rv in _target.ReferenceValues)
                {
                    if (rv.TargetType == TargetType.DataKey)
                    {
                        return rv.DataKeys;
                    }
                }
                return null;
            }
        }

        #endregion

        #region Methods

       ///////////////////////////////////////////////////////////////////////////////////////////////////
	   ////////////DEEP EQUALS							 //////////////////////////////////////////////////
	   ///////////////////////////////////////////////////////////////////////////////////////////////////
	
	    public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties) 
        {
            if (sdmxObject == null) 
            {
			  return false;
		    }
            if (sdmxObject.StructureType == this.StructureType)
            {
               IMetadataReport that = (IMetadataReport)sdmxObject;
			   if(!string.Equals(this.id, that.Id))
               {
				  return false;
			   }
			   if(!base.Equivalent(_target, that.Target, includeFinalProperties)) 
               {
				  return false;
			   }
		 	   if(!base.Equivalent(_reportedAttributes, that.ReportedAttributes, includeFinalProperties)) 
               {
				  return false;
			   }
		       return base.DeepEqualsInternal(that, includeFinalProperties);
	      }

		  return false;
  	    }

        /// <summary>
        ///   Gets the internal composites.
        /// </summary>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = new HashSet<ISdmxObject>();
            base.AddToCompositeSet(_target, composites);
            base.AddToCompositeSet(_reportedAttributes, composites);
            return composites;
        }

        /// <summary>
        ///   The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(this.id))
            {
                throw new SdmxSemmanticException("Metadata Report must have an Id");
            }

            if (this._target == null)
            {
                throw new SdmxSemmanticException("Metadata Report must have a Target");
            }

            if (!ObjectUtil.ValidCollection(this._reportedAttributes))
            {
                throw new SdmxSemmanticException("Metadata Report must have at least one Reported Attribute");
            }
        }

        #endregion
    }
}
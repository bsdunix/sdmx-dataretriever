// -----------------------------------------------------------------------
// <copyright file="CodeListUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Util
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.BaseObjects.Codelist;

    /// <summary>
    ///   Utility methods for dealing with CodeLists.
    /// </summary>
    public class CodeListUtil
    {
        #region Public Methods and Operators

        /// <summary>
        /// Returns the size of the longest code contained in the CodeList.
        ///   Zero will be returned if there are no codes in the CodeList.
        /// </summary>
        /// <param name="codelistObject">CodeList object
        /// </param>
        /// <returns>
        /// The size of the longest code or zero. 
        /// </returns>
        public static int DetermineMaxCodeLength(ICodelistObjectBase codelistObject)
        {
            IList<ICodeObjectBase> codes = codelistObject.Codes;
            int longestCodeLength = 0;
            foreach (ICodeObjectBase codeObjectBase in codes)
            {
                longestCodeLength = Math.Max(DetermineMaxCodeLength(codeObjectBase), longestCodeLength);
            }

            return longestCodeLength;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The determine max code length.
        /// </summary>
        /// <param name="codeObject">
        /// The code object. 
        /// </param>
        /// <returns>
        /// The <see cref="int"/> . 
        /// </returns>
        private static int DetermineMaxCodeLength(ICodeObjectBase codeObject)
        {
            int longestCodeLength = codeObject.Id.Length;
            if (codeObject.HasChildren())
            {
                foreach (ICodeObjectBase child in codeObject.Children)
                {
                    longestCodeLength = Math.Max(DetermineMaxCodeLength(child), longestCodeLength);
                }
            }

            return longestCodeLength;
        }

        #endregion
    }
}
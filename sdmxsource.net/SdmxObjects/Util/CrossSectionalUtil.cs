﻿// -----------------------------------------------------------------------
// <copyright file="CrossSectionalUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Util
{
    using System.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.structure;

    /// <summary>
    ///     The cross sectional data structure helper class.
    /// </summary>
    public static class CrossSectionalUtil
    {
        #region Public Methods and Operators

        /// <summary>
        /// Returns true if the specified <paramref name="currentType"/> has SDMX v2.0 cross sectional information.
        /// </summary>
        /// <param name="currentType">
        /// The KeyFamily instance
        /// </param>
        /// <returns>
        /// true if the specified <paramref name="currentType"/> has at least some SDMX v2.0 cross sectional information.
        /// </returns>
        public static bool IsCrossSectional(KeyFamilyType currentType)
        {
            bool isCrossSectional = currentType.Components.Dimension.All(
                component =>
                component.isFrequencyDimension || component.isMeasureDimension 
                || (component.crossSectionalAttachDataSet.HasValue && component.crossSectionalAttachDataSet.Value)
                || (component.crossSectionalAttachGroup.HasValue && component.crossSectionalAttachGroup.Value)
                || (component.crossSectionalAttachSection.HasValue && component.crossSectionalAttachSection.Value)
                || (component.crossSectionalAttachObservation.HasValue && component.crossSectionalAttachObservation.Value));
            if (currentType.Components.Attribute.Count > 0)
            {
                isCrossSectional = isCrossSectional
                                   && currentType.Components.Attribute.All(
                                       component =>
                                       (component.crossSectionalAttachDataSet.HasValue && component.crossSectionalAttachDataSet.Value)
                                       || (component.crossSectionalAttachGroup.HasValue && component.crossSectionalAttachGroup.Value)
                                       || (component.crossSectionalAttachSection.HasValue && component.crossSectionalAttachSection.Value)
                                       || (component.crossSectionalAttachObservation.HasValue && component.crossSectionalAttachObservation.Value));
            }

            return isCrossSectional;
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="ReUsingQueryParsingManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of ReUsingExamples.
// 
//     ReUsingExamples is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     ReUsingExamples is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with ReUsingExamples.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace ReUsingExamples.StructureQuery
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Structureparser.Workspace;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// The re using query parsing manager.
    /// </summary>
    public class ReUsingQueryParsingManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            IQueryParsingManager manager = new QueryParsingManager(SdmxSchemaEnumType.VersionTwo);
            IQueryWorkspace workspace;
            using (IReadableDataLocation location = new FileReadableDataLocation("QueryStructureRequest.xml"))
            {
                workspace = manager.ParseQueries(location);
            }

            if (workspace != null)
            {
                Console.WriteLine("Is Resolve references : {0}", workspace.ResolveReferences);
                Console.WriteLine("Has Structure Queries : {0}", workspace.HasStructureQueries());
                if (workspace.HasStructureQueries())
                {
                    foreach (IStructureReference simpleStructureQuery in workspace.SimpleStructureQueries)
                    {
                        IMaintainableRefObject reference = simpleStructureQuery.MaintainableReference;
                        Console.WriteLine(
                            "Requested a {0}\n\tAgency ID: {1}\n\tId: {2}\n\tVersion: {3}", 
                            simpleStructureQuery.MaintainableStructureEnumType, 
                            reference.HasAgencyId() ? reference.AgencyId : "(none)", 
                            reference.HasMaintainableId() ? reference.MaintainableId : "(none)", 
                            reference.HasVersion() ? reference.Version : "(none)");
                    }
                }
            }
        }

        #endregion
    }
}
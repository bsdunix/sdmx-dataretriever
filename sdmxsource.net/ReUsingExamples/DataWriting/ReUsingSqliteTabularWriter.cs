﻿// -----------------------------------------------------------------------
// <copyright file="ReUsingSqliteTabularWriter.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of ReUsingExamples.
// 
//     ReUsingExamples is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     ReUsingExamples is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with ReUsingExamples.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace ReUsingExamples.DataWriting
{
    using System;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;

    using Estat.Sri.TabularWriters.Engine;

    /// <summary>
    ///     Re-Using example for <see cref="SqliteTabularWriter" />
    /// </summary>
    public static class ReUsingSqliteTabularWriter
    {
        #region Public Methods and Operators

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            const int ColKeyCount = 10;
            const int ColAttributeCount = 5;
            const int Records = 1000000;
            const string TestSqlite = "test.sqlite";
            var stopwatch = new Stopwatch();

            try
            {
                stopwatch.Start();

                if (File.Exists(TestSqlite))
                {
                    File.Delete(TestSqlite);
                }

                DbProviderFactory dbProviderFactory = DbProviderFactories.GetFactory("System.Data.SQLite");

                using (DbConnection connection = dbProviderFactory.CreateConnection())
                {
                    if (connection != null)
                    {
                        connection.ConnectionString = "Data Source=" + TestSqlite + ";Version=3;";
                        using (var tabular = new SqliteTabularWriter(connection, "test_table"))
                        {
                            // write header i.e. create the table
                            tabular.StartColumns();
                            for (int i = 0; i < ColKeyCount; i++)
                            {
                                tabular.WriteColumnKey(string.Format(CultureInfo.InvariantCulture, "key{0:00}", i));
                            }

                            for (int i = 0; i < ColAttributeCount; i++)
                            {
                                tabular.WriteColumnAttribute(string.Format(CultureInfo.InvariantCulture, "attr{0:00}", i));
                            }

                            tabular.WriteColumnMeasure("measure");

                            // write data
                            for (int r = 0; r < Records; r++)
                            {
                                tabular.StartRecord();
                                for (int i = 0; i < ColKeyCount; i++)
                                {
                                    tabular.WriteCellKeyValue(string.Format(CultureInfo.InvariantCulture, "keycell{0:00}-{1:00}", r, i));
                                }

                                for (int i = 0; i < ColAttributeCount; i++)
                                {
                                    tabular.WriteCellAttributeValue(string.Format(CultureInfo.InvariantCulture, "attrcell{0:00}-{1:00}", r, i));
                                }

                                tabular.WriteCellMeasureValue(r.ToString(CultureInfo.InvariantCulture));
                            }
                        }
                    }
                }

                stopwatch.Stop();
                Console.WriteLine("Time for {0} records: {1}", Records, stopwatch.Elapsed);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.ToString());
                throw;
            }
        }

        #endregion
    }
}
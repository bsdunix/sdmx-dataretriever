﻿// -----------------------------------------------------------------------
// <copyright file="ReUsingCompactWriter.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of ReUsingExamples.
// 
//     ReUsingExamples is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     ReUsingExamples is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with ReUsingExamples.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace ReUsingExamples.DataWriting
{
    using System;
    using System.Linq;
    using System.Xml;

    using Estat.Sri.SdmxStructureMutableParser.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// The re using compact writer.
    /// </summary>
    public class ReUsingCompactWriter
    {
        #region Static Fields

        /// <summary>
        ///     The parsing manager.
        /// </summary>
        private static readonly IStructureParsingManager _parsingManager = new StructureMutableParsingManager();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            // 1. We need a IDataStructureObject. In this example we read it from a file. Alternative we could build it from a mutable object.
            IDataStructureObject dataStructure;
            using (IReadableDataLocation readable = new FileReadableDataLocation("ESTAT+STS+2.0.xml"))
            {
                IStructureWorkspace structureWorkspace = _parsingManager.ParseStructures(readable);

                ISdmxObjects structureBeans = structureWorkspace.GetStructureObjects(false);
                dataStructure = structureBeans.DataStructures.FirstOrDefault();
            }

            if (dataStructure == null)
            {
                throw new InvalidOperationException("Could not build dataStructure object");
            }

            using (XmlWriter writer = XmlWriter.Create("re-using-compact-writer.xml", new XmlWriterSettings { Indent = true }))
            {
                // initialize the data writing engine. It can be for SDMX versions 2.0 or 2.1
                IDataWriterEngine dataWriterEngine = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo));

                // write header
                dataWriterEngine.WriteHeader(new HeaderImpl("ZZ9", "ZZ9"));

                // start dataset
                dataWriterEngine.StartDataset(null, dataStructure, null);

                // write dataset attributes
                dataWriterEngine.WriteAttributeValue("TITLE", "CompactDataWriter test");

                // write 2 group entries
                dataWriterEngine.StartGroup("SIBLING");
                dataWriterEngine.WriteGroupKeyValue("REF_AREA", "EL");
                dataWriterEngine.WriteGroupKeyValue("STS_INDICATOR", "PROD");
                dataWriterEngine.WriteGroupKeyValue("STS_ACTIVITY", "NS0030");
                dataWriterEngine.WriteGroupKeyValue("STS_INSTITUTION", "1");
                dataWriterEngine.WriteGroupKeyValue("ADJUSTMENT", "N");
                dataWriterEngine.WriteAttributeValue("COMPILATION", "test");

                dataWriterEngine.StartGroup("SIBLING");
                dataWriterEngine.WriteGroupKeyValue("REF_AREA", "EL");
                dataWriterEngine.WriteGroupKeyValue("STS_INDICATOR", "IND");
                dataWriterEngine.WriteGroupKeyValue("STS_ACTIVITY", "NS0030");
                dataWriterEngine.WriteGroupKeyValue("STS_INSTITUTION", "1");
                dataWriterEngine.WriteGroupKeyValue("ADJUSTMENT", "N");
                dataWriterEngine.WriteAttributeValue("COMPILATION", "test2");

                // write a series entry
                dataWriterEngine.StartSeries();
                dataWriterEngine.WriteSeriesKeyValue("FREQ", "A");
                dataWriterEngine.WriteSeriesKeyValue("REF_AREA", "EL");
                dataWriterEngine.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                dataWriterEngine.WriteGroupKeyValue("STS_ACTIVITY", "NS0030");
                dataWriterEngine.WriteGroupKeyValue("STS_INSTITUTION", "1");
                dataWriterEngine.WriteGroupKeyValue("ADJUSTMENT", "N");
                dataWriterEngine.WriteAttributeValue("TIME_FORMAT", "P1Y");

                // write 2 observations for the abose series
                dataWriterEngine.WriteObservation("2001", "1.23");
                dataWriterEngine.WriteAttributeValue("OBS_STATUS", "A");
                dataWriterEngine.WriteAttributeValue("OBS_CONF", "F");

                dataWriterEngine.WriteObservation("2002", "4.56");
                dataWriterEngine.WriteAttributeValue("OBS_STATUS", "A");
                dataWriterEngine.WriteAttributeValue("OBS_CONF", "F");

                // write another series entry
                dataWriterEngine.StartSeries();
                dataWriterEngine.WriteSeriesKeyValue("FREQ", "A");
                dataWriterEngine.WriteSeriesKeyValue("REF_AREA", "EL");
                dataWriterEngine.WriteSeriesKeyValue("STS_INDICATOR", "IND");
                dataWriterEngine.WriteGroupKeyValue("STS_ACTIVITY", "NS0030");
                dataWriterEngine.WriteGroupKeyValue("STS_INSTITUTION", "1");
                dataWriterEngine.WriteGroupKeyValue("ADJUSTMENT", "N");
                dataWriterEngine.WriteAttributeValue("TIME_FORMAT", "P1Y");

                // write 1 observation for the abose series
                dataWriterEngine.WriteObservation("2001", "7.89");
                dataWriterEngine.WriteAttributeValue("OBS_STATUS", "A");
                dataWriterEngine.WriteAttributeValue("OBS_CONF", "F");

                // close compact Writer
                dataWriterEngine.Close();
            }
        }

        #endregion
    }
}
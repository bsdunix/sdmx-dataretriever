﻿// -----------------------------------------------------------------------
// <copyright file="ReUsingStructureWritingManagerFast.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of ReUsingExamples.
// 
//     ReUsingExamples is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     ReUsingExamples is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with ReUsingExamples.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace ReUsingExamples.Structure
{
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Xml;

    using Estat.Sri.SdmxStructureMutableParser.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;

    /// <summary>
    /// The re using structure writing manager.
    /// </summary>
    public class ReUsingStructureWritingManagerFast
    {
        #region Public Methods and Operators

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            // 1. Create a test mutable codelist with Count codes.
            const int Count = 1000;
            string countStr = Count.ToString(CultureInfo.InvariantCulture);
            ICodelistMutableObject codelist = new CodelistMutableCore();
            codelist.Id = "CL_K" + countStr;
            codelist.AgencyId = "TEST";
            codelist.AddName("en", "Test CL with " + countStr);
            for (int i = 0; i < Count; i++)
            {
                ICodeMutableObject code = new CodeMutableCore();
                code.Id = i.ToString(CultureInfo.InvariantCulture);
                code.AddName("en", "Code " + code.Id);
                codelist.AddItem(code);
            }

            // 2. Select the out filename
            string output = string.Format(CultureInfo.InvariantCulture, "{0}.xml", codelist.Id);

            // 3. Create the immutable instance of the codelist
            ICodelistObject immutableInstance = codelist.ImmutableInstance;

            using (FileStream writer = File.OpenWrite(output))
            using (XmlWriter xmlWriter = XmlWriter.Create(writer, new XmlWriterSettings { Encoding = Encoding.UTF8, Indent = true }))
            {
                // 4. Create the StructureMutableWritingManager, a faster but limited to a subset of SDMX v2.0 writer. 
                IStructureWriterManager writingManager = new StructureMutableWritingManager(xmlWriter);

                // 5. Write the codelist as a SDMX-ML v2.0 Registry Interface document. Other options include SDMX-ML 2.0 Structure document.
                writingManager.WriteStructure(immutableInstance, new HeaderImpl("ZZ9", "ZZ9"), new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument)), null);
            }
        }

        #endregion
    }
}
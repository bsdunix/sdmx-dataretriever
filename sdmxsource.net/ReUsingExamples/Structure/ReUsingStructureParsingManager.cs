﻿// -----------------------------------------------------------------------
// <copyright file="ReUsingStructureParsingManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2015 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of ReUsingExamples.
// 
//     ReUsingExamples is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     ReUsingExamples is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with ReUsingExamples.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace ReUsingExamples.Structure
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// The re using structure parsing manager.
    /// </summary>
    public class ReUsingStructureParsingManager
    {
        #region Public Methods and Operators

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="InvalidOperationException">
        /// Could not load Structure objects
        /// </exception>
        public static void Main(string[] args)
        {
            // 1. Initialize the StructureParsingManager. This implementation supports both 2.0 and 2.1.
            IStructureParsingManager parsingManager = new StructureParsingManager(SdmxSchemaEnumType.VersionTwo);

            // 2. open IReadableDataLocation
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation("ESTAT_CPI_v1.0.xml"))
            {
                // 3. Get the IStructureWorkspace
                IStructureWorkspace structureWorkspace = parsingManager.ParseStructures(dataLocation);

                // 4. Get the ISdmxObjects without resolving cross-references.
                ISdmxObjects structureObjects = structureWorkspace.GetStructureObjects(false);

                if (structureObjects == null)
                {
                    throw new InvalidOperationException("Could not load Structure objects");
                }
            }
        }

        #endregion
    }
}
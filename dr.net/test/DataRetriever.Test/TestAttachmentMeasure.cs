﻿// -----------------------------------------------------------------------
// <copyright file="TestAttachmentMeasure.cs" company="EUROSTAT">
//   Date Created : 2015-06-30
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace DataRetriever.Test
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Xml;

    using Estat.Nsi.DataRetriever;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;

    /// <summary>
    /// Test unit for <see cref="ISdmxDataRetrievalWithCrossWriter"/>
    /// </summary>
    [TestFixture("sqlserverA")]
    [TestFixture("mysql")]
    [TestFixture("odp3")]
    public class TestAttachmentMeasure
    {
        #region Fields

        /// <summary>
        /// The _data query parse manager.
        /// </summary>
        private readonly IDataQueryParseManager _dataQueryParseManager;

        /// <summary>
        /// The _data retrieval.
        /// </summary>
        private readonly ISdmxDataRetrievalWithCrossWriter _dataRetrieval;

        /// <summary>
        /// The _retrieval manager.
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _retrievalManager;

        private readonly string _name;

        #endregion
        /// <summary>
        /// Initializes a new instance of the <see cref="TestAttachmentMeasure"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public TestAttachmentMeasure(string name)
        {
            this._name = name;
            var connectionString = ConfigurationManager.ConnectionStrings [name];
            this._dataRetrieval = new DataRetrieverCore(new HeaderImpl("TestXS", "ZZ9"), connectionString, SdmxSchemaEnumType.VersionTwo);
            this._dataQueryParseManager = new DataQueryParseManager(SdmxSchemaEnumType.VersionTwo);
            this._retrievalManager = new MappingStoreSdmxObjectRetrievalManager(connectionString);
        }

        /// <summary>
        /// Test unit for <see cref="ISdmxDataRetrievalWithCrossWriter.GetData"/> 
        /// </summary>
        [Test]
        public void TestGetData()
        {
            var dataQuery = this._dataQueryParseManager.ParseRestQuery("data/ESTAT,DEMOGRAPHY,2.3/ALL", this._retrievalManager);
            var outputFileName = string.Format("test-attachment-measures{0}.xml", this._name);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings() {Indent = true}))
            using (ICrossSectionalWriterEngine xsWriter = new CrossSectionalWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo)))
            {
                this._dataRetrieval.GetData(dataQuery, xsWriter);
            }

            var crossDsd = (ICrossSectionalDataStructureObject)dataQuery.DataStructure;
            var crossMeasures = crossDsd.CrossSectionalMeasures.ToDictionary(measure => measure.Id);

            var crossObservationAttributes = crossDsd.GetCrossSectionalAttachObservation(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute)).ToDictionary(component => component.Id);
            using (XmlReader reader = XmlReader.Create(outputFileName))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                            case XmlNodeType.Element:
                            {
                                string localName = reader.LocalName;
                                ICrossSectionalMeasure crossMeasure;
                                if (crossMeasures.TryGetValue(localName, out crossMeasure))
                                {
                                    while (reader.MoveToNextAttribute())
                                    {
                                        string attributeName = reader.LocalName;
                                        IComponent attribute;
                                        if (crossObservationAttributes.TryGetValue(attributeName, out attribute))
                                        {
                                            var attributeCrossMeasures = crossDsd.GetAttachmentMeasures((IAttributeObject)attribute);
                                            CollectionAssert.Contains(attributeCrossMeasures, crossMeasure);
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }
    }
}
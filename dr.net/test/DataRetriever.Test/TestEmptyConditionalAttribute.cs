﻿// -----------------------------------------------------------------------
// <copyright file="TestEmptyConditionalAttribute.cs" company="EUROSTAT">
//   Date Created : 2014-05-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace DataRetriever.Test
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Xml;

    using Estat.Nsi.DataRetriever;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;

    /// <summary>
    /// Test unit for <see cref="DataRetrieverCore"/>
    /// </summary>
    public class TestEmptyConditionalAttribute
    {
        /// <summary>
        /// Test unit for <see cref="DataRetrieverCore.GetData(Org.Sdmxsource.Sdmx.Api.Model.Data.Query.IDataQuery,Org.Sdmxsource.Sdmx.Api.Engine.IDataWriterEngine)"/> 
        /// </summary>
        [TestCase("/data/SSTSCONS_PROD_A/ALL", "sqlserver2")]
        public void TestGetDataDataWriterEngineEmptyAttr(string query, string name)
        {
            var connectionString = ConfigurationManager.ConnectionStrings [name];
            var dataQuery = new DataQueryImpl(new RESTDataQueryCore(query), new MappingStoreSdmxObjectRetrievalManager(connectionString));
            const string EmptyAttributeXML = "empty-attribute.xml";
            using (XmlWriter writer = XmlWriter.Create(EmptyAttributeXML, new XmlWriterSettings() {Indent = true}))
            {
                IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));

                ISdmxDataRetrievalWithWriter sdmxDataRetrievalWithWriter = new DataRetrieverCore(new HeaderImpl("TestEmptyConditionalAttribute", "ZZ9"), connectionString, SdmxSchemaEnumType.VersionTwoPointOne);
                sdmxDataRetrievalWithWriter.GetData(dataQuery, dataWriter);
                writer.Flush();
            }
            var fileInfo = new FileInfo(EmptyAttributeXML);
            Assert.IsTrue(fileInfo.Exists);
            using (var fileStream = fileInfo.OpenRead())
            using (var reader = XmlReader.Create(fileStream))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            {
                                var localName = reader.LocalName;
                                if (localName.Equals("Group"))
                                {
                                    Assert.IsTrue(reader.HasAttributes);
                                    var dateStr = reader.GetAttribute("NAT_TITLE");
                                    Assert.IsNull(dateStr);
                                }
                                else if (localName.Equals("Obs"))
                                {
                                    Assert.IsTrue(reader.HasAttributes);
                                    var attribute = reader.GetAttribute("OBS_COM");
                                    Assert.IsTrue(attribute == null || !string.IsNullOrWhiteSpace(attribute));
                                }
                            }

                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Test unit for <see cref="DataRetrieverCore.GetData(Org.Sdmxsource.Sdmx.Api.Model.Data.Query.IDataQuery,Org.Sdmxsource.Sdmx.Api.Engine.IDataWriterEngine)"/> 
        /// </summary>
        [TestCase("/data/SSTSCONS_SDMXRI_137/ALL", "sqlserver4", "OBS_CONF", new[] {null, "F", null, "F", "F", null, "F", "C", null, null, "F", null})]
        public void TestGetDataDataWriterEngineEmptyAttrTranscoding(string query, string name, string attrId, string [] expectedValues)
        {
            var connectionString = ConfigurationManager.ConnectionStrings [name];
            var dataQuery = new DataQueryImpl(new RESTDataQueryCore(query), new MappingStoreSdmxObjectRetrievalManager(connectionString));
            const string EmptyAttributeXML = "empty-attribute-tr.xml";
            using (XmlWriter writer = XmlWriter.Create(EmptyAttributeXML, new XmlWriterSettings() { Indent = true }))
            {
                IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));

                ISdmxDataRetrievalWithWriter sdmxDataRetrievalWithWriter = new DataRetrieverCore(new HeaderImpl("TestEmptyConditionalAttribute", "ZZ9"), connectionString, SdmxSchemaEnumType.VersionTwoPointOne);
                sdmxDataRetrievalWithWriter.GetData(dataQuery, dataWriter);
                writer.Flush();
            }
            var fileInfo = new FileInfo(EmptyAttributeXML);
            Assert.IsTrue(fileInfo.Exists);
            var queue = new LinkedList<string>(expectedValues);
            using (var fileStream = fileInfo.OpenRead())
            using (var reader = XmlReader.Create(fileStream))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            {
                                var localName = reader.LocalName;
                                if (localName.Equals("Obs"))
                                {
                                    Assert.IsTrue(reader.HasAttributes);
                                    var value = reader.GetAttribute(attrId);
                                    Assert.IsTrue(value == null || !string.IsNullOrWhiteSpace(value));
                                    Assert.True(queue.Contains(value));
                                    queue.Remove(value);
                                }
                            }

                            break;
                    }
                }
            }

            Assert.IsEmpty(queue);
        }
    }
}
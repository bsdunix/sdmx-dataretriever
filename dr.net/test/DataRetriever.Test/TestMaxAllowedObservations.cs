﻿// -----------------------------------------------------------------------
// <copyright file="TestMaxAllowedObservations.cs" company="EUROSTAT">
//   Date Created : 2015-01-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace DataRetriever.Test
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Xml;

    using Estat.Nsi.DataRetriever;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Test unit for Max allowed observation behavior. This is not related to DataQuery First/Last obs or Default limit.
    /// </summary>
    [TestFixture]
    public class TestMaxAllowedObservations
    {
        #region Fields

        /// <summary>
        /// The _data query parse manager.
        /// </summary>
        private readonly IDataQueryParseManager _dataQueryParseManager;

        /// <summary>
        /// The _default header
        /// </summary>
        private readonly HeaderImpl _defaultHeader;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TestDataRetrieverSdmxV21"/> class. 
        /// </summary>
        public TestMaxAllowedObservations()
        {
            this._dataQueryParseManager = new DataQueryParseManager(SdmxSchemaEnumType.Null);
            this._defaultHeader = new HeaderImpl("TestMaxAllowedObservations", "ZZ9");
        }

        #endregion

        /// <summary>
        /// Test unit for 
        /// </summary>
        [TestCase("data/IT,56_259/ALL/ALL", "sqlserver", 10, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=AREA", "sqlserver2", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=STS_ACTIVITY", "sqlserver2", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=STS_INDICATOR", "sqlserver2", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=TIME_PERIOD", "sqlserver2", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=AllDimensions", "sqlserver2", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&firstNObservations=1", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&firstNObservations=1", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&firstNObservations=1", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=1", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=1", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=1", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=1&firstNObservations=1", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=1&firstNObservations=1", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=1&firstNObservations=1", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=AllDimensions&lastNObservations=1&firstNObservations=1", "sqlserver3", 100)]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&firstNObservations=3", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&firstNObservations=4", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&firstNObservations=4", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&firstNObservations=4", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=3", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=5", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=5", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&lastNObservations=5", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=5&firstNObservations=4", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=3&firstNObservations=4", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=5&firstNObservations=3", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&lastNObservations=5&firstNObservations=3", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=dataonly", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=nodata", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=serieskeysonly", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=full", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=dataonly", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=nodata", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=serieskeysonly", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=full", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=dataonly", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=nodata", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=serieskeysonly", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=full", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=dataonly", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=nodata", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=serieskeysonly", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=full", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,DEMOGRAPHY/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=full", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,DEMOGRAPHY/ALL/ALL?dimensionAtObservation=AllDimensions&detail=full", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,CPI_PCAXIS/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=full", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,CPI_PCAXIS/ALL/ALL?dimensionAtObservation=AllDimensions&detail=full", "sqlserver3", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,CPI_PCAXIS/ALL/ALL?dimensionAtObservation=AllDimensions&detail=full", "sqlserver3", 1000000)]
        public void TestRest(string restUrl, string connectionName, int allowedObs)
        {
            var connectionString = ConfigurationManager.ConnectionStrings [connectionName];
            var mappingStoreSdmxObjectRetrievalManager = new MappingStoreSdmxObjectRetrievalManager(connectionString);
            var dataQuery = this._dataQueryParseManager.ParseRestQuery(restUrl, mappingStoreSdmxObjectRetrievalManager);

            Assert.IsNotNull(dataQuery);

            var outputFileName = string.Format("REST-{0}-{1}-{2}-{3}--max-obsout.xml", dataQuery.Dataflow.Id, dataQuery.DimensionAtObservation, dataQuery.FirstNObservations, dataQuery.LastNObservations);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings { Indent = true }))
            {
                IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));

                ISdmxDataRetrievalWithWriter sdmxDataRetrievalWithWriter = new DataRetrieverCore(this._defaultHeader, connectionString, SdmxSchemaEnumType.VersionTwoPointOne, allowedObs);
                sdmxDataRetrievalWithWriter.GetData(dataQuery, dataWriter);
                writer.Flush();
            }
        }

        [TestCase("tests/get-structure-specific-primary-measure-op.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-primary-measure-op2.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-primary-measure-op-and.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-data-firstnobs-lastnobs.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-data-firstnobs-lastnobs.xml", "sqlserver2", 1, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("tests/get-structure-specific-full-notequal-and.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-full-equal-or.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-full-numeric-less.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-full-numeric-greater.xml", "sqlserver2", 100)]
        [TestCase("tests/text-search-madb-contains.xml", "sqlserver3", 100)]
        [TestCase("tests/text-search-madb-not-contains.xml", "sqlserver3", 100)]
        [TestCase("tests/get-structure-specific-data.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-data-default-limit.xml", "sqlserver2", 100, ExpectedException = typeof(SdmxResponseTooLargeException))]
        [TestCase("tests/get-structure-specific-data-dimatobs.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-data.xml", "sqlserver2", 100)]
        [TestCase("tests/ESTAT+SSTSIND_PROD_M+2.0_2014_01_28_18_05_46.query.xml", "sqlserver3", 100)]
        [TestCase("tests/IT1+161_267+1.0_2014_06_03_17_17_55.query.xml", "sqlserver", 100, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("tests/get-structure-specific-data-dimatobs.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-data-dimatobs-sts_activity.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-data2.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-dataonly.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-full.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-full-all-dim.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-serieskey.xml", "sqlserver2", 100)]
        [TestCase("tests/get-structure-specific-nodata.xml", "sqlserver2", 100)]
        public void TestSoap21(string filePath, string name, int allowedObs)
        {
             var connectionString = ConfigurationManager.ConnectionStrings[name];
            IList<IComplexDataQuery> dataQueries;
                var mappingStoreSdmxObjectRetrievalManager = new MappingStoreSdmxObjectRetrievalManager(connectionString);
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filePath))
            {
                dataQueries = this._dataQueryParseManager.BuildComplexDataQuery(dataLocation, mappingStoreSdmxObjectRetrievalManager);
            }

            Assert.IsNotEmpty(dataQueries);
            var dataQuery = dataQueries.First();

            var outputFileName = string.Format("{0}-soap-v21-max-obs-out.xml", filePath);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings { Indent = true }))
            {
                IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));

                IAdvancedSdmxDataRetrievalWithWriter advancedSdmxDataRetrievalWithWriter = new DataRetrieverCore(this._defaultHeader, connectionString, SdmxSchemaEnumType.VersionTwoPointOne, allowedObs);
                advancedSdmxDataRetrievalWithWriter.GetData(dataQuery, dataWriter);
                writer.Flush();
            }
        }

        [TestCase("tests/v20/QueryTimeRangeV20.xml", "sqlserver3", 1, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("tests/v20/QueryTimeRangeV20.xml", "sqlserver3", 1000)]
        public void TestSDMXv20(string filePath, string name, int allowedObs)
        {
            var connectionString = ConfigurationManager.ConnectionStrings [name];
            IList<IDataQuery> dataQueries;
            var mappingStoreSdmxObjectRetrievalManager = new MappingStoreSdmxObjectRetrievalManager(connectionString);
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filePath))
            {
                dataQueries = this._dataQueryParseManager.BuildDataQuery(dataLocation, mappingStoreSdmxObjectRetrievalManager);
            }

            Assert.IsNotEmpty(dataQueries);
            var dataQuery = dataQueries.First();

            var outputFileName = string.Format("{0}-soap-v20-max-obs-out.xml", filePath);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings { Indent = true }))
            {
                IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo));

                ISdmxDataRetrievalWithWriter advancedSdmxDataRetrievalWithWriter = new DataRetrieverCore(this._defaultHeader, connectionString, SdmxSchemaEnumType.VersionTwo, allowedObs);
                advancedSdmxDataRetrievalWithWriter.GetData(dataQuery, dataWriter);
                writer.Flush();
            }
        }

        [TestCase("tests/v20/get-CENSUSHUB_Q_XS1.xml", "sqlserver3", 4, ExpectedException = typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("tests/v20/get-CENSUSHUB_Q_XS1.xml", "sqlserver3", 100000)]
        public void TestSDMXv20XS(string filePath, string name, int allowedObs)
        {
            var connectionString = ConfigurationManager.ConnectionStrings [name];
            IList<IDataQuery> dataQueries;
            var mappingStoreSdmxObjectRetrievalManager = new MappingStoreSdmxObjectRetrievalManager(connectionString);
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filePath))
            {
                dataQueries = this._dataQueryParseManager.BuildDataQuery(dataLocation, mappingStoreSdmxObjectRetrievalManager);
            }

            Assert.IsNotEmpty(dataQueries);
            var dataQuery = dataQueries.First();

            var outputFileName = string.Format("{0}-soap-v20-xs-max-obs-out.xml", filePath);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings { Indent = true }))
            {
                ICrossSectionalWriterEngine dataWriter = new CrossSectionalWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo));

                ISdmxDataRetrievalWithCrossWriter advancedSdmxDataRetrievalWithWriter = new DataRetrieverCore(this._defaultHeader, connectionString, SdmxSchemaEnumType.VersionTwo, allowedObs);
                advancedSdmxDataRetrievalWithWriter.GetData(dataQuery, dataWriter);
                writer.Flush();
            }
        }
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="IQueryEngineManager.cs" company="EUROSTAT">
//   Date Created : 2011-12-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Manager
{
    using Estat.Nsi.DataRetriever.Engines;
    using Estat.Nsi.DataRetriever.Model;

    /// <summary>
    /// The interface for manager classes that determine which <see cref="IDataQueryEngine{T}"/> to use.
    /// </summary>
    /// <typeparam name="T">
    /// The DR state type.
    /// </typeparam>
    internal interface IQueryEngineManager<in T>
        where T : DataRetrievalInfo
    {
        #region Public Methods

        /// <summary>
        /// Get a <see cref="IDataQueryEngine{T}"/> implementation based on the specified <paramref name="info"/>
        /// </summary>
        /// <param name="info">
        /// The current data retrieval state 
        /// </param>
        /// <returns>
        /// a <see cref="IDataQueryEngine{T}"/> implementation based on the specified <paramref name="info"/> 
        /// </returns>
        IDataQueryEngine<T> GetQueryEngine(T info);

        #endregion
    }
}
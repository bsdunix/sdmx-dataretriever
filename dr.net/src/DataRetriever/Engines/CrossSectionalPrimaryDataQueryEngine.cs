﻿// -----------------------------------------------------------------------
// <copyright file="CrossSectionalPrimaryDataQueryEngine.cs" company="EUROSTAT">
//   Date Created : 2011-12-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Engines
{
    using Estat.Nsi.DataRetriever.Model;

    /// <summary>
    /// Dissemination Data Query engine for XS DSD without XS Measures.
    /// </summary>
    internal class CrossSectionalPrimaryDataQueryEngine : CrossSectionalDataQueryEngineBase, IDataQueryEngine<DataRetrievalInfoXS>
    {
        #region Constants and Fields

        /// <summary>
        ///   The singleton instance
        /// </summary>
        private static readonly CrossSectionalPrimaryDataQueryEngine _instance =
            new CrossSectionalPrimaryDataQueryEngine();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Prevents a default instance of the <see cref="CrossSectionalPrimaryDataQueryEngine" /> class from being created.
        /// </summary>
        private CrossSectionalPrimaryDataQueryEngine()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the singleton instance
        /// </summary>
        public static CrossSectionalPrimaryDataQueryEngine Instance
        {
            get
            {
                return _instance;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Write a primary measure observation
        /// </summary>
        /// <param name="row">
        /// The map between components and their values 
        /// </param>
        /// <param name="info">
        /// The current Data Retrieval state 
        /// </param>
        /// <returns>
        /// The number of observations stored. 
        /// </returns>
        protected override int WriteObservation(MappedXsValues row, DataRetrievalInfoXS info)
        {
            return WriteObservation(row, row.PrimaryMeasureValue.Key.Id, row.PrimaryMeasureValue.Value, info);
        }

        #endregion
    }
}
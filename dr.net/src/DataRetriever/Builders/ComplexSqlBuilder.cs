﻿// -----------------------------------------------------------------------
// <copyright file="ComplexSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2011-12-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;

using Estat.Nsi.DataRetriever.Model;
using Estat.Nsi.DataRetriever.Properties;
using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;

using log4net;

using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Nsi.DataRetriever.Builders
{
    /// <summary>
    /// Sql builder which includes all components and orders by dimensions
    /// </summary>
    internal class ComplexSqlBuilder : SqlBuilderBase, ISqlBuilder
    {
        #region Constants and Fields

        /// <summary>
        ///   The singleton instance
        /// </summary>
        private static readonly ComplexSqlBuilder _instance = new ComplexSqlBuilder();

        private static readonly ILog Logger = LogManager.GetLogger(typeof(ComplexSqlBuilder));

        /// <summary>
        /// The ordered component builder
        /// </summary>
        private static readonly SeriesOrderedDimensionBuilder _orderedComponentBuilder = new SeriesOrderedDimensionBuilder();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Prevents a default instance of the <see cref="ComplexSqlBuilder" /> class from being created.
        /// </summary>
        private ComplexSqlBuilder()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the singleton instance
        /// </summary>
        public static ComplexSqlBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// This method generates the SQL SELECT statement for the dissemination database that will return the data for the incoming Query.
        /// </summary>
        /// <param name="info">
        /// The current state of the data retrieval which containts the current query and mapping set 
        /// </param>
        public void GenerateSql(DataRetrievalInfo info)
        {
            Logger.Info(Resources.InfoBeginGenerateSql);

            var seriesInfo = info as DataRetrievalInfoSeries;
            if (seriesInfo == null)
            {
                throw new ArgumentException("seriesInfo is not of DataRetrievalInfoSeries type");
            }

            SqlQuery sqlQuery = new SqlQuery();
            string sql = string.Empty;

            try
            {
                sql = GenerateSelect(false, seriesInfo.ComponentMapping.Values);
                sqlQuery.appendSql(sql);

                sqlQuery.appendSql(GenerateFrom(seriesInfo.MappingSet));

                AppendCachedWhere(seriesInfo, sqlQuery);

                var orderComponents = _orderedComponentBuilder.Build(seriesInfo);
                sqlQuery.appendSql(GenerateOrderBy(seriesInfo, orderComponents));
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                throw new DataRetrieverException(ex,
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError),
                    Resources.ErrorUnableToGenerateSQL);
            }

            // log for easy debug
            Logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedSQLFormat1, sql));
            Logger.Info(Resources.InfoEndGenerateSql);

            info.SqlString = sqlQuery.getSql();
        }

        /// <summary>
        /// Appends the cached where to <paramref name="sql"/> from <see cref="DataRetrievalInfoSeries.SqlWhereCache"/> if it is not null or from <see cref="SqlBuilderBase.GenerateWhere"/>
        /// </summary>
        /// <param name="info">
        /// The current DataRetrieval state 
        /// </param>
        /// <param name="sql">
        /// The SQL String buffer to 
        /// </param>
        private static void AppendCachedWhere(DataRetrievalInfoSeries info, SqlQuery sqq)
        {
            if (string.IsNullOrEmpty(info.SqlWhereCache))
            {
                info.SqlWhereCache = GenerateComplexWhere(info);
            }

            sqq.appendSql(info.SqlWhereCache);
        }

        #endregion
    }
}

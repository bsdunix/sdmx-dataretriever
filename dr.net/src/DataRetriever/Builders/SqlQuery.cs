﻿// -----------------------------------------------------------------------
// <copyright file="SqlQuery.cs" company="EUROSTAT">
//   Date Created : 2013-04-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using Org.Sdmxsource.Sdmx.Api.Model.Query;

namespace Estat.Nsi.DataRetriever.Builders
{
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    class SqlQuery : IDataQueryFormat<SqlQuery>
    {
        private StringBuilder _sql = new StringBuilder(string.Empty);

        public string getSql()
        {
            return _sql.ToString();
        }

        public void appendSql(string sql)
        {
            _sql.Append(sql);
        }

    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="DataRetrieverHelper.cs" company="EUROSTAT">
//   Date Created : 2011-12-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever
{
    using Estat.Nsi.DataRetriever.Properties;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    /// A helper class with static methods
    /// </summary>
    public static class DataRetrieverHelper
    {
        #region Public Methods

        /// <summary>
        /// This method retrieves the <see cref="Estat.Sdmx.Model.Query.DataFlowBean"/> from a <see cref="Estat.Sdmx.Model.Query.QueryBean"/>
        /// </summary>
        /// <param name="query">
        /// The <see cref="Estat.Sdmx.Model.Query.QueryBean"/> conaining the <see cref="Estat.Sdmx.Model.Query.DataFlowBean"/> 
        /// </param>
        /// <exception cref="DataRetrieverException">
        /// See the
        ///   <see cref="ErrorTypes.QUERY_PARSING_ERROR"/>
        /// </exception>
        /// <returns>
        /// The <see cref="Estat.Sdmx.Model.Query.DataFlowBean"/> 
        /// </returns>
        public static IDataflowObject GetDataflowFromQuery(IDataQuery query)
        {
            return query.Dataflow;
        }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="DataRetrievalInfoComplex.cs" company="EUROSTAT">
//   Date Created : 2011-12-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;

using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;

using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

namespace Estat.Nsi.DataRetriever.Model
{
    internal class DataRetrievalInfoComplex : DataRetrievalInfoSeries
    {
        #region Constants and Fields

        /// <summary>
        ///   Writer provided that is based on the XS model to write the retrieved data.
        /// </summary>
        private readonly IDataWriterEngine _complexWriter;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRetrievalInfoTabular"/> class.
        /// </summary>
        /// <param name="mappingSet">
        /// The mapping set of the dataflow found in the sdmx query 
        /// </param>
        /// <param name="query">
        /// The current SDMX Query object 
        /// </param>
        /// <param name="connectionStringSettings">
        /// The Mapping Store connection string settings 
        /// </param>
        /// <param name="complexWriter">
        /// The tabular Writer. 
        /// </param>
        public DataRetrievalInfoComplex(
            MappingSetEntity mappingSet,
            IComplexDataQuery query,
            ConnectionStringSettings connectionStringSettings,
            IDataWriterEngine complexWriter)
            : base(mappingSet, query, connectionStringSettings, complexWriter)
        {
            this.Limit = query.DefaultLimit.HasValue ? query.DefaultLimit.Value : 0;
        }

        #endregion
    }
}

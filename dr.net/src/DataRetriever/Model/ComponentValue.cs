﻿// -----------------------------------------------------------------------
// <copyright file="ComponentValue.cs" company="EUROSTAT">
//   Date Created : 2012-01-20
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;

    /// <summary>
    /// A class that holds a component and value pairs
    /// </summary>
    internal class ComponentValue
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentValue"/> class.
        /// </summary>
        /// <param name="key">
        /// The dsd component.
        /// </param>
        public ComponentValue(ComponentEntity key)
        {
            this.Key = key;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the DSD Component
        /// </summary>
        public ComponentEntity Key { get; private set; }

        /// <summary>
        ///   Gets or sets the value
        /// </summary>
        public string Value { get; set; }

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="XsMeasureCache.cs" company="EUROSTAT">
//   Date Created : 2011-12-05
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// This class is used to use buffering in order prevent big TS files when XS measures are mapped.
    /// </summary>
    internal class XsMeasureCache
    {
        #region Constants and Fields

        /// <summary>
        ///   The list of time and observation value per XS Measure. It is used only when XS measures are mapped
        /// </summary>
        private readonly List<List<ComponentValue>> _attributes =
            new List<List<ComponentValue>>();

        /// <summary>
        ///   The list of all series dimension values minus the measure dimension. It is used only when XS measures are mapped.
        /// </summary>
        private readonly List<ComponentValue> _cachedSeriesAttributes;

        /// <summary>
        ///   The list of all series dimension values minus the measure dimension. It is used only when XS measures are mapped.
        /// </summary>
        private readonly List<ComponentValue> _cachedSeriesKey;

        /// <summary>
        ///   The list of time and observation value per XS Measure. It is used only when XS measures are mapped
        /// </summary>
        private readonly List<KeyValuePair<string, string>> _xsMeasureCachedObservations =
            new List<KeyValuePair<string, string>>();

        /// <summary>
        ///   The measure code
        /// </summary>
        private readonly string _xsMeasureCode;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XsMeasureCache"/> class.
        /// </summary>
        /// <param name="cachedSeriesKey">
        /// The last Cached Series Key. 
        /// </param>
        /// <param name="cachedSeriesAttributes">
        /// The xs Measure Cached Series Attributes. 
        /// </param>
        /// <param name="xsMeasureCode">
        /// The xs Measure Code. 
        /// </param>
        public XsMeasureCache(
            List<ComponentValue> cachedSeriesKey, 
            List<ComponentValue> cachedSeriesAttributes, 
            string xsMeasureCode)
        {
            this._cachedSeriesKey = cachedSeriesKey;
            this._cachedSeriesAttributes = cachedSeriesAttributes;
            this._xsMeasureCode = xsMeasureCode;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the list of time and observation value per XS Measure. It is used only when XS measures are mapped
        /// </summary>
        public List<List<ComponentValue>> Attributes
        {
            get
            {
                return this._attributes;
            }
        }

        /// <summary>
        ///   Gets the list of all series dimension values minus the measure dimension. It is used only when XS measures are mapped.
        /// </summary>
        public IEnumerable<ComponentValue> CachedSeriesAttributes
        {
            get
            {
                return this._cachedSeriesAttributes;
            }
        }

        /// <summary>
        ///   Gets the list of all series dimension values minus the measure dimension. It is used only when XS measures are mapped.
        /// </summary>
        public IEnumerable<ComponentValue> CachedSeriesKey
        {
            get
            {
                return this._cachedSeriesKey;
            }
        }

        /// <summary>
        ///   Gets the list of time and observation value per XS Measure. It is used only when XS measures are mapped
        /// </summary>
        public List<KeyValuePair<string, string>> XSMeasureCachedObservations
        {
            get
            {
                return this._xsMeasureCachedObservations;
            }
        }

        /// <summary>
        ///   Gets the measure code
        /// </summary>
        public string XSMeasureCode
        {
            get
            {
                return this._xsMeasureCode;
            }
        }

        #endregion
    }
}
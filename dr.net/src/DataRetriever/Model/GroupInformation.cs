// -----------------------------------------------------------------------
// <copyright file="GroupInformation.cs" company="EUROSTAT">
//   Date Created : 2011-11-30
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System.Collections.Generic;

    using Estat.Sri.MappingStoreRetrieval.Engine.Mapping;
    using Estat.Sri.MappingStoreRetrieval.Model.MappingStoreModel;

    /// <summary>
    /// This class holds TimeSeries Group information
    /// </summary>
    internal class GroupInformation
    {
        #region Constants and Fields

        /// <summary>
        ///   The component mappings for this group
        /// </summary>
        private readonly List<IComponentMapping> _componentMappings = new List<IComponentMapping>();

        /// <summary>
        ///   The set of keys already processed
        /// </summary>
        private readonly IDictionary<ReadOnlyKey, object> _keySet = new Dictionary<ReadOnlyKey, object>();

        /// <summary>
        ///   The group entity
        /// </summary>
        private readonly GroupEntity _thisGroup;

        /// <summary>
        ///   The SQL String
        /// </summary>
        private string _sql;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupInformation"/> class.
        /// </summary>
        /// <param name="thisGroup">
        /// The group entity 
        /// </param>
        public GroupInformation(GroupEntity thisGroup)
        {
            this._thisGroup = thisGroup;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the component mappings for this group
        /// </summary>
        public List<IComponentMapping> ComponentMappings
        {
            get
            {
                return this._componentMappings;
            }
        }

        /// <summary>
        ///   Gets the set of keys already processed
        /// </summary>
        public IDictionary<ReadOnlyKey, object> KeySet
        {
            get
            {
                return this._keySet;
            }
        }

        /// <summary>
        ///   Gets or sets the group dimension that is a measure component and is not mapped.
        /// </summary>
        public ComponentEntity MeasureComponent { get; set; }

        /// <summary>
        ///   Gets or sets the SQL String
        /// </summary>
        public string SQL
        {
            get
            {
                return this._sql;
            }

            set
            {
                this._sql = value;
            }
        }

        /// <summary>
        ///   Gets the group entity
        /// </summary>
        public GroupEntity ThisGroup
        {
            get
            {
                return this._thisGroup;
            }
        }

        #endregion
    }
}
// -----------------------------------------------------------------------
// <copyright file="IMappedValues.cs" company="EUROSTAT">
//   Date Created : 2011-12-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    /// <summary>
    /// Mapped values collection interface
    /// </summary>
    internal interface IMappedValues
    {
        #region Public Properties

        /// <summary>
        ///   Gets or sets the Time dimension value
        /// </summary>
        string TimeValue { get; set; }

        /// <summary>
        ///   Gets or sets the frequency value value
        /// </summary>
        string FrequencyValue { get; set; }

        /// <summary>
        /// Gets or sets the dimension at observation value
        /// </summary>
        string DimensionAtObservationValue { get; set; }

        #endregion

        #region Public Methods

        /////// <summary>
        /////// Add the <paramref name="component"/> and it's <paramref name="value"/> to the list
        /////// </summary>
        /////// <param name="component">
        /////// The component. 
        /////// </param>
        /////// <param name="value">
        /////// The mapped/transcoded value. 
        /////// </param>
        /////// <exception cref="ArgumentOutOfRangeException">
        /////// Invalid
        ///////   <see cref="SdmxComponentType"/>
        ///////   -or-
        ///////   Invalid
        ///////   <see cref="AttachmentLevel"/>
        /////// </exception>
        ////void Add(ComponentEntity component, string value);

        /// <summary>
        /// Add the <paramref name="value"/> to component at <paramref name="index"/>. 
        /// The index is specified in constructor
        /// </summary>
        /// <param name="index">The index</param>
        /// <param name="value">The value</param>
        void Add(int index, string value);
/*
        /// <summary>
        /// Clear all values
        /// </summary>
        void Clear();
        */
        #endregion
    }
}